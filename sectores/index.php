<?php
require_once ('head.php');
require_once '../datos/Database.php';
?>

<div class="page-content">
    <div class="page-breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="../inicio/">Inicio</a>
            </li>
            <li class="active" >Sectores</li>
        </ul>
    </div>
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                Sectores
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-body bordered-left bordered-warning">
                                    <span class="widget-caption"> Sectores</span>
                                </div>
                                <div class="widget-body">
                                    <center>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" ><b>Busqueda : </b></label>
                                            <input type="text"  id="bs-sector" name="bs-sector" size="40" placeholder="cod, nombre, almacen ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="nuevosector.php"  class="btn btn-primary" >Nuevo Sector</a>
                                        </div>
                                    </center>
                                    <br />
                                    <!-- TABLA DE SECTORES-->
                                    <div class="registros" id="agrega-registros">                    
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Cod.
                                                    </th>
                                                    <th>
                                                        Nombre sector
                                                    </th>

                                                    <th>
                                                        Almacen
                                                    </th>
                                                    <th>
                                                        Opciones
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $auxSuc = $_SESSION['userMaster'];
                                                $auxidsucursal = $auxSuc['idSucursal'];
                                                $registro = "SELECT sec.idSector,sec.nombreSector,al.nombreAlmacen FROM 
                                                                Sectores sec, Almacen al , Sucursal su 
                                                                WHERE sec.idAlmacen=al.idAlmacen 
                                                                AND su.idSucursal=al.idSucursal 
                                                                AND su.idSucursal='$auxidsucursal' and sec.estado='1'";
                                                
                                                try {
                                                    $comando = Database::getInstance()->getDb()->prepare($registro);
                                                    $comando->execute();
                                                    while ($row = $comando->fetch()) {
                                                        $idS = $row['idSector'];
                                                        ?><tr>
                                                            <td id="idSector">
                                                                <?= $idS ?>
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <?= $row['nombreSector'] ?>
                                                            </td>
                                                            <td>
                                                                <?= $row['nombreAlmacen'] ?>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:verSector('<?= $idS ?>');"  class="btn btn-default btn-xs blue" >
                                                                    <i class="fa fa-eye"></i> Ver</a>
                                                                <a href="javascript:editarSector('<?= $idS ?>');"  class="btn btn-default btn-xs purple"  >
                                                                    <i class="fa fa-edit"></i> Editar</a>
                                                                <a href="javascript:eliminarSector('<?= $idS ?>');" class="btn btn-default btn-xs black" >
                                                                    <i class="fa fa-trash-o"></i> Eliminar</a>    
                                                            </td>

                                                        </tr>
                                                        <?php
                                                    }//terminacion del while
                                                } catch (PDOException $e) {
                                                    echo 'Error: ' . $e;
                                                }
                                                ?>    
                                            </tbody>
                                        </table>
                                    </div>    
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--     D I A L O G   M O D A L ##V E R##   -->
<div class="modal fade" id="formulario_ver"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Sector</h4>
            </div>
            <form id="formularioVer" class="formulario">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Codigo
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control "  id="sectorcod" name="sectorcod" readonly="true">
                            </div>

                            <div class="form-group">
                                Nombre
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control"  id="sectornombre" name="sectornombre" readonly="true">
                            </div>

                            <div class="form-group">
                                Almacen
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control"  id="sectoralmacen" name="sectoralmacen" readonly="true">
                            </div>
                            <br />             

                            <br />             
                            <!--BOTONES-->
                            <br />            
                            <center>
                                <div class="form-group">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                </div>  
                            </center>
                        </div>
                    </div>                        
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- D I A L O G   M O D A L   ##E D I T A R## -->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModallLabel">Editar Almacen</h4>
            </div>
            <form id="formularioEditar" name="formularioEditar" class="formulario" >
                <div class="modal-body">                        
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Codigo
                                <input type="text" class="form-control"  id="seccod_edit" name="seccod_edit" readonly="true">
                            </div>

                            <div class="form-group">
                                Nombre
                                <input type="text" class="form-control"  id="secnombre_edit" name="secnombre_edit">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-left" for="inputEmail3">Almacen</label> 
                                <br>
                                <div id="sectoralmacen_edit">

                                </div>
                            </div>
                            <br />             

                            <br />             
                            <!--BOTONES-->
                            <br />            
                            <center>
                                <div class="form-group">
                                    <button class="btn btn-primary" onclick="return actualizarAlmacen()" data-dismiss="modal" aria-hidden="true">Guardar</button>&nbsp;&nbsp;&nbsp;
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                                </div>   
                            </center>
                        </div>
                    </div>                        
                </div>
            </form>
        </div> 
        <!--/.modal-content--> 
    </div> 
    <!--/.modal-dialog--> 
</div>


<?php
include_once '../header_footer/footer.php';
?>
<?php 
    function listaAlmacenes(){
        $auxSuc = $_SESSION['userMaster'];
        $auxidsucursal = $auxSuc['idSucursal'];
        $consultaalmacen = "SELECT al.* FROM 
                            Sectores sec, Almacen al , Sucursal su 
                            WHERE sec.idAlmacen=al.idAlmacen 
                            AND su.idSucursal=al.idSucursal 
                            AND su.idSucursal='$auxidsucursal'";
        require_once '../datos/Database.php';    
        try {
            $comando = Database::getInstance()->getDb()->prepare($consultaalmacen);
            $comando->execute();
                while ($listaAlmacenes = $comando->fetch()) {
            ?>
                <option value="<?= $listaAlmacenes['idAlmacen'] ?>"><?= $listaAlmacenes['nombreAlmacen'] ?></option>
                <?php
            }
            
        } catch (PDOException $e) {
           echo 'Error: ' . $e;
        }
    }
?>
