<?php
require_once ('head.php');
require_once '../datos/Database.php';
?>
<div class="page-content">
    <!-- Arbol de navegacion -->
    <div class="page-breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="../inicio/">Inicio</a>
            </li>
            <li>                
                <a href="../sectores/">Sector</a>
            </li>
            <li class="active">Nuevo Sector</li>
        </ul>
    </div>

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                Nuevo Sector
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>


    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-body bordered-left bordered-warning">
                                    <span class="widget-caption">Nuevo Sector</span>
                                </div>
                                <div class="widget-body">
                                    <div>

                                        <form id="registrarsector" method="post" action="sector_guardar.php"
                                              data-bv-message="This value is not valid"
                                              data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                                              data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                                              data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">

                                            <center>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Nombre</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" required class="form-control" id="sectornombre" name ="sectornombre" placeholder="ingrese un nombre"
                                                               data-bv-notempty="true"
                                                               data-bv-notempty-message="Ingrese nombre del sector en campo vacio"/>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Sector</label>
                                                    <div class="col-lg-10">
                                                        <div class="form-inline">
                                                            <select class="col-xs-3" required id="altipo" name="altipo" data-toggle="simplecolorpicker">
                                                                <option value="0">Selecciones un tipo de almacen</option>
                                                                <?php
                                                                    listaAlmacenes();
                                                                ?>
                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>                                                        
                                                <div class="row">                                                        
                                                    <div class="col-lg-offset-4 col-lg-4">
                                                        <input class="btn btn-primary" type="submit" value="Guardar" />
                                                        <button type="button" class="btn btn-info shiny"  onclick="location = 'index.php'" >Cancelar</button>
                                                    </div>

                                                </div>

                                            </center>



                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="registra_tipoAlm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Ver Datos de Cliente</b></h4>
            </div>

                          aqui agregamos los registros

            <form id="Formulario_Almacen" class="formulario">
                <div class="modal-body">


                    <div class="row" >
                        <div class="col-lg-12 col-sm-12 col-xs-12" >

                            <div class="form-inline" >        
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="nit">Tipo almacen</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" id="almacen" name="almacen">&nbsp;&nbsp;
                                </div>
                                BOTONES
                                <br>  
                                <center>
                                    <footer>
                                        <div class="form-inline" >
                                            <div class="form-group">
                                                <button onclick=" return RegistrarTipoAlmacen();" class="btn btn-primary" id="nuevo" data-dismiss="modal" aria-hidden="true">Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button  class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                                            </div>

                                        </div>
                                    </footer>
                                </center>
                            </div>
                        </div>
                    </div>

                </div>


            </form>
        </div>
    </div>
</div>


<!--F U N C I O N E S   P H P -->
<?php

function listaAlmacenes() {
    $auxSuc = $_SESSION['userMaster'];
        $auxidsucursal = $auxSuc['idSucursal'];
        $consultaalmacen = "SELECT al.* FROM 
                            Almacen al , Sucursal su 
                            WHERE su.idSucursal=al.idSucursal 
                            AND su.idSucursal='$auxidsucursal'";
        require_once '../datos/Database.php';    
        try {
            $comando = Database::getInstance()->getDb()->prepare($consultaalmacen);
            $comando->execute();
                while ($listaAlmacenes = $comando->fetch()) {
            ?>
                <option value="<?= $listaAlmacenes['idAlmacen'] ?>"><?= $listaAlmacenes['nombreAlmacen'] ?></option>
                <?php
            }
            
        } catch (PDOException $e) {
           echo 'Error: ' . $e;
        }
}
?>

<?php

function listaSucursales() {
    //$consulta = "SELECT * FROM privilegiosmodulo";
    $consulta = "select* from sucursal";
    require_once '../datos/Database.php';
    try {
        // Preparar sentencia
        $comando = Database::getInstance()->getDb()->prepare($consulta);
        // Ejecutar sentencia preparada
        $comando->execute();
        while ($lstModulos = $comando->fetch()) {
            ?>
            </option><option value="<?= $lstModulos['idSucursal'] ?>"><?= $lstModulos['nombreSucur'] ?></option>
            <?php
        }
    } catch (PDOException $e) {
        echo 'Error: ' . $e;
    }
}
?>
<!-- F U N C I O N E S   J A V A   S C R I P T -->

<?php
require_once ('../header_footer/footer.php');
?>