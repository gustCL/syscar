$(function() {
//HEN Busqueda almacen <-Funciona !  :-D ->
    $('#bs-sector').on('keyup', function() {
        var dato = $('#bs-sector').val();
        var url = 'sector_busqueda.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'dato=' + dato,
            success: function(datos) {
                $('#agrega-registros').html(datos);
            }
        });
        return false;
    });
});



//HEN  Funciona 
function verSector(id) {
    $('#formularioVer')[0].reset();
    var url = 'sector_ver.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(valores) {
            var datos = eval(valores);
            $('#sectorcod').val(datos[0]);
            $('#sectornombre').val(datos[1]);
            $('#sectoralmacen').val(datos[2]);
            $('#formulario_ver').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;

};

function eliminarSector(id) {
    var url = 'sector_eliminar.php';
    var pregunta = confirm('¿Esta seguro de eliminar este Sector?');
    if (pregunta == true) {
        $.ajax({
            type: 'POST',
            url: url,
            data: 'id=' + id,
            success: function(registro) {
                $('#agrega-registros').html(registro);
                return false;
            }
        });
        return false;
    } else {
        return false;
    }
};

function editarSector(id){ 
    alert(id);
    $('#formularioEditar')[0].reset();
    var url = 'sector_editar_ver.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(valores) {
            
            var datos = eval(valores);
            $('#seccod_edit').val(datos[0]);
            $('#secnombre_edit').val(datos[1]);
            $('#sectoralmacen_edit').html(datos[2]);
            $('#modal_editar').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;
}



function actualizarSector(){
     var dato = $("#formularioEditar").serialize();         
	$.ajax({                
                url: 'sector_editar.php',
                data: dato,
                type: 'post',
		success: function(registro){
			
			$('#mensaje').addClass('bien').html('Edicion completada con exito').show(200).delay(2500).hide(200);
			$('#agrega-registros').html(registro);
                       
			return false;
			
		}
                
	});
	return false;  
};


$(function(){

	$('#nuevoTipoAlmacen').on('click',function(){
		$('#Formulario_Almacen')[0].reset();
		//$('#actu').hide();
		$('#nuevo').show();
		$('#registra_tipoAlm').modal({
			show:true,
			backdrop:'static'
		});
	});
        	
});




function RegistrarTipoAlmacen(){ //MODIFICA LOS DATOS DEL CLIENTE 
     var frm = $("#Formulario_Almacen").serialize();
	$.ajax({
                url: '../datosAlmacen/guardar_almacen.php',
                data: frm,
                type: 'post',
		success: function(registro){			
			//$('#mensaje').addClass('bien').html('Edicion completada con exito').show(200).delay(2500).hide(200);
			
                       
			return false;
			
		}
                
	});
	return false;
};