<?php
    require_once '../extras/config.php';
    require_once '../datos/Database.php';

    $query_tipo_egreso = "SELECT * FROM TipoEgresoCaja WHERE estado = 1";
    $cmd_tipo_egreso = Database::getInstance()->getDb()->prepare($query_tipo_egreso);
    $cmd_tipo_egreso->execute();
?>
<table class="table table-striped">
    <?php
    $row_count = $cmd_tipo_egreso->rowCount();
    $nro = 0;
    if ($row_count > 0): ?>
        <tr>
            <th><strong>Número</strong></th>
            <th><strong>Descripción</strong></th>
            <th><strong>Opciones</strong></th>
        </tr>
        <?php while ($result = $cmd_tipo_egreso->fetch()):
            $nro++;
            $id = $result['idTipoEgresoCaja']; ?>
            <tr>
                <td><?= $nro; ?></td>
                <td><?= $result['nombreTipoEgresoCaja']; ?></td>
                <td>
                    <a href="javascript: editarTipoEgreso('<?= $id; ?>');" class="btn btn-default btn-xs purple"><i class="fa fa-edit"></i>Editar</a>
                    <a href="javascript: eliminarTipoEgreso('<?= $id; ?>');" class="btn btn-default btn-xs black"><i class="fa fa-trash-o"></i>
                        Eliminar</a>
                </td>
            </tr>
        <?php endwhile; ?>
    <?php else: ?>
        <div class="alert alert-info" role="alert">
            <center>
                <h3>No se encontraron resultados.</h3>
            </center>
        </div>
    <?php endif; ?>
</table>