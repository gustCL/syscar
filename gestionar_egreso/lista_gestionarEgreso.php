<?php 

include "../datos/conexion.php";
include "../extras/PHPPaging.lib/PHPPaging.lib.php";
ini_set("date.timezone", "America/La_Paz");
$fechaI=$_REQUEST['fechaInicial'];
$fechaF=$_REQUEST['fechaFinal'];
$fechaInicial=date_create($fechaI);
$fechaFinal=date_create($fechaF); 
mysql_query("SET NAMES 'utf8'");
$nro=0;
     $consulta="SELECT ec.*, tec.nombreTipoEgresoCaja FROM EgresoCaja ec, TipoEgresoCaja tec,  InicioSesion vs,Sucursal s WHERE ec.estado = 1 AND tec.estado=1 AND ec.fechaRegistro>='".date_format($fechaInicial, 'd-m-Y')."' AND ec.fechaRegistro<='".date_format($fechaFinal, 'd-m-Y')."' AND tec.idTipoEgresoCaja=ec.idTipoEgresoCaja  AND ec.idInicio = vs.idInicio and vs.sucursal=s.idSucursal ";
   $consultaTotal="SELECT sum(ec.montoTotal) as total FROM EgresoCaja ec, TipoEgresoCaja tec,  InicioSesion vs,Sucursal s WHERE ec.estado = 1 AND tec.estado=1 AND ec.fechaRegistro>='".date_format($fechaInicial, 'd-m-Y')."' AND ec.fechaRegistro<='".date_format($fechaFinal, 'd-m-Y')."' AND tec.idTipoEgresoCaja=ec.idTipoEgresoCaja  AND ec.idInicio = vs.idInicio and vs.sucursal=s.idSucursal ";
    if($_REQUEST['sucursal'] != 'Todos'){
        $consulta.="  and vs.sucursal=".$_REQUEST['sucursal'];
        $consultaTotal.=" and vs.sucursal=".$_REQUEST['sucursal'];       
    }
        if ($_REQUEST['glosa'] != ''){
        $consulta.=" and ec.glosa LIKE '".$_REQUEST['glosa']."%'";
        $consultaTotal.=" and ec.glosa LIKE '".$_REQUEST['glosa']."%'";
    }
    if($_REQUEST['tipoEgreso'] != 'Todos'){
        $consulta.=" and tec.idTipoEgresoCaja = ".$_REQUEST['tipoEgreso'];
        $consultaTotal.=" and tec.idTipoEgresoCaja = ".$_REQUEST['tipoEgreso'];       
    }   
$comando = mysql_query($consultaTotal);
$resTotal= mysql_fetch_array($comando);    
$montoTotal=$resTotal['total'];
$pagina = new PHPPaging;
?>
<br/> 
                                <table class="table table-striped">
                                    
                                    <thead>
                                        <!--Fila de titulos de columnas-->
                                        <tr>
                                            <th>
                                    <center><strong> Nro.</strong></center>
                                            </th>
                                            <th>
                                    <center>  <strong>Fecha</strong></center>
                                            </th>
                                            <th>
                                    <center>   <strong>Tipo Egreso</strong></center>
                                            </th>
                                            
                                            <th>
                                    <center> <strong>Glosa</strong></center>
                                            </th>
                                            <th>
                                    <center>  <strong>Nro. Factura</strong></center>
                                            </th>
                                            <th>
                                    <center><strong>Nro. de Compra</strong></center>
                                            </th>
                                            <th>
                                    <center> <strong>Monto (Bs.)</strong></center>
                                            </th>
                                            <th>
                                            </th>

                                        </tr>
                                    </thead>
<?php
$pagina->agregarConsulta($consulta);

  $pagina->modo('desarrollo'); 
  $pagina->verPost(true);
$pagina->porPagina(10);
  $pagina->paginasAntes(4);
  $pagina->paginasDespues(4);
  $pagina->linkSeparador(" - "); //Significa que no habrá separacion
  $pagina->div('div_listar');
  $pagina->linkSeparadorEspecial('...');   // Separador especial
  
$pagina->ejecutar();

while($res=$pagina->fetchResultado()){
        
	$fecha=$res["fechaRegistro"];
	$nombre=$res["nombreTipoEgresoCaja"];
	$glosa=$res["glosa"];
        $nFac=$res["nroFactura"];
        $nNot=$res["nroNotaIngreso"];
        $monto=$res['montoTotal'];
        $id=$res["idEgresoCaja"];
?>  
<tbody>                                    
<tr>
<td><center><?php echo $nro++ ?></center></td>    
<td><center><?php echo $fecha; ?></center></td>
<td><center><?php echo $nombre; ?></center></td>
<td><center><?php echo $glosa; ?></center></td>
<td><center><?php echo $nFac; ?></center></td>
<td><center><?php echo $nNot; ?></center></td>
<td><center><?php echo $monto?></center></td>
<td><center> 
    <a href="javascript:fn_mostrarEgreso('<?=$id?>');" id="editar" class="btn btn-default btn-xs purple" ><i class="fa fa-edit"></i> Editar</a>
    <a href="javascript:fn_eliminarEgreso('<?=$id?>');" id="eliminar" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
    </center></td>                                                               
</tr>
</tbody>
<?php
}
?>
<tfoot>
        <tr>
                                            <td colspan="5" invisible bg-snow> </td>
                                            <td align="right"><strong>Monto total (Bs.):</strong> </td>
                                            <td><center><?=  number_format($montoTotal,2)?></center></td>
                                        </tr>
            <td colspan="7">Numero de pagina: <?php echo $pagina->fetchNavegacion(); ?></td>                   
        </tr>
</tfoot>
</table>
