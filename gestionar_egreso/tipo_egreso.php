<?php
    require_once 'head.php';
?>
    <div class="page-content">
        <!-- subtTitulo -->
        <div class="page-header position-relative">
            <div class="header-title">
                <h1><strong>TIPO DE EGRESO</strong></h1>
            </div>
            <div class="header-buttons">
                <a class="sidebar-toggler" href="#">
                    <i class="fa fa-arrows-h"></i>
                </a>
                <a class="refresh" id="refresh-toggler" href="#">
                    <i class="glyphicon glyphicon-refresh"></i>
                </a>
                <a class="fullscreen" id="fullscreen-toggler" href="#">
                    <i class="glyphicon glyphicon-fullscreen"></i>
                </a>
            </div>
        </div>

        <div class="main-container container-fluid">
            <!-- Page Body -->
            <div class="page-body">
                <div class="widget-body">
                    <div class="container">
                    <h3>
                        <center><strong>REGISTRAR TIPO DE EGRESO</strong></center>
                    </h3>
                    <br>
                    <form class="form-horizontal" id="frm-registrar-tipo-egreso" name="frm-registrar-tipo-egreso" role="form"
                          action="javascript: registrarTipoEgreso();">
                        <div class="form-group">
                            <label class="control-label col-md-4 lead">Descripción:</label>
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="descripcion-tipo-egreso" id="descripcion-tipo-egreso" required>
                            </div>
                            <div class="col-md-2">
                                <!--div class="alert alert-danger" id="alerta" hidden>Disponible</div-->
                                <label class="control-label lead" id="alerta" style="color: cornflowerblue;" hidden><strong>Disponible</strong></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <center>
                                <input type="submit" class="btn btn-primary" id="btn-reg-tipo-egreso" value="Registrar">
                                <input type="button" value="Limpiar" class="btn btn-azure"
                                       onclick="$('#frm-registrar-tipo-egreso').trigger('reset');">
                            </center>
                        </div>
                    </form>
                    </div>
                </div>
                <br>

                <div class="widget-body">
                    <h3><center><strong>TIPOS DE EGRESO REGISTRADOS</strong></center></h3>
                    <div class="table-responsive" id="div-tipo-egreso">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="div-editar-sucursal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><b>Datos de Sucursal</b></h4>
                </div>
                <form id="frm-editar-sucursal" name="frm-editar-sucursal" class="formulario">
                    <input id="id-sucursal" name="id-sucursal" hidden>
                    <div class="modal-body">
                        <table style="border-collapse:separate; border-spacing: 5px; width: 100%;">
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label no-padding-right">Nombre</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input class="form-control" type="text"
                                               id="edit-nombre-sucursal"
                                               name="edit-nombre-sucursal" required>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label no-padding-right">Dirección</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input class="form-control" type="text"
                                               id="edit-direccion-sucursal" name="edit-direccion-sucursal" required/>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label no-padding-right"
                                               for="ci">Teléfono</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="edit-telefono-sucursal"
                                               onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                               name="edit-telefono-sucursal" required/>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label no-padding-right">Ubicación</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="edit-ubicacion-sucursal"
                                               name="edit-ubicacion-sucursal" required/>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <div class="form-inline">
                            <center>
                                <button onclick="return actualizarSucursal();" data-dismiss="modal" aria-hidden="true" class="btn btn-primary">Guardar</button>
                                <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">
                                    Cancelar
                                </button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php require_once ('../header_footer/footer.php'); ?>