$(document).ready(function(){
	fn_buscar();

    obtenerTipoEgreso();
});

$(function() {
    $('#btnNuevoIng').on('click', function() {
        
        $('#formularioReg')[0].reset();
        $('#idEgresoCaja').val('si');
        $('#registra-egreso').modal({
            show: true,
            backdrop: 'static'
        });
    });

    $('#btnNuevoTipo').on('click', function() {
        
        $('#formularioRegTipo')[0].reset();
        $('#registra-Tipoegreso').modal({
            show: true,
            backdrop: 'static'
        });
    });

    $('#descripcion-tipo-egreso').keyup(function() {
        if ($(this).val().length > 0) {
            $('#alerta').show();
        } else {
            $('#alerta').hide();
        }
        existeTipoEgreso($(this).val())
    });
});

function existeTipoEgreso(nombre) {

    $.ajax({
        url: 'existe_tipo_egreso.php',
        type: 'POST',
        data: {nombre: nombre},
        success: function (result) {
            if (result == 'true') {
                $('#btn-reg-tipo-egreso').prop('disabled', true);
                //$('#alerta').removeClass('alert alert-info').addClass('alert alert-danger').text('En uso');
                $('#alerta').css({
                    'font-weight': 'bold',
                    'color': 'red'
                }).text('En uso');
            } else {
                $('#btn-reg-tipo-egreso').prop('disabled', false);
                //$('#alerta').removeClass('alert alert-danger').addClass('alert alert-info').text('Disponible');
                $('#alerta').css({
                    'font-weight': 'bold',
                    'color': 'cornflowerblue'
                }).text('Disponible');
            }
        }
    });
    //return bool;
}

function fn_buscar(){
	var str = $("#frm_buscar").serialize();
        var inicial=$("#fechaInicial").val();
        var final=$("#fechaFinal").val();
        
	$.ajax({
		url: 'lista_gestionarEgreso.php',
		type: 'get',
		data: str+"&fechaInicial="+inicial+"&fechaFinal="+final,
		success: function(data){
                    $("#div_listar").html(data);
                    
		}
	});
}
function fn_paginar(var_div, url){
	var div = $("#" + var_div);
	$(div).load(url);
}
function GuardarRegistro(){//registra un nuevo egreso
    var frm = $("#formularioReg").serialize();
    $.ajax({
        url: 'registrar_egreso.php',
        data: frm,
        type: 'post',
        success: function(registro) {
            //alert(registro);
             swal("Guardado Correctamente", "Egreso Guardado Correctamente", "success");

           fn_buscar();
        }

    });
    return false;    
}
function fn_eliminarEgreso(id){
    $.ajax({
        url: 'eliminar_egreso.php',
        data: 'id='+id,
        type: 'post',
        success: function(registro) {
           swal("Eliminado Exitosamente!", "El Egreso Ha Sido Eliminado Correctamente", "success"); 

           fn_buscar();
        }

    });
    return false;    
}
function fn_mostrarEgreso(id){ //MUESTRA LOS DATOS DEL PROVEEDOR EN UN MODAL
   // alert(id);
	$('#formularioReg')[0].reset();
	var url = 'mostrar_egreso.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'id='+id,
		success: function(valores){
				var datos = eval(valores);
				$('#modalTipoEgreso').val(datos[0]);
				$('#nFac').val(datos[1]);
				$('#nNota').val(datos[2]);
				$('#modalGlosa').val(datos[3]);
                                $('#modalCambio').val(datos[4]);
                                $('#modalMonto').val(datos[5]); 
                                $('#modalEfectivoBs').val(datos[6]);
                                $('#modalEfectivoSus').val(datos[7]);
                                $('#idEgresoCaja').val(id);
				$('#registra-egreso').modal({
					show:true,
					backdrop:'static'
				});
			return false;
		}
	});
	return false;
}

function registrarTipoEgreso(){
    var frm = $("#frm-registrar-tipo-egreso").serialize();
    $.ajax({
        url: 'registrar_tipo_egreso.php',
        data: frm,
        type: 'post',
        success: function() {
            //swal("Guardado Correctamente", "Tipo Ingreso Guardado Correctamente", "success");
            alertify.success("Nuevo tipo de egreso registrado");
            //ActualizarRegistroTipo();
            obtenerTipoEgreso();
        }
    });
}

function ActualizarRegistroTipo(){
// var frm = $("#formularioRegTipo").serialize();
    $.ajax({
        url: 'modal_tipoEgreso.php',
//        data: frm,
        type: 'post',
        success: function(registro) {
            //alert(registro);
      $("#lista-modalTipoEgreso").html(registro);
          $("#tip_egr").html(registro); 
           
        }

    });
    return false;     
}
function fn_imprimirLista(){
       $("#frm_buscar").serialize();
redirect_by_post("imprimir.php",{sucursal:$("#sucursal").val(),fechaInicial:$("#fechaInicial").val(),fechaFinal:$("#fechaFinal").val(),tipoEgreso:$("#tipoEgreso").val(),glosa:$("#glosa").val()},true);
 
};

function redirect_by_post(purl, pparameters, in_new_tab) {
    pparameters = (typeof pparameters == 'undefined') ? {} : pparameters;
    in_new_tab = (typeof in_new_tab == 'undefined') ? true : in_new_tab;
    var form = document.createElement("form");
    $(form).attr("id", "reg-form").attr("name", "reg-form").attr("action", purl).attr("method", "post").attr("enctype", "multipart/form-data");
    if (in_new_tab) {
        $(form).attr("target", "_blank");
    }
    $.each(pparameters, function(key) {
        $(form).append('<input type="text" name="' + key + '" value="' + this + '" />');
    });
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
    return false;
}

function eliminarTipoEgreso(id){
    swal({
            title: "Eliminar Tipo de Egreso",
            text: "Está a punto de eliminar este registro ¿Desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: 'eliminar_tipo_egreso.php',
                    data: 'id='+id,
                    type: 'post',
                    success: function() {
                        //swal("Eliminado!", "Tipo de egreso eliminado!", "success");
                        alertify.success("Tipo de egreso eliminado");
                        obtenerTipoEgreso();
                    }
                });
            }
        }
    );
}

function obtenerTipoEgreso() {
    $('#div-tipo-egreso').load('obtener_tipo_egreso.php');
}
