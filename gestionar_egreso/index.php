<?php
    require_once 'head.php';
    ini_set("date.timezone", "America/La_Paz");
    $inicioDate = date("Y-m");
    $inicioDate = $inicioDate."-01";
    
?>
<!-- Page Content -->
<div class="page-content">
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>GESTIONAR EGRESO</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                   <div class="widget-body " >
                       <br/>
                       <form class="responsive" id="frm_buscar" name="frm_buscar" method="post" >  
                <div class="row">
                    <div class="col-sm-3 m-b">
                    <div class="form-group">
                        <fieldset>
                            <label for="user_name"><strong>Sucursal</strong></label>
                        <br />  
               
                        <select name='sucursal' id='sucursal' class="input-sm form-control input-s-sm inline">    
                          <!--  <option value="Todos">Todos</option>   --> 
                            <?php
                            $consulta1 = "SELECT * FROM Sucursal WHERE idEmpresa=1 AND estado = 1";
                            $comando1 = Database::getInstance()->getDb()->prepare($consulta1);
                            $comando1->execute();
                            while ($fila = $comando1->fetch()) {
                                    echo "<option value='$fila[idSucursal]'/>$fila[nombreSucur]";
                                }
                            ?>
                            </select>
                        </fieldset>
                    </div>
                    </div> 
                    <div id="lista-tipoEgreso">
                      <div class="col-sm-3 m-b">
                    <div class="form-group">
                        <fieldset>
                            <label for="user_name"><strong>Tipo de Egreso</strong></label>
                        <br />  
                        <select name="tipoEgreso" id="tipoEgreso" class="input-sm form-control input-s-sm inline">
                                <option value="Todos">Todos</option>
                            <?php
                            $consulta2 = "SELECT * FROM TipoEgresoCaja ";
                            $comando2 = Database::getInstance()->getDb()->prepare($consulta2);
                            $comando2->execute();
                            while ($fila = $comando2->fetch()) {
                                    echo "<option value='$fila[idTipoEgresoCaja]'/>$fila[nombreTipoEgresoCaja]";
                                }
                            ?>
                            </select>
                        </fieldset> 
                    </div>
                    </div>
                    </div>
                    <div class="col-sm-3 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Glosa</strong></label>
                        <br />
                        <input type="text" name="glosa" id="glosa" value="" class="input-sm form-control input-s-sm inline"/>
                        <script>
                                $('#glosa').autocomplete({
                                    source: function( request, response ) {
                                        $.ajax({
                                            url : 'autocompletado.php',
                                            dataType: "json",
                                            data: {
                                               name_startsWith: request.term,
                                               type: "G"
                                            },
                                             success: function( data ) {
                                                 response( $.map( data, function( item ) {
                                                    return {
                                                        label: item,
                                                        value: item
                                                    };
                                                }));
                                            }
                                        });
                                    }	      	
                                });
                            </script>
                        </fieldset>
                    </div>
                    </div>
                    <div class="col-sm-3 m-b">                                
                    <div class="input-group">
                       
                        <label for="user_name"></label>
                        <br />
                        <a href="tipo_egreso.php" class="btn btn-warning shiny"><i
                                class="fa fa-plus"></i>Nuevo Tipo Egreso</a>
                    </div> 
                </div>
                </div>
                <div class="row">
                <div class="col-sm-3 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Fecha Inicial</strong></label>
                        <br />
                        <input type="date" name="fechaInicial" id="fechaInicial" value="<?=$inicioDate?>" class="input-sm form-control input-s-sm inline"/>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-3 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Fecha Final</strong></label>
                        <br />
                        <input type="date" name="fechaFinal" id="fechaFinal" value="<?=date("Y-m-d")?>" class="input-sm form-control input-s-sm inline"/>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-4 m-b">                                
                    <div class="input-group">
                       
                        <label for="user_name"></label>
                        <br />
                        <input type="button" id="btnBuscar" class="btn btn-yellow" value="Buscar" onclick="fn_buscar()" />&nbsp;&nbsp;
                        <input type="button" id="btnImprimir" class="btn btn-primary" value="Imprimir" onclick="fn_imprimirLista()"/>&nbsp;&nbsp;
                        <a type="button" id="btnNuevoIng" name="btnNuevoIng" class="btn btn-warning shiny" ><i class="fa fa-plus"></i>Nuevo Egreso</a>
                        
                        
                    </div> 
                </div>
                </div>
            </form>
                            </div>
                            <br>
                          <div class="widget-body " >
                                
                              <div class="table-responsive" id="div_listar">
                              
                              </div>
                              <br/>
                            </div>
                            <br/>
                        </div>
                
                    </div>
                </div>
            </div>

    <!-- /Page Content -->
</div>
<!-- /Page Container -->
<!-- FORMULARIO MODAL PARA REGISTRAR NUEVO EGRESO-->
<div class="modal fade" id="registra-egreso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Egreso</b></h4>
            </div>
            <form id="formularioReg" name="formularioReg" class="formulario" >
                <div class="modal-body">
                  <div class="row">
                      <div id="lista-modalTipoEgreso">  
                    <div class="col-sm-4 m-b">
                    <div class="form-group">
                        <fieldset>
                            <label for="user_name"><strong>Tipo de Egreso</strong></label>
                        <br />  
                        <select name="modalTipoEgreso" id="modalTipoEgreso" class="input-sm form-control input-s-sm inline">
                            <?php
                            $consulta2 = "SELECT * FROM TipoEgresoCaja ";
                            $comando2 = Database::getInstance()->getDb()->prepare($consulta2);
                            $comando2->execute();
                            while ($fila = $comando2->fetch()) {
                                    echo "<option value='$fila[idTipoEgresoCaja]'/>$fila[nombreTipoEgresoCaja]";
                                }
                            ?>
                            </select>
                        </fieldset> 
                    </div>
                    </div>
                      </div>      
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Nro. Factura</strong></label>
                        <br />
                        <input type="text" name="nFac" id="nFac" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Nro. Nota Venta</strong></label>
                        <br />
                        <input type="text" name="nNota" id="nNota" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>    
                </div>
                    
                    <div class="row">
                    <div class="col-sm-8 m-b">                                
                    <div class="form-group">
                        <input name="idEgresoCaja" id="idEgresoCaja" type="text" hidden/>   
                        <fieldset>
                             
                        <label for="user_name"><strong>Glosa</strong></label>
                        <br />
                        <textarea name="modalGlosa" id="modalGlosa" class="form-control m-b" maxlength='200'></textarea>
                        
                        </fieldset>
                    </div>
                    </div>
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Tipo de Cambio</strong></label>
                        <br />
                        <input type="text" name="modalCambio" id="modalCambio" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div> 
                    
                </div>
                <div class="row">

                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Monto Total</strong></label>
                        <br />
                        <input type="text" name="modalMonto" id="modalMonto" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>   
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Efectivo Bs.</strong></label>
                        <br />
                        <input type="text" name="modalEfectivoBs" id="modalEfectivoBs" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Efectivo $us.</strong></label>
                        <br />
                        <input type="text" name="modalEfectivoSus" id="modalEfectivoSus" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>    
                </div>  
                   
                <div class="modal-footer">
                    <div class="form-inline" >   
                        <center>
                                        <div class="form-group">

                                            <button  id="reg"class="btn btn-primary" onclick="return GuardarRegistro();" data-dismiss="modal" aria-hidden="true">Guardar</button>
                                        </div>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="form-group">
                                            <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                                        </div>
                        </center>
                                    </div>
                </div>

            
              
        </div>
            </form>        
    </div>
</div>  
</div>
    <!--modal de tipo egreso-->
<div class="modal fade" id="registra-Tipoegreso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Tipo Egreso</b></h4>
            </div>

            <!--              aqui agregamos los registros-->

            <form id="formularioRegTipo" name="formularioRegTipo" class="formulario" >
                <div class="modal-body">
                 
                <div class="row">
                    
                    <div class="col-sm-12 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Nombre de Tipo Egreso</strong></label>
                        <br />
                        <input type="text" name="modalNombre" id="modalNombre" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>
                   
                </div>  
                    
                <div class="modal-footer">
                    <div class="form-inline" >   
                        <center>
                                        <div class="form-group">

                                            <button id="regtipo" class="btn btn-primary" onclick="return registrarTipoEgreso();" data-dismiss="modal" aria-hidden="true">Guardar</button>
                                        </div>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="form-group">
                                            <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                                        </div>
                        </center>
                    </div>
                </div>
                 <!------------------------------- tabla de tipos de egresos-->
             <div class="widget-body" >
                <div id="tip_egr"name="tip_egr">
                     <h2><strong> LISTA DE TIPOS DE EGRESOS </strong></h2>

                                <table class="table table-striped" >
                                    <?php  

                                    $query_tipo_comp = "SELECT * FROM TipoEgresoCaja WHERE estado = 1";
                                                             
                                    $cmd_tip_c = Database::getInstance()->getDb()->prepare($query_tipo_comp);
                                    $cmd_tip_c->execute();
                                    $row_count_dv = $cmd_tip_c->rowCount();
                                     if($row_count_dv > 0): ?>
                                                <tr>
                                                    <th><strong>Codigo</strong></th>
                                                    <th><strong>Nombre</strong></th>
                                                    <th><strong></strong></th>
                                                </tr>    
                                    <?php       while($tipo_com = $cmd_tip_c->fetch()): ?>
                                    <?php         $id = $tipo_com['idTipoEgresoCaja'];
                                        ?>
                                                    <tr>
                                                        <td><?=  $id ?></td>

                                                        <td><?= $tipo_com['nombreTipoEgresoCaja'] ;?></td>
                                                        
                                                        <td>
                                                         <a href="=<?= $id ?>" class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                                                         <a href="javascript: eliminarTipoEgreso('<?= $id ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                                                        </td>
                                                    </tr>
                                    <?php       endwhile; ?>
                                <?php       else: ?>
                                                
                                                    <div class="alert alert-info" role="alert">
                                                        <center>
                                                            <h3>No se encontraron resultados.</h3>
                                                        </center>
                                                    </div>
                                                
                                <?php       endif; ?>
                                </table>
                </div>
                               
            </div><br>
            
              
        </div>
            </form>        
    </div>
</div>  
</div>

<!-- End Formulario Modal-->
<?php require_once '../header_footer/footer.php'; ?>


