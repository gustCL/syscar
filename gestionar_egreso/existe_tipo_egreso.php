<?php

    require_once '../datos/Database.php';

    $descripcion = $_POST['nombre'];

    $query = "SELECT * FROM TipoEgresoCaja WHERE nombreTipoEgresoCaja = :descripcion AND estado = 1";
    $cmd = Database::getInstance()->getDb()->prepare($query);
    $cmd->bindParam(':descripcion', $descripcion);
    $cmd->execute();
    if ($cmd->rowCount() > 0) {
        echo 'true';
        exit();
    } else {
        echo 'false';
        exit();
    }