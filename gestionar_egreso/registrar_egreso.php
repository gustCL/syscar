<?php
require_once '../datos/Database.php';
ini_set("date.timezone", "America/La_Paz");
$fecha = date("d-m-Y");
$hora = date("H:i:s");
session_start();
$idEgresoCaja=$_POST['idEgresoCaja'];

$idInicio=$_SESSION['idSesion'];
$idTipoEgreso= $_POST['modalTipoEgreso'];
$nroF=$_POST['nFac'];
$nrN=$_POST['nNota'];
$glosa=$_POST['modalGlosa'];
$montoTotal=$_POST['modalMonto'];
$EfectivoBs=$_POST['modalEfectivoBs'];
$EfectivoSus=$_POST['modalEfectivoSus'];
$cambio=$_POST['modalCambio'];
if ($idEgresoCaja=='si'){
$consulta ="INSERT INTO EgresoCaja(idEgresoCaja, idInicio, idTipoEgresoCaja, nroFactura, nroNotaIngreso, glosa, montoTotal, idTipoCambio, tipoCambio, efectivoBs, efectivoSus, fechaRegistro, horaRegistro, estado) VALUES ('',?,?,?,?,?,?,' ',?,?,?,?,?,?)";
$comando =Database::getInstance()->getDb()->prepare($consulta);
$comando->execute([$idInicio,$idTipoEgreso,$nroF,$nrN,$glosa,$montoTotal,$cambio,$EfectivoBs,$EfectivoSus,$fecha,$hora,1]);
 
}else{
$consulta ="UPDATE EgresoCaja SET idTipoEgresoCaja=?,nroFactura=?,nroNotaIngreso=?,glosa=?,montoTotal=?,tipoCambio=?,efectivoBs=?,efectivoSus=? WHERE idEgresoCaja= '".$idEgresoCaja."'";
$comando =Database::getInstance()->getDb()->prepare($consulta);
$comando->execute([$idTipoEgreso,$nroF,$nrN,$glosa,$montoTotal,$cambio,$EfectivoBs,$EfectivoSus]);
       
}
?>