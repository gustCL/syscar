<?php
require_once ('head.php');
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                LISTA FACTURA DE COMPRAS
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>

    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <form>
                        <script>
                            var conta = 1;
                        </script>
                        <table>
                            <thead>
                                <tr>
                                    <th scope="col">Pista</th>
                                    <th scope="col">Album</th>
                              <!--      <th scope="col">fecha</th>-->
                                    <th scope="col">Artista</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td><input id="pista1" name="pista1" /></td>
                            <script>
                                
                                $('#pista' + conta).autocomplete({
                                    source: function (request, response) {
                                        conta = conta + 1;
                                        $.ajax({
                                            url: 'autocompletado.php',
                                            dataType: "json",
                                            data: {
                                                name_startsWith: request.term,
                                                type: 'Nombre'
                                            },
                                            success: function (data) {
                                                response($.map(data, function (item) {
                                                    return {
                                                        label: item,
                                                        value: item

                                                    };
                                                }));
                                                //onselect()
                                                
                                            }

                                        });
                                    }

                                });
                            </script> 
                            <td><input id="album1" name="album1" /></td>
                            <!--<td><input class="form-control date-picker" id="fecha1" name="fecha1" type="text" data-date-format="yyyy-mm-dd" /></td>-->
                            <td><select id="artista1" name="artista1">
                                    <option value="">Seleccionar</option>
                                    <option value="1">Artista 1</option>
                                    <option value="2">Artista 2</option>
                                    <option value="3">Artista 3</option>        
                                </select>
                            </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                    <button>Add</button>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once ('../header_footer/footer.php');
?> 

