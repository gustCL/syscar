$(document).ready(function() {
    fn_buscar6();
});

$(function() {
    //BUSCA LABORATORIO MEDIANTE EL TEXTBOX
    $('#bs-laboratorio').on('keyup', function() {
        fn_buscar6();
    });

    $('#cerrar').on('click', function() {
        $('#Editar').hide();
    });

    //MUESTRA MODAL PARA REGISTRAR NUEVO LABORATORIO	
    $('#nuevo-laboratorio').on('click', function() {
        $('#formularioEdit')[0].reset();
        $('#editar-laboratorio').modal({
            show: true,
            backdrop: 'static'
        });
    });

});


function fn_buscar6() {
    var str = $("#formLabPrincipal").serialize();
    $.ajax({
        url: 'tabla_lab.php',
        type: 'post',
        data: str+'&bs-laboratorio='+$('#bs-laboratorio').val(),
        success: function(data) {
            $("#agrega-registros").html(data);

        }
    });
}


function fn_paginar(var_div, url) {
    var div = $("#" + var_div);
    $(div).load(url);
}

function GuardarRegistro() { //GUARDA NUEVO LABORATORIO 
    var str = $("#formularioEdit").serialize();
    $.ajax({
        url: 'laboratorio_nuevo.php',
        data: str+'&bs-laboratorio='+$('#bs-laboratorio').val(),
        type: 'post',
        success: function(registro) {
                $('#agrega-registros').html(registro);
                swal("Laboratorio Registrado!", "El Laboratorio ha sido Registrado Correctamente", "success");
                return false;
        }

    });
    return false;
}

function editarLaboratorio(id) { //ESTA FUNCION ES PARA MOSTRAR LOS DATOS EN EL formulario "formEditarLab"
    $('#formEditarLab')[0].reset();
    var url = 'laboratorio_verEdit.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(valores) {
            var datos = eval(valores);

            $('#id-lab').val(id);
            $('#codLab').val(datos[0]);
            $('#labE').val(datos[1]);
            $('#paisE').val(datos[2]);
            $('#Editar').show();
            return false;
        }
    });
    return false;
}

function editarRegistro() { //MODIFICA LOS DATOS DEL LABORATORIO
    var str = $("#formEditarLab").serialize();
    $.ajax({
        url: 'laboratorio_edita.php',
        data: str+'&bs-laboratorio='+$('#bs-laboratorio').val(),
        type: 'post',
        success: function(registro) {
            $('#agrega-registros').html(registro);
            $('#Editar').hide();
            swal("Laboratorio Editado!", "El Laboratorio ha sido Editado Correctamente", "success");
            return false;

        }

    });
    return false;
}

function eliminarLaboratorioMMM(id, name) { //ELIMINA O DA DE BAJA A UN LABORATORIO
    var url = 'laboratorio_eliminar.php';
    var pregunta = confirm('¿Esta seguro de eliminar el Laboratorio:' + name + '?');
    if (pregunta == true) {
        $.ajax({
            type: 'POST',
            url: url,
            data: 'id=' + id,
            success: function(registro) {
                alert("LABORATORIO ELIMINADO");
                $('#agrega-registros').html(registro);
                return false;
            }
        });
        return false;
    } else {
        return false;
    }
};


function eliminarLaboratorio(id) {
    swal({
        title: "Eliminar Laboratorio",
        text: "Está a punto de eliminar los registros de este Laboratorio. ¿Desea continuar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, eliminar",
        cancelButtonText: "No, cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function(isConfirm) {
            if (isConfirm) {
                var url = 'laboratorio_eliminar.php';
                var str = $("#formLabPrincipal").serialize();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: str+'&bs-laboratorio='+$('#bs-laboratorio').val()+'&id='+id,
                    success: function(registro) {
                        $('#agrega-registros').html(registro);
                        swal("Laboratorio eliminado!", "El Laboratorio ha sido eliminado", "success");
                        return false;
                    }
                });
            }
           
        }
    );
}
;