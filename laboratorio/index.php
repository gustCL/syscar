<?php
require_once ('head.php');
        ?>  

<!-- Page Content -->
<div class="page-content">
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>LISTA LABORATORIO</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="widget-body ">
                         <div class="widget-body" id="Editar" hidden>
                        <form id="formEditarLab" name="formEditarLab" class="form-horizontal" action="javascript:editarRegistro()" method="POST">
                            <br>
                            <br>
                            <div class="form-group">   
                                <label class="col-lg-6 control-label">Nombre de Laboratorio</label>
                                <div class="col-lg-3">

                                    <input type ="text" class="form-control" id="labE" name="labE" placeholder="Nombre de laboratorio" title="INGRESE NOMBRE DE LABORATORIO" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <input id="id-lab" name="id-lab"type="text" hidden><!--OCULTO EL ID DEL LABORATORIO-->  
                                <label class="col-lg-6 control-label">Cod. Laboratorio </label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" id="codLab" name="codLab" placeholder="Cod de Laboratorio" title="INGRESE CODIGO" required/>
                                </div>
                            </div> 
                            <div class="form-group">           
                                <label class="col-lg-6 control-label">Nacionalidad</label>
                                <div class="col-lg-3">
                                    <?php
                                    $consultaP = "Select * from Pais";
                                    require_once("../datos/Database.php");
                                    $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
                                    $comandoP->execute();
                                    ?>
                                    <select  id='paisE' name="paisE" style='width:100%;'>
                                        <?php
                                        if ($comandoP->rowCount() > 0) {
                                        while ($Pais = $comandoP->fetch()) {
                                        echo "<option value='$Pais[nombrePais]'/>$Pais[nombrePais]";
                                        }
                                                } else {
                                                echo "<option value='Central'/>Central";
                                                }
                                        ?>
                                            </select>   

                                        </div>
                                    </div>
                                    <div class="form-group" id="mensaje">           

                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <input type="submit"  class="btn btn-primary" id="btn_editar" value="Editar" hidden/>
                                            &nbsp;&nbsp;&nbsp;
                                            <input type="button" class="btn btn-danger" id="cerrar" value="Cancelar"/>

                                        </center>
                                    </div>

                                </form>  
                            </div>
                        <form   class="form-horizontal" id="formLabPrincipal">
                            <br/>
                            <div class="form-inline">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <div class="col-lg-3">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" name="bs-laboratorio" id="bs-laboratorio" size="50" placeholder="Ingrese Cod. laboratorio o nombre de laboratorio"  onKeypress="if (event.keyCode < 45 || event.keyCode > 57)
                                                        event.returnValue = true;"/>
                                            <i class="glyphicon glyphicon-search circular blue"></i>
                                        </span>    
                                    </div>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="input-group">
                                    <a  type="button" id="nuevo-laboratorio"  class="btn btn-warning shiny"  ><i class="fa fa-plus"></i>Nuevo laboratorio</a>
                                </div>
                            </div>
                            <br/>
                            
                            <div class="widget-body " id="agrega-registros" name="agrega-registros">

                            </div>
                        </form>
                    </div>
                    <br>
                   

                            <br/>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- FORMULARIO MODAL PARA EDITAR DATOS DE PROVEEDOR"-->
        <div class="modal fade" id="editar-laboratorio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"><b>Laboratorio</b></h4>
                    </div>

                    <!--              aqui agregamos los registros-->

                    <form id="formularioEdit" name="formularioEdit" class="formulario" >
                        <div class="modal-body">

                            <center>
                                <div class="row" >
                                    <div class="col-lg-12 col-sm-12 col-xs-12" >

                                        <div class="form-inline" >

                                            <div class="form-group">
                                                Nombre de Laboratorio
                                                &nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control"  id="labE" name="labE" placeholder="Nombre de laboratorio" />
                                            </div>                    
                                        </div>   
                                        <br />             
                                        <div class="form-inline" >        
                                            <div class="form-group">
                                                Cod. Laboratorio 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <input id="id-lab" name="id-lab"type="text" hidden><!--OCULTO EL ID DEL PROVEDOR-->

                                            <div class="form-group">
                                                <input id="codLab" name="codLab"class="form-control" type="text" placeholder="Codigo de Laboratorio">
                                            </div>

                                        </div> 

                                        <br/>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                Nacionalidad
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                        <?php
                                        $consultaP = "Select * from Pais";
                                        require_once("../datos/Database.php");
                                        $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
                                        $comandoP->execute();
                                        ?>

                                        <select  id='paisE' name="paisE" style='width:100%;'>
                                            <?php
                                            if ($comandoP->rowCount() > 0) {
                                            while ($Pais = $comandoP->fetch()) {
                                            echo "<option value='$Pais[nombrePais]'/>$Pais[nombrePais]";
                                            }
                                            } else {
                                            echo "<option />Central";
                                            }
                                            ?>
                                        </select>


                                    </div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div> 

                            </div>
                    </center>
                </div>

                <div class="modal-footer">
                    <div class="form-inline" >   
                        <center>
                            <div class="form-group">

                                <button  id="reg"class="btn btn-primary" onclick="return GuardarRegistro();" data-dismiss="modal" aria-hidden="true">Guardar</button>
                            </div>&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="form-group">
                                <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                            </div>
                        </center>
                    </div>
                </div>

            </form>

        </div>

    </div>
</div>

<!-- End Formulario Modal-->
<?php require_once '../header_footer/footer.php'; ?>


