<?php
include "../datos/conexion.php";
include "../extras/PHPPaging.lib/PHPPaging.lib.php";
//OBTENEMOS PROVEEDORES ACTIVOS
$query_laboratorio = "SELECT * FROM Pais pa, Laboratorio la  WHERE la.idPais = pa.idPais AND estado = '1'";
if($_REQUEST['bs-laboratorio'] != ''){
    $dato = $_REQUEST['bs-laboratorio'];
    $query_laboratorio .="AND (la.nombreLab LIKE '$dato%' OR la.codLaboratorio LIKE '$dato%')";
}
$query_laboratorio.="ORDER BY la.idLaboratorio DESC";

$pagina = new PHPPaging;
?>

<script src="../extras/sortable/js/sortable.js"></script>
<link rel="stylesheet" href="../extras/data_table/media/css/dataTables.bootstrap.min.css">
<script src="../extras/js/jquery/jquery-2.2.0.min.js"></script>
<table class="sortable-theme-bootstrap" data-sortable>
    <thead>
        <!--Fila de titulos de columnas-->
        <tr>
            <th><strong> Cod. laboratorio</strong></th>
            <th><strong>Nombre laboratorio</strong></th>
            <th><strong>Nacionalidad</strong></th>
            <th></th>
        </tr>
    </thead>
    <?php
    $pagina->agregarConsulta($query_laboratorio);

    $pagina->modo('desarrollo');
    $pagina->verPost(true);
    $pagina->porPagina(10);
    $pagina->paginasAntes(5);
    $pagina->paginasDespues(5);
    $pagina->linkSeparador(" - "); //Significa que no habrá separacion
    $pagina->div('agrega-registros');
    $pagina->linkSeparadorEspecial('...');   // Separador especial s
    $pagina->ejecutar();

    while ($res = $pagina->fetchResultado()) {
        $idP = $res['idLaboratorio'];
        $cod_lab = $res["codLaboratorio"];
        $nomb_lab = $res["nombreLab"];
        $nomb_pais = $res["nombrePais"];
        ?>  

        <tbody>                  
            <tr>
                <td>
    <?= $cod_lab ?>
                </td>
                <td>
    <?= $nomb_lab ?>
                </td>

                <td>
    <?= $nomb_pais ?>
                </td>

                <td>
                    <a href="javascript:editarLaboratorio('<?= $idP ?>');" class="btn btn-default btn-xs purple" ><i class="fa fa-edit"></i> Editar</a>
                    <a href="javascript:eliminarLaboratorio('<?= $idP ?>',' <?= $nomb_lab ?>');" id="eliminar"class="btn btn-default btn-xs black" id="" ><i class="fa fa-trash-o"></i> Eliminar</a>

                </td>

            </tr>

        </tbody>
    <?php
}//terminacion del while
?>
    <tfoot>
        <tr>    
            <td colspan="7">Numero de pagina: <?php echo $pagina->fetchNavegacion(); ?></td>    
        </tr>
    </tfoot>
</table>
