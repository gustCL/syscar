<?php
require_once ('head.php');  
?>
              <div class="page-content">
                <!-- subtTitulo -->
                <div class="page-header position-relative">
                    <div class="header-title">
                        <h1>
                            <strong>NUEVO PROVEEDOR</strong>
                        </h1>
                    </div>
                    <!--Header Buttons-->
                    <div class="header-buttons">
                        <a class="sidebar-toggler" href="#">
                            <i class="fa fa-arrows-h"></i>
                        </a>
                        <a class="refresh" id="refresh-toggler" href="#">
                            <i class="glyphicon glyphicon-refresh"></i>
                        </a>
                        <a class="fullscreen" id="fullscreen-toggler" href="#">
                            <i class="glyphicon glyphicon-fullscreen"></i>
                        </a>
                    </div>
                                    
                      
                    <!--Header Buttons End-->
                </div>
                <!-- /Page Header -->
                <!-- Page Body -->
              <div class="page-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                   
                                    <div class="widget">
                                    <div class="widget-body">
                                        
                                        <form id="registrationForm" method="post" action="proveedor_validar.php" class="form-horizontal">
                                            <br/>
                                                <div class="form-group">
                                                            <label class="col-lg-2 control-label">NIT</label>
                                                   <div class="col-lg-3">
                                                        <input type="text" class="form-control" name="nit" id="nit" placeholder="Ingrese nro. de nit" />
                                                    </div>
                                                            
                                                    <label class="col-lg-2 control-label">Empresa</label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"  name="empresaN" id="empresaN" placeholder="Ingrese nombre de la Empresa"/>
                                                    </div>
                                                </div>
                                           
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Nombre Contacto</label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control" name="nombreContacto" id="nombreContacto" placeholder="Ingrese el nombre del Vendedor"/>
                                                    </div>
                                                    
                                                    <label class="col-lg-2 control-label">Nacionalidad </label>
                                                    <div class="col-lg-3">
                                                         <?php
                                                            require_once("../datos/Database.php");
                                                            $datos =Database::getInstance()->getDb()->prepare("select * from Pais");
                                                            $datos->execute();
                                                            ?>
                                                        <select name="pais" id="pais" style="width: 228px;">
                                                                        <option value="0">SELECCIONE PAIS</option>
                                                                        <?php while($row= $datos->fetch()) { ?>
                                                                        <option value="<?=$row['idPais']?>"><?=$row['nombrePais']?></option>
                                                                   <?php    } ?>
                                                                    </select>
                                                      </div>
                                                </div>
                                                       
                                                                                                   
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Direccion</label>
                                                    <div class="col-lg-3">
                                                        <input class="form-control" name="direccion" id="direccion"  type="text" placeholder="Ingrese direccion domiciliar" />
                                                    </div>
                                                    <label class="col-lg-2 control-label">Correo</label>
                                                    <div class="col-lg-3">
                                                        <input class="form-control" name="email" id="email" type="text"  placeholder="Ingrese correo electronico"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Telefono</label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control" name="telefono" id="telefono"  placeholder="Ingrese nro de telefono"/>
                                                    </div>
                                                    <label class="col-lg-2 control-label">Celular</label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control" name="celular" id="celular" placeholder="Ingrese nro de celular"/>
                                                    </div>
                                                </div>
                                               
                                      </br>
                                     
                                      <center>
                                                <div class="form-group">
                                                        <input class="btn btn-primary" type="submit" value="Guardar" />
                                                        &nbsp;&nbsp; &nbsp;
                                                        <a class="btn btn-danger" type="button"  href="../proveedor" >Cancelar</a>
                                                </div>
                                      </center>
                                            </form>
                                            </div> 
                                        </div>
                                    </div>                             
                                </div> 
                                </div>
                            </div>
                     
                        </div>
                    </div>
                
              
                <!-- /Page Body -->
            
            <!-- /Page Content -->
        
        <!-- /Page Container -->
        <!-- Main Container -->
 
 <?php
require_once ('../header_footer/footer.php');
?>  

