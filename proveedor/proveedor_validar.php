<?php
require_once ("../datos/Database.php");
$nit = $_POST['nit'];
$empresa=$_POST['empresaN'];
$contacto=$_POST['nombreContacto'];
$idPais=$_POST['pais'];
$direccion=$_POST['direccion'];
$email=$_POST['email'];
$telefono=$_POST['telefono'];
$celular = $_POST['celular'];
ini_set("date.timezone", "America/La_Paz");
$fecha = date('Y-m-d');
 $hora = date("H:i:s");

 $consultaP = "INSERT INTO Proveedor(idProveedor, nombreProve, nit, idPais, nombreContacto, fechaRegistro, horaRegistro, estado) "
             . "VALUES ('','$empresa','$nit','$idPais','$contacto','$fecha','$hora', 1)";
 $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
 $comandoP->execute();
 
   ////// este codigo saca el id  proveedor registrado  ////////
$valor=  Database::getInstance()->getDb()->prepare("Select Max(idProveedor) As idProveedor from Proveedor");
$valor->execute();
$count = $valor->fetch(PDO::FETCH_ASSOC);
$idProveedor = $count['idProveedor'];

if ($telefono !=""){
 $consultaT = "INSERT INTO Telefono(idTelefono, numeroTelefono, tipoTelefono, idProveedor) "
           . "VALUES ('','$telefono','1','$idProveedor')";
 $comandoT = Database::getInstance()->getDb()->prepare($consultaT);
 $comandoT->execute();   
}
if ($celular !=""){
 $consultaC = "INSERT INTO Telefono(idTelefono, numeroTelefono, tipoTelefono, idProveedor) "
           . "VALUES ('','$celular','2','$idProveedor')";
 $comandoC = Database::getInstance()->getDb()->prepare($consultaC);
 $comandoC->execute();   
}

if ($direccion !=""){
 $consultaD = "INSERT INTO Direccion(idDireccion, direccion, idProveedor) "
            . "VALUES ('','$direccion','$idProveedor')";
 $comandoD = Database::getInstance()->getDb()->prepare($consultaD);
 $comandoD->execute();   
}

if ($email !=""){
 $consultaE = "INSERT INTO CorreoEmail (idCorreo, correo, idProveedor) "
            . "VALUES ('','$email','$idProveedor')";
 $comandoE = Database::getInstance()->getDb()->prepare($consultaE);
 $comandoE->execute();   
}

 
 header('Location: ../proveedor/registrar_producto.php')
?>
