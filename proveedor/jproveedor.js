$(function(){
    
	//BUSCA PROVEEDOR MEDIANTE EL TEXTBOX
	$('#bs-prod').on('keyup',function(){
		var dato = $('#bs-prod').val();
		var url = 'proveedor_datos.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'dato='+dato,
		success: function(datos){
			$('#agrega-registros').html(datos);
		}
	});
	return false;
	});

});

//REALIZA BUSQUEDA DE PROVEEDOR CON LA FECHA DE REGISTRO
function buscar_Proveedor(){
     var str = $("#formProveedor").serialize();
  
    $.ajax({
            url: 'proveedor_datosFecha.php',
            type: 'get',
            data: str,
            success: function(data){
                $("#agrega-registros").html(data);
                 
            }
    });
}

function verProveedor(id){ //MUESTRA LOS DATOS DEL PROVEEDOR EN UN MODAL
	$('#formulario')[0].reset();
	var url = 'proveedor_ver.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'id='+id,
		success: function(valores){
				var datos = eval(valores);
				$('#nit').val(datos[0]);
				$('#empresa').val(datos[1]);
				$('#contacto').val(datos[2]);
				$('#pais').val(datos[3]);
                                $('#tel').html(datos[4]);
                                $('#cel').html(datos[5]); 
                                $('#producto').html(datos[6]);
				$('#ver-proveedor').modal({
					show:true,
					backdrop:'static'
				});
			return false;
		}
	});
	return false;
}

function eliminarProveedor(id){
	var url = 'proveedor_eliminar.php';
	var pregunta = confirm('¿Esta seguro de eliminar este Proveedor?');
	if(pregunta==true){
		$.ajax({
		type:'POST',
		url:url,
		data:'id='+id,
		success: function(registro){
			$('#agrega-registros').html(registro);
			return false;
		}
	});
	return false;
	}else{
		return false;
	}
}




function eliminarDirec(){
	var url = 'proveedor_eliminarDireccion.php';
        var frm = $("#formularioEdit").serialize();
	var pregunta = confirm('¿Esta seguro de eliminar esta direccion?');
	if(pregunta==true){
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-direccion').html(actualizo);
                        
                        
			return false;
		}
	});
	return false;
	}else{
		return false;
	}
}

function eliminarEmail(){
	var url = 'proveedor_eliminarEmail.php';
        var frm = $("#formularioEdit").serialize();
	var pregunta = confirm('¿Esta seguro de eliminar este correo?');
	if(pregunta==true){
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-correo').html(actualizo);
                        
                        
			return false;
		}
	});
	return false;
	}else{
		return false;
	}
}


function eliminarTelf(){
	var url = 'proveedor_eliminarTelf.php';
        var frm = $("#formularioEdit").serialize();
	var pregunta = confirm('¿Esta seguro de eliminar este Telefono?');
	if(pregunta==true){
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
                        $('#div-telefono').html(actualizo);
			return false;
		}
	});
	return false;
	}else{
		return false;
	}
}

function eliminarCel(){
	var url = 'proveedor_eliminarCel.php';
        var frm = $("#formularioEdit").serialize();
	var pregunta = confirm('¿Esta seguro de eliminar este numero?');
	if(pregunta==true){
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-celular').html(actualizo);
			return false;
		}
	});
	return false;
	}else{
		return false;
	}
}
//Para el modal de direccion
function DireccionModal(){
    $('#formularioDireccion')[0].reset();
     $("#formularioEdit").serialize();
    $('#guardarDir').show();
    $('#actualizarDir').hide();
    $('#provId').val($('#id-prov').val());
    $('#Mdireccion').val(" ");
		$('#editar-direccion').modal({
			show:true,
			backdrop:'static'
		});
}

function nuevaDireccion(){
	var url = 'proveedor_nuevaDirec.php';
        var frm = $("#formularioDireccion").serialize();
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-direccion').html(actualizo);
			return false;
		}
	});
	return false;
	
}


function DireccionModalEdit(){
    $('#formularioDireccion')[0].reset();
    $("#formularioEdit").serialize();
    $('#actualizarDir').show();
    $('#guardarDir').hide();
    if($('#direccion').val()!=""){    
    $('#provId').val($('#id-prov').val());    
    $('#Mdireccion').val($('#direccion').val());
    $('#idDireccion').val($('#direccion').val());
		$('#editar-direccion').modal({
			show:true,
			backdrop:'static'
		});
    }
}

function actualizarDireccion(){
	var url = 'proveedor_actualizarDirec.php';
        var frm = $("#formularioDireccion").serialize();
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-direccion').html(actualizo);
			return false;
		}
	});
	return false;
	
}

//para el modal de correo
function CorreoModal(){
    $('#formularioCorreo')[0].reset();
     $("#formularioEdit").serialize();
    $('#guardarCorreo').show();
    $('#actualizarCorreo').hide();
    $('#provIdC').val($('#id-prov').val());
    $('#Mcorreo').val(" ");
		$('#editar-correo').modal({
			show:true,
			backdrop:'static'
		});
}

function nuevoCorreo(){
	var url = 'proveedor_nuevoCorreo.php';
        var frm = $("#formularioCorreo").serialize();
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-correo').html(actualizo);
			return false;
		}
	});
	return false;
	
}

function CorreoModalEdit(){
    $('#formularioCorreo')[0].reset();
    $("#formularioEdit").serialize();
    $('#actualizarCorreo').show();
    $('#guardarCorreo').hide();
    if($('#correoE').val()!=""){    
    $('#provIdC').val($('#id-prov').val());    
    $('#Mcorreo').val($('#correoE').val());
    $('#idcorreo').val($('#correoE').val());
		$('#editar-correo').modal({
			show:true,
			backdrop:'static'
		});
    }
}

function actualizarCorreo(){
	var url = 'proveedor_actualizarCorreo.php';
        var frm = $("#formularioCorreo").serialize();
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-correo').html(actualizo);
			return false;
		}
	});
	return false;
	
}
//para el modal de telefono
function telefonoModal(){
    $('#formularioTelefono')[0].reset();
     $("#formularioEdit").serialize();
    $('#guardarTelefono').show();
    $('#actualizarTelefono').hide();
    $('#provIdTelf').val($('#id-prov').val());
    $('#Mtelefono').val(" ");
		$('#editar-telefono').modal({
			show:true,
			backdrop:'static'
		});
}

function nuevoTelefono(){
	var url = 'proveedor_nuevoTelefono.php';
        var frm = $("#formularioTelefono").serialize();
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-telefono').html(actualizo);
			return false;
		}
	});
	return false;
	
}

function telefonoModalEdit(){
    $('#formularioTelefono')[0].reset();
    $("#formularioEdit").serialize();
    $('#actualizarTelefono').show();
    $('#guardarTelefono').hide();
    if($('#telfE').val()!=""){    
    $('#provIdTelf').val($('#id-prov').val());    
    $('#Mtelefono').val($('#telfE').val());
    $('#idTelf').val($('#telfE').val());
		$('#editar-telefono').modal({
			show:true,
			backdrop:'static'
		});
    }
}

function actualizarTelefono(){
	var url = 'proveedor_actualizarTelefono.php';
        var frm = $("#formularioTelefono").serialize();
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-telefono').html(actualizo);
			return false;
		}
	});
	return false;
	
}

//para el modal de celular
function celularModal(){
    $('#formularioCelular')[0].reset();
     $("#formularioEdit").serialize();
    $('#guardarCelular').show();
    $('#actualizarCelular').hide();
    $('#provIdCel').val($('#id-prov').val());
    $('#Mcelular').val(" ");
//    $('#Mcelular').focus();
		$('#editar-celular').modal({
			show:true,
			backdrop:'static'
		});
}

function nuevoCelular(){
	var url = 'proveedor_nuevoCelular.php';
        var frm = $("#formularioCelular").serialize();
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-celular').html(actualizo);
			return false;
		}
	});
	return false;
	
}

function celularModalEdit(){
    $('#formularioCelular')[0].reset();
    $("#formularioEdit").serialize();
    $('#actualizarCelular').show();
    $('#guardarCelular').hide();
    if($('#celularE').val()!=""){    
    $('#provIdCel').val($('#id-prov').val());    
    $('#Mcelular').val($('#celularE').val());
    $('#idCel').val($('#celularE').val());
		$('#editar-celular').modal({
			show:true,
			backdrop:'static'
		});
    }
}

function actualizarCelular(){
	var url = 'proveedor_actualizarCelular.php';
        var frm = $("#formularioCelular").serialize();
		$.ajax({
		type:'POST',
		url:url,
		data:frm,
		success: function(actualizo){
			$('#div-celular').html(actualizo);
			return false;
		}
	});
	return false;
	
}

function validarNumeroProveedor(event) {
    if (event.keyCode == 46) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }
};




