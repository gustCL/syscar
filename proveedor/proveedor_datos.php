
<table class="table table-striped ">
                                    <thead>
                                        <!--Fila de titulos de columnas-->
                                        <tr>
                                            <th class="hidden-xs">
                                                Id.
                                            </th>
                                            <th class="hidden-xs">
                                                 NIT
                                            </th>
                                            <th class="hidden-xs">
                                                 Nombre Contacto
                                            </th>
                                            <th class="hidden-xs">
                                                 Empresa
                                            </th>
                                            <th class="hidden-xs">
                                                 Pais
                                            </th>
                                            
                                            <th>
                                               
                                            </th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        require_once '../datos/Database.php';
                                        $dato = $_POST['dato'];
                                        //$consulta = "select *from proveedor  where  nombreContacto LIKE '%$dato%' OR nombre LIKE '%$dato%' OR nit LIKE '%$dato%'"; //Consulta para proveedores     
                                        $consulta = "select * from Pais pa, Proveedor pro where (pro.idPais = pa.idPais) AND estado = '1' AND (pro.nombreContacto LIKE '%$dato%' OR pro.nombreProve LIKE '%$dato%' OR pro.nit LIKE '%$dato%') ORDER BY pro.idProveedor DESC";

                                      try {

                                             $comando = Database::getInstance()->getDb()->prepare($consulta);
                                             $comando->execute(); 
                                           if($comando->rowCount() > 0){  
                                            while ($row = $comando->fetch()) {
                                                $idP=$row['idProveedor'];
                                                
                                        ?>
                                        
                                        <tr>
                                            <td>
                                                <?= $idP ?>
                                            </td>
                                            <td class="hidden-xs">
                                                <?= $row['nit'] ?>
                                            </td>
                                            <td class="hidden-xs">
                                                <?= $row['nombreContacto'] ?>
                                            </td>
                                            <td>
                                                <?= $row['nombreProve'] ?>
                                            </td>
                                            <td>
                                                <?= $row['nombrePais'] ?>
                                            </td>
                                                                                        
                                            <td>
                                                 <a href="javascript:verProveedor('<?= $idP?>');"   class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                                               <a href="editar_proveedor.php?id=<?= $idP ?>" class="btn btn-default btn-xs purple" ><i class="fa fa-edit"></i> Editar</a>
                                                <a href="javascript:eliminarProveedor('<?= $idP?>');" id="eliminar"class="btn btn-default btn-xs black" id="" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-trash-o"></i> Eliminar</a>
                                                
                                            </td>
                                            
                                        </tr>
                                        <?php

                                          }//terminacion del while
                                           } // terminacion de if
                                           else{?>  
	                                           <tr>
                                                   <td colspan="6">No se encontraron resultados</td>
                                                   </tr>
                                                 
                                             
                                         <?php
                                         }
                                        } catch (PDOException $e) {

                                                    echo 'Error: ' . $e;

                                          }
                                        ?>
                                    </tbody>
                                </table>

