<?php
require_once '../datos/Database.php';
$id = $_POST['id'];

//ELIMINAMOS EL PRODUCTO

$cProv = "UPDATE Proveedor SET estado = 0 WHERE idProveedor = '$id'";
$comPr = Database::getInstance()->getDb()->prepare($cProv);
$comPr->execute(); 

//CREAMOS NUESTRA VISTA Y LA DEVOLVEMOS AL AJAX
?>
<center><h3><strong>LISTA DE PROVEEDORES</strong></h3></center>
<br/>
<table class="table table-striped " methodo="POST" >
                                    <thead>
                                        <!--Fila de titulos de columnas-->
                                        <tr>
                                            <th class="hidden-xs">
                                                Id.
                                            </th>
                                            <th class="hidden-xs">
                                                 NIT
                                            </th>
                                            <th class="hidden-xs">
                                                 Nombre Contacto
                                            </th>
                                            <th class="hidden-xs">
                                                 Empresa
                                            </th>
                                            <th class="hidden-xs">
                                                 Nacionalidad
                                            </th> 
                                     
                                            <th>
                                               
                                            </th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                         //OBTENEMOS PROVEEDORES ACTIVOS
                                        $consulta = "select * from Pais pa, Proveedor pro where pro.idPais = pa.idPais and estado = '1' ORDER BY pro.idProveedor DESC";
                                        //require_once '../datos/Database.php';

                                         try {

                                             $comando = Database::getInstance()->getDb()->prepare($consulta);
                                             $comando->execute(); 
                                            while ($row = $comando->fetch()) {
                                                $idP=$row['idProveedor'];
                                                
                                        ?>
                                        
                                        <tr>
                                            <td id = "idProveedor" name="idProvedor" >
                                                <?= $idP ?>
                                            </td>
                                            <td class="hidden-xs">
                                                <?= $row['nit'] ?>
                                            </td>
                                            <td class="hidden-xs">
                                                <?= $row['nombreContacto'] ?>
                                            </td>
                                            <td>
                                                <?= $row['nombreProve'] ?>
                                            </td>
                                            
                                            <td>
                                                <?= $row['nombrePais'] ?>
                                            </td>
                                           
                                            <td>
                                                <a href="javascript:verProveedor('<?= $idP?>');"   class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                                                <a href="editar_proveedor.php?id=<?= $idP ?>" class="btn btn-default btn-xs purple" ><i class="fa fa-edit"></i> Editar</a>
                                                <a href="javascript:eliminarProveedor('<?= $idP?>');" id="eliminar"class="btn btn-default btn-xs black" id="" ><i class="fa fa-trash-o"></i> Eliminar</a>
                                                
                                            </td>
                                            
                                        </tr>
                                        <?php

                                          }//terminacion del while

                                        } catch (PDOException $e) {

                                                    echo 'Error: ' . $e;

                                          }
                                        ?>
                                    </tbody>
                                </table>