<?php
require_once ('head.php');
require_once '../datos/Database.php';

$id = $_GET['id'];
//OBTENEMOS LOS VALORES DEL PROVEEDOR//
$consulta = "select * from Proveedor po,Pais pa where po.idProveedor = '$id' and po.idPais = pa.idPais";
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$valores = $comando->fetch();

// PARA LAS DIRECCIONES.// 
$direc = "SELECT * FROM Direccion WHERE idProveedor = '$id'";
$comDir = Database::getInstance()->getDb()->prepare($direc);
$comDir->execute();
// PARA LOS CORREOS.//  
$email = "SELECT * FROM CorreoEmail WHERE idProveedor = '$id'";
$comEmail = Database::getInstance()->getDb()->prepare($email);
$comEmail->execute();
// PARA LOS NUMEROS DE TELEFONOS .// 
$con = "select * from Telefono where idProveedor = '$id' and tipoTelefono = 1";
$com = Database::getInstance()->getDb()->prepare($con);
$com->execute();
// PARA LOS NUMEROS DE CELULAR
$con1 = "select * from Telefono where idProveedor = '$id' and tipoTelefono = 2";
$com1 = Database::getInstance()->getDb()->prepare($con1);
$com1->execute();
?> 

<!-- Page Content -->
<div class="page-content">

    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>EDITAR PROVEEDOR</strong>

            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget-body" id="Editar">
                                <form id="formularioEdit" name="formularioEdit" method="POST" action="proveedor_edita.php" class="form-horizontal">
                                    <br/><br/>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Empresa</label>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control" name="empresa" value="<?= $valores['nombreProve'] ?>"  placeholder="Codigo"  />                                           


                                        </div>                        

                                        <label class="col-lg-2 control-label">Nombre Contacto</label>

                                        <div class="col-lg-2">
                                            <input type="text" class="form-control" name="nombreContacto" value="<?= $valores['nombreContacto'] ?>"   placeholder="Codigo Barra" />
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">NIT</label>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control" name="nit" value="<?= $valores['nit'] ?>"  placeholder="NIT"  />                                           
                                        </div>                        

                                        <label class="col-lg-2 control-label">Nacionalidad</label>

                                        <div class="col-lg-2">
                                            <?php
                                            require_once("../datos/Database.php");
                                            $pais = Database::getInstance()->getDb()->prepare("select * from Pais");
                                            $pais->execute();
                                            ?>
                                            <select name="pais" id="pais" style="width:100%;"> 

                                                <?php
                                                while ($row = $pais->fetch()) {
                                                    $selected = "";
                                                    if ($row['idPais'] == $valores['nombrePais']) {
                                                        $selected = 'selected';
                                                    }
                                                    ?>
                                                    <option value="<?= $row['nombrePais'] ?>" <?= $selected ?> ><?= $row['nombrePais'] ?></option>
                                                <?php } ?>
                                            </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>
                                        </div>

                                    </div> 
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Direccion</label>  
                                        <div class="col-lg-2" id="div-direccion">
                                            <select name="direccion" id="direccion"  data-bv-field="country" style="width: 160px;">  
                                                <?php
                                                if ($comDir->rowCount() > 0) {
                                                    while ($dir = $comDir->fetch()) {
                                                        ?>
                                                        <option value="<?= $dir['direccion'] ?>"><?= $dir['direccion'] ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <option /></option>
                                                <?php } ?>   
                                            </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>
                                        </div> 

                                        <div class="form-inline">
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Agregar Nueva Dirección"class='btn btn-default' name='nuevoDirec' onclick="DireccionModal();"><i class='glyphicon glyphicon-book'></i></a> 
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Editar Dirección Seleccionada" class='btn btn-default' name='editarDirec' onclick="DireccionModalEdit();"><i class='glyphicon glyphicon-pencil'></i></a> 
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button'title="Eliminar Dirección Seleccionada"class='btn btn-default' name='eliminarDirec' onclick='return eliminarDirec();'><i class='fa fa-trash-o'></i></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Correo</label>  
                                        <div class="col-lg-2" id="div-correo">
                                            <select name="correoE" id="correoE"  data-bv-field="country" style="width: 160px;">  
                                                <?php
                                                if ($comEmail->rowCount() > 0) {
                                                    while ($cor = $comEmail->fetch()) {
                                                        ?>
                                                        <option value="<?= $cor['correo'] ?>"><?= $cor['correo'] ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <option></option>
                                                <?php } ?>   
                                            </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>
                                        </div>  
                                        <div class="form-inline">
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Agregar Nuevo E-mail" class='btn btn-default' name='nuevoCorreo' onclick="CorreoModal();"><i class='glyphicon glyphicon-envelope'></i></a> 
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Editar E-mail Seleccionado" class='btn btn-default' name='editarCorreo' onclick="CorreoModalEdit();" ><i class='glyphicon glyphicon-pencil'></i></a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Eliminar E-mail Seleccionado" class='btn btn-default' name='eliminarEmail'  onclick='return eliminarEmail();'><i class='fa fa-trash-o'></i></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Nro. Telefono</label>  
                                        <div class="col-lg-2" id="div-telefono">
                                            <select name="telfE" id="telfE" data-bv-field="country" style="width: 160px;">  
                                                <?php
                                                if ($com->rowCount() > 0) {
                                                    while ($filaT = $com->fetch()) {
                                                        ?>
                                                        <option value="<?= $filaT['numeroTelefono'] ?>"><?= $filaT['numeroTelefono'] ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <option ></option>
<?php } ?>   
                                            </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>
                                        </div>  
                                        <div class="form-inline">
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Agregar  Nro. Telefónico" class='btn btn-default' name='nuevoTelf' onclick="telefonoModal();"><i class='glyphicon glyphicon-phone-alt'></i></a> 
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Editar Teléfono Seleccionado" class='btn btn-default' name='editarTelf' onclick="telefonoModalEdit();"><i class='glyphicon glyphicon-pencil'></i></a> 
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Eliminar Teléfono Seleccionado"class='btn btn-default'   onclick='return eliminarTelf();'><i class='fa fa-trash-o'></i></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Nro. Celular</label>  
                                        <div class="col-lg-2" id="div-celular">
                                            <select name="celularE" id="celularE"  data-bv-field="country" style="width: 160px;">  
                                                <?php
                                                if ($com1->rowCount() > 0) {
                                                    while ($filaC = $com1->fetch()) {
                                                        ?>
                                                        <option value="<?= $filaC['numeroTelefono'] ?>"><?= $filaC['numeroTelefono'] ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <option ></option>
<?php } ?>   
                                            </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>
                                        </div>  
                                        <div class="form-inline">
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Agregar Número De Celular" class='btn btn-default' name='nuevoCel'  onclick="celularModal();"><i class='glyphicon glyphicon-earphone'></i></a> 
                                            &nbsp;&nbsp;&nbsp;
                                            <a type='button' title="Editar  Celular Seleccionado" class='btn btn-default' name='editarCel' onclick="celularModalEdit();"><i class='glyphicon glyphicon-pencil'></i></a>                                  
                                            &nbsp;&nbsp;&nbsp;  
                                            <a type='button' title="Eliminar  Celular Seleccionado" class='btn btn-default'   onclick='return eliminarCel();'><i class='fa fa-trash-o'></i></a>
                                        </div>
                                    </div>

                                    <center>
                                        <div class="form-group">
                                            <button  class="btn btn-primary" type="submit"  >Guardar</button>         
                                            <a class="btn btn-danger" type="button"  href="../proveedor" >Cancelar</a>
                                            <input type="text" id="id-prov" name="id-prov" value="<?= $id ?>" hidden/>
                                        </div>
                                    </center>    
                                </form>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- /Page Content -->
</div>
<!-- /Page Container -->
<!-- MODAL DE DIRECCION-->
<div class="modal fade" id="editar-direccion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Direccion</b></h4>
            </div>
            <form id="formularioDireccion" name="formularioDireccion" class="formulario" >
                <div class="modal-body">

                    <center>
                        <div class="row" >
                            <div class="col-lg-12 col-sm-12 col-xs-12" >

                                <div class="form-inline" >

                                    <div class="form-group">
                                        Direccion
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="Mdireccion" name="Mdireccion" placeholder="Direccion" />
                                    </div>                    
                                </div>    

                            </div>
                        </div>
                    </center>
                </div>
                <input type="text" id="provId" name="provId" hidden/>
                <input type="text" id="idDireccion" name="idDireccion" hidden/>
            </form>
            <div class="modal-footer">
                <div class="form-inline" >                          
                    <div class="form-group">

                        <button  id="actualizarDir"class="btn btn-primary" onclick="actualizarDireccion()" data-dismiss="modal" aria-hidden="true" >Actualizar</button>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="form-group">

                        <button  id="guardarDir"class="btn btn-primary" onclick="nuevaDireccion()" data-dismiss="modal" aria-hidden="true" >Guardar</button>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="form-group">
                        <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>

                    </div>

                </div>
            </div>  
        </div>

    </div>
</div>
<!-- MODAL DE CORREO-->
<div class="modal fade" id="editar-correo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Correo</b></h4>
            </div>
            <form id="formularioCorreo" name="formularioCorreo" class="formulario" >
                <div class="modal-body">

                    <center>
                        <div class="row" >
                            <div class="col-lg-12 col-sm-12 col-xs-12" >

                                <div class="form-inline" >

                                    <div class="form-group">
                                        Correo
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="Mcorreo" name="Mcorreo" placeholder="Direccion" />
                                    </div>                    
                                </div>    

                            </div>
                        </div>
                    </center>
                </div>
                <input type="text" id="provIdC" name="provIdC" hidden/>
                <input type="text" id="idcorreo" name="idcorreo" hidden/>
            </form>
            <div class="modal-footer">
                <div class="form-inline" >                          
                    <div class="form-group">

                        <button  id="actualizarCorreo"class="btn btn-primary" onclick="actualizarCorreo()" data-dismiss="modal" aria-hidden="true" >Actualizar</button>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="form-group">

                        <button  id="guardarCorreo"class="btn btn-primary" onclick="nuevoCorreo()" data-dismiss="modal" aria-hidden="true" >Guardar</button>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="form-group">
                        <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>

                    </div>

                </div>
            </div>  
        </div>

    </div>
</div>
<!-- MODAL DE TELEFONO-->
<div class="modal fade" id="editar-telefono" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Nro. de Telefono</b></h4>
            </div>
            <form id="formularioTelefono" name="formularioTelefono" class="formulario" >
                <div class="modal-body">

                    <center>
                        <div class="row" >
                            <div class="col-lg-12 col-sm-12 col-xs-12" >

                                <div class="form-inline" >

                                    <div class="form-group">
                                        Telefono
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="Mtelefono" name="Mtelefono" placeholder="Nro. de Telefono" onkeypress="return validarNumeroProveedor(event);"/>
                                    </div>                    
                                </div>    

                            </div>
                        </div>
                    </center>
                </div>
                <input type="text" id="provIdTelf" name="provIdTelf" hidden/>
                <input type="text" id="idTelf" name="idTelf" hidden/>
            </form>
            <div class="modal-footer">
                <div class="form-inline" >                          
                    <div class="form-group">

                        <button  id="actualizarTelefono"class="btn btn-primary" onclick="actualizarTelefono()" data-dismiss="modal" aria-hidden="true" >Actualizar</button>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="form-group">

                        <button  id="guardarTelefono"class="btn btn-primary" onclick="nuevoTelefono()" data-dismiss="modal" aria-hidden="true" >Guardar</button>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="form-group">
                        <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>

                    </div>

                </div>
            </div>  
        </div>

    </div>
</div>
<!-- MODAL DE CELULAR-->
<div class="modal fade" id="editar-celular" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Nro. de Celular</b></h4>
            </div>
            <form id="formularioCelular" name="formularioCelular" class="formulario" >
                <div class="modal-body">

                    <center>
                        <div class="row" >
                            <div class="col-lg-12 col-sm-12 col-xs-12" >

                                <div class="form-inline" >

                                    <div class="form-group">
                                        Celular
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="Mcelular" name="Mcelular" placeholder="Nro. de celular" onkeypress="return validarNumeroProveedor(event);"/>
                                    </div>                    
                                </div>    

                            </div>
                        </div>
                    </center>
                </div>
                <input type="text" id="provIdCel" name="provIdCel" hidden/>
                <input type="text" id="idCel" name="idCel" hidden/>
            </form>
            <div class="modal-footer">
                <div class="form-inline" >                          
                    <div class="form-group">

                        <button  id="actualizarCelular"class="btn btn-primary" onclick="actualizarCelular()" data-dismiss="modal" aria-hidden="true" >Actualizar</button>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="form-group">

                        <button  id="guardarCelular"class="btn btn-primary" onclick="nuevoCelular()" data-dismiss="modal" aria-hidden="true" >Guardar</button>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="form-group">
                        <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>

                    </div>

                </div>
            </div>  
        </div>

    </div>
</div>
<?php require_once '../header_footer/footer.php'; ?>


