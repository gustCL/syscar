
<?php require_once 'head.php'; ?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>PROVEEDORES</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>

    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
               

                            <div class="widget-body ">
                                <form id="formProveedor" class="form-horizontal">
                                    <br/><br/>
                                    <div class="form-inline">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;


                                        <div class="form-group">


                                            <div class="col-lg-3">
                                                <span class="input-icon">
                                                    <input type="text" class="form-control" id="bs-prod" name="bs-prod" size="50" placeholder="Busca un proveedor por: NIT o Empresa o Nombre de Contacto"/>
                                                    <i class="glyphicon glyphicon-search circular blue"></i>
                                                </span>    
                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                        <div class="input-group">
                                            <input id="fechaB" name="fechaB" type="date" placeholder="Fecha" >
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span> 
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                        <div class="input-group">
                                             <a type="button" href="javascript: buscar_Proveedor()" class="btn btn-yellow"  >Buscar</a>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                        <div class="input-group">
                                            <a href="nuevo_proveedor.php" id="" class="btn btn-warning shiny" ><i class="fa fa-plus"></i>Nuevo Proveedor</a>
                                        </div>
                                    </div>

                                    <br/><br/>

                                </form> 

                            </div>
                            <br/>
                            <div class="widget-body" id="agrega-registros" >
                                <center><h3><strong>LISTA DE PROVEEDORES</strong></h3></center>
                                <br/>
                                <table class="table table-striped " >
                                                   
                                    <thead>
                                        <!--Fila de titulos de columnas-->
                                        <tr>
                                            <th class="hidden-xs">
                                                Id.
                                            </th>
                                            <th class="hidden-xs">
                                                NIT
                                            </th>
                                            <th class="hidden-xs">
                                                Nombre Contacto
                                            </th>
                                            <th class="hidden-xs">
                                                Empresa
                                            </th>
                                            <th class="hidden-xs">
                                                Nacionalidad
                                            </th> 

                                            <th class="hidden-xs">

                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //OBTENEMOS PROVEEDORES ACTIVOS
                                        $consulta = "select * from Pais pa, Proveedor pro  where pro.idPais = pa.idPais and pro.estado = '1' ORDER BY pro.idProveedor DESC";
                                        require_once '../datos/Database.php';

                                        try {

                                            $comando = Database::getInstance()->getDb()->prepare($consulta);
                                            $comando->execute();
                                            while ($row = $comando->fetch()) {
                                                $idP = $row['idProveedor'];
                                                ?>

                                                <tr>
                                                    <td id = "idProveedor" name="idProvedor" >
                                                        <?= $idP ?>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        <?= $row['nit'] ?>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        <?= $row['nombreContacto'] ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['nombreProve'] ?>
                                                    </td>

                                                    <td>
                                                        <?= $row['nombrePais'] ?>
                                                    </td>

                                                    <td>
                                                        <a href="javascript:verProveedor('<?= $idP ?>');"   class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                                                        <a href="editar_proveedor.php?id=<?= $idP ?>" class="btn btn-default btn-xs purple" ><i class="fa fa-edit"></i> Editar</a>
                                                        <a href="javascript:eliminarProveedor('<?= $idP ?>');" id="eliminar"class="btn btn-default btn-xs black"><i class="fa fa-trash-o"></i> Eliminar</a>

                                                    </td>

                                                </tr>
                                                <?php
                                            }//terminacion del while
                                        } catch (PDOException $e) {

                                            echo 'Error: ' . $e;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <br/>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
    
    <!-- FORMULARIO MODAL PARA VER DATOS DE PROVEEDOR"-->

    <div class="modal fade" id="ver-proveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><b>Proveedor</b></h4>
                </div>

                <!--              aqui agregamos los registros-->
                
                <form id="formulario" class="formulario">
                     
                    <br/>
                             
                    <div class="form-inline" > 
                       &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;  
                        <div class="form-group">
                            <label class="col-lg-12"> NIT </label> 
                           
                        </div>
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                        <div class="input-group">

                            <input id="nit" class="form-control" type="text" placeholder="Numero de NIT" readonly>
                        </div>
                        
                        
                        <div class="form-group">
                        
                            <label class="col-lg-12"> Nacionalidad </label>
                        
                        </div>
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;                       
                        <div class="input-group">
                            <input id="pais" class="form-control" type="text" placeholder="Pais" readonly> 
                        </div> 

                    </div>

                    <br/>            
                    <div class="form-inline" > 
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label class="col-lg-12"> Nombre Empresa </label>
                        </div>
                        <div class="input-group">
                            <input type="email" class="form-control"  id="empresa"  placeholder="Empresa" readonly>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-12"> Nombre Contacto </label>

                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control"  id="contacto"  placeholder="Nombre contacto" readonly>
                        </div>

                    </div>                
                    <br />  
                    <div id="cel" class="form-inline">
                        <!--Aqui se muestra los las direcciones y correos-->
                    </div>
                    <div id="tel" class="form-inline">
                        <!--Aqui se muestra los N de telefono y  Celular-->
                    </div> 
                    <br/>
                    <div id="producto" class="form-inline">
                        <!--Aqui se muestra los productos-->
                    </div> 
                    <br/>
                </form>
                
                <div class="modal-footer">
                    <center>
                    <button  class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cerrar</button>  
                    </center>
                </div>
            </div> 

        </div>
    </div>

    <?php
    require_once ('../header_footer/footer.php');
    ?>