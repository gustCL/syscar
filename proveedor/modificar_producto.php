<?php
require_once ('head.php');
require_once ('../datos/Database.php');

$id = $_GET['id'];


$dato = Database::getInstance()->getDb()->prepare("select codigoProducto  from Provee p  where p.idProveedor = '$id'  ");
$dato->execute();
$i=0;
    $dar=[];
    while ($idCU=$dato->fetch()){
        $dar[$i]=$idCU['codigoProducto'];
        $i++;
    }
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>Nuevo Producto</strong>

            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>

    <div class="main-container container-fluid">

        <div class="page-body">
            <div class="row">
                <div class="col-xs-12 col-md-12">

                    <div class="widget-body">


                        <center>
                            <h1>
                                Elija el Proveedor 

                            </h1>
                        </center>
                        <form   method="post" action="proveedor_cambiaProductos.php?id=<?= $id ?>" class="form-horizontal" >


                            <table class="table table-striped table-bordered table-hover" id="simpledatatable">
                                <thead>
                                    <tr>
                                        <th>
                                            <label>
                                                <input type="checkbox" name="casouso[]" >
                                                <span class="text"></span>
                                            </label>
                                        </th>

                                        <th class="hidden-xs">
                                            Codigo
                                        </th>

                                        <th class="hidden-xs">
                                            Descripcion
                                        </th>
                                        <th class="hidden-xs">
                                            Precio Costo
                                        </th>
                                        <th class="hidden-xs">
                                            Precio Venta
                                        </th> 


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //OBTENEMOS PRODUCTOS ACTIVOS
                                     $consulta = "select * from Producto pro  where estado = '1' ORDER BY pro.codigoProducto DESC";
                                    require_once '../datos/Database.php';

                                    try {

                                        $comando = Database::getInstance()->getDb()->prepare($consulta);
                                        $comando->execute();
                                        while ($row = $comando->fetch()) {
                                            $idP = $row['codigo'];
                                            ?>
                                            <tr>
                                                <td>


                                                    <?php
                                                    $checked = "";

                                                    if (in_array($row['codigoProducto'], $dar)) {


                                                        $checked = 'checked';
                                                    }
                                                    ?>
                                                    <label>
                                                        <input type="checkbox" name="casouso[]" value="<?= $row['codigoProducto'] ?>" <?= $checked ?> >
                                                        <span class="text"> </span>
                                                    </label>


                                                </td>
                                                <td id = "idProveedor" name="idProvedor" >
                                                    <?= $idP ?>
                                                </td>

                                                <td class="hidden-xs">
                                                    <?= $row['nombreComercial'] ?>
                                                </td>
                                                <td>
                                                    <?= $row['precioCosto'] ?>
                                                </td>

                                                <td>
                                                    <?= $row['precioVenta'] ?>
                                                </td>


                                            </tr>
                                            <?php
                                        }//terminacion del while
                                    } catch (PDOException $e) {

                                        echo 'Error: ' . $e;
                                    }
                                    ?>    



                                </tbody>
                            </table>
                            <div class="form-inline">
                                &nbsp;&nbsp;&nbsp;&nbsp; <input class="btn btn-primary"  type="submit" value="Actualizar "/>
                            
                                &nbsp;&nbsp;&nbsp;&nbsp; <a class="btn btn-danger" type="button" href="../proveedor" > Cancelar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>      


</div>


</div>

<?php
require_once ('../header_footer/footer.php');
?>

