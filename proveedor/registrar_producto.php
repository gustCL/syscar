<?php
require_once ('head.php');
?>
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>NUEVO PROVEEDOR</strong>

            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>

    <div class="main-container container-fluid">

        <div class="page-body">
            <div class="row">
                <div class="col-xs-12 col-md-12">

                    <div class="widget-body">

                        <form  method="post" action="asigna_producto.php">
                            <center>
                            <h1>
                                Elija el Producto

                            </h1>
                            </center>
                            

                            <table class="table table-striped " id="simpledatatable">
                                <thead>
                                    <tr>
                                        <th>
                                            <label>
                                                <input type="checkbox" name="casouso[]" >
                                                <span class="text"></span>
                                            </label>
                                        </th>

                                        <th class="hidden-xs">
                                            Codigo
                                        </th>

                                        <th class="hidden-xs">
                                            Descripcion
                                        </th>
                                        <th class="hidden-xs">
                                            Precio Costo
                                        </th>
                                        <th class="hidden-xs">
                                            Precio Venta
                                        </th> 
                                        


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //OBTENEMOS PRODUCTOS ACTIVOS
                                    $consulta = "select * from Producto pro  where estado = '1' ORDER BY pro.codigoProducto DESC";
                                    require_once '../datos/Database.php';

                                    try {

                                        $comando = Database::getInstance()->getDb()->prepare($consulta);
                                        $comando->execute();
                                        while ($row = $comando->fetch()) {
                                            $idP = $row['codigoProducto'];
                                            ?>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input type="checkbox" name="casouso[]"  value="<?= $row['codigoProducto'] ?>"  >
                                                        <span class="text"></span>
                                                    </label>
                                                </td>
                                                <td id = "idProveedor" name="idProvedor" >
                                                    <?= $row['codigo'] ?>
                                                </td>

                                                <td class="hidden-xs">
                                                    <?= $row['nombreComercial'] ?>
                                                </td>
                                                <td>
                                                    <?= $row['precioCosto'] ?>
                                                </td>
                                                <td>
                                                    <?= $row['precioVenta'] ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }//terminacion del while
                                    } catch (PDOException $e) {

                                        echo 'Error: ' . $e;
                                    }
                                    ?>    



                                </tbody>
                            </table>
                            <div class="form-inline">
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-primary" type="submit" value="Asignar Producto" />
                                &nbsp;&nbsp;&nbsp;&nbsp; <a class="btn btn-danger" type="button" href="../proveedor" > Cancelar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>      


</div>


</div>

<?php
require_once ('../header_footer/footer.php');
?>