
<?php
require_once ('head.php');
?>

<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong> NUEVO ALMACEN </strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>


    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">                                          
                    <div class="widget-body">
                        <div>

                            <form id="registraralma" method="post" action="../almacen/datosAlmacen/almacen_guardar.php"
                                  data-bv-message="This value is not valid"
                                  data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                                  data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                                  data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">

                                <center>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Nombre</label>
                                        <div class="col-lg-4">
                                            <input type="text" required class="form-control" id="aldescripcion" name ="aldescripcion" placeholder="Descripcion"
                                                   data-bv-notempty="true"
                                                   data-bv-notempty-message="Ingrese datos en campo vacio"/>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <div class="form-group" id="holamundo">
                                        <label class="col-lg-2 control-label">Tipo de Almacen</label>
                                        <div class="col-lg-10" id="todoo">
                                            <div class="form-inline" id="listipo">
                                                <select class="col-xs-3" required id="altipo" name="altipo" data-toggle="simplecolorpicker">
                                                    <option value="0">Selecciones un tipo de almacen</option>
                                                    <?php
                                                    listaTipoAlmacen();
                                                    ?>
                                                </select>

                                            </div>
                                            <a href="#"  id="nuevoTipoAlmacen" class="btn btn-info">Nuevo Tipo de Almacen</a>

                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Sucursal</label>
                                        <div class="col-lg-10">
                                            <select class="col-xs-3" id="alsucursal" name="alsucursal" data-toggle="simplecolorpicker">
                                                <option value="0">Selecciones una sucursal</option>
                                                <?php
                                                listaSucursales();
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>                                                        
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Ubicacion</label>
                                        <div class="col-lg-4">
                                            <input type="text" required class="form-control" id="alubicacion"name="alubicacion" placeholder="Escriba la direccion exacta del almacen"
                                                   data-bv-notempty="true">
                                        </div>
                                    </div>
                                    <br>
                                    <br>

                                    <div class="row">                                                        
                                        <div class="col-lg-offset-4 col-lg-4">
                                            <input class="btn btn-primary" type="submit" value="Guardar" id="botiti" />
                                            <button type="button" class="btn btn-danger"  onclick="location = 'index.php'" >Cancelar</button>
                                        </div>

                                    </div>

                                </center>



                            </form>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>


<div class="modal fade" id="registra_tipoAlm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Tipo de Almacen</b></h4>
            </div>
            <form id="Formulario_Almacen" class="formulario">
                <div class="modal-body">
                    <div class="row" >
                        <div class="col-lg-12 col-sm-12 col-xs-12" >
                            <div class="form-inline" >        
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="nvotipoalmacen"><strong>Tipo almacen</strong></label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nvotipoalmacen" name="nvotipoalmacen">&nbsp;&nbsp;
                                </div>
                                <!--BOTONES-->
                                <div class="resposive">
                                    <table class="table table-striped">
                                        <thead>
                                            <!--Fila de titulos de columnas-->
                                            <tr>
                                                <th class="hidden-xs">
                                                    Id
                                                </th>
                                                <th class="hidden-xs">
                                                    Nombre
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $consulta = "SELECT * 
                                                            FROM TipoAlmacen";
                                            try {

                                                $comando = Database::getInstance()->getDb()->prepare($consulta);
                                                $comando->execute();
                                                while ($row = $comando->fetch()) {
                                                    $idP = $row['idTipoAlmacen'];
                                                    ?>

                                                    <tr>
                                                        <td>
                                                            <?= $idP ?>
                                                        </td>
                                                        <td class="hidden-xs">
                                                            <?= $row['nombreTipo'] ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }//terminacion del while
                                            } catch (PDOException $e) {
                                                echo 'Error: ' . $e;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <br>  
                                <br>
                                <br>
                                <center>
                                    <footer>
                                        <div class="form-inline" >
                                            <div class="form-group">
                                                <button onclick=" return RegistrarTipoAlmacen();" class="btn btn-primary" id="nuevo" data-dismiss="modal" aria-hidden="true">Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button  class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                                            </div>

                                        </div>
                                    </footer>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!--F U N C I O N E S   P H P -->
<?php

function listaTipoAlmacen() {
    //$consulta = "SELECT * FROM privilegiosmodulo";
    $consulta = "select*from TipoAlmacen";
    require_once '../datos/Database.php';
    try {
        // Preparar sentencia
        $comando = Database::getInstance()->getDb()->prepare($consulta);
        // Ejecutar sentencia preparada
        $comando->execute();
        while ($lstModulos = $comando->fetch()) {
            ?>
            </option><option value="<?= $lstModulos['idTipoAlmacen'] ?>"><?= $lstModulos['nombreTipo'] ?></option>
            <?php
        }
    } catch (PDOException $e) {
        echo 'Error: ' . $e;
    }
}
?>

<?php

function listaSucursales() {
    //$consulta = "SELECT * FROM privilegiosmodulo";
    $consulta = "select* from sucursal";
    require_once '../datos/Database.php';
    try {
        // Preparar sentencia
        $comando = Database::getInstance()->getDb()->prepare($consulta);
        // Ejecutar sentencia preparada
        $comando->execute();
        while ($lstModulos = $comando->fetch()) {
            ?>
            </option><option value="<?= $lstModulos['idSucursal'] ?>"><?= $lstModulos['nombreSucur'] ?></option>
            <?php
        }
    } catch (PDOException $e) {
        echo 'Error: ' . $e;
    }
}
?>
<!-- F U N C I O N E S   J A V A   S C R I P T -->

<?php
require_once ('../header_footer/footer.php');
?>