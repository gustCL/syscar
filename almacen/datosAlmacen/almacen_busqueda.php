<?php
    session_start();
    if ($_SESSION['logueado'] != 'SI') {
        header('Location: ../inicio');
    }
    require_once '../../datos/Database.php';
?>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <!--Fila de titulos de columnas-->
        <tr>
            <th class="hidden-xs">
                Cod.
            </th>
            <th class="hidden-xs">
                Nombre
            </th>
            <th class="hidden-xs">
                Ubicacion
            </th>
            <th class="hidden-xs">
                Tipo Almacen
            </th>
            <th>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $dato = $_POST['dato'];
        $auxSuc = $_SESSION['userMaster'];
        $auxidsucursal = $auxSuc['idSucursal'];
        $consulta ="SELECT * 
                    FROM Almacen al,TipoAlmacen tal  
                    WHERE al.idTipoAlmacen=tal.idTipoAlmacen 
                    AND al.estado=1
                    AND idSucursal='$auxidsucursal'
                    AND (al.idAlmacen LIKE '$dato%' 
                    OR al.nombreAlmacen 
                    LIKE '$dato%' 
                    OR al.direccion LIKE '$dato%')";
        try {

            $comando = Database::getInstance()->getDb()->prepare($consulta);
            $comando->execute();
            while ($row = $comando->fetch()) {
                $idP = $row['idAlmacen'];
                ?>

                <tr>
                    <td>
                        <?= $idP ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['nombreAlmacen'] ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['direccion'] ?>

                    </td>
                    <td>
                        <?= $row['nombreTipo'] ?>
                    </td>


                    <td>
                        <a href="javascript:verAlmacen('<?= $idP ?>');" class="btn btn-default btn-xs blue">
                            <i class="fa fa-eye"></i> Ver
                        </a>
                        <a href="javascript:editarAlmacen('<?= $idP ?>');" class="btn btn-default btn-xs purple">
                            <i class="fa fa-edit"></i> Editar
                        </a>
                        <a href="javascript:eliminarAlmacen('<?= $idP ?>');" class="btn btn-default btn-xs black">
                            <i class="fa fa-trash-o"></i> Eliminar
                        </a>

                    </td>

                </tr>
                <?php
            }//terminacion del while
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
        ?>
    </tbody>
</table>
