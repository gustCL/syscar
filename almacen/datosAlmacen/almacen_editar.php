<?php
require_once ("../../datos/Database.php");

$alcodigo = $_POST['alcod_edit'];
$aldescripcion = $_POST['alnombre_edit'];
$alubicacion = $_POST['alubicacion_edit'];
$altipo = $_POST['altipo_editar'];

$consultaP = "UPDATE Almacen SET nombreAlmacen='$aldescripcion', direccion='$alubicacion',"
        . "idTipoAlmacen='$altipo' WHERE idAlmacen = '$alcodigo'";
$comandoP = Database::getInstance()->getDb()->prepare($consultaP);
$comandoP->execute();
?>


<table class="table table-striped table-bordered table-hover">
    <thead>
        <!--Fila de titulos de columnas-->
        <tr>
            <th class="hidden-xs">
                Cod.
            </th>
            <th class="hidden-xs">
                Nombre
            </th>
            <th class="hidden-xs">
                Ubicacion
            </th>
            <th class="hidden-xs">
                Tipo Almacen
            </th>
            <th>
            </th>
        </tr>
    </thead>
    <tbody>
<?php
$consulta = "select*from Almacen al,TipoAlmacen tal where al.idTipoAlmacen=tal.idTipoAlmacen and al.estado=1"; //Consulta para los Almacenes
//Consulta para telefonos de proveedores
require_once '../../datos/Database.php';

try {

    $comando = Database::getInstance()->getDb()->prepare($consulta);
    $comando->execute();
    while ($row = $comando->fetch()) {
        $idP = $row['idAlmacen'];
        ?>

                <tr>
                    <td>
        <?= $idP ?>
                    </td>
                    <td class="hidden-xs">
        <?= $row['nombreAlmacen'] ?>
                    </td>
                    <td class="hidden-xs">
        <?= $row['direccion'] ?>
                    </td>
                    <td>
        <?= $row['nombreTipo'] ?>
                    </td>
                    <td>
                        <a href="javascript:verAlmacen('<?= $idP ?>');" class="btn btn-default btn-xs blue">
                            <i class="fa fa-eye"></i> Ver
                        </a>
                        <a href="javascript:editarAlmacen('<?= $idP ?>');" class="btn btn-default btn-xs purple">
                            <i class="fa fa-edit"></i> Editar
                        </a>
                        <a href="javascript:eliminarAlmacen('<?= $idP ?>');" class="btn btn-default btn-xs black">
                            <i class="fa fa-trash-o"></i> Eliminar
                        </a>

                    </td>

                </tr>
        <?php
    }//terminacion del while
} catch (PDOException $e) {
    echo 'Error: ' . $e;
}
?>
    </tbody>
</table>
