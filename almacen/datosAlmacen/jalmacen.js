
$(function() {
//HEN Busqueda almacen <-Funciona !  :-D ->
    $('#bs-almacen').on('keyup', function() {
        var dato = $('#bs-almacen').val();
        var url = '../almacen/datosAlmacen/almacen_busqueda.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'dato=' + dato,
            success: function(datos) {
                $('#agrega-registros').html(datos);
            }
        });
        return false;
    });

});
function  redireccionarCombo() {
    var url = listaTipoAlmacen();
}

//HEN  Funciona 
function verAlmacen(id) {
    $('#formularioVer')[0].reset();
    var url = '../almacen/datosAlmacen/almacen_ver.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(valores) {
            var datos = eval(valores);
            $('#alcod').val(datos[0]);
            $('#alnombre').val(datos[1]);
            $('#alubicacion').val(datos[2]);
            $('#altipo').val(datos[3]);
            $('#formulario_ver').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;
}
;
function eliminarAlmacen(id) {
    var url = '../almacen/datosAlmacen/almacen_eliminar.php';
    var pregunta = confirm('¿Esta seguro de eliminar este Almacen?');
    if (pregunta == true) {
        $.ajax({
            type: 'POST',
            url: url,
            data: 'id=' + id,
            success: function(registro) {
                $('#agrega-registros').html(registro);
                return false;
            }
        });
        return false;
    } else {
        return false;
    }
}
;
function editarAlmacen(id) {
    $('#formularioEditar')[0].reset();
    var url = '../almacen/datosAlmacen/almacen_editar_ver.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(valores) {
            var datos = eval(valores);
            $('#alcod_edit').val(datos[0]);
            $('#alnombre_edit').val(datos[1]);
            $('#alubicacion_edit').val(datos[2]);
            $('#altipo_edit').html(datos[3]);
            $('#modal_editar').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;
}



function actualizarAlmacen() {
    var dato = $("#formularioEditar").serialize();
    $.ajax({
        url: '../almacen/datosAlmacen/almacen_editar.php',
        data: dato,
        type: 'post',
        success: function(registro) {

            $('#mensaje').addClass('bien').html('Edicion completada con exito').show(200).delay(2500).hide(200);
            $('#agrega-registros').html(registro);
            return false;
        }

    });
    return false;
}
;
$(function() {
    $('#nuevoTipoAlmacen').on('click', function() {
        $('#Formulario_Almacen')[0].reset();
        //$('#actu').hide();
        $('#nuevo').show();
        $('#registra_tipoAlm').modal({
            show: true,
            backdrop: 'static'
        });
    });
});
function RegistrarTipoAlmacen() {
    var frm = $("#formularioNuevaTipoAlm").serialize();
    var almacen = $('#nvotipoalmacen').val();
    alert(almacen);
    $.ajax({
        url: 'nuevotipoalmacen.php',
        type: 'POST',
        data: frm + '&idalm=' + almacen,
        success: function() {
            
        }

    });
    return false;
}
;

function nuevaTipoAlm() {
    $('#formularioNuevaLinea')[0].reset();
    $.ajax({
        success: function() {
            $('#modal_Nueva_Linea').modal({
            });
            return false;
        }
    });
    return false;
}