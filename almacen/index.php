<?php
include_once 'head.php';
require_once '../datos/Database.php';
?>

<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>
                  LISTA ALMACEN 
                </strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                
                                <div class="widget-body">
                                    <center>
                                        <div class="form-inline">
                                            <br>
                                                <div class="form-group"> 
                                                    <div class="col-lg-3">
                                                        <span class="input-icon">
                                                            <input type="text"  id="bs-almacen" class="form-control" name="bs-almacen" size="40" placeholder="cod, nombre">
                                                            <i class="glyphicon glyphicon-search circular blue"></i>
                                                        </span>
                                                    </div>
                                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                     <a href="nuevoalmacen.php"  class="btn btn-primary" >Nuevo Almacen</a>
                                                </div>
                                        </div>
<!--                                        <div class="form-group">    
                                            <input type="text"  id="bs-almacen" name="bs-almacen" size="40" placeholder="cod, nombre">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="nuevoalmacen.php"  class="btn btn-primary" >Nuevo Almacen</a>
                                        </div>-->
                                    </center>
                                    <br>
                                </div>
                                
                                    <br />
                                 <div class="widget-body">
                                      <center><h3><strong>LISTA DE ALMACEN</strong></h3></center>
                                  <br/>
                                    <!-- TABLA DE ALMACENES-->
                                    <div class="resposive" id="agrega-registros">                    
                                        <table class="table table-striped">
                                            <thead>
                                                <!--Fila de titulos de columnas-->
                                                <tr>
                                                    <th class="hidden-xs">
                                                        Cod.
                                                    </th>
                                                    <th class="hidden-xs">
                                                        Nombre
                                                    </th>
                                                    <th class="hidden-xs">
                                                        Ubicacion
                                                    </th>
                                                    <th class="hidden-xs">
                                                        Tipo Almacen
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $auxSuc = $_SESSION['userMaster'];
                                                $auxidsucursal = $auxSuc['idSucursal'];
                                                $consulta ="SELECT * 
                                                            FROM Almacen al,TipoAlmacen tal  
                                                            WHERE al.idTipoAlmacen=tal.idTipoAlmacen 
                                                            AND al.estado=1
                                                            AND idSucursal='$auxidsucursal'";
                                                try {

                                                    $comando = Database::getInstance()->getDb()->prepare($consulta);
                                                    $comando->execute();
                                                    while ($row = $comando->fetch()) {
                                                        $idP = $row['idAlmacen'];
                                                        ?>

                                                        <tr>
                                                            <td>
                                                                <?= $idP ?>
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <?= $row['nombreAlmacen'] ?>
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <?= $row['direccion'] ?>
                                                            </td>
                                                            <td>
                                                                <?= $row['nombreTipo'] ?>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:verAlmacen('<?= $idP ?>');" class="btn btn-default btn-xs blue">
                                                                    <i class="fa fa-eye"></i> Ver
                                                                </a>
                                                                <a href="javascript:editarAlmacen('<?= $idP ?>');" class="btn btn-default btn-xs purple">
                                                                    <i class="fa fa-edit"></i> Editar
                                                                </a>
                                                                <a href="javascript:eliminarAlmacen('<?= $idP ?>');" class="btn btn-default btn-xs black">
                                                                    <i class="fa fa-trash-o"></i> Eliminar
                                                                </a>

                                                            </td>

                                                        </tr>
                                                        <?php
                                                    }//terminacion del while
                                                } catch (PDOException $e) {
                                                    echo 'Error: ' . $e;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>    
                              

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--     D I A L O G   M O D A L ##V E R##   -->
<div class="modal fade" id="formulario_ver"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Almacen</h4>
            </div>
            <form id="formularioVer" class="formulario">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Codigo
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control "  id="alcod" name="alcod" readonly="true">
                            </div>

                            <div class="form-group">
                                Nombre
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control"  id="alnombre" name="alnombre" readonly="true">
                            </div>
                            <div class="form-group">
                                Ubicacion
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control"  id="alubicacion" name="alubicacion" readonly="true">
                            </div>
                            <div class="form-group">
                                Tipo de Almacen
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control"  id="altipo" name="altipo" readonly="true">
                            </div>
                            <br />             

                            <br />             
                            <!--BOTONES-->
                            <br />            
                            <center>
                                <div class="form-group">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                </div>  
                            </center>
                        </div>
                    </div>                        
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- D I A L O G   M O D A L   ##E D I T A R## -->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModallLabel">Editar Almacen</h4>
            </div>
            <form id="formularioEditar" name="formularioEditar" class="formulario" >
                <div class="modal-body">                        
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Codigo
                                <input type="text" class="form-control"  id="alcod_edit" name="alcod_edit" readonly="true">
                            </div>

                            <div class="form-group">
                                Nombre
                                <input type="text" class="form-control"  id="alnombre_edit" name="alnombre_edit">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-left" for="inputEmail3">Ubicacion</label> 
                                <input type="text" class="form-control"  id="alubicacion_edit" name="alubicacion_edit" >
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-left" for="inputEmail3">Tipo de Almacen</label> 
                                <br>
                                <div id="altipo_edit">

                                </div>
                            </div>
                            <br />             

                            <br />             
                            <!--BOTONES-->
                            <br />            
                            <center>
                                <div class="form-group">
                                    <button class="btn btn-primary" onclick="return actualizarAlmacen()" data-dismiss="modal" aria-hidden="true">Guardar</button>&nbsp;&nbsp;&nbsp;
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                                </div>   
                            </center>
                        </div>
                    </div>                        
                </div>
            </form>
        </div> 
        <!--/.modal-content--> 
    </div> 
    <!--/.modal-dialog--> 
</div>



<?php
include_once '../header_footer/footer.php';
?>

<?php

function listaTipoAlmacen() {
    //$consulta = "SELECT * FROM privilegiosmodulo";
    $consulta = "select*from TipoAlmacen";
    require_once '../datos/Database.php';
    try {
        // Preparar sentencia
        $comando = Database::getInstance()->getDb()->prepare($consulta);
        // Ejecutar sentencia preparada
        $comando->execute();
        while ($lstModulos = $comando->fetch()) {
            ?>
            <option value="<?= $lstModulos['idTipoAlmacen'] ?>"><?= $lstModulos['nombreTipo'] ?></option>
            <?php
        }
    } catch (PDOException $e) {
        echo 'Error: ' . $e;
    }
}
?>
