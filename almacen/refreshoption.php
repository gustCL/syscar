<?php

function listaTipoAlmacen() {
    //$consulta = "SELECT * FROM privilegiosmodulo";
    $consulta = "select*from TipoAlmacen";
    require_once '../datos/Database.php';
    try {
        // Preparar sentencia
        $comando = Database::getInstance()->getDb()->prepare($consulta);
        // Ejecutar sentencia preparada
        $comando->execute();
        while ($lstModulos = $comando->fetch()) {
            ?>
            </option><option value="<?= $lstModulos['idTipoAlmacen'] ?>"><?= $lstModulos['nombreTipo'] ?></option>
            <?php
        }
    } catch (PDOException $e) {
        echo 'Error: ' . $e;
    }
}
?>

<div class="col-lg-10" id="todoo">
    <div class="form-inline" id="listipo">
        <select class="col-xs-3" required id="altipo" name="altipo" data-toggle="simplecolorpicker">
            <option value="0">Selecciones un tipo de almacen</option>
            <?php
            listaTipoAlmacen();
            ?>
        </select>

    </div>
    <a   id="nuevoTipoAlmacen" class="btn btn-info">Nuevo Tipo de Almacen</a>

</div> 
