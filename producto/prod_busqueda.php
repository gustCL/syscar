<?php
session_start();
if ($_SESSION['logueado'] != 'SI') {
    header('Location: ../inicio');
}
require_once '../datos/Database.php';
?>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>
                Codigo 
            </th>
            <th>
                Descripcion
            </th>
            <th>
                Stock
            </th>
            <th>
                Lote
            </th>
            <th>
                Precio Venta
            </th>
            <th>
                Fecha Vencimiento
            </th>
            <th>
                Ubicacion
            </th>
            <!--Botones-->
            <th>

            </th>

        </tr>
    </thead>
    <tbody>
        <?php
        require_once '../datos/Database.php';
        try {
            $auxSucursal = $_SESSION['userMaster'];
            $aux2 = $auxSucursal['idSucursal'];
            $registro = "SELECT p.codigoProducto, p.nombreComercial, p.idLote, p.precioVenta, p.fechavencimiento, p.nombreSector,p.stock FROM Inventario p WHERE p.stock>0 AND p.idSucursal='$aux2'";
            //$registro = "SELECT p.codigoProducto , e.codigo , p.nombreComercial , e.nroLote , e.precioVenta , e.fechaVencimiento , s.nombreSector , a.nombreAlmacen from Producto p , Existencias e , Ubicaciones u , Sectores s , Almacen a  where p.codigoProducto = e.codigoProducto and e.sector = s.idSector and s.idSector = u.idSector  and a.idAlmacen = u.idAlmacen ORDER BY p.codigoProducto DESC ";
            $comando = Database::getInstance()->getDb()->prepare($registro);
            $comando->execute();
            while ($row = $comando->fetch()) {
                $idP = $row['codigoProducto'];
                ?>
                <tr>
                    <td id = "codigo" name="codigo" > 
                        <?= $idP ?>
                    </td>                                                                
                    <td>
                        <?= $row['nombreComercial'] ?>
                    </td>
                    <td>
                        <?= $row['stock'] ?>
                    </td>

                    <td>
                        <?= $row['idLote'] ?>
                    </td>
                    <td>
                        <?= $row['precioVenta'] ?>
                    </td>
                    <td>
                        <?= $row['fechavencimiento'] ?>
                    </td>
                    <td>
                        <?= $row['nombreSector'] ?>
                    </td>
                    <!--aqui estan los botones que hacen activar las vnetanas del modal-->
                    <td>                          
                        <a href="ver_productocopy.php?id=<?= $row['codigoProducto'] ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                    </td>
                </tr>
                <?php
            }//terminacion del while
        } catch (PDOException $e) {

            echo 'Error: ' . $e;
        }
        ?>    
    </tbody>
</table>
