
<?php
require_once("../datos/Database.php");
$dato = $_POST['dato'];
//EJECUTAMOS LA CONSULTA DE BUSQUEDA
$dato1 = Database::getInstance()->getDb()->prepare("SELECT p.codigoProducto, p.nombreComercial, p.idLote, p.precioVenta, p.fechavencimiento, p.nombreSector,p.stock FROM Inventario p WHERE p.stock>0 AND  p.nombreComercial LIKE '$dato%'");
$dato1->execute();
//CREAMOS NUESTRA VISTA Y LA DEVOLVEMOS AL AJAX
?>
<table class="table table-striped" >
    <thead>
        <tr>
            <th>
                Codigo 
            </th>
            <th>
                Descripcion
            </th>
            <th>
                Stock
            </th>
            <th>
                Lote
            </th>
            <th>
                Precio Venta
            </th>
            <th>
                Fecha Vencimiento
            </th>
            <th>
                Ubicacion
            </th>
            <!--Botones-->
            <th>

            </th>

        </tr>
    </thead>
    <tbody>
        <?php
        try {
            while ($row = $dato1->fetch()) {
                $idP = $row['codigoProducto'];
                ?>
                <tr>
                    <td id = "codigo" name="codigo" > 
                        <?= $idP ?>
                    </td>                                                                
                    <td>
                        <?= $row['nombreComercial'] ?>
                    </td>
                    <td>
                        <?= $row['stock'] ?>
                    </td>

                    <td>
                        <?= $row['idLote'] ?>
                    </td>
                    <td>
                        <?= $row['precioVenta'] ?>
                    </td>
                    <td>
                        <?= $row['fechavencimiento'] ?>
                    </td>
                    <td>
                        <?= $row['nombreSector'] ?>
                    </td>
                    <!--aqui estan los botones que hacen activar las vnetanas del modal-->
                    <td>                          
                        <a href="ver_productocopy.php?id=<?= $row['codigoProducto'] ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                    </td>
                </tr>
                <?php
            }//terminacion del while
        } catch (PDOException $e) {

            echo 'Error: ' . $e;
        }
        ?>    
    </tbody>
</table>    
