<?php
require_once ('head.php');
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>            
                <strong>PRODUCTOS</strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>

    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget">
                                <form  method="post" class="form-horizontal">
                                    <div class="widget-body">

                                        <br>
                                        <center>
                                            <div class="form-inline">
                                                <div class="form-group"> 
                                                    <div class="col-lg-3">
                                                        <span class="input-icon">
                                                            <input type="text" size="40" class="form-control"  placeholder="Buscar Producto por : Codigo , Descripcion " id="bs-inventario"/>
                                                            <i class="glyphicon glyphicon-search circular blue"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                &nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;
                                            </div>
                                        </center>
                                        <br>
                                    </div>
                                    <br>
                                    <div class="widget-body">
                                        <br>

                                        <!-- TABLA DE USUARIOS-->
                                        <div class="table-responsive" id="agrega-registros">        
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Codigo 
                                                        </th>
                                                        <th>
                                                            Descripcion
                                                        </th>
                                                        <th>
                                                            Stock
                                                        </th>
                                                        <th>
                                                            Lote
                                                        </th>
                                                        <th>
                                                            Precio Venta
                                                        </th>
                                                        <th>
                                                            Fecha Vencimiento
                                                        </th>
                                                        <th>
                                                            Ubicacion
                                                        </th>
                                                        <!--Botones-->
                                                        <th>

                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    require_once '../datos/Database.php';
                                                    try {
                                                        $auxSucursal =$_SESSION['userMaster'];
                                                        $aux2=$auxSucursal['idSucursal'];
                                                        $registro = "SELECT p.codigoProducto, p.nombreComercial, p.idLote, p.precioVenta, p.fechavencimiento, p.nombreSector,p.stock FROM Inventario p WHERE p.stock>0 AND p.idSucursal='$aux2'";
                                                        $comando = Database::getInstance()->getDb()->prepare($registro);
                                                        $comando->execute();
                                                        while ($row = $comando->fetch()) {
                                                            $idP = $row['codigoProducto'];
                                                            ?>
                                                            <tr>
                                                                <td id = "codigo" name="codigo" > 
                                                                    <?= $idP ?>
                                                                </td>                                                                
                                                                <td>
                                                                    <?= $row['nombreComercial'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['stock'] ?>
                                                                </td>

                                                                <td>
                                                                    <?= $row['idLote'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['precioVenta'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['fechavencimiento'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['nombreSector'] ?>
                                                                </td>
                                                                <!--aqui estan los botones que hacen activar las vnetanas del modal-->
                                                                <td>                          
                                                                    <a href="ver_productocopy.php?id=<?= $row['codigoProducto'] ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }//terminacion del while
                                                    } catch (PDOException $e) {

                                                        echo 'Error: ' . $e;
                                                    }
                                                    ?>    
                                                </tbody>
                                            </table>
                                        </div>

                                        <!--finnn de tablaa-->

                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>
                <!------- otroooooo----->                                            


                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->





    </div>
    <!-- finnn -->          
</div>



<?php
require_once ('../header_footer/footer.php');
?>  
