<?php
require_once 'head.php';

$idUser = $_SESSION['userMaster'];
$idSucursal = $idUser['idSucursal'];
$idSesion = $_SESSION['idSesion'];
$idUsuario = $idUser['idUsuario'];

///////////  BUSCAR USUARIO ///////////
$consulta = "select * from Usuario u where idUsuario =? ";
$resul = Database::getInstance()->getDb()->prepare($consulta);
$resul->execute([$idUsuario]);
$row = $resul->fetch();
///////////  BUSCAR  NRO DE NOTA INGRESO ///////////
$nroFila = 1;
$idTransaccion = $_GET['id'];

$consulta2 = "SELECT * FROM BajadeProductos b WHERE b.idTransaccion =? ";
$resul2 = Database::getInstance()->getDb()->prepare($consulta2);
$resul2->execute([$idTransaccion]);
$row2 = $resul2->fetch();
$resumen = $row2['resumen'];
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong> DAR DE BAJA</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="widget-body ">
                        <form  method="post" class="form-horizontal" id="frm_baja"  >

                            <br>
                            <div class="form-group">

                                <label class="col-lg-2 control-label"><strong>Usuario</strong></label>
                                <div class="col-lg-5">
                                    <label class="form-control-static" ><?= $row['nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['apellido'] ?> </label>                                           
                                </div>
                            </div>   
                             <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>Glosa</strong></label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" rows="2" id="form-field-8" name="resumen" id="resumen" ><?= $resumen?></textarea>
                                </div>
                            </div>  


                            <center>
                                <h3><strong>Busqueda de Productos</strong></h3>
                            </center>
                            <br>

                            <!-- TABLA DE NOTA DE VENTA-->
                            <div class="table-responsive">
                                <table id="grilla" class="table table-striped ">
                                    <thead>
                                        <tr>
                                            <th><strong>Codigo</strong></th>
                                            <th><strong>Descripcion</strong></th>
                                            <th><strong>Precio Venta</strong></th>                            
                                            <th><strong>Lote</strong></th>                                            
                                            <th><strong>Fecha Vencimiento</strong></th>
                                            <th><strong>Ubicacion</strong></th>
                                            <th><strong>Cantidad</strong></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        require_once '../datos/Database.php';
                                        $registro = "SELECT p.codigo ,p.nombreComercial , d.precioVenta , d.cantidad , l.nroLote , l.fechaVencimiento , s.nombreSector "
                                                . "FROM Transaccion t , Usuario u , DetalleTransaccion d , Producto p , Existencias e , Lote l , Ubicacion ubi , Sectores s"
                                                . " WHERE t.idTransaccion = d.idTransaccion and t.idTransaccion = '$idTransaccion' "
                                                . " and u.idUsuario = t.idUsuario and p.codigoProducto = d.codigoProducto"
                                                . " and e.idExistencia = d.idExistencia and l.idLote = e.idLote"
                                                . " and e.idUbicacion = ubi.idUbicacion and ubi.idSector = s.idSector";
                                        
                                            $comando = Database::getInstance()->getDb()->prepare($registro);
                                            $comando->execute();
                                            while ($row1 = $comando->fetch()) {
                                                ?> <tr>
                                                    <td><?= $row1['codigo']?></td>
                                                    <td><?= $row1['nombreComercial'] ?></td>                                       
                                                    <td><?= $row1['precioVenta'] ?></td>
                                                    <td><?= $row1['nroLote'] ?></td>
                                                    <td><?= $row1['fechaVencimiento'] ?></td>
                                                    <td><?= $row1['nombreSector'] ?></td>
                                                    <td><?= $row1['cantidad'] ?></td> 
                                                </tr>
                                                <?php
                                                $nroFila = $nroFila + 1;
                                            }//terminacion del while
                                       
                                        ?>    
                                    </tbody>


                                </table>  

                            </div>
                            <br>
                            <br>
                            
                            <div class="form-inline" >
                               
                                    <center>
                                    <div class="input-group">
                                        <div class="col-lg-offset-4 col-lg-4"> 
                                            <a type="button" href="index.php" class="btn btn-danger"  > Aceptar</a>
                                        </div>
                                    </div>
                                </center>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once ('../header_footer/footer.php');
?> 
