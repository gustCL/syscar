<?php
require_once 'head.php';

$idUser = $_SESSION['userMaster'];
$idSucursal = $idUser['idSucursal'];
$idSesion = $_SESSION['idSesion'];
$idUsuario = $idUser['idUsuario'];

///////////  BUSCAR USUARIO ///////////
$consulta = "select * from Usuario u where idUsuario =? ";
$resul = Database::getInstance()->getDb()->prepare($consulta);
$resul->execute([$idUsuario]);
$row = $resul->fetch();


?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong> DAR DE BAJA</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="widget-body ">
                        <form  method="post" class="form-horizontal" action="javascript:addBaja();" id="frm_baja"  >

                            <br>
                            <div class="form-group">
                                
                                <label class="col-lg-2 control-label"><strong>Usuario</strong></label>
                                <div class="col-lg-5">
                                    <label class="form-control-static" ><?= $row['nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['apellido'] ?> </label>                                           
                                </div>
                                
                            </div>            
                             <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>Glosa</strong></label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" rows="2" id="form-field-8" name="resumen" id="resumen" ></textarea>
                                </div>
                            </div>  

                            <center>
                                <h3><strong>Busqueda de Productos</strong></h3>
                            </center>
                            <br>

                            <!-- TABLA DE NOTA DE VENTA-->
                            <div class="table-responsive">
                                <table id="grilla" class="table table-striped ">
                                    <thead>
                                        <tr>
                                            <th><strong>Codigo</strong></th>
                                            <th><strong>Descripcion</strong></th>
                                            <th><strong>Precio Venta</strong></th>
                                            <th><strong>Stock</strong></th>
                                            <th><strong>Lote</strong></th>                                            
                                            <th><strong>Fecha Vencimiento</strong></th>
                                            <th><strong>Ubicacion</strong></th>
                                            <th><strong>Cantidad</strong></th>
                                            <!--Botones-->
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                        <!--CONTADOR DE FILAS DE LA TABLA -->
                                    <input type="text" id="conta" name="conta" value="1" hidden/><!--CONTADOR DE FILAS DE LA TABLA -->
                                    
                                    <td><input size="10" type="text" class="form-control" id="codigo" name="codigo"  style="text-align: right" value="-" readonly required/><input type="text" id="codi" name="codi" hidden/><input type="text" id="idProd" name="idProd"  hidden/></td>
                                    <td><input size="80" type="text" class="form-control" id="descripcion" name="descripcion"  value=""  required/>
                                        <script>

                                            $('#descripcion').autocomplete({
                                                source: function (request, response) {
                                                    $.ajax({
                                                        url: 'autocompletado.php',
                                                        dataType: "json",
                                                        data: {
                                                            name_startsWith: request.term,
                                                            type: 'Detalle'
                                                        },
                                                        success: function (data) {
                                                            response($.map(data, function (item, id) {
                                                                return {
                                                                    label: id,
                                                                    value: id,
                                                                    id: item

                                                                };
                                                            }));
                                                        }
                                                    });
                                                },
                                                select: function (event, ui) {
                                                    var idpr = ui.item.id;
//                                                    alert(idpr);
                                                    var url = 'busqueda_detalle_baja.php';
                                                    $.ajax({
                                                        type: 'POST', url: url,
                                                        data: 'id=' + idpr,
                                                        success: function (valores) {
                                                            var datos = eval(valores);
                                                            $('#codigo').val(datos[0]);
                                                            $('#descripcion').val(datos[2]);
                                                            $('#precioV').val(datos[1]);
                                                            $('#stock').val(datos[4]);
                                                            $('#stock_aux').val(datos[4]);
                                                            $('#idProd').val(datos[5]);
                                                            $('#codi').val(datos[5]);
                                                            $('#fechaven').val(datos[6]);
                                                            $('#lote').val(datos[7]);
                                                            $('#idExist').val(datos[8]);
                                                            $('#ubicacion').val(datos[3]);
                                                            $('#cantidad').focus();
                                                            //                                                    
                                                        }
                                                    });

                                                }
                                            });
                                        </script>                               
                                    </td>                                       
                                    <td><input size="20" name="precioV" type="text" id="precioV" class="form-control" style="text-align: right"   readonly/>  </td>  
                                    <td><input size="20" name="stock" type="text" id="stock" class="form-control" style="text-align: right" readonly/><input name="stock_aux" type="text" id="stock_aux" hidden/> </td>
                                    <td><input size="10" name="lote" type="text" id="lote" class="form-control" style="text-align: right" readonly /><input name="idExist" id="idExist" type="text" hidden/></td>
                                    <td><input size="20" class="form-control" id="fechaven" name="fechaven" type="date" readonly/> </td>
                                    <td><input size="20" type="text" id="ubicacion" name="ubicacion" class="form-control"   readonly/></td>
                                    <td><input size="10"  name="cantidad" type="text" id="cantidad" class="form-control" style="text-align: right"  onkeyup=" verific_Stock();" required/> </td>
                                    
                                    <td>                                          
                                        <strong><input   name="agregar" id="agregar" value="+" class="btn-primary"  type="submit"/></strong>
                                    </td>                                     
                                    </tfoot>
                                </table>  

                            </div>
                            <div class="form-inline" >
                                <center>

                                    <br/>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-primary"  id="guardar" name="guardar" onclick="registrarDetalle_baja();" value="Guardar">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-success"  id="imprimir" name="imprimir" value="Imprimir">
                                            </div>
                                        </div>                                        
                                    </div>
                                </center> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once ('../header_footer/footer.php');
?> 