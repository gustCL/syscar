<?php
session_start();
require_once '../datos/Database.php';
$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];
$idSesion = $_SESSION['idSesion'];
$idUsuario = $id_usu['idUsuario'];
$resumen = $_POST['resumen'];
$cant = $_POST['contas'];


/// datos del sistema
ini_set("date.timezone", "America/La_Paz");
$fecha_sis = date("d-m-Y");
$hora_sis = date("H:i:s");
///////////////   SE LE DA EL ID DE TIPOTRANS NRO 3 ESTE ES EL QUE 
//HEMOS DEFINIDO PARA DAR BAJA A LOS PRODUCTOS

$Tipotrans = "3";
///////////////  SE INSERTA LA TRANSACCION  DE BAJA DE PRODUCTO
$consulta2 = "INSERT INTO Transaccion(tipoTrans, idTransaccion, fecha, hora, idInicioSesion, idUsuario, estado) "
                              . "VALUES (?,0,?,?,?,?,1)";
$dato2 = Database::getInstance()->getDb()->prepare($consulta2);
$dato2->execute([$Tipotrans,$fecha_sis,$hora_sis,$idSesion ,$idUsuario]);
//////////// SE SACA EL ID DE LA TRASACCION REALIZADA 
$CONSULTA55 = "SELECT Max(idTransaccion) as idTransacc FROM Transaccion  ";
$DATO55 = Database::getInstance()->getDb()->prepare($CONSULTA55);
$DATO55->execute();
$count = $DATO55->fetch(PDO::FETCH_ASSOC);
$idTransaccion = $count['idTransacc'];

/////////////   SE INSERTA EN LA TABLA BAJAPRODUCTOS ///////////////////////
$CONSULTA6 ="INSERT INTO BajadeProductos(idBajaProducto, idTransaccion, resumen) "
                . "VALUES (0,?,?)";
$DATO6 = Database::getInstance()->getDb()->prepare($CONSULTA6);
$DATO6->execute([$idTransaccion,$resumen]);

for ($index = 1; $index <= $cant; $index++) {
$ubicacion = $_POST['ubicacionT'.$index];
$codigoProd = $_POST['codig'.$index];
$descripcion = $_POST['descripcionT'.$index];
$PrecioV = $_POST['precioVT'.$index];
$stock = $_POST['stockT'.$index];
//$aux = $_POST['stock_auxT'.$index];
$nrolote = $_POST['loteT'.$index];
$cantidad = $_POST['cantidadT'.$index];
$fechaV = $_POST['fechavenT'.$index];
$idExistencia = $_POST['idExistT'.$index];
/////////////////  INSERTA EL DETALLE DE LA TRANSACCION ///////////////////////////////////////////////////////
$consulta3 = "INSERT INTO DetalleTransaccion( idTransaccion, idExistencia, codigoProducto, cantidad, precioVenta) "
                                    . "VALUES (?,?,?,?,?)";
$dato3 = Database::getInstance()->getDb()->prepare($consulta3);
$dato3->execute([ $idTransaccion,$idExistencia,$codigoProd , $cantidad ,$PrecioV]);

/////////////////  EN ESTA CONSULTA SE SELECCIONA EL IDLOTE DEL PRODUCTO QUE SE VA DAR DE BAJA///////////////////////////////////////////////////////
$consulta4 = "SELECT l.idLote FROM Lote l WHERE l.codigoProducto = ? and l.fechaVencimiento = ? and l.nroLote = ? and l.estado = 1 ";
$dato4 = Database::getInstance()->getDb()->prepare($consulta4);
$dato4->execute([$codigoProd , $fechaV ,$nrolote ]);
$row4 = $dato4->fetch();
$idLote = $row4['idLote'];

if( $stock == '0' ){
$consulta3 = "UPDATE Existencias e SET e.stock = 0 , e.terminado = 0 WHERE  e.idLote = ?";
$dato3 = Database::getInstance()->getDb()->prepare($consulta3);
$dato3->execute([$idLote]);  
}  else {
$consulta4 = "UPDATE Existencias e SET e.stock = ?  WHERE  e.idLote = ?";
$dato4 = Database::getInstance()->getDb()->prepare($consulta4);
$dato4->execute([$stock ,$idLote]);   
}

}


?>