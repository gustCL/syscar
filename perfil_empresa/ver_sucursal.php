<?php
	require_once '../datos/Database.php';

	$id = trim($_POST['id']);

	// Obtener los campos de la sucursal
	$query = "SELECT * FROM Sucursal WHERE idSucursal = :id_sucursal";
	$cmd = Database::getInstance()->getDb()->prepare($query);
    $cmd->bindParam(':id_sucursal', $id);
 	$cmd->execute();
 	$result = $cmd->fetch();

	$datos = array(
                0 => $result['idSucursal'],
                1 => $result['nombreSucur'],
				2 => $result['direccion'],
				3 => $result['telefono'],
				4 => $result['ciudad']
            );

    echo json_encode($datos);
    exit();
?>