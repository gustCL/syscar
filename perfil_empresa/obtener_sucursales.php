<?php
    require_once '../datos/Database.php';
?>

<script src="../extras/sortable/js/sortable.min.js"></script>
<center><h3><strong>LISTA DE SUCURSALES</strong></h3></center>
<br>
<table class="table sortable-theme-bootstrap table-hover table-bordered" data-sortable>
    <thead>
    <tr>
        <th><strong>Nro.</strong></th>
        <th><strong>Nombre de la Sucursal</strong></th>
        <th><strong>Dirección</strong></th>
        <th><strong>Teléfono</strong></th>
        <th><strong>Ubicación</strong></th>
        <th><strong>Opciones</strong></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sucursales = $db->Sucursal()->where('estado = ?','1');
    $nro = 0;
    foreach ($sucursales as $sucursal) : $nro++;    ?>
        <tr>
            <td><?= $nro; ?></td>
            <td><?= $sucursal['nombreSucur']; ?></td>
            <td><?= $sucursal['direccion']; ?></td>
            <td><?= $sucursal['telefono']; ?></td>
            <td><?= $sucursal['ciudad']; ?></td>
            <td>
                <a href="javascript: editarSucursal('<?= $sucursal['idSucursal']; ?>');"
                   class="btn btn-default btn-xs purple"><i class="fa fa-edit"></i>Editar</a>
                <a href="javascript: eliminarSucursal('<?= $sucursal['idSucursal']; ?>','<?= $sucursal['nombreSucur'];?>');"
                   class="btn btn-default btn-xs black"><i class="fa fa-trash-o"></i>Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>