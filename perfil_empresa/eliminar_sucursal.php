<?php

    require_once '../datos/Database.php';
    //require_once '../extras/notorm/notorm.lib.php';

    $id_sucursal = $_POST['id'];

    try {

        $query = "UPDATE Sucursal set estado = 0 WHERE idSucursal = :id_sucursal";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_sucursal', $id_sucursal);
        $cmd->execute();

        echo 'exito';
        exit();

    } catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }
?>