<?php
    require_once '../datos/Database.php';

    // Obtener datos del formulario
    $nombre_sucursal = $_POST['nombre-sucursal'];
    $direccion_sucursal = $_POST['direccion-sucursal'];
    $telefono_sucursal = $_POST['telefono-sucursal'];
    $ubicacion_sucursal = $_POST['ubicacion-sucursal'];

    //Obtener fecha y hora
    // Definir la zona horario
    ini_set("date.timezone", "America/La_Paz");
    $fecha_registro = date('d-m-Y');
    $hora_registro = date("H:i:s");

try {
    $sucursal = $db->Sucursal();
    $data = array(
        "idEmpresa" => '1',
        "nombreSucur" => $nombre_sucursal,
        "direccion" => $direccion_sucursal,
        "telefono" => $telefono_sucursal,
        "ciudad" => $ubicacion_sucursal,
        "fechaRegistro" => $fecha_registro,
        "horaRegistro" => $hora_registro
    );
    if ($sucursal->insert($data)) {
        echo 'exito';
        exit();
    }

}   catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
}
?>