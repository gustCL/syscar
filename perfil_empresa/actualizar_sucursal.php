<?php

    require_once '../datos/Database.php';

    $id_sucursal = htmlspecialchars($_POST['id-sucursal']);
    $nombre_sucursal = htmlspecialchars($_POST['edit-nombre-sucursal']);
    $direccion_sucursal = htmlspecialchars($_POST['edit-direccion-sucursal']);
    $telefono_sucursal = htmlspecialchars($_POST['edit-telefono-sucursal']);
    $ubicacion_sucursal = htmlspecialchars($_POST['edit-ubicacion-sucursal']);

    try {
        $query = "UPDATE Sucursal
                  SET nombreSucur = :nombre,
                      direccion = :direccion,
                      telefono = :telefono,
                      ciudad = :ubicacion
                  WHERE idSucursal = :id_sucursal";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':nombre', $nombre_sucursal);
        $cmd->bindParam(':direccion', $direccion_sucursal);
        $cmd->bindParam(':telefono', $telefono_sucursal);
        $cmd->bindParam(':ubicacion', $ubicacion_sucursal);
        $cmd->bindParam(':id_sucursal', $id_sucursal);
        $cmd->execute();

        echo 'exito';
        exit();

    } catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

?>