$(document).ready(function(){
    obtenerSucursales();
});

function guardarDatosEmpresa() {
    var frm = $("#frm-info-empresa").serialize();
    $.ajax({
        type: 'POST',
        url: 'guardar_datos_empresa.php',
        data: frm,
        success: swal("Empresa Actualizada","Datos de empresa actualizados correctamente","success")
    });
}

// Registrar Nueva Sucursal
function registrarSucursal() {
    var frm = $("#frm-registrar-sucursal").serialize();
    $.ajax({
        type: 'POST',
        url: 'registrar_sucursal.php',
        data: frm,
        success: function(result){
            if (result == 'error') {
                alertify.error("OCURRIO UN ERROR. INTENTE NUEVAMAEBNTE.");
            } else {
                alertify.success("SUCURSAL REGISTRADA SATISFACTORIAMENTE.");
                $("#div-sucursales").load('obtener_sucursales.php');
            }
        }
    });
}

// Eliminar sucursal
function eliminarSucursal(id, sucursal) {
    swal({
            title: "ELIMINAR SUCURSAL",
            text: "Está a punto de eliminar la sucursal "+sucursal+". ¿Desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: 'eliminar_sucursal.php',
                    data: 'id=' + id,
                    success: function (result) {
                        if (result == 'exito') {
                            swal("Sucursal eliminada!", "La sucursal fué removida de la base de datos", "success");
                            $('#div-sucursales').load('obtener_sucursales.php');
                        } else {
                            alertify.error('Ocurrió un error. Intente nuevamente.');
                        }
                    }
                });
            } else {
                swal("Operación cancelada", "Sucursal no fué eliminada", "error");
            }
        }
    );
}

// Editar Sucursal
function editarSucursal(id) {
    $('#frm-editar-sucursal')[0].reset();
    $.ajax({
        type:'POST',
        url: 'ver_sucursal.php',
        data:'id='+id,
        success: function(valores){
            var datos = eval(valores);
            $('#id-sucursal').val(datos[0]);
            $('#edit-nombre-sucursal').val(datos[1]);
            $('#edit-direccion-sucursal').val(datos[2]);
            $('#edit-telefono-sucursal').val(datos[3]);
            $('#edit-ubicacion-sucursal').val(datos[4]);
            $('#div-editar-sucursal').modal({
                show:true,
                backdrop:'static'
            });
            return false;
        }
    });
    return false;
}

function actualizarSucursal() {
    var frm = $("#frm-editar-sucursal").serialize();
    $.ajax({
        type: 'POST',
        url: 'actualizar_sucursal.php',
        data: frm,
        success: function(result) {
            if (result == 'exito') {
                $('#btn-cerrar-modal').click();
                alertify.success('Sucursal Actualizada');
                $('#div-sucursales').load('obtener_sucursales.php');
            }
        }
    });
}

function obtenerSucursales() {
    $('#div-sucursales').load('obtener_sucursales.php');
}

