<?php
    require_once 'head.php';
?>
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>PERFIL DE LA EMPRESA</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>

    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="widget-body">
                <div>
                    <h2><strong><center>INFORMACIÓN DE EMPRESA</center></strong></h2>
                    <br>
                    <form id="frm-info-empresa" action="javascript: guardarDatosEmpresa();" class="form-horizontal">
                        <div class="form-group">
                            <?php
//                            $conEmpresa = "SELECT * FROM Empresa";
//                            $comEmpresa = Database::getInstance()->getDb()->prepare($conEmpresa);
//                            $comEmpresa->execute();
//                            $emp = $comEmpresa->fetch();
                                $emp = $db->Empresa()->fetch();
                            ?>
                            <label class="col-lg-3 control-label lead">Nombre de la Empresa</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="nombre-empresa" id="nombre-empresa"
                                       value='<?= $emp['nombreEmpresa'] ?>' />
                            </div>
                            <label class="col-lg-1 control-label lead">NIT</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="nit" id="nit"
                                       value="<?= $emp['nit'] ?>" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label lead">Actividad económica</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="actividad-empresa" id="actividad-empresa"
                                       value="<?= $emp['actividadEco'] ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <center>
                                <input type="submit" class="btn btn-primary" id="btn-guardar" value="Guardar">
                                <input type="button" value="Limpiar" class="btn btn-azure" onclick="$('#frm-info-empresa').trigger('reset');"/>
                            </center>
                        </div>
                    </form>
                </div>
            </div>
            <br>

            <div class="widget-body">
                <h3><center><strong>REGISTRAR SUCURSAL</strong></center></h3>
                <br>
                <form id="frm-registrar-sucursal" name="frm-registrar-sucursal" class="form-horizontal" action="javascript: registrarSucursal();">
                    <div class="form-group">
                        <label class="col-lg-3 control-label lead">Nombre de Sucursal</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" name="nombre-sucursal" id="nombre-sucursal" required>
                        </div>
                        <label class="col-lg-1 control-label lead">Dirección</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" name="direccion-sucursal" id="direccion-sucursal">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label lead">Teléfono</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" name="telefono-sucursal" id="telefono-sucursal">
                        </div>
                        <label class="col-lg-1 control-label lead">Ubicación</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" name="ubicacion-sucursal" id="ubicacion-sucursal">
                        </div>
                    </div>
                    <div class="form-group">
                        <center>
                            <input type="submit" class="btn btn-primary" id="btn-guardar" value="Guardar">
                            <input type="button" value="Limpiar" class="btn btn-azure" onclick="$('#frm-registrar-sucursal').trigger('reset');"/>
                        </center>
                    </div>
                </form>
            </div>

            <br>
            <div class="widget-body">
                <div class="table-responsive" id="div-sucursales">

                </div>
            </div>

            <div class="modal fade" id="div-editar-sucursal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"><b>Datos de Sucursal</b></h4>
                        </div>
                        <form action="javascript: actualizarSucursal();" id="frm-editar-sucursal" name="frm-editar-sucursal" class="formulario">
                            <input id="id-sucursal" name="id-sucursal" hidden>
                            <div class="modal-body">
                                <table style="border-collapse:separate; border-spacing: 5px; width: 100%;">
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-right">Nombre</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                       id="edit-nombre-sucursal"
                                                       name="edit-nombre-sucursal" required>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-right">Dirección</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                       id="edit-direccion-sucursal" name="edit-direccion-sucursal" required>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-right"
                                                       for="ci">Teléfono</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input class="form-control" type="text" id="edit-telefono-sucursal"
                                                       onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                                       name="edit-telefono-sucursal" required>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-right">Ubicación</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input class="form-control" type="text" id="edit-ubicacion-sucursal"
                                                       name="edit-ubicacion-sucursal" required>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <div class="form-inline">
                                    <center>
                                        <input type="submit" class="btn btn-primary" value="Guardar">
                                        <button id="btn-cerrar-modal" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">
                                            Cancelar
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once ('../header_footer/footer.php'); ?>