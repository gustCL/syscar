<?php
    require_once '../datos/Database.php';

    // Obtener datos del formulario
    $nombre_empresa = $_POST['nombre-empresa'];
    $actividad_economica = $_POST['actividad-empresa'];

    try {
        $query = "UPDATE Empresa
                  SET nombreEmpresa = :nombre_empresa, actividadEco = :actividad_economica
                  WHERE idEmpresa = 1";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':nombre_empresa', $nombre_empresa);
        $cmd->bindParam(':actividad_economica', $actividad_economica);
        $cmd->execute();

        echo 'exito';
        exit();
    }   catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }
?>