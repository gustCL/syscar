<?php

require_once("../datos/Database.php");
session_start();
$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];
$idSesion = $_SESSION['idSesion'];

$idNotaIngreso = $_POST['idNota'];
$idalma = $_POST['idAlm'];
$nombreProveedor = $_POST['nombreProve'];
$cant = $_POST['contas'];
$SubTotal = $_POST['subTotal'];
$descuento1 = $_POST['descuento1'];
$descuento2 = $_POST['descuento2'];
$descuento3 = $_POST['descuento3'];

$total = $_POST['total'];
$contado = $total;

ini_set("date.timezone", "America/La_Paz");
$fechA = date("d-m-Y");
$horA = date("H:i:s");


///////// ACTUALIZAR LA TABLA FACTURA INGRESO ////////////////////////
$CONSULTA1 = "UPDATE FacturaIngreso  SET idInicioSesion ='$idSesion', fecha='$fechA' , hora='$horA' , montoSubTotal='$SubTotal' , descuento1='$descuento1' , descuento2 = '$descuento2' , descuento3 ='$descuento3', montoTotal='$total'  WHERE idNotaIngreso='$idNotaIngreso' ";
$DATO1 = Database::getInstance()->getDb()->prepare($CONSULTA1);
$DATO1->execute();



////////// ACTUALIZAR LA TABLA REGISTROPAGOCOMPRA///////////////////////
$CONSULTA5 = "UPDATE  RegistroPagoCompra SET montoContado='$contado' WHERE idNotaIngreso='$idNotaIngreso' ";
$DATO5 = Database::getInstance()->getDb()->prepare($CONSULTA5);
$DATO5->execute();

////////// SE INICIA A DAR DE BAJA A  LOS PRDUCTOS ////////////
$CONSULTA20 = "select l.idLote "
        . "from FacturaIngreso f , Lote l , Producto p   "
        . "where  f.idNotaIngreso = l.idNotaIngreso and "
        . "l.codigoProducto = p.codigoProducto and p.estado = 1 and "
        . "l.estado = 1 and f.estado = 1 and "
        . " f.idNotaIngreso = '$idNotaIngreso' ORDER BY f.idNotaIngreso DESC";
$DATO20 = Database::getInstance()->getDb()->prepare($CONSULTA20);
$DATO20->execute();
while ($ROW20 = $DATO20->fetch()) {
    $idLote = $ROW20['idLote'];
    $CONSULTA = "UPDATE Lote l SET l.estado = 0 WHERE l.idLote = ? ";
    $DATO = Database::getInstance()->getDb()->prepare($CONSULTA);
    $DATO->execute([$idLote]);


    $cons2 = "select e.idExistencia "
            . "from  Ubicacion u , Sectores s , Almacen a , Existencias e "
            . "where a.idAlmacen ='" . $idalma . "'  and u.idAlmacen = a.idAlmacen and u.idSector = s.idSector"
            . " and e.idUbicacion = u.idUbicacion and e.idLote = '" . $idLote . "'";
    $datos2 = Database::getInstance()->getDb()->prepare($cons2);
    $datos2->execute();
    $row3 = $datos2->fetch();

    $idExistencia = $row3['idExistencia'];

    $CONSULTA2 = "UPDATE Existencias e SET e.stock = 0 , e.terminado = 0   WHERE e.idExistencia = ? ";
    $DATO2 = Database::getInstance()->getDb()->prepare($CONSULTA2);
    $DATO2->execute([$idExistencia]);
}

for ($index = 1; $index <= $cant; $index++) {


    $codigoP = $_POST['codig'. $index];
    $PrecioC = $_POST['precioCT' . $index];
    $PrecioV = $_POST['precioVT' . $index];
    $margen = $_POST['margenT' . $index];
    $cantidad = $_POST['cantidadT' . $index];
    $nroLote = $_POST['nroloteT' . $index];
    $sectorTot = $_POST['sectorT' . $index];
    $fechaT = $_POST['fechaT' . $index];
    $monto = $_POST['montoT' . $index];


    //////////INSERTA LA TABLA LOTE /////////////////////////////////
    $CONSULTA2 = "INSERT INTO Lote( idLote,nroLote,codigoProducto,idNotaIngreso,precioCosto,precioVenta,margen,fechaVencimiento,cantidad,observaciones,estado)"
            . " VALUES ('0',?,?,?,?,?,?,?,?,'nada','1')";
    $DATO2 = Database::getInstance()->getDb()->prepare($CONSULTA2);
    $DATO2->execute([$nroLote, $codigoP, $idNotaIngreso, $PrecioC, $PrecioV, $margen, $fechaT, $cantidad]);
    //////////// OBTENER EL idLote ///////////////////////////////
    $CONSULTA6 = "SELECT Max(idLote) as idLote FROM Lote  ";
    $DATO6 = Database::getInstance()->getDb()->prepare($CONSULTA6);
    $DATO6->execute();
    $count = $DATO6->fetch(PDO::FETCH_ASSOC);
    $idlote1 = $count['idLote'];

    //////// INSERTANDO Y ACTUALIZANDO LAS UBICACIONES  //////////////////////////
    $CONSULTA15 = "SELECT idSector FROM Sectores where nombreSector ='$sectorTot'  ";
    $DATO15 = Database::getInstance()->getDb()->prepare($CONSULTA15);
    $DATO15->execute();
    $row15 = $DATO15->fetch();
    $idS = $row15['idSector'];



    $CONSULTA20 = "SELECT idUbicacion FROM Ubicacion where idAlmacen='$idalma' and idSector ='$idS' and codigoProducto='$codigoP' ";
    $DATO10 = Database::getInstance()->getDb()->prepare($CONSULTA20);
    $DATO10->execute();
    $row10 = $DATO10->fetch();
    $idUbicacion = $row10['idUbicacion'];

    $CONSULTA9 = "INSERT INTO Existencias(idExistencia,idLote,idUbicacion,stock,terminado,estado)VALUES "
            . "('0','$idlote1','$idUbicacion','$cantidad','1','1') ";
    $DATO9 = Database::getInstance()->getDb()->prepare($CONSULTA9);
    $DATO9->execute();



    //////// ACTUALIZAR LA TABLA PRECIOSUCURSAL //////////////////////////
    $CONSULTA4 = "UPDATE  PrecioSucursal SET precioCosto='$PrecioC' , precioVenta='$PrecioV' , fechaActual='$fechA' , horaActual='$horA' WHERE codigoProducto='$codigoP' and idSucursal='$idSucursal' ";
    $DATO4 = Database::getInstance()->getDb()->prepare($CONSULTA4);
    $DATO4->execute();
}
?>