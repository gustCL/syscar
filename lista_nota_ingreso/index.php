<?php
require_once 'head.php';
$idSucur = $_SESSION['userMaster'];
$idSucursal = $idSucur['idSucursal'];
$nroFila = 1;
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong> LISTA NOTA DE INGRESO</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>

    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">

                    <form method="post" class="form-horizontal">
                        <div class="widget-body ">
                            <br>
                            <center>
                                <div class="form-inline">

                                    <div class="input-group"> 
                                        <div class="col-lg-3">
                                            <span class="input-icon">
                                                <input id="buscar-nota-ingreso" type="text" size="25" class="form-control"  placeholder="Busca: Nro Nota Ingreso " />
                                                <i class="glyphicon glyphicon-search circular blue"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <input class="form-control date-picker" size="25" id="fechaN" name="fechaN" type="text" placeholder="Fecha" data-date-format="dd-mm-yyyy"/>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>

                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="input-group">
                                        <a  type="button" id="btnBuscar"  class="btn btn-yellow"  >Buscar</a>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="input-group">
                                        <a type="button" href="javascript:seleccion_alma();"  class="btn btn-primary"  >Nueva Nota Ingreso</a>
                                    </div>
                                </div>
                            </center>
                            <br>
                        </div>
                        <br>
                        <div class="widget-body">
                            <br>                                                             
                            <div class="table-responsive" id="agrega-registros">
                                <table class="table table-striped " >
                                    <thead>

                                        <tr>
                                            <th>
                                                Nro.
                                            </th>

                                            <th>
                                                GLOSA
                                            </th>
                                            <th>
                                                FECHA
                                            </th>
                                            <th>
                                                MONTO TOTAL
                                            </th>
                                            <th>

                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        require_once '../datos/Database.php';
                                        $registro = "select * "
                                                . "from FacturaIngreso f  "
                                                . "where f.estado = 1 and f.idTipoIngreso = 3  ORDER BY f.idNotaIngreso DESC";
                                        try {
                                            $comando = Database::getInstance()->getDb()->prepare($registro);
                                            $comando->execute();
                                            while ($row = $comando->fetch()) {

                                                $NroN = $row['idNotaIngreso'];
                                                ?><tr>

                                                    <td>
                                                        <?= $row['idNotaIngreso'] ?>
                                                    </td>                                                          
                                                    <td>
                                                        <?= $row['glosa'] ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['fecha'] ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['montoTotal'] ?>
                                                    </td>
                                                    <td>                          
                                                        <a href="nota_vista.php?id=<?= $NroN ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                                                        <a  href="editar_nota_ingreso.php?id=<?= $NroN ?>"  class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                                                        <a href="javascript:eliminarCliente('<?= $NroN ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>

                                                    </td>
                                                </tr>
                                                <?php
                                                $nroFila = $nroFila + 1;
                                            }//terminacion del while
                                        } catch (PDOException $e) {

                                            echo 'Error: ' . $e;
                                        }
                                        ?>    
                                    </tbody>
                                </table>   

                            </div>
                            <br>



                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!--///////////////////////////////// MODAL PARA ELEJIR ALMACEN /////////////////////-->

<div class="modal fade" id="selec-almacen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><center><b>Inicio para la nota Ingreso</b></center></h4>
            </div>

            <!--              aqui agregamos los registros-->

            <form id="formularioC">
                <div class="modal-body">


                    <div class="row" >
                        <div class="col-lg-12 col-sm-12 col-xs-12" >

                            <div class="form-inline" >   
                                <center>
                                    <div class="form-group">
                                        <label ><strong>Seleccione el almacen</strong></label>

                                        <br>
                                        <br>
                                        <div class="col-lg-3">
                                            <?php
                                            /* Aqui se cargan los almacenes exitentes */
                                            require_once("../datos/Database.php");
                                            $datos = Database::getInstance()->getDb()->prepare("select * from Almacen where idSucursal ='$idSucursal' ");
                                            $datos->execute();
                                            ?>
                                            <select  name="almac" id="almac"   data-bv-field="country" >
                                                <?php while ($row = $datos->fetch()) { ?>               
                                                    <option value="<?= $row['idAlmacen'] ?>"  ><?= $row['nombreAlmacen'] ?></option>
                                                <?php } ?>
                                            </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>
                                        </div>

                                    </div>
                                </center>

                            </div>

                            <!--BOTONES-->
                            <br>  
                            <center>
                                <footer>
                                    <div class="form-inline" >
                                        <div class="form-group">
                                            <button onclick=" return pasar_registrar()"  class="btn btn-primary" data-dismiss="modal" aria-hidden="true"  >Aceptar</button>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <button  class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cerrar</button>
                                        </div>

                                    </div>
                                </footer>
                            </center>
                        </div>
                    </div>


                </div>


            </form>
        </div>
    </div>
</div>



<?php
require_once ('../header_footer/footer.php');
?> 