<?php
require_once 'head.php';

$idUser = $_SESSION['userMaster'];
$idSucursal = $idUser['idSucursal'];
$idSesion = $_SESSION['idSesion'];
$idUsuario = $idUser['idUsuario'];
//$idalma = $_GET['id'];
$id = $_GET['id'];
////////////////////////////// SE HACE LA CONSULTA PARA OBTENER LOS DATOS  DE GLOSA , NRONOTAINGRESO 
$consultas = "select * from FacturaIngreso f , Proveedor p  "
        . "where f.idProveedor = p.idProveedor and p.estado = 1 and f.estado =1 and "
        . " f.idNotaIngreso = ?   ORDER BY f.idNotaIngreso DESC  ";
$resultado = Database::getInstance()->getDb()->prepare($consultas);
$resultado->execute([$id]);
$row11 = $resultado->fetch();

$desc1 =$row11['descuento1'];
$desc2 =$row11['descuento2'];
$desc3 =$row11['descuento3'];
///////////  BUSCAR USUARIO ///////////
$consulta = "select * from Usuario u where idUsuario =? ";
$resul = Database::getInstance()->getDb()->prepare($consulta);
$resul->execute([$idUsuario]);
$row = $resul->fetch();
///////////  BUSCAR ALMACEN ///////////
$consulta2 = "select u.idAlmacen "
        . "from FacturaIngreso f , Proveedor p , Ubicacion u , Existencias e , Lote l 
            where f.idProveedor = p.idProveedor and p.estado = 1 and f.estado =1 and 
            f.idNotaIngreso = ? and f.idNotaIngreso = l.idNotaIngreso and l.idLote = e.idLote 
            and e.idUbicacion = u.idUbicacion group by u.idAlmacen";
$resultado2 = Database::getInstance()->getDb()->prepare($consulta2);
$resultado2->execute([$id]);
$row2 = $resultado2->fetch();
$idalma = $row2['idAlmacen'];
$contador = 1;
$AcuSubTotal =0;
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>NOTA DE INGRESO</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="widget-body ">
                        <form  method="post" class="form-horizontal" action="" id="frm_nota"  >

                            <br>
                            <input  name="idNotaa" id="idNotaa" value="<?= $id ?>"  hidden/>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>Nro. Nota Ingreso </strong></label>
                                <div class="col-lg-3">
                                    <label name="idNota" id="idNota"><?= $id ?></label>                               
                                </div>
                                <label class="col-lg-2 control-label"><strong>Usuario</strong></label>
                                <div class="col-lg-5">
                                    <label class="form-control-static" ><?= $row['nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['apellido'] ?> </label>                                           
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label"><strong>Proveedor</strong></label>
                                <div class="col-lg-4">
                                    <input size="50" type="text" class="form-control" name="nombreProve" id="nombreProve" value="<?= $row11['nombreProve'] ?>"  placeholder="Nombre Proveedor"/>
                                    <script>

                                        $('#nombreProve').autocomplete({
                                            source: function (request, response) {
                                                $.ajax({
                                                    url: 'autocompletado.php',
                                                    dataType: "json",
                                                    data: {
                                                        name_startsWith: request.term,
                                                        type: 'Nombre'
                                                    },
                                                    success: function (data) {
                                                        response($.map(data, function (item, id) {
                                                            return {
                                                                label: id,
                                                                value: id,
                                                                id: item

                                                            };
                                                        }));
                                                    }
                                                });
                                            },
                                            select: function (event, ui) {
                                                var idpr = ui.item.id;
                                                // alert(idpr);
                                                $('#idProvee').val(idpr);

                                            }
                                        });
                                    </script>                                                
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>Glosa</strong></label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" rows="2" id="form-field-8"  name="glosa" id="glosa" ><?= $row11['glosa'] ?></textarea>
                                </div>
                            </div>                              
                            <center>
                                <h2><strong>DETALLE</strong></h2>
                            </center>
                            <br>

                            <!-- TABLA DE NOTA DE VENTA-->
                            <div class="table-responsive">
                                <table id="grilla" class="table table-striped " id="registros">
                                    <thead>
                                        <tr>

                                            <th><strong>Codigo</strong></th>
                                            <th><strong>Descripcion</strong></th>                                                    
                                            <th><strong>P Costo</strong></th>
                                            <th><strong>P Venta</strong></th>
                                            <th><strong>Margen</strong></th>
                                            <th><strong>Cant</strong></th>
                                            <th><strong>Nro Lote</strong></th>
                                            <th><strong>Sector</strong></th>
                                            <th><strong>Fecha V</strong></th>
                                            <th><strong>Monto</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--SE AGREGAN LAS FILAS DE LOS PRODUCTOS-->
                                        <?php
                                        $cons = " select p.codigo , p.nombreComercial , l.precioCosto , "
                                                . " l.precioVenta , l.margen , l.cantidad , l.nroLote ,l.fechaVencimiento , round((l.precioVenta * l.cantidad),2) AS Monto , l.idLote "
                                                . "from FacturaIngreso f , Lote l , Producto p   "
                                                . "where  f.idNotaIngreso = l.idNotaIngreso and "
                                                . "l.codigoProducto = p.codigoProducto and p.estado = 1 and "
                                                . "l.estado = 1 and f.estado = 1 and "
                                                . " f.idNotaIngreso = ?   ORDER BY f.idNotaIngreso DESC ";
                                        $dato = Database::getInstance()->getDb()->prepare($cons);
                                        $dato->execute([$id]);

                                        while ($row1 = $dato->fetch()) {
                                             $idLote = $row1['idLote'];
                                             
                                             $cons2 ="select s.nombreSector "
                                                   . "from  Ubicacion u , Sectores s , Almacen a , Existencias e "
                                                   . "where a.idAlmacen ='".$idalma."'  and u.idAlmacen = a.idAlmacen and u.idSector = s.idSector"
                                                   . " and e.idUbicacion = u.idUbicacion and e.idLote = '".$idLote."'";
                                             $datos2 = Database::getInstance()->getDb()->prepare($cons2);
                                             $datos2->execute();
                                             $row3 = $datos2->fetch();
                                             $sect = $row3['nombreSector'];    
                                             $montoP =$row1['Monto'];
                                            ?>
                                            <tr>
                                                <td><?= $row1['codigo'] ?></td>
                                                <td><?= $row1['nombreComercial'] ?></td>
                                                <td><?=$row1['precioCosto'] ?></td>
                                                <td><?= $row1['precioVenta'] ?></td>
                                                <td><?= $row1['margen'] ?></td>
                                                <td><?= $row1['cantidad'] ?></td>
                                                <td><?= $row1['nroLote'] ?></td>
                                                <td> <?= $sect ?></td>
                                                <td><?= $row1['fechaVencimiento'] ?> </td>
                                                <td><?= $montoP ?></td>
                                            </tr> 

                                            <?php
                                            $contador = $contador + 1;
                                            $AcuSubTotal= $AcuSubTotal +$montoP ;
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="8" class="invisible bg-snow"></td>
                                        <td >Sub Total</td>
                                        <td class="text-center"> <input type="text" id="subTotal" class="form-control" name="subTotal" value="<?= $AcuSubTotal ?>" size="8" readonly/> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="invisible bg-snow"></td>
                                        <td >Descuento %</td>
                                        <td class="text-center"><input type="text" id="descuento1" class="form-control" value="<?= $desc1 ?>" name="descuento1" size="8" onkeypress="validarNumeros(event);"  onkeyup="descuentos();" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="invisible bg-snow"></td>
                                        <td >Descuento %</td>
                                        <td class="text-center"><input type="text" id="descuento2" class="form-control" value="<?= $desc2 ?>"  name="descuento2" size="8" onkeypress="validarNumeros(event);" onkeyup="descuentos();"  readonly/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="invisible bg-snow"></td>
                                        <td >Descuento %</td>
                                        <td class="text-center"><input type="text" id="descuento3" class="form-control" value="<?= $desc3 ?>"  name="descuento3" size="8" onkeypress="validarNumeros(event);" onkeyup="descuentos();" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="invisible bg-snow"></td>
                                        <td class="text-center"><strong>Total</strong></td>
                                        <td class="text-center "><strong><input type="text" id="total" name="total" class="form-control" value="<?= $AcuSubTotal - $desc1 -$desc2 -$desc3 ?>" size="8" readonly/> <input type="text" id="monts" name="monts" value="<?= $AcuSubTotal ?>" hidden/></strong></td>
                                    </tr>
                                    </tfoot>
                                </table>  

                            </div>
                            <div class="form-inline" >
                                <center>

                                    <br/>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                               <a type="button" href="index.php" class="btn btn-danger"  > Aceptar</a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-success"  id="imprimir" name="imprimir" onclick="imprimirDetalle()" value="Imprimir">
                                            </div>
                                        </div>                                        
                                    </div>
                                </center> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once ('../header_footer/footer.php');
?> 