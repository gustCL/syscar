<?php
require_once("../datos/Database.php");
$dato = $_POST['dato'];
$consulta = "select * from FacturaIngreso f where  f.estado = 1 and f.idTipoIngreso = 3  and f.idNotaIngreso  LIKE '%$dato%' ORDER BY f.idNotaIngreso DESC  ";
$resul = Database::getInstance()->getDb()->prepare($consulta);
$resul->execute();
$nroFila = 1
?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>
                ID.
            </th>
           
            <th>
                GLOSA
            </th>
            <th>
                FECHA
            </th>
            <th>
                MONTO TOTAL
            </th>
            <!--AQUI VAN LOS BOTONES-->
            <th>
                
            </th>


        </tr>
    </thead>
    <?php
    if ($resul->rowCount() > 0) {
        while ($row = $resul->fetch()) {
            $NroF = $row['nroFactura'];
            ?><tr>
                <td>
                    <?= $row['idNotaIngreso']?>
                </td>
                
                <td>
                    <?= $row['glosa'] ?>
                </td>
                <td>
                    <?= $row['fecha'] ?>
                </td>
                <td>
                    <?= $row['montoTotal'] ?>
                </td>
                
                <td>                          
                    <a href="javascript:verCliente('<?= $idC ?>');"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                    <a  href="javascript:editarCliente('<?= $idC ?>');"  class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                    <a href="javascript:eliminarCliente('<?= $idC ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>

                </td>
                
            </tr>
            <?php
            $nroFila= $nroFila +1;
        }
    } else {
        ?>
        <tr>
            <td colspan="6">No se encontraron resultados</td>
        </tr>
    <?php } ?>
</table>


