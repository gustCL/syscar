<?php
require_once 'head.php';

$idUser = $_SESSION['userMaster'];
$idSucursal = $idUser['idSucursal'];
$idSesion = $_SESSION['idSesion'];
$idUsuario = $idUser['idUsuario'];
$idalma = $_GET['id'];
///////////  BUSCAR USUARIO ///////////
$consulta = "select * from Usuario u where idUsuario =? ";
$resul = Database::getInstance()->getDb()->prepare($consulta);
$resul->execute([$idUsuario]);
$row = $resul->fetch();
///////////  BUSCAR  NRO DE NOTA INGRESO ///////////
$consultaS = "select max(idNotaIngreso) as idNotaIngreso from FacturaIngreso   ";
$resulT = Database::getInstance()->getDb()->prepare($consultaS);
$resulT->execute();
$rows = $resulT->fetch();

$nroN = $rows['idNotaIngreso'];
//$nrofila = 1;
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>NOTA DE INGRESO</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="widget-body ">
                        <form  method="post" class="form-horizontal" action="javascript:addNota();" id="frm_nota"  >

                            <br>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>Nro. Nota Ingreso </strong></label>
                                <div class="col-lg-3">
                                    <label><?= $nroN + 1 ?></label>                               
                                </div>
                                <label class="col-lg-2 control-label"><strong>Usuario</strong></label>
                                <div class="col-lg-5">
                                    <label class="form-control-static" ><?= $row['nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['apellido'] ?> </label>                                           
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-2 control-label"><strong>Proveedor</strong></label>
                                <div class="col-lg-4">
                                    <input size="50" type="text" class="form-control" name="nombreProve" id="nombreProve"  placeholder="Nombre Proveedor" required/>
                                    <script>

                                        $('#nombreProve').autocomplete({
                                            source: function (request, response) {
                                                $.ajax({
                                                    url: 'autocompletado.php',
                                                    dataType: "json",
                                                    data: {
                                                        name_startsWith: request.term,
                                                        type: 'Nombre'
                                                    },
                                                    success: function (data) {
                                                        response($.map(data, function (item, id) {
                                                            return {
                                                                label: id,
                                                                value: id,
                                                                id: item

                                                            };
                                                        }));
                                                    }
                                                });
                                            },
                                            select: function (event, ui) {
                                                var idpr = ui.item.id;
                                                // alert(idpr);
                                                $('#idProvee').val(idpr);

                                            }
                                        });
                                    </script>                                                
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>Glosa</strong></label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" rows="2" id="form-field-8" name="glosa" id="glosa" required></textarea>
                                </div>
                            </div>                              
                            <center>
                                <h2><strong>DETALLE</strong></h2>
                            </center>
                            <br>

                            <!-- TABLA DE NOTA DE VENTA-->
                            <div class="table-responsive">
                                <table id="grilla" class="table table-striped ">
                                    <thead>
                                        <tr>
                                            <th><strong>Nro.</strong></th>
                                            <th><strong>Codigo</strong></th>
                                            <th><strong>Descripcion</strong></th>                                                    
                                            <th><strong>P Costo</strong></th>
                                            <th><strong>P Venta</strong></th>
                                            <th><strong>Margen</strong></th>
                                            <th><strong>Cant</strong></th>
                                            <th><strong>Nro Lote</strong></th>
                                            <th><strong>Sector</strong></th>
                                            <th><strong>Fecha V</strong></th>
                                            <th><strong>Monto</strong></th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                        <!--CONTADOR DE FILAS DE LA TABLA -->
                                    <input type="text" id="conta" name="conta" value="1" hidden/><!--CONTADOR DE FILAS DE LA TABLA -->
                                    <input type="text" id="monts" name="monts" value="0" hidden/><!--AQUI SE ACUMULA LOS MONTOS -->
                                    <input type="text" id="montodesc" name="montodesc"  hidden/><!--AQUI SE ACUMULA LOS MONTOS -->
                                    <input type="text" id="idProvee" name="idProvee"  hidden/><!--CODIGO PROVEEDOR -->

                                    <input type="text" name="idAlm" id="idAlm" value="<?= $idalma ?>" hidden/>
                                    <td></td>
                                    <td><input size="10" type="text" class="form-control" id="codigo" name="codigo"  style="text-align: right" value="-" readonly/><input type="text" id="codi" name="codi" hidden/><input type="text" id="idProd" name="idProd"  hidden/></td>
                                    <td><input size="80" type="text" class="form-control" id="descripcion" name="descripcion"  value=""  />
                                        <script>

                                            $('#descripcion').autocomplete({
                                                source: function (request, response) {
                                                    $.ajax({
                                                        url: 'autocompletado.php',
                                                        dataType: "json",
                                                        data: {
                                                            name_startsWith: request.term,
                                                            type: 'Detalle'
                                                        },
                                                        success: function (data) {
                                                            response($.map(data, function (item, id) {
                                                                return {
                                                                    label: id,
                                                                    value: id,
                                                                    id: item

                                                                };
                                                            }));
                                                        }
                                                    });
                                                },
                                                select: function (event, ui) {
                                                    var idpr = ui.item.id;

                                                    var url = 'busqueda_detalle_nota.php';
                                                    $.ajax({
                                                        type: 'POST', url: url,
                                                        data: 'id=' + idpr,
                                                        success: function (valores) {
                                                            var datos = eval(valores);
                                                            $('#precioC').val(datos[0]);
                                                            $('#precioV').val(datos[1]);
                                                            $('#descripcion').val(datos[2]);
                                                            $('#margen').val(datos[3]);
                                                            $('#idProd').val(datos[4]);
                                                            $('#codi').val(datos[5]);
                                                            $('#nombreSect').val(datos[6]);
                                                            $('#cantidad').focus();
                                                            //                                                    
                                                        }
                                                    });

                                                }
                                            });
                                        </script>                               
                                    </td>                                       
                                    <td><input size="20" name="precioC" type="text" id="precioC" class="form-control" style="text-align: right"  onkeyup="cal_Precio()" required /> </td>
                                    <td><input size="20" name="precioV" type="text" id="precioV" class="form-control" style="text-align: right"   onkeyup="cal_Margen()" required />  </td>
                                    <td><input size="10" name="margen" type="text" id="margen" class="form-control" style="text-align: right" onkeypress="validarNumeroVentas2(event);"  onkeyup="cal_Precio()" required /> </td>
                                    <td><input size="10"  name="cantidad" type="text" id="cantidad" class="form-control" style="text-align: right"  onkeypress="validarNumeroVentas(event);"  onkeyup=" cal_cantidad()" required /> </td>
                                    <td><input size="20" type="text" id="lote" name="lote" class="form-control" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" required /></td>
                                    <td> <?php
                                        require_once("../datos/Database.php");
                                        $datos = Database::getInstance()->getDb()->prepare("SELECT distinct s.nombreSector  FROM Ubicacion ubi , Sectores s,  Almacen a WHERE  s.idAlmacen = '$idalma' and a.idAlmacen = s.idAlmacen and a.idSucursal = '$idSucursal'  GROUP BY s.nombreSector");
                                        $datos->execute();
                                        ?>
                                        <select  name="nombreSect" id="nombreSect"   data-bv-field="country">
                                            <?php while ($row = $datos->fetch()) { ?>               
                                                <option value="<?= $row['nombreSector'] ?>"  ><?= $row['nombreSector'] ?></option>
                                            <?php } ?>
                                        </select><i class="form-control-feedback" data-bv-field="country" style="display: none;" ></i>
                                    </td>
                                    <td><input size="20" class="form-control" id="fechaven" name="fechaven" type="date" required /> </td>
                                    <td><input size="40" class="form-control" name="totalProducto" type="text" id="totalProducto"   style="text-align: right" value="" required /> </td>
                                    <td>                                          
                                        <strong><input   name="agregar" id="agregar" value="+" class="btn-primary"  type="submit"/></strong>
                                    </td> 



                                    </tfoot>
                                </table>  

                            </div>
                           <table align="right" >
                                <tr>
                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td >Sub Total &nbsp;</td>
                                    <td class="text-center"> <input type="text" id="subTotal" class="form-control" name="subTotal" value="" size="8" readonly/> </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow"></td> 
                                    <td><br></td>
                                    <td><br></td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td >Descuento % &nbsp;</td>
                                    <td class="text-center"><input type="text" id="descuento1" class="form-control" value="0" name="descuento1" size="8" onkeypress="validarNumeros(event);"  onkeyup="descuentos();" /></td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow"></td> 
                                    <td><br></td>
                                    <td><br></td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td >Descuento % &nbsp;</td>
                                    <td class="text-center"><input type="text" id="descuento2" class="form-control" value="0"  name="descuento2" size="8" onkeypress="validarNumeros(event);" onkeyup="descuentos();"  /></td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow"></td> 
                                    <td><br></td>
                                    <td><br></td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td >Descuento % &nbsp;</td>
                                    <td class="text-center"><input type="text" id="descuento3" class="form-control" value="0"  name="descuento3" size="8" onkeypress="validarNumeros(event);" onkeyup="descuentos();"  /></td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow"></td> 
                                    <td><br></td>
                                    <td><br></td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td class="text-center"><strong>Total</strong></td>
                                    <td class="text-center "><strong><input type="text" id="total" name="total" class="form-control" value="" size="8" readonly/></strong></td>
                                </tr>
                           </table>
                            <br>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#guardar').attr('disabled', true);
                                });
                            </script>
                            <div class="row" role="form">

                            </div>
                            <div class="form-inline" >
                                <center>

                                    <br/>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-primary"  id="guardar" name="guardar" onclick="registrarDetalle_nota_ingreso();" value="Guardar">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-success"  id="imprimir" name="imprimir" value="Imprimir" onclick="imprimirNota()">
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <a type="button" href="index.php" class="btn btn-danger"  > Cancelar</a>
                                            </div>
                                        </div>                                        
                                    </div>
                                </center> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once ('../header_footer/footer.php');
?> 