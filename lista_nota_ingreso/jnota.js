
$(function () {
    $('#buscar-nota-ingreso').on('keyup', function () {
        var dato = $('#buscar-nota-ingreso').val();
        var url = 'busca_nota_ingreso.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'dato=' + dato,
            success: function (datos) {
                $('#agrega-registros').html(datos);
            }
        });
        return false;
    });

    $('#btnBuscar').on('click', function () {
        var dato = $('#fechaN').val();
        var url = 'busca_nota_fecha.php';

        $.ajax({
            type: 'POST',
            url: url,
            data: 'dato=' + dato,
            success: function (datos) {
                $('#agrega-registros').html(datos);

            }
        });
        return false;
    });

});
function cal_cantidad() {
    var cantidad = $('#cantidad').val();
    var precioCosto = $('#precioC').val();
    var precioVenta = $('#precioV').val();
    var totalprod = 0;
    if ($.isNumeric(cantidad) > 0 && $.isNumeric(precioCosto) && $.isNumeric(precioVenta)) {
        cantidad = parseFloat(cantidad);
        precioCosto = parseFloat(precioCosto);
        totalprod = cantidad * precioCosto;
        $("#totalProducto").val(totalprod.toFixed(2));
    }
    return false;
}

function cal_Margen() {
    var margen = $('#margen').val();
    var precioC = $('#precioC').val();
    var precioV = $('#precioV').val();
    var cantidad = $('#cantidad').val();
    var monto = 0;
    margen = parseFloat(margen);
    if (margen > 0) {
        precioV = parseFloat(precioV);
        precioC = parseFloat(precioC);
        margen = (((precioV - precioC) * 100) / precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
        $("#margen").val(margen.toFixed(2));
        $("#totalProducto").val(monto.toFixed(2));

    }

    return false;
}
function cal_Precio() {
    var margen = $('#margen').val();
    var precioC = $('#precioC').val();
    var precioV = $('#precioV').val();
    var cantidad = $('#cantidad').val();
    var monto = 0;
    if ($.isNumeric(precioC) > 0) {
        margen = parseFloat(margen);
        precioC = parseFloat(precioC);
        precioV = parseFloat(precioV);

        precioV = (((margen * precioC) / 100) + precioC);

        $("#precioV").val(precioV.toFixed(2));
        cantidad = parseFloat(cantidad);
        if (cantidad > 0) {
            monto = (precioV * cantidad);
            $("#totalProducto").val(monto.toFixed(2));
            subTotal = subTotal + monto;
        }

    }
    return false;
}


function addNota() { //agrega filas a la tabla de nota de venta y suma los precios de cada producto  
    var subtotal_sw = $("#monts").val();
//        alert(subtotal_sw);
    $("#monts").val("");
    $("#descuento1").val("0");
    $("#descuento3").val("0");
    $("#descuento2").val("0");

    var frm = $("#frm_nota").serialize();
    $.ajax({
        url: 'agregar_detalle.php',
        data: frm,
        type: 'post',
        success: function (registro) {
            subtotal_sw = parseFloat(subtotal_sw);
            var total = $("#totalProducto").val();
            total = parseFloat(total);
            subtotal_sw = subtotal_sw + total;
            $('#subTotal').val(subtotal_sw.toFixed(2));
            $('#total').val(subtotal_sw.toFixed(2));

            $("#monts").val(subtotal_sw);

            $("#totalProducto").val("");
            $("#precioV").val("");
            $("#precioC").val("");
            $("#cantidad").val("");
            $("#lote").val("");
            $("#fechaven").val("");
            $("#descripcion").val("");
            $("#margen").val("");
            $("#antSector").val("");
            $("#nombreSector").val("");

            $('#grilla tbody').append(registro);

            var tablita = document.getElementById("grilla");
            var d = tablita.rows.length - 1;
//            alert(d);

            $('#conta').val(d);
            $('#descripcion').focus();
            eliminar_detalle_prod();
            if ($('#total').val() > 0) {
                $('#guardar').attr('disabled', false);
            } else {
                $('#guardar').attr('disabled', true);
            }
            return false;
        }
    });
    return false;
}
function eliminar_detalle_prod() { //Elimina las filas de la tabla compra y resta el subtotal
    var id = $(this).parents("tr").find("td").eq(0).html();
    id = parseInt(id);
    var dato = $('#montoT' + id).val();
    $("a.elimina").click(function () {

        if (confirm("Desea eliminar el producto ")) {
            $(this).parents("tr").fadeOut("normal", function () {
                $(this).remove();
                subT = parseFloat($('#subTotal').val()) - dato;
                $('#subTotal').val(parseFloat($('#subTotal').val()) - dato);
                $('#total').val($('#subTotal').val());
                $('#compra_saldo').val($('#subTotal').val());

            });
        }
    });
}
;
function registrarDetalle_nota_ingreso() {
    var frm = $("#frm_nota").serialize();

    $.ajax({
        url: 'registrar_nota_ingreso.php',
        data: frm,
        type: 'post',
        success: function () {
          
            document.location.href = 'index.php';


        }

    });


    return false;
}
function validarNumeros(event) {//Para validar numeros de cantidad de producto
    if (event.keyCode == 13) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }

}


function seleccion_alma() {
    $.ajax({
        type: 'POST',
        success: function () {

            $('#selec-almacen').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;
}


function  pasar_registrar() {

    var id = $('#almac').val();
    id = parseInt(id);
//       alert(id);
    $.ajax({
        type: 'POST',
        data: 'id=' + id,
        success: function () {
            document.location.href = 'nota_ingreso.php?id=' + id;
        }
    });
    return false;
}




function descuentos() {
    var des1 = $('#descuento1').val();
    var des2 = $('#descuento2').val();
    var des3 = $('#descuento3').val();
    var subt = $('#subTotal').val();

    if ($.isNumeric(des1) > 0 && $.isNumeric(subt)) {
        subt = parseFloat(subt);
        des1 = parseFloat(des1);
        subt = subt - des1;

        $('#total').val(subt.toFixed(2));
    }

    if ($.isNumeric(des2) > 0 && $.isNumeric(subt)) {
        subt = parseFloat(subt);
        des2 = parseFloat(des2);
        subt = subt - des2;

        $('#total').val(subt.toFixed(2));
    }

    if ($.isNumeric(des3) > 0 && $.isNumeric(subt)) {
        subt = parseFloat(subt);
        des3 = parseFloat(des3);
        subt = subt - des3;

        $('#total').val(subt.toFixed(2));
    }
    if ($('#total').val() > 0) {
        $('#guardar').attr('disabled', false);
    } else {
        $('#guardar').attr('disabled', true);
    }

    if (des1 == 0 && des2 == 0 && des3 == 0) {
        $('#total').val($('#subTotal').val());
    }
    return false;
}



function actualizarDetalle_nota_ingreso() {
    var frm = $("#frm_nota").serialize();

    $.ajax({
        url: 'actualizar_nota_ingreso.php',
        data: frm,
        type: 'post',
        success: function (valor) {
            alert(valor);
            document.location.href = 'index.php';


        }

    });


    return false;
}

function addNota_Actu() { //agrega filas a la tabla de nota de venta y suma los precios de cada producto  
    var subtotal_sw = $("#monts").val();

    $("#monts").val("");


    var frm = $("#frm_nota").serialize();
    $.ajax({
        url: 'agregar_detalle.php',
        data: frm,
        type: 'post',
        success: function (registro) {
            var resultado = 0;
            subtotal_sw = parseFloat(subtotal_sw);
            var total = $("#totalProducto").val();
            total = parseFloat(total);
            subtotal_sw = subtotal_sw + total;
            $('#subTotal').val(subtotal_sw.toFixed(2));

            resultado = subtotal_sw - $("#descuento1").val() - $("#descuento2").val() - $("#descuento3").val();

            $("#total").val(resultado.toFixed(2));
            $("#monts").val(subtotal_sw);

            $("#totalProducto").val("");
            $("#precioV").val("");
            $("#precioC").val("");
            $("#cantidad").val("");
            $("#lote").val("");
            $("#fechaven").val("");
            $("#descripcion").val("");
            $("#margen").val("");
            $("#antSector").val("");
            $("#nombreSector").val("");

            $('#grilla tbody').append(registro);

            var tablita = document.getElementById("grilla");
            var d = tablita.rows.length - 1;
//            alert(d);

            $('#conta').val(d);
            $('#descripcion').focus();
            eliminar_detalle_actu()
            return false;
        }
    });
    return false;
}
function eliminar_producto() {
    eliminar_detalle_actu();

    return false;
}
function eliminar_detalle_actu() { //Elimina las filas de la tabla compra y resta el subtotal

    $("a.elimina").click(function () {
        var id = $(this).parents("tr").find("td").eq(0).html();
        id = parseInt(id);
        var dato = $('#montoT' + id).val();
        var subT = $('#subTotal').val();
        var aux = 0;
        var caso_aux = 0;

        if (confirm("Desea eliminar el producto ")) {
            $(this).parents("tr").fadeOut("normal", function () {
                $(this).remove();


                subT = parseFloat(subT);
                aux = subT - dato;
                $('#subTotal').val(aux);
                caso_aux = $('#subTotal').val() - $('#descuento1').val() - $('#descuento2').val() - $('#descuento3').val();
                $('#total').val(caso_aux);
                $('#monts').val(aux);

            });
        }
    });
    };

function validarNumeros(event) {//Para validar numeros de descuento en bs y %
    if (event.keyCode == 46) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }
};


function imprimirDetalle()
{   
   var nota= $('#idNotaa').val();
            document.location.href = 'VistaImpNota.php?idNotaa='+ nota;

};
function imprimirNota(){
        var frm = $("#frm_nota").serialize();

    $.ajax({
        url: 'registrarNuevaImp.php',
        data: frm,
        type: 'post',
        success: function (registro) {
            var datos = eval(registro);
             var nota = datos[0];
             document.location.href = 'VistaImpNota.php?idNotaa='+ nota;
        }

    });


    return false;
}