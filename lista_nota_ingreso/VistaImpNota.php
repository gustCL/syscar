<?php
require_once("../datos/Database.php");
session_start();
$idUser = $_SESSION['userMaster'];
$idSucursal = $idUser['idSucursal'];
$idSesion = $_SESSION['idSesion'];
$idUsuario = $idUser['idUsuario'];
$id = $_GET['idNotaa'];

ini_set("date.timezone", "America/La_Paz");
$fecha = date("d-m-Y");
$hora = date("H:i:s");
////////////////////////////// SE HACE LA CONSULTA PARA OBTENER LOS DATOS  DE GLOSA , NRONOTAINGRESO 
$consultas = "select * from FacturaIngreso f , Proveedor p  "
        . "where f.idProveedor = p.idProveedor and p.estado = 1 and f.estado =1 and "
        . " f.idNotaIngreso = ?   ORDER BY f.idNotaIngreso DESC  ";
$resultado = Database::getInstance()->getDb()->prepare($consultas);
$resultado->execute([$id]);
$row11 = $resultado->fetch();

$desc1 = $row11['descuento1'];
$desc2 = $row11['descuento2'];
$desc3 = $row11['descuento3'];
///////////  BUSCAR USUARIO ///////////
$consulta = "select * from Usuario u where idUsuario =? ";
$resul = Database::getInstance()->getDb()->prepare($consulta);
$resul->execute([$idUsuario]);
$row = $resul->fetch();
///////////  BUSCAR ALMACEN ///////////
$consulta2 = "select u.idAlmacen "
        . "from FacturaIngreso f , Proveedor p , Ubicacion u , Existencias e , Lote l 
            where f.idProveedor = p.idProveedor and p.estado = 1 and f.estado =1 and 
            f.idNotaIngreso = ? and f.idNotaIngreso = l.idNotaIngreso and l.idLote = e.idLote 
            and e.idUbicacion = u.idUbicacion group by u.idAlmacen";
$resultado2 = Database::getInstance()->getDb()->prepare($consulta2);
$resultado2->execute([$id]);
$row2 = $resultado2->fetch();
$idalma = $row2['idAlmacen'];
$contador = 1;
$AcuSubTotal = 0;

//EMPRESA
$consulta0 = "select nombreEmpresa , nit from Empresa ";
$resultado0 = Database::getInstance()->getDb()->prepare($consulta0);
$resultado0->execute();
$empresa = $resultado0->fetch();
?>
<html> 
    <head> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="all">
        <link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen">
        <link href="../reporteCompras/printListaTransac.css" rel="stylesheet" type="text/css" media="print" />
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet"  media="all">
        <script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="jPedidos.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- USO DE LA LIBRERIA PARA EXPORTAR A EXCEL-->
        <script type="text/javascript" src="jquery-1.3.2.min.js"></script>
        <script language="javascript">
            $(document).ready(function () {
                $(".botonExcel").click(function (event) {
                    $("#datos_a_enviar").val($("<div>").append($("#Exportar_a_Excel").eq(0).clone()).html());
                    $("#FormularioExportacion").submit();
                });
            });
        </script>
    </head> 
    <body> 
        <script language="JavaScript">
            $(document).ready(function () {
                doPrint();
            });
            function doPrint() {
                //document.all.item("mostrarUser").style.visibility='visible'; 
                window.print()();
                //document.all.item("mostrarUser").style.visibility='hidden';
            }
            function volver(dato) {
                location.href = "index.php";
            }
        </script>
        <div id="leteral"></div>
        <div id="noprint">
            <table>
                <tr>
                    <td>
                        <button type="button" name="volver" id="volver" onclick="volver()">Volver</button>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <button type="button" onclick="doPrint()" >Imprimir</button>
                    </td>
                </tr>
            </table> 
        </div>
        <div id="hoja" style="height: auto"> 
            <div id="mostrarUser">

            </div>
            <style>
                #encabezado{
                    margin-left: 5%;
                    /*border: 1px solid black;*/
                    width: 90%;
                    height: 10%;
                    /*background-color: #fffff;*/
                }
                #logo
                {
                    margin-top: 1%;
                    margin-left: 1%;
                    margin-right: 1%;
                    margin-bottom: 1%;
                    width: 20%;
                    height: 40%;
                    float: left;
                    /*border: 1px solid yellow;*/
                    padding-bottom: 2%;
                    padding-top: 2%;
                    padding-left: 2%;
                    padding-right: 2%;
                }
                #titulo
                {
                    font-family: serif 18pt ;
                    margin-top: 1%;
                    width: 50%;
                    height: 61%;
                    float: left;
                    /*border: 1px solid green;*/
                    aling: center;
                }
                #tit
                {
                    alignment-adjust: central;
                    alignment-baseline: central;
                }
                #inf{
                    margin-top: 6%;
                    padding-bottom: 2%;
                    padding-top: 2%;
                    padding-left: 2%;
                    padding-right: 2%;
                    margin-right: 2%;
                    /*border: 1px solid red;*/
                    float: right;
                    width: 26%;
                    height: 30%;
                }
                #informacion{
                    margin-left: 5%;
                    margin-top: 3%;
                    /*border: 1px solid black;*/
                    width: 90%;
                    height: auto;
                }
                .tabla {
                    font-family: Verdana, Arial, Helvetica, sans-serif;
                    font-size:11px;
                    text-align: left;
                    width: 93%;
                }
                .tabla>tr>td {
                    font-family: Verdana, Arial, Helvetica, sans-serif;
                    font-size:11px;
                    text-align: right;
                    width: 100%;
                    padding-left: 1%;
                }
                .tabla th {
                    padding: 5px;
                    font-size: 16px;
                    background-color: #83aec0;
                    background-image: url(fondo_th.png);
                    background-repeat: repeat-x;
                    color: #FFFFFF;
                    border-right-width: 1px;
                    border-bottom-width: 1px;
                    border-right-style: solid;
                    border: 1px solid black;
                    border-bottom-style: solid;
                    border-right-color: #558FA6;
                    border-bottom-color: #558FA6;
                    font-family: “Trebuchet MS”, Arial;
                    text-transform: uppercase;
                }
                .tabla .modo1 {
                    font-size: 12px;
                    font-weight:bold;
                    background-color: #e2ebef;
                    background-image: url(fondo_tr01.png);
                    border: 1px solid black;
                    background-repeat: repeat-x;
                    color: #34484E;
                    font-family: “Trebuchet MS”, Arial;
                }
                .tabla .modo1 td {
                    padding: 5px;
                    border-right-width: 1px;
                    border-bottom-width: 1px;
                    border-right-style: solid;
                    border-bottom-style: solid;
                    border: 1px solid black;
                    border-right-color: #A4C4D0;
                    border-bottom-color: #A4C4D0;
                    text-align:left;
                }
                .tabla .modo2 {
                    font-size: 12px;
                    font-weight:bold;
                    background-color: #fdfdf1;
                    background-image: url(fondo_tr02.png);
                    background-repeat: repeat-x;
                    color: black;
                    font-family: “Trebuchet MS”, Arial;
                    text-align:right;
                }
                .tabla .modo2 td {
                    padding: 5px;
                    border-right-width: 1px;
                    border-bottom-width: 1px;
                    border-right-style: solid;
                    border-bottom-style: solid;
                    border-right-color: #EBE9BC;
                    border-bottom-color: #EBE9BC;
                }
                .tabla .modo2 th {
                    background-image: url(fondo_tr02a.png);
                    background-position: left top;
                    font-size: 12px;
                    font-weight:bold;
                    background-color: #fdfdf1;
                    background-repeat: repeat-x;
                    color: #990000;
                    font-family: “Trebuchet MS”, Arial;
                    text-align:left;

                    border-right-width: 1px;
                    border-bottom-width: 1px;
                    border-right-style: solid;
                    border-bottom-style: solid;
                    border-right-color: #EBE9BC;
                    border-left-color: #EBE9BC;
                    border-bottom-color: #EBE9BC;
                }
            </style>
            <div class="widget">
                <div id="encabezado" >
                    <div id="logo" >
                        <img width="100" height="100" src="..\..\farmacia\assets\img\Logo1.png"/>
                    </div>
                    <div id="titulo">
                        <center>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="right">
                                            <font size='6'><strong><label for="user_name">NOTA DE INGRESO</label></strong></font><br>
                                            <font size='5'><strong><center><label for="user_name"><?= $empresa['nombreEmpresa'] ?></label></center></strong></font><br>
                                <font size='4'><center><label for="user_name">NIT: <?= $empresa['nit'] ?></label></center></font>

                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                    <div id="inf">
                        <center>
                            <table >
                                <tbody>
                                    <tr class="">
                                        <td>
                                            <fieldset>
                                                <strong><label for="user_name"> Nro Nota Ingreso:</label></strong>
                                            </fieldset>
                                        </td>
                                        <td style="text-align: right">

                                            <label for="user_name"><?= $id ?> </label><br>

                                        </td>
                                    </tr>
                                    <tr class="">
                                        <td>
                                            <strong><label for="user_name">Fecha:</label></strong>
                                        </td>
                                        <td >
                                            <fieldset>
                                                <label for="user_name"><?= $fecha ?></label>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>

                </div>
               
                
                
                <div align="center" id="informacion">
                <br>
                <br>
                <br>
                <br>
                    <div id="content">
                        <table >
                            <tbody>
                                <tr class="">
                                    <td colspan="4">
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"><strong>Usuario</strong></label>
                                            <div class="col-lg-5">
                                                <label class="form-control-static" ><?= $row['nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $row['apellido'] ?> </label>                                           
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label  class="col-lg-2 control-label"><strong>Proveedor</strong></label>
                                            <div class="col-lg-10">
                                                <input size="50" type="text" class="form-control" name="nombreProve"  id="nombreProve" value="<?= $row11['nombreProve'] ?>"  placeholder="Nombre Proveedor"/>

                                            </div>

                                        </div>
                                    </td>
                                </tr>
                                <tr class="">                                    
                                </tr>
                                <tr class="">
                                    <td>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"><strong>Glosa</strong></label>
                                            <div class="col-lg-8">
                                                <textarea class="form-control" rows="2" id="form-field-8"  name="glosa" id="glosa" ><?= $row11['glosa'] ?></textarea>
                                            </div>
                                        </div> 
                                    </td> 
                                </tr>
                            </tbody>
                        </table>

                    </div>
                <br>
                    <div>
                        <table class="tabla" border="1" cellpadding="1" cellspacing="1" >
                            <thead>
                                <tr>
                            <center>
                                    <th><strong>Codigo</strong></th>
                                    <th><strong>Descripcion</strong></th>                                                    
                                    <th><strong>PCosto</strong></th>
                                    <th><strong>PVenta</strong></th>
                                    <th><strong>Margen</strong></th>
                                    <th><strong>Cant</strong></th>                                    
                                    <th><strong>Fecha_VenciV</strong></th>
                                    <th><strong>Monto_P</strong></th>
                            </center>  
                           
                                </tr>
                            </thead>
                            <tbody>
                                <!--SE AGREGAN LAS FILAS DE LOS PRODUCTOS-->
                                <?php
                                $cons = " select p.codigo , p.nombreComercial , l.precioCosto , "
                                        . " l.precioVenta , l.margen , l.cantidad , l.nroLote ,l.fechaVencimiento , round((l.precioVenta * l.cantidad),2) AS Monto , l.idLote "
                                        . "from FacturaIngreso f , Lote l , Producto p   "
                                        . "where  f.idNotaIngreso = l.idNotaIngreso and "
                                        . "l.codigoProducto = p.codigoProducto and p.estado = 1 and "
                                        . "l.estado = 1 and f.estado = 1 and "
                                        . " f.idNotaIngreso = ?   ORDER BY f.idNotaIngreso DESC ";
                                $dato = Database::getInstance()->getDb()->prepare($cons);
                                $dato->execute([$id]);

                                while ($row1 = $dato->fetch()) {
                                    $idLote = $row1['idLote'];

                                    $cons2 = "select s.nombreSector "
                                            . "from  Ubicacion u , Sectores s , Almacen a , Existencias e "
                                            . "where a.idAlmacen ='" . $idalma . "'  and u.idAlmacen = a.idAlmacen and u.idSector = s.idSector"
                                            . " and e.idUbicacion = u.idUbicacion and e.idLote = '" . $idLote . "'";
                                    $datos2 = Database::getInstance()->getDb()->prepare($cons2);
                                    $datos2->execute();
                                    $row3 = $datos2->fetch();
                                    $sect = $row3['nombreSector'];
                                    $montoP = $row1['Monto'];
                                    ?>
                                    <tr>
                                        <td colspan="1" align="center"><input type="text"  id="cod" class="form-control"  name="cod"  value="<?= $row1['codigo'] ?>"/></td>
                                        <td colspan="1" align="center"><input type="text"  id="cod" class="form-control"  name="cod"  value="<?= $row1['nombreComercial'] ?>"/></td>
                                        <td colspan="1" align="right"><input type="text"  id="cod" class="form-control"  name="cod"  value="<?= $row1['precioCosto'] ?>"/></td>
                                        <td colspan="1" align="right"><input type="text"  id="cod" class="form-control"  name="cod"  value="<?= $row1['precioVenta'] ?>"/></td>
                                        <td colspan="1" align="right"><input type="text"  id="cod" class="form-control"  name="cod"  value="<?= $row1['margen'] ?>"/></td>
                                        <td colspan="1" align="right"><input type="text"  id="cod" class="form-control"  name="cod"  value="<?= $row1['cantidad'] ?>"/></td>
                                        <td colspan="1" align="center"><input type="text"  id="cod" class="form-control"  name="cod"  value="<?= $row1['fechaVencimiento'] ?>"/> </td>
                                        <td colspan="1" align="right"><input type="text"  id="cod" class="form-control" size="8" name="cod"  value="<?= $montoP ?>"/></td>
                                    </tr> 

                                    <?php
                                    $contador = $contador + 1;
                                    $AcuSubTotal = $AcuSubTotal + $montoP;
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow" ></td>
                                    <td class="text-center">Sub Total</td>
                                    <td class="text-center" > <input type="text"  id="subTotal" class="form-control"  name="subTotal" value="<?= $AcuSubTotal ?>" readonly/> </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow" ></td>
                                    <td class="text-center">Descuento %</td>
                                    <td class="text-center"><input type="text" id="descuento1" class="form-control" value="<?= $desc1 ?>" name="descuento1" size="8" onkeypress="validarNumeros(event);"  onkeyup="descuentos();" readonly/></td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow" align="right"></td>
                                    <td class="text-center">Descuento %</td>
                                    <td class="text-center"><input type="text" id="descuento2" class="form-control" value="<?= $desc2 ?>"  name="descuento2" size="8" onkeypress="validarNumeros(event);" onkeyup="descuentos();"  readonly/></td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow" align="right"></td>
                                    <td class="text-center">Descuento %</td>
                                    <td class="text-center"><input type="text" id="descuento3" class="form-control" value="<?= $desc3 ?>"  name="descuento3" size="8" onkeypress="validarNumeros(event);" onkeyup="descuentos();" readonly /></td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow" align="right"></td>
                                    <td class="text-center"><strong>Total</strong></td>
                                    <td class="text-center "><strong><input type="text" id="total" name="total" class="form-control" value="<?= $AcuSubTotal - $desc1 - $desc2 - $desc3 ?>" size="8" readonly/> <input type="text" id="monts" name="monts" value="<?= $AcuSubTotal ?>" hidden/></strong></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                   
                </div>
                <form action="ficheroExcel.php" method="post" id="FormularioExportacion">
                    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                </form>
                </body>
                </html>