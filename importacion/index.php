<?php
require_once 'head.php';
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>
                    SELECCIONE LA TABLA A IMPORTAR
                </strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>

    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">


                    <div class="widget-body ">
                        <script>
                            $(function () {
                                $('#sort').dataTable();
                                $('[title]').tooltip();
                            });
                            function validar() {

                                if (confirm("Esta seguro que desea importar el archivo?")) {
                                    /**
                                     * verificamos si se selecciono algun archivo
                                     */
                                    if ($("#archivo").val() == "") {
                                        alert("Seleccione un archivo .csv");
                                        return false;
                                    }
                                    var nombreArchivo = $("#archivo").val().split(".");//recuperamos la extension del archivo a subir
                                    /**
                                     * verificamos si la extension es .csv
                                     */
                                    if (nombreArchivo[nombreArchivo.length - 1] != "csv") {
                                        alert("ARCHIVO NO VALIDO");
                                        return false;
                                    }
                                    return true;
                                }
                                else {
                                    return false;
                                }
                            }
                        </script> 

                        <form name="formid" id="formid" method="post" action="vista.php" enctype="multipart/form-data" onsubmit="return validar();">
                            <div class="form-group">
                                <label for="archivo"><p>Archivo .CSV</p>
                                    <input type="file" name="archivo"  id="archivo" class="form-control">
                                </label>
                                <label for="button">
                                    <button>Subir</button>
                                </label>
                            </div>
                        </form>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once ('../header_footer/footer.php');
?> 

