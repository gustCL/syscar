<?php
    require_once '../datos/Database.php';
    ini_set("date.timezone", "America/La_Paz");
    $fecha = date('d-m-Y');
    $hora = date("H:i:s");

    try {
        for ($i = 30; $i < 60; $i++) {

            $query = "INSERT INTO Transaccion(tipoTrans, fecha, hora, idInicioSesion, idUsuario, estado) VALUES(?, ?, ?, ?, ?, ?)";
            $cmd = Database::getInstance()->getDb()->prepare($query);
            $cmd->execute([1, $fecha, $hora, 69, 1, 1]);


            $query_venta = "INSERT INTO Ventas(tipoVenta, idTransaccion, idCliente, montoSubTotal, idPromoDes, descuento, montoTotal, idTipoCambio, tipoCambio, efectivoSus, efectivoBs, cambio) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
            $cmd_venta = Database::getInstance()->getDb()->prepare($query_venta);
            $cmd_venta->execute([0, $i, 1, 4.5, 0, 0, 4.5, 1, 6.96, 0, 5.00, 0.50]);

            $query_venta_sucursal = "Insert into VentaSucursal values (?,?,?)";
            $cmd_venta_sucursal = Database::getInstance()->getDb()->prepare($query_venta_sucursal);
            $cmd_venta_sucursal->execute([1, $i, $i]);
        }
    } catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

