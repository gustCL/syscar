<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 06/01/2016
 * Time: 15:29
 */
    require_once '../datos/Database.php';
    session_start();
    /*
    // Obtener las terminales registradas
    $query_terminal = "SELECT idTerminal, nombre FROM Terminal";
    $cmd_terminal = Database::getInstance()->getDb()->prepare($query_terminal);
    $cmd_terminal->execute();

    $array_terminal = array();
    while ($result_terminal = $cmd_terminal->fetch()) {
        $array_terminal[$result_terminal['idTerminal']] = $result_terminal['nombre'];
    }

    print_r($array_terminal);
    */
    /*
    $array_terminal = $cmd_terminal->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);
    $array_terminal = array_map('reset', $array_terminal);
    print_r($array_terminal);
    */

    /*
    $query_terminal = "SELECT pcu.idCasoUso, pcu.url
                        FROM Usuario u, PrivilegiosCasoUso pcu, AsignaPrivilegioCasoUso apcu
                        WHERE u.idUsuario = :id_usuario and u.idUsuario = apcu.idUsuario AND
                              apcu.idCasoUso = pcu.idCasoUso";
    $cmd_terminal = Database::getInstance()->getDb()->prepare($query_terminal);
    $cmd_terminal->bindParam(':id_usuario', $_SESSION['userMaster']['idUsuario']);
    $cmd_terminal->execute();

    $array_terminal = array();
    while ($result_terminal = $cmd_terminal->fetch()) {
        $array_terminal[$result_terminal['idCasoUso']] = $result_terminal['url'];
    }

    if (!array_search('../perfil_empresa', $array_terminal)) {
        echo 'existe';
    } else {
        echo 'NO existe';
    }
    //print_r($array_terminal);
    */




verificar_fecha_limite_dosificaciones();





// Verifica las fechas de finalización de las dosificaciones para combiar el estado de las que estén caducadas
function verificar_fecha_limite_dosificaciones() {
    $query = "SELECT * FROM Dosificacion";
    $cmd = Database::getInstance()->getDb()->prepare($query);
    $cmd->execute();
    while ($result = $cmd->fetch()) {
        $id_dosificacion = $result['idDosifi'];

        $fecha_limite = $result['fechaLimite'];
        date_default_timezone_set('America/La_Paz');
        $fecha_actual = date('d-m-Y');
        $diferencia_fecha = strtotime($fecha_limite) - strtotime($fecha_actual);
        $diferencia_dias = intval($diferencia_fecha / 60 / 60 / 24);
        if ($diferencia_dias <= -1) {
            actualizar_estado_dosificacion($id_dosificacion);
            echo 'actualizado';
        }
    }
}

function actualizar_estado_dosificacion($id_dosificacion) {
    $query = "UPDATE Dosificacion set estado = 2 WHERE idDosifi = :id_dosificacion";
    $cmd = Database::getInstance()->getDb()->prepare($query);
    $cmd->bindParam(':id_dosificacion', $id_dosificacion);
    $cmd->execute();
}