<?php
/*
define("HOSTNAME", "localhost");
define("DATABASE", "db_farmacia");
define("USERNAME", "root");
define("PASSWORD", "");
*/
    // Archivo de constantes de la base de datos
    include_once (__DIR__ . "/../extras/config.php");     //'\..\extras\config.php';

    // Archivo contenedor de NotORM
    require_once __DIR__ . "/NotORM.php";

    // Constructor para NotORM
    $dsn = 'mysql:dbname='.DB_DATABASE.';host='.DB_HOSTNAME;
    $pdo_connection = new PDO($dsn, DB_USERNAME, DB_PASSWORD);
    $db = new NotORM($pdo_connection);

    // Conexión utilizada para la paginación
    @mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
    mysql_select_db(DB_DATABASE);


    // Clase conexión para uso de PDO
    class Database
    {
        /**
         * Única instancia de la clase
         */
        private static $db = null;

        /**
         * Instancia de PDO
         */
        private static $pdo;

        final private function __construct()
        {
            try {
                self::getDb();
            } catch (PDOException $e) {
            }
        }
        /**
         * Retorna en la única instancia de la clase
         * @return Database|null
         */
        public static function getInstance()
        {
            if (self::$db === null) {
                self::$db = new self();
            }
            return self::$db;
        }

        /**
         * Crear una nueva conexión PDO basada
         * en los datos de conexión
         * @return PDO Objeto PDO
         */
        public function getDb()
        {
            if (self::$pdo == null) {
                self::$pdo = new PDO(
                    'mysql:dbname=' . DB_DATABASE .
                    ';host=' . DB_HOSTNAME .
                    ';',
                    DB_USERNAME,
                    DB_PASSWORD,
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8' ")
                );
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }

            return self::$pdo;
        }

        /**
         * Evita la clonación del objeto
         */
        final protected function __clone()
        {
        }

        function _destructor()
        {
            self::$pdo = null;
        }
    }
?>