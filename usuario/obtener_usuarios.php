<?php

    require_once '../datos/Database.php';
    require_once '../datos/conexion.php';
    require_once '../extras/PHPPaging.lib/PHPPaging.lib.php';

    if (isset($_GET['val'])) {
        $indicio = $_GET['val'];
        $query_usuarios = "SELECT *
                           FROM Usuario u, Cargo c
                           WHERE u.cargo = c.idCargo AND u.estado = 1 AND
                                 (u.nombre LIKE '%$indicio%' OR u.apellido LIKE '%$indicio%' OR c.cargo LIKE '%$indicio%')
                           ORDER BY u.idUsuario DESC";
    } else {
        $query_usuarios = "SELECT *
                           FROM Usuario u, Cargo c
                           WHERE u.cargo = c.idCargo AND estado = 1
                           ORDER BY u.idUsuario DESC";
    }
    $cmd_usuarios = Database::getInstance()->getDb()->prepare($query_usuarios);
    $cmd_usuarios->execute();

    $pagina = new PHPPaging;
?>
<script src="../extras/sortable/js/sortable.js"></script>
<table class="table sortable-theme-bootstrap table-hover table-bordered" data-sortable>
    <thead>
    <tr>
        <th>NRO.</th>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        <th>TELEFONO</th>
        <th>CELULAR</th>
        <th>CARGO</th>
        <th>OPCIONES</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $nro = 0;
    $pagina->agregarConsulta($query_usuarios);
             $pagina->modo('desarrollo');
             $pagina->verPost(true);
             $pagina->porPagina(20);
             $pagina->paginasAntes(3);
             $pagina->paginasDespues(3);
             $pagina->linkSeparador(" - ");
             $pagina->div('usuarios-activos');
             $pagina->linkSeparadorEspecial('...');
        $pagina->ejecutar();
        while ($row = $pagina->fetchResultado()) :
            $id_usuario = $row['idUsuario'];
            $nro++;     ?>
            <tr>
                <td><?= $nro; ?></td>
                <td><?= $row['nombre']; ?></td>
                <td><?= $row['apellido']; ?></td>
                <td><?= $row['telefonoFijo']; ?></td>
                <td><?= $row['telefonoCel']; ?></td>
                <td><?= $row['cargo']; ?></td>
                <td>
                    <a href="javascript:verUsuario('<?= $id_usuario ?>');"
                       class="btn btn-default btn-xs blue"><i class="fa fa-eye"></i> Ver</a>
                    <a href="modificar_usuario.php?id=<?= $id_usuario; ?>"
                       class="btn btn-default btn-xs purple"><i class="fa fa-edit"></i>
                        Editar</a>
                    <a href="javascript:eliminarUsuario('<?= $id_usuario ?>','<?= $row['nombre']; ?>');"
                       class="btn btn-default btn-xs black"><i class="fa fa-trash-o"></i>
                        Eliminar</a>
                    <a href="recuperar_clave.php?id=<?= $id_usuario ?>"
                       class="btn btn-default btn-xs red"><i class="fa fa-recycle"></i>
                        Recuperar</a>
                </td>
            </tr>
    <?php endwhile; ?>
    </tbody>
    <tfoot>
            <tr>
                <td colspan="7">Número de página: <?= $pagina->fetchNavegacion(); ?></td>
            </tr>
        </tfoot>
</table>