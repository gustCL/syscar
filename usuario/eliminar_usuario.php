<?php

    require_once '../datos/Database.php';

    $id_usuario = $_POST['id'];
    $query = "UPDATE Usuario SET estado = 0 WHERE idUsuario = :id_usuario";
    $cmd_eliminar_usuario = Database::getInstance()->getDb()->prepare($query);
    $cmd_eliminar_usuario->bindParam(':id_usuario', $id_usuario);
    $cmd_eliminar_usuario->execute();
    exit();
?>
