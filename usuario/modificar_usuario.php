<?php
    require_once '../usuario/head.php';
    require_once '../datos/Database.php';

    $id_usuario = $_GET['id'];
    //$id_usuario = $_POST['id_usuario'];



    $query_select_user = "SELECT u.*, c.cargo
                             FROM Usuario u, Cargo c
                             WHERE u.cargo = c.idCargo AND idUsuario = :id_usuario";
    $cmd_select_user = Database::getInstance()->getDb()->prepare($query_select_user);
    $cmd_select_user->bindParam(':id_usuario', $id_usuario);
    $cmd_select_user->execute();
    $result = $cmd_select_user->fetch();

    $query_privilegio = "SELECT idCasoUso FROM AsignaPrivilegioCasoUso WHERE idUsuario = :id_usuario";
    $privilegio = Database::getInstance()->getDb()->prepare($query_privilegio);
    $privilegio->bindParam(':id_usuario', $id_usuario);
    $privilegio->execute();

   $i = 0;
   $dar = [];
   while ($idCU = $privilegio->fetch()) {
       $dar[$i] = $idCU['idCasoUso'];
       $i++;
   }
   ?>
   <div class="page-content">
       <!-- subtTitulo -->
       <div class="page-header position-relative">
           <div class="header-title">
               <h1><strong>MODIFICAR USUARIO</strong></h1>
           </div>
           <!--Header Buttons-->
           <div class="header-buttons">
               <a class="sidebar-toggler" href="#">
                   <i class="fa fa-arrows-h"></i>
               </a>
               <a class="refresh" id="refresh-toggler" href="#">
                   <i class="glyphicon glyphicon-refresh"></i>
               </a>
               <a class="fullscreen" id="fullscreen-toggler" href="#">
                   <i class="glyphicon glyphicon-fullscreen"></i>
               </a>
           </div>
           <!--Header Buttons End-->
       </div>
       <div class="page-body">
           <div class="widget">
               <form name="frm-modificar-usuario" id="frm-modificar-usuario" action="javascript: actualizarUsuario();"
                     class="form-horizontal">
                   <div class="widget-body">
                       <h2><center><strong>INFORMACIÓN BÁSICA</strong></center></h2>
                       <br>
                       <div class="form-group">
                           <input name="idUsuario" value="<?= $result['idUsuario']; ?>" hidden>
                           <label class="col-md-2 control-label lead">Nombre</label>
                           <div class="col-md-4">
                               <input type="text" class="form-control" name="nombre" title="INGRESE NOMBRE"
                                      placeholder="Ingrese nombre" value="<?= $result['nombre'] ?>" required>
                           </div>
                           <label class="col-md-1 control-label lead">Apellido</label>
                           <div class="col-md-4">
                               <input type="text" class="form-control" name="apellido" title="INGRESE APELLIDO"
                                      placeholder="Ingrese apellido" value="<?= $result['apellido'] ?>" required>
                           </div>
                       </div>

                       <div class="form-group">
                           <label class="col-md-2 control-label lead">Teléfono</label>
                           <div class="col-md-4">
                               <input type="text" class="form-control" name="telefonoFijo"
                                      placeholder="Nro. de teléfono"
                                      value="<?= $result['telefonoFijo'] ?>" maxlength="8" onKeypress="if (event.keyCode < 45 || event.keyCode > 57)
                                                        event.returnValue = false;">
                           </div>
                           <label class="col-md-1 control-label lead">Celular</label>
                           <div class="col-md-4">
                               <input type="text" class="form-control" name="telefonoCel" placeholder="Nro. de celular"
                                      value="<?= $result['telefonoCel'] ?>" maxlength="8" onKeypress="if (event.keyCode < 45 || event.keyCode > 57)
                                                        event.returnValue = false;">
                           </div>
                       </div>

                       <div class="form-group">
                           <label class="col-md-2 control-label lead">Dirección</label>
                           <div class="col-md-4">
                               <input class="form-control" title="INGRESE DIRECCION DOMICILIAR" name="direccion"
                                      type="text"
                                      placeholder="Ingrese direccion domiciliar" value="<?= $result['direccion'] ?>"/>
                           </div>
                           <label class="col-md-1 control-label lead">Cargo</label>
                           <div class="col-md-4">
                               <?php
                               $cmd_cargo = Database::getInstance()->getDb()->prepare("select * from Cargo");
                               $cmd_cargo->execute();
                               ?>
                               <select class="form-control" name="cargo" data-bv-field="country">
                                   <?php
                                   while ($row_privilegio = $cmd_cargo->fetch()) {
                                       $selected = "";
                                       if ($row_privilegio['cargo'] == $result['cargo']) {
                                           $selected = 'selected';
                                       }
                                       ?>
                                       <option
                                           value="<?= $row_privilegio['idCargo'] ?>" <?= $selected ?> ><?= $row_privilegio['cargo'] ?></option>
                                   <?php } ?>
                               </select><i class="form-control-feedback" data-bv-field="country"
                                           style="display: none;"></i>
                           </div>
                       </div>

                       <div class="form-group">
                           <label class="col-md-2 control-label lead">Email</label>
                           <div class="col-md-4">
                               <input class="form-control" name="email" id="email" type="email"
                                      placeholder="Ingrese correo electronico" value="<?= $result['email'] ?>">
                           </div>
                           <label class="col-md-1 control-label lead">CI</label>
                           <div class="col-md-4">
                               <input type="text" title="INGRESE NRO. CARNET" class="form-control" name="ci" id="ci"
                                      placeholder="Ingrese cedula de identidad" value="<?= $result['ci'] ?>" required>
                           </div>
                       </div>

                       <div class="form-group">
                           <label class="col-md-2 control-label lead">Descuento (%)</label>
                           <div class="col-md-4">
                               <input type="number" min="0" max="100" title="INGRESE DESCUENTO" class="form-control"
                                      name="descuento" id="descuento" placeholder="Descuento de usuario"
                                      value="<?= $result['descuento'] * 100; ?>">
                           </div>
                       </div>
                   </div>
                   <br>
                   <div class="widget-body">
                       <h2><strong><center>Privilegios de Usuario</center></strong></h2>
                       <label>
                           <input type="checkbox" onclick="seleccionarTodo(this);">
                           <span class="text" style="font-size: 20px;">Seleccionar todos los privilegios</span>
                       </label>
                   </div>
                   <div class="checkbox">
                       <?php
                       $dato1 = Database::getInstance()->getDb()->prepare("SELECT DISTINCT p.idModulo, p.nombreModulo
																						FROM AsignarModuloCasoUso a, PrivilegiosModulo p
																						WHERE p.idModulo=a.idModulo");
                       $dato1->execute();

                       while ($row = $dato1->fetch()) {
                           $cmd_select_user = Database::getInstance()->getDb()->prepare("SELECT p.idCasoUso, p.nombreCasoUso
																									  FROM AsignarModuloCasoUso a, PrivilegiosCasoUso p
																									  WHERE a.idModulo='$row[idModulo]' AND p.idCasoUso = a.idCasoUso");
                           $cmd_select_user->execute();
                           ?>
                           <div class="widget-body">
                               <!--label>
                                                <h5><span class="text"><?= $row['nombreModulo'] ?></span></h5>
                                            </label><br-->
                               <div>
                                   <label
                                       style="font-size: 20px;  padding-right: 15px;"><?= $row['nombreModulo']; ?></label>
                                   <label>
                                       <input type="checkbox" class="<?= $row['nombreModulo']; ?>"
                                              onclick="seleccionarModulo(this);">
                                       <span class="text">Seleccionar Todo</span>
                                   </label>
                               </div>
                               <br>
                               <?php
                               while ($row1 = $cmd_select_user->fetch()) {
                                   $checked = "";
                                   if (in_array($row1['idCasoUso'], $dar)) {
                                       $checked = 'checked';
                                   }
                                   ?>
                                   <label>
                                       <!--input type="checkbox" name="casouso[]" value="<?= $row1['idCasoUso'] ?>" <?= $checked ?> >
                                                    <span class="text"><?= $row1['nombreCasoUso'] ?></span-->

                                       <input type="checkbox" class="<?= $row['nombreModulo']; ?>" name="casouso[]"
                                              value="<?= $row1['idCasoUso'] ?>" <?= $checked ?> >
                                       <span class="text"><?= $row1['nombreCasoUso'] ?></span>
                                   </label>
                               <?php } ?>
                           </div><br>
                       <?php } ?>
                   </div>
                   <div class="widget-body">
                       <center>
                           <div class="form-inline">
                               <div class="input-group">
                                   <input id="btn-guardar" class="btn btn-primary" type="submit" value="Guardar">
                               </div>
                               <div class="input-group">
                                   <a id="btn-cancelar" type="button" href="index.php"
                                      class="btn btn-danger">Cancelar</a>
                               </div>
                           </div>
                       </center>
                   </div>
               </form>
           </div>
       </div>
   </div>
   <?php require_once '../header_footer/footer.php'; ?>

