<?php

    require_once("../datos/Database.php");

    $nombre = $_REQUEST['nombre'];
    $apellido = $_REQUEST['apellido'];
    $usuario = $_REQUEST['nombreusuario'];
    $password = $_REQUEST['password'];
    $telefono = $_REQUEST['telefono'];
    $celular = $_REQUEST['celular'];
    $direccion = $_REQUEST['direccion'];
    $cargo = $_REQUEST['cargo'];
    $email = $_REQUEST['email'];
    $carnet = $_REQUEST['carnet'];
    $descuento_porcentual = $_REQUEST['descuento'];
    $casouso = $_REQUEST['casouso'];

    $descuento_decimal = $descuento_porcentual / 100;
/*
$nombre = $_POST['nombre'];
$apellido = $_POST['apellido'];
$usuario = $_POST['nombreusuario'];
$password = $_POST['password'];
$telefono = $_POST['telefono'];
$celular = $_POST['celular'];
$direccion = $_POST['direccion'];
$cargo = $_POST['cargo'];
$email = $_POST['email'];
$carnet = $_POST['carnet'];
$descuento = $_POST['descuento'];
$casouso = $_POST['casouso'];
*/

    date_default_timezone_set('America/La_Paz');
    $fecha_registro = date("Y-m-d");
    $hora_registro = date("H:i:s");

    // Verificar si NO existe otro usuario con el mismo nombre de usuario
    $query_exists_username = "SELECT usuario FROM Usuario WHERE usuario = :username AND estado = 1";
    $cmd_exists_username = Database::getInstance()->getDb()->prepare($query_exists_username);
    $cmd_exists_username->bindParam(':username', $usuario);
    $cmd_exists_username->execute();
    $row_count_exists_username = $cmd_exists_username->rowCount();

    if ($row_count_exists_username) {
        echo 'error_1'; // Nombre de usuario en uso
        exit();
    } else {    // Nombre de usuario disponible
        $cmd_insert_user = Database::getInstance()->getDb()->prepare("INSERT INTO Usuario
                                                                       VALUES(0,  :carnet,
                                                                                  :usuario,
                                                                                  :nombre,
                                                                                  :apellido,
                                                                                  :password_encrypt,
                                                                                  :telefono,
                                                                                  :celular,
                                                                                  :cargo,
                                                                                  :direccion,
                                                                                  :email,
                                                                                  :fecha_registro,
                                                                                  :hora_registro,
                                                                                  :descuento ,1,1)");

        $cmd_insert_user->bindParam(':carnet', $carnet);
        $cmd_insert_user->bindParam(':usuario', $usuario);
        $cmd_insert_user->bindParam(':nombre', $nombre);
        $cmd_insert_user->bindParam(':apellido', $apellido);
        $cmd_insert_user->bindParam(':password_encrypt', md5($password));
        $cmd_insert_user->bindParam(':telefono', $telefono);
        $cmd_insert_user->bindParam(':celular', $celular);
        $cmd_insert_user->bindParam(':cargo', $cargo);
        $cmd_insert_user->bindParam(':direccion', $direccion);
        $cmd_insert_user->bindParam(':email', $email);
        $cmd_insert_user->bindParam(':fecha_registro', $fecha_registro);
        $cmd_insert_user->bindParam(':hora_registro', $hora_registro);
        $cmd_insert_user->bindParam(':descuento', $descuento_decimal);
        $cmd_insert_user->execute();

        // Obtener el ID del usuario registrado
        $query_select_last_user =  Database::getInstance()->getDb()->prepare("Select Max(idUsuario) As idUsuario from Usuario");
        $query_select_last_user->execute();
        $count = $query_select_last_user->fetch(PDO::FETCH_ASSOC);
        $result = $count['idUsuario'];

        for ($index = 0; $index < count($casouso); $index++) {
            $value= $casouso[$index];

            $CONSULT = "INSERT INTO AsignaPrivilegioCasoUso VALUES ('$result','$value')";
            $date =  Database::getInstance()->getDb()->prepare($CONSULT);
            $date->execute();
        }

        //header('Location: ../usuario');
        echo 'exito_1';
        exit();
    }

?>