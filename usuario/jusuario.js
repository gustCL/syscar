﻿$(document).ready(function() {
    obtenerUsuarios();
});

$(function(){
    $('#buscar-usuario').keyup(function(){
        var val = $(this).val();
        val = val.replace(/ /g, "%20");
        buscarUsuario(val);
    });
});

function fn_paginar(var_div, url) {
    var div = $("#" + var_div);
    $(div).load(url);
}

// Cargar la tabla de usuarios activos
function obtenerUsuarios() {
    $('#usuarios-activos').load('obtener_usuarios.php');
}

// Busca usuarios en base a parámetros
function buscarUsuario(val) {
    $('#usuarios-activos').load('obtener_usuarios.php?val='+val);
}

// Funcion para eliminar el usuario
function eliminarUsuario(id, nombre) {
    swal({
            title: "Eliminar Usuario",
            text: "Está a punto de eliminar los registros de "+nombre+". ¿Desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: 'eliminar_usuario.php',
                    data: 'id=' + id,
                    success: function (result) {
                        swal("Usuario eliminado!", "El usuario ha sido removido de la base de datos", "success");
                        obtenerUsuarios();
                    }
                });
            } else {
                swal("Operación cancelada", "Usuario no eliminado", "error");
            }
        }
    );
}

// Función para ver la información del usuario
function verUsuario(id) {
    $('#formula')[0].reset();
    var url = 'ver_usuario.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function (valores) {
            var datos = eval(valores);
            $('#ci').val(datos[0]);
            $('#nombre').val(datos[2]);
            $('#apellido').val(datos[3]);
            $('#telefonoFijo').val(datos[4]);
            $('#telefonoCel').val(datos[5]);
            $('#cargo').val(datos[6]);
            $('#direccion').val(datos[7]);
            $('#email').val(datos[8]);
            $('#descuento').val(datos[9]);
            $('#ver-usuario').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;
}

// Genera una nueva clave para un usuario y la actualiza en la base de datos
function recuperarClave() {
    var frm = $("#reset-password").serialize();
    $.ajax({
        url: 'generar_nueva_clave.php',
        data: frm,
        type: 'post',
        success: function (valor) {
            var datos = eval(valor);
            $("#exito").show();
            $('#new-password').val(datos[0]);
            $('#btn-cancelar').html('Salir');
        }
    });
}

// Actualiza la clave de un usuario
function cambiarClave() {
    var pass_1 = document.getElementById('new-password-1');
    var pass_2 = document.getElementById('new-password-2');
    if (pass_1.value == pass_2.value) {

        pass_1.style.border = null;
        pass_2.style.border = null;

        var frm = $("#change-password").serialize();
        $.ajax({
            url: 'cambiar_clave.php',
            data: frm,
            type: 'post',
            success: function (result) {
                if (result == 'exito_1') {
                    $("#exito").show();
                    $("#error").hide();
                    $('#pass-diferentes').hide();

                    document.getElementById('nombreusuario').readOnly = true;
                    document.getElementById('password').readOnly = true;
                    pass_1.readOnly = true;
                    pass_2.readOnly = true;

                    document.getElementById('btn-guardar').isDisabled = true;
                    document.getElementById('btn-cancelar').innerHTML = "Salir";
                } else {
                    $("#error").show();
                    $("#exito").hide();
                    $('#pass-diferentes').hide();
                }
            }
        });
    } else {
        pass_1.style.border = '2px solid #da4517';
        pass_2.style.border = '2px solid #da4517';
        $("#exito").hide();
        $("#error").hide();
        $('#pass-diferentes').show();
    }
}

// Registra un nuevo usuario en base de datos
function registrarUsuario() {
    var frm = $("#frm-registrar-usuario").serialize();
    $.ajax({
        url: 'validar_usuario.php',
        data: frm,
        type: 'post',
        success: function (result) {
            if (result == 'error_1') {
                $("#usuario-registrado").hide();
                $("#usuario-existe").show();
                document.getElementById('nombreusuario').style.border = '2px solid #da4517';
                swal("ERROR", "Nombre de usuario en uso.", "error")
            } else {
                $("#usuario-existe").hide();
                $("#usuario-registrado").show();
                swal("Usuario Registrado", "Usuario registrado satisfactoriamente.", "success")
            }
        }
    });
}

// Actualiza la información del usuario
function actualizarUsuario() {
    var frm = $("#frm-modificar-usuario").serialize();
    $.ajax({
        url: 'actualizar_usuario.php',
        data: frm,
        type: 'post',
        success: function (result) {
            if (result == 'exito') {
                swal("Usuario Modificado", "Información y privilegios de usuario actualizados satisfactoriamente.", "success")
                $('#btn-cancelar').html('Salir');
            } else {
                swal("ERROR", "Ocurrió un error al intentar modificar el usuario. Intente nuevamente.", "error")
            }
        }
    });
}

// Selecciona todos los privilegios que están asignados a un módulo
function seleccionarModulo(cbx) {
    var className = cbx.className;
    var checked = cbx.checked;
    var lst = document.getElementsByClassName(className);
    for (var i = 0; i < lst.length; i++) {
        lst[i].checked = checked;
    }
}

// Selecciona TODOS los privilegios de TODOS los módulos
function seleccionarTodo(cbx) {
    var checked = cbx.checked;
    var elementos = document.getElementsByTagName('input');
    for (var i = 0; i < elementos.length; i++) {
        elementos[i].checked = checked;
    }
}

function editarUsuario(id) {
    alert('asd');
    $.post({
        url: 'modificar_usuario.php',
        data: {id_usuario: id}
    });
}