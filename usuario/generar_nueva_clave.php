<?php

    require_once("../datos/Database.php");
    $id_usuario = $_POST['id_usuario'];

    try {

        // Genera un código al azar, compuesto por números y letras
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $new_password = substr( str_shuffle( $chars ), 0, 10 );

        $query_update = "UPDATE Usuario
                         SET clave = :new_password
                         WHERE idUsuario = :id_usuario";
        $cmd_update = Database::getInstance()->getDb()->prepare($query_update);
        $cmd_update->bindParam(':new_password', md5($new_password));
        $cmd_update->bindParam(':id_usuario', $id_usuario);
        $cmd_update->execute();

        $datos = array(
            0 => $new_password,
        );
        echo json_encode($datos);
    }
    catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }




