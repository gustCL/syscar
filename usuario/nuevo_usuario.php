<?php
    require_once ('head.php');
    require_once('../datos/Database.php');
?>

<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>NUEVO USUARIO</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>

        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="page-body">
        <div class="widget">
            <form id="frm-registrar-usuario" action="javascript: registrarUsuario();" class="form-horizontal">
                <div class="widget-body">
                    <h2><center><strong>INFORMACIÓN DE NUEVO USUARIO</strong></center></h2>
                    <br>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-4">
                            <input type="text" title="INGRESE NOMBRE" class="form-control" name="nombre" id="nombre"
                                   placeholder="Nombre" REQUIRED/>
                        </div>

                        <label class="col-lg-2 control-label">Apellido:</label>
                        <div class="col-lg-4">
                            <input type="text" title="INGRESE APELLIDO" class="form-control" name="apellido"
                                   id="apellido" placeholder="Apellido" REQUIRED/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Usuario:</label>
                        <div class="col-lg-4">
                            <input type="text" title="INGRESE NOMBRE DE USUARIO" class="form-control"
                                   name="nombreusuario" id="nombreusuario" placeholder="Nombre de usuario" REQUIRED/>
                        </div>

                        <label class="col-lg-2 control-label">Clave:</label>
                        <div class="col-lg-4">
                            <input type="password" title="INGRESE CLAVE DE SEGURIDAD" class="form-control"
                                   name="password" id="password" placeholder="Clave" REQUIRED/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Teléfono:</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="telefono" id="telefono"
                                   placeholder="Ingrese nro de celular" maxlength="8" onKeypress="if (event.keyCode < 45 || event.keyCode > 57)
                                                        event.returnValue = false;"/>
                        </div>

                        <label class="col-lg-2 control-label">Celular:</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="celular" id="celular"
                                   placeholder="Ingrese nro de telefono" maxlength="8" onKeypress="if (event.keyCode < 45 || event.keyCode > 57)
                                                        event.returnValue = false;"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Dirección:</label>
                        <div class="col-lg-4">
                            <input class="form-control" title="INGRESE DIRECCION DOMICILIAR" name="direccion"
                                   id="direccion" type="text" placeholder="Ingrese direccion domiciliar" />
                        </div>

                        <label class="col-lg-2 control-label">Cargo:</label>
                        <div class="col-lg-4">
                            <select class="form-control" name="cargo" id="cargo">
                            <?php
                                    $cargos = $db->cargo();
                                    foreach ($cargos as $cargo) : ?>
                                        <option value="<?= $cargo['idCargo']; ?>"><?= $cargo['cargo'] ?></option>
                            <?php   endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Email:</label>
                        <div class="col-lg-4">
                            <input class="form-control" name="email" id="email" type="email"
                                   placeholder="Ingrese correo electronico"
                                   data-bv-emailaddress="true"
                                   data-bv-emailaddress-message="Ingrese el correo electronico"/>
                        </div>

                        <label class="col-lg-2 control-label">Cédula de Identidad:</label>
                        <div class="col-lg-4">
                            <input type="text" title="INGRESE NRO. DE CARNET" class="form-control" name="carnet"
                                   id="carnet" placeholder="Cédula de Identidad" REQUIRED/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Descuento (%):</label>
                        <div class="col-lg-4">
                            <input class="form-control" name="descuento" id="descuento" type="number"
                                   value="0" max="100" min="0" placeholder="Porcentaje de descuento"/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="widget-body">
                    <center><label style="font-size: 25px;"><strong>PRIVILEGIOS DE USUARIO</strong></label></center>
                    <label>
                        <input type="checkbox" onclick="seleccionarTodo(this);">
                        <span class="text" style="font-size: 20px;">Seleccionar todos los privilegios</span>
                    </label>
                </div>
                <div class="checkbox">

                    <?php
                    $dato1 = Database::getInstance()->getDb()->prepare("select distinct p.idModulo , p.nombreModulo from AsignarModuloCasoUso a , PrivilegiosModulo p where p.idModulo=a.idModulo ");
                    $dato1->execute();


                    while ($row = $dato1->fetch()) {
                        $dato = Database::getInstance()->getDb()->prepare("select DISTINCT p.idCasoUso , p.nombreCasoUso from AsignarModuloCasoUso a , PrivilegiosCasoUso p where a.idModulo='$row[idModulo]'  and p.idCasoUso= a.idCasoUso");
                        $dato->execute();
                        ?>
                        <div class="widget-body">
                            <div>
                                <label style="font-size: 20px;  padding-right: 15px;"><?= $row['nombreModulo']; ?></label>
                                <label>
                                    <input type="checkbox" class="<?= $row['nombreModulo']; ?>" onclick="seleccionarModulo(this);">
                                    <span class="text">Seleccionar Todo</span>
                                </label>
                            </div>
                            <br>
                            <?php
                            while ($row1 = $dato->fetch()) {
                                ?>
                                <label>
                                    <input type="checkbox" class="<?=$row['nombreModulo']; ?>" name="casouso[]" value="<?= $row1['idCasoUso'] ?>">
                                    <span class="text"><?= $row1['nombreCasoUso'] ?></span>
                                </label>
                            <?php } ?>
                        </div><br>
                    <?php } ?>
                </div>

                <div id="usuario-existe" class="alert alert-danger" role="alert" hidden>
                    <center>
                        <h2>Nombre de Usuario ya existe</h2>
                    </center>
                </div>
                <div id="usuario-registrado" class="alert alert-info" role="alert" hidden>
                    <center>
                        <label>Usuario registrado con éxito</label>
                    </center>
                </div>

                <center>
                    <div class="widget-body">
                        <div class="form-inline">
                            <div class="input-group">
                                <input class="btn btn-primary" type="submit" value="Guardar Datos"/>
                            </div>

                            <div class="input-group">
                                <a id="btn-cancelar" type="button" href="index.php" class="btn btn-danger">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </center>
            </form>
        </div>

    </div>
</div>


<!-- /Page Body -->

<!-- /Page Content -->

<!-- /Page Container -->
<!-- Main Container -->

<?php
require_once ('../header_footer/footer.php');
?>  

