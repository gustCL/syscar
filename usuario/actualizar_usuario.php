<?php
    require_once("../datos/Database.php");
    
    try {
        // Recuperamos los datos del formulario
        $id_usuario = $_REQUEST['idUsuario'];
        $nombre = $_REQUEST['nombre'];
        $apellido = $_REQUEST['apellido'];
        $telefono = $_REQUEST['telefonoFijo'];
        $celular = $_REQUEST['telefonoCel'];
        $direccion = $_REQUEST['direccion'];
        $cargo = $_REQUEST['cargo'];
        $email = $_REQUEST['email'];
        $carnet = $_REQUEST['ci'];
        $descuento = $_REQUEST['descuento']/100; // Descuento porcentual
        $casouso = $_REQUEST['casouso'];

        $query = "UPDATE Usuario
                  SET ci = :ci,
                      nombre = :nombre,
                      apellido = :apellido,
                      telefonoFijo = :telefono_fijo,
                      telefonoCel = :telefono_celular,
                      cargo = :cargo,
                      direccion = :direccion,
                      email = :email,
                      descuento = :descuento
                  WHERE idUsuario = :id_usuario";
        $cmd_update_usuario = Database::getInstance()->getDb()->prepare($query);
        $cmd_update_usuario->bindParam(':ci', $carnet);
        $cmd_update_usuario->bindParam(':nombre', $nombre);
        $cmd_update_usuario->bindParam(':apellido', $apellido);
        $cmd_update_usuario->bindParam(':telefono_celular', $celular);
        $cmd_update_usuario->bindParam(':telefono_fijo', $telefono);
        $cmd_update_usuario->bindParam(':cargo', $cargo);
        $cmd_update_usuario->bindParam(':direccion', $direccion);
        $cmd_update_usuario->bindParam(':email', $email);
        $cmd_update_usuario->bindParam(':descuento', $descuento);
        $cmd_update_usuario->bindParam(':id_usuario', $id_usuario);
        $cmd_update_usuario->execute();

        // Actualizamos los privilegios del usuario
        $query_privilegio = "DELETE FROM AsignaPrivilegioCasoUso WHERE idUsuario = :id_usuario";
        $cmd_privilegio = Database::getInstance()->getDb()->prepare($query_privilegio);
        $cmd_privilegio->bindParam(':id_usuario', $id_usuario);
        $cmd_privilegio->execute();

        for ($index = 0; $index < count($casouso); $index++) {
            $value = $casouso[$index];
            $consulta = "INSERT INTO AsignaPrivilegioCasoUso VALUES ('$id_usuario','$value')";
            $date =  Database::getInstance()->getDb()->prepare($consulta);
            $date->execute();
        }
        
        echo 'exito';
        exit();
    } catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

    
?>
