<?php

    require_once("../datos/Database.php");

    $id = htmlspecialchars($_POST['id']);

    //OBTENEMOS LOS VALORES DEL USUARIO
    $cmd = Database::getInstance()->getDb()->prepare("SELECT u.ci, u.usuario, u.nombre, u.apellido, u.telefonoFijo, u.telefonoCel, c.cargo, u.direccion, u.email, u.descuento
                                                      FROM Usuario u, Cargo c
                                                      WHERE u.cargo = c.idCargo and idUsuario=:id_usuario");
    $cmd->bindParam(':id_usuario', $id);
    $cmd->execute();
    $result = $cmd->fetch();
    $datos = array(
                    0 => $result['ci'],
                    1 => $result['usuario'],
                    2 => $result['nombre'],
                    3 => $result['apellido'],
                    4 => $result['telefonoFijo'],
                    5 => $result['telefonoCel'],
                    6 => $result['cargo'],
                    7 => $result['direccion'],
                    8 => $result['email'],
                    9 => $result['descuento']*100,
                );
    echo json_encode($datos);
?>
