<?php
    require_once 'head.php';
?>
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>USUARIOS</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="widget-body">
                <center><h3><strong>LISTA DE USUARIOS</strong></h3></center>
                <br>
                <div class="form-inline">
                    <div class="form-group">
                        <div class="col-lg-3">
                            <span class="input-icon">
                                <input type="text" size="40" class="form-control"
                                       placeholder="Busca un usuario por Nombre, Apellido o Cargo"
                                       id="buscar-usuario">
                                <i class="glyphicon glyphicon-search circular blue"></i>
                            </span>
                        </div>
                    </div>
                    <a href="nuevo_usuario.php" class="btn btn-primary">Agregar Usuario</a>
                </div>
                <br>
                <div class="table-responsive" id="usuarios-activos">
                </div>
                <br>
            </div>
        </div>
    </div>

    <!-- Modal para ver información de Usuario -->
    <div class="modal fade" id="ver-usuario" tabindex="-1" role="dialog" aria-
         labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-
                            hidden>&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><b> Información de
                            Usuario</b></h4>
                </div>

                <form id="formula" class="formulario">
                    <div class="modal-body">
                        <center>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <table style="border-collapse:separate; border-spacing: 5px; width: 100%;">
                                        <tr>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label no-padding-right" for="nombre">Nombre</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input class="form-control"
                                                               type="text" id="nombre" readonly>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label no-padding-right" for="apellido">Apellido</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input class="form-control"
                                                               id="apellido" readonly>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label no-padding-right" for="direccion ">Dirección</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input class="form-control"
                                                               id="direccion" readonly>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label no-padding-right" for="ci">Cédula de Identidad</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input class="form-control"
                                                               id="ci" readonly>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label no-padding-right" for="telefonoC">Celular</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input class="form-control"
                                                               type="text" id="telefonoCel" readonly>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label no-padding-right" for="telefonoF">Teléfono Fijo</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input class="form-control"
                                                               type="text" id="telefonoFijo" readonly>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label no-padding-right" for="inputEmail3">Cargo</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input class="form-control"
                                                               type="text" id="cargo" readonly>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label no-padding-right" for="email">Email</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input class="form-control"
                                                               id="email" readonly>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label no-padding-right" for="descuento">Descuento (%)</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <input class="form-control"
                                                               type="text" id="descuento" readonly>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <br>
                                    <!--BOTONES-->
                                    <br>
                                    <footer>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <button data-bb-handler="cancel" type="button" class="btn btn-danger"
                                                        data-dismiss="modal" aria-hidden="true">Cerrar
                                                </button>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </center>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    require_once ('../header_footer/footer.php');
?>




