<?php
    require_once ('head.php');
    require_once('../datos/Database.php');

    $id_usuario = $_GET['id'];

    $query_select_usuario = "SELECT usuario FROM Usuario WHERE idUsuario = :id_usuario";
    $cmd_select_usuario = Database::getInstance()->getDb()->prepare($query_select_usuario);
    $cmd_select_usuario->bindParam(':id_usuario', $id_usuario);
    $cmd_select_usuario->execute();

    $result = $cmd_select_usuario->fetch();
    $username = $result['usuario'];
?>
    <div class="page-content">
        <!-- subtTitulo -->
        <div class="page-header position-relative">
            <div class="header-title">
                <h1><strong>RECUPERAR CONTRASEÑA</strong></h1>
            </div>
            <!--Header Buttons-->
            <div class="header-buttons">
                <a class="sidebar-toggler" href="#">
                    <i class="fa fa-arrows-h"></i>
                </a>
                <a class="refresh" id="refresh-toggler" href="#">
                    <i class="glyphicon glyphicon-refresh"></i>
                </a>
                <a class="fullscreen" id="fullscreen-toggler" href="#">
                    <i class="glyphicon glyphicon-fullscreen"></i>
                </a>
            </div>

            <!--Header Buttons End-->
        </div>
        <!-- /Page Header -->
        <!-- Page Body -->
        <div class="page-body">
            <div class="widget">
                <form id="reset-password" action="javascript: recuperarClave();" class="form-horizontal">
                    <center>
                        <div class="widget-body">
                             <div class="form-inline">
                                <div>
                                    <h3>Usuario:</h3>
                                    <label style="font-size: 50px;" type="text"><?= $username;?></label>
                                </div>
                                    <input type="hidden" class="form-control" name="id_usuario"
                                           id="id_usuario" value=<?= $id_usuario; ?>>
                             </div>
                        </div>
                    </center>
                    <br>
                    <div id="exito" class="alert alert-info" role="alert">
                        <center>
                            <h1>Nueva Contraseña:</h1>
                            <div class="form-inline"><input type="text" class="form-control"
                                                            name="new-password"
                                                            id="new-password" value=""></div>
                            <br>
                            <h4>Esta es una clave temporal. Copie y entregue esta clave al usuario
                                correspondiente. Una vez iniciada la sesión, el mismo deberá cambiar
                                esta clave por una de su preferencia.</h4>
                        </center>
                    </div>
                    <center>
                        <div class="widget-body">
                            <div class="form-inline">
                                <div class="input-group">
                                    <input id="btn-guardar" class="btn btn-primary" type="submit"
                                           value="Recuperar Contraseña"/>
                                </div>

                                <div class="input-group">
                                    <a id="btn-cancelar" type="button" href="../usuario/index.php"
                                       class="btn btn-danger">Cancelar</a>
                                </div>
                            </div>
                        </div>
                    </center>
                </form>
            </div>
        </div>
    </div>
<?php
    require_once ('../header_footer/footer.php');
?>