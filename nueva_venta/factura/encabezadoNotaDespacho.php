<!--<script language="JavaScript">
    $(document).ready(function () {
        doPrint();
    });
    function doPrint() {
        //document.all.item("mostrarUser").style.visibility='visible'; 
        window.print()();
        //document.all.item("mostrarUser").style.visibility='hidden';
    }
    function volver(dato) {
        location.href = "../index.php";
    }
</script>-->
<?php


?>
<style type="text/css">
    @media all {
        div.saltopagina {
            display: none;
        }
    }

    @media print {
        div.saltopagina {
            display: block;
            page-break-before: always;
        }
    }
</style>
<div class="saltopagina">

</div>
<div id="noprint">
    <table>
        <tr>
            <td>
                <button type="button" name="volver" id="volver" onclick="volver()">Volver</button>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>
                <button type="button" onclick="doPrint()">Imprimir</button>
            </td>
        </tr>
    </table>
</div>
<div style="width: 100%;height: 10%; /*border: 1px solid red*/">
    <div style="float: left; width: 35%; /*border: 1px solid green*/">
        <div style="width: auto; height: 42%; /*border: 1px solid darkmagenta">

        </div>

    </div>
    <div style="text-align: center; padding-top: 5%; /*border: 1px solid black;*/height: 80%;  float: left; width: 29%">
        <label style="font-family: Arial; font-size: 13pt; color: red;"><strong>NOTA DE ENTREGA</strong></label><br>
        <label style="color: red;font-size: 14pt">Nº. 00<?= $idDestino ?></label>
    </div>
    <div style="float: right; padding-left: 1%; width: 34%;padding-top: 5%;/*border: 1px solid blue">
        <table cellpadding="3" cellspacing="4"
               style="font-size: 9pt; float: right; width: 75%; text-align: right; border:1.5px solid black; border-radius: 6px; margin-bottom: 1%">
            <tbody>
            <tr>
                <td align="left">
                    <label for="user_name"><strong>NIT&nbsp;&nbsp;: </strong></label>
                </td>
                <td align="center">
                    <label for="user_name"
                           style="font-size: 13pt"><strong><?= $nitEmpr ?></strong></label>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div style="padding-left: 1%;float: left;">
        <table>
            <tr>
                <td style="text-align: center">
                    <center>
                        <strong> <label for="" style="color: red;font-size: 10pt">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;
                                ORIGINAL </label></strong>
                    </center>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <strong></strong>Santa Cruz, <?= $Y ?> de <?= obtener_mes($M) ?> del <?= $D ?>
                </td>
            </tr>
        </table>
    </div>
</div>