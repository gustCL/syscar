<?php
require 'encabezadoFacturaCopia.php';
?>
<table id="tablaD" cellpadding="1" style=" width: 100%; border: 1px solid black; border-radius: 6px">
    <tr>
        <td style="width: 65%">
            <strong>Fecha : </strong>Santa Cruz, <?= $Y ?> de <?= obtener_mes($M) ?> del <?= $D ?>
        </td>

    </tr>
    <tr>
        <td>
            <strong>Señor(es): </strong> <?= $nombCliente ?>
        </td>
        <td style="width: 35%">
            <strong>NIT/CI: </strong> <?= $nitCliente ?>
        </td>
    </tr>
</table>
<br>
<div heigth="50">
    <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt">
        <thead>
        <th style="border-left:1px solid black; border-bottom:1px solid black;border-top:1px solid black; font-size: 9pt">
            CANTIDAD
        </th>
        <th style="border-left:1px solid black;border-bottom:1px solid black;border-top:1px solid black;font-size: 9pt">
            CONCEPTO
        </th>
        <th style="width:15%; border-top: 1px solid black;border-bottom: 1px solid black;border-left :1px solid black;font-size: 9pt">
            PRECIO UNT.
        </th>
        <th style="font-size: 9pt;width:15%; border-top: 1px solid black;border-bottom: 1px solid black;border-left :1px solid black;border-right:1px solid black;">
            SUBTOTAL
        </th>
        </thead>
        <tbody>
        <?php
        // EXTRAEMOS EL DETALLE DE LA VENTA ///
        $sql2 = "SELECT v.idVenta, p.codigoProducto, p.nombreComercial, sum(d.cantidad)cantidad, d.precioVenta, v.montoSubTotal, v.montoTotal, v.descuento, v.efectivoSus, v.efectivoBs, v.cambio "
            . " FROM DetalleTransaccion d, Transaccion t, Ventas v, Producto p "
            . "WHERE d.idTransaccion= t.idTransaccion  and t.idTransaccion = v.idTransaccion and d.codigoProducto = p.codigoProducto "
            . "and t.estado = 1 and v.idVenta =? group by p.codigoProducto";
        $com2 = Database::getInstance()->getDb()->prepare($sql2);
        $com2->execute([$venta['idVenta']]);

        $num = 1;
        while ($detalle = $com2->fetch()) {
            $cantidad = $detalle['cantidad'];
            $descripcion = $detalle['nombreComercial'];
            $precio = $detalle['precioVenta'];
            $subTotal = $cantidad * $precio;
            $pagobs = $detalle['efectivoBs'];
            $pagosus = $detalle['efectivoSus'];
            $cambio = $detalle['cambio'];
            $montot = $detalle['montoTotal'];
            $submonto = $detalle['montoSubTotal'];
            $descuento = $detalle['descuento'];
            if ($cantidad == 0) {
                ?>
                <tr>
                    <td style="font-size: 12px; border-left:1px solid black"></td>
                    <td style="font-size: 12px; border-left:1px solid black; padding-left: 1%">
                        <?= $descripcion ?>
                    </td>
                    <td style="border-left:1px solid black; text-align: center"></td>
                    <td style="border-right :1px solid black;border-left:1px solid black;text-align: center"></td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td style="font-size: 12px; border-left:1px solid black;text-align: center"><?= $cantidad ?></td>
                    <td style="font-size: 12px; border-left:1px solid black;padding-left: 1%">
                        <?= $descripcion ?>
                    </td>
                    <td style="border-left:1px solid black; text-align: center">
                        <?php echo number_format($precio, 2);
                        ?>
                    </td>
                    <td style="border-left:1px solid black;border-right:1px solid black;text-align: center">
                        <?php echo number_format($precio * $cantidad, 2);
                        ?>
                    </td>
                </tr>
                <?php
            }
            $monto = $montot;
            $num++;
        }
        $iva = $monto * 0.13;
        $dato = 6;
        if ($num == 1) {
            ImprimirEspacios($dato);
        } else {
            if ($num > 1) {
                $dato = $dato - $num;
                ImprimirEspacios($dato);
            }
        }

        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"
                style="border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;">
                <strong>Son:</strong> <?php
                echo $valor . " Bolivianos";
                ?>
            </td>
            <td style="font-size:9pt; text-align: right; border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black">
                <strong>TOTAL Bs.:</strong></td>
            <td style="text-align: center;border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;border-right:1px solid black;"><?= number_format($montot, 2) ?></td>
        </tr>
        </tfoot>
    </table>
</div>
<br>
<div style="width:65%; float:left">
    <?php
    $totalbs1 = number_format($montot, 2);
    //                    $sql2 = "insert into ParametrosFac (autorizacion, nroFac, nitCliente, fechaTrans, montoFac, llave, codigoControl) values(?,?,?,?,?,?,?)";
    //                    $com = Database::getInstance()->getDb()->prepare($sql2);
    //                    $com->execute([$autorizacion, $nroFac, $nitCliente, $FechaTransac, $totalbs1, $llave, $cod_con]);
    ///////////////////////////////////////////
    //$nombreDelCliente = $cliente['nombre_cliente'];
    $filename = $PNG_TEMP_DIR . 'test' . $FechaTransac . $nroFac . '.png';
    $datos = $nitEmpr . "|" . $nroFac . "|" . $autorizacion . "|" . $fechainvertida . "|" . $totalbs1 . "|" . $cod_con . "|" . $nitCliente . "|" . $nombCliente;
    //QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);
    QRcode::png($datos, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
    ?>
    <br>
    <table align="left" style="font-size: 9pt; padding-left: 5%" cellpadding="1">
        <thead>
        <tr>
            <td style="border: 1px solid black; border-radius: 5px">
                <strong>Codigo De Control:</strong> <?= $cod_con ?>&nbsp;&nbsp;&nbsp;
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td style="border: 1px solid black;border-radius: 5px">
                <strong>Fecha Limite De Emision:</strong> <?= Invertir_Fecha($fechaLimite) ?>&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        </thead>
    </table>
</div>
<div style="text-align: right; float:right; width: 30%; padding-right: 2%">
    <img width="75" height="75" src="<?= $PNG_WEB_DIR . basename($filename) ?>"/>
</div>
<div>
    <table width="100%" align="left" cellpadding="1">
        <tr>
            <td style="border:1px solid black;font-size: 9; border-radius: 5px;text-align: center">
                <strong>ESTA FACTURA CONTRIBUYE AL DESARROLLO DEL PAÍS. EL USO ILÍCITO DE ÉSTA SERÁ SANCIONADO DE
                    ACUERDO A LEY.</strong>
            </td>
        </tr>
        <tr>
            <td style="font-size: 9;">
                <center>Ley No. 453. Los servicios deben presparse con calidad y responsabilidad.</center>
            </td>
        </tr>
    </table>
</div>
</div>