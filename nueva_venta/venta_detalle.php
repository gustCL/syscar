<?php

require_once '../datos/Database.php';
session_start();
$ses = $_SESSION['userMaster'];

$id = trim($_POST['id']);
$consulta = "SELECT DISTINCT (inv.codigoProducto),inv.nombreComercial AS nombreComercialV,inv.precioVenta AS precioV, inv.nombreSector AS nombreSectorV, pr . * , um . * FROM Inventario inv, UnidadMedida um, Producto pr "
        . "WHERE inv.codigoProducto = '$id' AND inv.codigoProducto=pr.codigoProducto AND "
        . "pr.idUnidad = um.idUnidad";
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$valores = $comando->fetch();

if ($valores['codigoBarras'] == "") {
    $codB = $valores['codigo'];
} else {
    $codB = $valores['codigoBarras'];
}

$datos = array(
    0 => $valores['nombreComercialV'],
    1 => $valores['abreviatura'],
    2 => $valores['precioV'],
    3 => $valores['nombreSectorV'],
    4 => $codB,

);

echo json_encode($datos);
?>
