<?php
require_once '../datos/Database.php';
session_start();
$sucursal = $_SESSION['userMaster'];

$codigo = $_POST['descripcionAgr'];
$codProducto = substr($codigo, 1, 6);

$cons1 = "SELECT tipoProducto FROM Producto WHERE codigoBarras = '$codigo' or codigoBarras = '$codProducto'";
$resultado = Database::getInstance()->getDb()->prepare($cons1);
$resultado->execute();
$datosProducto = $resultado->fetch();
$TipoProducto = $datosProducto['tipoProducto'];

if ($TipoProducto == 0) {

    $idAlm = ($_POST['idAlm']);
    $cons = "SELECT inv.codigoProducto FROM Producto pr, Inventario inv WHERE inv.codigoProducto = pr.codigoProducto AND inv.idAlmacen ='$idAlm' AND inv.idSucursal='" . $sucursal['idSucursal'] . "' AND pr.codigoBarras = '$codigo'";
    $resultado = Database::getInstance()->getDb()->prepare($cons);
    $resultado->execute();
    $id = $resultado->fetch();
    $idCodP = $id['codigoProducto'];
    $consulta = "SELECT DISTINCT (inv.codigoProducto),inv.nombreComercial AS nombreComercialV,inv.precioVenta AS precioV, inv.nombreSector AS nombreSectorV, pr . * , um . * FROM Inventario inv, UnidadMedida um, Producto pr "
        . "WHERE inv.codigoProducto = '" . $idCodP . "' AND inv.codigoProducto=pr.codigoProducto AND "
        . "pr.idUnidad = um.idUnidad";
    $comando = Database::getInstance()->getDb()->prepare($consulta);
    $comando->execute();
    $valores = $comando->fetch();

    if ($valores['codigoBarras'] == "") {
        $codB = $valores['codigo'];
    } else {
        $codB = $valores['codigoBarras'];
    }

    $datos = array(
        0 => $valores['nombreComercialV'],
        1 => $valores['abreviatura'],
        2 => $valores['precioV'],
        3 => $valores['nombreSectorV'],
        4 => $codB,
        5 => $idCodP,
        6 => $TipoProducto,

    );

    echo json_encode($datos);
} else {

    $codProducto = substr($codigo, 1, 6);
    $cantidad = substr($codigo, 6, 6);

    $kilogramo = substr($cantidad, 1, 2);
    $gramos = substr($cantidad, 3, 5);


    $cantidad1 = $kilogramo . "." . $gramos;


    $idAlm = ($_POST['idAlm']);
    $cons = "SELECT inv.codigoProducto FROM Producto pr, Inventario inv WHERE inv.codigoProducto = pr.codigoProducto AND inv.idAlmacen ='$idAlm' AND inv.idSucursal='" . $sucursal['idSucursal'] . "' AND pr.codigoBarras = '$codProducto'";
    $resultado = Database::getInstance()->getDb()->prepare($cons);
    $resultado->execute();
    $id = $resultado->fetch();
    $idCodP = $id['codigoProducto'];
    $consulta = "SELECT DISTINCT (inv.codigoProducto),inv.nombreComercial AS nombreComercialV,inv.precioVenta AS precioV, inv.nombreSector AS nombreSectorV, pr . * , um . * FROM Inventario inv, UnidadMedida um, Producto pr "
        . "WHERE inv.codigoProducto = '" . $idCodP . "' AND inv.codigoProducto=pr.codigoProducto AND "
        . "pr.idUnidad = um.idUnidad";
    $comando = Database::getInstance()->getDb()->prepare($consulta);
    $comando->execute();
    $valores = $comando->fetch();

    if ($valores['codigoBarras'] == "") {
        $codB = $valores['codigo'];
    } else {
        $codB = $valores['codigoBarras'];
    }

    $datos = array(
        0 => $valores['nombreComercialV'],
        1 => $valores['abreviatura'],
        2 => $valores['precioV'],
        3 => $valores['nombreSectorV'],
        4 => $codB,
        5 => $idCodP,
        6 => $cantidad1,

    );

    echo json_encode($datos);
}
?>

