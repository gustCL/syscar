<?php
require_once '../datos/Database.php';

$actual = date("Y-m-d");
$inicioMes = date("Y-m");
$inicioMes = $inicioMes . "-01";
//EMPRESA
$consulta = 'SELECT * FROM Empresa';
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$empresa = $comando->fetch();
//SUCURSAL
$userMaster = $_SESSION['userMaster'];
$consulta1 = "SELECT * FROM Sucursal WHERE idSucursal =?";
$comando1 = Database::getInstance()->getDb()->prepare($consulta1);
$comando1->execute([$userMaster['idSucursal']]);
$sucursal = $comando1->fetch();
//MES LITERAL
/*function obtener_mes($valor) {
    $result = '';
    switch ($valor) {
        case '01':
            $result = 'Enero';
            break;
        case '02':
            $result = 'Febrero';
            break;
        case '03':
            $result = 'Marzo';
            break;
        case '04':
            $result = 'Abril';
            break;
        case '05':
            $result = 'Mayo';
            break;
        case '06':
            $result = 'Junio';
            break;
        case '07':
            $result = 'Julio';
            break;
        case '08':
            $result = 'Agosto';
            break;
        case '09':
            $result = 'Septiembre';
            break;
        case '10':
            $result = 'Octubre';
            break;
        case '11':
            $result = 'Noviembre';
            break;
        case '12':
            $result = 'Diciembre';
            break;
    }
    return $result;
}*/

//OBTENEMOS DATOS DE LA TRANSACCION

$nventa = $_REQUEST['vt'];
$consulta2 = "SELECT * FROM Ventas WHERE idVenta =?";
$comando2 = Database::getInstance()->getDb()->prepare($consulta2);
$comando2->execute([$nventa]);
$venta = $comando2->fetch();
//OBTENEMOS DATOS DE LA NOTA DE VENTA
$consulta4 = "SELECT * FROM VentaSucursal WHERE idVenta =?";
$comando4 = Database::getInstance()->getDb()->prepare($consulta4);
$comando4->execute([$nventa]);
$notaVenta = $comando4->fetch();
//OBTENEMOS DATOS DEL CLIENTE
$consulta3 = "SELECT * FROM Cliente WHERE idCliente =?";
$comando3 = Database::getInstance()->getDb()->prepare($consulta3);
$comando3->execute([$venta['idCliente']]);
$cliente = $comando3->fetch();

$idTransaccion = $venta['idTransaccion'];

$consulta5 = "SELECT Distinct( dt.codigoProducto) AS codigoProducto, pr.nombreComercial FROM DetalleTransaccion dt, Producto pr WHERE dt.idTransaccion =? AND dt.codigoProducto = pr.codigoProducto";
$comando5 = Database::getInstance()->getDb()->prepare($consulta5);
$comando5->execute([$idTransaccion]);

$consulta6 = "SELECT * FROM Transaccion WHERE idTransaccion =? ";
$comando6 = Database::getInstance()->getDb()->prepare($consulta6);
$comando6->execute([$idTransaccion]);
$trans = $comando6->fetch();
// EXTRAEMOS EL DESTINO SI EXISTE
$consultaDes = "SELECT * FROM OrdenDespacho WHERE idTransaccion = ?";
$comandoDes = Database::getInstance()->getDb()->prepare($consultaDes);
$comandoDes->execute([$venta['idTransaccion']]);
$datosDestino = $comandoDes->fetch();
$idDestino = $datosDestino['idDespacho'];
$destino = $datosDestino['destino'];
//INSERTAR DATOS IngresoCaja
$inicio = $_SESSION['idSesion'];
$consultaIC = "INSERT INTO IngresoCaja(idIngresoCaja, idTipoIngresoCaja, nroFactura, nroVenta, glosa, montoTotal, idTipoCambio, tipoCambio, efectivoBs, efectivoSus, fechaRegistro, horaRegistro, estado, idInicio) VALUES ('',1,'',?,?,?,?,?,?,?,?,?,?,?)";
$comandoIC = Database::getInstance()->getDb()->prepare($consultaIC);
$comandoIC->execute([$notaVenta['nroVenta'], 'Venta de productos', $venta['montoTotal'], $venta['idTipoCambio'], $venta['tipoCambio'], $venta['efectivoBs'], $venta['efectivoSus'], $trans['fecha'], $trans['hora'], 1, $inicio]);
//selecionar el id de IngresoCaja
$consultaICs = "SELECT * FROM IngresoCaja WHERE nroVenta='" . $notaVenta['nroVenta'] . "' AND estado=1 AND fechaRegistro='" . $trans['fecha'] . "'";
$comandoICs = Database::getInstance()->getDb()->prepare($consultaICs);
$comandoICs->execute();
$idIngresoCaja = $comandoICs->fetch(PDO::FETCH_ASSOC);
//INSERTAR DATOS IngresoVenta
$consultaIV = "INSERT INTO IngresoVenta (idIngresoCaja, idVenta) VALUES (?,?)";
$comandoIV = Database::getInstance()->getDb()->prepare($consultaIV);
$comandoIV->execute([$idIngresoCaja['idIngresoCaja'], $venta['idVenta']]);
///actividad
$consultaA = "select * FROM Actividad where estado = 1 and idSucursal = ?";
$comandoA = Database::getInstance()->getDb()->prepare($consultaA);
$comandoA->execute([$userMaster['idSucursal']]);
$datos_actividad = $comandoA->fetch();
////////////////
$nitEmpr = $empresa['nit'];
$nombSucur = $sucursal['nombreSucur'];
$actividad = $datos_actividad['nombre'];
$SucurTelef = $sucursal['telefono'];
$sucurLugar = $sucursal['ciudad'];
$FechaT = $trans['fecha'];
$nombCliente = $cliente['nombreCliente'];
$nitCliente = $cliente['nit'];
$D = substr($FechaT, 0, 4);
$M = substr($FechaT, 5, 2);
$Y = substr($FechaT, 8);
$FechaTransac = $D . $M . $Y;
//RECUPERAMOS DATOS DE ORDEN Y DESPACHO
$tieneDestino = $_REQUEST['ord'];
$lugarDestino = $_REQUEST['dest'];

?>
<style type="text/css">
    table {
        color: #000000;
        font-size: 10pt;
    }
    @media all {
        div.saltopagina{
            display: none;
        }
    }
    @media print{
        div.saltopagina{
            display:block;
            page-break-before:always;
        }
    }
</style>
<html>
<head>
    <!--<link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen">-->
    <!--<link rel="stylesheet" type="text/css" href="../css/print/print.css" media="print">-->
    <link href="printListaTransac.css" rel="stylesheet" type="text/css" media="print">
    <!--        <link href="estiloReportes.css" rel="stylesheet" type="text/css"/>-->
    <!--<link href="../css/style.css" rel="stylesheet" media="all">-->
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet" media="all">
    <script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- USO DE LA LIBRERIA PARA EXPORTAR A EXCEL-->
    <script language="javascript">
        $(document).ready(function()
        {
            $(".botonExcel").click(function(event)
            {
                $("#datos_a_enviar").val($("<div>").append($("#Exportar_a_Excel").eq(0).clone()).html());
                $("#FormularioExportacion").submit();
            });
        });
    </script>
</head>
<body style="background:transparent; padding-left: 2%">
<?php
    require 'encabezadoNotaVentaCopia.php';
?>
<table id="tablaD" cellpadding="1" style=" width: 100%; border: 1px solid black; border-radius: 6px">
    <tr>
        <td style="width: 65%">
            <strong>Fecha : </strong>Santa Cruz, <?= $Y ?> de <?= obtener_mes($M) ?> del <?= $D ?>
        </td>
    </tr>
    <tr>
        <td>
            <strong>Señor(es): </strong> <?= $nombCliente ?>
        </td>
        <td style="width: 35%">
            <strong>CI: </strong> <?= $nitCliente ?>
        </td>
    </tr>
</table>
<br>
<div heigth="50">
    <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt">
        <thead>
        <th style="border-left:1px solid black; border-bottom:1px solid black;border-top:1px solid black; font-size: 9pt">
            CANTIDAD
        </th>
        <th style="border-left:1px solid black;border-bottom:1px solid black;border-top:1px solid black;font-size: 9pt">
            CONCEPTO
        </th>
        <th style="width:15%; border-top: 1px solid black;border-bottom: 1px solid black;border-left :1px solid black;font-size: 9pt">PRECIO UNT.</th>
        <th style="font-size: 9pt;width:15%; border-top: 1px solid black;border-bottom: 1px solid black;border-left :1px solid black;border-right:1px solid black;">
            SUBTOTAL
        </th>
        </thead>
        <tbody>
        <?php
        // EXTRAEMOS EL DETALLE DE LA VENTA ///
        $sql2 = "SELECT v.idVenta, p.codigoProducto, p.nombreComercial, sum(d.cantidad)cantidad, d.precioVenta, v.montoSubTotal, v.montoTotal, v.descuento, v.efectivoSus, v.efectivoBs, v.cambio "
            . " FROM DetalleTransaccion d, Transaccion t, Ventas v, Producto p "
            . "WHERE d.idTransaccion= t.idTransaccion  and t.idTransaccion = v.idTransaccion and d.codigoProducto = p.codigoProducto "
            . "and t.estado = 1 and v.idVenta =? group by p.codigoProducto";
        $com2 = Database::getInstance()->getDb()->prepare($sql2);
        $com2->execute([$venta['idVenta']]);

        $num = 1;
        while ($detalle = $com2->fetch()) {
            $cantidad = $detalle['cantidad'];
            $descripcion = $detalle['nombreComercial'];
            $precio = $detalle['precioVenta'];
            $subTotal = $cantidad * $precio;
            $pagobs = $detalle['efectivoBs'];
            $pagosus = $detalle['efectivoSus'];
            $cambio = $detalle['cambio'];
            $montot = $detalle['montoTotal'];
            $submonto = $detalle['montoSubTotal'];
            $descuento = $detalle['descuento'];
            if ($cantidad == 0) {
                ?>
                <tr>
                    <td style="font-size: 12px; border-left:1px solid black"></td>
                    <td style="font-size: 12px; border-left:1px solid black; padding-left: 1%">
                        <?= $descripcion ?>
                    </td>
                    <td style="border-left:1px solid black; text-align: center"></td>
                    <td style="border-right :1px solid black;border-left:1px solid black;text-align: center"></td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td style="font-size: 12px; border-left:1px solid black;text-align: center"><?= $cantidad ?></td>
                    <td style="font-size: 12px; border-left:1px solid black;padding-left: 1%">
                        <?= $descripcion ?>
                    </td>
                    <td style="border-left:1px solid black; text-align: center">
                        <?php echo number_format($precio, 2);
                        ?>
                    </td>
                    <td style="border-left:1px solid black;border-right:1px solid black;text-align: center">
                        <?php echo number_format($precio * $cantidad, 2);
                        ?>
                    </td>
                </tr>
                <?php
            }
            $monto = $montot;
            $num++;
        }
        $iva = $monto * 0.13;
        $dato = 6;
        if ($num == 1) {
            ImprimirEspacios($dato);
        } else {
            if ($num > 1) {
                $dato = $dato - $num;
                ImprimirEspacios($dato);
            }
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2" style="border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;">
                <strong>Son:</strong> <?php
                echo $valor . " Bolivianos";
                ?>
            </td>
            <td style="font-size:9pt; text-align: right; border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black"><strong>TOTAL Bs.:</strong></td>
            <td style="text-align: center;border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;border-right:1px solid black;"><?= number_format($montot, 2) ?></td>
        </tr>
        </tfoot>
    </table>
</div>
<br>
<!--<div style="width:65%; float:left">
    <?php
/*    require_once('CodigoControl.class.php');
    $montoEntero = explode(".", $montot); // DIVIDO EL MONTO A PAGAR PARA EXTRAER LA PARTE ENTERA Y LA PARTE DECIMAL
    $entero = $montoEntero[0];
    $decimal = @$montoEntero[1];
    if ($decimal >= 50) {
        $entero = $entero + 1;
    } else {
        $entero;
    }
    $CodigoControl = new CodigoControl(
        $autorizacion, $nroFac, $nitCliente, $FechaTransac, $entero, $llave
    );
    $cod_con = $CodigoControl->generar();
    ///////// inseta los parametros de codigode control //////////////////////
    $fechainvertida = Invertir_Fecha($FechaT);
    $totalbs1 = 0;
    $totalbs1 = number_format($montot,2);
    //                    $sql2 = "insert into ParametrosFac (autorizacion, nroFac, nitCliente, fechaTrans, montoFac, llave, codigoControl) values(?,?,?,?,?,?,?)";
    //                    $com = Database::getInstance()->getDb()->prepare($sql2);
    //                    $com->execute([$autorizacion, $nroFac, $nitCliente, $FechaTransac, $totalbs1, $llave, $cod_con]);
    ///////////////////////////////////////////
    //$nombreDelCliente = $cliente['nombre_cliente'];
    $filename = $PNG_TEMP_DIR . 'test' . $FechaTransac . $nroFac . '.png';
    $datos = $nitEmpr . "|" . $nroFac . "|" . $autorizacion . "|" . $fechainvertida . "|" . $totalbs1 . "|" . $cod_con . "|" . $nitCliente . "|" .$nombCliente;
    //QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);
    QRcode::png($datos, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
    */?>
    <br>
    <table align="left" style="font-size: 9pt; padding-left: 5%" cellpadding="1">
        <thead>
        <tr>
            <td style="border: 1px solid black; border-radius: 5px">
                <strong>Codigo De Control:</strong> <?/*= $cod_con */?>&nbsp;&nbsp;&nbsp;
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td style="border: 1px solid black;border-radius: 5px">
                <strong>Fecha Limite De Emision:</strong> <?/*= Invertir_Fecha($fechaLimite) */?>&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        </thead>
    </table>
</div>-->
</body>
</html>
