<?PHP
require_once ("../datos/Database.php");
include ('factura/qrlib.php');
//require_once ('head.php');
session_start();
if (($_SESSION['logueado']) != 'SI') {
    header('Location: ../inicio');
}
$idVentaSave = $_GET['idVentaa']; //id de la ultima venta
///FECHA=HORA DEL SISTEMA
ini_set("date.timezone", "America/La_Paz");
$fechaActualSistema = date('d-m-Y'); //fecha actual del sistemaAND horaFin>='$horaSistema'
$horaActualSistema = date('H:i:s'); //hora actual del sistema
///////////////////////////////////////////////
///////////////////////////////////////////////
/////DATOS DE LA SUCURSAL
$indexSucursal = $_SESSION['userMaster']; //id de la sucursal logueada 
$idSucursal = $indexSucursal['idSucursal'];
$consulta = "SELECT s.* FROM Sucursal s WHERE s.idSucursal=$idSucursal";
$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$dato = $resultado->fetch();
$nombreDeSucursal = $dato['nombreSucur'];
$direccionSucursal = $dato['direccion'];
$telefonoSucursal = $dato['telefono'];
//////////////////////////////////////////////
/////DATOS DEL CLIENTE////////////////////////
$consultaCliente = "SELECT cl.* FROM Cliente cl ,Venta vn WHERE cl.idCliente = vn.idCliente AND vn.idVenta = '$idVentaSave' ";
$resultadoCliente = Database::getInstance()->getDb()->prepare($consultaCliente);
$resultado->execute();
$datoCliente = $resultadoCliente->fetch();
$nombreCl = $datoCliente['nombreCliente'];
$nit_ci = "S/N";
$idCliente = $datoCliente['idCliente'];
if ($datoCliente['nit'] != "") {
    $nit_ci = $datoCliente['nit'];
} else if ($datoCliente['nit'] = "" && $datoCliente['ci'] != "") {
    $nit_ci = $datoCliente['ci'];
}

//
//DATOS DEEMPRESA
$consultaEmpresa = "SELECT em.* FROM Empresa em,Sucursal su WHERE  su.idSucursal='$idSucursal' AND su.idEmpresa=em.idEmpresa ";
$resultado = Database::getInstance()->getDb()->prepare($consultaEmpresa);
$resultado->execute();
$dato = $resultado->fetch();
$nombreEmpresa = $dato['nombreEmpresa'];
$nitEmpresa = $dato['nit'];

/////////////////////////////////////////////
////////////////////////DETALLE DE LA FACTURA de VENTA AL CREDITO
$consultaIdTransaccion = "SELECT * FROM  Ventas vn WHERE vn.idVenta = '$idVentaSave'"; //obtenos el id de la ttransaccion de venta para obtner el detalle de la venta
$resultado = Database::getInstance()->getDb()->prepare($consultaIdTransaccion);
$resultado->execute();
$dato = $resultado->fetch();
$idTransac = $dato['idTransaccion'];
//$idTrans = $dataTrans['idTransaccion'];
///obtenemos el detalle de producto vendidos (de detalletransaccion  )
$queryDetalleTransaccion = "SELECT * FROM DetalleTransaccion dt,Producto pr WHERE dt.idTransaccion = '$idTransac' AND dt.codigoProducto = pr.codigoProducto";
$resultDetalleTrans = Database::getInstance()->getDb()->prepare($queryDetalleTransaccion);
$resultDetalleTrans->execute();


//////////////////////////////////////
////////////obtenemos el descuento de la venta
$queryDescuento = "SELECT * FROM Ventas v WHERE v.idVenta = '$idVentaSave'";
$resultDescuento = Database::getInstance()->getDb()->prepare($queryDescuento);
$resultDescuento->execute();
$montonDescuento = $resultDescuento->fetch();




/////////////////////////////////////////////////CONSULTASSSS DE IMPRIMIR QR
$consulta = 'SELECT * FROM Empresa';
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$empresa = $comando->fetch();
//SUCURSAL
$userMaster = $_SESSION['userMaster'];
$consulta1 = "SELECT * FROM Sucursal WHERE idSucursal =?";
$comando1 = Database::getInstance()->getDb()->prepare($consulta1);
$comando1->execute([$userMaster['idSucursal']]);
$sucursal = $comando1->fetch();

//USUARIO
$userMaster = $_SESSION['userMaster'];
$consulta5 = "select u.idUsuario, u.nombre, u.apellido from Usuario u , InicioSesion i, Sucursal s where u.idUsuario = i.usuario and i.sucursal = s.idSucursal and s.idSucursal =? and i.idInicio = ?";
$comando5 = Database::getInstance()->getDb()->prepare($consulta5);
$comando5->execute([$userMaster['idSucursal'], $_SESSION['idSesion']]);
$usuario = $comando5->fetch();

//OBTENEMOS DATOS DE LA DOSIFICACION
$consulta4 = "SELECT * FROM Dosificacion WHERE idSucursal =? and estado = 1";
$comando4 = Database::getInstance()->getDb()->prepare($consulta4);
$comando4->execute([$userMaster['idSucursal']]);
$dosificacion = $comando4->fetch();

//OBTENEMOS DATOS DE LA TRANSACCION
$nventa = $idVentaSave;
$consulta2 = "SELECT * FROM Ventas WHERE idVenta =?";
$comando2 = Database::getInstance()->getDb()->prepare($consulta2);
$comando2->execute([$nventa]);
$venta = $comando2->fetch();

$consu = "SELECT * FROM Transaccion WHERE idTransaccion =?";
$com = Database::getInstance()->getDb()->prepare($consu);
$com->execute([$venta['idTransaccion']]);
$trans = $com->fetch();

//OBTENEMOS DATOS DEL CLIENTE
$consulta3 = "SELECT * FROM Cliente WHERE idCliente =?";
$comando3 = Database::getInstance()->getDb()->prepare($consulta3);
$comando3->execute([$venta['idCliente']]);
$cliente = $comando3->fetch();

//////// EXTRAER EL NUMERO DE FACTURA SIGUIENTE /////
$sql = "select count(*)cant from Factura f, Dosificacion d, Sucursal s where f.idDosifi = d.idDosifi and d.idSucursal = s.idSucursal and s.idSucursal =?";
$com = Database::getInstance()->getDb()->prepare($sql);
$com->execute([$userMaster['idSucursal']]);
$dato = $com->fetch();
$cant = $dato['cant'];

$idSuc = $sucursal['idSucursal'];
$idDos = $dosificacion['idDosifi'];
if ($cant == 0) {
    $nroFac = $dosificacion['nroFacturaInicial'];  ///numero inicial qeu se registro en la dosificacion
} else {
    $sql = "select max(nroFactura)nro from Factura f, Dosificacion d, Sucursal s where f.idDosifi = d.idDosifi and d.idSucursal = s.idSucursal and s.idSucursal =?";
    $com = Database::getInstance()->getDb()->prepare($sql);
    $com->execute([$userMaster['idSucursal']]);
    $nro = $com->fetch();
    $nroFac = $nro['nro'];
    $nroFac++;
}
///////////// EXTRAER LOS PARAMETROS DE LA FACTURA //////////////

$nombEmpr = $empresa['nombreEmpresa'];
$nitEmpr = $empresa['nit'];
$autorizacion = $dosificacion['nroAutorizacion'];
$llave = $dosificacion['llaveDosificacion'];
$nombSucur = $sucursal['nombreSucur'];
$sucurLugar = $sucursal['ciudad'];
$SucurTelef = $sucursal['telefono'];
$ventaMonto = $venta['montoTotal'];
$FechaT = $trans['fecha'];
$HoraTransac = $trans['hora'];
$nombCliente = $cliente['nombreCliente'];
$nitCliente = $cliente['nit'];
$fechaLimite = $dosificacion['fechaLimite'];
$UsuarioNombre = $usuario['nombre'];
$usuarioApellido = $usuario['apellido'];

// QUITANDO LOS SEPARADORES DE LA FECHA DE TRANSACCION ///////////
$Y = substr($FechaT, 0, 4);
$M = substr($FechaT, 5, 2);
$D = substr($FechaT, 8);
$FechaTransac = $Y . $M . $D;
/////////////////////////////////////////////////////////////////
require_once './factura/CodigoControl.class.php';

$PNG_TEMP_DIR = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;

//html PNG location prefix
$PNG_WEB_DIR = 'temp/';

//ofcourse we need rights to create temp dir
if (!file_exists($PNG_TEMP_DIR))
    mkdir($PNG_TEMP_DIR);
$errorCorrectionLevel = 'L';
if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L', 'M', 'Q', 'H')))
    $errorCorrectionLevel = $_REQUEST['level'];

$matrixPointSize = 3;
if (isset($_REQUEST['size']))
    $matrixPointSize = min(max((int) $_REQUEST['size'], 1), 10);
//$query=$cons->cons_cond("ca_diariofac","numfac='$id_diario'",'cod_fac');
//$llave='F#I-gUHCtC*B5=7MM8=*#HD@6*(bEQ*FB6BEg3jQ)jpKB@%C)RE=XZnFpTDk-$x+';
$fec_lim = $dosificacion['fechaLimite']; //$cons->cons_simple('par_dos',"estado='1'",'fecha_limite');
$tipo_impre = 1;
?>
<html>
    <head>
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="all"/>
        <link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen"/>
        <link href="../reporteCompras/printListaTransac.css" rel="stylesheet" type="text/css" media="print" />  
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet"  media="all">
        <script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="jPedidos.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="formatoSinQR.css" media="screen" />

        <script language="JavaScript">
            $(document).ready(function() {
                doPrint();
            });
            function doPrint() {
                window.print()();
            }
            function volver(dato) {
                location.href = "index.php";
            }
        </script>
        <style type="text/css">
            table {
                color: #000000;
                font-size: 8pt; 
            }

            @media all {
                div.saltopagina{
                    display: none;
                }
            }

            @media print{
                div.saltopagina{ 
                    display:block; 
                    page-break-before:always;
                }
            }	

        </style>

    </head>
    <body >
        <div id="leteral">

        </div>
        <div id="noprint">
            <table>
                <tr>
                    <td>
                        <button type="button" name="volver" id="volver" onclick="volver()">Volver</button>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <button type="button" onclick="doPrint()" >Imprimir</button>
                    </td>
                </tr>
            </table> 
        </div>
        <div id="hoja" style="height: auto"> 
            <div id="mostrarUser">
            </div>
            <div class="widget">
                <div id="encabezado" >
                    <div id="logo" >
                        <img width="100" height="100" src="..\..\farmacia\assets\img\Logo1.png"/>
                    </div>
                    <div id="titulo">
                        <center>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="right">
                                            <font size='5'><strong><label for="user_name">FACTURA DE VENTA AL CREDITO</label></strong></font><br>
                                            <font size='4'><strong><center><label for="user_name"><?= $nombreEmpresa ?></label></center></strong></font><br>
                                <font size='4'><center><label for="user_name">NIT: <?= $nitEmpresa ?></label></center></font>
                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                    <div id="inf">
                        <center>
                            <div align="right">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label >Hora:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: right">
                                                <label ><?= $horaActualSistema ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <strong><label >Fecha:</label></strong>
                                            </td>
                                            <td>
                                                <fieldset>
                                                    <label ><?= $fechaActualSistema ?></label>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                </div>

                <div align="center" id="informacion">
                    <div id="content">
                        <center>
                            <div align="left">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Sucursal:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nombreDeSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Direccion:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $direccionSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Telefono:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $telefonoSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Se&ntilde;or(es):</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nombCliente ?> </label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                    <div style="float: left">
                        <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                            <thead>
                                <tr>
                                    <th style="width: 6%"><strong>Nro</strong></th>
                                    <th style="width: 14%"><strong>Codigo</strong></th>
                                    <th style="width: 50%"><strong>Producto</strong></th>
                                    <th style="width: 10%"><strong>Cantidad</strong></th>
                                    <th style="width: 10%"><strong>P. Unitario</strong></th>
                                    <th style="width: 10%"><strong>Costo</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $contador = 1;
                                $montoSubtotal = 0;
                                while ($row = $resultDetalleTrans->fetch()) {
                                    ?>
                                    <tr class="modo2">
                                        <td style="text-align: left;" >
                                            <label size="2" type="text"><?= $contador ?> </label>
                                        </td>
                                        <td style="text-align: left">
                                            <label size="4" type="text"><?= $row['codigo'] ?> </label>
                                        </td>
                                        <td style="text-align: left">
                                            <label size="30" type="text"><?= $row['nombreComercial'] ?> </label>
                                        </td>
                                        <td style="text-align: right">
                                            <label size="4" type="text"><?= $row['cantidad'] ?></label>
                                        </td>
                                        <td style="text-align: right">
                                            <label size="5" type="text"><?= $row['precioVenta'] ?> </label>
                                        </td>
                                        <td style="text-align: right">
                                            <label size="10" type="text"><?= $row['cantidad'] * $row['precioVenta'] ?> </label>
                                        </td>
                                    </tr>
                                    <?php
                                    $montoSubtotal = $montoSubtotal + ($row['cantidad'] * $row['precioVenta']);
                                    $contador = $contador + 1;
                                }
                                ?>

                            </tbody>
                            <tfoot>
                            <!--<intup type="text" id="cantFilas" name="cantFilas" name="subTotal" value="////" size="8" hidden/>-->
                                <tr style="padding-bottom: 1em"></tr>    
                                <tr style="padding-bottom: 1em;padding-top: 8em;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2" class="text-right"><strong>Sub Total:</strong></td>
                                    <td class="text-right">
                                        <label type="text" id="subTotal" name="subTotal" size="8"><?= $montoSubtotal ?></label></td>
                                </tr>
                                <tr style="padding-bottom: 1em">
                                    <td ></td><td ></td><td ></td>
                                    <td colspan="2" class="text-right"><strong>Descuento :</strong></td>
                                    <td class="text-right">
                                        <label type="text" id="descuentoB" name="descuentoB" size="8"><?= $montonDescuento['descuento'] ?></label>
                                </tr>
                            <br>
                            <tr style="padding-bottom: 1em">
                                <td ></td><td ></td><td ></td>
                                <td colspan="2" class="text-right"><strong>Total:</strong></td>
                                <td class="text-right ">
                                    <strong>
                                        <label type="text" id="total" name="total" size="8"><?= $montoSubtotal - $montonDescuento['descuento'] ?></label>
                                        <?php
                                        $total = $montoSubtotal - $montonDescuento['descuento'];
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                            <br>    
                            </tfoot>
                        </table>
                    </div><br>
                    <div style="float: left">
                        <strong>Nota:</strong> No se permiten cambios ni devolucion de factura. Por favor verifique sus datos antes de la emision de la misma
                        <br><br>
                        <strong style="float: left">
                            Son: 
                            <?php
                            require_once '../nueva_venta/factura/conversor.php';
                            echo @convertir($total);
                            ?>
                            <br>
                        </strong>
                    </div>
                    <br>
                    <div>
                        <?php
                        $montoOrigi = explode(".", $total);
                        $montoDefiniti = $montoOrigi[0] . @$montoOrigi[1];
                        $CodigoControl = new CodigoControl(
                                $autorizacion, $nroFac, $nitCliente, $FechaTransac, $montoDefiniti, $llave
                        );
                        $cod_con = $CodigoControl->generar();

                        $filename = $PNG_TEMP_DIR . 'test' . $FechaTransac . $nroFac . '.png';
                        $datos = $nitCliente . "|" . $autorizacion . "|" . $nroFac . "|" . $FechaTransac . "|" . $total . "|" . $cod_con . "|" . $nombCliente;
//QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
                        QRcode::png($datos, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
                        ?>
                    </div>
                    <div style="">
                        <br><br><br><br><br>
                    </div>
                    <br><br>

                    <div style="float: left">
                        CODIGO DE CONTROL: <?php echo $cod_con; ?><br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA LIMITE DE EMISION: <?php echo $fechaLimite; ?>
                    </div>
                    <div class="top" style="width: 180;height: 180; float: right">
                        <?php
                        echo '<img src="' . $PNG_WEB_DIR . basename($filename) . '"/>';
                        ?>
                    </div>
                    <br><br><br>
                    <div>
                        <br><br><br>
                        <strong><br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_______________________<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENTREGADO POR:</strong>
                    </div>
                    <?php
                    $impuestoTrecepor = $total * 0.13;
                    $sql = "insert into Factura (idVenta,nroFactura,idDosifi,montoTotal,impICE,impExce,impNeto,impuestos,codigoControl,idSucursal,estado) values(?,?,?,?,?,?,?,?,?,?,?)";
                    $com = Database::getInstance()->getDb()->prepare($sql);
                    $com->execute([$venta['idVenta'], $nroFac, $dosificacion['idDosifi'], $total, 0, 0, 0, $impuestoTrecepor, $cod_con, $idSucursal, 0]);
                    ?>
                </div>
                <center > 
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;"ESTA FACTURA CONTRIBUYE AL DESARROLLO DEL PAÍS<br>. 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        EL USO ILÍCITO DE ÉSTA SERÁ SANCIONADO DE ACUERDO A LEY"</p>    
                </center> 
            </div>
            <form action="ficheroExcel.php" method="post" id="FormularioExportacion">
                <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
            </form><br><br><br><br><br><br><br><br><br><br>
        </div>
        <div class="saltopagina">

        </div>
        <div id="hoja" style="height: auto"> 
            <div id="mostrarUser">
            </div>
            <div class="widget">
                <div id="encabezado" >
                    <div id="logo" >
                        <img width="100" height="100" src="..\..\farmacia\assets\img\Logo1.png"/>
                    </div>
                    <div id="titulo">
                        <center>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="right">
                                            <font size='5'><strong><label for="user_name">FORMULARIO PLAN DE PAGO</label></strong></font><br>
                                            <font size='4'><strong><center><label for="user_name"><?= $nombreEmpresa ?></label></center></strong></font><br>
                                <font size='4'><center><label for="user_name">NIT: <?= $nitEmpresa ?></label></center></font>
                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                    <div id="inf">
                        <center>
                            <div align="right">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label >Hora:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: right">
                                                <label ><?= $horaActualSistema ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <strong><label >Fecha:</label></strong>
                                            </td>
                                            <td>
                                                <fieldset>
                                                    <label ><?= $fechaActualSistema ?></label>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                </div>

                <div align="center" id="informacion">
                    <div id="content">
                        <center>
                            <div align="left">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Sucursal:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nombreDeSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Direccion:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $direccionSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Telefono:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $telefonoSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Se&ntilde;or(es):</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nombCliente ?> </label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                    <div style="float: Center">
                        <table class="table" >
                            <thead>
                                <tr>
                                    <th style="width: 20%"><strong>Nro</strong></th>
                                    <th style="width: 50%"><strong>Fecha</strong></th>
                                    <th style="width: 50%"><strong>Monto</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $datosPlanDePago = "SELECT * FROM VentasCredito vc WHERE vc.idVenta = '$idVentaSave'";
                                $resultPlanDePago = Database::getInstance()->getDb()->prepare($datosPlanDePago);
                                $resultPlanDePago->execute();
                                $row = $resultPlanDePago->fetch();
                                $contador = 1;
                                $montoSubtotal = 0;

                                /////nuemros de cutoas 
                                $cantidadCuotas = 0;
                                $cantidadCuotas = $row['nroCuotas'];
                                $fechaPunt = $row['fechaInicio'];
                                $cuottta = $row['montoCredito'] / $cantidadCuotas;
                                $intervaloDias = $row['diasPlazo']/$cantidadCuotas;
                                $sw = true; 
                                $ind = 1; //indice dl numero de cuotas
                                while ($ind <= $cantidadCuotas) {
                                    if($sw &&($cantidadCuotas>1)){
                                        
                                        $fechaPunt = nvFec($fechaPunt, $intervaloDias+1);  
                                        $sw=false;
                                    }else{
                                        $fechaPunt = nvFec($fechaPunt, $intervaloDias); 
                                    }
                                    ?>
                                    <tr class="modo2">
                                        <td style="text-align: left;" >
                                            <label size="2" type="text"><?= $ind ?> </label>
                                        </td>
                                        <td style="text-align: left">
                                            <label size="4" type="text"><?= $fechaPunt ?>
                                            </label>
                                        </td>
                                        <td style="text-align: left">
                                            <label size="30" type="text"><?= $cuottta ?> </label>
                                        </td>
                                    </tr>
                                    <?php
                                    $ind++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div><br>
                    <br>
                    <div>
                        <?php

                        function nvFec($fechaPunt, $dia) {
                            $fechaPunttt = date_create($fechaPunt);
                            date_format($fechaPunttt, 'd-m-y');
                            $fechaPunttt = date('d-m-Y', strtotime($fechaPunt) + (86400 * $dia));
                            return $fechaPunttt;
                        }
                        ?>
                    </div>
                </div>
            </div>
            <form action="ficheroExcel.php" method="post" id="FormularioExportacion">
                <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
            </form><br><br><br><br><br><br><br><br><br><br>
        </div>
    </body><br><br><br><br><br>
</html>

