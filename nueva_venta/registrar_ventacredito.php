<?php
session_start();
require_once ("../datos/Database.php");
//require_once ('head.php');

$idVentaSave = $_GET['idVentaa']; //id de la ultima venta
///FECHA=HORA DEL SISTEMA
ini_set("date.timezone", "America/La_Paz");
$fechaActualSistema = date('d-m-Y'); //fecha actual del sistemaAND horaFin>='$horaSistema'
$horaActualSistema = date('H:i:s'); //hora actual del sistema
///////////////////////////////////////////////
///////////////////////////////////////////////
/////DATOS DE LA SUCURSAL

$indexSucursal = $_SESSION['userMaster']; //id de la sucursal logueada 
$idSucursal = $indexSucursal['idSucursal'];

$consulta = "SELECT s.* FROM Sucursal s WHERE s.idSucursal=$idSucursal";
$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$dato = $resultado->fetch();
$nombreDeSucursal = $dato['nombreSucur'];
$direccionSucursal = $dato['direccion'];
$telefonoSucursal = $dato['telefono'];


//////////////////////////////////////////////
/////DATOS DEL CLIENTE////////////////////////
$consultaCliente = "SELECT cl.* FROM Cliente cl ,Ventas vn WHERE cl.idCliente = vn.idCliente AND vn.idVenta = '$idVentaSave' ";
$resultadoCliente = Database::getInstance()->getDb()->prepare($consultaCliente);
$resultadoCliente->execute();
$datoCliente = $resultadoCliente->fetch();
$nombreClit = $datoCliente['nombreCliente'];
$nit_ci = "S/N";
$idCliente = $datoCliente['idCliente'];
if ($datoCliente['nit'] != "") {
    $nit_ci = $datoCliente['nit'];
} else if ($datoCliente['nit'] = "" && $datoCliente['ci'] != "") {
    $nit_ci = $datoCliente['ci'];
}

//
//DATOS DEEMPRESA
$consultaEmpresa = "SELECT em.* FROM Empresa em,Sucursal su WHERE  su.idSucursal='$idSucursal' AND su.idEmpresa=em.idEmpresa ";
$resultado = Database::getInstance()->getDb()->prepare($consultaEmpresa);
$resultado->execute();
$dato = $resultado->fetch();
$nombreEmpresa = $dato['nombreEmpresa'];
$nitEmpresa = $dato['nit'];

/////////////////////////////////////////////
////////////////////////DETALLE DE LA FACTURA de VENTA AL CREDITO
$consultaIdTransaccion = "SELECT * FROM  Ventas vn WHERE vn.idVenta = '$idVentaSave'"; //obtenos el id de la ttransaccion de venta para obtner el detalle de la venta
$resultado = Database::getInstance()->getDb()->prepare($consultaIdTransaccion);
$resultado->execute();
$dato = $resultado->fetch();
$idTransac = $dato['idTransaccion'];
//$idTrans = $dataTrans['idTransaccion'];
///obtenemos el detalle de producto vendidos (de detalletransaccion  )
$queryDetalleTransaccion = "SELECT * FROM DetalleTransaccion dt,Producto pr WHERE dt.idTransaccion = '$idTransac' AND dt.codigoProducto = pr.codigoProducto";
$resultDetalleTrans = Database::getInstance()->getDb()->prepare($queryDetalleTransaccion);
$resultDetalleTrans->execute();


//////////////////////////////////////
////////////obtenemos el descuento de la venta
$queryDescuento = "SELECT * FROM Ventas v WHERE v.idVenta = '$idVentaSave'";
$resultDescuento = Database::getInstance()->getDb()->prepare($queryDescuento);
$resultDescuento->execute();
$montonDescuento = $resultDescuento->fetch();
?>
<link href="../css/bootstrap.min.css" rel="stylesheet" media="all">
<link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen">
<link href="../reporteCompras/printListaTransac.css" rel="stylesheet" type="text/css" media="print" />  
<link href="../font-awesome/css/font-awesome.css" rel="stylesheet"  media="all">
<script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
<script language="javascript" type="text/javascript" src="jPedidos.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="formatoSinQR.css" media="screen" />
<html> 
    <body> 
        <script language="JavaScript">
            $(document).ready(function() {
                doPrint();
            });
            function doPrint() {
                window.print()();
            }
            function volver(dato) {
                location.href = "index.php";
            }
        </script>
        <style type="text/css">
            table {
                color: #000000;
                font-size: 8pt; 
            }

            @media all {
                div.saltopagina{
                    display: none;
                }
            }

            @media print{
                div.saltopagina{ 
                    display:block; 
                    page-break-before:always;
                }
            }	

        </style>
        <div id="leteral"></div>
        <div id="noprint">
            <table>
                <tr>
                    <td>
                        <button type="button" name="volver" id="volver" onclick="volver()">Volver</button>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <button type="button" onclick="doPrint()" >Imprimir</button>
                    </td>
                </tr>
            </table> 
        </div>
        <div id="hoja" style="height: auto"> 
            <div id="mostrarUser">
            </div>
            <div class="widget">
                <div id="encabezado" >
                    <div id="logo" >
                        <img width="100" height="100" src="..\..\farmacia\assets\img\Logo1.png"/>
                    </div>
                    <div id="titulo">
                        <center>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="right">
                                            <font size='6'><strong><label for="user_name">NOTA DE VENTA AL CREDITO</label></strong></font><br>
                                            <font size='5'><strong><center><label for="user_name"><?= $nombreEmpresa ?></label></center></strong></font><br>
                                <font size='4'><center><label for="user_name">NIT: <?= $nitEmpresa ?></label></center></font>
                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                    <div id="inf">
                        <center>
                            <div align="right">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label >Hora:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: right">
                                                <label ><?= $horaActualSistema ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <strong><label >Fecha:</label></strong>
                                            </td>
                                            <td>
                                                <fieldset>
                                                    <label ><?= $fechaActualSistema ?></label>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                </div>

                <div align="center" id="informacion">
                    <div id="content">
                        <center>
                            <div align="left">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Sucursal:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nombreDeSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Direccion:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $direccionSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Telefono:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $telefonoSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                    <div>
                        <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                            <thead>
                                <tr>
                                    <th style="width: 6%"><strong>Nro</strong></th>
                                    <th style="width: 14%"><strong>Codigo</strong></th>
                                    <th style="width: 50%"><strong>Producto</strong></th>
                                    <th style="width: 10%"><strong>Cantidad</strong></th>
                                    <th style="width: 10%"><strong>P. Unitario</strong></th>
                                    <th style="width: 10%"><strong>Costo</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $contador = 1;
                                $montoSubtotal = 0;
                                while ($row = $resultDetalleTrans->fetch()) {
                                    ?>
                                    <tr>
                                        <td style="text-align: left;width:20px;">
                                            <label size="4" type="text"><?= $contador ?> </label>
                                        </td>
                                        <td style="text-align: left">
                                            <label size="30" type="text"><?= $row['codigo'] ?> </label>
                                        </td>
                                        <td style="text-align: left">
                                            <label size="30" type="text"><?= $row['nombreComercial'] ?> </label>
                                        </td>
                                        <td style="text-align: right">
                                            <label size="10" type="text"><?= $row['cantidad'] ?></label>
                                        </td>
                                        <td style="text-align: right">
                                            <label size="10" type="text"><?= $row['precioVenta'] ?> </label>
                                        </td>
                                        <td style="text-align: right">
                                            <label size="10" type="text"><?= $row['cantidad'] * $row['precioVenta'] ?> </label>
                                        </td>
                                    </tr>
                                    <?php
                                    $montoSubtotal = $montoSubtotal + ($row['cantidad'] * $row['precioVenta']);
                                    $contador = $contador + 1;
                                }
                                ?>

                            </tbody>

                            <tfoot>
                            <!--<intup type="text" id="cantFilas" name="cantFilas" name="subTotal" value="////" size="8" hidden/>-->
                                <tr style="padding-bottom: 1em"></tr><tr style="padding-bottom: 1em"></tr> <tr style="padding-bottom: 1em"></tr> <tr style="padding-bottom: 1em"></tr>     
                                <tr style="padding-bottom: 1em;padding-top: 8em;">
                                    <td ></td><td ></td><td ></td>
                                    <td colspan="2" class="text-right"><strong>Sub Total:</strong></td>
                                    <td class="text-right">
                                        <label type="text" id="subTotal" name="subTotal" size="8"><?= $montoSubtotal ?></label></td>
                                </tr>
                                <tr style="padding-bottom: 1em">
                                    <td ></td><td ></td><td ></td>
                                    <td colspan="2" class="text-right"><strong>Descuento :</strong></td>
                                    <td class="text-right">
                                        <label type="text" id="descuentoB" name="descuentoB" size="8"><?= $montonDescuento['descuento'] ?></label>
                                </tr>
                            <br>
                            <tr style="padding-bottom: 1em">
                                <td ></td><td ></td><td ></td>
                                <td colspan="2" class="text-right"><strong>Total:</strong></td>
                                <td class="text-right ">
                                    <strong>
                                        <label type="text" id="total" name="total" size="8"><?= $montoSubtotal - $montonDescuento['descuento'] ?></label>
                                        <?php
                                        $total = $montoSubtotal - $montonDescuento['descuento'];
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                            <br>
                            <tr>
                                <td rowspan="3" style="text-align: left">
                                    <strong>
                                        Son: 
                                        <?php
                                        require_once '../nueva_venta/factura/conversor.php';
                                        echo @convertir($total);
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <strong>Nota:</strong> No se permiten cambios ni devolucion de factura. Por favor verifique sus datos antes de la emision de la misma
                    <div style="border: 1px solid black; width: 40%; height: 12%; margin-top: 2%; " >
                        <br>
                        <strong><br>
                            _______________________<br>
                            ENTREGADO POR:</strong>
                    </div>
                </div>
            </div>
            <form action="ficheroExcel.php" method="post" id="FormularioExportacion">
                <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
            </form>
        </div>
        <div class="saltopagina">

        </div>

        <div id="hoja" style="height: auto"> 
            <div id="mostrarUser">
            </div>
            <div class="widget">
                <div id="encabezado" >
                    <div id="logo" >
                        <img width="100" height="100" src="..\..\farmacia\assets\img\Logo1.png"/>
                    </div>
                    <div id="titulo">
                        <center>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="right">
                                            <font size='6'><strong><label for="user_name">FORMULARIO PLAN DE PAGO</label></strong></font><br>
                                            <font size='5'><strong><center><label for="user_name"><?= $nombreEmpresa ?></label></center></strong></font><br>
                                <font size='4'><center><label for="user_name">NIT: <?= $nitEmpresa ?></label></center></font>
                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                    <div id="inf">
                        <center>
                            <div align="right">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label >Hora:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: right">
                                                <label ><?= $horaActualSistema ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <strong><label >Fecha:</label></strong>
                                            </td>
                                            <td>
                                                <fieldset>
                                                    <label ><?= $fechaActualSistema ?></label>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                </div>

                <div align="center" id="informacion">
                    <div id="content">
                        <center>
                            <div align="left">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Sucursal:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nombreDeSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Direccion:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $direccionSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Telefono:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $telefonoSucursal ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Se&ntilde;or(es):</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nombreClit ?> </label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                    <div>
                        <table class="table " >
                            <thead>
                                <tr>
                                    <th style="width: 20%"><strong>Nro</strong></th>
                                    <th style="width: 50%"><strong>Fecha</strong></th>
                                    <th style="width: 50%"><strong>Monto</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $datosPlanDePago = "SELECT * FROM VentasCredito vc WHERE vc.idVenta = '$idVentaSave'";
                                $resultPlanDePago = Database::getInstance()->getDb()->prepare($datosPlanDePago);
                                $resultPlanDePago->execute();
                                $row = $resultPlanDePago->fetch();
                                $contador = 1;
                                $montoSubtotal = 0;

                                /////nuemros de cutoas 
                                //$cantidadCuotas = 0;
                                $cantidadCuotas = $row['nroCuotas'];
                                $fechaPunt = $row['fechaInicio'];
                                $cuottta = $row['montoCredito'] / $cantidadCuotas;
                                $intervaloDias = $row['diasPlazo'] / $cantidadCuotas;
                                $sw = true;
                                $ind = 1; //indice dl numero de cuotas
                                while ($ind <= $cantidadCuotas) {
                                    if ($sw && ($cantidadCuotas>1)) {
                                        $fechaPunt = nvFec($fechaPunt, $intervaloDias + 1);
                                        $sw = false;
                                    } else {
                                        $fechaPunt = nvFec($fechaPunt, $intervaloDias);
                                    }
                                    ?>
                                    <tr class="modo2">
                                        <td style="text-align: left; " >
                                            <label size="2" type="text"><?= $ind ?> </label>
                                        </td>
                                        <td style="text-align: left; ">
                                            <label size="4" type="text"><?= $fechaPunt ?>
                                            </label>
                                        </td>
                                        <td style="text-align: left; ">
                                            <label size="30" type="text"><?= $cuottta ?> </label>
                                        </td>
                                    </tr>
                                    <?php
                                    $ind++;
                                }
                                ?>
                            </tbody>

<!--                            <tfoot>
                            <intup type="text" id="cantFilas" name="cantFilas" name="subTotal" value="////" size="8" hidden/>
                                <tr style="padding-bottom: 1em"></tr><tr style="padding-bottom: 1em"></tr> <tr style="padding-bottom: 1em"></tr> <tr style="padding-bottom: 1em"></tr>     
                                <tr style="padding-bottom: 1em;padding-top: 8em;">
                                    <td ></td><td ></td><td ></td>
                                    <td colspan="2" class="text-right"></td>
                                    <td class="text-right">

                                </tr>
                                <tr style="padding-bottom: 1em">
                                    <td ></td><td ></td><td ></td>
                                    <td colspan="2" class="text-right"></td>
                                    <td class="text-right">

                                </tr>
                            <br>
                            <tr style="padding-bottom: 1em">
                                <td ></td><td ></td><td ></td>
                                <td colspan="2" class="text-right"></td>
                                <td class="text-right ">

                                </td>
                            </tr>
                            <br>
                            
                            </tfoot>-->
                        </table>
                    </div>

                    <strong>Nota:</strong> No se permiten cambios ni devolucion de factura. Por favor verifique sus datos antes de la emision de la misma
                    <div style="border: 1px solid black; width: 40%; height: 12%; margin-top: 2%; " >
                        <br>
                        <strong><br>
                            _______________________<br>
                            ENTREGADO POR:</strong>
                    </div>
                </div>
            </div>
            <form action="ficheroExcel.php" method="post" id="FormularioExportacion">
                <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
            </form>
            <?php

            function nvFec($fechaPunt, $dia) {
                $fechaPunttt = date_create($fechaPunt);
                date_format($fechaPunttt, 'd-m-y');
                $fechaPunttt = date('d-m-Y', strtotime($fechaPunt) + (86400 * $dia));
                return $fechaPunttt;
            }
            ?>
        </div>
    </body>
</html>