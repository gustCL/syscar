<?php

require_once("../datos/Database.php");
session_start();
$UserMaster = $_SESSION['userMaster'];
$idSesion = $_SESSION['idSesion'];
$idSucursal = $UserMaster['idSucursal'];
$idProducto = $_POST['idProducto'];
//$unidad=$_POST['unidad'];
$precio = $_POST['precio'];
$cantidad = $_POST['cantidad'];
//$ubicacion=$_POST['ubicacion'];
//$monto=$_POST['monto'];
$idAlmacen = $_POST['idAlmacen'];

$idCliente = $_POST['nitCliente'];
$ClienteNombre = $_POST['dNombreCliente'];
$TipoCambio = $_POST['Tcambio'];
$idTipoCambio = $_POST['idTcambio'];
$subTotal = $_POST['total'];
//$descuentoCli=$_POST['descuentoCl'];
//$descuentoP=$_POST['descuentoP'];
//$descuentoB=$_POST['descuentoB'];
$total = $_POST['total'];
$efectivoBs = $_POST['efectivoBs'];
$efectivo = $_POST['efectivo'];
$cambio = $_POST['cambio'];
ini_set("date.timezone", "America/La_Paz");
$fecha = date('Y-m-d');
$hora = date("H:i:s");
//RECUPERAMOS DATOS DEL DESTINO
$destino = $_POST['destino'];
$nota_de_entrega = $_POST['notaEntrega'];
//recuperamos  la sucursal par apoder obtener su tipoImpresion
$cmd_obtener_sucursal = Database::getInstance()->getDb()->prepare("SELECT * FROM Sucursal WHERE idSucursal = :idSucursal");
$cmd_obtener_sucursal->bindParam(':idSucursal', $idSucursal);
$cmd_obtener_sucursal->execute();
$data_sucursal = $cmd_obtener_sucursal->fetch();
$idTipoimpresion = $data_sucursal['tipoFactura'];

if (Database::getInstance()->getDb()->beginTransaction()) {

//$consulta="SELECT idTransaccion FROM Transaccion FOR INSERT";   
//$comando= Database::getInstance()->getDb()->prepare($consulta);
//$comando->execute();
//CREAR LA TRANSACCION
    $consulTransa = "INSERT INTO Transaccion(tipoTrans, fecha, hora, idInicioSesion, idUsuario, estado) VALUES(?, ?, ?, ?, ?, ?)";
    $comandosTransa = Database::getInstance()->getDb()->prepare($consulTransa);
    $comandosTransa->execute([1, $fecha, $hora, $idSesion, $UserMaster['idUsuario'], 1]);

//OBTENER EL ID TRANSACCION NUEVO
    $idTransaccion = Database::getInstance()->getDb()->prepare("Select idTransaccion As idTransaccion from Transaccion Where fecha =? and hora =? ORDER BY idTransaccion DESC LIMIT 1");
    $idTransaccion->execute([$fecha, $hora]);
    $idTr = $idTransaccion->fetch(PDO::FETCH_ASSOC);
    if ($idTr == NULL) {
        Database::getInstance()->getDb()->rollBack();

        $datos = array(
            0 => "No obtiene idTransaccion",
            1 => 'false'
        );

        echo json_encode($datos);
        exit();
    }

    for ($index = 0; $index < count($idAlmacen); $index++) {
        $idAl = $idAlmacen[$index];
        $idProd = $idProducto[$index];
        $nCantidad = $cantidad[$index];
        $nPrecio = $precio[$index];

        // OBTIENE EL STOCK TOTAL
        $comandoStock = Database::getInstance()->getDb()->prepare("SELECT SUM(stock) as stock, nombreComercial FROM Inventario  WHERE codigoProducto=? AND idAlmacen=? GROUP BY codigoProducto");
        $comandoStock->execute([$idProd, $idAl]);
        $stockTotal = $comandoStock->fetch();

                    if ($stockTotal['stock'] < $nCantidad) {
                        Database::getInstance()->getDb()->rollBack();

                        $datos = array(
                            0 => "No existe stock suficientes para: " .$stockTotal['stock'],
                            1 => 'false'
                        );

                        echo json_encode($datos);
                        exit();
                    }
    }
    for ($index = 0; $index < count($idAlmacen); $index++) {
        //$idAl = $idAlmacen[$index];
        $idProd = $idProducto[$index];
        $nCantidad = $cantidad[$index];
        $nPrecio = $precio[$index];

        $consulta = "SELECT * FROM Inventario WHERE codigoProducto=? AND idAlmacen=? AND stock>0 ORDER BY fechaVencimiento ASC";
        $comandos = Database::getInstance()->getDb()->prepare($consulta);
        $comandos->execute([$idProd, $idAl]);

        while ($row = $comandos->fetch()) {
            $stock = $row['stock'];
            // echo $row .'_';

            if ($nCantidad == 0) { // SI LA CANTIDAD ES 0 LO PODEMOS TOMAR COMO SI FUERA UNA GLOSA
                $consulDeTansac = "INSERT INTO DetalleTransaccion(idTransaccion, idExistencia, codigoProducto, cantidad, precioVenta) VALUES(?, ?, ?, ?, ?)";
                $comandoDeTansac = Database::getInstance()->getDb()->prepare($consulDeTansac);
                $comandoDeTansac->execute([$idTr['idTransaccion'], $row['idExistencia'], $idProd, $nCantidad, $nPrecio]);
            } else {
                if ($stock >= $nCantidad) {
                        $consulDeTansac = "INSERT INTO DetalleTransaccion(idTransaccion, idExistencia, codigoProducto, cantidad, precioVenta) VALUES(?, ?, ?, ?, ?)";
                        $comandoDeTansac = Database::getInstance()->getDb()->prepare($consulDeTansac);
                        $comandoDeTansac->execute([$idTr['idTransaccion'], $row['idExistencia'], $idProd, $nCantidad, $nPrecio]);
                        $idLote = $row['idLote'];
                        $idUbicacion = $row['idUbicacion'];
                        $updat = "UPDATE Existencias SET stock = (stock - $nCantidad) WHERE idLote=? AND idUbicacion=?";
                        $updatResultado = Database::getInstance()->getDb()->prepare($updat);
                        $updatResultado->execute([$idLote, $idUbicacion]);
                        ////  SE BUSCA LA LINEA A LA QUE PERTENECE EL CORTE DE CARNE
                        $consulLinea = "select idLinea from Producto where codigoProducto = ?";
                        $comandoL = Database::getInstance()->getDb()->prepare($consulLinea);
                        $comandoL->execute([$idProd]);
                        $datosLinea = $comandoL->fetch();
                        $idLinea = $datosLinea['idLinea'];
                        /*//// DESCONTAMOS EL STOCK DEL CORTE MAYOR
                        $updateLinea = "UPDATE Linea SET stock = (stock - $nCantidad) WHERE idLinea=?";
                        $updatL = Database::getInstance()->getDb()->prepare($updateLinea);
                        $updatL->execute([$idLinea]);*/

                    break;

                } else {
                    $consulDeTansac = "INSERT INTO DetalleTransaccion(idTransaccion, idExistencia, codigoProducto, cantidad, precioVenta) VALUES(?, ?, ?, ?, ?)";
                    $comandoDeTansac = Database::getInstance()->getDb()->prepare($consulDeTansac);
                    $comandoDeTansac->execute([$idTr['idTransaccion'], $row['idExistencia'], $idProd, $stock, $nPrecio]);
                    $nCantidad = $nCantidad - $stock;

                    $idLote = $row['idLote'];
                    $idUbicacion = $row['idUbicacion'];

                    $updat = "UPDATE Existencias SET stock = (stock - $stock) WHERE idLote=? AND idUbicacion=?";
                    $updatResultado = Database::getInstance()->getDb()->prepare($updat);
                    $updatResultado->execute([$idLote, $idUbicacion]);


                }
            }
        }
    }

//OBTENGO idCliente
    if ($idCliente == "" && $ClienteNombre == "") {
        $idCl = 1;
    } else {

        $consultaC = "SELECT * FROM Cliente WHERE nombreCliente=? AND ( nit = ? or ci=? )";
        $comandoC = Database::getInstance()->getDb()->prepare($consultaC);
        $comandoC->execute([$ClienteNombre, $idCliente, $idCliente]);
        if ($comandoC->rowCount() > 0) {
            $id = $comandoC->fetch();
            $idCl = $id['idCliente'];
        } else {
            $consul_verificacion = "SELECT * FROM Cliente c WHERE c.nit = ? ";
            $verificacion = Database::getInstance()->getDb()->prepare($consul_verificacion);
            $verificacion->execute([$idCliente]);
            $verif_consul = $verificacion->fetch(PDO::FETCH_ASSOC);
            $nit = $verif_consul['nit'];
            $nombreC = $verif_consul['nombreCliente'];
            if ($nit == $idCliente && $nombreC != $ClienteNombre) {

                if ($nombreC != $ClienteNombre) {
                    Database::getInstance()->getDb()->rollBack();

                    $datos = array(
                        0 => "Error este nit ya existe",
                        1 => 'false'
                    );

                    echo json_encode($datos);
                    exit();
                }
            } else {
                $consultaC2 = "INSERT INTO Cliente(idCliente, nombreCliente, nit, ci, fechaRegistro, horaRegistro, estado) "
                    . " VALUES ('',?,?,?,?,?,1)";
                $comandoC2 = Database::getInstance()->getDb()->prepare($consultaC2);
                $comandoC2->execute([$ClienteNombre, $idCliente, $idCliente, $fecha, $hora]);
            }


            $consultaC3 = "SELECT* FROM Cliente WHERE nombreCliente=? AND fechaRegistro=? AND horaRegistro=? AND ( nit = ? or ci=? ) ";
            $comandoC3 = Database::getInstance()->getDb()->prepare($consultaC3);
            $comandoC3->execute([$ClienteNombre, $fecha, $hora, $idCliente, $idCliente]);
            $id2 = $comandoC3->fetch(PDO::FETCH_ASSOC);
            $idCl = $id2['idCliente'];
        }
    }

// INSERTAMOS EL DESTINO EN ORDENDESPACHO
    if($destino != ''){
        $consultaV = "INSERT INTO OrdenDespacho(idDespacho, destino, idTransaccion, estado) VALUES (?,?,?,?)";
        $comandoV = Database::getInstance()->getDb()->prepare($consultaV);
        $comandoV->execute([0,$destino,$idTr['idTransaccion'],1]);
    }

//INSERTAR DATOS DE VENTAS
    $descuento = $subTotal - $total;
    $consultaV = "INSERT INTO Ventas(idVenta, tipoVenta, idTransaccion, idCliente, montoSubTotal, descuento, montoTotal, idTipoCambio, tipoCambio, efectivoSus, efectivoBs, cambio) VALUES ('','0',?,?,?,?,?,?,?,?,?,?)";
    $comandoV = Database::getInstance()->getDb()->prepare($consultaV);
    $comandoV->execute([$idTr['idTransaccion'], $idCl, $subTotal, $descuento, $total, $idTipoCambio, $TipoCambio, $efectivo, $efectivoBs, $cambio]);

//OBTENEMOS EL idVenta PARA la nota de venta 
    $consultaId = "SELECT * FROM Ventas WHERE idTransaccion = '" . $idTr['idTransaccion'] . "'";
    $comandoId = Database::getInstance()->getDb()->prepare($consultaId);
    $comandoId->execute();
    $idVenta = $comandoId->fetch();
    // obtengo la ultima venta de la sucursal
    $consultaNV = "SELECT Max(nroVenta) as nroVenta FROM VentaSucursal WHERE   idSucursal= '" . $UserMaster['idSucursal'] . "'";
    $comandoNV = Database::getInstance()->getDb()->prepare($consultaNV);
    $comandoNV->execute();
    $nroVenta = $comandoNV->fetch();
//INSERTAR DATOS Nota de venta
    $nroV = $nroVenta['nroVenta'] + 1;
    $consultaVS = "INSERT INTO VentaSucursal(idSucursal, idVenta, nroVenta) VALUES (?,?,?)";
    $comandoVS = Database::getInstance()->getDb()->prepare($consultaVS);
    $comandoVS->execute([$UserMaster['idSucursal'], $idVenta['idVenta'], $nroV]);
    Database::getInstance()->getDb()->commit();
    //PONE ESTADO =0 LA EXISTENCIA DEL PRODUCTO QUE SU STOCK ESTE EN 0
    $updatExistencias = "UPDATE Existencias SET terminado=0 WHERE stock=0";
    $updatResultadoExistencias = Database::getInstance()->getDb()->prepare($updatExistencias);
    $updatResultadoExistencias->execute();
    $datos = array(
        0 => $idVenta['idVenta'],
        1 => 'true',
        2 => $idTipoimpresion,
        3 => $nota_de_entrega,
        4 => $destino
    );

    echo json_encode($datos);
}

?>

