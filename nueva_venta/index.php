<?php

require_once 'head.php';

//require_once '../datos/Database.php';

$idSesion = $_SESSION['idSesion'];

//*****Para validar las terminales****//

/*$consultaTerminal = "SELECT * FROM InicioTerminal it, Terminal t WHERE it.idInicio=? and it.idTerminal=t.idTerminal";

$comandoTerminal = Database::getInstance()->getDb()->prepare($consultaTerminal);

$comandoTerminal->execute([$idSesion]);

$terminal = $comandoTerminal->fetch();*/

//$conttt = 1 ;

//$rut ="";

//while ($conttt <= 69){

//    $rut = $conttt.".jpg";

//$consultaP = "UPDATE Producto SET imagen='$rut' WHERE codigoProducto='$conttt'";

//$comandoP = Database::getInstance()->getDb()->prepare($consultaP);

//$comandoP->execute();

//$conttt++;

//}


/*if ($comandoTerminal->rowCount() == 0) {

    header('Location: ../inicio');

}

if ($terminal['estado'] == 0) {

    header('Location: ../inicio');

}*/

?>

<script src="../js/jquery.js"></script>

<script src="jventa.js"></script>

<script src="../js/autocompletado/jquery-1.10.2.min.js"></script>

<script src="../js/autocompletado/jquery-ui-1.10.3.custom.min.js"></script>

<link rel="stylesheet" href="../js/autocompletado/jquery-ui-1.10.3.custom.min.css">

<!-- Page Content -->

<div class="page-content">

    <!-- Page Header -->

    <div class="page-header position-relative">

        <div class="header-title">

            <h1>

                <strong>NUEVA VENTA</strong>

            </h1>

        </div>

        <!--Header Buttons-->

        <div class="header-buttons">

            <a class="sidebar-toggler" href="#">

                <i class="fa fa-arrows-h"></i>

            </a>

            <a class="refresh" id="refresh-toggler" href="#">

                <i class="glyphicon glyphicon-refresh"></i>

            </a>

            <a class="fullscreen" id="fullscreen-toggler" href="#">

                <i class="glyphicon glyphicon-fullscreen"></i>

            </a>

        </div>

        <!--Header Buttons End-->

    </div>

    <!-- /Page Header -->

    <!-- Page Body -->

    <div class="page-body">

        <div class="row">

            <div class="col-lg-12 col-sm-12 col-xs-12">

                <div class="row">

                    <div class="col-lg-12 col-sm-12 col-xs-12">

                        <div class="widget-body">

                            <br/>

                            <div id="imagenes" class="col-sm-2" hidden>

                                <img src="" width="170" height="180" align="right" id="im_prod"/>

                            </div>

                            <div class="form-inline" role="form" align="left">

                                <div class="col-sm-1">

                                </div>

                                <div class="col-sm-2">

                                    <?php

                                    $consulataCambio = "SELECT * FROM TipoCambio WHERE estado = 1";

                                    $comandoCambio = Database::getInstance()->getDb()->prepare($consulataCambio);

                                    $comandoCambio->execute();

                                    $tipoCambio = $comandoCambio->fetch();

                                    ?>

                                    <div class="tickets-container" hidden>

                                        <ul class="tickets-list">

                                            <li class="ticket-item" style="background-color: #0088cc">

                                                <span class="user-name"><strong><font size="2" style="color: #F0F0F0">Tipo

                                                            de cambio: </font>

                                                        <center><font size="3"

                                                                      style="color: #F0F0F0"><?= $tipoCambio['tipoCambioCompra'] ?>

                                                                Bs.</font></center>

                                                    </strong></span>

                                                <input type="text" id="tipoCambio" name="tipoCambio"

                                                       value="<?= $tipoCambio['tipoCambioCompra'] ?>" hidden/>

                                                <input type="text" id="idtipoCambio" name="idtipoCambio"

                                                       value="<?= $tipoCambio['idTipoCambio'] ?>" hidden/>

                                            </li>

                                        </ul>

                                    </div>

                                </div>

                                <div class="col-sm-2" hidden>

                                    <select name="almacen" id="almacen">

                                        <option value="1">Almacen</option>

                                    </select>

                                    <input id="idUsarioDes" name="idUsarioDes" value=""

                                           hidden/>

                                </div>

                            </div>

                            <div class="row" role="form" align="left">

                                <div class="col-sm-2">

                                    <label class="control-label">&nbsp;</label> <br>

                                    <a id="nuevo-cliente" class="btn btn-warning shiny"><i class="fa fa-plus"></i>Nuevo

                                        Cliente</a>

                                </div>

                                <div class="col-sm-2">

                                    <label class="control-label"><strong>CI/NIT</strong></label>

                                    <input type="text" class="form-control" id="nitci" namborder-colore="nitci"

                                           placeholder="Nro. de NIT">

                                    <script>

                                        $('#nitci').autocomplete({

                                            source: function (request, response) {

                                                $.ajax({

                                                    url: 'autocompletado.php',

                                                    dataType: "json",

                                                    data: {

                                                        name_startsWith: request.term,

                                                        type: 'NitCi'

                                                    },

                                                    success: function (data) {

                                                        response($.map(data, function (item, nombre) {

                                                            return {

                                                                label: nombre,

                                                                value: nombre,

                                                                id: item

                                                            };

                                                        }));

                                                    }

                                                });

                                            },

                                            select: function (event, ui) {

                                                var Date = (ui.item.id);

                                                var elem = Date.split('/');

                                                $('#nombreCliente').val(elem[0]);

                                                $('#dNombreCliente').val(elem[0]);

                                                $('#nitCliente').val(ui.item.value);

                                                $('#descuentoC').val(elem[1]);

                                            }

                                        });

                                        $('#nitci').on('keyup', function () {

                                            var nit = $('#nitci').val();

                                            $('#nitCliente').val(nit);

                                        });

                                    </script>

                                </div>

                                <div class="col-sm-3">

                                    <label class="control-label"><strong>Cliente</strong></label>

                                    <input type="text" class="form-control" id="nombreCliente" name="nombreCliente"

                                           placeholder="Nombre del cliente">

                                    <script>

                                        $('#nombreCliente').autocomplete({

                                            source: function (request, response) {

                                                $.ajax({

                                                    url: 'autocompletado.php',

                                                    dataType: "json",

                                                    data: {

                                                        name_startsWith: request.term,

                                                        type: 'nombreCliente'
                                                    },
                                                    success: function (data) {
                                                        response($.map(data, function (item, nit) {
                                                            return {
                                                                label: nit,
                                                                value: nit,
                                                                id: item
                                                            };
                                                        }));
                                                    }
                                                });
                                            },
                                            select: function (event, ui) {
                                                var Date = (ui.item.id);
                                                var elem = Date.split('/');
                                                $('#nitCliente').val(elem[0]);
                                                $('#nitci').val(elem[0]);
                                                $('#dNombreCliente').val(ui.item.value);
                                                $('#descuentoC').val(elem[1]);
                                            }
                                        });
                                        $(function () {
                                            $('#nombreCliente').on('keyup', function () {
                                                var nombre = $('#nombreCliente').val();
                                                $('#dNombreCliente').val(nombre);
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="widget-body">
                            <div class="form-group">
                                <center><font size=5><strong>DETALLE</strong></font></center>
                            </div>
                            <br/>
                            <form action="javascript: tieneStock();" method="post" id="notaVenta" name="notaVenta">
                                <!-- TABLA DE NOTA DE VENTA-->
                                <input type="text" id="nitCliente" name="nitCliente" hidden><!-- NIT DEL CLIENTE-->
                                <input type="text" id="dNombreCliente" name="dNombreCliente" hidden>
                                <!-- NOMBRE DEL CLIENTE-->
                                <input type="text" id="Tcambio" name="Tcambio"
                                       value="<?= $tipoCambio['tipoCambioCompra'] ?>" hidden/><!--TIPO DE CAMBIO-->
                                <input type="text" id="idTcambio" name="idTcambio"
                                       value="<?= $tipoCambio['idTipoCambio'] ?>" hidden/><!--TIPO DE CAMBIO-->
                                <input type="text" id="descuentoC" name="descuentoC" value="0.00" hidden/>
                                <input type="text" value="SinNota" id="notaEntrega" name="notaEntrega" hidden>
                                <!--DESTINO-->
                                <!--Decsuento del cliente-->
                                <table id="grilla" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th><strong>Nro</strong></th>
                                        <th><strong>Codigo</strong></th>
                                        <th><strong>Descripcion</strong></th>
                                        <th><strong>Unidad</strong></th>
                                        <th><strong>Precio</strong></th>
                                        <th><strong>Cantidad</strong></th>
                                        <th><strong>Ubicacion</strong></th>
                                        <th><strong>Monto</strong></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!-- SE AGREGAN LAS FILAS DE LOS PRODUCTOS POR VENDER -->
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td><strong>--</strong></td>
                                        <td><strong>--</strong></td>
                                        <td>
                                            <input id="descripcionAgr" name="descripcionAgr" type="text"
                                                   Class="form-control" size="40"
                                                   placeholder="Descripcion / Codigo barras / Codigo Producto"
                                                   required/>
                                            <script>
                                                $('#descripcionAgr').on('keydown', function (key) {
                                                    if (key.keyCode == 13) {
                                                        // alert($('#descripcionAgr').val());
                                                        var frm = $('#notaVenta').serialize();
                                                        $.ajax({
                                                            url: 'venta_codBarras.php',
                                                            data: frm + '&idAlm=' + $('#almacen').val(),
                                                            type: 'post',
                                                            success: function (valores) {
                                                                var datos = eval(valores);
                                                                mostrarImagen(datos[5]);
                                                                $('#descripcionAgr').val(datos[0]);
                                                                $('#unidadAgr').val(datos[1]);
                                                                $('#precioAgr').val(datos[2]);
                                                                $('#ubicacionAgr').val(datos[3]);
                                                                $('#codBarras').val(datos[4]);
                                                                $('#cantidadAgr').focus();
                                                                $('#cantidadAgr').val(datos[6]);
                                                                $('#datito').val($('#almacen').val());
                                                                $('#idProd').val(datos[5]);
                                                                document.notaVenta.submit();
                                                            }
                                                        });
                                                    }
                                                    if (key.keyCode == 16) {
                                                        $('#descripcionAgr').val("");
                                                        $('#descripcionAgr').focus();
                                                    }
                                                });
                                                function mostrarImagen(id) {
                                                    var idp = id;
                                                    $('#notaVenta').serialize();
                                                    var url = 'busca_imagen.php';
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: url,
                                                        data: 'idp=' + idp,
                                                        success: function (valores) {
                                                            $('#imagenes').html(valores);
                                                        }
                                                    });
                                                }
                                                ;
                                                $('#descripcionAgr').autocomplete({
                                                    source: function (request, response) {
                                                        //alert(JSON.stringify(request.term));
                                                        $.ajax({
                                                            url: 'autocompletado.php',
                                                            dataType: "json",
                                                            data: {
                                                                name_startsWith: request.term,
                                                                data: 1,
                                                                type: 'Detalle'
                                                            },
                                                            success: function (data) {
                                                                response($.map(data, function (item, id) {
                                                                    return {
                                                                        label: id,
                                                                        value: id,
                                                                        id: item
                                                                    };
                                                                }));
                                                            }
                                                        });
                                                    },
                                                    select: function (event, ui) {
                                                        var id = ui.item.id;
                                                        $('#notaVenta').serialize();
                                                        var url = 'venta_detalle.php';
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: url,
                                                            data: 'id=' + id,
                                                            success: function (valores) {
                                                                var datos = eval(valores);
                                                                //$('#cont').val(f);
                                                                mostrarImagen(id);
                                                                $('#idProd').val(id);
                                                                $('#descripcionAgr').val(datos[0]);
                                                                $('#unidadAgr').val(datos[1]);
                                                                $('#precioAgr').val(datos[2]);
                                                                $('#ubicacionAgr').val(datos[3]);
                                                                $('#codBarras').val(datos[4]);
//                                                                    $('#im_prod').val(datos[5]);
//                                                                   alert ( $('#im_prod').val(datos[5]));
                                                                $('#cantidadAgr').focus();
                                                                $('#datito').val($('#almacen').val());
                                                            }
                                                        });
                                                    }
                                                });
                                            </script>
                                            <!--<input type="text" id="dati" name="dati">CONTADOR DE FILAS DE LA TABLA -->
                                            <input type="text" id="cont" name="cont" hidden>
                                            <!--CONTADOR DE FILAS DE LA TABLA -->
                                            <input type="text" id="idProd" name="idProd" hidden>
                                            <!--CODIGO DE PRODUCTO-->
                                        </td>
                                        <td>
                                            <input name="unidadAgr" type="text" id="unidadAgr" class="form-control"
                                                   size="4" placeholder="" readonly/>
                                            <input type="text" name="datito" id="datito" hidden/>
                                            <input type="text" id="codBarras" name="codBarras" hidden>
                                            <!--CODIGO DE BARRAS O DE PRODUTO-->
                                        </td>
                                        <td>
                                            <input name="precioAgr" type="text" id="precioAgr" class="form-control"
                                                   size="4" style="text-align: right" readonly/>
                                        </td>
                                        <td>
                                            <input name="cantidadAgr" type="text" id="cantidadAgr" class="form-control"
                                                   size="4" style="text-align: right"
                                                   onkeypress="validarNumeroVentas(event);" required/>
                                        </td>
                                        <td>
                                            <input name="ubicacionAgr" type="text" id="ubicacionAgr"
                                                   class="form-control" size="4" style="text-align: right" readonly/>
                                        </td>
                                        <td></td>
                                        <td>
                                            <input name="agregar" type="submit" id="agregar" value="Agregar"
                                                   class="btn btn-primary"/>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <br/>
                                <table align="right">
                                    <tr>
                                        <td colspan="6" class="invisible bg-snow"></td>
                                        <td><br></td>
                                        <td><br></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="invisible bg-snow"></td>
                                        <td><br></td>
                                        <td><br></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="invisible bg-snow">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="invisible bg-snow"></td>
                                        <td><br></td>
                                        <td><br></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="invisible bg-snow">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="invisible bg-snow"></td>
                                        <td id="destinoO" hidden>
                                            <label class="control-label"><strong>Destino</strong></label>&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td style="padding-right: 50%">
                                            <div hidden id="divDestinoO"><strong><input class="form-control" type="text"
                                                                                        id="destinoO" name="destinoO"
                                                                                        size="70"
                                                                                        style="text-align: left"
                                                                                        hidden="hidden"/></strong>
                                            </div>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="invisible bg-snow"></td>
                                        <td colspan="7">
                                            <label class="control-label"><strong>Total</strong></label>&nbsp;
                                        </td>
                                        <td>
                                            <div><strong><input class="form-control" type="text" id="total" name="total"
                                                                size="10" readonly style="text-align: right"/></strong>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label"><strong>Efectivo Bs</strong></label>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td hidden>
                                            <label class="control-label"><strong>Efectivo $</strong></label>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>
                                            <label class="control-label"><strong>Cambio</strong></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong><input class="form-control" type="text" id="efectivoBs"
                                                           name="efectivoBs" style="text-align: right"
                                                           size="4"/></strong>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td hidden>
                                            <strong><input class="form-control" type="text" id="efectivo"
                                                           name="efectivo" style="text-align: right" size="4"/></strong>
                                        </td>
                                        <td></td>
                                        <td>
                                            <strong><input class="form-control" type="text" id="cambio" name="cambio"
                                                           style="text-align: right" size="4" readonly/></strong>
                                        </td>
                                    </tr>
                                </table>
                                <br/>
                                <div class="row" role="form">
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-6">
                                        <div id="destino" hidden class="form-group">
                                            <label ><strong>D E S T I N O :</strong></label>
                                            <div hidden id="divDestino" class="form-horizontal">
                                                <strong>
                                                    <input class="form-control" type="text" id="destino" name="destino"
                                                           size="70" style="text-align: left" hidden="hidden"/>
                                                </strong>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="col-sm-3"></div>

                                </div>
                            </form>
                        </div>
                        <br/>
                        <div class="widget-body">
                            <div class="container ">
                                <center>
                                    <button id="sinNota" class="btn btn-primary btn-lg" role="button">SIN ORDEN
                                        DE DESPACHO
                                    </button>
                                    <button id="conNota" class="btn btn-default btn-lg" role="button">CON ORDEN
                                        DE DESPACHO
                                    </button>
                                </center>
                            </div>
                            <center>
                                <br/>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="button" class="btn btn-success" id="notaDeVenta" name="notaDeVenta"
                                               onclick="registrarDetalle()" value="Nota De Venta">
                                    </div>
                                    <div class="form-group">
                                        <input type="button" class="btn btn-success" id="facturar" name="facturar"
                                               onclick="RegistrarFactura()" value="Facturar">
                                    </div>
                                    <div class="form-group">
                                    <a type="button" class="btn btn-danger" id="cancelar" name="cancelar"
                                       href="../nueva_venta/">Cancelar</a>
                                    </div>
                                </div>
                            </center>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<!-- /Page Container -->

<!-- Main Container -->

<!-- Modal para nuevo cliente -->

<div class="modal fade" id="edita-cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"

     aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title" id="myModalLabel"><b>Ver Datos de Cliente</b></h4>

            </div>

            <!--              aqui agregamos los registros-->

            <form id="formularioE" class="formulario">

                <div class="modal-body">

                    <div class="row">

                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="form-inline">

                                <div class="form-group">

                                    <label class="col-sm-2 control-label no-padding-right" for="nit">Nit</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                </div>

                                <div class="form-group">

                                    <input class="form-control" type="text" id="nitC" name="nitC">&nbsp;&nbsp;

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label no-padding-right">Nombre</label>&nbsp;&nbsp;&nbsp;

                                </div>

                                <div class="form-group">

                                    <input class="form-control" type="text" id="nombreClienteC" name="nombreClienteC">

                                </div>

                            </div>

                            <input id="idClienteC" name="idClienteC" hidden/>

                            <br>

                            <div class="form-inline">

                                <div class="form-group">

                                    <label class="col-sm-2 control-label no-padding-right" for="celular">Celular</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                </div>

                                <div class="form-group">

                                    <input class="form-control" type="text" id="telefonoCelC" name="telefonoCelC">

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label no-padding-right"

                                           for="telefono">Telefono</label>&nbsp;&nbsp;&nbsp;&nbsp;

                                </div>

                                <div class="form-group">

                                    <input class="form-control" type="text" id="telefonoFijoC" name="telefonoFijoC">

                                </div>

                            </div>

                            <br>

                            <div class="form-inline">

                                <div class="form-group">

                                    <label class="col-sm-2 control-label no-padding-right"

                                           for="direccion">Direccion</label> &nbsp;&nbsp;

                                </div>

                                <div class="form-group">

                                    <input class="form-control" type="text" id="direccionC" name="direccionC">

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label no-padding-right" for="email">Email</label>

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                </div>

                                <div class="form-group">

                                    <input class="form-control" type="text" id="emailC" name="emailC">

                                </div>

                            </div>

                            <br>

                            <div class="form-inline">

                                <div class="form-group">

                                    <label class="col-sm-2 control-label no-padding-right" for="banco">Banco</label>

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                </div>

                                <div class="form-group">

                                    <input class="form-control" type="text" id="bancoC" name="bancoC">

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label no-padding-right" for="nrocuenta">Nro.

                                        Cuenta</label>

                                </div>

                                <div class="form-group">

                                    <input class="form-control" type="text" id="nroCuentaC" name="nroCuentaC">

                                </div>

                            </div>

                            <!--BOTONES-->

                            <br>

                            <center>

                                <footer>

                                    <div class="form-inline">

                                        <div class="form-group">

                                            <button onclick="nuevoCliente();" data-dismiss="modal" aria-hidden="true"

                                                    class="btn btn-primary" id="nuevo">Guardar

                                            </button>

                                            &nbsp;&nbsp;&nbsp;&nbsp;

                                            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">

                                                Cancelar

                                            </button>

                                        </div>

                                    </div>

                                </footer>

                            </center>

                        </div>

                    </div>

                </div>

            </form>

        </div>

    </div>

</div>

<!-- End Formulario Modal-->

<!--Basic Scripts-->

<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/slimscroll/jquery.slimscroll.min.js"></script>

<!--Beyond Scripts-->

<script src="assets/js/beyond.min.js"></script>

<!--Page Related Scripts-->

<script src="assets/js/bootbox/bootbox.js"></script>

<?php require_once '../header_footer/footer.php'; ?>



