<?php

require_once("../datos/Database.php");
session_start();
$UserMaster = $_SESSION['userMaster'];
$idSesion = $_SESSION['idSesion'];

$idProducto = $_POST['idProducto'];
//$unidad=$_POST['unidad'];
$precio = $_POST['precio'];
$cantidad = $_POST['cantidad'];
//$ubicacion=$_POST['ubicacion'];
//$monto=$_POST['monto'];
$idAlmacen = $_POST['idAlmacen'];

$idCliente = $_POST['nitCliente'];
$TipoCambio = $_POST['Tcambio'];
$subTotal = $_POST['subTotal'];
//$descuentoCli=$_POST['descuentoCl'];
//$descuentoP=$_POST['descuentoP'];
//$descuentoB=$_POST['descuentoB'];
$total = $_POST['total'];
$efectivoBs = $_POST['efectivoBs'];
$efectivo = $_POST['efectivo'];
$cambio = $_POST['cambio'];
ini_set("date.timezone", "America/La_Paz");
$fecha = date('Y-m-d');
$hora = date("H:i:s");


if (Database::getInstance()->getDb()->beginTransaction()) {
//$comando->execute();
//CREAR LA TRANSACCION
    $consulTransa = "INSERT INTO Transaccion(tipoTrans, fecha, hora, idInicioSesion, idUsuario, estado) VALUES(?, ?, ?, ?, ?, ?)";
    $comandosTransa = Database::getInstance()->getDb()->prepare($consulTransa);
    $comandosTransa->execute([1, $fecha, $hora, $idSesion, $UserMaster['idUsuario'], 1]);

//OBTENER EL ID TRANSACCION NUEVO
    $idTransaccion = Database::getInstance()->getDb()->prepare("Select idTransaccion As idTransaccion from Transaccion Where fecha =? and hora =? ORDER BY idTransaccion DESC LIMIT 1");
    $idTransaccion->execute([$fecha, $hora]);
    $idTr = $idTransaccion->fetch(PDO::FETCH_ASSOC);
    if ($idTr == NULL) {
        Database::getInstance()->getDb()->rollBack();
        $datos = array(
            0 => "No obtiene idTransaccion",
            1 => 'false'
        );

        echo json_encode($datos);
        exit();
    }

    for ($index = 0; $index < count($idAlmacen); $index++) {
        $idAl = $idAlmacen[$index];
        $idProd = $idProducto[$index];
        $nCantidad = $cantidad[$index];
        $nPrecio = $precio[$index];

        // OBTIENE EL STOCK TOTAL
        $comandoStock = Database::getInstance()->getDb()->prepare("SELECT SUM(stock) as stock, nombreComercial FROM Inventario  WHERE codigoProducto=? AND idAlmacen=? AND idSucursal=? GROUP BY codigoProducto");
        $comandoStock->execute([$idProd, $idAl, $UserMaster['idSucursal']]);
        $stockTotal = $comandoStock->fetch();
        if ($stockTotal['stock'] < $nCantidad) {
            Database::getInstance()->getDb()->rollBack();
            $datos = array(
                0 => "No existe stock suficientes para: " . $stockTotal['nombreComercial'],
                1 => 'false'
            );
            echo json_encode($datos);
            exit();
        }
    }
    for ($index = 0; $index < count($idAlmacen); $index++) {
        $idAl = $idAlmacen[$index];
        $idProd = $idProducto[$index];
        $nCantidad = $cantidad[$index];
        $nPrecio = $precio[$index];

        $consulta = "SELECT* FROM Inventario WHERE codigoProducto=? AND idAlmacen=? AND idSucursal=? AND stock>0 ORDER BY fechaVencimiento ASC";
        $comandos = Database::getInstance()->getDb()->prepare($consulta);
        $comandos->execute([$idProd, $idAl, $UserMaster['idSucursal']]);

        while ($row = $comandos->fetch()) {
            $stock = $row['stock'];
            // echo $row .'_';

            if ($stock >= $nCantidad) {
                $consulDeTansac = "INSERT INTO DetalleTransaccion(idTransaccion, idExistencia, codigoProducto, cantidad, precioVenta) VALUES(?, ?, ?, ?, ?)";
                $comandoDeTansac = Database::getInstance()->getDb()->prepare($consulDeTansac);
                $comandoDeTansac->execute([$idTr['idTransaccion'], $row['idExistencia'], $idProd, $nCantidad, $nPrecio]);

                $idLote = $row['idLote'];
                $idUbicacion = $row['idUbicacion'];
                $updat = "UPDATE Existencias SET stock = (stock - $nCantidad) WHERE idLote=? AND idUbicacion=?";
                $updatResultado = Database::getInstance()->getDb()->prepare($updat);
                $updatResultado->execute([$idLote, $idUbicacion]);


                break;
            } else {
                $consulDeTansac = "INSERT INTO DetalleTransaccion(idTransaccion, idExistencia, codigoProducto, cantidad, precioVenta) VALUES(?, ?, ?, ?, ?)";
                $comandoDeTansac = Database::getInstance()->getDb()->prepare($consulDeTansac);
                $comandoDeTansac->execute([$idTr['idTransaccion'], $row['idExistencia'], $idProd, $stock, $nPrecio]);
                $nCantidad = $nCantidad - $stock;

                $idLote = $row['idLote'];
                $idUbicacion = $row['idUbicacion'];
                $updat = "UPDATE Existencias SET stock = (stock - $stock) WHERE idLote=? AND idUbicacion=?";
                $updatResultado = Database::getInstance()->getDb()->prepare($updat);
                $updatResultado->execute([$idLote, $idUbicacion]);
            }
        }
    }

//OBTENGO idCliente
    if ($idCliente == "") {
        $idCl = 1;
    } else {
        $consultaC = "SELECT* FROM Cliente WHERE nit = ? or ci=?";
        $comandoC = Database::getInstance()->getDb()->prepare($consultaC);
        $comandoC->execute([$idCliente, $idCliente]);
        $id = $comandoC->fetch();
        $idCl = $id['idCliente'];
    }

//INSERTAR DATOS DE VENTAS
    $descuento = $subTotal - $total;
    $consultaV = "INSERT INTO Ventas(idVenta, tipoVenta, idTransaccion, idCliente, montoSubTotal, descuento, montoTotal, tipoCambio, efectivoSus, efectivoBs, cambio) VALUES ('','1',?,?,?,?,?,?,?,?,?)";
    $comandoV = Database::getInstance()->getDb()->prepare($consultaV);
    $comandoV->execute([$idTr['idTransaccion'], $idCl, $subTotal, $descuento, $total, $TipoCambio, $efectivo, $efectivoBs, $cambio]);

//OBTENEMOS EL idVenta PARA la nota de venta 
    $consultaId = "SELECT * FROM Ventas WHERE idTransaccion = '" . $idTr['idTransaccion'] . "'";
    $comandoId = Database::getInstance()->getDb()->prepare($consultaId);
    $comandoId->execute();
    $idVenta = $comandoId->fetch();
    // obtengo la ultima venta de la sucursal
    $consultaNV = "SELECT Max(nroVenta) as nroVenta FROM VentaSucursal WHERE   idSucursal= '" . $UserMaster['idSucursal'] . "'";
    $comandoNV = Database::getInstance()->getDb()->prepare($consultaNV);
    $comandoNV->execute();
    $nroVenta = $comandoNV->fetch();
//INSERTAR DATOS Nota de venta
    $nroV = $nroVenta['nroVenta'] + 1;
    $consultaVS = "INSERT INTO VentaSucursal(idSucursal, idVenta, nroVenta) VALUES (?,?,?)";
    $comandoVS = Database::getInstance()->getDb()->prepare($consultaVS);
    $comandoVS->execute([$UserMaster['idSucursal'], $idVenta['idVenta'], $nroV]);
    Database::getInstance()->getDb()->commit();
    $datos = array(
        0 => $idVenta['idVenta'],
    );

    echo json_encode($datos);
}
?>

