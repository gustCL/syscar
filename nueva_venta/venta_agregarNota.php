<?php
require_once '../datos/Database.php';

$contador= $_POST['cont'];
$idProducto=$_POST['idProd'];
$descripcion=$_POST['descripcionAgr'];
$unidad=$_POST['unidadAgr'];
$precio = $_POST['precioAgr'];
$cantidad = $_POST['cantidadAgr'];
$ubicacion = $_POST['ubicacionAgr'];
$almacen = $_POST['datito'];
$codigoBarra=$_POST['codBarras'];
$accion='0';
$descuent= $_POST['descuentoC'];//descuento por cliente
//
$descuentoPorcen=$descuent;
/////Consulta para validar si exsiste promocion de ventas
$consulta="SELECT * FROM PromocionDescuento WHERE estado =1";
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$descuentos=$comando->fetch();
if ($descuentos['idPromoDes'] > 1) {
    $descuent=$descuentos['descuentoPromo'];
    $accion='1';
    $descuentoPorcen=$descuentos['descuentoPromo'];
}
//agregamos un producto a la nota de venta
$montoRedondeado = round($cantidad*$precio,2);
$tabla='<tr>
<td>'.$contador.'</td>
<td><input type="text" value="'.$codigoBarra.'" id="codigoB[]" name="codigoB[]" hidden/>'.$codigoBarra.'</td>
<td><input type="text" value="'.$idProducto.'" id="idProducto" name="idProducto[]" hidden/><input type="text" value="'.$descripcion.'" id="descrip" name="descrip[]" hidden/>'.$descripcion.'</td>
<td><input type="text" value="'.$unidad.'" id="unidad" name="unidad[]" hidden/>'.$unidad.'</td>
<td><input type="text" value="'.$precio.'" id="precio'.$contador.'" name="precio[]" hidden/>'.$precio.'</td>
<td><input type="text" class="form-control" value="'.$cantidad.'" id="cantida'.$contador.'" name="cantidad[]" size="4" onkeyup="modificarDetalle('.$contador.')" onclick="modificaDetalle('.$contador.')" style="text-align: right"/><input type="text" value="'.$almacen.'" id="idAlmacen" name="idAlmacen[]" hidden/></td>\n\
<td><input type="text" value="'.$ubicacion.'" id="ubicacion" name="ubicacion[]" hidden/>'.$ubicacion.'</td>
<td><input type="text" class="form-control" value="'.$montoRedondeado.'" id="monto'.$contador.'" name="monto[]" size="4" style="text-align: right" readonly/></td>
<td><a class="elimina"><i class="fa fa-trash-o" /></a></td>
</tr>';
$Subtotal = $montoRedondeado;
//$descuentoBs= ($descuent*$subTotal)/100;
$descuent = 1-($descuent);

$datos = array(
            0 => $tabla, 
            1 => $contador, 
            2 => $Subtotal,
            3 => $descuent,
            4 => ($descuentoPorcen*100),
            5 => $accion,
        );

echo json_encode($datos);
?>



