
<?php
require_once ('head.php');
$varMontoTotal = $_GET['nomto'];
$varMontoBS = $_GET['montoBs'];
$varIdVenta = $_GET['idVenta'];
$fecha = Date('d-m-Y');



//recuperamos el interes del cliente
$queryInt = "SELECT cl.* FROM Cliente cl, Ventas v WHERE v.idVenta = '$varIdVenta' AND v.idCliente = cl.idCliente";
$comandoP = Database::getInstance()->getDb()->prepare($queryInt);
$comandoP->execute();
$interes = $comandoP->fetch();
$inter = $interes['interes'];
?>
<link href="../css/estilo.css" rel="stylesheet">
<script src="../js/jquery.js"></script>
<script src="jventa.js"></script>
<script>
    function val(e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 8)
            return true;
        patron = /[A-Za-z]/;
        te = String.fromCharCode(tecla);
        return patron.test(te);
    }

    function  validarNro(e) {
        var key;
        if (window.event) // IE
        {
            key = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            key = e.which;
        }

        if (key < 48 || key > 57)
        {
            if (key == 46 || key == 8) // Detectar . (punto) y backspace (retroceso)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return true;
    }

//                                   

</script> 

<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong> NUEVO ALMACEN </strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">                                          
                    <div class="widget-body">
                        <div>
                            <form id="ventaCreditoNota" method="post" name="ventaCreditoNota">
                                <center><br>
                                    <h4><strong>NUEVO PLAN DE CUOTA</strong></h4><br><br>
                                    <div class="form-group">
                                        <input id="punteroVenta" name="punteroVenta" value="<?= $varIdVenta; ?>" readonly hidden/>
                                    </div>
                                    <div class="form-group-md">
                                        <label class="col-lg-2 control-label" for="cuotaMontoTotal"><strong>Monto Total a Pagar</strong></label>
                                        <div class="col-lg-4">
                                            <input  readonly type="text" required class="form-control" id="cuotaMontoTotal" name ="cuotaMontoTotal" placeholder="Descripcion" value="<?= $varMontoTotal ?>"/>
                                        </div>
                                    </div>
                                    <!--<input id="nroVentaCr" hidden="true"/>-->
                                    <div class="form-group-md">
                                        <label class="col-lg-2 control-label" for="cuotaFechaInicial"><strong>Fecha de Inicio</strong></label>
                                        <div class="col-lg-4">
                                            <input type="text" required class="form-control" id="cuotaFechaInicial" name ="cuotaFechaInicial" value="<?= $fecha ?>" readonly/>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <br><br>
                                    <div class="form-group-md">
                                        <label class="col-lg-2 control-label" for="cuotaMontoInicial"><strong>Cuota Inicial</strong></label>
                                        <div class="col-lg-4">
                                            <input type="text" readonly  class="form-control" id="cuotaMontoInicial" name ="cuotaMontoInicial" value="<?= $varMontoBS; ?>"/>
                                        </div>
                                    </div>
                                    <div class="form-group-md">
                                        <label class="col-lg-2 control-label" for="cuotaFechaFin"><strong>Fecha de Finalizacion</strong></label>
                                        <div class="col-lg-4">
                                            <input type="date" required class="form-control" id="cuotaFechaFin" name ="cuotaFechaFin" placeholder="Descripcion"
                                                   data-bv-notempty="true"
                                                   data-bv-notempty-message="Ingrese datos en campo vacio" readonly/>
                                        </div>
                                    </div>
                                    <br><br><br>
                                    <div class="form-group-md">
                                        <label class="col-lg-2 control-label" for="cuotaPlazo"><strong>Plazo</strong></label>
                                        <div class="col-lg-4">
                                            <input type="text" required class="form-control" id="cuotaPlazo" name ="cuotaPlazo" placeholder="Descripcion" onkeypress="javascript:return validarNro(event)"/>
                                        </div>
                                    </div>
                                    <div class="form-group-md">
                                        <label class="col-lg-2 control-label" for="cuotaNroCuotas"><strong>Numero de Cuotas</strong></label>
                                        <div class="col-lg-4">
                                            <input type="text" required class="form-control" id="cuotaNroCuotas" name ="cuotaNroCuotas" placeholder="Descripcion" onkeypress="javascript:return validarNro(event)" />
                                        </div>
                                    </div>
                                    <br><br><br>
                                    <div class="form-group-md">
                                        <label class="col-lg-2 control-label" for="cuotaInteres"><strong>Tasa de interes</strong></label>
                                        <div class="col-lg-4">
                                            <input type="text" value="<?= $inter?>" required class="form-control" id="cuotaInteres" name ="cuotaInteres" placeholder="Descripcion"
                                                   data-bv-notempty="true"
                                                   data-bv-notempty-message="Ingrese datos en campo vacio" onkeypress="javascript:return validarNro(event)"/>
                                        </div>
                                    </div>
                                    <div class="form-group-md">
                                        <label class="col-lg-2 control-label" for="cuotaMontoPorCuota"><strong>Monto por cuota</strong></label>
                                        <div class="col-lg-4">
                                            <input type="text"   required class="form-control" id="cuotaMontoPorCuota" name ="cuotaMontoPorCuota" placeholder="Descripcion"
                                                   data-bv-notempty="true"
                                                   data-bv-notempty-message="Ingrese datos en campo vacio" />
                                        </div>
                                    </div>
                                    <br><br><br>
                                    <div class="form-group-md" >
                                        <div class="form-group">
                                            <input type="button" class="btn btn-default"  id="btncalcularFechas" name="btncalcularFechas" value="Calcular Fechas de Pago">
                                        </div>
                                        <textarea rows="10"  class="form-group-md" readonly disabled="true" id="fechasDePago"></textarea>
                                        <div class="form-group">
                                            <input type="button" class="btn btn-success" name="imprimirCr" id="imprimirCr"  value= "Imprimir nota" onclick="fn_registrarVC()" placeholder=""/>
                                        </div>
                                        <div class="form-group">
                                            <input type="button" class="btn btn-primary"  id="facturarVentaCredito" name="facturarVentaCredito" onclick="fn_registrarFC()" value="  Imprimir  Factura">
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br><br>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>            
    </div>
</div>
<!-- F U N C I O N E S   J A V A   S C R I P T -->
<script type="text/javascript">
    $('#btncalcularFechas').on('click', function() {
        var numeroDeCuotas = $('#cuotaNroCuotas').val();
        numeroDeCuotas = parseInt(numeroDeCuotas);
        var dias = $('#cuotaPlazo').val();
        var fechainicio = $('#cuotaFechaInicial').val();
        dias = parseInt(dias);
        var cantididad = dias / numeroDeCuotas;
        var intervalo = cantididad;
        cantididad = parseInt(cantididad);
        var fechasDePago = "";
        for (i = 0; i < numeroDeCuotas; i++) {
            fechasDePago = fechasDePago + fechaSumaReves(cantididad, fechainicio) + '\n';
            cantididad = cantididad + intervalo;
        }
        $('#fechasDePago').val(fechasDePago);
    });
//    
</script>

<?php

function fechaActual() {
    $fecha = Date('d/m/Y');
    return $fecha;
}

require_once ('../header_footer/footer.php');
?>