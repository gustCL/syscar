$(document).ready(function () {
    doc_promocion();
    $('#cuotaPlazo').blur(function () {
        var dias = $('#cuotaPlazo').val();
        dias = parseInt(dias);
        var nvoFechaFin = fechaSuma(dias);
        $('#cuotaFechaFin').val(nvoFechaFin);
    });
    $('#cuotaFechaFin').blur(function () {
        var fechaIn = $('#cuotaFechaInicial').val();
        var fechaFi = $('#cuotaFechaFin').val();
        var numeroDias = diferenciaFechas(fechaIn, fechaFi);
        $('#cuotaPlazo').val(numeroDias);
    });

    $('#cuotaNroCuotas').on('keyup', function () {
        if ($('#cuotaNroCuotas').val() != "" && $('#cuotaPlazo').val() != "" && $('#cuotaInteres').val() != "") {
            $('#cuotaMontoPorCuota').val(cuotas());
        } else {
            $('#cuotaMontoPorCuota').val("");
        }
        ;

    });

    $('#cuotaPlazo').on('keyup', function (event) {
        if (event.keyCode == 13) {
            $('#cuotaNroCuotas').focus();
//          }  
        }
    });

    $("#sinNota").on('click', function () {
        $('#sinNota').attr("class", "btn btn-primary btn-lg");
        $('#conNota').attr("class", "btn btn-default btn-lg");
        $("#notaEntrega").val("SinNota");
        $("#destino").hide();
        $("#divDestino").hide();
    });

    $("#conNota").on('click', function () {
        $('#conNota').attr("class", "btn btn-primary btn-lg");
        $('#sinNota').attr("class", "btn btn-default btn-lg");
        $("#notaEntrega").val("ConNota");
        $("#destino").show();
        $("#divDestino").show();
    });
});
$(function () {
    $('#nuevo-cliente').on('click', function () {
        $('#formularioE')[0].reset();
        $('#edita-cliente').modal({
            show: true,
            backdrop: 'static'
        });
    });
    $('#cantidadAgr').on('keyup', function (event) {
        if (event.keyCode == 13) {
            return true;
        }
        var precio = $('#precioAgr').val();
        var cantidad = $('#cantidadAgr').val();
        if ($.isNumeric(cantidad) && $.isNumeric(precio)) {
            cantidad = parseFloat(cantidad);
            precio = parseFloat(precio);
            var total = cantidad * precio;
            $('#totalProducto').val(total.toFixed(2));
        } else {
            $('#totalProducto').val("0.00");
        }
        return false;
    });

    //devuelbe el descuento en bolivianos y el total del descuento
    $('#descuentoP').on('keyup', function () {
        var descuento = $('#descuentoP').val();
        var subtotal = $('#subTotal').val();
        var descuentoUsuario = parseFloat($('#idUsarioDes').val());
        var descuentoCliente = parseFloat($('#descuentoC').val());
        descuentoUsuario = descuentoUsuario * 100;
        descuentoCliente = descuentoCliente * 100;
        //enontrar el mayor descuento
        var descuentoUC = descuentoUsuario;
        if (descuentoCliente > descuentoUsuario) {
            descuentoUC = descuentoCliente;
        }
        /////////////////////////
        if (descuento <= descuentoUC) {
            if ($.isNumeric(descuento) && $.isNumeric(subtotal)) {
                descuento = parseFloat(descuento);
                subtotal = parseFloat(subtotal);
                var descuentoBs = parseFloat(descuento * subtotal) / 100;
                var total = parseFloat(subtotal - descuentoBs);
                $('#descuentoB').val(descuentoBs.toFixed(2));
                $("#total").val(total.toFixed(2));

            } else {
                $('#descuentoB').val("0.00");
                $("#total").val($('#subTotal').val());
            }
        } else {
            $('#descuentoB').val("0.00");
            $("#total").val($('#subTotal').val());
            // alert('Solo puede dar hasta el ' + descuentoUC + '% de descuento');
            sweetAlert("Solo puede dar hasta el " + descuentoUC + "% de descuento", "", "warning");

            $('#descuentoP').val(" ");
        }
        return false;
    });
    //devuelbe el descuento en porcentaje y el total del descuento
    $('#descuentoB').on('keyup', function () {
        var descuento = $('#descuentoB').val();
        var subtotal = $('#subTotal').val();
        var descuentoUsuario = $('#idUsarioDes').val();
        var descuentoCliente = $('#descuentoC').val();
        descuentoUsuario = parseFloat(descuentoUsuario);
        descuentoCliente = parseFloat(descuentoCliente);
        subtotal = parseFloat(subtotal);
        //enontrar el mayor descuento
        var rango = (descuentoUsuario * subtotal);
        if (descuentoCliente > descuentoUsuario) {
            rango = (descuentoCliente * subtotal);
        }
        /////////////////////////

        if (descuento <= rango) {
            if ($.isNumeric(descuento) && $.isNumeric(subtotal)) {
                descuento = parseFloat(descuento);
                var descuentoP = parseFloat(((descuento * 100) / subtotal));
                var total = parseFloat(subtotal - descuento);
                $('#descuentoP').val(descuentoP.toFixed(2));
                $("#total").val(total.toFixed(2));

            } else {
                $('#descuentoP').val("0.00");
                $("#total").val($('#subTotal').val());
            }
        } else {
            $('#descuentoP').val("0.00");
            $("#total").val($('#subTotal').val());
            // alert('Solo puede dar hasta ' + rango + '  de descuento');
            sweetAlert("Solo puede dar hasta " + rango + " de descuento", "", "warning");

            $('#descuentoB').val(" ");
        }
        return false;
    });

    $('#efectivoBs').on('keyup', function () { //obtiene el tipo de cambio con efectivo en Bs
        var efectivo = $('#efectivoBs').val();
        var total = $('#total').val();
        var efectivos = $('#efectivo').val();
        var tipoCambio = $('#tipoCambio').val();

        if ($.isNumeric(efectivo) && $.isNumeric(total)) {
            if ($.isNumeric(efectivos)) {
                efectivo = parseFloat(efectivo);
                efectivos = parseFloat(efectivos);
                total = parseFloat(total);
                tipoCambio = parseFloat(tipoCambio);
                if (total <= ((efectivos * tipoCambio) + efectivo)) {
                    var cambio = (((efectivos * tipoCambio) + efectivo) - total);
                    $("#cambio").val(cambio.toFixed(2));
                }
            } else {
                efectivo = parseFloat(efectivo);
                total = parseFloat(total);
                if (total <= efectivo) {
                    var cambio = (efectivo - total);
                    $("#cambio").val(cambio.toFixed(2));
                }
            }
        } else {
            $("#cambio").val(" ");
        }

        return false;
    });

    $('#efectivo').on('keyup', function () { //obtiene el tipo de cambio con efectivo en $
        var efectivo = $('#efectivoBs').val();
        var total = $('#total').val();
        var efectivos = $('#efectivo').val();
        var tipoCambio = $('#tipoCambio').val();

        if ($.isNumeric(efectivos) && $.isNumeric(total)) {
            if ($.isNumeric(efectivo)) {
                efectivo = parseFloat(efectivo);
                efectivos = parseFloat(efectivos);
                total = parseFloat(total);
                tipoCambio = parseFloat(tipoCambio);
                if (total < ((efectivos * tipoCambio) + efectivo)) {
                    var cambio = (((efectivos * tipoCambio) + efectivo) - total);
                    $("#cambio").val(cambio.toFixed(2));
                }
            } else {
                efectivos = parseFloat(efectivos);
                total = parseFloat(total);
                if (total < (efectivos * tipoCambio)) {
                    var cambio = ((efectivos * tipoCambio) - total);
                    $("#cambio").val(cambio.toFixed(2));
                }
            }
        } else {
            if ($.isNumeric(efectivo) && $.isNumeric(total)) {
                $("#cambio").val(" ");
            } else {
                $("#cambio").val(" ");
                $('#efectivoBs').val(" ");
            }
        }
        return false;
    });
});

function doc_promocion()//para verificar si existe promocion vigente
{
    $.ajax({
        url: 'verificarPromocion.php',
        success: function (valores) {
            var datos = eval(valores);
            if (datos[0] == 'true') {
                $('#descuentoP').val(datos[1]);
                $('#descuentoB').val("0.00");
                $('#descuentoB').attr("readonly", "readonly");
                $('#descuentoP').attr("readonly", "readonly");
            }
        }
    });
    return false;
}
;

function veri(d) {
    alert(d);
}

function nuevoCliente() { //REGISTRA NUEVO CLIENTE 
    var frm = $("#formularioE").serialize();
    $.ajax({
        url: 'registrar_cliente.php',
        data: frm,
        type: 'post',
        success: function (registro) {
            if (registro == 'Vacio') {
                alert('Cliento no registrado');
            } else {
                var datos = eval(registro);
                $('#nitci').val(datos[0]);
                $('#nombreCliente').val(datos[1]);
                return false;
            }

        }

    });
    return false;
}


var f = 0;
function mostrarDetalle(id) {
    //  f=f+1;
    $('#notaVenta')[0].reset();
    var url = '../datos/venta_detalle.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function (valores) {
            var datos = eval(valores);
            $('#descripcionAgr').val(datos[0]);
            $('#unidadAgr').val(datos[1]);
            $('#precioAgr').val(datos[2]);
            $('#ubicacionAgr').val(datos[3]);
            $('#cantidadAgr').focus();

            return false;
        }
    });
    return false;
}


var subT = 0;

function tieneStock() {//verifica si hay stock suficiente en la base de datos
    var frm = $("#notaVenta").serialize();
    var cantidad = $('#cantidadAgr').val();
    $.ajax({
        url: 'venta_verificarStock.php',
        data: frm,
        type: 'post',
        success: function (stock) {
            stock = parseFloat(stock);
            cantidad = parseFloat(cantidad);
            if (stock < cantidad) {
                sweetAlert("Stock No Suficiente Para Completar La Venta", "", "warning");

                //alert("Stock No Suficiente Para Completar La Venta");
                $('#cantidadAgr').val("");
                $('#ubicacionAgr').val("");
                $('#precioAgr').val("");
                $('#unidadAgr').val("");
                $('#descripcionAgr').val("");
                $('#descripcionAgr').focus();
            } else {
                addRow1();
            }

            return false;
        }
    });
    return false;
}

function addRow1() { //agrega filas a la tabla de nota de venta y suma los precios de cada producto  
    f = f + 1;
    $('#cont2').val(f);//recibe el  numero de fila de la descripcion del producto
    $('#cont').val(f);
    var frm = $("#notaVenta").serialize();

    $.ajax({
        url: 'venta_agregarNota.php',
        data: frm,
        type: 'post',
        success: function (registro) {
            var datos = eval(registro);
            $('#descripcionAgr').val("");
            $('#unidadAgr').val("");
            $('#precioAgr').val("");
            $('#cantidadAgr').val("");
            $('#totalProducto').val("");
            $('#ubicacionAgr').val("");
            //$('#cont').val("");

            $('#grilla tbody').append(datos[0]); //dataTable > tbody:first
            subT = parseFloat(subT) + parseFloat(datos[2]);
            //$('#subTotal').val(subT.toFixed(2));
            var descuentoT = subT * parseFloat(datos[3]);
            $('#descuentoP').val(datos[4]);
            var descuenBs = (subT * parseFloat(datos[4])) / 100;
            $('#descuentoB').val(descuenBs.toFixed(2));
            $('#total').val(descuentoT.toFixed(2));
            $('#descripcionAgr').focus();
            if (datos[5] == '1') {
                $('#descuentoP').attr('readonly', true);
                $('#descuentoB').attr('readonly', true);
            }
            fn_dar_eliminar(parseFloat(datos[2]));
            return false;
        }
    });
    return false;
}

//function fn_dar_eliminar(dato) { //Elimina las filas de la tabla de nota de venta y resta el subtotal
//
//    $("a.elimina").click(function() {
//        id = $(this).parents("tr").find("td").eq(0).html();
//
//        if (confirm("Desea eliminar el producto Nro:" + id)) {
//
//            $(this).parents("tr").fadeOut("normal", function() {
//
//                $(this).remove();
//
//                subT = parseFloat($('#total').val()) - dato;
//                $('#total').val(subT.toFixed(2));
//                f=f-1;
//                $('#cont').val(f);//recibe el  numero de fila de la descripcion del producto
//            });
//        }
//    });
//}
//;

function fn_dar_eliminar(dato) { //Elimina las filas de la tabla de nota de venta y resta el subtotal

    $("a.elimina").click(function () {
//          id = $(this).parents("tr").find("td").eq(0).html();

//          if (confirm("Desea eliminar el producto Nro:" + id)){

        $(this).parents("tr").fadeOut("normal", function () {

            $(this).remove();

            subT = parseFloat($('#total').val()) - dato;
            //$('#subTotal').val(parseFloat($('#subTotal').val()) - dato);
            $('#total').val((subT.toFixed(2)));
            f = f - 1;
            $('#cont').val(f);//recibe el  numero de fila de la descripcion del producto
        });
//          }
    });
}
;

function validarNumeroVentas(event) {//Para validar numeros de cantidad de producto
    if (event.keyCode == 13) {
        return true;
    }
    if (event.keyCode == 46) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }

}
;
function validarNumeroVentas2(event) {//Para validar numeros de descuento en bs y %
    if (event.keyCode == 46) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }
}
;
function registrarDetalle() {
    var frm = $("#notaVenta").serialize();
    swal({
        title: "IMPRIMIENDO...",
        text: "Generando su vista de impresion",
        timer: 3000,
        showConfirmButton: false
    });
    $("#notaDeVenta").prop("disabled", true);
    setTimeout(function () {
            $("#notaDeVenta").prop("disabled", false)
        },
        4000);
    setInterval(function () {
        $.ajax({
            url: 'registrar_detalle.php',
            data: frm,
            type: 'post',
            success: function (valores) {
                var datos = eval(valores);
                var bn = datos[1];
                var ord = datos[3]; //orden con o snn destino
                var dest = datos[4]; // destino
                if (bn == "false") {
                    $('#efectivoBs').focus();
                    // alert(datos[0]);
                } else {
                    document.location.href = 'venta_notaVenta.php?vt=' + datos[0] + '&ord=' + ord + '&dest=' + dest;
                }

            }

        });
        return false;
    }, 3000);
}

function RegistrarFactura() {
    var frm = $("#notaVenta").serialize();
    swal({
        title: "IMPRIMIENDO...",
        text: "Generando su vista de impresion",
        timer: 3000,
        showConfirmButton: false
    });
    $("#facturar").prop("disabled", true);
    setTimeout(function () {
            $("#facturar").prop("disabled", false)
        },
        4000);
    setInterval(function () {
        $.ajax({
            url: 'registrar_detalle.php',
            data: frm,
            type: 'post',
            success: function (valores) {
                // alert(valores);
                var datos = eval(valores);
                var bn = datos[1];
                var ti = datos[2]; // ti = tipoImpresion
                var ord = datos[3]; //orden con o snn destino
                var dest = datos[4]; // destino
                if (bn == "false") {
                    $('#efectivoBs').focus();
                } else {
                    document.location.href = 'factura/imprime_facturaCartaTipo1.php?vt=' + datos[0] + '&ord=' + ord + '&dest=' + dest;
                }
            }

        });
        return false;
    }, 3000);

}
;

var bn = 0;
var modiSub = 0;
function modificaDetalle(contador) {
    if ($.isNumeric($("#cantida" + contador).val())) {
        // $("#cantida" + contador).removeAttr("readonly");
        //modiSub = $("#monto" + contador).val();
        $("#cantida" + contador).val("");
        // alert(modiSub);
    }

}

function modificarDetalle(contador) {

    // var vc = "cantida" + contador;
    var cant = $("#cantida" + contador).val();
//     alert(cant);
    if ($.isNumeric(cant)) {
        // alert(cant);
        var precio = $('#precio' + contador).val();
        var monto = parseFloat(cant) * parseFloat(precio);
        modiSub = $("#monto" + contador).val();
        $('#monto' + contador).val(monto);
        var monto1 = parseFloat(modiSub);
        var st = $('#total').val();
        st = parseFloat(st) - monto1;
        var subTotal = st + monto;
        //$('#subTotal').val(subTotal.toFixed(2));
        $('#total').val(subTotal.toFixed(2));
        subT = subTotal;
    }
};
///Funciones para ventas al credito

//onclik en la venta al credito
function registrarDetalleCredito() {
    var efectivoMonto = parseFloat($('#efectivoBs').val());
    var totalMonto = parseFloat($('#total').val());
    if ((efectivoMonto >= 0) && (totalMonto > efectivoMonto)) {
        var pregunta = confirm('¿Esta Seguro De Realizar Venta Al Credito?');
        if (pregunta == true) {
            var frm = $("#notaVenta").serialize();
            var monto = $('#total').val();
            monto = parseFloat(monto);

            var montoBs = $('#efectivoBs').val();
            //alert(frm + 'HOLA=' + monto + 'CHAUU=' + montoBs);
            $.ajax({
                url: 'registrar_detallecredito.php',
                data: frm,
                type: 'post',
                success: function (valores) {
                    var datos = eval(valores);
                    // alert(datos);
                    var venta = datos[0];
                    // alert(venta);
                    if (monto > 0) {
                        document.location.href = 'nuevoplan.php?nomto=' + monto + '&montoBs=' + montoBs + '&idVenta=' + datos;
                    }
                }
            });
        }
    } else {
        if (efectivoMonto >= totalMonto) {
            swal("El monto Ingreso Es Basto Para Realizar Venta Al Contado", "Por Favor Verifique", "success");
            // alert("El monto Ingreso Es Basto Para Realizar Venta Al Contado, Por Favor Verifique");
            $('#efectivoBs').val("");
            $('#cambio').val("");
            $('#efectivoBs').focus();
        } else {
        }
    }
    ;
    return false;
};

////
function fn_registrarVC() {
    var plazo = parseFloat($('#cuotaPlazo').val());
    var interes = parseFloat($('#cuotaInteres').val());
    var numeroCuotas = parseFloat($('#cuotaNroCuotas').val());
    var frm = $("#ventaCreditoNota").serialize();
    ///validamos 
    if (plazo > 0) {
        alert(plazo);
        if (parseFloat(interes) >= 0) {
            if (numeroCuotas >= 1 && plazo >= numeroCuotas) {
                var frm = $("#ventaCreditoNota").serialize();
                $.ajax({
                    url: 'registraDatosVC.php',
                    data: frm,
                    type: 'POST',
                    success: function (valores) {
                        var datos = eval(valores);
                        document.location.href = 'registrar_ventacredito.php?idVentaa=' + datos[0];
                    }
                });
                return false;
            } else {
                sweetAlert("Numero De Cuotas Invalido", "Debe Ser Menor Que Los Dias De Plazo", "warning");
                //alert("Numero De Cuotas Invalido, Debe Ser Menor Que Los Dias De Plazo");
                $('#cuotaNroCuotas').focus();
                return;
            }
        } else {
            $('#cuotaInteres').focus();
            return;
        }

    } else {
        alert(plazo);
        $('#cuotaPlazo').focus();
        return;
    }
    return false;
}
;


//METODO PARA REGISTRAR LA VENTAFACTURA CREDITO
function fn_registrarFC() {
    var frm = $("#ventaCreditoNota").serialize();
    alert(frm);
    $.ajax({
        url: 'registraDatosVC.php',
        data: frm,
        type: 'POST',
        success: function (valores) {
            var datos = eval(valores);
            alert(valores);
            document.location.href = 'registrar_detallecreditoFactura.php?idVentaa=' + datos[0];
        }
    });
    return false;
}
;

///obtiene nueva fecha suma
function fechaSuma(dias, fecha) {
    fecha = new Date();
    day = fecha.getDate();
    month = fecha.getMonth();
    year = fecha.getYear();

    tiempo = fecha.getTime();
    miliSegundo = parseInt(dias * 24 * 60 * 60 * 1000);
    total = fecha.setTime(tiempo + miliSegundo);
    day = ("0" + fecha.getDate()).slice(-2);
    month = ("0" + (fecha.getMonth() + 1)).slice(-2);
    year = fecha.getFullYear();
    return nvofecha = year + "-" + (month) + "-" + (day);
}
;
function fechaSumaReves(dias, fecha) {
    fecha = new Date();
    day = fecha.getDate();
    month = fecha.getMonth();
    year = fecha.getYear();

    tiempo = fecha.getTime();
    miliSegundo = parseInt(dias * 24 * 60 * 60 * 1000);
    total = fecha.setTime(tiempo + miliSegundo);
    day = ("0" + fecha.getDate()).slice(-2);
    month = ("0" + (fecha.getMonth() + 1)).slice(-2);
    year = fecha.getFullYear();
    return nvofecha = day + "-" + (month) + "-" + (year);
}
;

function diferenciaFechas(f1, f2) {
    var aFecha1 = f1.split('/');
    var aFecha2 = f2.split('/');
    var fFecha1 = Date.UTC(aFecha1[2], aFecha1[1] - 1, aFecha1[0]);
    var fFecha2 = Date.UTC(aFecha2[2], aFecha2[1] - 1, aFecha2[0]);
    var dif = fFecha2 - fFecha1;
    var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
    return dias;
}
;

function cuotas() {
    var total = $('#cuotaMontoTotal').val();
    total = parseFloat(total);
    var cuotaInicial = $('#cuotaMontoInicial').val();
    cuotaInicial = parseFloat(cuotaInicial);
    var numeroDeCuotas = $('#cuotaNroCuotas').val();
    numeroDeCuotas = parseFloat(numeroDeCuotas);
    var interes = $('#cuotaInteres').val();
    interes = parseFloat(interes);
    interes = interes / 100;
    return montoPorCuota = ((total - cuotaInicial) + ((total - cuotaInicial) * interes)) / numeroDeCuotas;

}
;
