﻿<?php

    // Título de páginas
    const TITULO = 'Don Fileto';

    // Propiedades constantes de base de datos
    const DB_DATABASE = 'carnesfilete';
    const DB_HOSTNAME = 'localhost';
    const DB_USERNAME = 'root';
    const DB_PASSWORD = '';
