<?php


    //require_once '../config.php'; //Innecesario, ya que ya fué requerido en el header.php
    require_once 'NotORM.php';

    // Constructor para NotORM
    $dsn = 'mysql:dbname='.DB_DATABASE.';host='.DB_HOSTNAME;
    $pdo_connection = new PDO($dsn, DB_USERNAME, DB_PASSWORD);
    $db = new NotORM($pdo_connection);