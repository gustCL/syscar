<?php

require_once '../extras/config.php';

require_once '../datos/Database.php';



session_start();



if (!isset($_SESSION['logueado']) || ($_SESSION['logueado']!= 'SI')) {

    header('Location: ../login/');

    exit();

}



if (!usuario_tiene_sesion_activa($_SESSION['userMaster']['idUsuario'])) {

    session_unset();

    session_destroy();

    header('Location: ../inicio/');

    exit();

}



// Funciones

function usuario_tiene_sesion_activa($id_usuario) {

    $query = "SELECT activo FROM Usuario WHERE idUsuario = :id_usuario";

    $cmd = Database::getInstance()->getDb()->prepare($query);

    $cmd->bindParam(':id_usuario', $id_usuario);

    $cmd->execute();

    $result = $cmd->fetch();

    $activo = $result['activo'];

    if ($activo == 1) {

        return true;

    } else {

        return false;

    }

}



function get_fecha_limite($id_terminal) {

    $query = "SELECT idDosifi, fechaLimite

                  FROM   Dosificacion

                  WHERE  idTerminal = :id_terminal AND

                         estado = 1

                  ORDER BY idDosifi DESC LIMIT 1";

    $cmd = Database::getInstance()->getDb()->prepare($query);

    $cmd->bindParam(':id_terminal', $id_terminal);

    $cmd->execute();

    $result = $cmd->fetch();

    return $result['fechaLimite'];

}



function tiene_dosificacion_registrada_activa($id_terminal) {

    $query = "SELECT * FROM Dosificacion WHERE idTerminal = :id_terminal AND estado = 1";

    $cmd = Database::getInstance()->getDb()->prepare($query);

    $cmd->bindParam(':id_terminal', $id_terminal);

    $cmd->execute();

    $row_count = $cmd->rowCount();

    if ($row_count > 0) {

        return true;

    } else {

        return false;

    }

}



?>

<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8" />

    <title><?= TITULO; ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!--    <link rel="shortcut icon" href="../assets/img/icono.ico" type="image/x-icon" media="all">-->



    <!--Basic Styles-->

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet"   media="all"/>

    <link href="../assets/css/font-awesome.min.css" rel="stylesheet"   media="all"/>

    <link href="../assets/css/weather-icons.min.css" rel="stylesheet"   media="all"/>



    <!--Fonts-->

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">



    <!--Beyond styles-->

    <link href="../assets/css/beyond.min.css" rel="stylesheet"   media="all" />

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->

    <script src="../assets/js/skins.min.js"></script>

</head>

<?php

require_once '../header_footer/header.php';

?>

<div class="page-content">

    <!-- subtTitulo -->

    <div class="page-header position-relative">

        <div class="header-title">

            <h1><strong>PAGINA PRINCIPAL</strong></h1>

        </div>

        <!--Header Buttons-->

        <div class="header-buttons">

            <a class="sidebar-toggler" href="#">

                <i class="fa fa-arrows-h"></i>

            </a>

            <a class="refresh" id="refresh-toggler" href="#">

                <i class="glyphicon glyphicon-refresh"></i>

            </a>

            <a class="fullscreen" id="fullscreen-toggler" href="#">

                <i class="glyphicon glyphicon-fullscreen"></i>

            </a>

        </div>

        <!--Header Buttons End-->

    </div>

    <div class="page-body">

        <div class="row">

            <?php



            $id_sucursal = $_SESSION['userMaster']['idSucursal'];

            // Obtener la dosificaciÃ³n correspondiente a esta sucursal

            $query = "SELECT d.fechaLimite

                      FROM Dosificacion d, Sucursal s

                      WHERE d.idSucursal = s.idSucursal and s.estado = 1 and d.estado = 1 and s.idSucursal = :id_sucursal";

            $cmd = Database::getInstance()->getDb()->prepare($query);

            $cmd->bindParam(':id_sucursal', $id_sucursal);

            $cmd->execute();

            date_default_timezone_set('America/La_Paz');

            $fecha_actual = date('d-m-Y');

            while ($result = $cmd->fetch()) :

            $fecha_limite = $result['fechaLimite'];

            $diferencia_fecha = strtotime($fecha_limite) - strtotime($fecha_actual);

            $diferencia_dias = intval($diferencia_fecha / 60 / 60 / 24);

            if ($diferencia_dias >= 10) : ?>

            <div class="col-md-6">

                <div class="panel panel-primary">

                    <div class="panel-heading">DOSIFICACION</div>

                    <div class="panel-body"><label style="font-size: 25px;">Quedan <?= $diferencia_dias; ?>

                            dias de su dosificacion vigente.</label></div>

                    <div class="panel-footer"><a href="../dosificacion/">Registrar nueva dosificacion</a></div>

                </div>

                <?php else: ?>

                    <div class="panel panel-danger">

                        <div class="panel-heading">DOSIFICACION</div>

                        <div class="panel-body">

                            <?php if ($diferencia_dias <= 0): ?>

                                <?php if ($diferencia_dias == 0): ?>

                                    <label style="font-size: 25px;">La dosificacion vigente en este punto vence

                                        hoy.</label>

                                <?php else: ?>

                                    <label style="font-size: 25px;">La dosificacion vigente en este punto ha

                                        caducado.</label>

                                <?php endif; ?>

                            <?php else: ?>

                                <label style="font-size: 25px;">Quedan <?= $diferencia_dias; ?> dias de su dosificacion

                                    vigente.</label>

                            <?php endif; ?>

                        </div>

                        <div class="panel-footer">Registrar Nueva</div>

                    </div>

                <?php endif; ?>

                <?php   endwhile; ?>

            </div>

        </div>

    </div>

</div>

<?php

require_once '../header_footer/footer.php';

?>