<?php
require_once 'head.php';

//$productos = $db->Producto('estado = ?','1');/
// Obtener el stock de los productos activos
?>
<div class="page-content">
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>STOCK</strong></h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="widget-body">
                <div class="row">
                    <div class="col-md-offset-5 col-md-4">
                        <h3><strong>STOCK DE INVENTARIO</strong></h3>
                    </div>
                    <div class="col-md-offset-4 col-md-4" >
                         <span class="input-icon">
                             <input type="text" size="40" class="form-control" placeholder="Buscar Producto por : Codigo , Descripcion " id="bus-producto"/>
                             <i class="glyphicon glyphicon-search circular blue"></i>
                         </span>
                    </div>
                    <br>
                </div>
                <br>
                <form action="javascript: registrarStock();" class="form-horizontal" id="frm-registrar-stock">
                    <div class="table-responsive" id="div-producto">
                        <!-- atributo de table data-sortable-->
                        <!--atributo de class table-bordered table-hover sortable-theme-bootstrap-->
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
require_once '../header_footer/footer.php';
?>

