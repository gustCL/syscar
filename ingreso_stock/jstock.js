$(document).ready(function() {
    obtenerProductos();
    $("#bus-producto").on('keyup',function() {
        var incidencia= $("#bus-producto").val();
        $('#div-producto').load('obtener_productos.php?val='+incidencia);
    })
    /*obtenerMesas();

    obtenerCategorias();*/
});

$(function() {
    $("#codigo").keyup(function(){
        if ($(this).val().length > 0) {
            $('#disponible').show();
            existeCodigoProducto($(this).val());
        } else {
            $('#disponible').hide();
        }
    });
});

function obtenerProductos() {
    $('#div-producto').load('obtener_productos.php');
}

// Busca productos en base a parámetros
function buscarProducto(val) {
    val = val.replace(/ /g, "%20");
    $('#div-producto').load('obtener_productos.php?val='+val);
}

function registrarStock() {
    var frm = $("#frm-registrar-stock").serialize();
    $.ajax({
        url: 'registrar_stock.php',
        data: frm,
        type: 'POST',
        success: function () {
            alertify.success("Stock registrado");
            $('#btn-cancelar').text('Volver');
        }
    });
}

function registrarProducto() {
    var frm = $("#frm-registrar-producto").serialize();
    $.ajax({
        url: 'registrar_producto.php',
        data: frm,
        type: 'post',
        success: function () {
            alertify.success("Producto registrado");
        }
    });
}

function existeCodigoProducto(codigo) {
    $.ajax({
        url: 'existe_codigo_producto.php',
        type: 'POST',
        data: {codigo: codigo},
        success: function (result) {
            if (result == 'true') {
                $('#btn-registrar-producto').prop('disabled', true);
                $('#disponible').css({
                    'font-weight': 'bold',
                    'color': 'red'
                }).text('En uso');
            } else {
                $('#btn-registrar-producto').prop('disabled', false);
                $('#disponible').css({
                    'font-weight': 'bold',
                    'color': 'cornflowerblue'
                }).text('Disponible');
            }
        }
    });
}

function eliminarProducto(id, nombre) {
    swal({
            title: "Eliminar Producto",
            text: "Está a punto de eliminar "+nombre+". ¿Desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: 'eliminar_producto.php',
                    data: {id: id},
                    success: function () {
                        swal("Producto eliminado!", "El producto ha sido removido de la base de datos", "success");
                        obtenerProductos();
                    }
                });
            }
        }
    );
}

function modificarProducto(codigo) {
    $.ajax({
        type:'POST',
        url: 'obtener_producto.php',
        data: {codigo: codigo},
        success: function(valores){
            var datos = eval(valores);
            $('#codigo-producto').val(datos[0]);
            $('#codigo').val(datos[1]);
            $('#id-linea').val(datos[2]).change();
            $('#nombre-comercial').val(datos[3]);
            $('#precio-venta').val(datos[4]);
            $('#stock').val(datos[5]);
            $('#div-modal-producto').modal({
                show:true,
                backdrop:'static'
            });
        }
    });
}

function actualizarProducto() {
    var frm = $("#frm-actualizar-producto").serialize();
    $.ajax({
        url: 'actualizar_producto.php',
        data: frm,
        type: 'post',
        success: function () {
            $('#btn-hide').click();
            alertify.success("Producto modificado");
            obtenerProductos();
        }
    });
}

function obtenerMesas() {
    $('#div-mesas').load('obtener_mesas.php');
}


function actualizarMesa() {
    var frm = $("#frm-actualizar-mesa").serialize();
    $.ajax({
        url: 'actualizar_mesa.php',
        data: frm,
        type: 'post',
        success: function () {
            $('#btn-hide').click();
            alertify.success("Mesa modificada");
            obtenerMesas();
        }
    });
}


function obtenerCategorias() {
    $('#div-categorias').load('obtener_categorias.php');
}

function agregarStock(id){
    if($("#stock"+id).val()=='' || $("#stock"+id).val()==0){
        $("#stock"+id).focus();
        sweetAlert("DATOS INCORRECTOS", "Señor Usuario Verifique Sus Datos", "error");
        $("#stock"+id).focus();
        return;
    }else{
        var stock= $("#stock"+id).val();
        $.ajax({
            url: 'addStok.php',
            data: 'id='+id+'&stock='+stock,
            type: 'POST',
            success: function (valores) {
                obtenerProductos();
                swal("DATOS CORRECTOS!", "Stock registrado correctamente!", "success");
            }
        });
    }
}


function fn_paginar(var_div, url){
    var div = $("#" + var_div);
    $(div).load(url);
}

function onlyNumbers(e) {//Funcion que devuelve solo numeros y punto
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "";
    especiales = [46,48, 49, 50, 51, 52, 53, 54, 55, 56, 57]; //,37,39,46,8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57
    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}
