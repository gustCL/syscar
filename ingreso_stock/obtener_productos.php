<?php
require_once '../datos/conexion.php';
//require_once '../datos/Database.php';
include "../extras/PHPPaging.lib/PHPPaging.lib.php";

if (isset($_GET['val'])) {
    $indicio = $_GET['val'];
    $query = "SELECT p.codigoProducto, p.codigo, li.nombreLinea, p.nombreComercial, ps.precioVenta, SUM(e.stock) AS totalStock 
                  FROM Producto p, Linea li, PrecioSucursal ps, Ubicacion u, Existencias e, Lote lo 
                  WHERE p.idLinea = li.idLinea AND p.estado = 1 AND li.estado = 1 AND p.codigoProducto = lo.codigoProducto 
                  AND lo.idLote = e.idLote AND p.codigoProducto = ps.codigoProducto AND p.codigoProducto = u.codigoProducto 
                  AND e.idUbicacion = u.idUbicacion 
                  AND (p.nombreComercial LIKE '$indicio%') GROUP BY lo.codigoProducto ORDER BY p.nombreComercial ASC ";
} else {
    $query = "SELECT p.codigoProducto, p.codigo, li.nombreLinea, p.nombreComercial, ps.precioVenta, SUM(e.stock) AS totalStock 
                  FROM Producto p, Linea li, PrecioSucursal ps, Ubicacion u, Existencias e, Lote lo 
                  WHERE p.idLinea = li.idLinea AND p.estado = 1 AND li.estado = 1 AND p.codigoProducto = lo.codigoProducto 
                  AND lo.idLote = e.idLote AND p.codigoProducto = ps.codigoProducto AND p.codigoProducto = u.codigoProducto 
                  AND e.idUbicacion = u.idUbicacion GROUP BY lo.codigoProducto ORDER BY `p`.`nombreComercial` ASC";
}
$pagina = new PHPPaging;
$pagina->agregarConsulta($query);
$pagina->modo('desarrollo');
$pagina->verPost(true);
$pagina->porPagina(8);
$pagina->paginasAntes(4);
$pagina->paginasDespues(4);
$pagina->linkSeparador(" - "); //Significa que no habrá separacion
$pagina->div('div-producto');
$pagina->linkSeparadorEspecial('...');   // Separador especial
$pagina->ejecutar();
?>
<table class="table table-striped ">
    <thead>
    <tr>

        <th hidden>ID</th>
        <!-- <th>NRO.</th>-->
        <th width="10%">CÓDIGO</th>
        <th width="30%">NOMBRE COMERCIAL</th>
        <th width="15%">CATEGORÍA</th>
        <th width="15%">STOCK</th>
        <th width="30%">AGREGAR STOCK</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nro = 0;
    while ($result = $pagina->fetchResultado()) :  ?>
        <?php
        $codigoP = $result['codigoProducto'];
        ?>
        <tr>
            <td hidden>
                <input type="number" name="codproducto[]" value="<?= $result['codigoProducto']; ?>">
            </td>
            <!-- <td><? /*= $nro; */ ?></td>-->
            <td><?= $result['codigo']; ?></td>
            <td><?= $result['nombreComercial']; ?></td>
            <td><?= $result['nombreLinea']; ?></td>
            <td><?= $result['totalStock']; ?></td>
            <td>
                <div class="col-md-8">
                    <input type="text" id="stock<?= $codigoP ?>" name="stock<?= $codigoP ?>" class="form-control"
                           size="100%" min="0" value="" onkeypress="return onlyNumbers(event);">
                </div>
                <div>
                    <button type="button" class="btn btn-success btn-sm shiny"
                            onclick="agregarStock('<?= $codigoP ?>')">Agregar Stock
                    </button>
                </div>
            </td>
        </tr>
    <?php endwhile; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="7">Numero de pagina: <?php echo $pagina->fetchNavegacion(); ?></td>
        </tr>
    </tfoot>
</table>