<?php
    require_once '../datos/Database.php';
    $id_cliente = $_POST['id'];

    $cliente = $db->Cliente('idCliente=?',$id_cliente)->fetch();
    $datos = array(
        0 => $cliente['nombreCliente'],
        1 => $cliente['nit'],
        2 => $cliente['ci'],
        3 => $cliente['telefonoFijo'],
        4 => $cliente['telefonoCel'],
        5 => $cliente['direccion'],
        6 => $cliente['email'],
        7 => $cliente['banco'],
        8 => $cliente['nroCuenta'],
        9 => $cliente['descuento'] * 100,
        10 => $cliente['interes'] * 100,
        11 => $cliente['idCliente']
    );
    echo json_encode($datos);
?>