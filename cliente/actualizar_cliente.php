<?php

    require_once '../datos/Database.php';

    $id_cliente = $_POST['id-cliente'];

    $nit = $_POST['editar-nit'];
    $nombre = $_POST['editar-nombre'];
    $ci = $_POST['editar-ci'];
    $telefono_fijo = $_POST['editar-telefono'];
    $telefono_celular = $_POST['editar-celular'];
    $direccion = $_POST['editar-direccion'];
    $email = $_POST['editar-email'];
    $banco = $_POST['editar-banco'];
    $nro_cuenta = $_POST['editar-nro-cuenta'];
    $descuento = $_POST['editar-descuento'];
    $interes = $_POST['editar-interes'];

    $descuento_porcentual = $descuento / 100;
    $interes_porcentual = $interes / 100;

    $cliente = $db->Cliente()->where('idCliente = ?', $id_cliente);
    $data = array(
        "nombreCliente" => $nombre,
        "nit" => $nit,
        "ci" => $ci,
        "telefonoFijo" => $telefono_fijo,
        "telefonoCel" => $telefono_celular,
        "direccion" => $direccion,
        "email" => $email,
        "banco" => $banco,
        "nroCuenta" => $nro_cuenta,
        "descuento" => $descuento_porcentual,
        "interes" => $interes_porcentual,
        "estado" => 1
    );
    $cliente->update($data);
    exit();

?>
