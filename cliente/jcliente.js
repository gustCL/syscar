$(document).ready(function() {
    obtenerClientes();
});

$(function(){
    // Buscar un cliente por nombre, nit o cédula de identidad
	$('#buscar-cliente').keyup(function(){
        var val = $(this).val();
        val = val.replace(/ /g, "%20");
        buscarClientes(val);
	});

    // Abre una ventana modal para registrar un nuevo cliente
	$('#btn-registrar-cliente').click(function(){
		$('modal-new-corte-mayor').modal({
			show:true,
			backdrop:'static'
		});
	});

});

/**
 * Función utilizada para realizar la paginación
 *
 * @param var_div
 * @param url
 */
function fn_paginar(var_div, url) {
    var div = $("#" + var_div);
    $(div).load(url);
}

// Obtiene la lista de todos los clientes activos
function obtenerClientes() {
    $("#div-clientes").load('obtener_clientes.php');
}

function buscarClientes(val) {
    $('#div-clientes').load('obtener_clientes.php?val='+val);
}

// Función para eliminar cliente
function eliminarCliente(id){
	swal({
        title: "Eliminar Cliente",
        text: "Está a punto de eliminar los registros de este cliente. ¿Desea continuar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, eliminar",
        cancelButtonText: "No, cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: 'eliminar_cliente.php',
                    data: {id: id},
                    success: function () {
                        obtenerClientes();
                        swal("Cliente Eliminado!", "El cliente ha sido removido de los registros", "success");
                    }
                });
            }
       }
   );
}

// Abre un formulario modal para ver la información del cliente
function verCliente(id){
    $.ajax({
        type: 'POST',
        url: 'obtener_cliente.php',
        data: {id: id},
        success: function (valores) {
            var datos = eval(valores);
            $('#ver-nombre').val(datos[0]);
            $('#ver-nit').val(datos[1]);
            $('#ver-ci').val(datos[2]);
            $('#ver-telefono').val(datos[3]);
            $('#ver-celular').val(datos[4]);
            $('#ver-direccion').val(datos[5]);
            $('#ver-email').val(datos[6]);
            $('#ver-banco').val(datos[7]);
            $('#ver-nro-cuenta').val(datos[8]);
            $('#ver-descuento').val(datos[9]);
            $('#ver-interes').val(datos[10]);
            $('#modal-ver').modal({
                show: true,
                backdrop: 'static'
            });
        }
    });
}

/**
 * Recupera la información de un cliente
 * en un formulario modal para poder editarla
 *
 * @param id
 * @returns {boolean}
 */
function editarCliente(id){
    $.ajax({
        type: 'POST',
        url: 'obtener_cliente.php',
        data: {id: id},
        success: function (valores) {
            var datos = eval(valores);
            $('#editar-nombre').val(datos[0]);
            $('#editar-nit').val(datos[1]);
            $('#editar-ci').val(datos[2]);
            $('#editar-telefono').val(datos[3]);
            $('#editar-celular').val(datos[4]);
            $('#editar-direccion').val(datos[5]);
            $('#editar-email').val(datos[6]);
            $('#editar-banco').val(datos[7]);
            $('#editar-nro-cuenta').val(datos[8]);
            $('#editar-descuento').val(datos[9]);
            $('#editar-interes').val(datos[10]);
            $('#id-cliente').val(datos[11]);
            $('#modal-editar').modal({
                show: true,
                backdrop: 'static'
            });
        }
    });
}

function actualizarCliente() {
	var frm = $("#frm-editar-cliente").serialize();
	$.ajax({
        url: 'actualizar_cliente.php',
        data: frm,
        type: 'post',
		success: function() {
            $('#btn-cerrar-modal-editar').click();
            obtenerClientes();
            swal("Edicion Finalizada", "Cliente Modificado Correctamente", "success");
        }
	});
}

// Registra un nuevo cliente en la base de datos
function registrarCliente() {
     var frm = $("#frm-registrar-cliente").serialize();
	 $.ajax({
		 url: 'registrar_cliente.php',
		 data: frm,
		 type: 'post',
		 success: function () {
			 $('#btn-cerrar-modal-registrar').click();
			 swal("Registro Correcto", "Cliente registrado correctamente", "success");
			 obtenerClientes();
		 }
	 });
}