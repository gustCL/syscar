<?php
    require_once '../datos/Database.php';

    $nit = $_POST['nit'];
    $nombre = $_POST['nombre'];
    $ci = $_POST['ci'];
    $telefono_fijo = $_POST['telefono'];
    $telefono_celular = $_POST['celular'];
    $direccion = $_POST['direccion'];
    $email = $_POST['email'];
    $banco = $_POST['banco'];
    $nro_cuenta = $_POST['nro-cuenta'];
    $descuento = $_POST['descuento'];
    $interes = $_POST['interes'];

    $descuento_porcentual = $descuento / 100;
    $interes_porcentual = $interes / 100;

    ini_set("date.timezone", "America/La_Paz");
    $fecha = date("d-m-Y");
    $hora = date("H:i:s");

    $cliente = $db->Cliente();
    $data = array(
        "nombreCliente" => $nombre,
        "nit" => $nit,
        "ci" => $ci,
        "telefonoFijo" => $telefono_fijo,
        "telefonoCel" => $telefono_celular,
        "direccion" => $direccion,
        "email" => $email,
        "banco" => $banco,
        "nroCuenta" => $nro_cuenta,
        "fechaRegistro" => $fecha,
        "horaRegistro" => $hora,
        "descuento" => $descuento_porcentual,
        "interes" => $interes_porcentual,
        "estado" => 1
    );
    $result = $cliente->insert($data);
    exit();
