<?php
    require_once '../datos/Database.php';
    require_once '../extras/PHPPaging.lib/PHPPaging.lib.php';


    if (isset($_GET['val'])) {
        $indicio = $_GET['val'];
        $query_clientes = "SELECT *
                           FROM Cliente
                           WHERE estado = 1 AND
                                 nit != 0 AND
                                 (nombreCliente LIKE '%$indicio%' OR nit LIKE '%$indicio%' OR ci LIKE '%$indicio%')
                           ORDER BY nombreCliente ASC";
    } else {
        $query_clientes = "SELECT *
                           FROM Cliente
                           WHERE estado = 1 AND
                                 nit != 0
                           ORDER BY nombreCliente ASC";
    }

    $cmd = Database::getInstance()->getDb()->prepare($query_clientes);
    $cmd->execute();

    $pagina = new PHPPaging;

    if ($cmd->rowCount() == 0) : ?>
        <div class="alert alert-info" role="alert">
            <center>
                <h3>No se encontraron resultados.</h3>
            </center>
        </div>
<?php else: ?>
        <table class="table sortable-theme-bootstrap table-hover table-bordered" data-sortable>
            <thead>
            <tr>
                <th>Nro.</th>
                <th>Nit</th>
                <th>Nombre</th>
                <th>CI</th>
                <th>Teléfono</th>
                <th>Celular</th>
                <th>Dirección</th>
                <th>Fecha de Registro</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $pagina->agregarConsulta($query_clientes);
            $pagina->modo('desarrollo');
            $pagina->verPost(true);
            $pagina->porPagina(20);
            $pagina->paginasAntes(3);
            $pagina->paginasDespues(3);
            $pagina->linkSeparador(' - ');
            $pagina->div('div-clientes');
            $pagina->linkSeparadorEspecial('...');
            $pagina->ejecutar();

            $nro = 0;
            while ($row = $pagina->fetchResultado()) :
            $id_cliente = $row['idCliente'];
            $nro++; ?>
            <tr>
                <td><?= $nro; ?></td>
                <td><?= $row['nit'] ?></td>
                <td><?= $row['nombreCliente']; ?></td>
                <td><?= $row['ci']; ?></td>
                <td><?= $row['telefonoFijo']; ?></td>
                <td><?= $row['telefonoCel']; ?></td>
                <td><?= $row['direccion']; ?></td>
                <td><?= $row['fechaRegistro']; ?></td>
                <td>
                    <a href="javascript: verCliente('<?= $id_cliente; ?>');" class="btn btn-default btn-xs blue"><i
                            class="fa fa-eye"></i> Ver</a>
                    <a href="javascript: editarCliente('<?= $id_cliente; ?>');" class="btn btn-default btn-xs purple"><i
                            class="fa fa-edit"></i> Editar</a>
                    <a onclick="eliminarCliente('<?= $id_cliente; ?>');" class="btn btn-default btn-xs black"><i
                            class="fa fa-trash-o"></i>Eliminar</a>
                </td>
            </tr>
            <?php endwhile; ?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="9">Número de página: <?= $pagina->fetchNavegacion(); ?></td>
            </tr>
            </tfoot>
        </table>

        <script src="../extras/sortable/js/sortable.js"></script>
<?php endif; ?>