<?php
    require_once 'head.php';
?>
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>AGENDA CLIENTE</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="widget-body">
                <center><h3><strong>LISTA DE CLIENTES</strong></h3></center>
                <br>
                <div class="form-inline">
                    <div class="form-group">
                        <div class="col-lg-3">
                            <span class="input-icon">
                            <input type="text" size="40" class="form-control"
                                   placeholder="Busca cliente por:  Nombre, Nit o CI"
                                   id="buscar-cliente">
                            <i class="glyphicon glyphicon-search circular blue"></i>
                        </span>
                        </div>
                    </div>
                    <button id="btn-registrar-cliente" class="btn btn-primary">Registrar Cliente</button>
                </div>
                <br>
                <div class="table-responsive" id="div-clientes">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para registrar nuevo cliente -->
<div class="modal fade" id="modal-registrar">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frm-registrar-cliente" class="form-horizontal" action="javascript: registrarCliente();">
                <!-- Header de la ventana -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><strong>REGISTRAR CLIENTE</strong></h4>
                </div>

                <!-- Contenido de la ventana -->
                <div class="modal-body">
                    <!-- Nit -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="nit">NIT:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="nit" name="nit">
                        </div>
                    </div>

                    <!-- Nombre -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="nombre">Nombre:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="nombre" name="nombre" required>
                        </div>
                    </div>

                    <!-- CI -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="ci">CI:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="ci" name="ci">
                        </div>
                    </div>

                    <!-- Teléfono -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="telefono">Teléfono:</label>
                        <div class="col-md-8">
                            <input type="number" min="0" class="form-control" id="telefono" name="telefono">
                        </div>
                    </div>

                    <!-- Celular -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="celular">Celular:</label>
                        <div class="col-md-8">
                            <input type="number" min="0" class="form-control" id="celular" name="celular">
                        </div>
                    </div>

                    <!-- Dirección -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="direccion">Dirección:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="direccion" name="direccion">
                        </div>
                    </div>

                    <!-- Email -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="email">Email:</label>
                        <div class="col-md-8">
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                    </div>

                    <!-- Banco -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="banco">Banco:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="banco" name="banco">
                        </div>
                    </div>

                    <!-- Nro de cuenta -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="nro-cuenta">Nro. Cuenta:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="nro-cuenta" name="nro-cuenta">
                        </div>
                    </div>

                    <!-- Descuento -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="descuento">Descuento(%):</label>
                        <div class="col-md-8">
                            <input type="number" min="0" max="100" class="form-control" id="descuento" name="descuento">
                        </div>
                    </div>

                    <!-- Interés -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="interes">Interés(%):</label>
                        <div class="col-md-8">
                            <input type="number" min="0" max="100" class="form-control" id="interes" name="interes">
                        </div>
                    </div>
                </div>

                <!-- Footer de la ventana -->
                <div class="modal-footer">
                    <div class="col-md-8">
                        <input type="submit" class="btn btn-primary" value="Registrar">
                        <button id="btn-cerrar-modal-registrar" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal  para ver clientes-->
<div class="modal fade" id="modal-ver">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal">
                    <!-- Header de la ventana -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><strong>INFORMACIÓN DE CLIENTE</strong></h4>
                    </div>

                    <!-- Contenido de la ventana -->
                    <div class="modal-body">
                        <!-- Nit -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="nit">NIT:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="ver-nit" name="nit" readonly>
                            </div>
                        </div>

                        <!-- Nombre -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="nombre">Nombre:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="ver-nombre" name="nombre" readonly>
                            </div>
                        </div>

                        <!-- CI -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="ci">CI:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="ver-ci" name="ci" readonly>
                            </div>
                        </div>

                        <!-- Teléfono -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="telefono">Teléfono:</label>
                            <div class="col-md-8">
                                <input type="number" min="0" class="form-control" id="ver-telefono" name="telefono" readonly>
                            </div>
                        </div>

                        <!-- Celular -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="celular">Celular:</label>
                            <div class="col-md-8">
                                <input type="number" min="0" class="form-control" id="ver-celular" name="celular" readonly>
                            </div>
                        </div>

                        <!-- Dirección -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="direccion">Dirección:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="ver-direccion" name="direccion" readonly>
                            </div>
                        </div>

                        <!-- Email -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="email">Email:</label>
                            <div class="col-md-8">
                                <input type="email" class="form-control" id="ver-email" name="email" readonly>
                            </div>
                        </div>

                        <!-- Banco -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="banco">Banco:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="ver-banco" name="banco" readonly>
                            </div>
                        </div>

                        <!-- Nro de cuenta -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="nro-cuenta">Nro. Cuenta:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="ver-nro-cuenta" name="nro-cuenta" readonly>
                            </div>
                        </div>

                        <!-- Descuento -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="descuento">Descuento(%):</label>
                            <div class="col-md-8">
                                <input type="number" min="0" class="form-control" id="ver-descuento" name="descuento" readonly>
                            </div>
                        </div>

                        <!-- Interés -->
                        <div class="form-group">
                            <label class="control-label lead col-md-3" for="interes">Interés(%):</label>
                            <div class="col-md-8">
                                <input type="number" min="0" class="form-control" id="ver-interes" name="interes" readonly>
                            </div>
                        </div>
                    </div>

                    <!-- Footer de la ventana -->
                    <div class="modal-footer">
                        <button id="btn-cerrar-modal-registrar" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!-- Modal para editar cliente -->
<div class="modal fade" id="modal-editar">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frm-editar-cliente" class="form-horizontal" action="javascript: actualizarCliente();">
                <!-- Header de la ventana -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><strong>EDITAR CLIENTE</strong></h4>
                </div>

                <!-- Contenido de la ventana -->
                <div class="modal-body">
                    <!-- Id cliente -->
                    <input type="number" id="id-cliente" name="id-cliente" value="" hidden>

                    <!-- Nit -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="nit">NIT:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="editar-nit" name="editar-nit">
                        </div>
                    </div>

                    <!-- Nombre -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="nombre">Nombre:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="editar-nombre" name="editar-nombre" required>
                        </div>
                    </div>

                    <!-- CI -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="ci">CI:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="editar-ci" name="editar-ci">
                        </div>
                    </div>

                    <!-- Teléfono -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="telefono">Teléfono:</label>
                        <div class="col-md-8">
                            <input type="number" min="0" class="form-control" id="editar-telefono" name="editar-telefono">
                        </div>
                    </div>

                    <!-- Celular -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="celular">Celular:</label>
                        <div class="col-md-8">
                            <input type="number" min="0" class="form-control" id="editar-celular" name="editar-celular">
                        </div>
                    </div>

                    <!-- Dirección -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="direccion">Dirección:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="editar-direccion" name="editar-direccion">
                        </div>
                    </div>

                    <!-- Email -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="email">Email:</label>
                        <div class="col-md-8">
                            <input type="email" class="form-control" id="editar-email" name="editar-email">
                        </div>
                    </div>

                    <!-- Banco -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="banco">Banco:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="editar-banco" name="editar-banco">
                        </div>
                    </div>

                    <!-- Nro de cuenta -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="nro-cuenta">Nro. Cuenta:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="editar-nro-cuenta" name="editar-nro-cuenta">
                        </div>
                    </div>

                    <!-- Descuento -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="descuento">Descuento(%):</label>
                        <div class="col-md-8">
                            <input type="number" min="0" max="100" class="form-control" id="editar-descuento" name="editar-descuento">
                        </div>
                    </div>

                    <!-- Interés -->
                    <div class="form-group">
                        <label class="control-label lead col-md-3" for="interes">Interés(%):</label>
                        <div class="col-md-8">
                            <input type="number" min="0" max="100" class="form-control" id="editar-interes" name="editar-interes">
                        </div>
                    </div>
                </div>

                <!-- Footer de la ventana -->
                <div class="modal-footer">
                    <div class="col-md-9">
                        <input type="submit" class="btn btn-primary" value="Guardar Cambios">
                        <button id="btn-cerrar-modal-editar" type="button" class="btn btn-danger" data-dismiss="modal">
                            Cerrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    require_once '../header_footer/footer.php';
?>