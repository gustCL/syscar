<?php
    require_once 'head.php';

    /*if (!isset($_GET['id_cargo']) || !isset($_GET['id_usuario']) || !isset($_GET['responsabilidad'])) {
        header("Location: ../inicio/");
        exit();
    }*/

    $tipo_resp = $db->TipoResponsableCargo();
    $usuarios = $db->Usuario('estado = ?', '1');
?>
<div class="page-content">
    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>MODIFICAR RESPONSABLE</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>

    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="widget-body ">
                <div class="container">
                    <h2><center><strong>REGISTRAR NUEVO</strong></center></h2>
                    <form class="form-horizontal" id="frm-registrar" name="form-registrar" action="javascript: registrarResponsable();">
                        <div class="form-group">
                            <label class="control-label lead col-md-4" for="cargo">Cargo:</label>
                            <div class="col-md-4">
                                <select class="form-control" id="cargo" name="cargo">
                                    <option value="0" selected>Cargo</option>
                                    <?php foreach ($tipo_resp as $tp) : ?>
                                        <option value="<?= $tp['idTipoResponsable']; ?>"><?= $tp['nombre']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lead col-md-4" for="usuario">Usuario:</label>
                            <div class="col-md-4">
                                <select class="form-control" id="usuario" name="usuario">
                                    <option value="0" selected>Usuario</option>
                                    <?php foreach ($usuarios as $usuario) : ?>
                                        <option value="<?= $usuario['idUsuario']; ?>"><?= $usuario['nombre']." ".$usuario['apellido']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lead col-md-4" for="responsabilidad">Responsabilidad:</label>
                            <div class="col-md-4">
                                <select class="form-control" id="responsabilidad" name="responsabilidad">
                                    <option value="0">Responsabilidad</option>
                                    <option value="Ingreso">Ingreso</option>
                                    <option value="Egreso">Egreso</option>
                                    <option value="Compra">Compra</option>
                                    <option value="Venta">Venta</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-5">
                                <input class="btn btn-primary" type="submit" value="Registrar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>


            <div class="widget-body">
                <form action="">
                    <label for="usuario">Usuario:</label><input type="text" id="usuario" required>
                    <label for="usuario">Cargo:</label><input type="text" id="cargo" required>
                    <input type="submit" value="Guardar">
                </form>
                <div class="container">
                    <br>
                    <a href="#ventana1" class="btn btn-primary btn-lg" data-toggle="modal">Boton</a>

                    <div class="modal fade" id="ventana1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Encabezado</h4>
                                </div>
                                <form action="">
                                <div class="modal-body">
                                        <label for="usuario">Usuario:</label><input type="text" id="usuario" required>
                                        <label for="usuario">Cargo:</label><input type="text" id="cargo" required>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                    <!--button type="button" class="btn btn-primary">Guardar Cambios</button-->
                                    <input class="btn btn-primary" type="submit" value="Guardar">
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<?php require_once '../header_footer/footer.php'; ?>

