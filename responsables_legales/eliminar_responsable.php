<?php

    require_once '../datos/Database.php';

    $id_cargo = $_POST['id_cargo'];
    $id_usuario = $_POST['id_usuario'];
    $responsabilidad = $_POST['responsabilidad'];

    $query = "DELETE
              FROM ResponsableUsuario
              WHERE idTipoResponsable = :id_cargo AND
                    idUsuario = :id_usuario AND
                    tipoResponsabilidad = :resp";
    $cmd = Database::getInstance()->getDb()->prepare($query);
    $cmd->bindParam(':id_cargo', $id_cargo);
    $cmd->bindParam(':id_usuario', $id_usuario);
    $cmd->bindParam(':resp', $responsabilidad);
    $cmd->execute();
    exit();

