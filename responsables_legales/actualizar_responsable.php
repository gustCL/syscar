<?php

    require_once '../datos/Database.php';

    $id_row = $_POST['modal-id'];

    $cargo = $_POST['modal-cargo'];
    $resp = $_POST['modal-resp'];

    $query = "UPDATE ResponsableUsuario
              SET idTipoResponsable = :cargo, tipoResponsabilidad = :resp
              WHERE id = :id";
    $cmd = Database::getInstance()->getDb()->prepare($query);
    $cmd->bindParam(':cargo', $cargo);
    $cmd->bindParam(':resp', $resp);
    $cmd->bindParam(':id', $id_row);
    $cmd->execute();
    exit();