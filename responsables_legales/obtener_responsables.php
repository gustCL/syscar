<?php
    require_once '../extras/config.php';
    require_once '../datos/Database.php';

    $query_tipo_resp = "SELECT ti.nombre AS tipo_resp, re.tipoResponsabilidad AS resp,
                                                       CONCAT(us.nombre, ' ', us.apellido) AS usuario,
                                                       re.idTipoResponsable AS idTipoResponsable, re.idUsuario AS idUsuario, re.id
                                                FROM  ResponsableUsuario re, TipoResponsableCargo ti, Usuario us
                                                WHERE re.idTipoResponsable = ti.idTipoResponsable AND
                                                      re.idUsuario=us.idUsuario ORDER BY resp DESC";
    $cmd_resp = Database::getInstance()->getDb()->prepare($query_tipo_resp);
    $cmd_resp->execute();
    $row_count = $cmd_resp->rowCount();
?>
<table class="table table-striped">
    <?php if ($row_count > 0): $nro = 0; ?>
        <tr>
            <th><strong>Nro.</strong></th>
            <th><strong>Usuario</strong></th>
            <th><strong>Tipo de Responsabilidad </strong></th>
            <th><strong>Responsabilidad</strong></th>
            <th><strong>Opciones</strong></th>
        </tr>
        <?php while ($row = $cmd_resp->fetch()):
            $nro++;
            $id = $row['id'];
            $idcargo = $row['idTipoResponsable'];
            $idus = $row['idUsuario'];
            $resp = $row['resp'];
            ?>
            <tr>
                <td><?= $nro; ?></td>
                <td><?= $row['usuario']; ?></td>
                <td><?= $row['tipo_resp']; ?></td>
                <td><?= $row['resp']; ?></td>
                <td>
                    <a href="#modal" id="btn-editar" name="btn-editar" class="btn btn-default btn-xs purple" data-toggle="modal"
                       onclick="editarResponsable('<?=$id;?>','<?= $idcargo ?>','<?= $idus ?>','<?= $resp ?>');"><i class="fa fa-edit"></i>Editar</a>
                    <a href="javascript: eliminarResponsable('<?= $idcargo ?>','<?= $idus ?>','<?= $resp ?>');"
                       class="btn btn-default btn-xs black"><i class="fa fa-trash-o"></i>Eliminar</a>
                </td>
            </tr>
            <?php
        endwhile; ?>
    <?php else: ?>
        <div class="alert alert-info" role="alert">
            <center>
                <h3>No se encontraron resultados.</h3>
            </center>
        </div>
    <?php endif; ?>
</table>