<?php
    require_once '../datos/Database.php';

    function existeResponsable($id_cargo, $id_usuario, $resp) {
        $query = "SELECT *
              FROM ResponsableUsuario
              WHERE idTipoResponsable = :id_cargo AND
                    idUsuario = :id_usuario AND
                    tipoResponsabilidad = :resp";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_cargo', $id_cargo);
        $cmd->bindParam(':id_usuario', $id_usuario);
        $cmd->bindParam(':resp', trim($resp));
        $cmd->execute();
        $count = $cmd->rowCount();
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    $id_cargo = $_POST['cargo'];
    $id_usuario = $_POST['usuario'];
    $responsabilidad = $_POST['responsabilidad'];

    if (existeResponsable($id_cargo, $id_usuario, $responsabilidad)) {
        echo 'existe';
        exit();
    } else {
        $query = "INSERT INTO ResponsableUsuario() VALUES (:id_cargo, :id_usuario, :resp)";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_cargo', $id_cargo);
        $cmd->bindParam(':id_usuario', $id_usuario);
        $cmd->bindParam(':resp', $responsabilidad);
        $cmd->execute();
        exit();
    }

