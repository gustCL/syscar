$(document).ready(function() {
    obtenerResponsables();

    $('#btn-editar').click(function(){
        alert('click');
    });
});

function obtenerResponsables() {
    $('#div-tabla-resp').load('obtener_responsables.php');
}

function registrarResponsable(){
    if ($('#usuario').val() == 0 || $('#cargo').val() == 0 || $('#responsabilidad').val() == 0) {
        alertify.error("Debe seleccionar todos los campos");
    } else {
        var frm = $("#frm-registrar").serialize();
        $.ajax({
            type: "POST",
            url: "registrar_responsable.php",
            data: frm,
            success: function (result) {
                if (result == 'existe') {
                    alertify.error("Ya existe un responsable con los mismos parámetros");
                } else {
                    swal("Operacion Finalizada", "El responsable legal fue registrado correctamente", "success");
                    obtenerResponsables();
                }
            }
        });
    }
}

function editarResponsable(id, idCargo, idUsuario, resp) {
    $('#modal-id').val(id);
    $('#modal-cargo').val(idCargo).change();
    $('#modal-resp').val(resp).change();
    $.ajax({
        type: "POST",
        url: "obtener_usuario.php",
        data: {id_usuario: idUsuario},
        success: function (result) {
            $('#modal-usuario').text(result);
        }
    });
}

function actualizarResponsable() {
    var frm = $('#frm-editar').serialize();
    $.ajax({
        type: "POST",
        url: "actualizar_responsable.php",
        data: frm,
        success: function () {
            alertify.success("Registro Actualizado");
            obtenerResponsables();
        }
    });
}

function eliminarResponsable(id_cargo, id_usuario, resp) {
    var frm = $("#frm-registrar").serialize();
    swal({
            title: "Eliminar Registro",
            text: "Está a punto de eliminar un registro ¿Desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: 'eliminar_responsable.php',
                    data: { id_cargo: id_cargo,
                            id_usuario: id_usuario,
                            responsabilidad: resp },
                    success: function () {
                        swal("Registro Eliminado!", "El registro ha sido removido de la base de datos", "success");
                        obtenerResponsables();
                    }
                });
            }
        }
    );
}

