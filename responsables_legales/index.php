<?php
    require_once 'head.php';

    $tipo_resp = $db->TipoResponsableCargo();
    $usuarios = $db->Usuario('estado = ?', '1');
?>
<div class="page-content">
    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>RESPONSABLES LEGALES</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>

    <div class="page-body">
        <div class="widget-body">
            <div class="container">
                <h2><center><strong>REGISTRAR NUEVO</strong></center></h2>
                <form class="form-horizontal" id="frm-registrar" name="form-registrar" action="javascript: registrarResponsable();">
                    <div class="form-group">
                        <label class="control-label lead col-md-4" for="cargo">Cargo:</label>
                        <div class="col-md-4">
                            <select class="form-control" id="cargo" name="cargo">
                                <option value="0" selected>Cargo</option>
                                <?php foreach ($tipo_resp as $tp) : ?>
                                    <option value="<?= $tp['idTipoResponsable']; ?>"><?= $tp['nombre']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label lead col-md-4" for="usuario">Usuario:</label>
                        <div class="col-md-4">
                            <select class="form-control" id="usuario" name="usuario">
                                <option value="0" selected>Usuario</option>
                                <?php foreach ($usuarios as $usuario) : ?>
                                    <option value="<?= $usuario['idUsuario']; ?>"><?= $usuario['nombre']." ".$usuario['apellido']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label lead col-md-4" for="responsabilidad">Responsabilidad:</label>
                        <div class="col-md-4">
                            <select class="form-control" id="responsabilidad" name="responsabilidad">
                                <option value="0">Responsabilidad</option>
                                <option value="Ingreso">Ingreso</option>
                                <option value="Egreso">Egreso</option>
                                <option value="Compra">Compra</option>
                                <option value="Venta">Venta</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-5">
                            <input class="btn btn-primary" type="submit" value="Registrar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <br>

        <div class="widget-body">
            <h2><center><strong>REGISTROS</strong></center></h2>
            <div class="table-responsive" id="div-tabla-resp">
            </div>
        </div>
        <br>
    </div>
</div>

    <!-- Modal para editar responsable -->
    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><strong>EDITAR RESPONSABLE</strong></h4>
                </div>
                <form id="frm-editar" name="frm-editar" action="javascript: actualizarResponsable();">
                    <div class="modal-body">
                        <div>
                            <div class="form-group">
                                <input type="text" id="modal-id" name="modal-id" readonly hidden>
                                <!--label class="control-label" for="modal-usuario">Usuario:</label-->
                                <!--input class="form-control" type="text" id="modal-usuario" name="modal-usuario" readonly-->
                                <label class="lead" id="modal-usuario"></label>
                            </div>
                            <div>
                                <label class="control-label" for="modal-cargo">Cargo:</label>
                                <select class="form-control" id="modal-cargo" name="modal-cargo">
                                    <?php foreach ($tipo_resp as $tp) : ?>
                                        <option value="<?= $tp['idTipoResponsable']; ?>"><?= $tp['nombre']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div>
                                <label class="control-label" for="modal-resp">Resp:</label>
                                <!--input class="form-control" type="text" id="modal-resp" required-->
                                <select class="form-control" id="modal-resp" name="modal-resp" required>
                                    <option value="">Responsabilidad</option>
                                    <option value="Ingreso">Ingreso</option>
                                    <option value="Egreso">Egreso</option>
                                    <option value="Compra">Compra</option>
                                    <option value="Venta">Venta</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="btn-cerrar-modal">Cerrar</button>
                        <!--button type="button" class="btn btn-primary">Guardar Cambios</button-->
                        <button type="submit" class="btn btn-primary" onclick="actualizarResponsable();" data-dismiss="modal">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>





<?php require_once '../header_footer/footer.php'; ?>