$(function() {

$('#nroPedido').on('keyup',function(){
		var dato = $('#nroPedido').val();
		var url = 'buscarPedido.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'dato='+dato,
		success: function(datos){
			$('#agrega-registros').html(datos);
		}
	});
	return false;
	});
$('#fechaI').on('keyup',function(){
		var dato = $('#fechaI').val();
		var url = 'buscarPedido.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'dato='+dato,
		success: function(datos){
			$('#agrega-registros').html(datos);
		}
	});
	return false;
	});
   


});

function ImprimirGuardar(){
    var frm = $("#resultadoP").serialize();
        
	$.ajax({
                
                url: 'ImprimiryGuardar.php',
                data: frm,
                type: 'POST',
		
                
	});
	return false; 
};
function ActualizarTotal() {

    var total = $('#subotal').val();
    if ($.isNumeric(total)) {
        total = parseFloat(total);
        $("#total").val(total);

    }
    return false;
}

function VistaDetalle()
{
    var str = $("#vistadetalle").serialize();

    $.ajax({
        url: 'VistaImpPedido.php',
        type: 'get',
        data: str,
        success: function(data) {
            $("#agrega-registros").html(data);

        }
    });

}

function buscar()
{
    var str = $("#listaPedidos").serialize();

    $.ajax({
        url: 'buscar_pedido.php',
        type: 'get',
        data: str,
        success: function(data) {
            $("#agrega-registros").html(data);

        }
    });

}

function DescuentoP() {
    var descuento = $('#descuentoP').val();
    var subtotal = $('#subtotal').val();

    if ($.isNumeric(descuento) && $.isNumeric(subtotal)) {
        descuento = parseFloat(descuento);
        subtotal = parseFloat(subtotal);
        var descuentoBs = parseFloat(((descuento * subtotal) / 100));
        var total = parseFloat(subtotal - descuentoBs);
        $('#descuentoB').val(descuentoBs.toFixed(2));
        $('#total').val(total.toFixed(2));

    }
    return false;
}

function DescuentoB() {

    var descuento = $('#descuentoB').val();
    var subtotal = $('#subtotal').val();
    var total = 0;
    var descp = 0;
    if ($.isNumeric(descuento) > 0 && $.isNumeric(subtotal)) {
        descuento = parseFloat(descuento);
        subtotal = parseFloat(subtotal);

        total = parseFloat(subtotal - descuento);

        descp = parseFloat((descuento * 100) / subtotal);
        $('#total').val(total.toFixed(2));
        $('#descuentoP').val(descp.toFixed(2));

    }

    return false;
}

function calculoPrecio(id) {
    var cantidad = $('#cantidad' + id).val();
    var costo = $('#costo' + id).val();
    var frm = $("#resultadoP").serialize();
    $("#datos").val(frm);

    if ($.isNumeric(cantidad) && $.isNumeric(costo)) {
        cantidad = parseFloat(cantidad);
        costo = parseFloat(costo);

        monto = (costo * cantidad);
        // alert(monto);
        $('#suma' + id).val(monto.toFixed(2));
        var cant = $('#cant').val();
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i <= cant; i++) {
            monto = $('#suma' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        $("#subtotal").val(subTotal.toFixed(2));

        var descuentob = $('#descuentoB').val();
        var total = 0;
        descuentob = parseFloat(descuentob);
        total = subTotal - descuentob;
        $("#total").val(total.toFixed(2));
    }

    return false;

}

function montSubtotal() {

    var subTotal = $('#subTotal').val();
    var descuento = $('#descuentoB').val();
    var total = 0;
    if ($.isNumeric(subTotal) > 0 && $.isNumeric(descuentoB)) {
        descuento = parseFloat(descuento);
        subTotal = parseFloat(subTotal);
        total = subTotal - descuento;
        $("#total").val(total.toFixed(2));
    }

    return false;

}

function calculoPrecioVenta(id) {
    var costo = $('#costo' + id).val();
    var cantidad = $('#cantidad' + id).val();

    if ($.isNumeric(costo) && $.isNumeric(cantidad)) {
        costo = parseFloat(costo);
        cantidad = parseFloat(cantidad);

        monto = (costo * cantidad);
        $('#suma' + id).val(monto.toFixed(2));
        var cant = $('#cant').val();
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i <= cant; i++) {
            monto = $('#suma' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        $("#subtotal").val(subTotal.toFixed(2));

        var descuento1 = $('#descuentoB').val();
        var total = 0;
        descuento1 = parseFloat(descuento1);
        total = subTotal - descuento1;
        $("#total").val(total.toFixed(2));
    }

    return false;

}

function eliminar(id) {
    var url = '../pedidos/eliminar_pedido.php';
    var pregunta = confirm('¿Esta seguro de eliminar este pedido?');
    if (pregunta == true) {
        $.ajax({
            type: 'POST',
            url: url,
            data: 'id=' + id,
            success: function(registro) {
                $('#agrega-registros').html(registro);
                return false;
            }
        });
        return false;
    } else {
        return false;
    }
}

function fn_dar_eliminar(dato) { //Elimina las filas de la tabla de nota de venta y resta el subtotal

    var cantidad = $('#cant').val();
    var cantNueva = cantidad - 1;
    $('#cant').val(cantNueva);
    $("a.eliminar").click(function() {
        id = $(this).parents("tr").find("td").eq(0).html();

        if (confirm("Desea eliminar el producto Nro:" + id)) {

            $(this).parents("tr").fadeOut("normal", function() {

                $(this).remove();

                subT = parseFloat($('#subtotal').val()) - dato;
                $('#subt').val(parseFloat($('#subt').val()) - dato);
                $('#total').val($('#subt').val());
                f = f - 1;
            });
        }
    });
}
;

function ProductoProveedor()
{
    var str = $("#nuevo_pedido").serialize();

    $.ajax({
        url: 'Verificar_Pedido.php',
        type: 'get',
        data: str,
        success: function(data) {
            $("#agrega-registros").html(data);

        }
    });

}

function Buscar_Productos() {
    var str = $("#nuevo_pedido").serialize();

    $.ajax({
        url: 'ConsultaProductoProveedor.php',
        type: 'post',
        data: str,
        success: function(data) {
            $("#productosProv").html(data);
            //alert(data);
        }
    });
}

function ListarProductosProv()
{
    var str = $("#nuevo_pedido").serialize();

    $.ajax({
        url: 'Verificar_Pedido.php',
        type: 'get',
        data: str,
        success: function(data) {
            $("#agrega-registros").html(data);

        }
    });

}

function nuevoPedido() { //REGISTRA NUEVO CLIENTE 
    var frm = $("#nuevo_pedido").serialize();
    var frm2 = $("#resultadoP").serialize();
    $.ajax({
        url: 'PedidosDetalles.php',
        data: frm,
        type: 'post',
        success: function(registro) {
            var datos = eval(registro);
            $('#nitci').val(datos[0]);
            $('#nombreCliente').val(datos[1]);
            return false;

        }

    });
    return false;
}

