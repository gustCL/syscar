<?php
require_once '../datos/Database.php';

$actual = date("Y-m-d");
$inicioMes = date("Y-m");
$inicioMes = $inicioMes . "-01";
$i = 1;
$idp = $_POST['cmp'];
$cant = $_POST['cantFilas'];
$provee = $_POST['provee'];
$nit = $_POST['nitp'];
//VARIABLES ESTATICAS DEL FORMULARIO
$subtotal = $_POST['subTotal'];
$total = $_POST['total'];
$desc = $_POST['descuentoB'];

// DATOS DE LA SUCURSAL
$consulta = "select nombreSucur, fecha, hora, idUsuario from compra c, sucursal s where c.idSucursal = s.idSucursal and c.idCompra = '$idp'";
$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$dato = $resultado->fetch();
$nombreS = $dato['nombreSucur'];
$fecha = $dato['fecha'];
$hora = $dato['hora'];
$user = $dato['idUsuario'];

//EMPRESA
$consulta = "select nombreEmpresa, nit from Empresa ";
$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$empresa = $resultado->fetch();

//DATOS DEL USUARIO
$consulta = "select nombre, apellido from Usuario where idUsuario = '$user'";
$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$dato = $resultado->fetch();


//EXTRAER LOS DATOS DEL PROVEEDOR
$consulta = "select idProveedor, nombreProve  from Proveedor where nit = '$nit' or nombreProve = '$provee' ";
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$row = $comando->fetch();
$idProveedor = $row['idProveedor'];

$consulta = "select p.nit as nit, p.nombreProve as proveedor, d.direccion, t.numeroTelefono as telefono from Proveedor p, Direccion d, Telefono t where p.idProveedor = d.idProveedor and p.idProveedor = t.idProveedor and p.idProveedor = '$idProveedor'";
$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$dato1 = $resultado->fetch();
$nitP = $dato1["nit"];
$proveedor = $dato1["proveedor"];
$direcProv = $dato1["direccion"];
$telef = $dato1["telefono"];

$nombreUser = $dato['nombre'];
$apellido = $dato['apellido'];
?>

<html> 
    <head> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="all">
        <link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen">
        <!--<link rel="stylesheet" type="text/css" href="../css/print/print.css" media="print">-->
        <link href="../reporteCompras/printListaTransac.css" rel="stylesheet" type="text/css" media="print" />  
        <!--<link href="../css/style.css" rel="stylesheet" media="all">-->
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet"  media="all">
        <script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="jPedidos.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- USO DE LA LIBRERIA PARA EXPORTAR A EXCEL-->
        <script type="text/javascript" src="jquery-1.3.2.min.js"></script>
        <script language="javascript">
            $(document).ready(function() {
                $(".botonExcel").click(function(event) {
                    $("#datos_a_enviar").val($("<div>").append($("#Exportar_a_Excel").eq(0).clone()).html());
                    $("#FormularioExportacion").submit();
                });
            });
        </script>
    </head> 
    <body> 
        <script language="JavaScript">
            $(document).ready(function() {
                doPrint();
            });
            function doPrint() {
                //document.all.item("mostrarUser").style.visibility='visible'; 
                window.print()();
                //document.all.item("mostrarUser").style.visibility='hidden';
            }
            function volver(dato) {
                location.href = "index.php";
            }
        </script>
        <div id="leteral"></div>
        <div id="noprint">
            <table>
                <tr>
                    <td>
                        <button type="button" name="volver" id="volver" onclick="volver()">Volver</button>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <button type="button" onclick="doPrint()" >Imprimir</button>
                    </td>
<!--                    <td>
                        <a type="button" class="botonExcel">Exportar a excel</a>
                        <link rel="alternate" media="print" href="nota_pedido.xlsx"/>
                    </td>-->
                </tr>
            </table> 
        </div>
        <div id="hoja" style="height: auto"> 
            <div id="mostrarUser">

<!--                <label><script type="text/javascript">
                    //<![CDATA[
                    function makeArray() {
                        for (i = 0; i < makeArray.arguments.length; i++)
                            this[i + 1] = makeArray.arguments[i];
                    }
                    var months = new makeArray('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                            'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                    var days = new makeArray('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes',
                            'Sabado');
                    var date = new Date();
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var yy = date.getYear();
                    var year = (yy < 1000) ? yy + 1900 : yy;
                    document.write(days[date.getDay() + 1] + " " + day + " de " + months[month] + " del " + year + " " + date.getHours() + ":" + date.getMinutes());
                    //]]>
                    </script></label>-->
            </div>
            <style>
                #encabezado{
                    margin-left: 5%;
                    /*border: 1px solid black;*/
                    width: 90%;
                    height: 20%;
                    /*background-color: #fffff;*/
                }
                #logo
                {
                    margin-top: 1%;
                    margin-left: 1%;
                    margin-right: 1%;
                    margin-bottom: 1%;
                    width: 20%;
                    height: 40%;
                    float: left;
                    /*border: 1px solid yellow;*/
                    padding-bottom: 2%;
                    padding-top: 2%;
                    padding-left: 2%;
                    padding-right: 2%;
                }
                #titulo
                {
                    font-family: serif 18pt ;
                    margin-top: 1%;
                    width: 50%;
                    height: 61%;
                    float: left;
                    /*border: 1px solid green;*/
                    aling: center;
                }
                #tit
                {
                    alignment-adjust: central;
                    alignment-baseline: central;
                }
                #inf{
                    margin-top: 6%;
                    padding-bottom: 2%;
                    padding-top: 2%;
                    padding-left: 2%;
                    padding-right: 2%;
                    margin-right: 2%;
                    /*border: 1px solid red;*/
                    float: right;
                    width: 26%;
                    height: 30%;
                }
                #informacion{
                    margin-left: 5%;
                    margin-top: 3%;
                    /*border: 1px solid black;*/
                    width: 90%;
                    height: auto;
                }
                .tabla {
                    font-family: Verdana, Arial, Helvetica, sans-serif;
                    font-size:11px;
                    text-align: left;
                    width: 93%;
                }
                .tabla>tr>td {
                    font-family: Verdana, Arial, Helvetica, sans-serif;
                    font-size:11px;
                    text-align: right;
                    width: 100%;
                    padding-left: 1%;
                }
                .tabla th {
                    padding: 5px;
                    font-size: 16px;
                    background-color: #83aec0;
                    background-image: url(fondo_th.png);
                    background-repeat: repeat-x;
                    color: #FFFFFF;
                    border-right-width: 1px;
                    border-bottom-width: 1px;
                    border-right-style: solid;
                    border: 1px solid black;
                    border-bottom-style: solid;
                    border-right-color: #558FA6;
                    border-bottom-color: #558FA6;
                    font-family: “Trebuchet MS”, Arial;
                    text-transform: uppercase;
                }
                .tabla .modo1 {
                    font-size: 12px;
                    font-weight:bold;
                    background-color: #e2ebef;
                    background-image: url(fondo_tr01.png);
                    border: 1px solid black;
                    background-repeat: repeat-x;
                    color: #34484E;
                    font-family: “Trebuchet MS”, Arial;
                }
                .tabla .modo1 td {
                    padding: 5px;
                    border-right-width: 1px;
                    border-bottom-width: 1px;
                    border-right-style: solid;
                    border-bottom-style: solid;
                    border: 1px solid black;
                    border-right-color: #A4C4D0;
                    border-bottom-color: #A4C4D0;
                    text-align:left;
                }
                .tabla .modo2 {
                    font-size: 12px;
                    font-weight:bold;
                    background-color: #fdfdf1;
                    background-image: url(fondo_tr02.png);
                    background-repeat: repeat-x;
                    color: black;
                    font-family: “Trebuchet MS”, Arial;
                    text-align:right;
                }
                .tabla .modo2 td {
                    padding: 5px;
                    border-right-width: 1px;
                    border-bottom-width: 1px;
                    border-right-style: solid;
                    border-bottom-style: solid;
                    border-right-color: #EBE9BC;
                    border-bottom-color: #EBE9BC;
                }
                .tabla .modo2 th {
                    background-image: url(fondo_tr02a.png);
                    background-position: left top;
                    font-size: 12px;
                    font-weight:bold;
                    background-color: #fdfdf1;
                    background-repeat: repeat-x;
                    color: #990000;
                    font-family: “Trebuchet MS”, Arial;
                    text-align:left;

                    border-right-width: 1px;
                    border-bottom-width: 1px;
                    border-right-style: solid;
                    border-bottom-style: solid;
                    border-right-color: #EBE9BC;
                    border-left-color: #EBE9BC;
                    border-bottom-color: #EBE9BC;
                }
            </style>
            <div class="widget">
                <div id="encabezado" >
                    <div id="logo" >
                        <img width="100" height="100" src="..\..\farmacia\assets\img\Logo1.png"/>
                    </div>
                    <div id="titulo">
                        <center>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="right">
                                            <font size='6'><strong><label for="user_name">NOTA DE PEDIDO</label></strong></font><br>
                                            <font size='5'><strong><center><label for="user_name"><?= $empresa['nombreEmpresa'] ?></label></center></strong></font><br>
                                <font size='4'><center><label for="user_name">NIT: <?= $empresa['nit'] ?></label></center></font>

                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                    <div id="inf">
                        <center>
                            <table >
                                <tbody>
                                    <tr class="">
                                        <td>
                                            <fieldset>
                                                <strong><label for="user_name">Pedido Nro:</label></strong>
                                            </fieldset>
                                        </td>
                                        <td style="text-align: right">

                                            <label for="user_name"><?= $idp ?> </label><br>

                                        </td>
                                    </tr>
                                    <tr class="">
                                        <td>

                                            <strong><label for="user_name">Fecha:</label></strong>

                                        </td>
                                        <td >
                                            <fieldset>
                                                <label for="user_name"><?= $fecha ?></label>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>

                </div>

                <div align="center" id="informacion">
                    <div id="content">
                        <table class="tabla" border="1" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th><strong>A:</strong></th>
                                    <th><strong>Enviar a:</strong></th>
                                    <th><strong>Lugar de Entrega:</strong></th>
                                </tr>
                            </thead>
                            <tr class="">
                                <td>
                                    <strong><label for="user_name">Proveedor:</label></strong> <?= $proveedor ?><br>
                                    <strong><label>Nro. Nit:</label></strong> <?= $nitP ?><br>
                                    <strong><label for="user_name">Direccion:</label></strong> <?= $direcProv ?><br>
                                    <strong><label for="user_name">Telefono:</label></strong> <?= $telef ?><br>
                                </td>
                                <td>
                                    <strong><label for="user_name">Solicita:</label></strong>
                                    <?= $nombreUser ?> <?= $apellido ?><br>
                                    <label for="user_name">Empresa:</label> <?= $empresa['nombreEmpresa'] ?><br>
                                    <label for="user_name">Sucursal:</label> <?= $nombreS ?>
                                </td>
                                <td>
                                    <strong><label for="user_name">Lugar de entrega: </label></strong> <?= $nombreS ?><br>
                                    <strong><label for="user_name">Fecha: </label></strong> <?= $fecha ?><br>
                                    <strong><label for="user_name">Hora: </label></strong> <?= $hora ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                            <thead>
                                <tr>
                                    <th><strong>Profucto</strong></th>
                                    <th><strong>Cantidad</strong></th>
                                    <th><strong>Precio Unidad</strong></th>
                                    <th><strong>Costo</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                while ($i <= $cant) {
                                    $Producto = $_POST['nombreComer' . +$i];
                                    $cantidadp = $_POST['cantidad' . +$i];
                                    $costoP = $_POST['costo' . +$i];
                                    ?>
                                    <tr class="modo2">
                                        <td style="text-align: left">
                                            <label size="30" type="text"  id="nombreComer<?= $i ?>" name="nombreComer<?= $i ?>"><?= $Producto ?> </label>
                                        </td>
                                        <td >
                                            <label size="10" type="text" id="cantidad<?= $i ?>" name="cantidad<?= $i ?>"><?= $cantidadp ?></label>
                                        </td>
                                        <td >
                                            <label size="10" type="text" id="precioU<?= $i ?>" name="precioU<?= $i ?>"><?= $costoP ?> </label>
                                        </td>
                                        <td >
                                            <label size="10" type="text" id="costo<?= $i ?>" name="costo<?= $i ?>"><?= $costoP * $cantidadp ?> </label>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                            
                            <tfoot>          
                            <intup type="text" id="cantFilas" name="cantFilas" name="subTotal" value="<?= $cant ?>" size="8" hidden/>
                            <tr style="padding-bottom: 1em">
                                <td ></td>
                                <td colspan="2" class="text-right"><strong>Sub Total:</strong></td>
                                <td class="text-right">
                                    <label type="text" id="subTotal" name="subTotal" size="8"><?= $subtotal ?></label></td>
                            </tr>
                            <tr style="padding-bottom: 1em">
                                <td ></td>
                                <td colspan="2" class="text-right"><strong>Descuento Bs:</strong></td>
                                <td class="text-right">
                                    <label type="text" id="descuentoB" name="descuentoB" size="8"><?= $desc ?></label>
                            </tr>
                            <br>
                            <tr style="padding-bottom: 1em">
                                <td ></td>
                                <td colspan="2" class="text-right"><strong>Total:</strong></td>
                                <td class="text-right ">
                                    <strong>
                                        <label type="text" id="total" name="total" size="8"><?= $total ?></label>
                                    </strong>
                                </td>
                            </tr>
                            <br>
                            <tr>
                                <td rowspan="3" style="text-align: left">
                                    <strong>
                                        Son: 
                                    <?php
                                    require_once '../nueva_venta/factura/conversor.php';
                                     echo @convertir($total);
                                    ?>
                                    </strong>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div style="border: 1px solid black; width: 40%; height: 12%; margin-top: 2%; " >
                        <br>
                        <strong><br>
                        _______________________<br>
                        ENTREGADO POR:</strong>
                    </div>
                </div>
                <form action="ficheroExcel.php" method="post" id="FormularioExportacion">
                    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                </form>
                </body>
                </html>