<?php
require_once 'head.php';
require_once("../datos/Database.php");

$id = $_GET['id'];
$cant = 0;
$consulta = "select c.idCompra, pt.codigoProducto, pt.nombreComercial,  pr.nit, pr.nombreProve, p.cantidad, p.precioCosto, c.fecha, c.montoSubTotal, c.montoTotal, c.descuento
from Proveedor pr, Compra c, Pedido p, Producto pt
where pr.idProveedor = c.idProveedor and c.idCompra = p.idCompra and p.codigoProducto = pt.codigoProducto and c.estado = 1 and c.idCompra = '$id'";

$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$consulta1 = "select distinct pr.nit, pr.nombreProve, c.fecha, c.idCompra
from Proveedor pr, Compra c, Pedido p, Producto pt
where pr.idProveedor = c.idProveedor and c.idCompra = p.idCompra and p.codigoProducto = pt.codigoProducto and c.estado = 1 and c.idCompra = '$id'";

$resultado1 = Database::getInstance()->getDb()->prepare($consulta1);
$resultado1->execute();
$datosp = $resultado1->fetch();
$nro = 0;
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                MODIFICAR PEDIDO
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget-body ">
                                <div class="form-group">
                                    <center><h2><strong>DETALLE DEL PEDIDO</strong></h2></center>
                                </div>
                                <form action=javascript:ListarProductosProv() id="nuevo_pedido" class="form-horizontal">
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label"><strong>Pedido</strong></label>
                                        <div class="col-lg-3">
                                            <input id="pedido" style="text-align: right" type="text" value="<?= $datosp['idCompra'] ?>" name="pedido" readonly="readonly"/>
                                        </div>
                                        <label class="col-lg-2 control-label"><strong>Fecha</strong></label>
                                        <div class="col-lg-3">
                                            <input id="fecha" style="text-align: right" type="text" value="<?= $datosp['fecha'] ?>" name="fecha" readonly="readonly"/>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-lg-2 control-label"><strong>NIT</strong></label>
                                        <div class="col-lg-3">
                                            <input id="nit" style="text-align: right" type="text" value="<?= $datosp['nit'] ?>" name="nit" readonly="readonly"/>
                                        </div>
                                        <label class="col-lg-2 control-label"><strong>Proveedor</strong></label>
                                        <div class="col-lg-3">
                                            <input type="text" style="text-align: right" value="<?= $datosp['nombreProve'] ?>" name="proveedor" id="proveedor"  readonly="readonly"/>                                      
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-lg-2 control-label"><strong>Pedido para dias:</strong></label>
                                        <div class="col-lg-3">

                                            <select type="text" class="form-control" name="dias" id="dias">
                                                <option value="1">1 mes</option>
                                                <option value="2">2 meses</option>
                                                <option value="3">3 meses</option>
                                                <option value="4">4 meses</option>
                                                <option value="5">5 meses</option>
                                                <option value="6">6 meses</option>

                                            </select>

                                        </div>

                                        <label class="col-lg-2 control-label"><strong>Por ventas de los meses:</strong></label>
                                        <div class="col-lg-3">

                                            <select type="text" class="form-control" name="meses" id="meses">
                                                <option value="1">1 anterior</option>
                                                <option value="2">2 anterior</option>
                                                <option value="3">3 anterior</option>
                                                <option value="4">4 anterior</option>
                                                <option value="5">5 anterior</option>
                                                <option value="6">6 anterior</option>
                                                <option value="7">7 anterior</option>
                                                <option value="8">8 anterior</option>
                                                <option value="9">9 anterior</option>
                                                <option value="10">10 anterior</option>
                                                <option value="11">11 anterior</option>
                                                <option value="12">12 anterior</option>
                                            </select>

                                        </div>
                                        <div class="col-lg-2">
                                            <input type="submit"  class="btn btn-yellow" name="Buscar" id="Buscar" value="Buscar" />

                                        </div>

                                    </div>


                                </form>

                            </div>
                            <br>
                            <div class="widget-body">

                                <form action="actualizar_pedido.php" method="post" class="form-horizontal">
                                    <!-- TABLA DE NOTA DE VENTA-->

                                    <table id="grilla" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Nro</th>
                                                <th>Codigo Producto</th>
                                                <th>Nombre de producto</th>
                                                <th>Cant. Pedida</th>
                                                <th>Precio costo</th>
                                                <th>SubTotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while ($row = $resultado->fetch()) {
                                                $subtotal = $row['montoSubTotal'];
                                                $total = $row['montoTotal'];
                                                $desc = $row['descuento'];
                                                $nro++;
                                                ?>
                                                <tr>
                                                    <td class="hidden-xs">
                                                        <?= $nro ?>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        <input size="10" type="text" id="codP<?= $nro ?>" name="codP<?= $nro ?>" value="<?= $row['codigoProducto'] ?> " readonly/>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        <input size="50" type="text" id="nombreComer<?= $nro ?>" name="nombreComer<?= $nro ?>" value="<?= $row['nombreComercial'] ?> " readonly />
                                                    </td>
                                                    <td class="hidden-xs">
                                                        <input style="text-align: right" size="10" type="text" id="cantidad<?= $nro ?>" name="cantidad<?= $nro ?>" value="<?= $row['cantidad'] ?> " onkeyup="calculoPrecio(<?= $nro ?>)" />
                                                    </td>
                                                    <td class="hidden-xs">
                                                        <input style="text-align: right" size="10" type="text" id="costo<?= $nro ?>" name="costo<?= $nro ?>" value="<?= $row['precioCosto'] ?> " onkeyup="calculoPrecio(<?= $nro ?>)"/>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        <input style="text-align: right" size="10" type="text" id="suma<?= $nro ?>" name="suma<?= $nro ?>" value="<?= $row['precioCosto'] * $row['cantidad'] ?> " />
                                                    </td>
                                                    <td><a class="eliminar"><i id="eliminar<?= $nro ?>" class="fa fa-trash-o" onclick="fn_dar_eliminar(<?= $nro ?>)" /></a></td>
                                                </tr>
                                                <?php
                                                $cant = $nro;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                        <input type="text" id="cant" name="cant" value="<?= $cant ?>" size="8"/>
                                        <input type="text" id="cmp" name="cmp" value="<?= $datosp['idCompra'] ?>" size="8"/>
                                        <input type="text" id="prov" name="prov" value="<?= $datosp['nombreProve'] ?>" size="8"/>
                                        <input type="text" id="nitp" name="nitp" value="<?= $datosp['nit'] ?>" size="8"/>
                                        <input type="text" id="fechap" name="fechap" value="<?= $datosp['fecha'] ?>" size="8"/>
                                        <tr>
                                            <td colspan="4" class="invisible bg-snow"></td>
                                            <td ><strong>Sub Total</strong></td>
                                            <td class="text-center"> <input style="text-align: right" type="text" id="subtotal" name="subtotal" size="8" value="<?= $subtotal ?>" onkeyup="montSubtotal()" readonly/> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="invisible bg-snow"></td>
                                            <td ><strong>Descuento Bs</strong></td>
                                            <td class="text-center"><input style="text-align: right" type="text" id="descuentoB" name="descuentoB" value="<?= $desc ?>" onkeyup="DescuentoB()" size="8"/></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="invisible bg-snow"></td>
                                            <td class="text-center"><strong>Total</strong></td>
                                            <td class="text-center "><strong><input style="text-align: right"  type="text" id="total" name="total" size="8" value="<?= $total ?>" readonly/></strong></td>
                                        </tr>

                                        </tfoot>
                                    </table>  
                                    <div class="form-inline">

                                        <input type="button" class="btn btn-success" name="imprimir" id="imprimir"  value= "Imprimir" placeholder=""/>
                                        <input type="submit" class="btn btn-primary" name="guardar" id="guardar"  value= "Guardar" placeholder=""/>
                                        <input type="button" class="btn btn-danger" name="cancelar" id="cancelar"  value= "Cancelar" placeholder=""/>                                                
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once ('../header_footer/footer.php');
?>