<?php
require_once '../datos/Database.php';
session_start();
$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];

$nitprov = $_REQUEST['buscar_nit'];
$nombreprov = $_REQUEST['nombre_prov'];
$cant = 0;
$subtotal = 0;
ini_set("data.timezone", "America/La_Paz");
$nro = 0;
$fechaActual = date('Y-m-d');
$dato = $_GET['meses'];
$dias = $_GET['dias'];

for ($i = $dato; $i <= $dato; $i++) {
    $fecha = date("Y-m-d", mktime(0, 0, 0, date("m") - $i, date("d"), date("Y")));
}


$consulta = "select p.codigoProducto, p.nombreComercial, sum(Cant_Vendida)TotalVendido, p.StockTotal, p.precioCosto, p.nit, p.Proveedor "
        . "from PedidoProducto p "
        . "where p.nit = '" . $_REQUEST['buscar_nit'] . "' and "
        . "p.proveedor like '" . $_REQUEST['nombre_prov'] . "%' and "
        . "(p.fecha >= '$fecha') and "
        . "(p.fecha <= '$fechaActual') and "
        . " p.idSucursal = '$idSucursal' or p.StockTotal = 0 group by p.codigoProducto";


$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();

if ($resultado->rowCount() < 1) {
    echo '<table id="grilla" class="table table-striped table-bordered table-hover">
            <thead>
                 <tr>
                    <th>Nro</th>
                    <th>Codigo</th>
                    <th>Nombre de producto</th>
                    <th>Cant. Vendida</th>
                    <th>Stock</th>
                    <th>Precio costo anterior</th>
                    <th>Precio costo</th>
                    <th>Cantidad</th>
                    <th>SubTotal</th>
                </tr>
            </thead>
            <tbody>
            <!--SE AGREGAN LAS FILAS DE LOS PRODUCTOS POR VENDER-->
            </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" class="invisible bg-snow"></td>
                        <td >Sub Total</td>
                        <td class="text-center"> <input type="text" id="subTotal" name="subTotal" size="8" readonly/> </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="invisible bg-snow"></td>
                        <td >Descuento %</td>
                        <td class="text-center"><input type="text" id="descuentoP" name="descuentoP" size="8" /></td>
                    </tr>
                    <tr>
                        <td colspan="6" class="invisible bg-snow"></td>
                        <td >Descuento Bs</td>
                        <td class="text-center"><input type="text" id="descuentoB" name="descuentoB" size="8"/></td>
                    </tr>
                    <tr>
                        <td colspan="6" class="invisible bg-snow"></td>
                        <td class="text-center"><strong>Total</strong></td>
                        <td class="text-center "><strong><input type="text" id="total" name="total" size="8" readonly/></strong></td>
                    </tr>
                </tfoot>
        </table>
        <div class="form-inline">

            <input type="button" class="btn btn-success" name="" id=""  value= "Imprimir" placeholder=""/>

            <input type="button" class="btn btn-primary" name="" id=""  value= "Guarda" placeholder=""/>
            <input type="button" class="btn btn-danger" name="" id=""  value= "Cancelar" placeholder=""/>                                                


        </div>';
} else {
    ?>
    <div class="table-responsive">
        <form action="PedidosDetalles.php" id="resultadoP" name="resultadoP" method="post" class="form-horizontal">
            <table id="grilla" class="table table-striped">
                <thead>
                    <tr>
                        <th><strong>Nro</strong></th>
                        <th><strong>Codigo</strong></th>
                        <th><strong>Nombre de Producto</strong></th>
                        <th><strong>Cant. Vendida</strong></th>
                        <th><strong>Stock</strong></th>
                        <th><strong>Precio Costo Anterior</strong></th>
                        <th><strong>Precio Costo</strong></th>
                        <th><strong>Cantidad Sugerida</strong></th>
                        <th><strong>SubTotal</strong></th>
                    </tr>
                </thead>
                <tbody>
                    <!--SE AGREGAN LAS FILAS DE LOS PRODUCTOS POR VENDER-->
                    <?php
                    while ($row = $resultado->fetch()) {
                        $nro++;
                        ?>
                        <tr>
                            <td class="hidden-xs">
                                <?php echo $nro; ?>
                            </td>
                            <td class="hidden-xs">
                                <input size="10" style="text-align:right" type="text" id="codProducto<?= $nro ?>" name="codProducto<?= $nro ?>" value="<?= $row['codigoProducto'] ?>" readonly/>
                            </td>
                            <td class="hidden-xs">
                                <input size="45" type="text" id="nomComer<?= $nro ?>" name="nomComer<?= $nro ?>" value="<?= $row['nombreComercial'] ?>" readonly/>
                            </td>
                            <td class="hidden-xs">
                                <input size="10" type="text" style="text-align:right" id="totalvendido<?= $nro ?>" name="totalvendido<?= $nro ?>" value="<?= $row['TotalVendido'] ?>" readonly/>
                            </td>
                            <td class="hidden-xs">
                                <input size="10" style="text-align:right" type="text" id="stocktotal<?= $nro ?>" name="stocktotal<?= $nro ?>" value="<?= $row['StockTotal'] ?>" readonly />
                            </td>
                            <td class="hidden-xs">
                                <input type="text" style="text-align:right" id="precioCosto<?= $nro ?>" name="precioCosto<?= $nro ?>" value="<?= $row['precioCosto'] ?>" readonly />
                            </td>
                            <td class="hidden-xs">
                                <input size="10" style="text-align:right" type="text" id="costo<?= $nro ?>" name="costo<?= $nro ?>" value="<?= $row['precioCosto'] ?>" onkeyup="calculoPrecio(<?= $nro ?>)"/>
                            </td>
                            <td class="hidden-xs">
                                <input size="10" style="text-align:right" type="text" id="cantidad<?= $nro ?>" name="cantidad<?= $nro ?>" value="<?= intval($row['TotalVendido'] * $dias) ?>" onkeyup="calculoPrecio(<?= $nro ?>)"/>
                            </td>
                            <td class="hidden-xs">
                                <input type="text" size="10" style="text-align:right" id="suma<?= $nro ?>" name="suma<?= $nro ?>" value="<?= (($row['TotalVendido'] * $row['precioCosto'])) ?>"/>
                            </td>
                            <td><a class="eliminar"><i id="eliminar<?php $nro ?>" class="fa fa-trash-o" onclick="fn_dar_eliminar(<?php $nro ?>)" /></a></td>
                        </tr>
                        <?php
                        $cant = $nro;
                        $subtotal = $subtotal + ((intval($row['TotalVendido'])) * $row['precioCosto']);
                    }
                    ?>
                <input type="text" id="cant" name="cant" value="<?= $cant ?>" size="8" hidden/>
                <input type="text" id="nitproveedor" name="nitproveedor<?= $row['nit'] ?>" value="<?= $nitprov ?>" size="8" hidden/>
                <input type="text" id="nombreproveedor" name="nombreproveedor" value="<?= $nombreprov ?>" size="8" hidden/>
                </tbody>
                <tfoot>

                    <tr>
                        <td colspan="7" class="invisible bg-snow"></td>
                        <td ><strong>Sub Total</strong></td>
                        <td class="text-center"> <input type="text" style="text-align:right" id="subtotal" name="subtotal" size="8" value="<?= $subtotal ?>" onkeyup="montSubtotal()" readonly/> </td>
                    </tr>
                    <tr>
                        <td colspan="7" class="invisible bg-snow"></td>
                        <td ><strong>Descuento %</strong></td>
                        <td class="text-center"><input type="text" style="text-align:right" id="descuentoP" value="0" name="descuentoP" onkeyup="DescuentoP()" size="8" /></td>
                    </tr>
                    <tr>
                        <td colspan="7" class="invisible bg-snow"></td>
                        <td ><strong>Descuento Bs</strong></td>
                        <td class="text-center"><input type="text" style="text-align:right" id="descuentoB" value="0.0" name="descuentoB" onkeyup="DescuentoB()" size="8"/></td>
                    </tr>
                    <tr>
                        <td colspan="7" class="invisible bg-snow"></td>
                        <td class="text-center"><strong>Total</strong></td>
                        <td class="text-center "><strong><input type="text" style="text-align:right" id="total" name="total" value="<?= $subtotal ?>" size="8" readonly/></strong></td>
                    </tr>

                </tfoot>

            </table> 
            <div class="form-inline">

                <input type="button" class="btn btn-success" name="imprimir" id="imprimir" value= "Imprimir" onclick="ImprimirGuardar()" placeholder/>
                <input type="submit" class="btn btn-primary" name="guardar" id="guardar"  value= "Guardar" placeholder=""/>
                <input type="button" class="btn btn-danger" name="cancelar" id="cancelar" onclick="volver()" value= "Cancelar" placeholder=""/>                                                
                <script>
                    function volver() {
                        location.href = "../pedidos";
                    }
                </script>

            </div>
        </form>
<!--        <form action="ImprimiryGuardar.php" id="impvista" method="post">
            <div id="contenedor" name="contenedor">
                <input type="text" id="datos" name="datos" value="" hidden="hidden"/>
                
            </div>
        </form>-->
    </div>
    <?php
}
?>