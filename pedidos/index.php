<?php
require_once 'head.php';

$sesion = $_SESSION['userMaster'];
$sucursal = $sesion['idSucursal'];
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                LISTA DE PEDIDOS
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>

    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget-body ">
                                <form id="listaPedidos" method="post" class="form-horizontal">
                                    <br>
                                    <br>
                                    <div class="form-inline">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="form-group">

                                            <label class="col-lg-4 control-label"><strong>Nro. Pedido</strong></label>
                                            <div class="col-lg-3">
                                                <span class="input-icon">
                                                    <input type="text" class="form-control" name="nroPedido" id="nroPedido"  placeholder="Nro. Pedido"/>
                                                    <i class="glyphicon glyphicon-search circular blue"></i>

                                                </span>
                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="input-group">
                                            <input id="fechaI" name="fechaI" type="date" placeholder="Fecha" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>

                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="input-group">
                                            <a  type="button" id="btnBuscar" name="btnBuscar"  class="btn btn-yellow" >Buscar</a>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="input-group">
                                            <a type="button" href="nuevo_pedido.php" class="btn btn-primary"  >Nuevo Pedido</a>
                                        </div>
                                    </div>

                                    <br>
                                    <br>
                                    <br>
                                </form>


                            </div>
                            <br>
                            <div class="widget-body">
                                <form id="listadoP" name="listadoP" method="post" class="form-horizontal">
                                    <div id="agrega-registros">
                                        <table class="table table-striped table-bordered table-hover" >
                                            <thead>

                                                <tr>
                                                    <th class="hidden-xs">
                                                        NRO. COMPRA
                                                    </th>
                                                    <th class="hidden-xs">
                                                        NIT
                                                    </th>
                                                    <th class="hidden-xs">
                                                        PROVEEDOR
                                                    </th>
                                                    <th class="hidden-xs">
                                                        FECHA
                                                    </th>
                                                    <th class="hidden-xs">
                                                        MONTO TOTAL
                                                    </th>
                                                    <th class="hidden-xs">

                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $registro = "select distinct c.idCompra,  pr.nit, pr.nombreProve, c.fecha, c.montoTotal 
                                                    from Proveedor pr, Compra c, Pedido p, Producto pt
where pr.idProveedor = c.idProveedor and c.idCompra = p.idCompra and p.codigoProducto = pt.codigoProducto and c.estado = 1 and c.idSucursal = '$sucursal'";
                                                require_once '../datos/Database.php';
                                                try {
                                                    $comando = Database::getInstance()->getDb()->prepare($registro);
                                                    $comando->execute();
                                                    while ($row = $comando->fetch()) {
                                                        $id = $row['idCompra'];
                                                        ?>
                                                        <tr>
                                                            <td id = "" name="" >
                                                                <?= $id ?>
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <?= $row['nit'] ?>
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <?= $row['nombreProve'] ?>
                                                            </td>
                                                            <td>
                                                                <?= $row['fecha'] ?>
                                                            </td>
                                                            <td>
                                                                <?= $row['montoTotal'] ?>
                                                            </td>
                                                            <td>                          
                                                                <a href="ver_pedido.php?id=<?= $id ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i>Ver</a>
                                                                <a href="modificar_pedidos.php?id=<?= $id ?>"  class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                                                                <a href="javascript:eliminar('<?= $id ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }//terminacion del while
                                                } catch (PDOException $e) {

                                                    echo 'Error: ' . $e;
                                                }
                                                ?>  
                                            </tbody>
                                        </table>   

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once ('../header_footer/footer.php');
?> 