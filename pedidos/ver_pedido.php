<?php
require_once 'head.php';
require_once("../datos/Database.php");

$id = $_GET['id'];
$nro = 0;
$cant = 0;

$consulta = "select c.idCompra, pt.codigoProducto, pt.nombreComercial,  pr.nit, pr.nombreProve, p.cantidad, p.precioCosto, c.fecha, c.montoSubTotal, c.montoTotal, c.descuento
from Proveedor pr, Compra c, Pedido p, Producto pt
where pr.idProveedor = c.idProveedor and c.idCompra = p.idCompra and p.codigoProducto = pt.codigoProducto and c.estado = 1 and c.idCompra = '$id'";

$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();

$sql = "select distinct pr.nit, pr.nombreProve, c.fecha
from Proveedor pr, Compra c, Pedido p, Producto pt
where pr.idProveedor = c.idProveedor and c.idCompra = p.idCompra and p.codigoProducto = pt.codigoProducto and c.estado = 1 and c.idCompra = '$id'";

$res1 = Database::getInstance()->getDb()->prepare($sql);
$res1->execute();
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                VER DE PEDIDO
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
        <div class="main-container container-fluid">
            <!-- Page Body -->
            <div class="page-body">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="widget-body">
                            DETALLE DEL PEDIDO
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="widget-body ">
                                    <form action="VistaImpPedido.php" id="vistadetalle" name="vistadetalle" method="post" class="form-horizontal">
                                       <div class="form-inline">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <div class="form-group">
                                                <?php $prov = $res1->fetch() ?>
                                                <label ><strong>Proveedor: </strong></label> 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="text" id="provee" name="provee" value="<?= $prov['nombreProve'] ?>" readonly="readonly"/>

                                            </div>
                                            <br>
                                            <br>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <div class="form-group">
                                                <label ><strong>NIT: </strong></label>
                                            </div>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <div class="form-group">
                                                <input class="form-control" type="text" id="nitp" name="nitp" value="<?= $prov['nit'] ?>" readonly="readonly"/>
                                            </div>
                                            <br>
                                            <br>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <div class="form-group">
                                                <label ><strong>Fecha: </strong></label>
                                            </div>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                            <div class="form-group">
                                                <input class="form-control" type="text" id="nitp" name="nitp" value="<?= $prov['fecha'] ?>" readonly="readonly"/><br><br>

                                            </div>
                                            
                                        </div>
                                    <!-- TABLA DE NOTA DE VENTA-->

                                    <table id="grilla" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Nombre de producto</th>
                                                <th>Cant. Pedida</th>
                                                <th>Precio costo</th>
                                                <th>Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while ($row = $resultado->fetch()) {
                                                $nro++;
                                                ?>
                                                <tr>
                                        <input class="" size="50" type="text" id="cmp" name="cmp" value="<?= $row['idCompra'] ?> " hidden/>
                                                    <td class="">
                                                        <input class="" size="50" type="text" class="text-right" id="nombreComer<?= $nro ?>" name="nombreComer<?= $nro ?>" value="<?= $row['nombreComercial'] ?> " readonly="readonly" />
                                                    </td>
                                                    <td class="text-center">
                                                        <input size="10" type="text" class="text-right" id="cantidad<?= $nro ?>" name="cantidad<?= $nro ?>" value="<?= $row['cantidad'] ?>" readonly="readonly"/>
                                                    </td>
                                                    <td class="text-center">
                                                        <input size="10" type="text" class="text-right" id="costo<?= $nro ?>" name="costo<?= $nro ?>" value="<?= $row['precioCosto'] ?> " readonly="readonly"/>
                                                    </td>
                                                    <td class="text-center">
                                                        <input size="10" type="text" id="subt<?= $nro ?>" class="text-right" name="subt<?= $nro ?>" value="<?= $row['precioCosto'] * $row['cantidad'] ?> " readonly="readonly"/>
                                                    </td>
                                                </tr>
                                                <?php
                                                
                                                $subtotal = $row['montoSubTotal'];
                                                $total = $row['montoTotal'];
                                                $desc = $row['descuento'];
                                            }
                                            $cant = $nro;
                                            ?>
                                        </tbody>
                                        <tfoot>
                                        <input type="text" id="cantFilas" name="cantFilas" value="<?= $cant ?>" name="subTotal" size="8" hidden/>
                                        <input type="text" id="opcion" name="opcion" value="ver" name="subTotal" size="8" hidden/>
                                        <tr>
                                            <td colspan="2" class="invisible bg-snow"></td>
                                            <td class="text-center" ><strong>Sub Total</strong></td>
                                            <td class="text-center"><input type="text" class="text-right" id="subTotal" value="<?= $subtotal ?>" name="subTotal" size="8" readonly="readonly"/> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="invisible bg-snow"></td>
                                            <td class="text-center" ><strong>Descuento Bs</strong></td>
                                            <td class="text-center"><input type="text" class="text-right" id="descuentoB" value="<?= $desc ?>" name="descuentoB" size="8" readonly="readonly"/></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="invisible bg-snow"></td>
                                            <td class="text-center"><strong>Total</strong></td>
                                            <td class="text-center "><strong><input type="text" class="text-right" id="total" value="<?= $total ?>" name="total" size="8" readonly="readonly"/></strong></td>
                                        </tr>




                                        </tfoot>
                                    </table>  
                                    <div class="form-inline">
                                        <script>
                                            function volver() {
                                                location.href = "../pedidos";
                                            }
                                        </script>
                                        <input type="button" class="btn btn-success" name="imprimirP" id="imprimirP"  value= "Imprimir" onclick="vistadetalle.submit()" placeholder=""/>
                                        <input type="button" class="btn btn-primary" name="" id="" onclick="volver()" value= "Aceptar" placeholder=""/>                                                


                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php
require_once ('../header_footer/footer.php');
?>