<?php require_once 'head.php'; ?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                ORDEN DE PEDIDO
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget-body ">
                                <form action=javascript:ListarProductosProv() id="nuevo_pedido" class="form-horizontal">
                                    <br>
                                    <br>
                                    <div class="form-group">

                                        <label class="col-lg-2 control-label"><strong>NIT</strong></label>
                                        <div class="col-lg-3">

                                            <input id="buscar_nit" type="text" required  class="form-control" name="buscar_nit" placeholder="Todos"/>
                                            <script>
                                                $('#buscar_nit').autocomplete({
                                                    source: function (request, response) {
                                                        $.ajax({
                                                            url: 'autocompletado.php',
                                                            dataType: "json",
                                                            data: {
                                                                name_startsWith: request.term,
                                                                type: 'Nit'
                                                            },
                                                            success: function (data) {
                                                                response($.map(data, function (item, nombre) {
                                                                    return {
                                                                        label: nombre,
                                                                        value: nombre,
                                                                        id: item

                                                                    };
                                                                }));
                                                            }
                                                        });
                                                    },
                                                    select: function (event, ui) {
                                                        $('#nombre_prov').val(ui.item.id);
                                                    }
                                                });
                                            </script>

                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-lg-2 control-label"><strong>Proveedor</strong></label>
                                        <div class="col-lg-3">
                                            <input type="text" class="form-control" required name="nombre_prov" id="nombre_prov"  placeholder="Todos"/>
                                            <script>
                                                $('#nombre_prov').autocomplete({
                                                    source: function (request, response) {
                                                        $.ajax({
                                                            url: 'autocompletado.php',
                                                            dataType: "json",
                                                            data: {
                                                                name_startsWith: request.term,
                                                                type: 'Proveedor'
                                                            },
                                                            success: function (data) {
                                                                response($.map(data, function (item, nit) {
                                                                    return {
                                                                        label: nit,
                                                                        value: nit,
                                                                        id: item

                                                                    };
                                                                }));
                                                            }
                                                        });
                                                    },
                                                    select: function (event, ui) {
                                                        $('#buscar_nit').val(ui.item.id);
                                                    }
                                                });
                                           
                                            </script>
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">

                                        <label class="col-lg-2 control-label"><strong>Pedido para dias:</strong></label>
                                        <div class="col-lg-3">

                                            <select type="text" class="form-control" name="dias" id="dias">
                                                <option value="1">1 mes</option>
                                                <option value="2">2 meses</option>
                                                <option value="3">3 meses</option>
                                                <option value="4">4 meses</option>
                                                <option value="5">5 meses</option>
                                                <option value="6">6 meses</option>

                                            </select>

                                        </div>

                                        <label class="col-lg-2 control-label"><strong>Por ventas de los meses:</strong></label>
                                        <div class="col-lg-3">

                                            <select type="text" class="form-control" name="meses" id="meses">
                                                <option value="1">1 anterior</option>
                                                <option value="2">2 anterior</option>
                                                <option value="3">3 anterior</option>
                                                <option value="4">4 anterior</option>
                                                <option value="5">5 anterior</option>
                                                <option value="6">6 anterior</option>
                                                <option value="7">7 anterior</option>
                                                <option value="8">8 anterior</option>
                                                <option value="9">9 anterior</option>
                                                <option value="10">10 anterior</option>
                                                <option value="11">11 anterior</option>
                                                <option value="12">12 anterior</option>
                                            </select>

                                        </div>
                                        <div class="col-lg-2">
                                            <input type="submit"  class="btn btn-yellow" name="Buscar" id="Buscar" value="Buscar" />

                                        </div>

                                    </div>


                                </form>

                            </div>
                            <br>
                            <div id="agrega-registros" name="agrega-registros" class="widget-body ">
                                <form method="post" class="form-horizontal">
                                    <!-- TABLA DE NOTA DE VENTA-->
                                    <table id="grilla" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th><strong>Nro</strong></th>
                                                <th><strong>Codigo</strong></th>
                                                <th><strong>Nombre de Producto</strong></th>
                                                <th><strong>Cant. Vendida</strong></th>
                                                <th><strong>Stock</strong></th>
                                                <th><strong>Precio Costo Anterior</strong></th>
                                                <th><strong>Precio Costo</strong></th>
                                                <th><strong>Cantidad Sugerida</strong></th>
                                                <th><strong>SubTotal</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!--SE AGREGAN LAS FILAS DE LOS PRODUCTOS POR VENDER-->
                                        </tbody>
                                        <tfoot>

                                            <tr>
                                                <td colspan="7" class="invisible bg-snow"></td>
                                                <td ><strong>Sub Total</strong></td>
                                                <td class="text-center"><input type="text" id="subTotal" name="subTotal" size="8" onkeyup="montSubtotal()" readonly/> </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" class="invisible bg-snow"></td>
                                                <td ><strong>Descuento %</strong></td>
                                                <td class="text-center"><input type="text" id="descuentoP" name="descuentoP" onkeyup="DescuentoP()" size="8" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" class="invisible bg-snow"></td>
                                                <td ><strong>Descuento Bs</strong></td>
                                                <td class="text-center"><input type="text" id="descuentoB" name="descuentoB"  onkeyup="DescuentoB()"size="8"/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" class="invisible bg-snow"></td>
                                                <td class="text-center"><strong>Total</strong></td>
                                                <td class="text-center "><strong><input type="text" id="total" name="total" size="8" readonly/></strong></td>
                                            </tr>

                                            


                                        </tfoot>
                                    </table>  
                                    <div class="form-inline">

                                        <input type="button" class="btn btn-success" name="" id=""  value= "Imprimir" placeholder=""/>
                                        <input type="button" class="btn btn-primary" name="" id=""  value= "Guardar" placeholder=""/>
                                        <input type="button" class="btn btn-danger" name="" id=""  value= "Cancelar" placeholder=""/>                                                


                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
require_once ('../header_footer/footer.php');
?> 