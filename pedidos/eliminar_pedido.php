<?php
require_once("../datos/Database.php");
$id = $_POST['id'];


//ELIMINAMOS EL PEDIDO


$elimina = Database::getInstance()->getDb()->prepare("UPDATE compra SET estado = 0 WHERE idCompra = '$id'");
$elimina->execute();

//ACTUALIZAMOS LOS REGISTROS Y LOS OBTENEMOS
$dato = Database::getInstance()->getDb()->prepare("select distinct c.idCompra, pp.nit, pp.Proveedor, c.fecha, c.montoTotal "
        . "from Pedido p, Compra c, Proveedor pr, PedidoProducto pp "
        . "where p.idCompra = c.idCompra and c.idProveedor = pr.idProveedor and pr.nit = pp.nit and c.estado = 1");

//CREAMOS NUESTRA VISTA Y LA DEVOLVEMOS AL AJAX
echo '<table class="table table-striped table-bordered table-hover" >
                                            <thead>

                                                <tr>
                                                    <th class="hidden-xs">
                                                        ID
                                                    </th>
                                                    <th class="hidden-xs">
                                                        NIT
                                                    </th>
                                                    <th class="hidden-xs">
                                                        PROVEEDOR
                                                    </th>
                                                    <th class="hidden-xs">
                                                        FECHA
                                                    </th>
                                                    <th class="hidden-xs">
                                                        MONTO TOTAL
                                                    </th>
                                                    <th class="hidden-xs">
                                                       
                                                    </th>

                                                </tr>
                                            </thead>';

$dato->execute();
if ($dato->rowCount() > 0) {
    while ($registro2 = $dato->fetch()) {
        echo '<tr>
				<td>' . $registro2['idCompra'] . '</td>
				<td>' . $registro2['nit'] . '</td>
				<td>' . $registro2['Proveedor'] . '</td>
				<td>' . $registro2['fecha'] . '</td>
				<td>' . $registro2['montoTotal'] . '</td>
                                <td>
                                                       
                                  <a href="ver_pedido.php?id=<?= $id ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i>Ver</a>
                                  <a href="modificar_pedidos.php?id=<?= $id ?>"  class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                                 <a href="javascript:eliminar(<?= $id ?>)" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                                 </td>
				</tr>';
    }
} else {
echo '<tr>
    <td colspan="6">No se encontraron resultados</td>
</tr>';
}
echo '</table>';
?>