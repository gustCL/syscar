<?php 
require_once("../datos/Database.php");
session_start();
$id_usu = $_SESSION['userMaster'];
$idUser = $id_usu['idUsuario'];
$idSesion = $_SESSION['idSesion'];
//fecha actual
ini_set("date.timezone", "America/La_Paz");
$fechA = date("Y-m-d");
$horA = date("H:i:s");
$total = $_POST['total'];
$descuentoBs = $_POST['descuentoB'];
$cant = $_POST['cant'];
$subt = $_POST['subtotal'];
$idSucur = $id_usu['idSucursal'];

//SACAR EL ID DEL PROVEEDOR
$nit = $_POST['nitproveedor'];
$proveedor = $_POST['nombreproveedor'];
    $consulta = "select idProveedor from Proveedor where nit = '$nit' and nombreProve like '".$proveedor."%'";
    $comando =Database::getInstance()->getDb()->prepare($consulta);
    $comando->execute();
    
    $row = $comando->fetch();
    $idProveedor = $row['idProveedor'];

    ///// EXTRAER EL ULTIMO PEDIDO EN COMPRA QUE SE INSERTO 
    $consultar = "select count(*)as id from Compra";
    $comando =Database::getInstance()->getDb()->prepare($consultar);
    $comando->execute();
    $row = $comando->fetch();
    $idCompra = $row['id'];
    
//CONSULTAR LOS TIPOS DE CAMBIOS
    $consultar = "select MAX(IdCompra), TipoCambioVenta, TipoCambioCompra from compra";
    $comando =Database::getInstance()->getDb()->prepare($consultar);
    $comando->execute();
    
    $row = $comando->fetch();
    $CambioVenta = $row['TipoCambioVenta'];
    $CambioCompra = $row['TipoCambioCompra'];
////////////////////////////////////////////////////////  
    //INSERTAR LOS REGISTROS DE PEDIDO EN COMPRAS
    $consulta = "insert into Compra (idCompra,fecha,hora,montoSubTotal,montoTotal,descuento,idProveedor,idUsuario,idInicioSesion,idSucursal,tipoCambioVenta,tipoCambioCompra,estado) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $comando =Database::getInstance()->getDb()->prepare($consulta);
    $comando->execute([0,$fechA,$horA,$subt,$total,$descuentoBs,$idProveedor,$idUser,$idSesion,$idSucur,$CambioVenta,$CambioCompra,1]);
    
    ///// EXTRAER EL ULTIMO PEDIDO EN COMPRA QUE SE INSERTO 
    $consultar = "select count(*)as id from Compra";
    $comando =Database::getInstance()->getDb()->prepare($consultar);
    $comando->execute();
    
    $row = $comando->fetch();
    $idCompra = $row['id'];
    
 ////////////////////////////////////////////
for ($index = 1; $index <= $cant; $index++) {
$idprod = $_POST['codProducto'.$index];
$cantidad = $_POST['cantidad'.$index];
$costo = $_POST['costo'.$index];
   
//INSETAR LOS REGISTROS EN LA TABLA PEDIDOS
    $consulta = "insert into Pedido (idCompra,codigoProducto,cantidad,precioCosto) values (?,?,?,?)";
    $comando =Database::getInstance()->getDb()->prepare($consulta);
    $comando->execute([$idCompra,$idprod,$cantidad,$costo]);   
    
}

header('Location: ../pedidos/index.php');

?>