<?php
require_once ('head.php');
require_once '../datos/Database.php';
$contador = 1;
?>
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative" > 
        <div class="header-title" >

            <h1>
                <Strong>PROMOCION DESCUENTO</strong>
            </h1> 

        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="widget" >
                            <div class="widget-body">
                                <br>
                                <center>
                                    <div class="form-inline">

                                        <div class="input-group"> 
                                            <div class="col-lg-3">
                                                <span class="input-icon">
                                                    <input id="bs-credito" name="bs-credito" type="text" size="70" class="form-control"  />
                                                    <i class="glyphicon glyphicon-search circular blue"></i>
                                                </span>

                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="input-group">                                                
                                            <a onclick="nuevaPromoDes();" class="btn btn-warning shiny">
                                                <i class="fa fa-plus">
                                                </i> Nueva Promo-Descuento
                                            </a>
                                        </div>
                                    </div>

                                </center>
                                <br>
                            </div>
                            <br />
                            <div class="widget-body">
                                <center><h3><strong>LISTA PROMOCION-DESCUENTO</strong></h3></center>
                                <br/>
                                <!-- TABLA DE SECTORES-->
                                <div class="registros" id="agrega-registros">                    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Numero
                                                </th>
                                                <th>
                                                    Nombre 
                                                </th>
                                                <th>
                                                    Descuento
                                                </th>
                                                <th>
                                                    Fecha de registro
                                                </th>
                                                <th>
                                                    Fecha Limite
                                                </th>
                                                <th>
                                                    Estado
                                                </th>
                                                <th>

                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            ini_set("date.timezone", "America/La_Paz");
                                            $fechaSistema = date('d-m-Y'); //fecha actual del sistemaAND horaFin>='$horaSistema'
                                            $fechaSistema = strtotime($fechaSistema); 
                                            $horaSistema = date('H:i:s'); //hora actual del sistema  AND horaFin>='$horaSistema' AND fechaFin>='$fechaSistema'
                                            $registro = "SELECT * FROM PromocionDescuento WHERE idPromoDes != 1";
                                            // WHERE  ('$fechaSistema' < fechaFin) AND ('$horaSistema'< horaFin)          // para poder recuperar por fechas
                                            try {
                                                $comando = Database::getInstance()->getDb()->prepare($registro);
                                                $comando->execute();


                                                while ($row = $comando->fetch()) {
                                                    $idS = $row['idPromoDes'];
                                                    $estado = $row['estado'];
                                                    $fw =  date($row['fechaFin']);
                                                    $fw = strtotime($fw);
                                                    if($fw >= $fechaSistema) {
                                                        ?><tr>
                                                            <td id="idCredito">
                                                                <?= $idS; ?>
                                                                <input id="cont<?= $contador ?>" value="<?= $contador ?>" hidden/> 
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <?= $row['nombrePromo'] ?>
                                                            </td>
                                                            <td>
                                                                <?= $row['descuentoPromo'] * 100 . " %" ?>
                                                            </td>
                                                            <td>
                                                                <?= $row['fechaRegistro'] ?>
                                                            </td>
                                                            <td>
                                                                <?= $row['fechaFin'] ?>
                                                            </td>
                                                            <td>
                                                                <input style="border: none;background:inherit " type="text" class="form-control" name="estado" id="estado<?= $contador ?>" value="<?= estadoString($estado) ?>" readonly/>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:verPromoDesc('<?= $idS ?>');"  class="btn btn-default btn-xs blue" >
                                                                    <i class="fa fa-eye"></i> Ver
                                                                </a>
                                                                <script type="text/javascript">
                                                                    $(document).ready(function() {
                                                                        var cont = $('#cont' + <?= $contador ?>).val();
                                                                        var estado = $('#estado' + <?= $contador ?>).val();
                                                                        //alert(estado);
                                                                        if (estado == "Activo") {
                                                                            $("#activar" + cont).hide();
                                                                            $("#desactivar" + cont).show();

                                                                        } else {

                                                                            $("#desactivar" + cont).hide();
                                                                            $("#activar" + cont).show();
                                                                        }
                                                                    });
                                                                </script>
                                                                <input onclick="activarPromoDes('<?= $idS ?>');" type="button"  id="activar<?= $contador ?>" value="Activar" class="btn-success "/>
                                                                <input onclick="desactivarPromoDes('<?= $idS ?>');" type="button"   id="desactivar<?= $contador ?>" value="Desactivar" class="input-s-sm btn-danger"/>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $contador = $contador + 1;
                                                    }
                                                }//terminacion del while
                                            } catch (PDOException $e) {
                                                echo 'Error: ' . $e;
                                            }
                                            ?>    
                                        </tbody>
                                    </table>
                                </div>    
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--     D I A L O G   M O D A L ##V E R##   -->
<div class="modal fade" id="formulario_ver"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Promocion Descuento</h4>
            </div>
            <form id="formularioVer" class="form-horizontal" role="form" >
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <label hidden="true"></label>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="nombrepromo">Nombre Promocion</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="nombrepromo" name="nombrepromo" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="descuentopromo">Descuento</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="descuentopromo" name="descuentopromo" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="fechainiciopromo">Fecha inicio</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="fechainiciopromo" name="fechainiciopromo" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="fechafinpromo">Fecha fin</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="fechafinpromo" name="fechafinpromo" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="horainiciopromo">Hora inicio</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="horainiciopromo" name="horainiciopromo" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="horafinpromo">Hora fin</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="horafinpromo" name="horafinpromo" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="estadopromo">Estado</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="estadopromo" name="estadopromo" readonly="true">
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <center>
                                <div class="form-group">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                </div>  
                            </center>
                        </div>  
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- MODAL PARA NUEVA PROMO DESCUENTO-->
<div class="modal fade" id="modal_nuevaPromoDes"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel"><strong>Promocion Descuento</strong></h4>
            </div>
            <form id="formularioNuevaPromoDes" class="form-horizontal" role="form" >
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="nombrepromonvo">Nombre Promocion</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="nombrepromonvo" name="nombrepromonvo" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="descuentopromonvo">Descuento</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="descuentopromonvo" name="descuentopromonvo" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="fechainiciopromonvo">Fecha inicio</label>
                                <div class="col-sm-4"> 
                                    <input  id="fechainiciopromonvo" name="fechainiciopromonvo" type="date" placeholder="Fecha"   required />
                                </div>
                                <label class="col-sm-2 control-label no-padding-right" for="fechafinpromonvo">Fecha fin</label>
                                <div class="col-sm-4">
                                    <input s id="fechafinpromonvo" name="fechafinpromonvo" type="date" placeholder="Fecha"   required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="horainiciopromnvo">Hora inicio</label>
                                <div class="col-sm-4"> 
                                    <input  type="time" class="form-control"  id="horainiciopromnvo" name="horainiciopromnvo" >
                                </div>
                                <label class="col-sm-2 control-label no-padding-right" for="horafinpromonvo">Hora fin</label>
                                <div class="col-sm-4"> 
                                    <input  type="time" class="form-control"  id="horafinpromonvo" name="horafinpromonvo" >
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-inline">                                
                                <center>
                                    <button onclick="return guardarNuevaPromoDes();" id="btn-guardar-linea" type="button" class="btn btn-info shiny" data-dismiss="modal" aria-hidden="true">Guardar</button>
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                </center>
                            </div>
                        </div>  
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
include_once '../header_footer/footer.php';
?>
<?php

function estadoString($estado) {
    $cadena = "";
    if ($estado == 1) {
        $cadena = "Activo";
    } else if ($estado == 0) {
        $cadena = "Inactivo";
    }
    return $cadena;
}

function actualizarPromoDefault($id) {//funcion q habilita 0% de descuento
    $consultaP = "UPDATE PromocionDescuento SET estado = 0 WHERE idPromoDes ='$id'";
    $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
    $comandoP->execute();
    $consultaP = "UPDATE PromocionDescuento SET estado= 1 WHERE idPromoDes =1";
    $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
    $comandoP->execute();
}
?>