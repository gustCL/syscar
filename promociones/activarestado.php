<?php
require_once '../datos/Database.php';
$id = $_POST['id'];

$consultaP = "UPDATE PromocionDescuento SET estado=1 WHERE idPromoDes='$id'";
$comandoP = Database::getInstance()->getDb()->prepare($consultaP);
$comandoP->execute();
$consultaP = "UPDATE PromocionDescuento SET estado=0 WHERE idPromoDes!='$id'";
$comandoP = Database::getInstance()->getDb()->prepare($consultaP);
$comandoP->execute();
$contador = 1;
?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>
                Numero
            </th>
            <th>
                Nombre 
            </th>
            <th>
                Descuento
            </th>
            <th>
                Fecha de registro
            </th>
            <th>
                Fecha Limite
            </th>
            <th>
                Estado
            </th>
            <th>

            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        ini_set("date.timezone", "America/La_Paz");
        $fechaSistema = date('d-m-Y'); //fecha actual del sistemaAND horaFin>='$horaSistema'
        $fechaSistema = strtotime($fechaSistema);
        $horaSistema = date('H:i:s'); //hora actual del sistema  AND horaFin>='$horaSistema' AND fechaFin>='$fechaSistema'
        $registro = "SELECT * FROM PromocionDescuento WHERE idPromoDes != 1";
        // WHERE  ('$fechaSistema' < fechaFin) AND ('$horaSistema'< horaFin)          // para poder recuperar por fechas
        try {
            $comando = Database::getInstance()->getDb()->prepare($registro);
            $comando->execute();


            while ($row = $comando->fetch()) {
                $idS = $row['idPromoDes'];
                $estado = $row['estado'];
                $fw = date($row['fechaFin']);
                $fw = strtotime($fw);
                if ($fw >= $fechaSistema) {
                    ?><tr>
                        <td id="idCredito">
                            <?= $idS; ?>
                            <input id="cont<?= $contador ?>" value="<?= $contador ?>" hidden/> 
                        </td>
                        <td class="hidden-xs">
                            <?= $row['nombrePromo'] ?>
                        </td>
                        <td>
                            <?= $row['descuentoPromo'] * 100 . " %" ?>
                        </td>
                        <td>
                            <?= $row['fechaRegistro'] ?>
                        </td>
                        <td>
                            <?= $row['fechaFin'] ?>
                        </td>
                        <td>
                            <input style="border: none;background:inherit " type="text" class="form-control" name="estado" id="estado<?= $contador ?>" value="<?= estadoString($estado) ?>" readonly/>
                        </td>
                        <td>
                            <a href="javascript:verPromoDesc('<?= $idS ?>');"  class="btn btn-default btn-xs blue" >
                                <i class="fa fa-eye"></i> Ver
                            </a>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    var cont = $('#cont' + <?= $contador ?>).val();
                                    var estado = $('#estado' + <?= $contador ?>).val();
                                    //alert(estado);
                                    if (estado == "Activo") {
                                        $("#activar" + cont).hide();
                                        $("#desactivar" + cont).show();

                                    } else {

                                        $("#desactivar" + cont).hide();
                                        $("#activar" + cont).show();
                                    }
                                });
                            </script>
                            <input onclick="activarPromoDes('<?= $idS ?>');" type="button"  id="activar<?= $contador ?>" value="Activar" class="btn-success "/>
                            <input onclick="desactivarPromoDes('<?= $idS ?>');" type="button"   id="desactivar<?= $contador ?>" value="Desactivar" class="input-s-sm btn-danger"/>
                        </td>
                    </tr>
                    <?php
                    $contador = $contador + 1;
                }
            }//terminacion del while
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
        ?>    
    </tbody>
</table>
<?php

function estadoString($estado) {
    $cadena = "";
    if ($estado == 1) {
        $cadena = "Activo";
    } else if ($estado == 0) {
        $cadena = "Inactivo";
    }
    return $cadena;
}
?>