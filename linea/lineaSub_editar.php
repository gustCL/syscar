<?php
require_once ("../datos/Database.php");

$idPadreEdit = $_POST['idPadreEdit'];

$idLin = $_POST['editsublinid'];
$codLin = $_POST['editsublincodigo'];
$nombreLin = $_POST['editsublinnombre'];
$padreLin = $_POST['editsublin_lin'];

if ($padreLin != 0) {
    $consultaP = "UPDATE Linea SET codLinea='$codLin', nombreLinea='$nombreLin',"
            . "idLineaPadre='$padreLin' WHERE idLinea = '$idLin'";
    $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
    $comandoP->execute();
    $nrofila = 1;
}
$query_getlista_sublinea = "SELECT * FROM Linea WHERE idLineaPadre = $idPadreEdit AND estado = 1 ORDER BY idLinea DESC";
$cmd_getlista_sublinea = Database::getInstance()->getDb()->prepare($query_getlista_sublinea);
$cmd_getlista_sublinea->execute();
?>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <!--Fila de titulos de columnas-->
        <tr>
            <th class="hidden-xs">
                Nro
            </th>

            <th class="hidden-xs">
                Codigo SubLinea
            </th>
            <th class="hidden-xs">
                Nombre Sublinea
            </th>

            <th>
                subLinea
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        try {
            //Obtenemos todas las sub lineas que pertenecen a la Linea Elegi
            while ($row = $cmd_getlista_sublinea->fetch()) {
                $idLinea = $row['idLinea'];
                ?>
                <tr>
                    <td>
                        <?= $nrofila ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['codLinea'] ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['nombreLinea'] ?>
                    </td>

                    <td>
                        <a href="javascript:verSubLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs blue">
                            <i class="fa fa-eye"></i> Ver
                        </a>
                        <a  href="javascript:editarSubLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs purple">
                            <i class="fa fa-edit"></i> Editar
                        </a>
                        <a href="javascript:eliminarSubLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs black">
                            <i  class="fa fa-trash-o"></i> Eliminar
                        </a>

                    </td>
                </tr>
                <?php
                $nrofila = $nrofila + 1;
            }//terminacion del while
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
        ?>

    </tbody>
</table>