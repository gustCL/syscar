<?php
    require_once '../datos/conexion.php';
    require_once '../extras/PHPPaging.lib/PHPPaging.lib.php';

    session_start();
    $id_sucursal = $_SESSION['userMaster']['idSucursal'];
    if (isset($_GET['indicio'])) {
        $indicio = $_GET['indicio'];
        $query_lineas = "SELECT *
                         FROM Linea
                         WHERE estado = 1 AND
                               idLineaPadre = 0 AND
                               (codLinea LIKE '$indicio%' OR nombreLinea LIKE '$indicio%')
                         ORDER BY idLinea DESC ";
    } else {
        $query_lineas = "SELECT * FROM Linea WHERE estado = 1 ORDER BY idLinea DESC";
    }
    $pagina = new PHPPaging();
?>

<table class="table table-striped">
    <thead>
        <tr>
            <th>Nro</th>
            <th>Código Línea</th>
            <th>Nombre Línea</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $pagina->agregarConsulta($query_lineas);
        $pagina->modo('desarrollo');
        $pagina->verPost(true);
        $pagina->porPagina(10);
        $pagina->paginasAntes(5);
        $pagina->paginasDespues(5);
        $pagina->linkSeparador(" - ");
        $pagina->div('agrega-registroLineas');
        $pagina->linkSeparadorEspecial('...');
        $pagina->ejecutar();

        $row_number = 0;
        while ($res = $pagina->fetchResultado()) :
            $row_number++;
            $idLinea = $res["idLinea"];
            $codigoLinea = $res["codLinea"];
            $nombreLinea = $res["nombreLinea"];
            ?>
            <tr>
                <td><?= $row_number; ?></td>
                <td><?= $codigoLinea; ?></td>
                <td><?= $nombreLinea; ?></td>
                <td>
                    <a href="javascript:verLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs blue">
                        <i class="fa fa-eye"></i> Ver
                    </a>
                    <a href="javascript:editarLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs purple" >
                        <i class="fa fa-edit"></i> Editar
                    </a>
                    <a href="javascript:eliminarLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs black">
                        <i class="fa fa-trash-o"></i> Eliminar
                    </a>
                    <a href="javascript:redireccion('<?= $idLinea ?>');" class="btn btn-default btn-xs ">
                        <i class="fa fa-archive"></i> ver Sublineas
                    </a>
                </td>
            </tr>  
        </tbody>
        <?php endwhile; ?>
    <tfoot>
        <tr>    
            <td colspan="7">Número de página: <?= $pagina->fetchNavegacion(); ?></td>
        </tr>
    </tfoot>
</table>
