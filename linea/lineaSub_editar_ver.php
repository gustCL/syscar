<?php
require_once '../datos/Database.php';

$id = trim($_POST['id']);


$query_getlinea = "SELECT * 
            FROM Linea 
            WHERE idLinea='$id'";

$cmd_getlinea = Database::getInstance()->getDb()->prepare($query_getlinea);
$cmd_getlinea->execute();
$datos_sublinea = $cmd_getlinea->fetch();


$query_lineas_padres = "SELECT * FROM Linea WHERE estado=1 AND idLineaPadre = 0";
$cmd_lineas_padres = Database::getInstance()->getDb()->prepare($query_lineas_padres);
$cmd_lineas_padres->execute();
?>

<div class="form-inline">
    <div class="form-group">
        ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <!--Envio por post el id del padre-->
        <input type="text" class="form-control "  id="editsublinid" name="editsublinid" value="<?= $datos_sublinea['idLinea'] ?>" readonly="true">
    </div>
</div>
<br/>
<div class="form-inline">
    <div class="form-group">
        Codigo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <input type="text" class="form-control"  id="editsublincodigo" name="editsublincodigo" value="<?= $datos_sublinea['codLinea'] ?>">
    </div>
</div>
<br/>
<div class="form-inline">
    <div class="form-group">
        Nombre &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <input type="text" class="form-control"  id="editsublinnombre" name="editsublinnombre" value="<?= $datos_sublinea['nombreLinea'] ?>">
    </div>
</div>
<br/>
<div class="form-inline">
    <div class="form-group">
        Linea &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <select name="editsublin_lin" id="altipo_editar" style="width:100%;">
            <?php
            try {
                while ($row_lineas_padres = $cmd_lineas_padres->fetch()) {
                    $selected = '';
                    if ($row_lineas_padres['idLinea'] == $id) {
                        $selected = 'Selected';
                    }
                    ?>  
                    <option value="<?= $row_lineas_padres['idLinea'] ?>" <?php echo $selected?> >
                        <?= $row_lineas_padres['nombreLinea'] ?>
                    </option>
                    <?php
                }
            } catch (PDOException $e) {
                echo 'Error: ' . $e;
            }
            ?>
        </select>
    </div>                           
</div>
</div>

<br />             
<!--BOTONES-->
<br />            

<div class="form-group">
    <button class="btn btn-primary" onclick="return actualizarSubLinea()" data-dismiss="modal" aria-hidden="true">Guardar</button>&nbsp;&nbsp;&nbsp;
    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancelar</button>
</div>  






<?php

function mostrarLista() {


    try {
        while ($row_lineas_padres = $cmd_lineas_padres->fetch()) {
            $selected = '';
            if ($row_lineas_padres['idLinea'] == $id) {
                $selected = 'Selected';
            }
            ?>  
            <option value="<?= $row_lineas_padres['idLinea'] ?>" '<?= $selected ?>'>
                <?= $row_lineas_padres['nombreLinea'] ?>
            </option>
            <?php
        }
    } catch (PDOException $e) {
        echo 'Error: ' . $e;
    }
}
?>
