<?php
session_start();
if ($_SESSION['logueado'] != 'SI') {
    header('Location: ../inicio');
}
require_once '../datos/Database.php';
require_once ("../datos/conexion.php");
require_once("../extras/PHPPaging.lib/PHPPaging.lib.php");

$pagina = new PHPPaging;
$ggg = "";;
if($_POST['dato']!=null){
    $dato = $_POST['dato'];
    $ggg = $dato;
}
$dato = $ggg;
$auxSuc = $_SESSION['userMaster'];
$auxidsucursal = $auxSuc['idSucursal'];
$query_listabuscaqueda = "SELECT * FROM Linea WHERE idLineaPadre = 0 AND  estado = 1 AND (codLinea LIKE '$dato%' OR nombreLinea LIKE '$dato%') ORDER BY idLinea DESC";
$row_number = 1;
?><table class="table table-striped">
    <thead>
        <!--Fila de titulos de columnas-->
        <tr>
            <th class="hidden-xs">
                Nro
            </th>
            <th class="hidden-xs">
                Codigo Linea
            </th>
            <th class="hidden-xs">
                Nombre Linea
            </th>
            <th>  
            </th>

        </tr>
    </thead>
    <tbody>
        <?php
        $pagina->agregarConsulta($query_listabuscaqueda);

        $pagina->modo('desarrollo');
        $pagina->verPost(true);
        $pagina->porPagina(10);
        $pagina->paginasAntes(5);
        $pagina->paginasDespues(5);
        $pagina->linkSeparador(" - "); //Significa que no habrá separacion
        $pagina->div('agrega-registroLineas');
        $pagina->linkSeparadorEspecial('...');   // Separador especial 
        $pagina->ejecutar();

        while ($res = $pagina->fetchResultado()) {
            $idLinea = $res["idLinea"];
            $codigoLinea = $res["codLinea"];
            $nombreLinea = $res["nombreLinea"];
            ?>

            <tr>
                <td>
                    <?= $row_number ?>
                </td>
                <td class="hidden-xs">
                    <?= $codigoLinea ?>
                </td>
                <td class="hidden-xs">
                    <?= $nombreLinea ?>
                </td>
                <td>
                    <a href="javascript:verLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs blue">
                        <i class="fa fa-eye"></i> Ver
                    </a>
                    <a href="javascript:editarLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs purple" >
                        <i class="fa fa-edit"></i> Editar
                    </a>
                    <a href="javascript:eliminarLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs black">
                        <i  class="fa fa-trash-o"></i> Eliminar
                    </a>
                    <a href="javascript:redirect_by_post('sub_linea.php',{id: <?= $idLinea ?>},false);" class="btn btn-default btn-xs ">
                        <i  class="fa fa-archive"></i> ver Sublineas
                    </a>
                </td>

            </tr>    
        </tbody>
        <?php
        $row_number++;
    }//terminacion del while
    ?>
    <tfoot>
        <tr>    
            <td colspan="7">Numero de pagina: <?php echo $pagina->fetchNavegacion(); ?></td>    
        </tr>
    </tfoot>
</table>