<?php
    require_once 'head.php';
    $nrofila = 1;
?>
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <!--Header Buttons-->
        <div class="header-title">
            <h1>
                <Strong>LINEAS</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="widget-body"><br>
                <form id="form_alm" name="form_linea">
                    <center>
                        <div class="form-inline">
                            <div class="input-group">
                                <div class="col-lg-3">
                                    <span class="input-icon">
                                        <input type="text" size="40" class="form-control"
                                               placeholder="Buscar Linea por : Codigo , Nombre "
                                               id="bs-linea" name="bs-linea">
                                        <i class="glyphicon glyphicon-search circular blue"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="input-group">
                                <a onclick="nuevaLinea()" class="btn btn-warning shiny">
                                    <i class="fa fa-plus">
                                    </i> Nueva Linea
                                </a>
                            </div>
                        </div>
                    </center>
                    <br>
                </form>
            </div>
            <br>
            <div class="widget-body">
                <center><h3><strong>LISTA DE LINEAS</strong></h3></center>
                <br>
                <div id="agrega-registroLineas" class="table-responsive">
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal VER Linea-->
<div class="modal fade" id="formulario_verlinea"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">    
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Linea</h4>
            </div>
            <form id="formularioverlinea" class="formulario">
                <div class="modal-body">

                    <center>
                        <div class="form-inline">
                            <div class="form-group">
                                ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control " id="linid" name="linid" readonly>
                            </div>
                        </div>
                        <br/>
                        <div class="form-inline">
                            <div class="form-group">
                                Código &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control " id="lincodigo" name="lincodigo"
                                       readonly="true">
                            </div>
                        </div>
                        <br/>
                        <div class="form-inline">
                            <div class="form-group">
                                Nombre &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="linnombre" name="linnombre" readonly>
                            </div>
                        </div>
                        <br/>
                        <center>
                            <div class="form-group">
                                <button data-bb-handler="cancel" type="button" class="btn btn-danger"
                                        data-dismiss="modal" aria-hidden="true">Salir
                                </button>
                            </div>
                        </center>
                    </center>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal para editar linea-->
<div class="modal fade" id="modal_Editarlinea"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Linea</h4>
            </div>
            <form id="formularioEditarlinea" name="formularioEditarlinea" class="formulario">
                <div class="modal-body">
                    <center>
                        <div class="form-inline">
                            <div class="form-group">
                                ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control " id="editlinid" name="editlinid"
                                       readonly>
                            </div>
                        </div>
                        <br>
                        <div class="form-inline">
                            <div class="form-group">
                                Codigo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="editlincodigo" name="editlincodigo">
                            </div>
                        </div>
                        <br>
                        <div class="form-inline">
                            <div class="form-group">
                                Nombre &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="editlinnombre" name="editlinnombre">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <button class="btn btn-primary" onclick="actualizarLinea();" data-dismiss="modal"
                                    aria-hidden="true">Guardar
                            </button>
                            &nbsp;&nbsp;&nbsp;
                            <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal"
                                    aria-hidden="true">Cancelar
                            </button>
                        </div>
                    </center>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal Para EDITAR Sub - Linea-->
<div class="modal fade" id="modal_EditarSublinea"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Sub Linea</h4>
            </div>
            <form id="formularioEditarlinea" name="formularioEditarlinea" class="formulario">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">ID</div>
                            <div class="form-group">
                                <input type="text" class="form-control " id="subLineaID" name="subLineaID"
                                       readonly>
                            </div>

                            <div class="form-group">Codigo</div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="subLineaCOD" name="subLineaCOD">
                            </div>

                            <div class="form-group">Nombre</div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="subLineaNAME" name="editlinnombre">
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-left" for="inputEmail3">Linea</label>
                                <div id="subLineaLINEA">

                                </div>
                            </div>
                            <br>
                            <!--BOTONES-->
                            <br/>
                            <center>
                                <div class="form-group">
                                    <button class="btn btn-primary" onclick="return actualizarSubLinea()"
                                            data-dismiss="modal" aria-hidden="true">Guardar
                                    </button>
                                    &nbsp;&nbsp;&nbsp;
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger"
                                            data-dismiss="modal" aria-hidden="true">Cancelar
                                    </button>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>



<!--MODAL NUEVA LINEA - SUBL-INEA-->
<div class="modal fade" id="modal_Nueva_Linea"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Nuevo Linea-SubLinea</h4>
            </div>
            <form id="formularioNuevaLinea" name="formularioNuevaLinea" class="formulario" >
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <center>
                                    L I N E A                                    
                                </center>
                            </div>
                            <div class="form-inline">
                                <center>
                                    <div class="form-group">
                                        Código Linea &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input  type="text" class="form-control" id="codigo_linea" name="codigo_linea" placeholder="cod. linea">
                                    </div>
                                </center>
                            </div>
                            <br>
                            <div class="form-inline">
                                <center>
                                    <div class="form-group">
                                        Nombre Línea &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input  type="text" class="form-control" id="nombre_linea" name="nombre_linea" placeholder="nombre linea">
                                    </div>
                                </center>
                            </div>
                            <br>
                            <div class="form-group">                                
                                <center>
                                    <button onclick="guardarNuevaLinea();" id="btn-guardar-linea" type="button" class="btn btn-info shiny" data-dismiss="modal" aria-hidden="true">Guardar</button>
                                </center>
                            </div>
                            <div class="form-group">
                                <label>
                                    <span class="text">Agregar Sub-Linea</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php

    function mostrarLista() {
        //$consulta = "SELECT * FROM privilegiosmodulo";
        $consulta = "select*from Lista where estado=1 and id=";
        require_once '../datos/Database.php';
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();
            while ($lstModulos = $comando->fetch()) {
                ?>
                <option value="<?= $lstModulos['idTipoAlmacen'] ?>"><?= $lstModulos['nombreTipo'] ?></option>
                <?php
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
    }
?>
<?php require_once '../header_footer/footer.php'; ?>