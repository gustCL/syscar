$(document).ready(function() {
    obtenerLineas();
});
$(function() {//*
//HEN Busqueda de Sub-Linea   :-D ->
    $('#bs-sublinea').on('keyup', function() {
        var dato = $('#bs-sublinea').val();
        var idPadre = $('#idPadrePP').val();
//        alert(idPadre);
        var url = 'lineaSub_busqueda.php';
        $.ajax({
            type: 'POST',
            url: url,
//            data: 'dato=' + dato,
            data: 'dato=' + dato + '&idPadre=' + idPadre,
            success: function(datos) {
                $('#agrega-registros-subLinea').html(datos);
            }
        });
        return false;
    });

    $('#bs-linea').on('keyup', function() {
        //obtenerLineas();
        var indicio = $('#bs-linea').val();
        $("#agrega-registroLineas").load('tabla_lineas.php?indicio='+indicio);
    });
});

function obtenerLineas() {
    $("#agrega-registroLineas").load('tabla_lineas.php');
}



function fn_paginar(var_div, url) {
    var div = $("#" + var_div);
    $(div).load(url);
}


//HEN  Funciona  Linea
function verLinea(id) {//*
    $('#formularioverlinea')[0].reset();
    var url = 'linea_ver.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(valores) {
            //  alert(valores)
            var datos = eval(valores);
            $('#linid').val(datos[0]);
            $('#lincodigo').val(datos[1]);
            $('#linnombre').val(datos[2]);
            $('#formulario_verlinea').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;
}
;

function eliminarLinea(id) {
    swal({
        title: "Eliminar Linea",
        text: "Está a Punto De Eliminar Los Registros De Esta Linea. ¿Desea continuar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, eliminar",
        cancelButtonText: "No, cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },  function(isConfirm) {
            if (isConfirm) {
                var url = 'linea_eliminar.php';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: 'id=' + id,
                    success: function() {
                        $('#agrega-registroLineas').load('tabla_lineas.php');
                        swal("Linea Eliminada!", "Linea removida de la base de datos.", "success");
                    }
                });
            }
        }
    );
}

function eliminarSubLinea(id) {
    swal({
        title: "Eliminar Sub-Linea",
        text: "Está a Punto De Eliminar Los Registros De Esta Sub-Linea. ¿Desea continuar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, eliminar",
        cancelButtonText: "No, cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },  function(isConfirm) {
            if (isConfirm) {
                var url = 'lineaSub_eliminar.php';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: 'id=' + id + '&idPadreElim=' + idPad,
                    success: function() {
                        swal("Sub-Linea Eliminada!", "Sub-Linea Ha Sido Removido", "success");
                        $('#agrega-registros-subLinea').load('obtener_sublinea.php');
                        return false;
                    }
                });
            }
        }
    );
}

function editarLinea(id) { //*
    $('#formularioEditarlinea')[0].reset();
    $.ajax({
        type: 'POST',
        url: 'linea_editar_ver.php',
        data: 'id=' + id,
        success: function(valores) {
            var datos = eval(valores);
            $('#editlinid').val(datos[0]);
            $('#editlincodigo').val(datos[1]);
            $('#editlinnombre').val(datos[2]);
            $('#modal_Editarlinea').modal({
                show: true,
                backdrop: 'static'
            });
        }
    });
}

function actualizarLinea() {
    var dato = $("#formularioEditarlinea").serialize();
    $.ajax({
        url: 'linea_editar.php',
        data: dato,
        type: 'post',
        success: function(registro) {
            $('#agrega-registroLineas').html(registro);
            swal("Edicion Correcta", "Linea Modificada Correctamente", "success");
            return false;
        }
    });
    return false;
}

function editarSubLinea(id) {
    $('#formularioEditarSublinea')[0].reset();
    var url = 'lineaSub_editar_ver.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(valores) {
            $('#modal_sub_linea').html(valores);
            $('#modal_EditarSublinea').modal({
//                show: true,
//                backdrop: 'static'
            });
            return false;
        }
    });
    return false;
}

function actualizarSubLinea() {
    var dato = $("#formularioEditarSublinea").serialize();
    $.ajax({
        url: 'lineaSub_editar.php',
        data: dato,
        type: 'post',
        success: function(registro) {
            swal("Edicion Correcta", "Sub-Linea Modificada Correctamente", "success");
            $('#agrega-registros-subLinea').html(registro);
            return false;
        }
    });
    return false;
}


//Para la nueva Linea
function nuevaLinea() {
    $('#formularioNuevaLinea')[0].reset();
    $.ajax({
        success: function() {
            $('#modal_Nueva_Linea').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;
}

function guardarNuevaLinea() {
    var dato = $("#formularioNuevaLinea").serialize();
    $.ajax({
        url: 'linea_guardar.php',
        data: dato,
        type: 'post',
        success: function(registro) {
            $('#agrega-registros').html(registro);
            swal("Registro Correcto", "Linea Agregada Correctamente", "success");
            return false;
        }
    });
    return false;
}

function nuevaSubLinea() {
    $('#formularioNuevaSubLinea')[0].reset();
    $.ajax({
        success: function() {
            $('#modal_Nueva_Sub_Linea').modal({});
            return false;
        }
    });
    return false;
}

function guardarNuevaSubLinea() {
        var dato = $("#formularioNuevaSubLinea").serialize();
        $.ajax({
            url: 'lineaSub_guardar.php',
            data: dato,
            type: 'POST',
            success: function(registro) {
                $('#agrega-registros-subLinea').html(registro);
                return false;
            }
        });
}

// Fin de la Nueva SUB - Linea
function validarCamposSubLinea(){
    var valido = false;
    if ($('#codigo_sublinea').val() && $('#nombre_sublinea').val()) {
        valido = true;
    }
    return valido;
}

var t = true;
function mostrar_subLinea() {
    if (t == false) {
        $("#mostrar_subLinea").hide();
        t = true;
    } else {
        $("#mostrar_subLinea").show();
        t = false;
    }
}

function ocultar_Linea() {
    if (t == false) {
        $("#ocultar_Linea").hide();
        t = true;
    } else {
        $("#ocultar_Linea").show();
        t = false;
    }
}

// Función
function redireccion(id){
    document.location.href = 'sub_linea.php?id='+id;
}

//Nuvas funciones
function redirect_by_post(purl, pparameters, in_new_tab) {
    pparameters = (typeof pparameters == 'undefined') ? {} : pparameters;
    in_new_tab = (typeof in_new_tab == 'undefined') ? true : in_new_tab;
    var form = document.createElement("form");
    $(form).attr("id", "reg-form").attr("name", "reg-form").attr("action", purl).attr("method", "post").attr("enctype", "multipart/form-data");
    if (in_new_tab) {
        $(form).attr("target", "_blank");
    }
    $.each(pparameters, function(key) {
        $(form).append('<input type="text" name="' + key + '" value="' + this + '" />');
    });
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
    return false;
}