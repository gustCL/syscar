<?php
include_once 'head.php';
$nrofila = 1;

$idLineaP = $_GET['id'];

    $queryPadreLinea = "SELECT * FROM Linea WHERE idLinea = '$idLineaP'";
    $cmd_padre = Database::getInstance()->getDb()->prepare($queryPadreLinea);
    $cmd_padre->execute();
    $lineaP = $cmd_padre->fetch();
    $name_lineaP = $lineaP['nombreLinea'];
    $id_lineaP = $lineaP['idLinea'];

?>
<div class="page-content">
    <div class="page-header position-relative">
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="widget">
                <div class="widget-body">
                    <center>
                        <div class="form-inline">
                            <!--id del Padre oculto para enviar en el post-->
                            <input id="idPadrePP" name="idPadrePP" value="<?= $idLineaP ?>" hidden>
                        </div>
                        <div class="form-inline">
                            <div class="input-group">
                                <div class="col-lg-3">
                                    <span class="input-icon">
                                        <input id="bs-sublinea" name="bs-sublinea" type="text" size="40"
                                               class="form-control"
                                               placeholder="Buscar sublinea por : Codigo, Nombre">
                                        <i class="glyphicon glyphicon-search circular blue"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="input-group">
                                <a onclick="nuevaSubLinea();" class="btn btn-warning shiny">
                                    <i class="fa fa-plus">
                                    </i> Nueva SubLinea
                                </a>
                            </div>
                        </div>
                    </center>

                    <div id="editar_Linea" hidden>

                    </div>
                    <br>
                    <!-- TABLA DE Sub Linea-->
                    <div class="registros" id="agrega-registros-subLinea">
                        <table class="table table-striped">
                            <thead>
                            <!--Fila de titulos de columnas-->
                            <tr>
                                <th>Nro.</th>
                                <th>Codigo SubLinea</th>
                                <th>Nombre Sublinea</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            try {
                                //Obtenemos todas las sub lineas que pertenecen a la Linea Elegida
                                $consulta = "SELECT * FROM Linea WHERE estado = 1 AND idLineaPadre='$idLineaP'";
                                $comando = Database::getInstance()->getDb()->prepare($consulta);
                                $comando->execute();

                                while ($row = $comando->fetch()) {
                                    $idLinea = $row['idLinea'];
                                    ?>
                                    <tr>
                                        <td><?= $nrofila ?></td>
                                        <td><?= $row['codLinea'] ?></td>
                                        <td><?= $row['nombreLinea'] ?></td>
                                        <td>
                                            <a href="javascript:verLinea('<?= $idLinea ?>');"
                                               class="btn btn-default btn-xs blue">
                                                <i class="fa fa-eye"></i> Ver
                                            </a>
                                            <a href="javascript:editarSubLinea('<?= $idLinea ?>');"
                                               class="btn btn-default btn-xs purple">
                                                <i class="fa fa-edit"></i> Editar
                                            </a>
                                            <a href="javascript:eliminarSubLinea('<?= $idLinea ?>');"
                                               class="btn btn-default btn-xs black">
                                                <i class="fa fa-trash-o"></i> Eliminar
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                    $nrofila = $nrofila + 1;
                                }//terminacion del while
                            } catch (PDOException $e) {
                                echo 'Error: ' . $e;
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal VER Linea-->
<div class="modal fade" id="formulario_verlinea"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">    
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Linea</h4>
            </div>
            <form id="formularioverlinea" class="formulario">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <center>
                                <div class="form-inline">
                                    <div class="form-group">
                                        ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control "  id="linid" name="linid" readonly="true">
                                    </div>
                                </div>
                                <br>
                                <div class="form-inline">
                                    <div class="form-group">
                                        Codigo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control "  id="lincodigo" name="lincodigo" readonly="true">
                                    </div>
                                </div>
                                <br>
                                <div class="form-inline">
                                    <div class="form-group">
                                        Nombre &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="linnombre" name="linnombre" readonly="true">
                                    </div>
                                </div>
                                <br />             
                                <!--BOTONES-->                                         
                                <center>
                                    <div class="form-group">
                                        <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Salir</button>
                                    </div>  
                                </center>
                            </center>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal Para EDITAR Sub - Linea-->
<div class="modal fade" id="modal_EditarSublinea"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Sub Linea</h4>
            </div>
            <form id="formularioEditarSublinea" name="formularioEditarSublinea" class="formulario">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <center>
                                <!--Aqui viene los datos del modal-->
                                <input id="idPadreEdit" name="idPadreEdit" value="<?=$idLineaP?>" hidden>
                                <div id="modal_sub_linea">
                                </div>
                            </center>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<!--MODAL NUEVA SUBL-LINEA-->
<div class="modal fade" id="modal_Nueva_Sub_Linea"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Nueva SubLinea Para : <?= $name_lineaP?></h4>
            </div>
            <form id="formularioNuevaSubLinea" name="formularioNuevaLinea" class="formulario">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <center>
                                        S U B  -  L I N E A 
                                    </center>
                                </div>
                                <div class="form-inline">
                                    <center>
                                        <div class="form-group">
                                            Codigo SubLinea &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="form-group">
                                            <input  type="text" class="form-control col-lg-3" id="codigo_sublinea" name="codigo_sublinea" type="text" placeholder="cod Sub-linea"/>
                                            <input id="idPadreP" name="idPadreP" value="<?= $idLineaP ?>" hidden>
                                        </div>
                                    </center>
                                </div>
                                <br>
                                <div class="form-inline">
                                    <center>
                                        <div class="form-group">
                                            nombre SubLinea &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="form-group">
                                            <input  type="text" class="form-control col-lg-3" id="nombre_sublinea" name="nombre_sublinea" placeholder="nombre Sub-linea">
                                        </div>
                                    </center>
                                </div>
                                <br>
                                <center>
                                    <br>
                                    <button onclick="return guardarNuevaSubLinea()" id="btn-guardar-sublinea" type="button" class="btn btn-info shiny"  data-dismiss="modal" aria-hidden="true">Guardar</button>                                    
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require_once '../header_footer/footer.php'; ?>