<?php
$nrofila = 1;
require_once '../datos/Database.php';
//Obtenemos todas las sub lineas que pertenecen a la Linea Elegida
            $dato = $_POST['dato'];
//            $val=$dato[0];
            $idPadre=$_POST['idPadre'];            
?>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <!--Fila de titulos de columnas-->
        <tr>
            <th class="hidden-xs">
                Nro.
            </th>

            <th class="hidden-xs">
                Codigo SubLinea
            </th>
            <th class="hidden-xs">
                Nombre Sublinea
            </th>

            <th>
                subLinea
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        try {
            
//            $consulta = "select* from Linea where idLineaPadre<>0 AND estado=1 and(codLinea LIKE '%$dato%' or nombreLinea LIKE '%$dato%')";
            $consulta = "SELECT * FROM Linea WHERE  idLineaPadre = $idPadre AND estado=1 AND (codLinea LIKE '$dato%' OR nombreLinea LIKE '$dato%') ORDER BY idLinea DESC";
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            $comando->execute();

            while ($row = $comando->fetch()) {
                $idLinea = $row['idLinea'];
                ?>
                <tr>
                    <td>
                        <?= $nrofila ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['codLinea'] ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['nombreLinea'] ?>
                    </td>

                    <td>
                        <a href="javascript:verLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs blue">
                            <i class="fa fa-eye"></i> Ver
                        </a>
                        <a  href="javascript:editarSubLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs purple">
                            <i class="fa fa-edit"></i> Editar
                        </a>
                        <a href="javascript:eliminarSubLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs black">
                            <i  class="fa fa-trash-o"></i> Eliminar
                        </a>

                    </td>



                </tr>
                <?php
                $nrofila = $nrofila + 1;
            }//terminacion del while
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
        ?>

    </tbody>
</table>


