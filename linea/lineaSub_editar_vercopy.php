<?php
require_once '../datos/Database.php';

$id = trim($_POST['id']);
echo $id;
$consulta = "select * from Linea where idLinea='$id'";

$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$valores = $comando->fetch();

?>
<div class="form-inline">
    <div class="form-group">
        ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <!--Envio por post el id del padre-->
        <input type="text" class="form-control "  id="editsublinid" name="editsublinid" value="<?=$valores['idLinea']?>" readonly="true">
    </div>
</div>
<br/>
<div class="form-inline">
    <div class="form-group">
        Codigo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <input type="text" class="form-control"  id="editsublincodigo" name="editsublincodigo" value="<?=$valores['codLinea']?>">
    </div>
</div>
<br/>
<div class="form-inline">
    <div class="form-group">
        Nombre &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <input type="text" class="form-control"  id="editsublinnombre" name="editsublinnombre" value="<?=$valores['nombreLinea']?>">
    </div>
</div>
<br/>
<div class="form-inline">
    <div class="form-group">
        Linea &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <select name="editsublin_lin" id="altipo_editar" style="width:100%;">
            <option value="0"></option>
           <?php mostrarLista();?>
        </select>
        </div>                           
    </div>
</div>

<br />             
<!--BOTONES-->
<br />            

<div class="form-group">
    <button class="btn btn-primary" onclick="return actualizarSubLinea()" data-dismiss="modal" aria-hidden="true">Guardar</button>&nbsp;&nbsp;&nbsp;
    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancelar</button>
</div>  






<?php
function mostrarLista() {
    $consulta = "select*from Linea where estado=1 and idLineaPadre=0";
    try {
        // Preparar sentencia
        $comando = Database::getInstance()->getDb()->prepare($consulta);
        // Ejecutar sentencia preparada
        $comando->execute();
        while ($lstModulos = $comando->fetch()) {
            ?>
            <option value="<?= $lstModulos['idLinea'] ?>"><?= $lstModulos['nombreLinea'] ?></option>
            <?php
        }
    } catch (PDOException $e) {
        echo 'Error: ' . $e;
    }
}
?>
