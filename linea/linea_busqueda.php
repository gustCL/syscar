<?php
$nrofila = 1;
require_once '../datos/Database.php';
?>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <!--Fila de titulos de columnas-->
        <tr>
            <th class="hidden-xs">
                Nro.
            </th>
            <th class="hidden-xs">
                Codigo Linea
            </th>
            <th class="hidden-xs">
                Nombre Linea
            </th>

            <th>  
                Linea
            </th>

        </tr>
    </thead>
    <tbody>
        <?php
        try {
            //obtenemos todas las lineas
            $dato = $_POST['dato'];
            $consulta = "SELECT * FROM Linea WHERE idLineaPadre = 0 AND  estado = 1 AND (codLinea LIKE '$dato%' OR nombreLinea LIKE '$dato%') ORDER BY idLinea DESC";
            
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            $comando->execute();

            while ($row = $comando->fetch()) {
                $idLinea = $row['idLinea'];
                ?>
                <tr>
                    <td>
                        <?= $nrofila ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['codLinea'] ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['nombreLinea'] ?>
                    </td>

                    <td>
                        <a href="javascript:verLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs blue">
                            <i class="fa fa-eye"></i> Ver
                        </a>
                        <a  href="javascript:editarLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs purple">
                            <i class="fa fa-edit"></i> Editar
                        </a>
                        <a href="javascript:eliminarLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs black">
                            <i  class="fa fa-trash-o"></i> Eliminar
                        </a>
                        <<a href="javascript:redirect_by_post('sub_linea.php',{id: <?= $idLinea ?>},false);" class="btn btn-default btn-xs ">
                                                                        <i  class="fa fa-archive"></i> ver Sublineas
                                                                    </a>

                    </td>
                </tr>
                <?php
                $nrofila = $nrofila + 1;
            }//terminacion del while
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
        ?>
    </tbody>
</table>
