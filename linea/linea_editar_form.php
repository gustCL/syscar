<?php
require_once '../../datos/Database.php';
$id = trim($_POST['id']);
//OBTENEMOS LOS VALORES DE LA LINEA//
$consulta = "select * from Linea where idLinea='$id'";
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$valores = $comando->fetch();
?>



<form id="formularioEditarlinea" class="formulario">
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    ID
                </div>
                <div class="form-group">
                    <input type="text" class="form-control "  id="editlinid" name="editlinid" readonly="true" value="<?= $valores['idLinea'] ?>">
                </div>

                <div class="form-group">
                    Codigo
                </div>
                <div class="form-group">
                    <input type="text" class="form-control"  id="editlincodigo" name="editlincodigo" value="<?= $valores['codLinea'] ?>">
                </div>
                <div class="form-group">
                    Nombre
                </div>
                <div class="form-group">
                    <input type="text" class="form-control"  id="editlinnombre" name="editlinnombre" value="<?= $valores['nombreLinea'] ?>">
                </div>

                <br />             

                <br />             
                <!--BOTONES-->
                <br />            
                <center>
                    <div class="form-group">
                        <button class="btn btn-primary" onclick="return actualizarLinea()" data-dismiss="modal" aria-hidden="true">Guardar</button>&nbsp;&nbsp;&nbsp;
                        <button  type="button" class="btn btn-danger">Cancelar</button>
                    </div>  
                </center>
            </div>
        </div>

    </div>
</form>