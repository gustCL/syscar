<?php
include_once './head.php';
?>

<div class="page-content">
    <!-- Arbol de navegacion -->
   
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong> REGISTRAR NUEVA LINEA </strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>

        <!--Header Buttons End-->
    </div>


    <div class="page-body">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
   
                   
                            <div class="widget-body">

                                <form role="form" method="post" action="../datos/linea_guardar.php" 
                                      data-bv-message="This value is not valid"
                                      data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                                      data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                                      data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">

                                    <div class="form-group">
                                        <center>
                                            <strong> L I N E A </strong>                                    
                                        </center>
                                    </div>

                                    <div class="form-group">


                                        <label for="exampleInputEmail1">Codigo Linea</label>
                                        <input  type="text" class="form-control" id="codigo_linea" name="codigo_linea" type="text"
                                                data-bv-notempty="true" data-bv-notempty-message="llene el campo" placeholder="nombre linea"/>

                                        <label for="exampleInputEmail1">Nombre Linea</label>
                                        <input  type="text" class="form-control" id="nombre_linea" name="nombre_linea" type="text"
                                                data-bv-notempty="true" data-bv-notempty-message="llene el campo" placeholder="nombre linea"/>
                                        <br>
                                        <center>
                                            <button id="btn-guardar-linea" type="submit" class="btn btn-info shiny" >Guardar</button>
                                        </center>

                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <strong>  S U B  -  L I N E A </strong> 
                                        </center>
                                    </div>


                                    <div class="form-group">

                                        <label for="exampleInputEmail1">Codigo SubLinea</label>
                                        <input  type="text" class="form-control" id="codigo_sublinea" name="codigo_sublinea" type="text"
                                                data-bv-notempty="true" data-bv-notempty-message="llene el campo" placeholder="nombre Sub-linea"/>

                                        <label for="exampleInputEmail1">nombre SubLinea</label>
                                        <input  type="text" class="form-control" id="nombre_sublinea" name="nombre_sublinea" type="text"
                                                data-bv-notempty="true" data-bv-notempty-message="llene el campo" placeholder="nombre Sub-linea"/>

                                        <br>

                                        <label for="exampleInputPassword1">Linea</label>
                                        <br>
                                        <div>
                                            <select class="col-xs-12" id="sleclinea" name="sleclinea" data-toggle="simplecolorpicker">
                                                <option value="vacio" selected="" data-bv-notempty="true"></option>
                                                <?php
                                                listarLinea();
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <center>
                                            <button id="btn-guardar-sublinea" type="submit" class="btn btn-info shiny" >Guardar</button>
                                        </center>

                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
     



<?php

function listarLinea() {
    //$consulta = "SELECT * FROM privilegiosmodulo";
    $consulta = "select * from Linea where idLineaPadre=0 and estado = 1";
    require_once '../datos/Database.php';
    try {
        // Preparar sentencia
        $comando = Database::getInstance()->getDb()->prepare($consulta);
        // Ejecutar sentencia preparada
        $comando->execute();
        while ($lstModulos = $comando->fetch()) {
            ?>
            </option><option value="<?= $lstModulos['idLinea'] ?>"><?= $lstModulos['nombreLinea'] ?></option>
            <?php
        }
    } catch (PDOException $e) {
        echo 'Error: ' . $e;
    }
}
?>


<?php
include_once '../header_footer/footer.php';
?>