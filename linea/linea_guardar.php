<?php
            require_once ("../datos/Database.php");
            //$id_linea = $_POST['idLinea'];

            $cod_linea = $_POST['codigo_linea'];
            $nombre_linea = $_POST['nombre_linea'];

            if ($cod_linea <> '' and $nombre_linea <> '') { //Inserta Linea
                $existelin = "SELECT * FROM Linea WHERE codLinea = '$cod_linea'";
                $exist = Database::getInstance()->getDb()->prepare($existelin);
                $exist->execute();

                if ($exist->rowCount() == 0) {//Verifico que el codigo no se repita
                    $consultaP = "INSERT INTO Linea(codLinea,nombreLinea,estado,idLineaPadre)VALUES ('$cod_linea','$nombre_linea',1,0)";
                    $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
                    $comandoP->execute();
            //        header('Location: ../linea/');
                } else {
                    //El codigo ya Existe
                }
            } else {
                //no se inserta
            }
            $nrofila = 1;


            //obtenemos todas las lineas
            $consulta = "SELECT * FROM Linea WHERE idLineaPadre=0 and estado = 1 ORDER BY idLinea DESC";
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            $comando->execute();
?>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <!--Fila de titulos de columnas-->
        <tr>
            <th class="hidden-xs">
                Nro
            </th>
            <th class="hidden-xs">
                Codigo Linea
            </th>
            <th class="hidden-xs">
                Nombre Linea
            </th>

            <th>  
                Linea
            </th>

        </tr>
    </thead>
    <tbody>
        <?php
        try {
            while ($row = $comando->fetch()) {
                $idLinea = $row['idLinea'];
                ?>
                <tr>
                    <td>
                        <?= $nrofila ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['codLinea'] ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['nombreLinea'] ?>
                    </td>

                    <td>
                        <a href="javascript:verLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs blue">
                            <i class="fa fa-eye"></i> Ver
                        </a>
                        <a  href="javascript:editarLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs purple">
                            <i class="fa fa-edit"></i> Editar
                        </a>
                        <a href="javascript:eliminarLinea('<?= $idLinea ?>');" class="btn btn-default btn-xs black">
                            <i  class="fa fa-trash-o"></i> Eliminar
                        </a>

                        <a href="javascript:redirect_by_post('sub_linea.php',{id: <?= $idLinea ?>},false);" class="btn btn-default btn-xs ">
                            <i  class="fa fa-archive"></i> ver Sublineas
                        </a>
                    </td>

                </tr>
                <?php
                $nrofila = $nrofila + 1;
            }//terminacion del while
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
        ?>
    </tbody>
</table>