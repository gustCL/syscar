<?php
require_once '../datos/Database.php';
    $id_usuario = $_SESSION['userMaster']['idUsuario'];
?>

<!-- Alertify -->
<script src="../extras/js/alertifyjs/alertify.min.js"></script>
<link href="../extras/js/alertifyjs/css/alertify.min.css" rel="stylesheet" />
<link href="../extras/js/alertifyjs/css/themes/default.css" rel="stylesheet" />

<!-- SweetAlert -->
<script src="../extras/js/sweetalert/sweetalert.min.js"></script>
<link href="../extras/js/sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />

<!-- jQuery 2.2.0 -->
<script src="../extras/js/jquery/jquery-2.2.0.min.js"></script>

<!-- Font Awesome 4.5.0 -->
<link rel="stylesheet" href="../extras/css/font-awesome-4.5.0/css/font-awesome.min.css">

<body>
    <!-- Loading Container -->
    <div class="loading-container">
        <div class="loader"></div>
    </div>
    <!--  /Loading Container -->
    <!-- Navbar -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="navbar-container" >
                <!-- Navbar Barnd -->
                <div>                    
                </div>
                <div class="navbar-header pull-left">
                    <large>
                        <img src="../assets/img/sc.png" alt="" width="200" height="80" />
                    </large>
                </div>
                <!-- /Navbar Barnd -->
                <!-- Sidebar Collapse -->
                <div class="sidebar-collapse" id="sidebar-collapse">
                    <i class="collapse-icon fa fa-bars"></i>
                </div>
                <!-- /Sidebar Collapse -->
                <!-- lado derecho iconos de mensaje y sesion --->
                <div class="navbar-header pull-right">
                </div>
                <!-- /Account Area and Settings -->
            </div>
        </div>
    </div>
    <!-- /Navbar -->
    <!-- Main Container -->
    <div class="main-container container-fluid">
        <!-- Menu de la izquierda-->
        <div class="page-container">
            <!-- Page Sidebar -->
            <div class="page-sidebar" id="sidebar">
                <!-- Sidebar Menu -->
                <ul class="nav sidebar-menu">
                    <!--Inicio-->
                    <li>
                        <a href="../inicio/">
                            <i class="menu-icon glyphicon glyphicon-home"></i>
                            <span class="menu-text"> <strong>INICIO</strong></span>
                        </a>
                    </li>
                    <li id="perfil">
                        <a href="../perfil/">
                            <i class="menu-icon glyphicon glyphicon-user"></i>
                            <span class="menu-text"> <strong>PERFIL</strong></span>
                        </a>
                    </li>

                    <?php
                        // Obtener los módulos a los que tiene acceso el usuario
                        $query = "SELECT DISTINCT (pm.nombreModulo)
                                  FROM PrivilegiosModulo pm, PrivilegiosCasoUso pcu, AsignarModuloCasoUso amcu,
                                       AsignaPrivilegioCasoUso apcu, Usuario u
                                  WHERE pm.idModulo = amcu.idModulo AND
                                        pcu.idCasoUso = amcu.idCasoUso AND
                                        pcu.idCasoUso = apcu.idCasoUso AND
                                        apcu.idUsuario = u.idUsuario AND
                                        u.estado = 1 AND
                                        apcu.idUsuario = :id_usuario";

                        try {
                            $cmd_ventas = Database::getInstance()->getDb()->prepare($query);
                            $cmd_ventas->bindParam(':id_usuario', $id_usuario);
                            $cmd_ventas->execute();

                            while ($lstModulos = $cmd_ventas->fetch()) {   ?>
                                <li>
                                    <a href="#" class="menu-dropdown">
                                        <?php
                                        switch ($lstModulos['nombreModulo']) {
                                            case 'VENTAS':
                                                echo'<i class="menu-icon fa fa-usd"></i>';
                                                break;
                                            case 'COMPRAS':
                                                echo'<i class="menu-icon fa fa-shopping-cart"></i>';
                                                break;
                                            case 'INVENTARIO':
                                                echo'<i class="menu-icon fa fa-pencil-square-o"></i>';
                                                break;
                                            case 'DEUDAS':
                                                echo'<i class="menu-icon fa fa-warning"></i>';
                                                break;
                                            case 'CAJA CHICA':
                                                echo '<i class="menu-icon fa fa-credit-card"></i>';
                                                break;
                                            case 'REPORTES':
                                                echo'<i class="menu-icon fa fa-bar-chart"></i>';
                                                break;
                                            case 'TIPO CAMBIO':
                                                echo'<i class="menu-icon fa fa-money"></i>';
                                                break;
                                            case 'USUARIOS':
                                                echo'<i class="menu-icon fa fa-group"></i>';
                                                break;
                                            case 'PLAN CUENTAS':
                                                echo'<i class="menu-icon fa fa-folder-o"></i>';
                                                break;
                                            case 'COMPROBANTES':
                                                echo'<i class="menu-icon fa fa-suitcase"></i>';
                                                break;
                                            case 'EMPRESA':
                                                echo'<i class="menu-icon fa fa-building"></i>';
                                                break;
                                            case 'UTILIDADES':
                                                echo'<i class="menu-icon fa fa-database"></i>';
                                                break;
                                            case 'CONFIGURACIONES':
                                                echo'<i class="menu-icon fa fa-cogs"></i>';
                                                break;
                                            default:
                                                echo'<i class="menu-icon fa fa-cart-plus"></i>';
                                                break;
                                        }
                                        ?>
                                        <span class="menu-text"> <strong><?= $lstModulos['nombreModulo'] ?> </strong>
                                            <i class="menu-expand"></i>
                                        </span>
                                    </a>
                                    <!--subMenu-->
                                    <ul class="submenu" style="display: none">
                                    <?php
                                        cargar_sub_menu($id_usuario, $lstModulos['nombreModulo']);
                                    ?>
                                    </ul>
                                </li>
                                <?php
                            }
                        } catch (PDOException $e) {
                            echo 'Error: ' . $e;
                        }
                    ?>
                    <li>
                        <a href="../cierresesion/">
                            <i class="menu-icon glyphicon glyphicon-lock"></i>
                            <span class="menu-text"><strong>CERRAR SESIÓN</strong></span>
                        </a>
                    </li>

                </ul>
            </div>


            <!--Cargar Sub Menu-->
            <?php

            function cargar_sub_menu($id_usuario, $nombre_modulo) {
                $cmd = "SELECT pcu.nombreCasoUso, pcu.url
                             FROM PrivilegiosModulo pm, PrivilegiosCasoUso pcu, AsignarModuloCasoUso amcu,
                                  AsignaPrivilegioCasoUso apcu, Usuario u
                             WHERE pm.idModulo = amcu.idModulo AND
                                   pcu.idCasoUso = amcu.idCasoUso AND
                                   pcu.idCasoUso = apcu.idCasoUso AND
                                   apcu.idUsuario = u.idUsuario AND
                                   u.estado = 1 AND
                                   apcu.idUsuario = :id_usuario AND pm.nombreModulo = :nombre_modulo";

                try {
                    $cmd = Database::getInstance()->getDb()->prepare($cmd);
                    $cmd->bindParam(':id_usuario', $id_usuario);
                    $cmd->bindParam(':nombre_modulo', $nombre_modulo);
                    $cmd->execute();
                    while ($row = $cmd->fetch()) {
                        ?>
                        <li>
                            <a href="<?= $row['url'] ?>">
                                <span class="menu-text"><strong><?= $row['nombreCasoUso'] ?></strong></span>
                            </a>
                        </li>
                        <?php
                    }
                } catch (PDOException $e) {
                    echo 'Error: ' . $e;
                }
            }
            ?>