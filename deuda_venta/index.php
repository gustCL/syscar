<?php
require_once ('head.php');
require_once '../datos/Database.php';
?>
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative" > 
        <div class="header-title" >
            <h1>
                <Strong>VENTAS CON SALDO PENDIENTE</strong>
            </h1> 
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            
                        
                            <div class="widget-body">
                                <br>
                                <center>
                                    <div class="form-inline">

                                        <div class="input-group"> 
                                            <div class="col-lg-3">
                                                <span class="input-icon">
                                                    <input id="bs-credito" name="bs-credito" type="text" size="70" class="form-control"  placeholder="NIT/CI del Cliente, Nombre del  Cliente, Nombre de la Sucursal" />
                                                    <i class="glyphicon glyphicon-search circular blue"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </center>
                                <br>
                            </div>
                                <br />
                                 <div class="widget-body">
                                      <center><h3><strong>LISTA DE VENTAS AL CREDITO</strong></h3></center>
                                  <br/>
                                <!-- TABLA DE SECTORES-->
                                <div class="registros" id="agrega-registros">                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Codigo
                                                </th>
                                                <th>
                                                    Nro. de venta
                                                </th>
                                                <th>
                                                    Sucursal
                                                </th>
                                                <th>
                                                    Cliente
                                                </th>
                                                <th>
                                                    Fecha de pago
                                                </th>
                                                <th>
                                                    Cuotas faltantes
                                                </th>
                                                <th>
                                                    Monto total deuda
                                                </th>
                                                <th>
                                                    Opciones
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $registro = "SELECT vc.idCredito,vsuc.nroVenta,suc.nombreSucur,cl.nombreCliente,vc.fechaVencimiento,vc.nroCuotas,vc.saldoPagar  FROM VentasCredito vc, Ventas v, Cliente cl, Sucursal suc, VentaSucursal vsuc  WHERE vc.idVenta=v.idVenta AND v.idCliente = cl.idCliente  AND vsuc.idVenta=v.idVenta AND suc.idSucursal=vsuc.idSucursal AND vc.pagado=1";

                                            try {
                                                $comando = Database::getInstance()->getDb()->prepare($registro);
                                                $comando->execute();
                                                while ($row = $comando->fetch()) {
                                                    $idS = $row['idCredito'];
                                                    ?><tr>
                                                        <td id="idCredito">
                                                            <?= $idS; ?>
                                                        </td>
                                                        <td class="hidden-xs">
                                                            <?= $row['nroVenta'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $row['nombreSucur'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $row['nombreCliente'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $row['fechaVencimiento'] ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $numeroCuotas = "SELECT COUNT(*) AS total FROM PagoCuotasCobrar pcc WHERE pcc.idCredito='$idS'";
                                                            $datoCuotas = Database::getInstance()->getDb()->prepare($numeroCuotas);
                                                            $datoCuotas->execute();
                                                            $cuotas = $datoCuotas->fetch();
                                                            echo $row['nroCuotas'] - $cuotas['total']
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?= $row['saldoPagar'] ?>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:verCredito('<?= $idS ?>');"  class="btn btn-default btn-xs blue" >
                                                                <i class="fa fa-eye"></i> Ver</a>
                                                            <a href="credito_pago.php?id=<?= $idS ?>"   class="btn btn-default btn-xs purple"  >
                                                                <i class="fa fa-edit"></i> Realizar pago</a>   
                                                        </td>

                                                    </tr>
                                                    <?php
                                                }//terminacion del while
                                            } catch (PDOException $e) {
                                                echo 'Error: ' . $e;
                                            }
                                            ?>    
                                        </tbody>
                                    </table>
                                </div>    
                            </div>

                        
                  
        </div>
    </div>
</div>

<!--     D I A L O G   M O D A L ##V E R##   -->
<div class="modal fade" id="formulario_ver"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Saldo de clientes</h4>
            </div>
            <form id="formularioVer" class="form-horizontal" role="form" >
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" >Codigo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control "  id="credcodigo" name="credcodigo" readonly="true" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd">Nro. de venta</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="credventa" name="credventa" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd">Sucursal</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="credsucursal" name="credsucursal" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd">Cliente</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="credcliente" name="credcliente" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd">Fecha de pago</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="credfecha" name="credfecha" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd"> Cuotas faltantes</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="credcuota" name="credcuota" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd">Monto total deuda</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="credtotal" name="credtotal" readonly="true">
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <center>

                                <div class="form-group">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                </div>  
                            </center>
                        </div>  
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


<?php
include_once '../header_footer/footer.php';
?>
