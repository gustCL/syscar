<?php
session_start();
if ($_SESSION['logueado'] != 'SI') {
    header('Location: ../inicio');
}
require_once '../datos/Database.php';
?>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>
                Codigo
            </th>
            <th>
                Nro. de venta
            </th>
            <th>
                Sucursal
            </th>
            <th>
                Cliente
            </th>
            <th>
                Fecha de pago
            </th>
            <th>
                Cuotas faltantes
            </th>
            <th>
                Monto total deuda
            </th>
            <th>
                Opciones
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $dato = $_POST['dato'];
        $registro = "SELECT vc.idCredito,vsuc.nroVenta,suc.nombreSucur,cl.nombreCliente,vc.fechaVencimiento,vc.nroCuotas,vc.saldoPagar  FROM VentasCredito vc, Ventas v, Cliente cl, Sucursal suc, VentaSucursal vsuc  WHERE vc.idVenta=v.idVenta AND v.idCliente = cl.idCliente  AND vsuc.idVenta=v.idVenta AND suc.idSucursal=vsuc.idSucursal AND vc.pagado=1 AND (cl.nit LIKE '$dato%' OR cl.ci LIKE '$dato%' OR cl.nombreCliente LIKE '$dato%' OR suc.nombreSucur LIKE '$dato%')";
        try {
            $comando = Database::getInstance()->getDb()->prepare($registro);
            $comando->execute();
            while ($row = $comando->fetch()) {
                $idS = $row['idCredito'];
                ?><tr>
                    <td id="idCredito">
                        <?= $idS; ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['nroVenta'] ?>
                    </td>
                    <td>
                        <?= $row['nombreSucur'] ?>
                    </td>
                    <td>
                        <?= $row['nombreCliente'] ?>
                    </td>
                    <td>
                        <?= $row['fechaVencimiento'] ?>
                    </td>
                    <td>
                        <?php
                        $numeroCuotas = "SELECT COUNT(*) AS total FROM PagoCuotasCobrar pcc WHERE pcc.idCredito='$idS'";
                        $datoCuotas = Database::getInstance()->getDb()->prepare($numeroCuotas);
                        $datoCuotas->execute();
                        $cuotas = $datoCuotas->fetch();
                        echo $row['nroCuotas'] - $cuotas['total']
                        ?>
                    </td>
                    <td>
                        <?= $row['saldoPagar'] ?>
                    </td>
                    <td>
                        <a href="javascript:verCredito('<?= $idS ?>');"  class="btn btn-default btn-xs blue" >
                            <i class="fa fa-eye"></i> Ver</a>
                        <a href="credito_pago.php?id=<?= $idS ?>"   class="btn btn-default btn-xs purple"  >
                            <i class="fa fa-edit"></i> Realizar pago</a>   
                    </td>

                </tr>
                <?php
            }//terminacion del while
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
        ?>    
    </tbody>
</table>
