$(function() {      
    $('#bs-credito').on('keyup', function() {
        var dato = $('#bs-credito').val();
        var url = 'credito_busqueda.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'dato=' + dato,
            success: function(datos) {
                $('#agrega-registros').html(datos);
            }
        });
        return false;
    });
});
function validarmonto() {
    //  var form=$('#formEnviar').serialize();
    var dato = $('#montopagar').val();
    var monto = $('#saldoporpagar').val();
    dato = parseFloat(dato);
    monto = parseFloat(monto);
    if (dato <= monto) {
        var form = $('#formEnviar').serialize();
        var url = 'guardar_pagocuotacobrar.php';
        $.ajax({
            url: url,
            data: form,
            type: 'POST',
            success: function(valores) {
                var datos = eval(valores);
                document.location.href = 'imprimirpago.php?vt=' + datos[0]+ '&montoBs=' + datos[1];
            }
        });
    } else {
        $('#montopagar').val('');
        $('#montopagar').focus();
        return false;
    }
    return false;
}
function verCredito(id) {
    $('#formularioVer')[0].reset();
    var url = 'credito_ver.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(valores) {
            var datos = eval(valores);
            $('#credcodigo').val(datos[0]);
            $('#credventa').val(datos[1]);
            $('#credsucursal').val(datos[2]);
            $('#credcliente').val(datos[3]);
            $('#credfecha').val(datos[4]);
            $('#credcuota').val(datos[5]);
            $('#credtotal').val(datos[6]);
            $('#formulario_ver').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;

}
;

$(function() {

    $('#nuevoTipoAlmacen').on('click', function() {
        $('#Formulario_Almacen')[0].reset();
        //$('#actu').hide();
        $('#nuevo').show();
        $('#registra_tipoAlm').modal({
            show: true,
            backdrop: 'static'
        });
    });

});
//function validarmonto() {
//    //  var form=$('#formEnviar').serialize();
//    var dato = $('#montopagar').val();
//    var monto = $('#saldoporpagar').val();
//    dato = parseFloat(dato);
//    monto = parseFloat(monto);
//    if (dato <= monto) {
//        var form = $('#formEnviar').serialize();
//        var url = 'guardar_pagocuotacobrar.php';
//        $.ajax({
//            url: url,
//            data: form,
//            type: 'POST',
//            success: function() {
//                document.location.href='../ventacredito';
//            }
//        });
//    } else {
//        $('#montopagar').val('');
//        $('#montopagar').focus();
//        return false;
//    }
//    return false;
//}