<?php
require_once '../datos/Database.php';
$id = $_POST['id'];
$consulta = "SELECT vc.idCredito,vsuc.nroVenta,suc.nombreSucur,cl.nombreCliente,vc.fechaVencimiento,vc.nroCuotas,vc.saldoPagar  
            FROM VentasCredito vc, Ventas v, Cliente cl, Sucursal suc, VentaSucursal vsuc  
            WHERE vc.idVenta=v.idVenta 
            AND v.idCliente = cl.idCliente  
            AND vsuc.idVenta=v.idVenta 
            AND suc.idSucursal=vsuc.idSucursal 
            AND vc.pagado=1 
            AND vc.idCredito='$id'";
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$valores = $comando->fetch();

$datos = array(
    0 => $valores['idCredito'],
    1 => $valores['nroVenta'],
    2 => $valores['nombreSucur'],
    3 => $valores['nombreCliente'],
    4 => $valores['fechaVencimiento'],
    5 => $valores['nroCuotas'],
    6 => $valores['saldoPagar'], 
);
echo json_encode($datos);
?>
