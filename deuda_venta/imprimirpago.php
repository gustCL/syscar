<?php
session_start();
require_once ("../datos/Database.php");
//require_once ('head.php');

$idCredit = $_GET['vt']; //id de la ultima venta
$montoCancelado = $_GET['montoBs'];
///FECHA=HORA DEL SISTEMA
ini_set("date.timezone", "America/La_Paz");
$fechaActualSistema = date('d-m-Y'); //fecha actual del sistemaAND horaFin>='$horaSistema'
$horaActualSistema = date('H:i:s'); //hora actual del sistema
///////////////////////////////////////////////
///////////////////////////////////////////////
/////DATOS DE LA SUCURSAL

$indexSucursal = $_SESSION['userMaster']; //id de la sucursal logueada 
$idSucursal = $indexSucursal['idSucursal'];

$consulta = "SELECT s.* FROM Sucursal s WHERE s.idSucursal=$idSucursal";
$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$dato = $resultado->fetch();
$nombreDeSucursal = $dato['nombreSucur'];
$direccionSucursal = $dato['direccion'];
$telefonoSucursal = $dato['telefono'];


//////////////////////////////////////////////
/////DATOS DEL CLIENTE////////////////////////
$consultaCliente = "SELECT cl.* FROM Cliente cl ,Ventas vn, VentasCredito vc WHERE cl.idCliente = vn.idCliente AND vn.idVenta = vc.idVenta AND vc.idCredito = '$idCredit'";
$resultadoCliente = Database::getInstance()->getDb()->prepare($consultaCliente);
$resultadoCliente->execute();
$datoCliente = $resultadoCliente->fetch();
$nombreClit = $datoCliente['nombreCliente'];
$nit_ci = "S/N";
$idCliente = $datoCliente['idCliente'];
if ($datoCliente['nit'] != "") {
    $nit_ci = $datoCliente['nit'];
} else if ($datoCliente['nit'] = "" && $datoCliente['ci'] != "") {
    $nit_ci = $datoCliente['ci'];
}

//
//DATOS DEEMPRESA
$consultaEmpresa = "SELECT em.* FROM Empresa em,Sucursal su WHERE  su.idSucursal='$idSucursal' AND su.idEmpresa=em.idEmpresa ";
$resultado = Database::getInstance()->getDb()->prepare($consultaEmpresa);
$resultado->execute();
$dato = $resultado->fetch();
$nombreEmpresa = $dato['nombreEmpresa'];
$nitEmpresa = $dato['nit'];
//monto total
$consultaMontoTotal = "SELECT vn.* FROM  Ventas vn,VentasCredito vc WHERE vn.idVenta = vc.idVenta AND vc.idCredito = '$idCredit'"; //obtenos el id de la ttransaccion de venta para obtner el detalle de la venta
$resultado = Database::getInstance()->getDb()->prepare($consultaMontoTotal);
$resultado->execute();
$dato = $resultado->fetch();
$montoTotaVenta = $dato['montoTotal'];
$idDeVenta = $dato['idVenta'];


$queryMontoPagado = "SELECT vc.* FROM  VentasCredito vc WHERE vc.idCredito = '$idCredit'"; //obtenos el id de la ttransaccion de venta para obtner el detalle de la venta
$resultado = Database::getInstance()->getDb()->prepare($queryMontoPagado);
$resultado->execute();
$dato = $resultado->fetch();
$montoPagado = $dato['montoContado'];

$queryMontoDebe = "SELECT vc.* FROM  VentasCredito vc WHERE vc.idCredito = '$idCredit'"; //obtenos el id de la ttransaccion de venta para obtner el detalle de la venta
$resultado = Database::getInstance()->getDb()->prepare($queryMontoDebe);
$resultado->execute();
$dato = $resultado->fetch();
$montoDebe = $dato['saldoPagar'];






/// VAMOS A REGISTRAR EN INGRESO

$cadenaGlosa = "Pago de Credito S/F";
$nroFac =0;
$cantFac = "SELECT COUNT(*)cant FROM Factura WHERE idVenta =?";
$comando2 = Database::getInstance()->getDb()->prepare($cantFac);
$comando2->execute([$idDeVenta]);
$ventaS = $comando2->fetch();
$cantFac = $ventaS['cant'];
if($cantFac > 0){
    $cantFacN = "SELECT * FROM Factura WHERE idVenta =?";
    $comandoNumero = Database::getInstance()->getDb()->prepare($cantFacN);
    $comandoNumero->execute([$idDeVenta]);
    $ventaT = $comandoNumero->fetch();
    $nroFac = $ventaT['nroFactura'];
    $cadenaGlosa = "Pago de Credito C/F";
}else{
    $nroFac="";
}

$idVentaSave = $idDeVenta;

///INSERCCION EN INGRESO CAJA FACTURA
$nventa = $idDeVenta;
$consulta2 = "SELECT * FROM Ventas WHERE idVenta =?";
$comando2 = Database::getInstance()->getDb()->prepare($consulta2);
$comando2->execute([$nventa]);
$venta = $comando2->fetch();
//OBTENEMOS DATOS DE LA NOTA DE VENTA
$consulta4 = "SELECT * FROM VentaSucursal WHERE idVenta =?";
$comando4 = Database::getInstance()->getDb()->prepare($consulta4);
$comando4->execute([$nventa]);
$notaVenta = $comando4->fetch();
//OBTENEMOS DATOS DEL CLIENTE
$consulta3 = "SELECT * FROM Cliente WHERE idCliente =?";
$comando3 = Database::getInstance()->getDb()->prepare($consulta3);
$comando3->execute([$venta['idCliente']]);
$cliente = $comando3->fetch();

$idTransaccion = $venta['idTransaccion'];

$consulta5 = "SELECT Distinct( dt.codigoProducto) AS codigoProducto, pr.nombreComercial FROM DetalleTransaccion dt, Producto pr WHERE dt.idTransaccion =? AND dt.codigoProducto = pr.codigoProducto";
$comando5 = Database::getInstance()->getDb()->prepare($consulta5);
$comando5->execute([$idTransaccion]);

$consulta6 = "SELECT * FROM Transaccion WHERE idTransaccion =? ";
$comando6 = Database::getInstance()->getDb()->prepare($consulta6);
$comando6->execute([$idTransaccion]);
$trans = $comando6->fetch();

//INSERTAR DATOS IngresoCaja
$queryIngresoCaja = "SELECT COUNT(*) FROM IngresoCaja WHERE nroVenta = '$idVentaSave'";
$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$dato = $resultado->fetch();
//
//if ($dato == 0){
    $consultaIC = "INSERT INTO IngresoCaja(idIngresoCaja, idInicio, idTipoIngresoCaja, nroFactura, nroVenta, glosa, montoTotal, idTipoCambio, tipoCambio, efectivoBs, efectivoSus, fechaRegistro, horaRegistro, estado) "
            . "                  VALUES ('',?,1,?,?,?,?,?,?,?,?,?,?,?)";
    $comandoIC = Database::getInstance()->getDb()->prepare($consultaIC);
    $comandoIC->execute([$trans['idInicioSesion'], $nroFac, $idVentaSave,$cadenaGlosa, $montoCancelado, $venta['idTipoCambio'], $venta['tipoCambio'], $montoCancelado, $venta['efectivoSus'], $fechaActualSistema, $horaActualSistema, 1]);
//selecionar el id de IngresoCaja
    $consultaICs = "SELECT * FROM IngresoCaja ORDER BY idIngresoCaja DESC Limit 1";
    $comandoICs = Database::getInstance()->getDb()->prepare($consultaICs);
    $comandoICs->execute();
    $idIngresoCaja = $comandoICs->fetch();
    $idUltimoCaja = $idIngresoCaja['idIngresoCaja'];
//INSERTAR DATOS IngresoVenta
    $consultaIV = "INSERT INTO IngresoVenta (idIngresoCaja, idVenta) VALUES (?,?)";
    $comandoIV = Database::getInstance()->getDb()->prepare($consultaIV);
    $comandoIV->execute([$idUltimoCaja, $venta['idVenta']]);
    
    
    
    
    
?>
<link href="../css/bootstrap.min.css" rel="stylesheet" media="all">
<link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen">
<link href="../reporteCompras/printListaTransac.css" rel="stylesheet" type="text/css" media="print" />  
<link href="../font-awesome/css/font-awesome.css" rel="stylesheet"  media="all">
<script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
<script language="javascript" type="text/javascript" src="jPedidos.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="formatoSinQR.css" media="screen" />
<html> 
    <body> 
        <script language="JavaScript">
            $(document).ready(function() {
                doPrint();
            });
            function doPrint() {
                window.print()();
            }
            function volver(dato) {
                location.href = "index.php";
            }
        </script>
        <style type="text/css">
            table {
                color: #000000;
                font-size: 8pt; 
            }

            td.colum2{
                border: 5px;
            }
            @media all {
                div.saltopagina{
                    display: none;
                }
            }

            @media print{
                div.saltopagina{ 
                    display:block; 
                    page-break-before:always;
                }
            }	

        </style>
        <div id="leteral"></div>
        <div id="noprint">
            <table>
                <tr>
                    <td>
                        <button type="button" name="volver" id="volver" onclick="volver()">Volver</button>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <button type="button" onclick="doPrint()" >Imprimir</button>
                    </td>
                </tr>
            </table> 
        </div>
        <div id="hoja" style="height: auto"> 
            <div id="mostrarUser">
            </div>
            <div class="widget">
                <div id="encabezado" >
                    <div id="logo" >
                        <img width="100" height="100" src="..\..\farmacia\assets\img\Logo1.png"/>
                    </div>
                    <div id="titulo">
                        <center>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="right">
                                            <font size='6'><strong><label for="user_name">NOTA DE PAGO</label></strong></font><br>
                                            <font size='5'><strong><center><label for="user_name"><?= $nombreEmpresa ?></label></center></strong></font><br>
                                <font size='4'><center><label for="user_name">NIT: <?= $nitEmpresa ?></label></center></font>
                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                    <div id="inf">
                        <center>
                            <div align="right">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label >Hora:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: right">
                                                <label ><?= $horaActualSistema ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <strong><label >Fecha:</label></strong>
                                            </td>
                                            <td>
                                                <fieldset>
                                                    <label ><?= $fechaActualSistema ?></label>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                </div>

                <div align="center" id="informacion">
                    <div id="content">
                        <center>
                            <div align="left">
                                <table >
                                    <tbody>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Sucursal:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nombreDeSucursal ?> </label><br>
                                            </td>
                                            <td class="colum2">
                                                <fieldset>
                                                    <strong><label for="user_name">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            Cliente:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nombreClit ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Direccion:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $direccionSucursal ?> </label><br>
                                            </td>
                                            <td class="colum2">
                                                <fieldset>
                                                    <strong><label for="user_name">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            NIT/CI:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $nit_ci ?> </label><br>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Telefono:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name"><?= $telefonoSucursal ?> </label><br>
                                            </td>
                                            <td class="colum2">
                                                <fieldset>
                                                    <strong><label for="user_name">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            Concepto:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name">Pago de Venta al credito</label><br>
                                            </td>
                                        </tr> 
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Monto Total:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $montoTotaVenta?> </label><br>
                                            </td>
                                            <td class="colum2">
                                                <fieldset>
                                                    <strong><label for="user_name">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            Saldo Pagado:</label>
                                                    </strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $montoPagado?></label><br>
                                            </td>
                                        </tr>   
                                        <tr class="">
                                            <td>
                                                <fieldset>
                                                    <strong><label for="user_name">Monto Pendiente:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $montoDebe?> </label><br>
                                            </td>
                                            <td class="colum2">
                                                <fieldset>
                                                    <strong><label for="user_name">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            Monto de Pago:</label></strong>
                                                </fieldset>
                                            </td>
                                            <td style="text-align: left">
                                                <label for="user_name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $montoCancelado?></label><br>
                                            </td>
                                        </tr>   
                                    </tbody>
                                </table>
                            </div>    
                        </center>
                    </div>
                    <strong>Nota:</strong> No se permiten cambios ni devolucion de factura. Por favor verifique sus datos antes de la emision de la misma
                    <div style="border: 1px solid black; width: 40%; height: 12%; margin-top: 2%; " >
                        <br>
                        <strong><br>
                            _______________________<br>
                            ENTREGADO POR:</strong>
                    </div>
                </div>
            </div>
            <form action="ficheroExcel.php" method="post" id="FormularioExportacion">
                <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
            </form>
        </div>
    </body>
</html>