<?php
require_once("../datos/Database.php");
$dato = $_POST['dato'];
//EJECUTAMOS LA CONSULTA DE BUSQUEDA
$dato1 = Database::getInstance()->getDb()->prepare("SELECT p.codigoProducto, p.codigoBarras,p.codigo, li.nombreLinea, p.nombreComercial, ps.precioVenta, SUM(e.stock) AS totalStock 
                  FROM Producto p, Linea li, PrecioSucursal ps, Ubicacion u, Existencias e, Lote lo 
                  WHERE p.idLinea = li.idLinea AND p.estado = 1 AND li.estado = 1 AND p.codigoProducto = lo.codigoProducto
                  AND lo.idLote = e.idLote AND p.codigoProducto = ps.codigoProducto AND p.codigoProducto = u.codigoProducto
                  AND e.idUbicacion = u.idUbicacion  AND  (p.nombreComercial LIKE '%$dato%' OR p.codigoProducto LIKE '%$dato%'  OR p.codigo LIKE '%$dato%' ) GROUP BY lo.codigoProducto ORDER BY p.nombreComercial ASC");
$dato1->execute();
$row_number = 1;
//CREAMOS NUESTRA VISTA Y LA DEVOLVEMOS AL AJAX
?>

<table class="table table-striped" >
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <strong>NRO.</strong>
                                                        </th>
                                                        <th>
                                                            <strong>CODIGO</strong>
                                                        </th>
                                                        <th>
                                                            <strong>DESCRIPCION</strong>
                                                        </th>
                                                        <th>
                                                            <strong>STOCK</strong>
                                                        </th>
                                                        <th>
                                                            <strong>PRECIO VENTA</strong>
                                                        </th>
                                                        <!--Botones-->
                                                        <th>

                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php

                                                    try {
                                                        while ($row = $dato1->fetch()) {
                                                            $idP = $row['codigoProducto'];
                                                            ?>
                                                            <tr>
                                                                <td id = "codigo" name="codigo" > 
                                                                    <?= $row_number ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['codigoBarras'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['nombreComercial'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['totalStock'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['precioVenta'] ?>
                                                                </td>

                                                                

                                                                <!--aqui estan los botones que hacen activar las vnetanas del modal-->
                                                                <td>                          
                                                                    <a href="ver_producto.php?id=<?= $idP ?>" class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                                                                    <a href="mod_producto.php?id=<?= $idP ?>"   class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                                                                    <a href="javascript:eliminarProd('<?= $idP ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $row_number = $row_number + 1;
                                                        }//terminacion del while
                                                    } catch (PDOException $e) {

                                                        echo 'Error: ' . $e;
                                                    }
                                                    ?>    
                                                </tbody>
                                            </table>