<?php
require_once("../datos/Database.php");
$dato = $_POST['dato'];
//EJECUTAMOS LA CONSULTA DE BUSQUEDA


$registro = "select p.codigoProducto , e.codigo , p.nombreComercial , e.nroLote , e.precioVenta , e.fechaVencimiento , s.nombreSector , a.nombreAlmacen from Producto p , Existencias e , Ubicaciones u , Sectores s , Almacen a  where p.codigoProducto = e.codigoProducto and e.sector = s.idSector and s.idSector = u.idSector  and a.idAlmacen = u.idAlmacen and ( p.nombreComercial LIKE '%$dato%' OR e.codigo LIKE '%$dato%'  ) ";

$comando = Database::getInstance()->getDb()->prepare($registro);
$comando->execute();

//CREAMOS NUESTRA VISTA Y LA DEVOLVEMOS AL AJAX
?>
<table class="table table-striped table-bordered table-hover" >
    <thead>
        <tr>
            <th class="hidden-xs">
                Codigo 
            </th>
            <th class="hidden-xs">
                Descripcion
            </th>

            <th class="hidden-xs">
                Lote
            </th>
            <th class="hidden-xs">
                Precio Venta
            </th>
            <th class="hidden-xs">
                Fecha Vencimiento
            </th>
            <th class="hidden-xs">
                Ubicacion
            </th>
            <th class="hidden-xs">
                Sector
            </th>
            <!--Botones-->
            <th>

            </th>

        </tr>
    </thead>
    <tbody>
        <?php
        

        try {

            while ($row = $comando->fetch()) {
                $idP = $row['codigo'];
                ?>
                <tr>
                    <td id = "codigo" name="codigo" > 
                        <?= $idP ?>
                    </td>                                                                
                    <td>
                        <?= $row['nombreComercial'] ?>
                    </td>

                    <td>
                        <?= $row['nroLote'] ?>
                    </td>
                    <td>
                        <?= $row['precioVenta'] ?>
                    </td>
                    <td>
                        <?= $row['fechaVencimiento'] ?>
                    </td>
                    <td>
                        <?= $row['nombreAlmacen'] ?>
                    </td>
                    <td>
                        <?= $row['nombreSector'] ?>
                    </td>
                    <!--aqui estan los botones que hacen activar las vnetanas del modal-->
                    <td>                          
                        <a href="vista_inventario.php?id=<?= $row['codigoProducto'] ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                    </td>
                </tr>
                <?php
            }//terminacion del while
        } catch (PDOException $e) {

            echo 'Error: ' . $e;
        }
        ?>    
    </tbody>
</table>
