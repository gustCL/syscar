<?php
 require_once("../datos/Database.php");
session_start();
ini_set("date.timezone", "America/La_Paz");
$fecha = date("d-m-Y");
$hoy = date("H:i:s");
$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];
$idSesion = $_SESSION['idSesion'];
$codigoCategoria = $_POST['codCategoria'];
$nombreCategoria = $_POST['nombCategoria'];

//inserta producto
$cmd_insertCategoria = Database::getInstance()->getDb()->prepare("INSERT INTO  Linea VALUES(0,
																						:codLinea,
                                                                         				:nombreLinea,
                                                                         				1,
                                                                         				0)");
$cmd_insertCategoria->bindParam(':codLinea', $codigoCategoria);
$cmd_insertCategoria->bindParam(':nombreLinea', $nombreCategoria);
$cmd_insertCategoria->execute();

$cons = Database::getInstance()->getDb()->prepare("SELECT * FROM Linea");
$cons->execute();

?>
<select  name="linea" id="linea"  data-bv-field="country" class="col-lg-12">                            
    <?php while ($row = $cons->fetch(PDO::FETCH_ASSOC)) { ?>
    <option value="<?php echo $row['idLinea']; ?>">
    	<?php echo $row['nombreLinea']; ?>
    </option>
    <?php 
	} 
	?>
 </select> 