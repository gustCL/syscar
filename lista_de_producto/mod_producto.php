<?php
require_once('head.php');
require_once('../datos/Database.php');

$id = $_GET['id'];
$cmd_getProducto = Database::getInstance()->getDb()->prepare("SELECT *, ex.stock AS stockExis, ps.precioVenta AS precioDeVenta 
                                                              FROM Producto p, Lote lo, Existencias ex,PrecioSucursal ps
                                                              WHERE p.codigoProducto = lo.codigoProducto  AND ps.codigoProducto = p.codigoProducto AND lo.idLote = ex.idLote  AND p.codigoProducto = '$id' ");
$cmd_getProducto->execute();
$datosProducto = $cmd_getProducto->fetch();
$idActividad = $datosProducto['idActividad'];
$idLinea = $datosProducto['idLinea'];
$idUnidad = $datosProducto['idUnidad'];
$idLaboratorio = $datosProducto['idLaboratorio'];


?>
<script>
    var openFile = function (event) {
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function () {
            var dataURL = reader.result;
            var output = document.getElementById('img');
            output.src = dataURL;
        };
        reader.readAsDataURL(input.files[0]);
    };


    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " 0123456789abcdefghijklmnñopqrstuvwxyz";
        especiales = [8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57]; //,37,39,46

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial)return false;
    }

    // call your ajax function when the document is ready...
    $(function () {
        callAjax();
    });

    // function that processes your ajax calls...
    function callAjax() {
        var my_delay = 2000;
        $.ajax({
            // ajax parameters here...
            // ...
            success: function () {
                setTimeout(callAjax, my_delay);
            }
        });
    }


</script>

<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>MODIFICAR PRODUCTO</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>

    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget">
                                <form method="post" action="modificar_Producto.php?id=<?= $id ?>" class="form-horizontal"
                                      enctype="multipart/form-data" name="form1" id="form1">
                                    <div class="widget-body">
                                        <br>
                                        <br>
                                        <div class="form-group">
                                            <center>
                                                <label><h3><strong>Actividad</strong></h3></label><br>
                                                <select name="idActividad" id="idActividad" class="btn btn-success"
                                                        style="font-size:16px;  ">
                                                    <?php
                                                    $query_actividad = "SELECT * FROM Actividad WHERE estado = 1";
                                                    $cmd_actividad = Database::getInstance()->getDb()->prepare($query_actividad);
                                                    $cmd_actividad->execute();
                                                    ?>
                                                    <?php while ($row_actividad = $cmd_actividad->fetch()) {
                                                        $selected = '';
                                                        if ($row_actividad['idActividad'] == $datosProducto['idActividad']) {
                                                            $selected = 'selected';
                                                        }
                                                        ?>
                                                        <option
                                                            value="<?= $row_actividad['idActividad'] ?>" <?= $selected ?> >
                                                            <?= $row_actividad['nombre'] ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                                <br>
                                            </center>
                                        </div>
                                        <div class="form-group">

                                            <label class="col-lg-2 control-label">Nombre Comercial</label>
                                            <div class="col-lg-3">
                                                <input value="<?= $datosProducto['nombreComercial'] ?>" type="text"
                                                       title="INGRESE NOMBRE COMERCIAL" class="form-control"
                                                       name="nombreC" id="nombreC" placeholder="Nombre comercial"
                                                       REQUIRED/>

                                            </div>
                                            <label class="col-lg-2 control-label">Codigo</label>
                                            <div class="col-lg-3">
                                                <input value="<?= $datosProducto['codigo'] ?>" type="text"
                                                       title="INGRESE CODIGO" class="form-control"
                                                       onkeypress="javascript:return validarNro(event)" name="codigo"
                                                       placeholder="Codigo" id="codigo" REQUIRED/>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Precio de Costo</label>
                                            <div class="col-lg-3">
                                                <input value="<?= $datosProducto['precioCosto'] ?>" type="text"
                                                       title="INGRESE PRECIO DE COSTO" class="form-control"
                                                       name="precioC" id="precioC" placeholder=" precio Costo"
                                                       maxlength="9"
                                                       onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                                       REQUIRED/>
                                            </div>
                                            <label class="col-lg-2 control-label">Precio de Venta</label>
                                            <div class="col-lg-3">
                                                <input value="<?= $datosProducto['precioDeVenta'] ?>" type="text"
                                                       title="INGRESE PRECIO DE VENTA" class="form-control"
                                                       name="precioV" id="precioV" placeholder="Precio Venta"
                                                       maxlength="9"
                                                       onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                                       REQUIRED/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <!-- categoria es misma linea -->
                                            <label class="col-lg-2 control-label">Categoria</label>
                                            <div class="col-lg-3">
                                                <?php
                                                $cons = Database::getInstance()->getDb()->prepare("SELECT * FROM Linea");
                                                $cons->execute();
                                                ?>
                                                <div id="agrega-datosCategoria">
                                                    <select name="linea" id="linea" data-bv-field="country"
                                                            class="col-lg-12">
                                                        <?php while ($row = $cons->fetch(PDO::FETCH_ASSOC)) {
                                                            $selected = '';
                                                            if ($row['idLinea'] == $datosProducto['idLinea']) {
                                                                $selected = 'selected';
                                                            }
                                                            ?>
                                                            <option value="<?= $row['idLinea'] ?>" <?= $selected ?>>
                                                                <?= $row['nombreLinea'] ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <i class="form-control-feedback" data-bv-field="country"
                                                   style="display: none;"></i>
                                                <div>
                                                    <a id="btn-nuevaCategoria" class="btn btn-warning shiny">
                                                        <i class="fa fa-plus">
                                                        </i>Nueva Categoria
                                                    </a>
                                                </div>
                                            </div>
                                            <label class="col-lg-2 control-label">Unidad de Medida</label>
                                            <div class="col-lg-3">
                                                <?php
                                                $datos = Database::getInstance()->getDb()->prepare("SELECT * FROM UnidadMedida");
                                                $datos->execute();
                                                ?>
                                                <div id="agrega-datosUnidadMedida">
                                                    <select name="medida" id="medida" data-bv-field="country"
                                                            class="col-lg-12">
                                                        <?php while ($row = $datos->fetch()) {
                                                            $selected = '';
                                                            if ($row['idUnidad'] == $datosProducto['idUnidad']) {
                                                                $selected = 'selected';
                                                            }
                                                            ?>
                                                            <option value="<?= $row['idUnidad'] ?>" <?= $selected ?> >
                                                                <?= $row['nombreUM'] ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <i class="form-control-feedback" data-bv-field="country"
                                                   style="display: none;"></i>
                                                <div>
                                                    <a id="btn-addUnitMedida" class="btn btn-warning shiny">
                                                        <i class="fa fa-plus">
                                                        </i>Nueva U. de Medida
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" hidden>
                                            <label class="col-lg-2 control-label">Cod. Barra</label>
                                            <div class="col-lg-3">
                                                <input value="<?= $datosProducto['codigoBarras'] ?>" type="text"
                                                       title="INGRESE CODIGO DE BARRA" class="form-control"
                                                       onkeypress="javascript:return validarNro(event)"
                                                       name="codigoBarra" id="codigoBarra" placeholder="Codigo Barra"
                                                       />
                                            </div>
                                            <label class="col-lg-2 control-label"> Stock </label>
                                            <div class="col-lg-3">
                                                <input value="<?= $datosProducto['stockExis'] ?>" type="text"
                                                       title="INGRESE STOCK MINIMO" class="form-control"
                                                       name="stockDiser" id="stockDiser" placeholder="Stock Minino"
                                                       maxlength="9"
                                                       onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                                       />
                                            </div>
                                        </div>

                                        <div class="form-group" hidden>
                                            <label class="col-lg-2 control-label">Nombre Abreviado</label>
                                            <div class="col-lg-3">
                                                <input value="<?= $datosProducto['nombreGenerico'] ?>" type="text"
                                                       title="INGRESE NOMBRE ABREVIADO" class="form-control"
                                                       onkeypress="return soloLetras(event)" name="nombreGenerico"
                                                       id="nombreGenerico" placeholder="Nombre Abreviado" />

                                            </div>
                                            <label class="col-lg-2 control-label">Margen %</label>
                                            <div class="col-lg-3">
                                                <input value="<?= $datosProducto['margen'] ?>" type="text"
                                                       title="INGRESE MARGEN" class="form-control" name="margen"
                                                       id="margen" placeholder="Margen" maxlength="9"
                                                       onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                                       />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <!-- Laboratio es marca -->
                                            <label class="col-lg-2 control-label">Marca</label>
                                            <div class="col-lg-3">
                                                <?php

                                                $datos = Database::getInstance()->getDb()->prepare("SELECT * FROM Laboratorio");
                                                $datos->execute();
                                                ?>
                                                <div id="agrega-datosMarca">
                                                    <select name="laboratorioMarca" id="laboratorioMarca"
                                                            data-bv-field="country" class="col-lg-12">
                                                        <?php while ($row = $datos->fetch(PDO::FETCH_ASSOC)) {
                                                            $selected = '';
                                                            if ($row['idLaboratorio'] == $datosProducto['idLaboratorio']) {
                                                                $selected = 'selected';
                                                            }
                                                            ?>
                                                            <option
                                                                value="<?php echo $row['idLaboratorio']; ?>" <?= $selected ?> >
                                                                <?php echo $row['nombreLab']; ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <i class="form-control-feedback" data-bv-field="country"
                                                   style="display: none;"></i>
                                                <div>
                                                    <a id="btn-addMarca" class="btn btn-warning shiny">
                                                        <i class="fa fa-plus">
                                                        </i>Nueva Marca
                                                    </a>
                                                </div>
                                            </div>
                                            <label class="col-lg-2 control-label">Proveedor</label>
                                            <div class="col-lg-3">
                                                <?php
                                                $datos = Database::getInstance()->getDb()->prepare("SELECT * FROM Proveedor");
                                                $datos->execute();
                                                //recuperamos el idProveedor de la tabla idProvee
                                                $cmd_getIdProveedor = Database::getInstance()->getDb()->prepare("SELECT * FROM Provee WHERE codigoProducto='$id'");
                                                $cmd_getIdProveedor->execute();
                                                $idProvee = $cmd_getIdProveedor->fetch();
                                                ?>
                                                <div id="agrega-datosProveedor">
                                                    <select name="provedorAdd" id="provedorAdd" data-bv-field="country"
                                                            class="col-lg-12">
                                                        <?php while ($row = $datos->fetch()) {
                                                            $selected = '';
                                                            if ($row['idProveedor'] == $idProvee['idProveedor']) {
                                                                $selected = 'selected';
                                                            }
                                                            ?>
                                                            <option
                                                                value="<?php echo $row['idProveedor']; ?>" <?= $selected ?> >
                                                                <?php echo $row['nombreProve']; ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <i class="form-control-feedback" data-bv-field="country"
                                                   style="display: none;">
                                                </i>
                                                <div>
                                                    <a id="btn-addProveedor" class="btn btn-warning shiny">
                                                        <i class="fa fa-plus">
                                                        </i>Nuevo Proveedor
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">País</label>
                                            <div class="col-lg-3">
                                                <?php
                                                $datos = Database::getInstance()->getDb()->prepare("SELECT * FROM Pais");
                                                $datos->execute();
                                                ?>
                                                <select name="pais" id="pais" data-bv-field="country" class="col-lg-12">
                                                    <?php while ($row = $datos->fetch()) {
                                                        $selected = '';
                                                        if ($row['idPais'] == $datosProducto['idProcedencia']) {
                                                            $selected = 'selected';
                                                        }
                                                        ?>
                                                        <option value="<?= $row['idPais'] ?>" <?= $selected ?>>
                                                            <?= $row['nombrePais'] ?>
                                                        </option>
                                                    <?php } ?>
                                                </select><i class="form-control-feedback" data-bv-field="country"
                                                            style="display: none;"></i>
                                            </div>
                                            <label class="col-lg-2 control-label"> Stock </label>
                                            <div class="col-lg-3">
                                                <input value="<?= $datosProducto['stockExis'] ?>" type="text"
                                                       title="INGRESE STOCK MINIMO" class="form-control"
                                                       name="stockDiser" id="stockDiser" placeholder="Stock Minino"
                                                       maxlength="9"
                                                       onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                                />
                                            </div>
                                            <label class="col-lg-2 control-label" hidden>Equivalente</label>
                                            <div class="col-lg-3" hidden>
                                                <input value="<?= $datosProducto['accionTerapeutica']; ?>" type="text"
                                                       class="form-control" title="INGRESE EQUIVALENCIA"
                                                       name="punteroEquivalente" id="punteroEquivalente"
                                                       placeholder="Equivalencia"/>
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label class="col-lg-2 control-label">Impuesto ICE</label>
                                            <div class="col-lg-3">
                                                <input value="0" type="text" title="INGRESE IMPUESTO ICE" class="form-control"
                                                       onkeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" name="impuestoIce"
                                                       id="impuestoIce" placeholder="Nombre Abreviado" />

                                            </div>
                                            <label class="col-lg-2 control-label">Forma de Venta</label>
                                            <div class="col-lg-3">

                                                <select name="formaVenta" id="formaVenta" data-bv-field="country" class="col-lg-12">
                                                    <option value="0" <?php if($datosProducto['tipoProducto']==0){echo 'Selected';}?>>POR UNIDAD</option>
                                                    <option value="1" <?php if($datosProducto['tipoProducto']==1){echo 'Selected';}?>>POR PESO</option>
                                                </select><i class="form-control-feedback" data-bv-field="country"
                                                            style="display: none;"></i>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-inline">
                                            <center>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-4 col-lg-4">
                                                        <input class="btn btn-primary" type="submit"
                                                               onclick="return callAjax();" value="Guardar"/>
                                                    </div>
                                                </div>

                                                <div class="input-group">
                                                    <div class="col-lg-offset-4 col-lg-4">
                                                        <a type="button" href="../lista_de_producto/"
                                                           class="btn btn-danger"> Cancelar</a>
                                                    </div>
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL PARA AGREGAR UNA CATEGORIA(LINEA)"-->
    <div class="modal fade" id="modal-addCategoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><b>Nueva Categoria</b></h4>
                </div>
                <form id="formAddCategoria" name="formAddCategoria" class="formulario" method="post">
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="form-inline">
                                    <center>
                                        <div class="form-group">
                                            <strong>Codigo</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="codCategoria"
                                                   name="codCategoria" type="text" placeholder="cod. linea"/>
                                        </div>
                                    </center>
                                </div>
                                <br>
                                <div class="form-inline">
                                    <center>
                                        <div class="form-group">
                                            <strong>Nombre</strong> &nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="nombCategoria"
                                                   name="nombCategoria" type="submit" placeholder="nombre linea"/>
                                        </div>
                                    </center>
                                </div>
                                <br>
                                <div class="modal-footer">
                                    <div class="form-inline">
                                        <center>
                                            <div class="form-group">
                                                <button id="guardaCat" class="btn btn-primary"
                                                        onclick="return GuardarRegistro();" data-dismiss="modal"
                                                        aria-hidden="true">Guardar
                                                </button>
                                            </div>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <div class="form-group">
                                                <button data-bb-handler="cancel" type="button" class="btn btn-danger"
                                                        data-dismiss="modal" aria-hidden="true">Cancelar
                                                </button>
                                            </div>
                                        </center>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- MODAL PARA AGREGAR UNA MARCA(LABORATORIO)"-->
    <div class="modal fade" id="modal-addMarca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><b>Nueva Marca</b></h4>
                </div>
                <form id="formAddMarca" name="formAddMarca" class="formulario">
                    <div class="modal-body">

                        <center>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-xs-12">

                                    <div class="form-inline">

                                        <div class="form-group">
                                            <strong>Nombre</strong>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="nombreMarcaModal"
                                                   name="nombreMarcaModal" placeholder="Nombre de laboratorio"/>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <strong>Codigo</strong>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <input id="id-lab" name="id-lab" type="text" hidden>
                                        <!--OCULTO EL ID DEL PROVEDOR-->

                                        <div class="form-group">
                                            <input id="codigoModal" name="codigoModal" class="form-control" type="text"
                                                   placeholder="Codigo de Laboratorio">
                                        </div>
                                    </div>

                                    <br/>
                                </div>
                        </center>
                    </div>

                    <div class="modal-footer">
                        <div class="form-inline">
                            <center>
                                <div class="form-group">
                                    <button id="guardarMarca" class="btn btn-primary"
                                            onclick="return GuardarRegistro();" data-dismiss="modal" aria-hidden="true">
                                        Guardar
                                    </button>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger"
                                            data-dismiss="modal" aria-hidden="true">Cancelar
                                    </button>
                                </div>
                            </center>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <!-- MODAL PARA AGREGAR UNA PROVEEDOR"-->
    <div class="modal fade" id="modal-addProveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content ">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><b>Nueva Proveedor</b></h4>
                </div>
                <form id="formAddProveedor" name="formAddProveedor" class="formulario form-horizontal">
                    <br/>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><strong>NIT</strong></label>
                        <div class="col-lg-3">
                            <input type="text" maxlength="20"
                                   onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                   class="form-control" name="nitProveedorModal" id="nitProveedorModal"
                                   placeholder="Ingrese nro. de nit"/>
                        </div>

                        <label class="col-lg-2 control-label"><strong>Empresa</strong></label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" name="empresaProveedorModal"
                                   id="empresaProveedorModal" placeholder="Ingrese nombre de la Empresa"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"><strong>Nombre Contacto</strong></label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" name="nombreContactoProveedorModal"
                                   id="nombreContactoProveedorModal" placeholder="Ingrese el nombre del Vendedor"/>
                        </div>

                        <label class="col-lg-2 control-label"><strong>Nacionalidad</strong></label>
                        <div class="col-lg-3">
                            <?php
                            $datos = Database::getInstance()->getDb()->prepare("SELECT * FROM Pais");
                            $datos->execute();
                            ?>
                            <select name="paisProveedorModal" id="paisProveedorModal" class="col-lg-12">
                                <option value="0">SELECCIONE PAIS</option>
                                <?php while ($row = $datos->fetch()) { ?>
                                    <option value="<?php echo $row['idPais']; ?>">
                                        <?php echo $row['nombrePais']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label"><strong>Direccion</strong></label>
                        <div class="col-lg-3">
                            <input class="form-control" name="direccionProveedorModal" id="direccionProveedorModal"
                                   type="text" placeholder="Ingrese direccion domiciliar"/>
                        </div>
                        <label class="col-lg-2 control-label"><strong>Correo</strong></label>
                        <div class="col-lg-3">
                            <input class="form-control" name="emailProveedorModal" id="emailProveedorModal" type="text"
                                   placeholder="Ingrese correo electronico"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><strong>Telefono</strong></label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" name="telefonoProveedorModal"
                                   id="telefonoProveedorModal" maxlength="7"
                                   onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                   max placeholder="Ingrese nro de telefono"/>
                        </div>
                        <label class="col-lg-2 control-label"><strong>Celular</strong></label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" name="celularProveedorModal"
                                   id="celularProveedorModal" maxlength="8"
                                   onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
                                   placeholder="Ingrese nro de celular"/>
                        </div>
                    </div>

                    </br>

                    <div class="modal-footer">
                        <div class="form-inline">
                            <center>
                                <div class="form-group">
                                    <button id="btn-guardarProveedor" class="btn btn-primary"
                                            onclick="return GuardarRegistro();" data-dismiss="modal" aria-hidden="true">
                                        Guardar
                                    </button>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger"
                                            data-dismiss="modal" aria-hidden="true">Cancelar
                                    </button>
                                </div>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- MODAL PARA AGREGAR NUEVA UNIDAD DE MEDIDA-->
    <div class="modal fade" id="modal-addUnidadMedida" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><b>Nueva Unidad De Medida</b></h4>
                </div>
                <form id="formAddUnidadMedida" name="formAddUnidadMedida" class="formulario">
                    <div class="modal-body">

                        <center>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-xs-12">

                                    <div class="form-inline">

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"><h5><strong>Nombre</strong></h5>
                                            </label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="nombreMedida"
                                                   name="nombreMedida" placeholder="Nombre de laboratorio"/>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"><h5><strong>Abreviatura</strong></h5>
                                            </label>
                                        </div>
                                        <input id="id-Medida" name="id-Medida" type="text" hidden>
                                        <!--OCULTO EL ID DEL PROVEDOR-->

                                        <div class="form-group">
                                            <input id="abreviaturaMedida" name="abreviaturaMedida" class="form-control"
                                                   type="text" placeholder="Codigo de Laboratorio">
                                        </div>
                                    </div>

                                    <br/>
                                </div>
                        </center>
                    </div>

                    <div class="modal-footer">
                        <div class="form-inline">
                            <center>
                                <div class="form-group">
                                    <button id="guardarUnidadMedida" class="btn btn-primary"
                                            onclick="return GuardarRegistro();" data-dismiss="modal" aria-hidden="true">
                                        Guardar
                                    </button>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger"
                                            data-dismiss="modal" aria-hidden="true">Cancelar
                                    </button>
                                </div>
                            </center>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <?php
    require_once('../header_footer/footer.php');
    ?>

