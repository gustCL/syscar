<?php
require_once ('head.php');
require_once ('../datos/Database.php');

$id = $_GET['id'];


$dato = Database::getInstance()->getDb()->prepare("select idProveedor  from Provee p  where p.codigoProducto = '$id'  ");
$dato->execute();


$i = 0;
$dar = [];
while ($idCU = $dato->fetch()) {
    $dar[$i] = $idCU['idProveedor'];
    $i++;
}
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                Nuevo Producto

            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>

    <div class="main-container container-fluid">

        <div class="page-body">
            <div class="row">
                <div class="col-xs-12 col-md-12">

                    <div class="widget-body">


                        <center>
                            <h1>
                                Elija el Proveedor 

                            </h1>
                        </center>

                        <form   method="post" action="cambia_proveedor.php?id=<?= $id ?>" class="form-horizontal" >
                           


                            <table class="table table-striped table-bordered table-hover" id="simpledatatable">
                                <thead>
                                    <tr>
                                        <th>
                                            <label>
                                                <input type="checkbox" name="casouso[]" >
                                                <span class="text"></span>
                                            </label>
                                        </th>

                                        <th class="hidden-xs">
                                            Id.
                                        </th>

                                        <th class="hidden-xs">
                                            Nombre Contacto
                                        </th>
                                        <th class="hidden-xs">
                                            Empresa
                                        </th>
                                        <th class="hidden-xs">
                                            Nacionalidad
                                        </th> 


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //OBTENEMOS PROVEEDORES ACTIVOS
                                    $consulta = "select * from Pais pa, Proveedor pro  where pro.idPais = pa.idPais and estado = '1' ORDER BY pro.idProveedor DESC";
                                    require_once '../datos/Database.php';

                                    try {

                                        $comando = Database::getInstance()->getDb()->prepare($consulta);
                                        $comando->execute();
                                        while ($row = $comando->fetch()) {
                                            $idP = $row['idProveedor'];
                                            ?>
                                            <tr>
                                                <td>


                                                    <?php
                                                    $checked = "";

                                                    if (in_array($row['idProveedor'], $dar)) {


                                                        $checked = 'checked';
                                                    }
                                                    ?>
                                                    <label>
                                                        <input type="checkbox" name="casouso[]" value="<?= $row['idProveedor'] ?>" <?= $checked ?> >
                                                        <span class="text"> </span>
                                                    </label>


                                                </td>
                                                <td id = "idProveedor" name="idProvedor" >
                                                    <?= $idP ?>
                                                </td>

                                                <td class="hidden-xs">
                                                    <?= $row['nombreContacto'] ?>
                                                </td>
                                                <td>
                                                    <?= $row['nombreProve'] ?>
                                                </td>

                                                <td>
                                                    <?= $row['nombrePais'] ?>
                                                </td>


                                            </tr>
                                            <?php
                                        }//terminacion del while
                                    } catch (PDOException $e) {

                                        echo 'Error: ' . $e;
                                    }
                                    ?>    



                                </tbody>
                            </table>
                            
                                                    
                             <div class="form-group">
                                   &nbsp;&nbsp;&nbsp;&nbsp;
                                   &nbsp;&nbsp;&nbsp;&nbsp;
                                   &nbsp;&nbsp;&nbsp;&nbsp;
                                   &nbsp;&nbsp;&nbsp;&nbsp;
                                   &nbsp;&nbsp;&nbsp;&nbsp;
                               <input class="btn btn-primary"  type="submit" value="Actualizar "/>
                            </div>
                           
                        </form>


                        <form method="post" action="../lista_de_Producto/" class="form-horizontal" >

                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input class="btn btn-danger"  value="Cancelar" type="submit"  />
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>      


</div>


</div>

<?php
require_once ('../header_footer/footer.php');
?>

