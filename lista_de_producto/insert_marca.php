<?php
require_once("../datos/Database.php");
session_start();
ini_set("date.timezone", "America/La_Paz");
$fecha = date("d-m-Y");
$hoy = date("H:i:s");
$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];
$idSesion = $_SESSION['idSesion'];
$codigoLab = $_POST['codigoModal'];
$nombreMarca = $_POST['nombreMarcaModal'];

//inserta producto
$cmd_insertCategoria = Database::getInstance()->getDb()->prepare("INSERT INTO  Laboratorio VALUES(0,
                                                                                    :codLaboratorio,
                                                                         			:nombreLab,
                                                                         				0,
                                                                         				1)");
$cmd_insertCategoria->bindParam(':codLaboratorio', $codigoLab);
$cmd_insertCategoria->bindParam(':nombreLab', $nombreMarca);
$cmd_insertCategoria->execute();

$datos = Database::getInstance()->getDb()->prepare("SELECT * FROM Laboratorio");
$datos->execute();
?>
<select name="laboratorioMarca" id="laboratorioMarca"  data-bv-field="country" class="col-lg-12">                            
    <?php while ($row = $datos->fetch(PDO::FETCH_ASSOC)) { ?>
        <option value="<?php echo $row['idLaboratorio']; ?>">
            <?php echo $row['nombreLab']; ?>
        </option>
    <?php } ?>
</select>