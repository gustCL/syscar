<?php
require_once ('head.php');
require_once ('../datos/Database.php');

$id = $_GET['id'];
$dato = Database::getInstance()->getDb()->prepare("select *  from Producto where codigoProducto='$id' ");
$dato->execute();
$dat = $dato->fetch();

$con = Database::getInstance()->getDb()->prepare("Select nombrePais from Pais , Producto Where codigoProducto='$id' and idPais = idProcedencia ");
$con->execute();
$d = $con->fetch();
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                Ver Producto

            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>

    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget">                   
                                <form id="registrationForm" method="post" action="list_producto.php" class="form-horizontal">

                                    <div class="widget-body">
                                        <br>
                                        <br>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Codigo</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="codigo" value="<?= $dat['codigo'] ?>"  placeholder="Codigo" readonly  />                                           


                                            </div>                        

                                            <label class="col-lg-2 control-label">Cod. Barra</label>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="codigoBarra" value="<?= $dat['codigoBarras'] ?>"   placeholder="Codigo Barra" readonly />
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Procedencia</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="cod" value="<?= $d['nombrePais'] ?>"  readonly />

                                            </div>

                                            <label class="col-lg-2 control-label">Laboratorio</label>
                                            <div class="col-lg-3">
                                                <?php
                                                require_once("../datos/Database.php");
                                                $da = Database::getInstance()->getDb()->prepare("select nombreLab from Laboratorio l , Producto p where p.codigoProducto ='$id' and l.idLaboratorio = p.idLaboratorio ");
                                                $da->execute();
                                                $r = $da->fetch()
                                                ?>
                                                <input type="text" class="form-control" name="laboratorio" value="<?= $r['nombreLab'] ?>"  readonly />

                                            </div>
                                            <!--<a href="#" id="nuevo-laboratorio" class="btn btn-primary"  >Nuevo </a>-->
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Codigo de Fabrica</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="codigoF"  value="<?= $dat['codigoFabrica'] ?>"   placeholder="Codigo Fabrica" readonly />                           

                                            </div>     
                                            <label class="col-lg-2 control-label">Linea</label>
                                            <div class="col-lg-3">
                                                <?php
                                                require_once("../datos/Database.php");
                                                $d= Database::getInstance()->getDb()->prepare("select l.nombreLinea from Linea l, Producto p where p.codigoProducto = '$id' and p.idLinea = l.idLinea");
                                                $d->execute();
                                                $dd= $d->fetch();
                                                ?>
                                                <input type="text" class="form-control" name="linea"  value="<?= $dd['nombreLinea'] ?>"   placeholder=" Linea" readonly />                           

                                            </div>
                                            <!--<a href="nuevo_usuario.php" class="btn btn-primary" readonly >Nuevo</a>-->
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Registro Sanitario</label>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="registroSanitario" value=" <?= $dat['registroSanitario'] ?>"  placeholder="Reg. Sanitario" readonly/>

                                            </div> 
                                            <label class="col-lg-2 control-label">Proveedor</label>
                                            <div class="col-lg-4">
                                                <?php
                                                require_once("../datos/Database.php");
                                                $datos = Database::getInstance()->getDb()->prepare("select * from Proveedor pr , Provee p where p.idProveedor = pr.idProveedor and  p.codigoProducto ='$id'");
                                                $datos->execute();
                                                ?>
                                                <select class="form-control" name="proveedor" id="proveedor"  data-bv-field="country">                            
                                                    <?php while ($row = $datos->fetch()) { ?>
                                                        <option value="<?= $row['idProveedor'] ?>"><?= $row['nombreProve'] ?></option>
                                                    <?php } ?>
                                                </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>

                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Nombre Comercial</label>
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control"  name="nombreC" id="nombreC" value=" <?= $dat['nombreComercial'] ?>"  placeholder="Nombre comercial" readonly/>

                                            </div>
                                            <label class="col-lg-1 control-label">Factor</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="factor" id="factor" value=" <?= $dat['factor'] ?>"  placeholder="Factor" readonly />                                             

                                            </div>                        


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Nombre Generico</label>  
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control" name="nombreGenerico" id="nombreGenerico" value=" <?= $dat['nombreGenerico'] ?>"  placeholder="Nombre Generico" readonly />

                                            </div>
                                            <label class="col-lg-1 control-label">Precio de Costo</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="precioC" id="precioC" value=" <?= $dat['precioCosto'] ?>"  placeholder="Precio Costo" readonly />                             

                                            </div>     


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Accion Terapeutica</label>  
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control" name="accionT" id="accionT" value=" <?= $dat['accionTerapeutica'] ?>"  placeholder="Accion Terapeutica" readonly />

                                            </div>

                                            <label class="col-lg-1 control-label">Margen %</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="margen" id="margen" value=" <?= $dat['margen'] ?>"   placeholder="Margen" readonly />                                                  
                                            </div>       

                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Composicion</label>  
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control" name="composicion" id="composicion" value=" <?= $dat['composicion'] ?>"  placeholder="Composicion" readonly />

                                            </div>

                                            <label class="col-lg-1 control-label">Precio de Venta</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="precioV" id="precioV" value=" <?= $dat['precioVenta'] ?>"   placeholder="Precio Venta" readonly />                                  


                                            </div>       


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Unidad de Medida</label>
                                            <div class="col-lg-2">
                                                <?php
                                                require_once("../datos/Database.php");
                                                $ds = Database::getInstance()->getDb()->prepare("select nombreUM from UnidadMedida u , Producto p where u.idUnidad=p.idUnidad and p.codigoProducto='$id' ");
                                                $ds->execute();
                                                $ro = $ds->fetch()
                                                ?>
                                          <input type="text" class="form-control" name="unidad" id="unidad" value=" <?= $ro['nombreUM'] ?>"   placeholder="Precio Venta" readonly />                                  

                                            </div>      

                                            <label class="col-lg-2 control-label">Stock Minimo</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="stock" id="stock"  placeholder="Stock Minino"  value=" <?= $dat['stockMinimo'] ?>"   readonly />
                                            </div>       

                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Forma Farmaceutica</label>
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control"  name="formaFarma" id="formaFarma" placeholder="Forma Farmaceutica" value=" <?= $dat['formaFarmaceutica'] ?>" readonly
                                                       />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Imagen</label>

                                            <?php
                                            echo ' <img height="150" width="150" src="data:image;base64,'.$dat['imagen'] . ' "> ';
                                            ?>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div class="col-lg-offset-4 col-lg-4">
                                                <input class="btn btn-danger" type="submit" value="Cerrar" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<!-- FORMULARIO MODAL PARA EDITAR DATOS DE PROVEEDOR"-->
<div class="modal fade" id="editar-laboratorio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Laboratorio</b></h4>
            </div>

            <!--              aqui agregamos los registros-->

            <form id="formularioEdit" name="formularioEdit" class="formulario" >
                <div class="modal-body">

                    <center>
                        <div class="row" >
                            <div class="col-lg-12 col-sm-12 col-xs-12" >

                                <div class="form-inline" >

                                    <div class="form-group">
                                        Nombre de Laboratorio
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="labE" name="labE" placeholder="Nombre de laboratorio" />
                                    </div>                    
                                </div>   
                                <br />             
                                <div class="form-inline" >        
                                    <div class="form-group">
                                        Cod. Laboratorio 
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <input id="id-lab" name="id-lab"type="text" hidden><!--OCULTO EL ID DEL PROVEDOR-->

                                    <div class="form-group">
                                        <input id="codLab" name="codLab"class="form-control" type="text" placeholder="Numero de NIT">
                                    </div>

                                </div> 

                                <br/>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label no-padding-right" for="inputEmail3">Nacionalidad</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        $consultaP = "Select * from Pais";
                                        require_once("../datos/Database.php");
                                        $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
                                        $comandoP->execute();
                                        ?>

                                        <select  id='paisE' name="paisE" style='width:100%;'>
                                            <?php
                                            if ($comandoP->rowCount() > 0) {
                                                while ($Pais = $comandoP->fetch()) {
                                                    echo "<option value='$Pais[nombrePais]'/>$Pais[nombrePais]";
                                                }
                                            } else {
                                                echo "<option value='Central'/>Central";
                                            }
                                            ?>
                                        </select>


                                    </div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>  
                                <br/><br/>
                                <div id="mensaje"></div>
                                <!--BOTONES-->

                                <footer>
                                    <div class="form-inline" >                          
                                        <div class="form-group">

                                            <button  id="reg"class="btn btn-primary" onclick="return GuardarRegistro();" data-dismiss="modal" aria-hidden="true"hidden>Guardar</button>
                                        </div>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="form-group">
                                            <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                                        </div>

                                    </div>

                                </footer>

                            </div>
                    </center>
                </div>



            </form>
        </div>
    </div>
</div>


<!-- End Formulario Modal-->

<!-- FORMULARIO MODAL PARA EDITAR DATOS DE PROVEEDOR"-->
<div class="modal fade" id="editar-proveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Proveedor</b></h4>
            </div>

            <!--              aqui agregamos los registros-->

            <form id="formularioEditar" name="formularioEdit" class="formulario" >
                <div class="modal-body">

                    <center>
                        <div class="row" >
                            <div class="col-lg-12 col-sm-12 col-xs-12" >


                                <div class="form-inline" >

                                    <div class="form-group">
                                        Empresa
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="empresaE" name="empresaE" placeholder="Empresa" >
                                    </div>
                                    <div class="form-group">
                                        &nbsp; &nbsp; Nombre Contacto &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="contactoE" name="contactoE"  placeholder="Nombre contacto">
                                    </div>                     
                                </div>   
                                <br />             
                                <div class="form-inline" >        
                                    <div class="form-group">
                                        NIT 
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <input id="id-prov" name="id-prov"type="text" hidden><!--OCULTO EL ID DEL PROVEDOR-->
                                    <input type="text" required="required" readonly="readonly" id="pro" name="pro" hidden/>
                                    <div class="form-group">
                                        <input id="nitE" name="nitE"class="form-control" type="text" placeholder="Numero de NIT">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label no-padding-right" for="inputEmail3">Nacionalidad</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        $consultaP = "Select * from Pais";
                                        require_once("../datos/Database.php");
                                        $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
                                        $comandoP->execute();
                                        ?>

                                        <select  id='paisE' name="paisE" style='width:100%;'>
                                            <?php
                                            if ($comandoP->rowCount() > 0) {
                                                while ($Pais = $comandoP->fetch()) {
                                                    echo "<option value='$Pais[nombrePais]'/>$Pais[nombrePais]";
                                                }
                                            } else {
                                                echo "<option value='Central'/>Central";
                                            }
                                            ?>
                                        </select>


                                    </div>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;                        
                                </div> 

                                <br>
                                <div class="form-inline" >

                                    <div class="form-group">
                                        Direccion
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="direccionE" name="direccionE" placeholder="Direccion" >
                                    </div>
                                    <div class="form-group">
                                        &nbsp; &nbsp;  Correo &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="emailE" name="emailE"  placeholder="Correo">
                                    </div>                     
                                </div>   
                                <br />   
                                <div class="form-inline" >

                                    <div class="form-group">
                                        Telefono
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="telefonoE" name="telefonoE" placeholder="Celular" >
                                    </div>
                                    <div class="form-group">
                                        &nbsp; &nbsp;Celular &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  id="celularE" name="celularE"  placeholder="Celular">
                                    </div>                     
                                </div>   
                                <br>
                                <!--<span id="mensaje"></span>-->
                                <!--BOTONES-->

                                <footer>
                                    <div class="form-inline" >                          
                                        <div class="form-group">
                                            <button  class="btn btn-primary" onclick="return GuardarProveedor();" data-dismiss="modal" aria-hidden="true">Guardar</button>
                                        </div>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="form-group">
                                            <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                                        </div>

                                    </div>

                                </footer>

                            </div>
                    </center>
                </div>

            </form>    
        </div>

    </div>
</div>
</div>







<?php
require_once ('../header_footer/footer.php');
?>  

