<script type="text/javascript">
    $(document).ready(function () {
        $('#registrationForm')[0].reset();
    });
</script>
<?php
require_once('head.php');
require_once('../datos/Database.php');

$id = $_GET['id'];
$cmd_getProducto = Database::getInstance()->getDb()->prepare("SELECT *  FROM Producto WHERE codigoProducto = '$id' ");
$cmd_getProducto->execute();
$datosProducto = $cmd_getProducto->fetch();
$idActividad = $datosProducto['idActividad'];
$idLinea = $datosProducto['idLinea'];
$idUnidad = $datosProducto['idUnidad'];
$idLaboratorio = $datosProducto['idLaboratorio'];

$cmd_getPais = Database::getInstance()->getDb()->prepare("SELECT  * FROM Pais p, Producto pr WHERE pr.codigoProducto = '$id' AND p.idPais = pr.idProcedencia ");
$cmd_getPais->execute();
$datosPais = $cmd_getPais->fetch();

$cmd_getActividad = Database::getInstance()->getDb()->prepare("SELECT * FROM Actividad WHERE idActividad = '$idActividad'");
$cmd_getActividad->execute();
$datosActividad = $cmd_getActividad->fetch();


//obtenemos datos de linea por q hay s eguarda como categoria
$cmd_getLinea = Database::getInstance()->getDb()->prepare("SELECT * FROM Linea WHERE idLinea = '$idLinea'");
$cmd_getLinea->execute();
$datosLinea = $cmd_getLinea->fetch();


//obtenemos la unidada de medida 
$cmd_getMedida = Database::getInstance()->getDb()->prepare("SELECT * FROM UnidadMedida WHERE idUnidad = '$idUnidad'");
$cmd_getMedida->execute();
$datosMedida = $cmd_getMedida->fetch();

//sacamos datos del laboratorio q ha sido guardado como MARCA
$cmd_getLaboratorio = Database::getInstance()->getDb()->prepare("SELECT * FROM Laboratorio WHERE idLaboratorio = '$idLaboratorio'");
$cmd_getLaboratorio->execute();
$datosLaboratorio = $cmd_getLaboratorio->fetch();
//recuperamos el proveedor del producto
$cmd_getProveedor = Database::getInstance()->getDb()->prepare("SELECT pr.* FROM Provee p,Proveedor pr WHERE p.codigoProducto = '$id' AND p.idProveedor = pr.idProveedor");
$cmd_getProveedor->execute();
$datosProveedor = $cmd_getProveedor->fetch();
?>

<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>VER PRODUCTO</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>

    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget">
                                <form id="registrationForm" name="registrationForm" method="post"
                                      action="../lista_de_producto/" class="form-horizontal">
                                    <div class="widget-body">
                                        <div class="form-group">
                                            <center>

                                                <div class="form-group">
                                                    <center>
                                                        <label><h3><strong>Actividad</strong></h3></label><br>
                                                        <input value="<?= $datosActividad['nombre'] ?>" name="idAct"
                                                               id="idAct" class="btn btn-primary"
                                                               style="font-size:16px;" size="75%" readonly/>
                                                        <br>
                                                    </center>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label"><strong>Nombre
                                                            Comercial</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosProducto['nombreComercial'] ?>"
                                                               readonly/>
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label"><strong>Codigo</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" value="<?= $datosProducto['codigo'] ?>"
                                                               class="form-control" readonly/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label"><strong>Precio de
                                                            Costo</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosProducto['precioCosto'] ?>" readonly/>
                                                    </div>
                                                    <label class="col-lg-2 control-label"><strong>Precio de
                                                            Venta</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosProducto['precioVenta'] ?>" readonly/>
                                                    </div>
                                                </div>
                                               

                                                <div class="form-group">
                                                    <!-- categoria es misma linea -->
                                                    <label
                                                        class="col-lg-2 control-label"><strong>Categoria</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosLinea['nombreLinea']; ?>" readonly/>
                                                    </div>
                                                    <label class="col-lg-2 control-label"><strong>Unidad de
                                                            Medida</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosMedida['nombreUM']; ?>" readonly/>
                                                    </div>
                                                </div>
                                                <div class="form-group" hidden>
                                                    <label class="col-lg-2 control-label"><strong>Cod.
                                                            Barra</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" value="<?= $datosProducto['codigoBarras'] ?>"
                                                               class="form-control" readonly/>
                                                    </div>

                                                    <label class="col-lg-2 control-label"> <strong>Stock</strong>
                                                    </label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosProducto['stockMinimo'] ?>" readonly/>
                                                    </div>
                                                </div>

                                                <div class="form-group" hidden>
                                                    <label class="col-lg-2 control-label"><strong>Nombre
                                                            Abreviado</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosProducto['nombreGenerico'] ?>"
                                                               readonly/>

                                                    </div>
                                                    <label class="col-lg-2 control-label"><strong>Margen
                                                            %</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosProducto['margen'] ?>" readonly/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <!-- Laboratio es marca -->
                                                    <label class="col-lg-2 control-label"><strong>Marca</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosLaboratorio['nombreLab']; ?>" readonly/>
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label"><strong>Proveedor</strong></label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosProveedor['nombreProve']; ?>" readonly/>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-lg-2 control-label"><strong>País</strong></label>
                                                    <div class="col-lg-3">
                                                        <input value="<?= $datosPais['nombrePais'] ?>" type="text"
                                                               class="form-control" readonly/>
                                                    </div>
                                                    <label class="col-lg-2 control-label" hidden><strong>Equivalente</strong></label>
                                                    <div class="col-lg-3" hidden>
                                                        <input type="text" class="form-control"
                                                               value="<?= $datosProducto['accionTerapeutica']; ?>"
                                                               readonly/>
                                                    </div>
                                                </div>

                                                <center>
                                                    <div class="col-lg-offset-4 col-lg-4">
                                                        <input class="btn btn-danger" type="submit" value="Cerrar"/>
                                                    </div>
                                                </center>
                                            </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once('../header_footer/footer.php');
?>  

