<?php
 require_once("../datos/Database.php");
session_start();
ini_set("date.timezone", "America/La_Paz");
$fechaActualSistema = date("d-m-Y");
$horaActualSistema = date("H:i:s");
$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];
$idSesion = $_SESSION['idSesion'];

//datos recuperados 
    $nitProveedor = $_POST['nitProveedorModal'];
    $contactoProveedor = $_POST['nombreContactoProveedorModal'];
    $direccion = $_POST['direccionProveedorModal'];
    $telefono = $_POST['telefonoProveedorModal'];
    $empresa = $_POST['empresaProveedorModal'];
    $nacionalidad = $_POST['paisProveedorModal'];
    $correo = $_POST['emailProveedorModal'];
    $celular = $_POST['celularProveedorModal'];

//INSERTAR PROVEEDOR
$cmd_insertProveedor = Database::getInstance()->getDb()->prepare("INSERT INTO  Proveedor VALUES(0,
																						:nombreProve,
                                                                         				:nit,
                                                                                        :idPais,
                                                                                        :nombreContacto,
                                                                                        :fechaRegistro,
                                                                         				:horaRegistro,
                                                                                        1)");
$cmd_insertProveedor->bindParam(':nombreProve', $empresa);
$cmd_insertProveedor->bindParam(':nit', $nitProveedor);
$cmd_insertProveedor->bindParam(':idPais', $nacionalidad);
$cmd_insertProveedor->bindParam(':nombreContacto', $contactoProveedor);
$cmd_insertProveedor->bindParam(':fechaRegistro', $fechaActualSistema);
$cmd_insertProveedor->bindParam(':horaRegistro', $horaActualSistema);
$cmd_insertProveedor->execute();


//recuperamos el ultimo ID del provedor insertado
$query_ultimoProveedor = "SELECT * FROM Proveedor ORDER BY idProveedor DESC LIMIT 1";
$cmd_ultimoProveedor = Database::getInstance()->getDb()->prepare($query_ultimoProveedor);
$cmd_ultimoProveedor->execute();
$result_ultimoProveedor= $cmd_ultimoProveedor->fetch();
$id_ultimoProveedor = $result_ultimoProveedor['idProveedor'];
//INSERTAR TELEFONOS
    //telefono fijo
    $tipoTelefono = 1;
    $cmd_insertTelefono = Database::getInstance()->getDb()->prepare("INSERT INTO  Telefono VALUES(0,
                                                                                            :numeroTelefono,
                                                                                            :tipoTelefono,
                                                                                            :idProveedor)");
    $cmd_insertTelefono->bindParam(':numeroTelefono', $telefono);
    $cmd_insertTelefono->bindParam(':tipoTelefono', $tipoTelefono);
    $cmd_insertTelefono->bindParam(':idProveedor', $id_ultimoProveedor);
    $cmd_insertTelefono->execute();
    //telefono celular
    $tipoCel = 2;
    $cmd_insertCelular = Database::getInstance()->getDb()->prepare("INSERT INTO  Telefono VALUES(0,
                                                                                            :numeroTelefono,
                                                                                            :tipoTelefono,
                                                                                            :idProveedor)");
    $cmd_insertCelular->bindParam(':numeroTelefono', $celular);
    $cmd_insertCelular->bindParam(':tipoTelefono', $tipoCel);
    $cmd_insertCelular->bindParam(':idProveedor', $id_ultimoProveedor);
    $cmd_insertCelular->execute();
//inertanis Correo
$cmd_insertEmail = Database::getInstance()->getDb()->prepare("INSERT INTO  CorreoEmail VALUES(0,
                                                                                        :correo,
                                                                                        :idProveedor)");
$cmd_insertEmail->bindParam(':correo', $correo);
$cmd_insertEmail->bindParam(':idProveedor', $id_ultimoProveedor);
$cmd_insertEmail->execute();





$datos = Database::getInstance()->getDb()->prepare("SELECT * FROM Proveedor");
                                                    $datos->execute();

?>
<select name="provedorAdd" id="provedorAdd"  data-bv-field="country" class="col-lg-12">                            
                                                    <?php while ($row = $datos->fetch()) { ?>
                                                        <option value="<?php echo $row['idProveedor']; ?>"><?php echo $row['nombreProve']; ?></option>
                                                    <?php } ?>
                                                </select>