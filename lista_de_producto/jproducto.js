$(function () {
    $('#bs-inventario').on('keyup', function () {
        var dato = $('#bs-inventario').val();
        var url = 'busca_inventario.php';
//                 alert(dato);
        $.ajax({
            type: 'POST',
            url: url,
            data: 'dato=' + dato,
            success: function (datos) {
                $('#agrega-registros').html(datos);
            }
        });
        return false;
    });
    ///// BUSQUEDA DE PRODUCTO
    $('#bus-producto').on('keyup', function () {
        var dato = $('#bus-producto').val();
        var url = 'buscar_producto.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'dato=' + dato,
            success: function (datos) {
                $('#agrega-registros').html(datos);
            }
        });
        return false;
    });
//////////////////////MARGEN//////////////////////////
    ///FUNCION  USADA AL PONER MARGEN Funcion para margen, keyup actualzia los campos de precio venta y margen y viceversa
    $('#margen').on('keyup', function () {
        if ($('#precioC').val() != "") {//si existe algun dato en campo de costo $('#precioC').val()!=""
            var valorPorcentaje = parseFloat($('#margen').val()) / 100;//dividimos el margen entre 100

            var a = valorPorcentaje * parseFloat($('#precioC').val());
            $('#precioV').val(parseFloat($('#precioC').val()) + a);
        }
        ;
    });
    ///funcion al teclear en precioventa

    $('#precioV').on('keyup', function () {

        var costo = parseFloat($('#precioC').val());
        var venta = parseFloat($('#precioV').val()) * 100;
        costo = venta / costo;
        var mar = costo - 100;
        var ifventa = costo / 100;
        $('#margen').val(mar);
    });


//CATEGORIA
//modalmuestra Modal para agregar nueva categoria
    $('#btn-nuevaCategoria').on('click', function () {
        $('#formAddCategoria')[0].reset();
        $('#reg').show();
        $('#modal-addCategoria').modal({
            show: true,
            backdrop: 'static'
        });
    });


    $('#guardaCat').on('click', function () {
        if (validarCategoria()) {
            var frm = $("#formAddCategoria").serialize(); //FORMULARIO
            var url = 'insert_categoria.php';
            $.ajax({
                url: url,
                data: frm,
                type: 'post',
                success: function (registros) {
                    $('#agrega-datosCategoria').html(registros);
                    swal("Categoria Registrada!", "Categoria registrada correctamente", "success");
                }
            });
        } else {
            sweetAlert("DATOS INCORRECTOS", "Verifique sus datos", "error");
        }

    });


    //PROVEEDOR
    //muestra modal agregar nuevo proveedor
    $('#btn-addProveedor').on('click', function () {
        $('#formAddProveedor')[0].reset();
        $('#reg').show();
        $('#modal-addProveedor').modal({
            show: true,
            backdrop: 'static'
        });
    });
    //guardar datos del proveedor
    $('#btn-guardarProveedor').on('click', function () {
        if (validarProveedor()) {
            var frm = $("#formAddProveedor").serialize(); //FORMULARIO
            var url = 'insert_proveedor.php';
            $.ajax({
                url: url,
                data: frm,
                type: 'post',
                success: function (registros) {
                    $('#agrega-datosProveedor').html(registros);
                    swal("Proveedo Registrado!", "Proveedor registrado correctamente", "success");
                }
            });
        } else {
            sweetAlert("DATOS INCORRECTOS", "Verifique sus datos", "error");
        }

    });


    //MARCA
    //modal muestra nueva marca
    $('#btn-addMarca').on('click', function () {
        $('#formAddMarca')[0].reset();
        $('#reg').show();
        $('#modal-addMarca').modal({
            show: true,
            backdrop: 'static'
        });
    });
    //registrar marca
    $('#guardarMarca').on('click', function () {
        if (validarMarca()) {
            var frm = $("#formAddMarca").serialize(); //FORMULARIO
            var url = 'insert_marca.php';
            $.ajax({
                url: url,
                data: frm,
                type: 'post',
                success: function (registros) {
                    $('#agrega-datosMarca').html(registros);
                    swal("Marca Registrada!", "Marca registrada correctamente", "success");
                }
            });
        } else {
            sweetAlert("DATOS INCORRECTOS", "Verifique sus datos", "error");
        }

    });

    //UNIDAD DE MEDIDA
    //mostrar modal para Unidad de medida
    $('#btn-addUnitMedida').on('click', function () {
        $('#formAddUnidadMedida')[0].reset();
        $('#reg').show();
        $('#modal-addUnidadMedida').modal({
            show: true,
            backdrop: 'static'
        });
    });

    //registra unidad de medida
    $('#guardarUnidadMedida').on('click', function () {
        if (validarUnidadDeMedida()) {
            var frm = $("#formAddUnidadMedida").serialize(); //FORMULARIO
            var url = 'insert_unidadMedida.php';
            $.ajax({
                url: url,
                data: frm,
                type: 'post',
                success: function (registros) {
                    $('#agrega-datosUnidadMedida').html(registros);
                    swal("DATOS CORRECTOS !", "Unidad De Medida Registrada Correctamente", "success");
                }
            });
        } else {
            sweetAlert("DATOS INCORRECTOS", "Verifique sus datos", "error");
        }

    });

});
function GuardarRegistro() { //MODIFICA LOS DATOS DEL LABORATORIO 
    var frm = $("#formularioEdit").serialize();
    $.ajax({
        url: 'laboratorio_new.php',
        data: frm,
        type: 'post',
        success: function (registro) {

            $('#mensaje').addClass('bien').html('Edicion completada con exito').show(200).delay(2500).hide(200);
            $('#laboratorio').html(registro);

            return false;

        }

    });
    return false;
}
function mostrarDetalle(id) {

    $('#registrationForm')[0].reset();
    var url = 'detalle_proveedor.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function (valores) {
            var datos = eval(valores);
            $('#proveedorP').val(datos[0]);
            return false;
        }
    });
    return false;
}

function GuardarProveedor() { //MODIFICA LOS DATOS DEL LABORATORIO 
    var frm = $("#formularioEditar").serialize();
    $.ajax({
        url: 'nuevo_proveedor.php',
        data: frm,
        type: 'post',
    });
    return false;
}


//eliminarLOAD
function eliminarProd(id) {
    //var url = 'elimina_usuario.php';
    swal({
            title: "Eliminar Producto",
            text: "Está a punto de eliminar los registros de este Producto. ¿Desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: 'eliminar_Prod.php',
                    data: 'id=' + id,
                    success: function (registros) {
                        $('#agrega-registros').html(registros);
                        swal("Producto eliminado!", "El Producto ha sido removido de la base de datos", "success");
                    }
                });
            } else {
                swal("OPERACION CANCELADA", "Producto no eliminado", "error");
            }
        }
    );
}

///

function verificar_producto() { //MODIFICA LOS DATOS DEL LABORATORIO
    if (!validarCampos()) {
        sweetAlert("DATOS INCORRECTOS", "Verifique sus datos", "error");
    } else {
        var dato = $("#codigo").val();
        $.ajax({
            url: 'verificar_cod.php',
            data: 'dato=' + dato,
            type: 'post',
            success: function (valor) {
                var dato = eval(valor);
                var respuesta = dato[0];
                if(respuesta=='no existe'){
                    registrar_prod(respuesta);
                    swal({
                            title: "Registros de Producto Exitoso",
                            type: "success",
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true
                        },
                        function () {
                            redirect();
                        });
                    return false;
                }else if(respuesta=="existe"){
                    sweetAlert("DATOS INCORRECTOS", "EL CODIGO INGRESADO YA EXISTE", "error");

                    return false;

                }
            }

        });
        return false;
    }
}

function validarCampos() {
    if($("#nombreC").val()==''){
        return false;
    }
    if($("#precioV").val()==''){
        return false;
    }
    if($("#stockDiser").val()==''){
        return false;
    }
    return true;
}
function registrar_prod(respuesta) { //MODIFICA LOS DATOS DEL LABORATORIO 
    var frm = $("#nuevo_p").serialize(); //FORMULARIO
    if (respuesta == "no existe") {
        $.ajax({
            url: 'validar_producto.php',
            data: frm,
            type: 'post',
            success: function (respuesta) {
                //document.location.href='../lista_de_producto'
            }

        });
        return false;
    }
}
;

function redirect() {
    document.location.href = '../lista_de_producto'

}
;


//validaciones al registrar CATEGORIA
function validarCategoria() {
    var rpt = false;
    if ($('#codCategoria').val() != "" && $('#nombCategoria').val() != "") {
        rpt = true;
    }
    return rpt;
};

//validaciones marca
function validarMarca() {
    var rpt = false;
    if ($('#nombreMarcaModal').val() != "" && $('#codigoModal').val() != "") {
        rpt = true;
    }
    return rpt;
};
//validaciones proveedor
function validarProveedor() {
    rpt = false;
    if (
        $('#nitProveedorModal').val() != "" &&
        $('#empresaProveedorModal').val() != "" &&
        $('#nombreContactoProveedorModal').val() != "" &&
        parseFloat($('#paisProveedorModal').val()) != 0 &&
        $('#direccionProveedorModal').val() != "" &&
        $('#emailProveedorModal').val() != "" &&
        $('#emailProveedorModal').val() != "" &&
        $('#telefonoProveedorModal').val() != "" &&
        $('#celularProveedorModal').val()
    ) {
        rpt = true;
    }
    ;

    return rpt;
};
//validar Unidad de medida
function validarUnidadDeMedida() {
    rpt = false;
    if ($('#nombreMedida').val() != "" && $('#abreviaturaMedida').val()) {
        rpt = true;
    }
    return rpt;
}
