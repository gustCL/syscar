<?php
require_once ('head.php');

$id = $_GET['id'];
$dato = Database::getInstance()->getDb()->prepare("select p.codigoBarras , p.accionTerapeutica , p.codigoFabrica , p.codigoProducto , e.codigo , li.nombreLinea , p.formaFarmaceutica , p.nombreComercial , pa.nombrePais , e.nroLote , e.precioCosto , e.precioVenta , e.fechaVencimiento , s.nombreSector , a.nombreAlmacen ,l.nombreLab from Producto p , Existencias e , Ubicaciones u , Sectores s , Almacen a ,Laboratorio l , Pais pa , Linea li where p.idLinea = li.idLinea and  l.idLaboratorio = p.idLaboratorio  and  p.codigoProducto = e.codigoProducto and e.sector = s.idSector and s.idSector = u.idSector and pa.idPais = p.idProcedencia and a.idAlmacen = u.idAlmacen and p.codigoProducto= '$id' ");
$dato->execute();
$dar = $dato->fetch();
?>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                Inventario
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>

    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget">
                                <form id="registrationForm" method="post" action="index.php" class="form-horizontal"  >

                                    <div class="widget-body">
                                        <br>
                                        <br>
                                        
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Laboratorio</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="Laboratorio"  value="<?= $dar['nombreLab'] ?>" id="Laboratorio" readonly />                                           
                                            </div>                        

                                            <label class="col-lg-2 control-label">Procedencia</label>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="Procedencia" value="<?= $dar['nombrePais'] ?>" id="Procedencia" readonly />
                                            </div> 
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Linea</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="Linea"  value="<?= $dar['nombreLinea'] ?>"  id="Linea" readonly />                                           
                                            </div>                        

                                            <label class="col-lg-2 control-label">cod Fabrica</label>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="codFabrica" value="<?= $dar['codigoFabrica'] ?>" id="codFabrica" readonly/>
                                            </div> 
                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Proveedor</label>
                                            <div class="col-lg-2">
                                                <?php
                                                require_once("../datos/Database.php");
                                                $d = Database::getInstance()->getDb()->prepare("select * from Proveedor pr , Provee p where p.idProveedor = pr.idProveedor and  p.codigoProducto ='$id'");
                                                $d->execute();
                                                ?>
                                                <select  name="proveedor" id="proveedor"  data-bv-field="country">                            
                                                    <?php while ($row = $d->fetch()) { ?>
                                                        <option value="<?= $row['idProveedor'] ?>"><?= $row['nombreProve'] ?></option>
                                                    <?php } ?>
                                                </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>

                                            </div>
                                            <label class="col-lg-2 control-label">Unidad de Medida</label>
                                            <?php
                                            require_once("../datos/Database.php");
                                            $d = Database::getInstance()->getDb()->prepare("select * from Producto p , UnidadMedida u where p.idUnidad = u.idUnidad and  p.codigoProducto ='$id'");
                                            $d->execute();
                                            $valor = $d->fetch();
                                            ?>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="Unidad" value="<?= $valor['nombreUM'] ?>" id="Unidad" readonly />
                                            </div> 
                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Codigo </label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="Codigo" value="<?= $dar['codigo'] ?>"  id="Codigo" readonly />                                           
                                            </div>                        

                                            <label class="col-lg-2 control-label">Nro. Lote</label>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" value="<?= $dar['nroLote'] ?>" name="Lote" id="Lote"  readonly />
                                            </div> 
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Codigo Barra</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="CodigoB"  value="<?= $dar['codigoBarras'] ?>"  id="CodigoB" readonly />                                           
                                            </div>                        

                                            <label class="col-lg-2 control-label">Fecha Vencimiento</label>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" value="<?= $dar['fechaVencimiento'] ?>" name="FechaV" id="FechaV" readonly />
                                            </div> 
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"> Nombre Comercial</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="NombreC"  value="<?= $dar['nombreComercial'] ?>"  id="NombreC" readonly />                                           
                                            </div>                        

                                            <label class="col-lg-2 control-label">Cantidad</label>
                                            <?php
                                            require_once("../datos/Database.php");
                                            $sav = Database::getInstance()->getDb()->prepare("select * from  Lote  where  codigoProducto ='$id'");
                                            $sav->execute();
                                            $ver = $sav->fetch();
                                            ?>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="Cantidad" value="<?= $ver['cantidad'] ?>" id="Cantidad" readonly />
                                            </div> 
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"> Accion Terapeutica</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="AccionT" value="<?= $dar['accionTerapeutica'] ?>"   id="AccionT" readonly />                                           
                                            </div>                        

                                            <label class="col-lg-2 control-label">Precio Costo</label>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="PrecioC" value="<?= $dar['precioCosto'] ?>" id="PrecioC" readonly />
                                            </div> 
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"> Forma Farmaceutica</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="FormaF" value="<?= $dar['formaFarmaceutica'] ?>"   id="FormaF" readonly />                                           
                                            </div>                        

                                            <label class="col-lg-2 control-label">Precio Venta</label>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="PrecioV" value="<?= $dar['precioVenta'] ?>" id="PrecioV" readonly />
                                            </div> 
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"> Almacen</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="Almacen" value="<?= $dar['nombreAlmacen'] ?>"   id="Almacen" readonly />                                           
                                            </div>                        

                                            <label class="col-lg-2 control-label">Ubicacion</label>

                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="Ubicacion" value="<?= $dar['nombreSector'] ?>" id="Ubicacion" readonly />
                                            </div> 
                                        </div>
                                        

                                        <div class="form-group">
                                            <div class="col-lg-offset-4 col-lg-4">                                        
                                                <input class="btn btn-danger" type="submit" value="Cerrar" />
                                            </div>
                                        </div>



                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>



<?php
require_once ('../header_footer/footer.php');
?>

