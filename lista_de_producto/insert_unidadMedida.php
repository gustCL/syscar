<?php
require_once("../datos/Database.php");
session_start();

$nombreUnidadMedida = $_POST['nombreMedida'];
$abreviatura = $_POST['abreviaturaMedida'];

//inserta producto
$cmd_insertUnidadMeida = Database::getInstance()->getDb()->prepare("INSERT INTO  UnidadMedida VALUES(0,
                                                                                    :nombreUM,
                                                                         			:abreviatura)");
$cmd_insertUnidadMeida->bindParam(':nombreUM', $nombreUnidadMedida);
$cmd_insertUnidadMeida->bindParam(':abreviatura', $abreviatura);
$cmd_insertUnidadMeida->execute();

$datos = Database::getInstance()->getDb()->prepare("SELECT * FROM UnidadMedida");
$datos->execute();
?>

<select  name="medida" id="medida"  data-bv-field="country"class="col-lg-12">                            
	<?php while ($row = $datos->fetch(PDO::FETCH_ASSOC)) { ?>
	<option value="<?php echo $row['idUnidad'] ?>">
		<?php echo $row['nombreUM'] ?>
	</option>
	<?php } ?>
</select>