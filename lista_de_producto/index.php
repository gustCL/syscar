<?php
require_once ('head.php');
    $row_number = 1;

    $registro = "SELECT p.codigoProducto, p.codigoBarras,p.codigo, li.nombreLinea, p.nombreComercial, ps.precioVenta, SUM(e.stock) AS totalStock 
                  FROM Producto p, Linea li, PrecioSucursal ps, Ubicacion u, Existencias e, Lote lo 
                  WHERE p.idLinea = li.idLinea AND p.estado = 1 AND li.estado = 1 AND p.codigoProducto = lo.codigoProducto
                  AND lo.idLote = e.idLote AND p.codigoProducto = ps.codigoProducto AND p.codigoProducto = u.codigoProducto
                  AND e.idUbicacion = u.idUbicacion GROUP BY lo.codigoProducto ORDER BY p.nombreComercial ASC";
    $comando = Database::getInstance()->getDb()->prepare($registro);
    $comando->execute();
    ///PRODUCTOS REPETIDO POR  LOTE
    /*SELECT p.codigoProducto,p.codigoBarras,p.nombreComercial,p.precioVenta,ex.stock
                 FROM Producto p, Lote lo, Existencias ex, Ubicacion ub, PrecioSucursal ps
                 WHERE p.codigoProducto = lo.codigoProducto AND lo.idLote = ex.idLote
AND ub.codigoProducto = p.codigoProducto AND ps.codigoProducto = p.codigoProducto AND p.estado = 1 ORDER BY p.codigoProducto DESC */
?>
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong> LISTA DE PRODUCTOS</strong>

            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>

    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget">
                                <form  method="post" class="form-horizontal">
                                    <div class="widget-body">
                                        <br>
                                        <center>
                                            <table align="center">
                                         <tr>
                                             <td>
                                                 <span class="input-icon">
                                                            <input type="text" size="40" class="form-control"  placeholder="Buscar Producto por : Codigo , Descripcion " id="bus-producto"/>
                                                            <i class="glyphicon glyphicon-search circular blue"></i>

                                                        </span>
                                             </td>
                                             <td>&nbsp;&nbsp;</td>
                                             <td>
                                                 <a class="btn btn-warning shiny"  href="nuevo_producto.php" >
                                                        <i class="fa fa-plus">
                                                        </i> Nuevo Producto
                                                    </a>
                                             </td>
                                         </tr>
                                            </table>
                                            
                                        </center>    
                                        <br>                                        
                                    </div>
                                    <br>
                                    <div class="widget-body">
                                        <br>

                                        <!-- TABLA DE USUARIOS-->
                                        <div class="table-responsive" id="agrega-registros">        
                                            <table class="table table-striped" >
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <strong>NRO.</strong>
                                                        </th>
                                                        <th>
                                                            <strong>CODIGO</strong>
                                                        </th>
                                                        <th>
                                                            <strong>DESCRIPCION</strong>
                                                        </th>
                                                        <th>
                                                            <strong>STOCK</strong>
                                                        </th>
                                                        <th>
                                                            <strong>PRECIO VENTA</strong>
                                                        </th>
                                                        <!--Botones-->
                                                        <th>

                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php

                                                    try {
                                                        while ($row = $comando->fetch()) {
                                                            $idP = $row['codigoProducto'];
                                                            ?>
                                                            <tr>
                                                                <td id = "codigo" name="codigo" > 
                                                                    <?= $row_number ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['codigoBarras'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['nombreComercial'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['totalStock'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['precioVenta'] ?>
                                                                </td>

                                                                

                                                                <!--aqui estan los botones que hacen activar las vnetanas del modal-->
                                                                <td>                          
                                                                    <a href="ver_producto.php?id=<?= $idP ?>" class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                                                                    <a href="mod_producto.php?id=<?= $idP ?>"   class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                                                                    <a href="javascript:eliminarProd('<?= $idP ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $row_number = $row_number + 1;
                                                        }//terminacion del while
                                                    } catch (PDOException $e) {

                                                        echo 'Error: ' . $e;
                                                    }
                                                    ?>    
                                                </tbody>
                                            </table>
                                        </div>
                                        <br>
                                        <!--finnn de tablaa-->

                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>
                <!------- otroooooo----->                                            


                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->





    </div>
    <!-- finnn -->          
</div>



<?php
require_once ('../header_footer/footer.php');
?>  
