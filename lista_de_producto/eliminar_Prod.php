<?php
require_once("../datos/Database.php");
$id = $_POST['id'];

$elimina = Database::getInstance()->getDb()->prepare("UPDATE Producto SET estado=0 WHERE codigoProducto = '$id'");
$elimina->execute();

$registro = "select * from Producto where estado=1 ";
$dato1 = Database::getInstance()->getDb()->prepare($registro);
$dato1->execute();
$row_number = 1;
?>
<table class="table table-striped" >
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Nro 
                                                        </th>
                                                        <th>
                                                            Descripcion
                                                        </th>
                                                        <th>
                                                            Nombre Abreviado
                                                        </th>
                                                        <th>
                                                            Precio Venta
                                                        </th>
                                                        <th>
                                                            Codigo
                                                        </th>

                                                        <!--Botones-->
                                                        <th>

                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php

                                                    try {
                                                        while ($row = $dato1->fetch()) {
                                                            $idP = $row['codigoProducto'];
                                                            ?>
                                                            <tr>
                                                                <td id = "codigo" name="codigo" > 
                                                                    <?= $row_number ?>
                                                                </td>                                                                
                                                                <td>
                                                                    <?= $row['nombreComercial'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['nombreGenerico'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['precioVenta'] ?>
                                                                </td>

                                                                <td>
                                                                    <?= $row['codigo'] ?>
                                                                </td> 
                                                                

                                                                <!--aqui estan los botones que hacen activar las vnetanas del modal-->
                                                                <td>                          
                                                                    <a href="ver_producto.php?id=<?= $idP ?>" class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                                                                    <a href="mod_producto.php?id=<?= $idP ?>"   class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                                                                    <a href="javascript:eliminarProd('<?= $idP ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $row_number = $row_number + 1;
                                                        }//terminacion del while
                                                    } catch (PDOException $e) {

                                                        echo 'Error: ' . $e;
                                                    }
                                                    ?>    
                                                </tbody>
                                            </table>