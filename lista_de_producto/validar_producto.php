<?php 
 require_once("../datos/Database.php");
session_start();

$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];
$idSesion = $_SESSION['idSesion'];



$codigo =$_POST['codigo'];
$codigoB =$_POST['codigo'];
$procedencia =$_POST['pais'];
$codigoF =$_POST['codigoF'];
$linea =$_POST['linea'];
$forma_de_venta = $_POST['formaVenta'];
if($linea == null){
    $linea = 0;
}
$RegSanitario =$_POST['registroSanitario'];
$nombComercial =$_POST['nombreC'];
$factor =$_POST['factor'];
$nombGenerico =$_POST['nombreGenerico'];
$PVenta =$_POST['precioV'];
$accionTerapeutica =$_POST['accionT'];
$margen =$_POST['margen'];
$composicion =$_POST['composicion'];
$PCosto =$_POST['precioC'];
$unidadM =$_POST['medida'];
$stock = $_POST['stockDiser'];
$formaF =$_POST['formaFarma'];
//$impuestoICE = $_POST['impuestoICE'];
$marca = $_POST['laboratorioMarca'];//sacamos de laboratorio q se insertara como marca
$nombreAbreviado = $_POST['nombreGenerico'];//sacamos de nombreGenerico para guardar como su nombre abreviado;
$equivalente = $_POST['punteroEquivalente'];//sacamos el dato de formaFarma y lo pondremos como equivalente
$idActividad = $_POST['idActividad'];//obtenemos el id de la actividad
$categoria = $_POST['linea']; // sacamos de linea para guardarlo como si fuera una categria en la tabla de linea
$provedorAdd = $_POST['provedorAdd'];
$impuestoICE = $_POST['impuestoIce'];
if($categoria == null){
    $categoria = 0;
}
$image = "";

INI_SET("date.timezone", "America/La_Paz");
$fecha = DATE("Y-m-d");
$hoy = DATE("H:i:s");
//inserta producto

      //DATOS POR DEFECTO
     $cero = 0;
$cmd_lote_insert = Database::getInstance()->getDb()->prepare("INSERT INTO  Producto VALUES(
                                                                         '0',
                                                                         :codigo,
                                                                         :tipoProducto,
                                                                         :idActividad,
                                                                         :idUnidad,
                                                                         :idLinea,
                                                                         :idLaboratorio,
                                                                         :idProcedencia,
                                                                         :codigoBarras,
                                                                         :registroSanitario,
                                                                         :codigoFabrica,
                                                                         :nombreComercial,
                                                                         :nombreGenerico,
                                                                         :factor,
                                                                         :accionTerapeutica,
                                                                         :composicion,
                                                                         :stockMinimo,
                                                                         :formaFarmaceutica,
                                                                         :imagen,
                                                                         :precioCosto,
                                                                         :precioVenta,
                                                                         :margen,   
                                                                         :impuestoICE,
                                                                         1)");
$cmd_lote_insert->bindParam(':codigo', $codigo);
$cmd_lote_insert->bindParam(':tipoProducto', $forma_de_venta);
$cmd_lote_insert->bindParam(':idActividad', $idActividad);
$cmd_lote_insert->bindParam(':idUnidad', $unidadM);
$cmd_lote_insert->bindParam(':idLinea', $categoria);
$cmd_lote_insert->bindParam(':idLaboratorio', $marca);
$cmd_lote_insert->bindParam(':idProcedencia', $procedencia);
$cmd_lote_insert->bindParam(':codigoBarras', $codigoB);
$cmd_lote_insert->bindParam(':registroSanitario', $RegSanitario);
$cmd_lote_insert->bindParam(':codigoFabrica', $codigoF);
$cmd_lote_insert->bindParam(':nombreComercial', $nombComercial);
$cmd_lote_insert->bindParam(':nombreGenerico', $nombreAbreviado);
$cmd_lote_insert->bindParam(':factor', $factor);
$cmd_lote_insert->bindParam(':accionTerapeutica', $equivalente);
$cmd_lote_insert->bindParam(':composicion', $composicion);
$cmd_lote_insert->bindParam(':stockMinimo', $stock);
$cmd_lote_insert->bindParam(':formaFarmaceutica', $formaF);
$cmd_lote_insert->bindParam(':imagen', $image);
$cmd_lote_insert->bindParam(':precioCosto', $PCosto);
$cmd_lote_insert->bindParam(':precioVenta', $PVenta);
$cmd_lote_insert->bindParam(':margen', $margen);
$cmd_lote_insert->bindParam(':impuestoICE', $impuestoICE);
$cmd_lote_insert->execute();



///VAMOS A INSERTAR EN PROVEE EL ID DEL PROVEEDOR Y EL ID DEL PRODUCTO
//recuperamos el id del Ultimo Producto q lo veremos con el codigo
$codDat = Database::getInstance()->getDb()->prepare("SELECT codigoProducto FROM Producto WHERE codigo = '$codigo' ");
$codDat->execute();   
$d =  $codDat->fetch();
 
$id_prod = $d['codigoProducto'];


//registramos el 
$cmd_insertProvee = Database::getInstance()->getDb()->prepare("INSERT INTO  Provee VALUES(
                                                                         :codigoProducto,
                                                                         :idProveedor)");
$cmd_insertProvee->bindParam(':codigoProducto', $id_prod);
$cmd_insertProvee->bindParam(':idProveedor', $provedorAdd);
$cmd_insertProvee->execute();

/* $consulta ="INSERT INTO Producto VALUES(0,'$codigo','0','$unidadM','$linea','$laboratorio','$procedencia','$codigoB','$RegSanitario','$codigoF','$nombComercial','$nombGenerico','$factor','$accionTerapeutica','$composicion','$stock','$formaF','$image','$PCosto','$PVenta','$margen','$impuestoICE','1')";
 $dato =Database::getInstance()->getDb()->prepare($consulta);
 $dato->execute();*/

 //inserta la imagen

    
      
    
 //////////// BUSQUEDA DEL idTipoIngreso ///////////
$CONSULTA11 = "SELECT idTipoIngreso FROM TipoIngreso WHERE nombreTipoIng='Factura de Compras' ";
$DATO11 = Database::getInstance()->getDb()->prepare($CONSULTA11);
$DATO11->execute();
$ROW11 = $DATO11->fetch();
$idTipoing = $ROW11['idTipoIngreso'];



//////////// BUSQUEDA DEL TIPOCAMBIO ///////////
$CONSULTA22 = "SELECT * FROM TipoCambio t WHERE t.estado=1 ";
$DATO22 = Database::getInstance()->getDb()->prepare($CONSULTA22);
$DATO22->execute();
$ROW22 = $DATO22->fetch();
$tipocambiocompra = $ROW22['tipoCambioCompra'];
$tipocambioventa = $ROW22['tipoCambioVenta'];
$idTipoCambio = $ROW22['idTipoCambio'];

/*

$CONSULTA1 = " INSERT INTO FacturaIngreso (idTipoIngreso, idNotaIngreso, idProveedor,idTipoCambio, tipoCambioVenta, tipoCambioCompra, idInicioSesion, idSucursal,nroFactura, nroAutorizacion, nroControl, fecha, hora, glosa, montoSubTotal, descuento1, descuento2, descuento3, montoInteres, montoTotal, estado) "
        . "VALUES(?,'0','1',?,?,?,?,?,'11111','121212','141516',?,?,'SIN NOVEDAD','1545','12','15','15','155','155','1')";
$DATO1 = Database::getInstance()->getDb()->prepare($CONSULTA1);
$DATO1->execute([$idTipoing,$idTipoCambio, $tipocambioventa, $tipocambiocompra, $idSesion, $idSucursal, $fecha, $hoy ]);
//////////// OBTENER EL IDNOTAINGRESO ///////////////////////////////

$CONSULTA55 = "SELECT Max(idNotaIngreso) as idNota FROM FacturaIngreso  ";
$DATO55 = Database::getInstance()->getDb()->prepare($CONSULTA55);
$DATO55->execute();
$count = $DATO55->fetch(PDO::FETCH_ASSOC);*/
$idNotaIngreso = 1;



$CONSULTA23 = " INSERT INTO Lote ( idLote, nroLote, codigoProducto, idNotaIngreso, precioCosto, precioVenta, margen, fechaVencimiento, cantidad, observaciones, estado) VALUES ('0','XXX','$id_prod','$idNotaIngreso','$PCosto','$PVenta','10','$fecha','$stock','nada','1')";
$DATO23 = Database::getInstance()->getDb()->prepare($CONSULTA23);
$DATO23->execute();




    //////////// OBTENER EL idLote ///////////////////////////////
    $CONSULTA6 = "SELECT Max(idLote) as idLote FROM Lote  ";
    $DATO6 = Database::getInstance()->getDb()->prepare($CONSULTA6);
    $DATO6->execute();
    $count = $DATO6->fetch(PDO::FETCH_ASSOC);
    $idlote1 = $count['idLote'];

 


        $CONSULTA8 = "INSERT INTO Ubicacion(idUbicacion,idAlmacen,codigoProducto,idSector)VALUES "
                . "('0','1','$id_prod','1') ";
        $DATO8 = Database::getInstance()->getDb()->prepare($CONSULTA8);
        $DATO8->execute();

        $CONSULTA10 = "SELECT Max(idUbicacion) as idUbicacion FROM Ubicacion  ";
        $DATO10 = Database::getInstance()->getDb()->prepare($CONSULTA10);
        $DATO10->execute();
        $count = $DATO10->fetch(PDO::FETCH_ASSOC);
        $idUbica = $count['idUbicacion'];

        $CONSULTA9 = "INSERT INTO Existencias(idExistencia,idLote,idUbicacion,stock,terminado,estado)VALUES "
                . "('0','$idlote1','$idUbica','$stock','1','1') ";
        $DATO9 = Database::getInstance()->getDb()->prepare($CONSULTA9);
        $DATO9->execute();
    

  
      //INSERTA PrecioSucursal
     
      $consul_inser_pre ="INSERT INTO PrecioSucursal VALUES('$idSucursal','$id_prod','$PCosto','$PVenta','$fecha','$hoy')";
      $dato_inser_pre =Database::getInstance()->getDb()->prepare($consul_inser_pre);
      $dato_inser_pre->execute();
      exit();
      
?>
