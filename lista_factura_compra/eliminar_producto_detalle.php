<?php
session_start();
$idSucursal = $_SESSION['userMaster'];
$idS = $idSucursal['idSucursal'];
require_once("../datos/Database.php");
$codigo = $_POST['id'];
$idL = $_POST['idL'];
$id = $_POST['idN'];
$costo = 0;
$venta = 0;

/////////////////// ACTUALIZO LA TABLA EXISTENCIA
$cons1 = "UPDATE Existencias SET terminado = 0 , stock =0 WHERE idLote = '$idL' ";
$res1 = Database::getInstance()->getDb()->prepare($cons1);
$res1->execute();
//////////////////// ACTUALIZO LA  TABLA LOTE
$cons2 = "UPDATE Lote SET estado = 0 WHERE idLote = '$idL' ";
$res2 = Database::getInstance()->getDb()->prepare($cons2);
$res2->execute();

//////////////////////////// CONSULTA PARA LA TABLA

$cnn = "select * from FacturaIngreso f , Proveedor p  where f.idProveedor = p.idProveedor and p.estado = 1 and f.estado =1 and "
        . " f.nroFactura ='$id'   ORDER BY f.idNotaIngreso DESC  ";
$re = Database::getInstance()->getDb()->prepare($cnn);
$re->execute();
$row = $re->fetch();
$nrofila = 1;
$cant = 0;
$subto = 0;
$total = 0;
?>
<table id="grilla" class="table table-striped ">
    <thead>
        <tr>
            <th><strong>Nro</strong></th>
            <th><strong>Codigo</strong></th>
            <th><strong>Descripcion</strong></th>                                                    
            <th><strong>Precio Costo</strong></th>
            <th><strong>Precio Venta</strong></th>
            <th><strong>Margen</strong></th>
            <th><strong>Cantidad</strong></th>
            <th><strong>Nro Lote</strong></th>
            <th><strong>Fecha Vencimiento</strong></th>
            <th><strong>Monto</strong></th>
            <th><strong></strong></th>
        </tr>
    </thead>
    <tbody>
        <!--SE AGREGAN LAS FILAS DE LOS PRODUCTOS-->
        <?php
        $cons = " select p.codigoProducto , p.codigo , p.nombreComercial , l.precioCosto , l.idLote ,"
                . " l.precioVenta , l.margen , l.cantidad , l.nroLote ,l.fechaVencimiento , round((l.precioVenta * l.cantidad),2) AS Monto "
                . "from FacturaIngreso f , Lote l , Producto p   "
                . "where  f.idNotaIngreso = l.idNotaIngreso and "
                . "l.codigoProducto = p.codigoProducto and p.estado = 1 and "
                . "l.estado = 1 and f.estado =1 and "
                . " f.nroFactura = ?   ORDER BY f.idNotaIngreso DESC ";
        $dato = Database::getInstance()->getDb()->prepare($cons);
        $dato->execute([$id]);

        while ($row1 = $dato->fetch()) {
            $idP = $row1['codigoProducto'];
            ?>
            <tr>
                <td>
                    <?= $nrofila ?>
                </td>
                <td>
                    <?= $row1['codigo'] ?>
                </td>
                <td>
                    <?= $row1['nombreComercial'] ?>
                </td>
                <td>
                    <input type="text" class="form-control" id="precioC<?= $nrofila ?>" name="precioC<?= $nrofila ?>"  value="<?= $row1['precioCosto'] ?>" onkeyup="calculoPrecioVenta(<?= $nrofila ?>)" >  
                </td>
                <td>
                    <input type="text" class="form-control" id="precioV<?= $nrofila ?>" name="precioV<?= $nrofila ?>" value="<?= $row1['precioVenta'] ?>" onkeyup="calculoMargen(<?= $nrofila ?>)"  > 
                </td>
                <td>
                    <input type="text" class="form-control" id="margen<?= $nrofila ?>" name="margen<?= $nrofila ?>" value="<?= $row1['margen'] ?>"  onkeyup="calculoPrecioVenta(<?= $nrofila ?>)" >
                </td>
                <td>
                    <input size="5" type="text" class="form-control" id="cantidad<?= $nrofila ?>" name="cantidad<?= $nrofila ?>" value="<?= $row1['cantidad'] ?>" onkeyup="calculoPrecioVenta(<?= $nrofila ?>)"  >
                </td>
                <td>
                    <input size="35" type="text" class="form-control" id="nrolote<?= $nrofila ?>" name="nrolote<?= $nrofila ?>" value="<?= $row1['nroLote'] ?>" >
                </td>
                <td>
                    <input size="30" class="form-control date-picker" id="fecha<?= $nrofila ?>" name="fecha<?= $nrofila ?>" type="text" placeholder="Fecha" value="<?= $row1['fechaVencimiento'] ?>" data-date-format="yyyy-mm-dd"/>
                </td>
                <td>
                    <input type="text" size="30" class="form-control" id="monto<?= $nrofila ?>" name="monto<?= $nrofila ?>" value=" <?= $row1['Monto'] ?>"  >
                </td>
                <td>
                    <a href="javascript:eliminarProductoDetalle('<?= $idP ?>','<?= $id ?>','<?= $row1['idLote'] ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                </td>
        <input type="text" id="idLote<?= $nrofila ?>" name="idLote<?= $nrofila ?>" value="<?= $row1['idLote'] ?>" size="8" hidden/>
        <input type="text" id="codigo<?= $nrofila ?>" name="codigo<?= $nrofila ?>" value="<?= $idP ?>" size="8" hidden/>
    </tr> 

    <?php
    $cant = $nrofila;
    $nrofila = $nrofila + 1;

    $subto = $subto + $row1['Monto'];
}
//   $subto = $subto + $row1['Monto'];
?>
</tbody>

<tfoot>
<input type="text" id="cant" name="cant" value="<?= $cant ?>" size="8" hidden/>
<tr>

    <td colspan="8" class="invisible bg-snow"></td>
    <td ><strong>Sub Total</strong></td>
    <td class="text-center"> <input type="text" id="subTotal"  class="form-control"  name="subTotal" value="<?= $subto ?>" size="8" onkeyup="montSubtotal()" /> </td>
</tr>
<tr>
    <td colspan="8" class="invisible bg-snow"></td>
    <td >Descuento BS</td>
    <td class="text-center"><input type="text" id="descuento1"  class="form-control"  value="<?= $row['descuento1'] ?>" name="descuento1" size="8" onkeyup="descuent1()" /></td>
</tr>
<tr>
    <td colspan="8" class="invisible bg-snow"></td>
    <td >Descuento BS</td>
    <td class="text-center"><input type="text" id="descuento2"  class="form-control"  value="<?= $row['descuento2'] ?>"  name="descuento2" size="8" onkeyup="descuent2()" /></td>
</tr>
<tr>
    <td colspan="8" class="invisible bg-snow"></td>
    <td >Descuento BS</td>
    <td class="text-center"><input type="text" id="descuento3"  class="form-control"  value="<?= $row['descuento3'] ?>"  name="descuento3" size="8" onkeyup=" descuent3()" /></td>
</tr>
<tr>
    <td colspan="8" class="invisible bg-snow"></td>
    <td> Interes / Costo Adicional</td>
    <td class="text-center"><input type="text" id="montoInteres"  class="form-control"  value="<?= $row['montoInteres'] ?>"  name="montoInteres" size="8" onkeyup="interes()" /></td>
</tr>
<tr>
    <?php
    $total = $subto - $row['descuento1'] - $row['descuento2'] - $row['descuento3'] - $row['montoInteres'] + $row['montoInteres'];
    ?>
    <td colspan="8" class="invisible bg-snow"></td>
    <td class="text-center"><strong>Total</strong></td>
    <td class="text-center "><strong><input type="text"  class="form-control"  id="total" name="total" value="<?= $total ?>" size="8" /></strong></td>
</tr>
</tfoot>
</table>  
