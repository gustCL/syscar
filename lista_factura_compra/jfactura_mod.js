
function descuentos() {
   
    var subTotal = $('#subTotal').val();
    var descuento1 = $('#descuento1').val();
    var descuento2 = $('#descuento2').val();
    var descuento3 = $('#descuento3').val();

    var montoCredito = $('#montoCredito').val();
    var saldo = $('#saldo').val();
    
    var contado = $('#contado').val();
    var Interes = $('#montoInteres').val();
    var total = subTotal;
    if ($.isNumeric(descuento1)>0 && $.isNumeric(subTotal)) {
        descuento1 = parseFloat(descuento1);
        subTotal = parseFloat(subTotal);
        total = total - descuento1 ;
        $("#total").val(total.toFixed(2));

    
     }
     if ($.isNumeric(descuento2)>0 && $.isNumeric(subTotal)) {
        descuento2 = parseFloat(descuento2);
        subTotal = parseFloat(subTotal);
        total = total - descuento2 ;
        $("#total").val(total.toFixed(2));   
     }
     if ($.isNumeric(descuento3)>0 && $.isNumeric(subTotal)) {
        descuento3 = parseFloat(descuento3);
        subTotal = parseFloat(subTotal);
        total = total - descuento3 ;
        $("#total").val(total.toFixed(2));   
     }
     if ($.isNumeric(Interes)>0 && $.isNumeric(subTotal)) {
        Interes = parseFloat(Interes);
        subTotal = parseFloat(subTotal);
        total = total + Interes ;
        $("#total").val(total.toFixed(2));   
     }
           
        if (total >= contado ) {
            saldo = total - contado;
            montoCredito = saldo;
          
            $("#saldo").val(saldo.toFixed(2));
            $("#montoCredito").val(montoCredito.toFixed(2));
           
        } else {
            saldo = contado - total;
           
            montoCredito = 0;
            saldo = 0;
            $("#saldo").val(saldo.toFixed(2));
            $("#montoCredito").val(montoCredito.toFixed(2));
           
        }
        if(descuento1 == 0 && descuento2 == 0 && descuento3 ==0 ){
           $('#total').val($('#subTotal').val()); 
       }

   

    return false;

}




function calculoPrecioVenta(id) {
    var margen = $('#margen' + id).val();
    var precioC = $('#precioC' + id).val();
    var precioV = $('#precioV' + id).val();
    var cantidad = $('#cantidad' + id).val();

    var montoCredito = $('#montoCredito').val();
    var saldo = $('#saldo').val();
  
    var contado = $('#contado').val();
    var montoInteres = $('#montoInteres').val();
    if ($.isNumeric(margen) && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        margen = parseFloat(margen);
        precioC = parseFloat(precioC);
        precioV = parseFloat(precioV);

        precioV = (((margen * precioC) / 100) + precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
        $("#precioV" + id).val(precioV.toFixed(2));
        $("#monto" + id).val(monto.toFixed(2));
        var cant = $('#cant').val();
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i <= cant; i++) {
            monto = $('#monto' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        $("#subTotal").val(subTotal.toFixed(2));

        var descuento1 = $('#descuento1').val();
        var descuento2 = $('#descuento2').val();
        var descuento3 = $('#descuento3').val();
        var total = 0;
        descuento1 = parseFloat(descuento1);
        descuento2 = parseFloat(descuento2);
        descuento3 = parseFloat(descuento3);
        montoInteres = parseFloat(montoInteres);
        total = subTotal - descuento1 - descuento2 - descuento3 + montoInteres;
        $("#total").val(total.toFixed(2));

        montoCredito = parseFloat(montoCredito);
        saldo = parseFloat(saldo);
       
        contado = parseFloat(contado);

        if (total >= contado) {
            saldo = total - contado;
            montoCredito = saldo;
          
            $("#saldo").val(saldo.toFixed(2));
            $("#montoCredito").val(montoCredito.toFixed(2));
            
        } else {
            saldo = contado - total;
            saldofavor = saldo;
            montoCredito = 0;
            saldo = 0;
            $("#saldo").val(saldo.toFixed(2));
            $("#montoCredito").val(montoCredito.toFixed(2));
            
        }

    }

    return false;

}

function calculoMargen(id) {
    var subTotal = $('#subTotal').val();
    var precioC = $('#precioC' + id).val();
    var precioV = $('#precioV' + id).val();
    var cantidad = $('#cantidad' + id).val();

    var montoCredito = $('#montoCredito').val();
    var saldo = $('#saldo').val();
   
    var contado = $('#contado').val();
    var montoInteres = $('#montoInteres').val();
    if ($.isNumeric(precioV) && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        precioV = parseFloat(precioV);
        precioC = parseFloat(precioC);
        margen = (((precioV - precioC) * 100) / precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
        $("#margen" + id).val(margen.toFixed(2));
        $("#monto" + id).val(monto.toFixed(2));
        var cant = $('#cant').val();
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i <= cant; i++) {
            monto = $('#monto' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        $("#subTotal").val(subTotal.toFixed(2));

        var descuento1 = $('#descuento1').val();
        var descuento2 = $('#descuento2').val();
        var descuento3 = $('#descuento3').val();
        var total = 0;
        descuento1 = parseFloat(descuento1);
        descuento2 = parseFloat(descuento2);
        descuento3 = parseFloat(descuento3);
        montoInteres = parseFloat(montoInteres);
        total = subTotal - descuento1 - descuento2 - descuento3 + montoInteres;
        $("#total").val(total.toFixed(2));

        montoCredito = parseFloat(montoCredito);
        saldo = parseFloat(saldo);
        
        contado = parseFloat(contado);


        if (total >= contado) {
            saldo = total - contado;
            montoCredito = saldo;
            
            $("#saldo").val(saldo.toFixed(2));
            $("#montoCredito").val(montoCredito.toFixed(2));
            
        } else {
            saldo = contado - total;
            
            montoCredito = 0;
            saldo = 0;
            $("#saldo").val(saldo.toFixed(2));
            $("#montoCredito").val(montoCredito.toFixed(2));
            
        }

    }

    return false;
}
function eliminarFactura (id){
    var url = 'eliminar_factura.php';

    var pregunta = confirm('¿Esta seguro de eliminar ?');
    if (pregunta == true) {
        $.ajax({
            type: 'POST',
            url: url,
            data: 'id=' + id ,
            success: function (registro) {
                $('#agrega-registros').html(registro);
                return false;
            }
        });
        return false;
    } else {
        return false;
    }
}
function eliminarProductoDetalle(id, idN, idl) {
    var url = 'eliminar_producto_detalle.php';

    var pregunta = confirm('¿Esta seguro de eliminar este Producto?');
    if (pregunta == true) {
        $.ajax({
            type: 'POST',
            url: url,
            data: 'id=' + id + '&idN=' + idN + '&idL=' + idl,
            success: function (registro) {
                $('#registros-tabla').html(registro);
                return false;
            }
        });
        return false;
    } else {
        return false;
    }
}



/////////////////// buscar proveedor ///////////////
function fn_buscarP() {

    var frm = $("#frm_buscarP").serialize();
    $.ajax({
        url: 'datosP.php',
        data: frm,
        type: 'post',
        success: function (registro) {


            $("#nit").val("");
            $("#nombreProve").val("");
            $('#div_datosP').html(registro); //dataTable > tbody:first 
            

            return false;
        }
    });
    return false;
}

function interes() {

    var subTotal = $('#subTotal').val();
    var descuento1 = $('#descuento1').val();
    var descuento2 = $('#descuento2').val();
    var descuento3 = $('#descuento3').val();


    var montoCredito = $('#montoCredito').val();
    var saldo = $('#saldo').val();
    var saldofavor = $('#saldofavor').val();
    var contado = $('#contado').val();
    var montoInteres = $('#montoInteres').val();
    var total = 0;
    if ($.isNumeric(descuento1) && $.isNumeric(descuento2) && $.isNumeric(descuento3) && $.isNumeric(montoInteres) > 0) {
        descuento1 = parseFloat(descuento1);
        descuento2 = parseFloat(descuento2);
        descuento3 = parseFloat(descuento3);
        subTotal = parseFloat(subTotal);
        montoInteres = parseFloat(montoInteres);

        total = subTotal - descuento1 - descuento2 - descuento3 + montoInteres;

        $("#total").val(total.toFixed(2));

        montoCredito = parseFloat(montoCredito);
        saldo = parseFloat(saldo);
        saldofavor = parseFloat(saldofavor);
        contado = parseFloat(contado);

        if (total >= contado) {
            saldo = total - contado;
            montoCredito = saldo;
            saldofavor = 0;
            $("#saldo").val(saldo.toFixed(2));
            $("#montoCredito").val(montoCredito.toFixed(2));
            $("#saldofavor").val(saldofavor.toFixed(2));
        } else {
            saldo = contado - total;
            saldofavor = saldo;
            montoCredito = 0;
            saldo = 0;
            $("#saldo").val(saldo.toFixed(2));
            $("#montoCredito").val(montoCredito.toFixed(2));
            $("#saldofavor").val(saldofavor.toFixed(2));
        }

    }

    return false;

}
