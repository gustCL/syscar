
$(function () {
    $('#nrofact').on('keyup', function () {
        var dato1 = $('#nrofact').val();
        var dato2 = $('#nroControl').val();
        var dato3 = $('#nroAuto').val();

        if (dato1 != '') {
            if (dato2 != '') {
                if (dato3 != '') {
                    $('#compra_saldo').removeAttr('readonly');
                    $('#compra_credito').removeAttr('readonly');
                    $('#compra_montoInteres').removeAttr('readonly');
                    $('#compra_fechaPago').removeAttr('readonly');
                    $('#compra_contado').removeAttr('readonly');

                }
            }
        } else {

            $('#compra_saldo').attr('readonly', true);
            $('#compra_credito').attr('readonly', true);
            $('#compra_montoInteres').attr('readonly', true);
            $('#compra_fechaPago').attr('readonly', true);
            $('#compra_contado').attr('readonly', true);
        }

    });

    $('#nroControl').on('keyup', function () {
        var dato1 = $('#nroControl').val();
        var dato2 = $('#nroAuto').val();
        var dato3 = $('#nrofact').val();

        if (dato1 != '') {
            if (dato2 != '') {
                if (dato3 != '') {
                    $('#compra_saldo').removeAttr('readonly');
                    $('#compra_credito').removeAttr('readonly');
                    $('#compra_montoInteres').removeAttr('readonly');
                    $('#compra_fechaPago').removeAttr('readonly');
                    $('#compra_contado').removeAttr('readonly');

                }
            }
        } else {

            $('#compra_saldo').attr('readonly', true);
            $('#compra_credito').attr('readonly', true);
            $('#compra_montoInteres').attr('readonly', true);
            $('#compra_fechaPago').attr('readonly', true);
            $('#compra_contado').attr('readonly', true);
        }

    });

    $('#nroAuto').on('keyup', function () {
        var dato1 = $('#nroAuto').val();
        var dato2 = $('#nrofact').val();
        var dato3 = $('#nroControl').val();

        if (dato1 != '') {
            if (dato2 != '') {
                if (dato3 != '') {
                    $('#compra_saldo').removeAttr('readonly');
                    $('#compra_credito').removeAttr('readonly');
                    $('#compra_montoInteres').removeAttr('readonly');
                    $('#compra_fechaPago').removeAttr('readonly');
                    $('#compra_contado').removeAttr('readonly');

                }
            }
        } else {

            $('#compra_saldo').attr('readonly', true);
            $('#compra_credito').attr('readonly', true);
            $('#compra_montoInteres').attr('readonly', true);
            $('#compra_fechaPago').attr('readonly', true);
            $('#compra_contado').attr('readonly', true);
        }

    });
    $('#nroPedido').on('keyup', function () {

        if ($('#nroPedido').val() != '') {
            $('#nrofact').removeAttr('readonly');
            $('#almacen').removeAttr('readonly');
            $('#nroAuto').removeAttr('readonly');
            $('#nroControl').removeAttr('readonly');




        } else {
            $('#nrofact').attr('readonly', true);
            $('#almacen').attr('readonly', true);
            $('#nroAuto').attr('readonly', true);
            $('#nroControl').attr('readonly', true);

        }

    });
   
   $('#compra_contado').on('keyup', function () {
        var contado = $('#compra_contado').val();
        var total = $('#total_compra').val();
        contado = parseFloat(contado);
        total = parseFloat(total);
        if (contado > total) {
            alert("Dato erroneo ingresde de nuevo");
            $('#compra_contado').val("");
        }
        if (contado > 0 || total == contado) {
            $('#guardarCompra').attr('disabled', false);

        } else {
            $('#guardarCompra').attr('disabled', true);
        }

    });


});

function validarNumeroVentas(event) {//Para validar numeros de cantidad de producto
    if (event.keyCode == 13) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }

}


function validarNumeroVentas2(event) {//Para validar numeros de margen en bs y %
    if (event.keyCode == 46) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }
}

function cal_PrecioV() {
    var margen = $('#margen').val();
    var precioC = $('#precioC').val();
    var precioV = $('#precioV').val();
    var cantidad = $('#cantidad').val();
    var monto = 0;
    if ($.isNumeric(precioC) > 0) {
        margen = parseFloat(margen);
        precioC = parseFloat(precioC);
        precioV = parseFloat(precioV);

        precioV = (((margen * precioC) / 100) + precioC);

        $("#precioV").val(precioV.toFixed(2));
        cantidad = parseFloat(cantidad);
        if (cantidad > 0) {
            monto = (precioV * cantidad);
            $("#totalProducto").val(monto.toFixed(2));
            subTotal = subTotal + monto;
        }

    }
    return false;
}

function cal_Margen() {
    var margen = $('#margen').val();
    var precioC = $('#precioC').val();
    var precioV = $('#precioV').val();
    var cantidad = $('#cantidad').val();
    var monto = 0;
    margen = parseFloat(margen);
    if (margen > 0) {
        precioV = parseFloat(precioV);
        precioC = parseFloat(precioC);
        margen = (((precioV - precioC) * 100) / precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
        $("#margen").val(margen.toFixed(2));
        $("#totalProducto").val(monto.toFixed(2));

    }

    return false;
}

function cal_cantidad() {
    var cantidad = $('#cantidad').val();
    var precioCosto = $('#precioC').val();
    var precioVenta = $('#precioV').val();
    var totalprod = 0;
    if ($.isNumeric(cantidad) > 0 && $.isNumeric(precioCosto) && $.isNumeric(precioVenta)) {
        cantidad = parseFloat(cantidad);
        precioCosto = parseFloat(precioCosto);
        totalprod = cantidad * precioCosto;
        $("#totalProducto").val(totalprod.toFixed(2));
    }
    return false;
}




function cal_Precio_Venta(id) {
    var margen = $('#margenT' + id).val();
    var precioC = $('#precioCT' + id).val();
    var precioV = $('#precioVT' + id).val();
    var cantidad = $('#cantidadT' + id).val();
    var monto = 0;
    if ($.isNumeric(margen) && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        margen = parseFloat(margen);
        precioC = parseFloat(precioC);
        precioV = parseFloat(precioV);

        precioV = (((margen * precioC) / 100) + precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioV * cantidad);
        $("#precioV" + id).val(precioV.toFixed(2));
        $("#totalProducto" + id).val(monto.toFixed(2));

    }

    return false;

}


function descuentos() {
    var subTotal = $('#subTotal').val();
    var descuento1 = $('#compra_descuento1').val();
    var descuento2 = $('#compra_descuento2').val();
    var descuento3 = $('#compra_descuento3').val();

    var saldo = $('#compra_saldo').val();
    var montoInteres = $('#compra_montoInteres').val();
    var credito = $('#compra_credito').val();
    var contado = $('#compra_contado').val();
    subTotal = parseFloat(subTotal);
    var total = subTotal;

    if ($.isNumeric(descuento1) > 0) {
        descuento1 = parseFloat(descuento1);
        total = total - descuento1;
        $("#total_compra").val(total.toFixed(2));
        saldo = total;
        $("#compra_saldo").val(saldo.toFixed(2));

    }
    if ($.isNumeric(descuento2) > 0) {
        descuento2 = parseFloat(descuento2);
        total = total - descuento2;
        $("#total_compra").val(total.toFixed(2));
        saldo = total;
        $("#compra_saldo").val(saldo.toFixed(2));

    }
    if ($.isNumeric(descuento3) > 0) {
        descuento3 = parseFloat(descuento3);
        total = total - descuento3;
        $("#total_compra").val(total.toFixed(2));
        saldo = total;
        $("#compra_saldo").val(saldo.toFixed(2));

    }
    if ($.isNumeric(montoInteres) > 0) {
        montoInteres = parseFloat(montoInteres);
        total = total + montoInteres;
        $("#total_compra").val(total.toFixed(2));
        saldo = total;
        $("#compra_saldo").val(saldo.toFixed(2));

    }

    if ($.isNumeric(contado) > 0) {

        credito = parseFloat(credito);
        contado = parseFloat(contado);
        total = parseFloat(total);
        if (total >= contado) {
            saldo = total - contado;
            $('#compra_credito').val(saldo.toFixed(2));
            $('#compra_saldo').val(saldo.toFixed(2));
            if (total == contado) {

                $('#compra_fechaPago').attr('readonly', true);
            }
        }

    } else {
        total = parseFloat(total);
        $('#compra_saldo').val(total.toFixed(2));
        $('#compra_contado').val();
        $('#compra_credito').val('0');
        $('#compra_fechaPago').removeAttr('readonly');
    }
    return false;
}

function contado_compra() {
    var saldo = $('#compra_saldo').val();
    var total = $("#total_compra").val();

    var credito = $('#compra_credito').val();
    var contado = $('#compra_contado').val();
    if ($.isNumeric(contado) > 0) {
        saldo = parseFloat(saldo);
        credito = parseFloat(credito);
        contado = parseFloat(contado);
        total = parseFloat(total);
        if (contado > total) {
            total = parseFloat(total);

            $('#compra_saldo').val('0');
            $('#compra_credito').val('0');

        } else {

            total = parseFloat(total);
            saldo = total - contado;
            $('#compra_credito').val(saldo.toFixed(2));
            $('#compra_saldo').val(saldo.toFixed(2));
        }

    } else {
        total = parseFloat(total);
        $('#compra_saldo').val(total.toFixed(2));
        $('#compra_contado').val();
        $('#compra_credito').val('0');

    }
}


function validarNumeroVentas(event) {//Para validar numeros de cantidad de producto
    if (event.keyCode == 13) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }

}
;

function validarNumeroVentas2(event) {//Para validar numeros de margen en bs y %
    if (event.keyCode == 46) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }
}
;

//////////////////////////COMPRA DE PEDIDO 



function addfilapedido() { //agrega filas a la tabla de nota de venta y suma los precios de cada producto  

    var frm = $("#frm_usuP").serialize();

//$('#cont2').val(f);//recibe el  numero de fila de la descripcion del producto
    $.ajax({
        url: 'agregar_pedido_detalle.php',
        data: frm,
        type: 'post',
        success: function (registro) {
//            var datos = eval(registro);
            $("#totalProducto").val("");
            $("#precioV").val("");
            $("#precioC").val("");
            $("#cantidad").val("");
            $("#lote").val("");
            $("#fechaven").val("");
            $("#descripcion").val("");
            $("#margen").val("");
            $("#antSector").val("");
            $("#nombreSector").val("");
            $('#grilla tbody').append(registro); //dataTable > tbody:first
            var subt = $('#subt').val();
            subt = parseFloat(subt);
            $('#monts').val(subt);

//            subT = parseFloat(subT) + parseFloat(datos[0]);
            $('#subTotal').val(subt.toFixed(2));
            $('#total_compra').val(subt.toFixed(2));
            $('#compra_saldo').val(subt.toFixed(2));

            var tablita = document.getElementById("grilla");
            var d = tablita.rows.length - 1;
//            alert(d);
            $('#conta').val(d);
            $('#descripcion').focus();
            eliminar_detalle_prod();
            return false;
        }
    });
    return false;
}

function eliminar_detalle_prod() { //Elimina las filas de la tabla compra y resta el subtotal

    $("a.elimina").click(function () {
        var id = $(this).parents("tr").find("td").eq(0).html();
        id = parseInt(id);
        var dato = $('#montoT' + id).val();


        if (confirm("Desea eliminar el producto ")) {

            $(this).parents("tr").fadeOut("normal", function () {

                $(this).remove();

                subT = parseFloat($('#subTotal').val()) - dato;
                $('#subTotal').val(parseFloat($('#subTotal').val()) - dato);
                $('#total_compra').val($('#subTotal').val());
                $('#compra_saldo').val($('#subTotal').val());

            });
        }
    });
}
;


function calculoMargenCompraPed(id) {
    var margen = $('#margenT' + id).val();
    var precioC = $('#precioCT' + id).val();
    var precioV = $('#precioVT' + id).val();
    var cantidad = $('#cantidadT' + id).val();

    var subTotal = 0;
    if ($.isNumeric(margen) > 0 && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        precioV = parseFloat(precioV);
        precioC = parseFloat(precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
        margen = (((precioV - precioC) * 100) / precioC);

        $("#margenT" + id).val(margen);
        $("#montoT" + id).val(monto.toFixed(2));
        var cant = $('#conta').val();
        cant = parseFloat(cant);
        var monto = 0;
        for (var i = 1; i < cant; i++) {
            monto = $('#montoT' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        alert(subTotal);
        $("#subTotal").val(subTotal.toFixed(2));
        $("#total_compra").val(subTotal.toFixed(2));
        $("#compra_saldo").val(subTotal.toFixed(2));
    }

    return false;
}

function calculoPrecioVP(id) {
    var margen = $('#margenT' + id).val();
    var precioC = $('#precioCT' + id).val();
    var precioV = $('#precioVT' + id).val();
    var cantidad = $('#cantidadT' + id).val();
    var cant = $('#contas').val();
    var monto = 0;
    if ($.isNumeric(margen) && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        margen = parseFloat(margen);
        precioC = parseFloat(precioC);
        precioV = parseFloat(precioV);

        precioV = (((margen * precioC) / 100) + precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);

        $("#precioVT" + id).val(precioV.toFixed(2));
        $("#montoT" + id).val(monto.toFixed(2));
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i <= cant; i++) {
            monto = $('#montoT' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }

        $("#subTotal").val(subTotal.toFixed(2));
        $("#total_compra").val(subTotal.toFixed(2));
        $("#compra_saldo").val(subTotal.toFixed(2));
    }

    return false;

}
;

function registrarDetalle_pedido() {
    var frm = $("#frm_usuP").serialize();

    $.ajax({
        url: 'registrar_detalle_pedido.php',
        data: frm,
        type: 'post',
        success: function () {
            document.location.href = '../lista_factura_compra';


        }

    });


    return false;
}


function calculoCantidad(id) {
    var precioC = $('#precioCT' + id).val();
    var cantidad = $('#cantidadT' + id).val();
    var monto = 0;
    if ($.isNumeric(cantidad) > 0 && $.isNumeric(precioC)) {
        cantidad = parseFloat(cantidad);
        precioC = parseFloat(precioC);
        monto = cantidad * precioC;
        $("#montoT" + id).val(monto.toFixed(2));
        var cant = $('#contas').val();
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i <= cant; i++) {
            monto = $('#montoT' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        $("#subTotal").val(subTotal.toFixed(2));
        $("#total_compra").val(subTotal.toFixed(2));
        $("#compra_saldo").val(subTotal.toFixed(2));
    }
    return false;
}
;
function calculoPrecioVP(id) {
    var margen = $('#margenT' + id).val();
    var precioC = $('#precioCT' + id).val();
    var precioV = $('#precioVT' + id).val();
    var cantidad = $('#cantidadT' + id).val();
    var cant = $('#conta').val();

    if ($.isNumeric(margen) && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        margen = parseFloat(margen);
        precioC = parseFloat(precioC);
        precioV = parseFloat(precioV);

        precioV = (((margen * precioC) / 100) + precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
//        margen = (((precioV - precioC) * 100) / precioC);
//        $("#margenT" + id).val(margen);
        $("#precioVT" + id).val(precioV.toFixed(2));
        $("#montoT" + id).val(monto.toFixed(2));
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i < cant; i++) {
            monto = $('#montoT' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }

        $("#subTotal").val(subTotal.toFixed(2));
        $("#total_compra").val(subTotal.toFixed(2));
        $("#compra_saldo").val(subTotal.toFixed(2));
    }

    return false;

}
function calculoMargenCompraP(id) {
    var margen = $('#margenT' + id).val();
    var precioC = $('#precioCT' + id).val();
    var precioV = $('#precioVT' + id).val();
    var cantidad = $('#cantidadT' + id).val();
    var cant = $('#contas').val();
    if ($.isNumeric(precioV) && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        precioV = parseFloat(precioV);
        precioC = parseFloat(precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
        margen = (((precioV - precioC) * 100) / precioC);
        $("#margenT" + id).val(margen);
        $("#montoT" + id).val(monto.toFixed(2));
        var cant = $('#contas').val();
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i < cant; i++) {
            monto = $('#montoT' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        $("#subTotal").val(subTotal.toFixed(2));
        $("#total_compra").val(subTotal.toFixed(2));
        $("#compra_saldo").val(subTotal.toFixed(2));
    }

    return false;
}

function imprimirCompraP(){
var frm = $("#frm_usuP").serialize();

    $.ajax({
        url: 'registrarPedidoImp.php',
        data: frm,
        type: 'post',
        success: function (registro) {
             var datos = eval(registro);
             var nota = datos[0];
               document.location.href = 'VistaImpNuevaC.php?idNotaa='+ nota;
        }

    });

    return false;
}