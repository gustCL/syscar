
<?php
require_once 'head.php';
$idSucur = $_SESSION['userMaster'];
$idS = $idSucur['idSucursal'];
$nroFila = 1;
?>
<script src="../js/jquery.js"></script>
<script src="jfactura.js"></script>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">     
            <h1>
                <strong>
                  FACTURA DE COMPRAS
                </strong>
            </h1>                              
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>

    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">

                    <form method="post" class="form-horizontal">
                        <div class="widget-body ">

                            <br>
                            <br>
                            <center>
                                <div class="form-inline">

                                    <div class="input-group"> 
                                        <div class="col-lg-3">
                                            <span class="input-icon">
                                                <input id="buscar-factura" type="text" size="25" class="form-control"  placeholder="Busca: Nro de Factura " />
                                                <i class="glyphicon glyphicon-search circular blue"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <input  size="25" id="fechaI" name="fechaI" type="date" placeholder="Fecha" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>

                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="input-group">
                                        <a  type="button" id="btnBuscar"  class="btn btn-yellow"  >Buscar</a>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="input-group">
                                        <a type="button" href="nueva_compra_provee.php" class="btn btn-primary"  >Nueva Compra</a>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="input-group">
                                        <a type="button" href="pedido_compra.php" class="btn btn-primary"  >Compra Pedido</a>
                                    </div>
                                </div>
                            </center>
                            <br>
                        </div>
                        <br>
                        <div class="widget-body">
                              <center><h3><strong>LISTA DE FACTURAS DE COMPRAS</strong></h3></center>
                                  <br/>                                                            
                            <div class="table-responsive" id="agrega-registros">
                                <table class="table table-striped " >
                                    <thead>

                                        <tr>
                                            <th>
                                                Nro.
                                            </th>
                                            <th>
                                                NRO. FACTURA
                                            </th>
                                            <th>
                                                PROVEEDOR
                                            </th>
                                            <th>
                                                FECHA
                                            </th>
                                            <th>
                                                MONTO TOTAL
                                            </th>
                                            <th>

                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        require_once '../datos/Database.php';
                                        $registro = "select distinct f.nroFactura , f.idNotaIngreso , p.nombreProve ,f.fecha , f.hora , f.montoTotal "
                                                . "from FacturaIngreso f , Proveedor p "
                                                . "where f.idProveedor = p.idProveedor and p.estado = 1  and f.estado = 1"
                                                . " and f.idTipoIngreso = 1 ORDER BY f.idNotaIngreso DESC";
                                        try {
                                            $comando = Database::getInstance()->getDb()->prepare($registro);
                                            $comando->execute();
                                            while ($row = $comando->fetch()) {

                                                $NroF = $row['nroFactura'];
                                                ?><tr>

                                                    <td>
                                                        <?= $nroFila ?>
                                                    </td>
                                                    <td> 
                                                        <?= $NroF ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['nombreProve'] ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['fecha'] ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['montoTotal'] ?>
                                                    </td>
                                                    <td>                          
                                                        <a href="vista_factura.php?id=<?= $row['idNotaIngreso'] ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                                                        <a  href="modificar_factura.php?id=<?= $NroF ?>&notaIngreso=<?= $row['idNotaIngreso'] ?>"  class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                                                        <a href="javascript:eliminarFactura('<?= $row['idNotaIngreso'] ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>

                                                    </td>
                                                </tr>
                                                <?php
                                                $nroFila = $nroFila + 1;
                                            }//terminacion del while
                                        } catch (PDOException $e) {

                                            echo 'Error: ' . $e;
                                        }
                                        ?>    
                                    </tbody>
                                </table>   

                            </div>
                            <br>



                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once ('../header_footer/footer.php');
?> 