﻿$(document).ready(function () {
    cal_PrecioV();
});
﻿$(document).ready(function () {
    cal_Margen();
});
﻿$(document).ready(function () {
    cal_cantidad();
});


$(function () {
    $('#nrofactura').on('keyup', function () {
        var dato1 = $('#nrofactura').val();
        var dato2 = $('#nroControl').val();
        var dato3 = $('#nroAuto').val();

        if (dato1 != '') {
            if (dato2 != '') {
                if (dato3 != '') {
                    $('#precioC').removeAttr('readonly');
                    $('#precioV').removeAttr('readonly');
                    $('#descripcion').removeAttr('readonly');
                    $('#margen').removeAttr('readonly');
                    $('#descripcion').removeAttr('readonly');
                    $('#nombreSector').removeAttr('readonly');
                    $('#fechaven').removeAttr('readonly');
                    $('#lote').removeAttr('readonly');
                    $('#cantidad').removeAttr('readonly');
                }
            }
        } else {
            $('#precioC').attr('readonly', true);
            $('#precioV').attr('readonly', true);
            $('#descripcion').attr('readonly', true);
            $('#margen').attr('readonly', true);
            $('#descripcion').attr('readonly', true);
            $('#nombreSector').attr('readonly', true);
            $('#fechaven').attr('readonly', true);
            $('#lote').attr('readonly', true);
            $('#cantidad').attr('readonly', true);
        }

    });

    $('#nroControl').on('keyup', function () {
        var dato1 = $('#nroControl').val();
        var dato2 = $('#nrofactura').val();
        var dato3 = $('#nroAuto').val();

        if (dato1 != '') {

            if (dato2 != '') {
                if (dato3 != '') {
                    $('#precioC').removeAttr('readonly');
                    $('#precioV').removeAttr('readonly');
                    $('#descripcion').removeAttr('readonly');
                    $('#margen').removeAttr('readonly');
                    $('#descripcion').removeAttr('readonly');
                    $('#nombreSector').removeAttr('readonly');
                    $('#fechaven').removeAttr('readonly');
                    $('#lote').removeAttr('readonly');
                    $('#cantidad').removeAttr('readonly');
                }
            }

        } else {
            $('#precioC').attr('readonly', true);
            $('#precioV').attr('readonly', true);
            $('#descripcion').attr('readonly', true);
            $('#margen').attr('readonly', true);
            $('#descripcion').attr('readonly', true);
            $('#nombreSector').attr('readonly', true);
            $('#fechaven').attr('readonly', true);
            $('#lote').attr('readonly', true);
            $('#cantidad').attr('readonly', true);
        }

    });

    $('#nroAuto').on('keyup', function () {
        var dato1 = $('#nroAuto').val();
        var dato2 = $('#nroControl').val();
        var dato3 = $('#nrofactura').val();

        if (dato1 != '') {
            if (dato2 != '') {
                if (dato3 != '') {
                    $('#precioC').removeAttr('readonly');
                    $('#precioV').removeAttr('readonly');
                    $('#descripcion').removeAttr('readonly');
                    $('#margen').removeAttr('readonly');
                    $('#descripcion').removeAttr('readonly');
                    $('#nombreSector').removeAttr('readonly');
                    $('#fechaven').removeAttr('readonly');
                    $('#lote').removeAttr('readonly');
                    $('#cantidad').removeAttr('readonly');
                }
            }
        } else {
            $('#precioC').attr('readonly', true);
            $('#precioV').attr('readonly', true);
            $('#descripcion').attr('readonly', true);
            $('#margen').attr('readonly', true);
            $('#descripcion').attr('readonly', true);
            $('#nombreSector').attr('readonly', true);
            $('#fechaven').attr('readonly', true);
            $('#lote').attr('readonly', true);
            $('#cantidad').attr('readonly', true);
        }

    });
    $('#nroPedido').on('keyup', function () {

        if ($('#nroPedido').val() != '') {
            $('#nrofact').removeAttr('readonly');
            $('#almacen').removeAttr('readonly');
            $('#nroAuto').removeAttr('readonly');
            $('#nroControl').removeAttr('readonly');




        } else {
            $('#nrofact').attr('readonly', true);
            $('#almacen').attr('readonly', true);
            $('#nroAuto').attr('readonly', true);
            $('#nroControl').attr('readonly', true);

        }

    });

    $('#compra_contado').on('keyup', function () {
        var contado = $('#compra_contado').val();
        var total = $('#total_compra').val();
        contado = parseFloat(contado);
        total = parseFloat(total);
        if (contado > total) {
            alert("Dato erroneo ingresde de nuevo");
            $('#compra_contado').val("");
        }
        if (contado > 0 || total == contado) {
            $('#guardar').attr('disabled', false);

        } else {
            $('#guardar').attr('disabled', true);
        }

    });

});


function cal_PrecioV() {
    var margen = $('#margen').val();
    var precioC = $('#precioC').val();
    var precioV = $('#precioV').val();
    var cantidad = $('#cantidad').val();
    var monto = 0;
    if ($.isNumeric(precioC) > 0) {
        margen = parseFloat(margen);
        precioC = parseFloat(precioC);
        precioV = parseFloat(precioV);

        precioV = (((margen * precioC) / 100) + precioC);

        $("#precioV").val(precioV.toFixed(2));
        cantidad = parseFloat(cantidad);
        if (cantidad > 0) {
            monto = (precioV * cantidad);
            $("#totalProducto").val(monto.toFixed(2));
            subTotal = subTotal + monto;
        }

    }
    return false;
}

function cal_Margen() {
    var margen = $('#margen').val();
    var precioC = $('#precioC').val();
    var precioV = $('#precioV').val();
    var cantidad = $('#cantidad').val();
    var monto = 0;
    margen = parseFloat(margen);
    if (margen > 0) {
        precioV = parseFloat(precioV);
        precioC = parseFloat(precioC);
        margen = (((precioV - precioC) * 100) / precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
        $("#margen").val(margen.toFixed(2));
        $("#totalProducto").val(monto.toFixed(2));

    }

    return false;
}

function cal_cantidad() {
    var cantidad = $('#cantidad').val();
    var precioCosto = $('#precioC').val();
    var precioVenta = $('#precioV').val();
    var totalprod = 0;
    if ($.isNumeric(cantidad) > 0 && $.isNumeric(precioCosto) && $.isNumeric(precioVenta)) {
        cantidad = parseFloat(cantidad);
        precioCosto = parseFloat(precioCosto);
        totalprod = cantidad * precioCosto;
        $("#totalProducto").val(totalprod.toFixed(2));
    }
    return false;
}
var subT = 0;
function addfila() { //agrega filas a la tabla de nota de venta y suma los precios de cada producto  

    var frm = $("#frm_usu").serialize();

//$('#cont2').val(f);//recibe el  numero de fila de la descripcion del producto
    $.ajax({
        url: 'agregar_compra_detalle.php',
        data: frm,
        type: 'post',
        success: function (registro) {

            var datos = eval(registro);

            $("#totalProducto").val("");
            $("#precioV").val("");
            $("#precioC").val("");
            $("#cantidad").val("");
            $("#lote").val("");
            $("#fechaven").val("");
            $("#descripcion").val("");
            $("#margen").val("");
            $("#antSector").val("");
            $("#nombreSector").val("");
            $('#grilla tbody').append(datos[2]); //dataTable > tbody:first

            subT = parseFloat(subT) + parseFloat(datos[0]);
            $('#subTotal').val(subT.toFixed(2));
            $('#total_compra').val(subT.toFixed(2));
            $('#compra_saldo').val(subT.toFixed(2));

            $('#descripcion').focus();
            eliminar_detalle_prod(parseFloat(datos[0]));
            return false;
        }
    });
    return false;
}


function eliminar_detalle_prod(dato) { //Elimina las filas de la tabla de nota de venta y resta el subtotal

    $("a.elimina").click(function () {
        id = $(this).parents("tr").find("td").eq(0).html();

        if (confirm("Desea eliminar el producto ")) {

            $(this).parents("tr").fadeOut("normal", function () {

                $(this).remove();

                subT = parseFloat($('#subTotal').val()) - dato;
                $('#subTotal').val(parseFloat($('#subTotal').val()) - dato);
                $('#total_compra').val($('#subTotal').val());
                $('#compra_saldo').val($('#subTotal').val());
//                  var conta;
//                   $('#conta').val(f);//recibe el  numero de fila de la descripcion del producto
            });
        }
    });
}
;

function cal_Precio_Venta(id) {
    var margen = $('#margenT' + id).val();
    var precioC = $('#precioCT' + id).val();
    var precioV = $('#precioVT' + id).val();
    var cantidad = $('#cantidadT' + id).val();
    var monto = 0;
    if ($.isNumeric(margen) && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        margen = parseFloat(margen);
        precioC = parseFloat(precioC);
        precioV = parseFloat(precioV);

        precioV = (((margen * precioC) / 100) + precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioV * cantidad);
        $("#precioV" + id).val(precioV.toFixed(2));
        $("#totalProducto" + id).val(monto.toFixed(2));

    }

    return false;

}




function descuentos() {
    var subTotal = $('#subTotal').val();
    var descuento1 = $('#compra_descuento1').val();
    var descuento2 = $('#compra_descuento2').val();
    var descuento3 = $('#compra_descuento3').val();
    var montoInteres = $('#compra_montoInteres').val();
    var total = subTotal;
    var credito = $('#compra_credito').val();
    var contado = $('#compra_contado').val();
    var saldo = $("#compra_saldo").val();

    contado = parseFloat(contado);
    total = parseFloat(total);

    if ($.isNumeric(descuento1) > 0 && $.isNumeric(subTotal)) {
        descuento1 = parseFloat(descuento1);
        subTotal = parseFloat(subTotal);
        total = total - descuento1;
        $("#total_compra").val(total.toFixed(2));


    }
    if ($.isNumeric(descuento2) > 0 && $.isNumeric(subTotal)) {
        descuento2 = parseFloat(descuento2);
        subTotal = parseFloat(subTotal);
        total = total - descuento2;
        $("#total_compra").val(total.toFixed(2));
    }
    if ($.isNumeric(descuento3) > 0 && $.isNumeric(subTotal)) {
        descuento3 = parseFloat(descuento3);
        subTotal = parseFloat(subTotal);
        total = total - descuento3;
        $("#total_compra").val(total.toFixed(2));
    }
    if ($.isNumeric(montoInteres) > 0 && $.isNumeric(subTotal)) {
        montoInteres = parseFloat(montoInteres);
        subTotal = parseFloat(subTotal);
        total = total + montoInteres;
        $("#total_compra").val(total.toFixed(2));
    }
    if (total >= contado) {
        saldo = total - contado;
        credito = saldo;

        $("#compra_saldo").val(saldo.toFixed(2));
        $("#compra_credito").val(credito.toFixed(2));
        if (total == contado) {

            $('#compra_fechaPago').attr('readonly', true);
        }
    } else {
        saldo = contado - total;
        credito = 0;
        saldo = 0;
        $("#compra_saldo").val($('#total_compra').val());
        $("#compra_credito").val($('#total_compra').val());
        $('#compra_fechaPago').removeAttr('readonly');
    }
    if (descuento1 == 0 && descuento2 == 0 && descuento3 == 0) {
        $('#total_compra').val($('#subTotal').val());
    }
    return false;
}


function validarNumeroVentas(event) {//Para validar numeros de cantidad de producto
    if (event.keyCode == 13) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }

}


function validarNumeroVentas2(event) {//Para validar numeros de margen en bs y %
    if (event.keyCode == 46) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }
}

function registrarDetalle_compra() {

    var frm = $("#frm_usu123").serialize();

    $.ajax({
        url: 'registrar_detalle_compra.php',
        data: frm,
        type: 'post',
        success: function () {
//            alert(valor);
            document.location.href = '../lista_factura_compra';


        }

    });

    return false;
}

function calculoPrecioV(id) {
    var margen = $('#margenT' + id).val();
    var precioC = $('#precioCT' + id).val();
    var precioV = $('#precioVT' + id).val();
    var cantidad = $('#cantidadT' + id).val();
    var cant = $('#contas').val();
    var monto = 0;
    if ($.isNumeric(margen) && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        margen = parseFloat(margen);
        precioC = parseFloat(precioC);
        precioV = parseFloat(precioV);

        precioV = (((margen * precioC) / 100) + precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
//        margen = (((precioV - precioC) * 100) / precioC);
//        $("#margenT" + id).val(margen);
        $("#precioVT" + id).val(precioV.toFixed(2));
        $("#montoT" + id).val(monto.toFixed(2));
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i <= cant; i++) {
            monto = $('#montoT' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        $("#subTotal").val(subTotal.toFixed(2));

        $("#total_compra").val(subTotal.toFixed(2));
        $("#compra_saldo").val(subTotal.toFixed(2));
    }

    return false;

}

function calculoMargenCompra(id) {
    var margen = $('#margenT' + id).val();
    var precioC = $('#precioCT' + id).val();
    var precioV = $('#precioVT' + id).val();
    var cantidad = $('#cantidadT' + id).val();
    var cant = $('#contas').val();
    if ($.isNumeric(precioV) && $.isNumeric(precioC) && $.isNumeric(cantidad)) {
        precioV = parseFloat(precioV);
        precioC = parseFloat(precioC);
        cantidad = parseFloat(cantidad);
        monto = (precioC * cantidad);
        margen = (((precioV - precioC) * 100) / precioC);
        $("#margenT" + id).val(margen);
        $("#montoT" + id).val(monto.toFixed(2));
        var cant = $('#contas').val();
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i <= cant; i++) {
            monto = $('#montoT' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        $("#subTotal").val(subTotal.toFixed(2));
        $("#total_compra").val(subTotal.toFixed(2));
        $("#compra_saldo").val(subTotal.toFixed(2));
    }

    return false;
}
function calculoCantidad(id) {
    var precioC = $('#precioCT' + id).val();
    var cantidad = $('#cantidadT' + id).val();
    var monto = 0;
    if ($.isNumeric(cantidad) > 0 && $.isNumeric(precioC)) {
        cantidad = parseFloat(cantidad);
        precioC = parseFloat(precioC);
        monto = cantidad * precioC;
        $("#montoT" + id).val(monto.toFixed(2));
        var cant = $('#contas').val();
        cant = parseFloat(cant);
        var subTotal = 0;
        for (var i = 1; i <= cant; i++) {
            monto = $('#montoT' + i).val();
            monto = parseFloat(monto);
            subTotal = subTotal + monto;
        }
        $("#subTotal").val(subTotal.toFixed(2));
        $("#total_compra").val(subTotal.toFixed(2));
        $("#compra_saldo").val(subTotal.toFixed(2));
    }
    return false;
}

function imprimirCompra(){
var frm = $("#frm_usu123").serialize();

    $.ajax({
        url: 'registraCompImp.php',
        data: frm,
        type: 'post',
        success: function (registro) {
             var datos = eval(registro);
             var nota = datos[0];
               document.location.href = 'VistaImpNuevaC.php?idNotaa='+ nota;
        }

    });

    return false;
}