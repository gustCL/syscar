
<?php

require_once("../datos/Database.php");
$dato = $_POST['id'];
$date=date_create($dato);


$consulta = "select * from FacturaIngreso f , Proveedor p where p.idProveedor = f.idProveedor and f.estado = 1 and f.idTipoIngreso = 1  and f.fecha = '".date_format($date, 'd-m-Y')."' ORDER BY f.idNotaIngreso DESC  ";
$resul = Database::getInstance()->getDb()->prepare($consulta);
$resul->execute();
$nroFila = 1
?>
<table class="table table-striped">
    <thead>

        <tr>

            <th>
                ID.
            </th>
            <th>
                NRO. FACTURA
            </th>
            <th>
                PROVEEDOR
            </th>
            <th>
                FECHA
            </th>
            <th>
                MONTO TOTAL
            </th>
            <!--AQUI VAN LOS BOTONES-->
            <th>

            </th>


        </tr>
    </thead>
    <?php
    if ($resul->rowCount() > 0) {
        while ($row = $resul->fetch()) {
            $NroF = $row['nroFactura'];
            ?><tr>
                <td>
                    <?= $nroFila ?>
                </td>
                <td> 
                    <?= $NroF ?>
                </td>
                <td>
                    <?= $row['nombreProve'] ?>
                </td>
                <td>
                    <?= $row['fecha'] ?>
                </td>
                <td>
                    <?= $row['montoTotal'] ?>
                </td>

                <td>                          
                    <a href="vista_factura.php?id=<?= $row['idNotaIngreso'] ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                    <a  href="modificar_factura.php?id=<?= $NroF ?>&notaIngreso=<?= $row['idNotaIngreso'] ?>"  class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                    <a href="javascript:eliminarCliente('<?= $row['idNotaIngreso'] ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>

                </td>
            </tr>
            <?php
            $nroFila = $nroFila + 1;
        }
    } else {
        ?>
        <tr>
            <td colspan="6">No se encontraron resultados</td>
        </tr>
    <?php } ?>
</table>


