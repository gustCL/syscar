<?php
require_once 'head.php';
$idSucur = $_SESSION['userMaster'];
$idS = $idSucur['idSucursal'];
$id = $_GET['id'];

$consulta = "select * from FacturaIngreso f , Proveedor p , RegistroPagoCompra r "
        . "where f.idProveedor = p.idProveedor and p.estado = 1 and f.estado =1 and "
        . " r.idNotaIngreso = f.idNotaIngreso and f.idNotaIngreso = ?   ORDER BY f.idNotaIngreso DESC  ";
$resul = Database::getInstance()->getDb()->prepare($consulta);
$resul->execute([$id]);
$row = $resul->fetch();
$nrofila = 1;
$subt = 0;
$fecha = $row['fechaPago'];
$ymd = DateTime::createFromFormat('d-m-Y', $fecha)->format('Y-m-d');

?>
<script src="../js/jquery.js"></script>
<script src="jfactura.js"></script>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>FACTURA COMPRA</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">

                    <form  method="post" class="form-horizontal" id="frn_vista" >
                        <div class="widget-body ">
                            <br>
                            <br>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><strong>Prov:&nbsp;&nbsp;&nbsp; </strong> <?= $row['nombreProve'] ?></label>
                                <label class="col-lg-2 control-label"><strong>Nro. Factura</strong></label>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" name="nroFactura" id="nroFactura" value="<?= $row['nroFactura'] ?>"  readonly/>
                                    <input  name="nroNotaa" id="nroNotaa" value="<?= $id ?>"  hidden/>
                                </div>
                                <label class="col-lg-1 control-label"><strong>Nit.</strong></label>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" name="nit" id="nit" value="<?= $row['nit'] ?>" readonly/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>Nro. Control</strong></label>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" name="nroControl" id="nroControl" value="<?= $row['nroControl'] ?>"  readonly/>
                                </div>
                                <label class="col-lg-2 control-label"><strong>Nro. Autorizacion</strong></label>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" name="nroAuto" id="nroAuto" value="<?= $row['nroAutorizacion'] ?>" readonly/>
                                </div>
                                 <label class="col-lg-2 control-label"><strong>Nota Ingreso :&nbsp;&nbsp;&nbsp; </strong><?= $row['idNotaIngreso'] ?></label>
                                           
                            </div>
                        </div>
                        <br>
                        <div class="widget-body ">

                            <center>
                                <h2><strong>DETALLE</strong></h2>
                            </center>
                            <br>

                            <!-- TABLA DE NOTA DE VENTA-->
                            <div class="table-responsive">
                                <table id="grilla" class="table table-striped ">
                                    <thead>
                                        <tr>
                                            <th><strong>Nro</strong></th>
                                            <th><strong>Codigo</strong></th>
                                            <th><strong>Descripcion</strong></th>                                                    
                                            <th><strong>Precio Costo</strong></th>
                                            <th><strong>Precio Venta</strong></th>
                                            <th><strong>Margen</strong></th>
                                            <th><strong>Cantidad</strong></th>
                                            <th><strong>Nro Lote</strong></th>
                                            <th><strong>Fecha Vencimiento</strong></th>
                                            <th><strong>Monto</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--SE AGREGAN LAS FILAS DE LOS PRODUCTOS-->
                                        <?php
                                        $cons = " select p.codigo , p.nombreComercial , l.precioCosto , "
                                                . " l.precioVenta , l.margen , l.cantidad , l.nroLote ,l.fechaVencimiento , round((l.precioCosto * l.cantidad),2) AS Monto "
                                                . "from FacturaIngreso f , Lote l , Producto p "
                                                . "where  f.idNotaIngreso = l.idNotaIngreso and "
                                                . "l.codigoProducto = p.codigoProducto and p.estado = 1 and "
                                                . "l.estado = 1 and f.estado =1 and "
                                                . " l.idNotaIngreso = ? ";
                                        $dato = Database::getInstance()->getDb()->prepare($cons);
                                        $dato->execute([$id]);
                                        
                                        while ($row1 = $dato->fetch()) {
                                              $fecha = date("d-m-Y", strtotime($row1['fechaVencimiento']));
                                            ?>
                                            <tr>
                                                <td>
                                                    <?= $nrofila ?>
                                                </td>
                                                <td>
                                                    <?= $row1['codigo'] ?>
                                                </td>
                                                <td>
                                                    <?= $row1['nombreComercial'] ?>
                                                </td>
                                                <td>
                                                    <?= $row1['precioCosto'] ?>
                                                </td>
                                                <td>
                                                    <?= $row1['precioVenta'] ?>
                                                </td>
                                                <td>
                                                    <?= $row1['margen'] ?>
                                                </td>
                                                <td>
                                                    <?= $row1['cantidad'] ?>
                                                </td>
                                                <td>
                                                    <?= $row1['nroLote'] ?>
                                                </td>
                                                <td>
                                                    <?= $fecha  ?>
                                                </td>
                                                <td>
                                                    <?= $row1['Monto'] ?>
                                                </td>
                                            </tr> 

                                            <?php
                                            $nrofila = $nrofila + 1;
                                            $subt = $row1['Monto'] + $subt;
                                        }
                                        ?>
                                    </tbody>

                                    <tfoot>

                                        <tr>
                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td >Sub Total</td>
                                            <td class="text-center">                                                                                              
                                                <?= $subt ?>                                                   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td>Descuento %</td>
                                            <td class="text-center"><?= $row['descuento1'] ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td>Descuento %</td>
                                            <td class="text-center"><?= $row['descuento2'] ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td >Descuento %</td>
                                            <td class="text-center"><?= $row['descuento3'] ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td class="text-center"><strong>Total</strong></td>
                                            <td class="text-center "><strong><?= $row['montoTotal'] ?></td>
                                        </tr>
                                    </tfoot>
                                </table>                                
                            </div>
                              <div class="form-inline">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;
                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><strong>Contado</strong></label>
                                    <div class="col-lg-3">
                                        <strong><input size="10" style="text-align: right"  type="text" class="form-control" name="compra_contado" id="compra_contado" value="<?= $row['montoContado'] ?>" readonly/></strong>
                                    </div>
                                </div>
                               
                                <div class="form-group" >
                                    <label class="col-lg-5 control-label"><strong>Interes / Costo Adiciona</strong></label>
                                    <div class="col-lg-4">
                                        <input type="text" id="compra_montoInteres"  class="form-control" style="text-align: right"   name="compra_montoInteres" size="8"  value="<?= $row['montoInteres'] ?>" readonly/>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-lg-6 control-label"><strong>Fecha de Pago</strong></label>
                                    <div class="col-lg-6">
                                      <input type="date" id="fecha"  class="form-control" style="text-align: right"   name="fecha" size="8"  value="<?= $ymd?>" readonly/>
  
                                    </div>
                                </div>
                                <br>
                                <br>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;                        
                                <strong><input size="10" type="text"  name="compra_saldofavor" id="compra_saldofavor" value="0"  hidden/></strong>

                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><strong>Credito</strong></label>
                                    <div class="col-lg-3">
                                        <strong><input size="10" type="text" class="form-control" name="compra_credito" style="text-align: right" value="<?= $row['montoCredito'] ?>" id="compra_credito" readonly/></strong>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-6 control-label"><strong>Saldo</strong></label>
                                    <div class="col-lg-4">
                                        <strong><input size="10" type="text" class="form-control" name="compra_saldo" style="text-align: right"  id="compra_saldo" value="<?= $row['saldo'] ?>" readonly/></strong>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <div class="form-inline" >
                                <center>
                                    <div class="input-group">
                                        <div class="col-lg-4"> 
                                            <a type="button" href="index.php" class="btn btn-danger"  > Aceptar</a>
                                        </div>
                                           
                                    </div>
                                    <div class="form-group">
                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-success"  id="imprimir" name="imprimir" onclick="imprimirDetalle()" value="Imprimir">
                                            </div>
                                        </div>  
                                </center>
                            </div>
                            <br> 
                        </div>
                    </form>                      
                </div>
            </div>
        </div>
    </div>

</div>
<?php
require_once ('../header_footer/footer.php');
?> 