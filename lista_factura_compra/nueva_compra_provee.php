<?php
require_once 'head.php';
$suc = $_SESSION['userMaster'];
$idSucursal = $suc['idSucursal'];
?>
<script src="../js/jquery.js"></script>
<script src="jfactura_mod.js"></script>
<script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
<script src="../js/autocompletado/jquery-ui-1.10.3.custom.min.js"></script> 
<link rel="stylesheet" href="../js/autocompletado/jquery-ui-1.10.3.custom.min.css"> 
<div class="page-content">

    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>
                    SELECCIONE PROVEEDOR
                </strong>                
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">

        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="widget-body ">
                        <form  class="form-horizontal" action="javascript:fn_buscarP()" id="frm_buscarP" name="frm_buscarP"  >
                            <br>
                            <br>
                            <div class="form-inline">
                                <div class="form-group">
                                    <div class="col-lg-1"></div>  

                                    <div class="col-lg-4">  
                                        <br>
                                        <label class="control-label"><strong>Proveedor</strong></label>
                                        <input size="50" type="text" class="form-control" name="nombreProve" id="nombreProve"  placeholder="Nombre Proveedor"/>
                                        <script>

                                            $('#nombreProve').autocomplete({
                                                source: function (request, response) {
                                                    $.ajax({
                                                        url: 'autocompletado.php',
                                                        dataType: "json",
                                                        data: {
                                                            name_startsWith: request.term,
                                                            type: 'Nombre'
                                                        },
                                                        success: function (data) {
                                                            response($.map(data, function (item, id) {
                                                                return {
                                                                    label: id,
                                                                    value: id,
                                                                    id: item

                                                                };
                                                            }));
                                                        }
                                                    });
                                                },
                                                select: function (event, ui) {
                                                    var idpr = ui.item.id;

                                                    $('#nit').val(idpr);

                                                }
                                            });
                                        </script>                                                
                                    </div>


                                </div>
                                <div class="form-group">

                                    <div class="col-lg-2">
                                        <br>
                                        <label class="control-label"><strong>Nit</strong></label>
                                        <input type="text" class="form-control" name="nit" id="nit"  placeholder="Nro. nit"/>
                                        <script>
                                            $('#nit').autocomplete({
                                                source: function (request, response) {
                                                    $.ajax({
                                                        url: 'autocompletado.php',
                                                        dataType: "json",
                                                        data: {
                                                            name_startsWith: request.term,
                                                            type: 'Nit'
                                                        },
                                                        success: function (data) {
                                                            response($.map(data, function (item, id) {
                                                                return {
                                                                    label: id,
                                                                    value: id,
                                                                    id: item

                                                                };
                                                            }));
                                                        }
                                                    });
                                                },
                                                select: function (event, ui) {
                                                    var idpr = ui.item.id;

                                                    $('#nombreProve').val(idpr);
                                                }
                                            });

                                        </script>                                              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-3">
                                        <br>
                                        <label class="control-label"></label>
                                        <br>
                                        <input  name="botonProve" type="submit" id="botonProve" value="Buscar" class="btn btn-primary"/>
                                    </div>  
                                </div>

                            </div>
                            <br>
                            <Br>
                        </form>
                    </div>
                    <br>
                    <div class="widget-body ">

                        <div class="widget flat radius-bordered">
                            <div class="widget-header bordered-bottom bordered-blue">
                                <span class="widget-caption">Datos Proveedor</span>
                            </div>
                        </div>
                        <form id="frm_datosProve">
                            <div id="div_datosP">
                                <input type="text" name="ciPD" id="ciPD" value="" hidden/>
                            </div> 
                        </form>



                    </div>
                    <br>
                    <div class="widget-body " >
                        <form class="form-horizontal" method="post" action="nueva_compra.php" name="frm_datoSiguiente" id="frm_datoSiguiente">
                            <br>
                            <div class="form-group">
                                <div class="col-lg-1"></div>

                                <div class="col-lg-2">
                                    <?php
                                    /* Aqui se cargan los cargos */
                                    require_once("../datos/Database.php");
                                    $datos = Database::getInstance()->getDb()->prepare("select * from Almacen where idSucursal ='$idSucursal' ");
                                    $datos->execute();
                                    ?>
                                    <select class="form-control" name="idAlmacen" id="idAlmacen"   data-bv-field="country" >
                                        <?php while ($row = $datos->fetch()) { ?>               
                                            <option value="<?= $row['idAlmacen'] ?>"  ><?= $row['nombreAlmacen'] ?></option>
                                        <?php } ?>
                                    </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>
                                </div>

                                <input type="text" name="codigoP" id="codigoP" value=""  hidden/>
                                <input type="text" name="codigoAl" id="codigoAl" value=""  hidden/>

                                <div class="col-lg-4">
                                    <input  type="submit" id="btnSiguiente" value="Siguiente"  name="btnSiguiente"  class="btn btn-primary" />   
                                </div>
                            </div>
                            <br>
                        </form>
                        <script lang="javascript">
                            $("#frm_datoSiguiente").submit(function () {
                                var idP = $("#ciPD").val();
                                var almacen = $("#idAlmacen").val();
                                if (idP != "") {
                                    $("#codigoP").val(idP);
                                    $("#codigoAl").val(almacen);

                                } else {
                                    alert("Necesita seleccionar un Proveedor para continuar..");
                                    return false;
                                }

                            });
                        </script>

                    </div>

                </div>


            </div>
        </div>
    </div>
</div>




<?php
require_once ('../header_footer/footer.php');
?> 