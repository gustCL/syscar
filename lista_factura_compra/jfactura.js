$(function () {

    $('#buscar-factura').on('keyup', function () {

        var dato = $('#buscar-factura').val();
        var url = 'busca_factura.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'dato=' + dato,
            success: function (datos) {
                $('#agrega-registros').html(datos);
            }
        });
        return false;
    });

    $('#btnBuscar').on('click', function () {
        var dato = $('#fechaI').val();
       
        var url = 'busca_factura_fecha.php';

        $.ajax({
            type: 'POST',
            url: url,
            data: 'id='+ dato,
            success: function (valores) {
                
                $('#agrega-registros').html(valores);

            }
        });
        return false;
    });
});

function imprimirDetalle()
{   
   var nota= $('#nroNotaa').val();
            document.location.href = 'VistaImpCompra.php?idNotaa='+ nota;

};
function eliminarFactura(id){
	var url = 'eliminar_factura.php';
	var pregunta = confirm('¿Esta seguro de eliminar este factura ?');
	if(pregunta==true){
		$.ajax({
		type:'POST',
		url:url,
		data:'id='+id,
		success: function(registro){
			$('#agrega-registros').html(registro);
			return false;
		}
	});
	return false;
	}else{
		return false;
	}
}