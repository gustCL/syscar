<?php
require_once 'head.php';
$id = $_GET['id'];
$idNota = $_GET['notaIngreso'];

$consulta = "select * from FacturaIngreso f , Proveedor p , RegistroPagoCompra r  "
        . "where f.idProveedor = p.idProveedor and p.estado = 1 and f.estado =1 and r.idNotaIngreso = f.idNotaIngreso  "
        . " and f.nroFactura = ? and f.idNotaIngreso = ? ";
$resul = Database::getInstance()->getDb()->prepare($consulta);
$resul->execute([$id, $idNota]);
$row = $resul->fetch();

$nrofila = 1;
$cant = 0;
$subto = 0;
$total = 0;
$saldo = 0;
$saldoFavor = 0;
$montocredito = 0;
?>
<script src="../js/jquery.js"></script>
<script src="jfactura_mod.js"></script>
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>FACTURA COMPRA</strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <form  method="post" class="form-horizontal" action="actualizar_detalle_factura.php"  >
                                <div class="widget-body ">
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label"><strong>Prov:&nbsp;&nbsp;&nbsp; </strong> <?= $row['nombreProve'] ?></label>
                                        <label class="col-lg-2 control-label"><strong>Nro. Factura</strong></label>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control" name="nroFactura" id="nroFactura" value="<?= $row['nroFactura'] ?>"  />
                                        </div>
                                        <label class="col-lg-1 control-label"><strong>Nit.</strong></label>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control" name="nit" id="nit" value="<?= $row['nit'] ?>" readonly/>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label"><strong>Nro. Control</strong></label>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control" name="nroControl" id="nroControl" value="<?= $row['nroControl'] ?>"  />
                                        </div>
                                        <label class="col-lg-2 control-label"><strong>Nro. Autorizacion</strong></label>
                                        <div class="col-lg-2">
                                            <input type="text" class="form-control" name="nroAutorizacion" id="nroAutorizacion" value="<?= $row['nroAutorizacion'] ?>" />

                                        </div>
                                        <label class="col-lg-1 control-label"><strong>Nota Ingreso </strong></label>
                                        <div class="col-lg-2">                                           
                                            <input type="text" class="form-control" name="idNota" id="idNota" value="<?= $row['idNotaIngreso'] ?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="widget-body ">

                                    <center>
                                        <h2><strong>DETALLE</strong></h2>
                                    </center>
                                    <br>

                                    <!-- TABLA DE NOTA DE VENTA-->
                                    <div class="table-responsive" id="registros-tabla">
                                        <table id="grilla" class="table table-striped ">
                                            <thead>
                                                <tr>
                                                    <th><strong>Nro</strong></th>
                                                    <th><strong>Codigo</strong></th>
                                                    <th><strong>Descripcion</strong></th>                                                    
                                                    <th><strong>P. Costo</strong></th>
                                                    <th><strong>P. Venta</strong></th>
                                                    <th><strong>Margen</strong></th>
                                                    <th><strong>Cant.</strong></th>
                                                    <th><strong>Nro Lote</strong></th>
                                                    <th><strong>Fecha V.</strong></th>
                                                    <th><strong>Monto</strong></th>
                                                    <th><strong></strong></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!--SE AGREGAN LAS FILAS DE LOS PRODUCTOS-->
                                                <?php
                                                $cons = " select p.codigoProducto , p.codigo , p.nombreComercial , l.precioCosto , l.idLote ,"
                                                        . " l.precioVenta , l.margen , l.cantidad , l.nroLote ,l.fechaVencimiento , round((l.precioCosto * l.cantidad),2) AS Monto "
                                                        . "from FacturaIngreso f , Lote l , Producto p   "
                                                        . "where  f.idNotaIngreso = l.idNotaIngreso and "
                                                        . "l.codigoProducto = p.codigoProducto and p.estado = 1 and "
                                                        . "l.estado = 1 and f.estado =1 and "
                                                        . " l.idNotaIngreso = ?   ORDER BY f.idNotaIngreso DESC ";
                                                $dato = Database::getInstance()->getDb()->prepare($cons);
                                                $dato->execute([$idNota]);

                                                while ($row1 = $dato->fetch()) {
                                                    $idP = $row1['codigoProducto'];
                                                    $fecha = $row1['fechaVencimiento'];
                                                    $ymd = DateTime::createFromFormat('d-m-Y', $fecha)->format('Y-m-d');
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?= $nrofila ?>
                                                        </td>
                                                        <td>
                                                            <?= $row1['codigo'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $row1['nombreComercial'] ?>
                                                        </td>
                                                        <td>
                                                            <input size="15" type="text" class="form-control" id="precioC<?= $nrofila ?>" name="precioC<?= $nrofila ?>"  value="<?= $row1['precioCosto'] ?>" onkeyup="calculoPrecioVenta(<?= $nrofila ?>)" />  
                                                        </td>
                                                        <td>
                                                            <input size="15" type="text" class="form-control" id="precioV<?= $nrofila ?>" name="precioV<?= $nrofila ?>" value="<?= $row1['precioVenta'] ?>" onkeyup="calculoMargen(<?= $nrofila ?>)"  /> 
                                                        </td >
                                                        <td>
                                                            <input size="15" type="text" class="form-control" id="margen<?= $nrofila ?>" name="margen<?= $nrofila ?>" value="<?= $row1['margen'] ?>"  onkeyup="calculoPrecioVenta(<?= $nrofila ?>)" />
                                                        </td>
                                                        <td>
                                                            <input size="15" type="text" class="form-control" id="cantidad<?= $nrofila ?>" name="cantidad<?= $nrofila ?>" value="<?= $row1['cantidad'] ?>" onkeyup="calculoPrecioVenta(<?= $nrofila ?>)"  />
                                                        </td>
                                                        <td>
                                                            <input size="20" type="text" class="form-control" id="nrolote<?= $nrofila ?>" name="nrolote<?= $nrofila ?>" value="<?= $row1['nroLote'] ?>" />
                                                        </td>
                                                        <td>
                                                            <input size="20"  id="fecha<?= $nrofila ?>" name="fecha<?= $nrofila ?>" type="date" placeholder="Fecha" value="<?= $ymd ?>"  />
                                                        </td>
                                                        <td>
                                                            <input type="text" size="30" class="form-control" id="monto<?= $nrofila ?>" name="monto<?= $nrofila ?>" value=" <?= $row1['Monto'] ?>"  >
                                                        </td>
                                                        <td>
                                                            <a href="javascript:eliminarProductoDetalle('<?= $idP ?>','<?= $id ?>','<?= $row1['idLote'] ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                                                        </td>
                                                <input type="text" id="idLote<?= $nrofila ?>" name="idLote<?= $nrofila ?>" value="<?= $row1['idLote'] ?>" size="8" hidden/>
                                                <input type="text" id="codigo<?= $nrofila ?>" name="codigo<?= $nrofila ?>" value="<?= $idP ?>" size="8" hidden/>
                                                </tr> 

                                                <?php
                                                $cant = $nrofila;
                                                $nrofila = $nrofila + 1;

                                                $subto = $subto + $row1['Monto'];
                                            }
                                            //   $subto = $subto + $row1['Monto'];
                                            ?>
                                            </tbody>

                                            <tfoot>
                                            <input type="text" id="cant" name="cant" value="<?= $cant ?>" size="8" hidden/>
                                            <tr>

                                                <td colspan="8" class="invisible bg-snow"></td>
                                                <td ><strong>Sub Total</strong></td>
                                                <td class="text-center"> <input type="text" id="subTotal"  class="form-control"  name="subTotal" value="<?= $subto ?>" size="8" onkeyup="montSubtotals()" readonly/> </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" class="invisible bg-snow"></td>
                                                <td >Descuento BS </td>
                                                <td class="text-center"><input type="text" id="descuento1"  class="form-control"  value="<?= $row['descuento1'] ?>" name="descuento1" size="8" onkeyup="descuentos()" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" class="invisible bg-snow"></td>
                                                <td >Descuento BS</td>
                                                <td class="text-center"><input type="text" id="descuento2"  class="form-control"  value="<?= $row['descuento2'] ?>"  name="descuento2" size="8" onkeyup="descuentos()" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" class="invisible bg-snow"></td>
                                                <td >Descuento BS</td>
                                                <td class="text-center"><input type="text" id="descuento3"  class="form-control"  value="<?= $row['descuento3'] ?>"  name="descuento3" size="8" onkeyup=" descuentos()" /></td>
                                            </tr>

                                            <tr>
                                                <?php
                                                $total = $subto - $row['descuento1'] - $row['descuento2'] - $row['descuento3'] + $row['montoInteres'];
                                                ?>
                                                <td colspan="8" class="invisible bg-snow"></td>
                                                <td class="text-center"><strong>Total</strong></td>
                                                <td class="text-center "><strong><input type="text"  class="form-control"  id="total" name="total" value="<?= $total ?>" size="8" /></strong></td>
                                            </tr>
                                            </tfoot>
                                        </table>  

                                    </div>
                                    <input type="text"  name="cant" id="cant" value="<?= $cant ?>" hidden />
                                </div>
                                <br>
                                <div class="widget-body "> 
                                    <br>
                                    <br>

                                    <div class="form-inline">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;
                                        <?php
                                        if ($total >= $row['montoContado']) {
                                            $saldo = $total - $row['montoContado'];
                                            $montocredito = $saldo;
                                        } else {
                                            $saldo = $row['montoContado'] - $total;

                                            $montocredito = 0;
                                            $saldo = 0;
                                        }
                                        ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><strong>Contado</strong></label>
                                            <div class="col-lg-3">
                                                <strong><input size="10" type="text" class="form-control" name="contado" id="contado" value="<?= $row['montoContado'] ?>" onkeyup="descuentos()"/></strong>
                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                         <div class="form-group">
                                            <label class="col-lg-4 control-label"><strong>Saldo</strong></label>
                                            <div class="col-lg-3">
                                                <strong><input size="10" type="text" class="form-control" name="saldo" id="saldo" value="<?= $saldo ?>" /></strong>
                                            </div>
                                        </div>
                                       
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label"><strong>Fecha de Pago</strong></label>
                                            <div class="col-lg-4">
                                                <input  id="fechaPago" name="fechaPago" type="date" placeholder="Fecha" value="<?= DateTime::createFromFormat('d-m-Y', $row['fechaPago'])->format('Y-m-d'); ?>" />
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;   
                                        &nbsp;&nbsp;&nbsp;&nbsp;   
                                        &nbsp;&nbsp;&nbsp;&nbsp;   
                                        &nbsp;&nbsp;&nbsp;&nbsp;   
                                        &nbsp;&nbsp;&nbsp;&nbsp;   
                                        
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><strong>Credito</strong></label>
                                            <div class="col-lg-3">
                                                <strong><input size="10" type="text" class="form-control" name="montoCredito" id="montoCredito" value="<?= $montocredito ?>" /></strong>
                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label"><strong>Interes / Costo Adicional</strong></label>
                                            <div class="col-lg-4">
                                                <strong><input type="text" id="montoInteres"  class="form-control"  value="<?= $row['montoInteres'] ?>"  name="montoInteres" size="8" onkeyup="descuentos()" /></strong>
                                            </div>
                                        </div>



                                    </div>
                                    <br>
                                    <div class="form-inline" >
                                        <center>
                                            <div class="form-group">
                                                <div class="col-lg-offset-4 col-lg-4">                                        
                                                    <input class="btn btn-primary" type="submit" value="Guardar" />
                                                </div>
                                            </div>

                                            <div class="input-group">
                                                <div class="col-lg-offset-4 col-lg-4"> 
                                                    <a type="button" href="index.php" class="btn btn-danger"  > Cancelar</a>
                                                </div>
                                            </div>
                                        </center>
                                    </div>
                                    <br>  
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
require_once ('../header_footer/footer.php');
?> 