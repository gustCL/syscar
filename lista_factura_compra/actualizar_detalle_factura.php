<?php
require_once("../datos/Database.php");
session_start();
$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];
$idSesion = $_SESSION['idSesion'];

$idNotaIngreso = $_POST['idNota'];
$nroFactura = $_POST['nroFactura'];
$nroControl = $_POST['nroControl'];
$nroAutorizacion = $_POST['nroAutorizacion'];
$nit = $_POST['nit'];
$nombreProveedor = $_POST['nombreProvedor'];
$cant = $_POST['cant'];
$SubTotal = $_POST['subTotal'];
$descuento1 = $_POST['descuento1'];
$descuento2 = $_POST['descuento2'];
$descuento3 = $_POST['descuento3'];
$montoInteres = $_POST['montoInteres'];
$total = $_POST['total'];
$contado = $_POST['contado'];
$montoCredito = $_POST['montoCredito'];
$saldo = $_POST['saldo'];
$saldoFavor = $_POST['saldofavor'];
$fechaPago = $_POST['fechaPago'];
ini_set("date.timezone", "America/La_Paz");
$fechA = date("d-m-Y");
$horA = date("H:i:s");       

  $CONSULTA ="SELECT idProveedor FROM Proveedor WHERE nombreProve='$nombreProveedor' ";
  $DATO= Database::getInstance()->getDb()->prepare($CONSULTA);
  $DATO->execute();
  $ROW =$DATO->fetch();
  $idProveedor=$ROW['idProveedor'];
  ///////// ACTUALIZAR LA TABLA FACTURA INGRESO ////////////////////////
  $CONSULTA1 ="UPDATE FacturaIngreso  SET idInicioSesion ='$idSesion', nroFactura ='$nroFactura' , fecha='$fechA' , hora='$horA' , montoSubTotal='$SubTotal' , descuento1='$descuento1' , descuento2 = '$descuento2' , descuento3 ='$descuento3' , montoInteres ='$montoInteres' , montoTotal='$total' , nroControl='$nroControl' ,nroAutorizacion='$nroAutorizacion'  WHERE idNotaIngreso='$idNotaIngreso' ";
  $DATO1= Database::getInstance()->getDb()->prepare($CONSULTA1);
  $DATO1->execute();
  
  ////////// ACTUALIZAR LA TABLA REGISTROPAGOCOMPRA///////////////////////
  $CONSULTA5 ="UPDATE  RegistroPagoCompra SET montoContado='$contado' , montoCredito='$montoCredito' , saldo='$saldo' , saldoFavor='$saldoFavor' , fechaPago='$fechaPago' WHERE idNotaIngreso='$idNotaIngreso' ";
  $DATO5 = Database::getInstance()->getDb()->prepare($CONSULTA5);
  $DATO5->execute();

for ($index = 1; $index <= $cant; $index++) {
  $PrecioC = $_POST['precioC'.$index];
  $PrecioV = $_POST['precioV'.$index];
  $margen = $_POST['margen'.$index];
  $cantidad = $_POST['cantidad'.$index];
  $nroLote = $_POST['nrolote'.$index];
  $fechaT = $_POST['fecha'.$index];
  $monto = $_POST['monto'.$index];
  $idLote = $_POST['idLote'.$index];
  $codigoP = $_POST['codigo'.$index];

  
  //////////ACTUALIZAR LA TABLA LOTE /////////////////////////////////
  $CONSULTA2 ="UPDATE Lote SET precioCosto='$PrecioC' , precioVenta='$PrecioV', margen='$margen', fechaVencimiento='$fechaT' , cantidad='$cantidad' WHERE idNotaIngreso='$idNotaIngreso' and idLote='$idLote' ";
  $DATO2= Database::getInstance()->getDb()->prepare($CONSULTA2);
  $DATO2->execute();
  
  //////// ACTUALIZAR LA TABLA EXISTENCIAS //////////////////////////
  $CONSULTA3 ="UPDATE Existencias  SET stock='$cantidad'  WHERE idLote='$idLote' ";
  $DATO3= Database::getInstance()->getDb()->prepare($CONSULTA3);
  $DATO3->execute();
  
    //////// ACTUALIZAR LA TABLA PRECIOSUCURSAL //////////////////////////
  $CONSULTA4 ="UPDATE  PrecioSucursal SET precioCosto='$PrecioC' , precioVenta='$PrecioV' , fechaActual='$fechA' , horaActual='$horA' WHERE codigoProducto='$codigoP' and idSucursal='$idSucursal' ";
  $DATO4= Database::getInstance()->getDb()->prepare($CONSULTA4);
  $DATO4->execute();
  

}
 header('Location: ../lista_factura_compra');
?>