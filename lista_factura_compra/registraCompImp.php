<?php

require_once("../datos/Database.php");
session_start();

$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];
$idSesion = $_SESSION['idSesion'];
$idAlmacen = $_POST['idAlm'];
$idProveedor = $_POST['codigoProve'];
$SubTot = $_POST['subTotal']; 

$descuento1 = $_POST['compra_descuento1'];
$descuento2 = $_POST['compra_descuento2'];
$descuento3 = $_POST['compra_descuento3'];
$montoInteres = $_POST['compra_montoInteres'];
$total = $_POST['total_compra'];
$contado = $_POST['compra_contado'];
$montoCredito = $_POST['compra_credito'];
$saldo = $_POST['compra_saldo'];
$saldoFavor = $_POST['compra_saldofavor'];
$fec = $_POST['compra_fechaPago'];
ini_set("date.timezone", "America/La_Paz");
$fechA = date("d-m-Y");
$horA = date("H:i:s");
$nroFactura = $_POST['factura'];
$codAuto = $_POST['nAuto'];
$codControl = $_POST['nControl'];
  $fechaPago = date("d-m-Y", strtotime($fec));
//
//////////// BUSQUEDA DEL idTipoIngreso ///////////
$CONSULTA11 = "SELECT idTipoIngreso FROM TipoIngreso WHERE nombreTipoIng='Factura de Compras' ";
$DATO11 = Database::getInstance()->getDb()->prepare($CONSULTA11);
$DATO11->execute();
$ROW11 = $DATO11->fetch();
$idTipoing = $ROW11['idTipoIngreso'];
//////////// BUSQUEDA DEL TIPOCAMBIO ///////////
$CONSULTA22 = "SELECT * FROM TipoCambio t WHERE t.estado=1 ";
$DATO22 = Database::getInstance()->getDb()->prepare($CONSULTA22);
$DATO22->execute();
$ROW22 = $DATO22->fetch();
$tipocambiocompra = $ROW22['tipoCambioCompra'];
$tipocambioventa = $ROW22['tipoCambioVenta'];
$idTipoCambio = $ROW22['idTipoCambio'];
///////// REGISTRAR TABLA FACTURA INGRESO ////////////////////////
$CONSULTA1 = " INSERT INTO FacturaIngreso (idTipoIngreso, idNotaIngreso, idProveedor,idTipoCambio, tipoCambioVenta, tipoCambioCompra, idInicioSesion, idSucursal,nroFactura, nroAutorizacion, nroControl, fecha, hora, glosa, montoSubTotal, descuento1, descuento2, descuento3, montoInteres, montoTotal, estado) "
        . "VALUES(?,'0',?,?,?,?,?,?,?,?,?,?,?,'SIN NOVEDAD',?,?,?,?,?,?,'1')";
$DATO1 = Database::getInstance()->getDb()->prepare($CONSULTA1);
$DATO1->execute([$idTipoing, $idProveedor,$idTipoCambio, $tipocambioventa, $tipocambiocompra, $idSesion, $idSucursal, $nroFactura ,$codAuto,$codControl, $fechA, $horA, $SubTot, $descuento1, $descuento2, $descuento3, $montoInteres, $total]);
//////////// OBTENER EL IDNOTAINGRESO ///////////////////////////////

$CONSULTA55 = "SELECT Max(idNotaIngreso) as idNota FROM FacturaIngreso  ";
$DATO55 = Database::getInstance()->getDb()->prepare($CONSULTA55);
$DATO55->execute();
$count = $DATO55->fetch(PDO::FETCH_ASSOC);
$idNotaIngreso = $count['idNota'];

////////// ACTUALIZAR LA TABLA REGISTROPAGOCOMPRA///////////////////////
if ($saldo == '0.00') {
    $CONSULTA5 = " INSERT INTO RegistroPagoCompra (idPagoCompra,idNotaIngreso,montoContado,montoCredito,saldo,saldoFavor,fechaPago,pagado,contado,estado) "
            . "VALUES ('0',?,?,?,?,?,?,'2','1','1'  ) ";
    $DATO5 = Database::getInstance()->getDb()->prepare($CONSULTA5);
    $DATO5->execute([$idNotaIngreso, $contado, $montoCredito, $saldo, $saldoFavor, $fechaPago]);
} else {
    $CONSULT51 = " INSERT INTO RegistroPagoCompra (idPagoCompra,idNotaIngreso,montoContado,montoCredito,saldo,saldoFavor,fechaPago,pagado,contado,estado) "
            . "VALUES ( '0',?,?,?,?,?,?,'1','0','1' ) ";
    $DATO51 = Database::getInstance()->getDb()->prepare($CONSULT51);
    $DATO51->execute([$idNotaIngreso, $contado, $montoCredito, $saldo, $saldoFavor, $fechaPago]);
}

///////////////////////////// DATOS DE LA TABLA ////////////////////////////////////
$cod = $_POST['codig'];
$descripcion = $_POST['descripcionT'];
$precioCosto = $_POST['precioCT'];
$precioVenta = $_POST['precioVT'];
$margen = $_POST['margenT'];
$cantidad = $_POST['cantidadT'];
$nrolote = $_POST['nrolote'];
$nombSector = $_POST['sectorT'];
$fechaT = $_POST['fechaT'];
$monTotalP = $_POST['montoT'];
$antSect = $_POST['anSector'];


for ($index = 0; $index < count($cod); $index++) {
    $codigoP = $cod[$index];
    $descrip = $descripcion[$index];
    $PrecioCost = $precioCosto[$index];
    $PrecioVent = $precioVenta[$index];
    $margenn = $margen[$index];
    $cantid = $cantidad[$index];
    $nlote = $nrolote[$index];
    $fecha = $fechaT[$index];
    $montoTot = $monTotalP[$index];
    $sectorTot = $nombSector[$index];
    $sectorAnt = $antSect[$index];
   $fech = date("d-m-Y", strtotime($fecha));

    //////////INSERTA LA TABLA LOTE /////////////////////////////////
    $CONSULTA2 = "INSERT INTO Lote( idLote,nroLote,codigoProducto,idNotaIngreso,precioCosto,precioVenta,margen,fechaVencimiento,cantidad,observaciones,estado)"
            . " VALUES ('0',?,?,?,?,?,?,?,?,'nada','1')";
    $DATO2 = Database::getInstance()->getDb()->prepare($CONSULTA2);
    $DATO2->execute([$nlote, $codigoP, $idNotaIngreso, $PrecioCost, $PrecioVent, $margenn, $fech, $cantid]);
    //////////// OBTENER EL idLote ///////////////////////////////
    $CONSULTA6 = "SELECT Max(idLote) as idLote FROM Lote  ";
    $DATO6 = Database::getInstance()->getDb()->prepare($CONSULTA6);
    $DATO6->execute();
    $count = $DATO6->fetch(PDO::FETCH_ASSOC);
    $idlote1 = $count['idLote'];

    //////// INSERTANDO Y ACTUALIZANDO LAS UBICACIONES  //////////////////////////
    $CONSULTA15 = "SELECT idSector FROM Sectores where nombreSector ='$sectorTot'  ";
    $DATO15 = Database::getInstance()->getDb()->prepare($CONSULTA15);
    $DATO15->execute();
    $row15 = $DATO15->fetch();
    $idS = $row15['idSector'];

    if ($sectorAnt != $sectorTot) {
        $CONSULTA15 = "SELECT idSector FROM Sectores where nombreSector ='$sectorTot'  ";
        $DATO15 = Database::getInstance()->getDb()->prepare($CONSULTA15);
        $DATO15->execute();
        $row15 = $DATO15->fetch();
        $idS = $row15['idSector'];

        $CONSULTA8 = "INSERT INTO Ubicacion(idUbicacion,idAlmacen,codigoProducto,idSector)VALUES "
                . "('0','$idAlmacen','$codigoP','$idS') ";
        $DATO8 = Database::getInstance()->getDb()->prepare($CONSULTA8);
        $DATO8->execute();

        $CONSULTA10 = "SELECT Max(idUbicacion) as idUbicacion FROM Ubicacion  ";
        $DATO10 = Database::getInstance()->getDb()->prepare($CONSULTA10);
        $DATO10->execute();
        $count = $DATO10->fetch(PDO::FETCH_ASSOC);
        $idUbica = $count['idUbicacion'];

        $CONSULTA9 = "INSERT INTO Existencias(idExistencia,idLote,idUbicacion,stock,terminado,estado)VALUES "
                . "('0','$idlote1','$idUbica','$cantid',1,'1') ";
        $DATO9 = Database::getInstance()->getDb()->prepare($CONSULTA9);
        $DATO9->execute();
    } else {
        $CONSULTA20 = "SELECT idUbicacion FROM Ubicacion where idAlmacen='$idAlmacen' and idSector ='$idS' and codigoProducto='$codigoP' ";
        $DATO10 = Database::getInstance()->getDb()->prepare($CONSULTA20);
        $DATO10->execute();
        $row10 = $DATO10 ->fetch();
        $idUbicacion = $row10['idUbicacion'];

        $CONSULTA9 = "INSERT INTO Existencias(idExistencia,idLote,idUbicacion,stock,terminado,estado)VALUES "
                . "('0','$idlote1','$idUbicacion','$cantid','1','1') ";
        $DATO9 = Database::getInstance()->getDb()->prepare($CONSULTA9);
        $DATO9->execute();
    }


    //////// ACTUALIZAR LA TABLA PRECIOSUCURSAL //////////////////////////
    $CONSULTA4 = "UPDATE  PrecioSucursal SET precioCosto='$PrecioCost' , precioVenta='$PrecioVent' , fechaActual='$fech' , horaActual='$horA' WHERE codigoProducto='$codigoP' and idSucursal='$idSucursal' ";
    $DATO4 = Database::getInstance()->getDb()->prepare($CONSULTA4);
    $DATO4->execute();
}
$datos = array(
				0 => $idNotaIngreso, 
				);

echo json_encode($datos);
?>