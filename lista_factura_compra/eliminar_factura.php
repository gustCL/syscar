<?php
require_once("../datos/Database.php");
$codigo = $_POST['id'];

/////////////////// elimina una factura
$cons1 = "UPDATE FacturaIngreso SET estado = 0 WHERE idNotaIngreso = '$codigo' ";
$res1 = Database::getInstance()->getDb()->prepare($cons1);
$res1->execute();
  $nroFila=1;
?>
<table class="table table-striped " >
    <thead>

        <tr>
            <th>
                Nro.
            </th>
            <th>
                NRO. FACTURA
            </th>
            <th>
                PROVEEDOR
            </th>
            <th>
                FECHA
            </th>
            <th>
                MONTO TOTAL
            </th>
            <th>

            </th>

        </tr>
    </thead>
    <tbody>
        <?php
        require_once '../datos/Database.php';
        $registro = "select distinct f.nroFactura , f.idNotaIngreso , p.nombreProve ,f.fecha , f.hora , f.montoTotal "
                . "from FacturaIngreso f , Proveedor p "
                . "where f.idProveedor = p.idProveedor and p.estado = 1  and f.estado = 1"
                . "  ORDER BY f.idNotaIngreso DESC";
        try {
            $comando = Database::getInstance()->getDb()->prepare($registro);
            $comando->execute();
            while ($row = $comando->fetch()) {

                $NroF = $row['nroFactura'];
                ?><tr>

                    <td>
                        <?= $nroFila ?>
                    </td>
                    <td> 
                        <?= $NroF ?>
                    </td>
                    <td>
                        <?= $row['nombreProve'] ?>
                    </td>
                    <td>
                        <?= $row['fecha'] ?>
                    </td>
                    <td>
                        <?= $row['montoTotal'] ?>
                    </td>
                    <td>                          
                        <a href="vista_factura.php?id=<?= $row['idNotaIngreso'] ?>"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                        <a  href="modificar_factura.php?id=<?= $NroF ?>&notaIngreso=<?= $row['idNotaIngreso'] ?>"  class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                        <a href="javascript:eliminarFactura('<?= $row['idNotaIngreso'] ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>

                    </td>
                </tr>
                <?php
                $nroFila = $nroFila + 1;
            }//terminacion del while
        } catch (PDOException $e) {

            echo 'Error: ' . $e;
        }
        ?>    
    </tbody>
</table>   