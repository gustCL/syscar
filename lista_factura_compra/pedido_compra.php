<?php
require_once 'head.php';

$suc = $_SESSION['userMaster'];
$idSucursal = $suc['idSucursal'];
?>

<script src="../js/jquery.js"></script>
<script src="jfactura_compra_pedido.js"></script>
<script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
<script src="../js/autocompletado/jquery-ui-1.10.3.custom.min.js"></script> 
<link rel="stylesheet" href="../js/autocompletado/jquery-ui-1.10.3.custom.min.css"> 
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>
                    COMPRA POR PEDIDO
                </strong>
            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">

                    <div class="widget-body "> 
                        <form method="post"  action="javascript:addfilapedido();" id="frm_usuP">
                            <br>
                            <div class="form-group">

                                <label class="col-lg-2 control-label"><strong>  Nro. Pedido:</strong></label>
                                <div class="col-lg-2">
                                    <span class="input-icon">
                                        <strong><input size="5" type="text" class="form-control" style="text-align: right"  name="nroPedido" id="nroPedido" /></strong>
                                        <script>
                                            var cont = 0;
                                            $('#nroPedido').autocomplete({
                                                source: function (request, response) {
                                                    $.ajax({
                                                        url: 'autocompletado.php',
                                                        dataType: "json",
                                                        data: {
                                                            name_startsWith: request.term,
                                                            type: 'DetallePedido'
                                                        },
                                                        success: function (data) {
                                                            response($.map(data, function (item, id) {
                                                                return {
                                                                    label: id,
                                                                    value: id,
                                                                    id: item

                                                                };
                                                            }));
                                                        }
                                                    });
                                                },
                                                select: function (event, ui) {
                                                    var idpr = ui.item.id;
                                                    var alm = $('#almacen').val();
                                                    $('#idAlm').val(alm);
                                                    var url = 'pedido_detalle.php';
                                                    $.ajax({
                                                        type: 'POST', url: url,
                                                        data: 'id=' + idpr + '&idalm=' + alm,
                                                        success: function (valor) {

//                                                            var datos = eval(valor);

                                                            $('#grilla tbody').html(valor);
                                                            var tablita = document.getElementById("grilla");
                                                            var d = tablita.rows.length - 1;
                                                            $('#conta').val(d);
                                                            var alma = $('#almacen').val();
                                                            $('#pasarAlma').val(alma);

                                                            var prove = $('#prove').val();
                                                            $('#proveedor').val(prove);
                                                            //                                                            $('#conta').val(datos[1]);  
                                                            var subt = $('#subto' + (d - 1)).val();
                                                            eliminar_detalle_prod();
                                                            $('#subTotal').val(subt);
                                                            $('#total_compra').val(subt);
                                                            $('#compra_saldo').val(subt);

                                                            var prov = $('#idprov').val();



                                                            prov = parseFloat(prov);
                                                            $('#codigoProve').val(prov);
                                                        }
                                                    });
                                                }
                                            });
                                        </script>


                                        <i class="glyphicon glyphicon-search circular blue"></i>
                                    </span>
                                </div>
                                <label class="col-lg-2 control-label"><strong>  Nro. Factura :</strong></label>
                                <div class="col-lg-2">
                                    <strong><input size="5" type="text" class="form-control" style="text-align: right"  name="nrofact" id="nrofact" readonly/></strong>           
                                </div>
                                <label class="col-lg-2 control-label"><strong> Seleccione Almacen :</strong></label>
                                <div class="col-lg-2">
                                    <strong>
                                        <?php
                                        require_once("../datos/Database.php");
                                        $dat = Database::getInstance()->getDb()->prepare("SELECT * FROM Almacen WHERE idSucursal = '$idSucursal'  ");
                                        $dat->execute();
                                        ?>
                                        <select  name="almacen" id="almacen"   data-bv-field="country"  >
                                            <?php while ($row = $dat->fetch()) { ?>

                                                <option value="<?= $row['idAlmacen'] ?>" ><?= $row['nombreAlmacen'] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select><i class="form-control-feedback" data-bv-field="country" style="display: none;" ></i>

                                    </strong>           
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>  Proveeedor :</strong></label>
                                <div class="col-lg-2">
                                    <strong><input size="5" type="text" class="form-control" value="" name="proveedor" id="proveedor" readonly/></strong>
                                </div>
                                <label class="col-lg-2 control-label"><strong>  Nro. Control:</strong></label>
                                <div class="col-lg-2">
                                    <strong><input size="5" type="text" class="form-control" style="text-align: right"  name="nroControl" id="nroControl" readonly/></strong>
                                </div>
                                <label class="col-lg-2 control-label"><strong>  Nro. Autorizacion:</strong></label>
                                <div class="col-lg-2">
                                    <strong><input size="5" type="text" class="form-control" style="text-align: right"  name="nroAuto" id="nroAuto" readonly/></strong>
                                </div>
                            </div>
                            <br>
                            <center>                              
                                <h2>
                                    <strong>
                                        DETALLE
                                    </strong>                                                                     
                                </h2>

                            </center>



                            <input type="text" name="codigoProve" id="codigoProve" value="" hidden/>
                            <input type="text" id="subT" name="subT" hidden/>
                            <input type="text" id="pasarAlma" name="pasarAlma" hidden/>
                            <input type="text" id="idAlm" name="idAlm"  hidden/>
                            <div class="table-responsive" >
                                <table id="grilla" class="table table-striped">
                                    <thead>
                                        <tr>

                                            <th><strong>Nro</strong></th>
                                            <th><strong>Codigo</strong></th>
                                            <th><strong>Descripcion</strong></th>                                                    
                                            <th><strong>P Costo</strong></th>
                                            <th><strong>P Venta</strong></th>
                                            <th><strong>Margen</strong></th>
                                            <th><strong>Cant</strong></th>
                                            <th><strong>Nro Lote</strong></th>
                                            <th><strong>Sector</strong></th>
                                            <th><strong>Fecha V</strong></th>
                                            <th><strong>Monto</strong></th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                        <!--CONTADOR DE FILAS DE LA TABLA -->
                                    <input type="text" id="conta" name="conta" value="1" hidden/><!--CONTADOR DE FILAS DE LA TABLA -->
                                    <input type="text" id="monts" name="monts" hidden/><!--CONTADOR DE FILAS DE LA TABLA -->
                                    <input type="text" name="idAlm" id="idAlm" value="<?= $idSucursal ?>" hidden/>
                                    <td></td>
                                    <td><input size="10" type="text" class="form-control" id="codigo" name="codigo"  style="text-align: right" value="-" readonly/><input type="text" id="codi" name="codi" hidden/><input type="text" id="idProd" name="idProd"  hidden/></td>
                                    <td><input size="80" type="text" class="form-control" id="descripcion" name="descripcion"  value=""  />
                                        <script>

                                            $('#descripcion').autocomplete({
                                                source: function (request, response) {
                                                    $.ajax({
                                                        url: 'autocompletado.php',
                                                        dataType: "json",
                                                        data: {
                                                            name_startsWith: request.term,
                                                            type: 'Detalle'
                                                        },
                                                        success: function (data) {
                                                            response($.map(data, function (item, id) {
                                                                return {
                                                                    label: id,
                                                                    value: id,
                                                                    id: item

                                                                };
                                                            }));
                                                        }
                                                    });
                                                },
                                                select: function (event, ui) {
                                                    var idpr = ui.item.id;
                                                    //                                                var contado = $('#conta').val();
                                                    var url = 'busqueda_pedido_detalle.php';
                                                    $.ajax({
                                                        type: 'POST', url: url,
                                                        data: 'id=' + idpr,
                                                        success: function (valores) {
                                                            var datos = eval(valores);
                                                            $('#precioC').val(datos[0]);
                                                            $('#precioV').val(datos[1]);
                                                            $('#descripcion').val(datos[2]);
                                                            $('#margen').val(datos[3]);
                                                            $('#idProd').val(datos[4]);
                                                            $('#codi').val(datos[5]);
                                                            $('#nombreSect').val(datos[6]);
                                                            $('#cantidad').focus();
                                                            //                                                        $('#contas').val(contado);
                                                            //                                                    
                                                        }
                                                    });

                                                }
                                            });
                                        </script>

                                        <!--CODIGO DADO AL PRODUCTO-->
                                        <!--CODIGO PRODUCTO-->

                                    </td>                                       
                                    <td><input size="20" name="precioC" type="text" id="precioC" class="form-control" style="text-align: right"  onkeyup="cal_PrecioV()" required /> </td>
                                    <td><input size="20" name="precioV" type="text" id="precioV" class="form-control" style="text-align: right"   onkeyup="cal_Margen()" required />  </td>
                                    <td><input size="10" name="margen" type="text" id="margen" class="form-control" style="text-align: right" onkeypress="validarNumeroVentas2(event);"  onkeyup="cal_PrecioV()" required /> </td>
                                    <td><input size="10"  name="cantidad" type="text" id="cantidad" class="form-control" style="text-align: right"  onkeypress="validarNumeroVentas(event);"  onkeyup=" cal_cantidad()" required /> </td>
                                    <td><input size="20" type="text" id="lote" name="lote" class="form-control" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" required /></td>
                                    <td> <?php
                                        require_once("../datos/Database.php");
                                        $datos = Database::getInstance()->getDb()->prepare("SELECT distinct s.nombreSector ,s.idSector  FROM Ubicacion ubi , Sectores s WHERE ubi.idAlmacen = '$idSucursal' and s.idSector = ubi.idSector  GROUP BY s.nombreSector");
                                        $datos->execute();
                                        ?>
                                        <select  name="nombreSect" id="nombreSect"   data-bv-field="country">
                                            <?php while ($row = $datos->fetch()) { ?>               
                                                <option value="<?= $row['nombreSector'] ?>"  ><?= $row['nombreSector'] ?></option>
                                            <?php } ?>
                                        </select><i class="form-control-feedback" data-bv-field="country" style="display: none;" ></i>
                                    </td>
                                    <td><input size="20" class="form-control" id="fechaven" name="fechaven" data-date-format="dd-mm-yyyy" type="date" required /> </td>
                                    <td><input size="40" class="form-control" name="totalProducto" type="text" id="totalProducto"   style="text-align: right" value="" required /> </td>
                                    <td>                                          
                                        <strong><input   name="agregar" id="agregar" value="+" class="btn-primary"  type="submit"/></strong>
                                    </td> 
                                    </tfoot>
                                </table>
                            </div>
                            <br>
                            <table align="right" >
                                <tr>

                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td ><strong>Sub Total</strong></td>
                                    <td class="text-center"> <input type="text" class="form-control" style="text-align: right"   id="subTotal"  name="subTotal"  size="8" /> </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow"></td> 
                                    <td><br></td>
                                    <td><br></td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td >Descuento BS &nbsp; </td>
                                    <td class="text-center"><input type="text" id="compra_descuento1"  class="form-control"  style="text-align: right" name="compra_descuento1" size="8" onkeyup="descuentos();" /></td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow"></td> 
                                    <td><br></td>
                                    <td><br></td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td >Descuento BS &nbsp; </td>
                                    <td class="text-center"><input type="text" id="compra_descuento2"  class="form-control" style="text-align: right"  name="compra_descuento2" size="8" onkeyup="descuentos();" /></td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="invisible bg-snow"></td> 
                                    <td><br></td>
                                    <td><br></td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td >Descuento BS &nbsp; </td>                                  
                                    <td class="text-center"><input type="text" id="compra_descuento3"  class="form-control" style="text-align: right"   name="compra_descuento3" size="8" onkeyup="descuentos();" /></td>
                                </tr>  
                                <tr>
                                    <td colspan="6" class="invisible bg-snow"></td> 
                                    <td><br></td>
                                    <td><br></td>
                                </tr>
                                <tr>

                                    <td colspan="8" class="invisible bg-snow"></td>
                                    <td class="text-center"><strong>Total</strong></td>
                                    <td class="text-center "><strong><input type="text"  class="form-control" style="text-align: right"  id="total_compra" name="total_compra"  size="8" /></strong></td>
                                </tr>
                            </table>
                            <br>
                            <div class="row" role="form">

                            </div>


                            <div class="form-inline">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;
                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><strong>Contado</strong></label>
                                    <div class="col-lg-3">
                                        <strong><input size="10" style="text-align: right"  type="text" class="form-control" name="compra_contado" id="compra_contado" onkeyup="descuentos();" /></strong>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label"><strong>Fecha de Pago</strong></label>
                                    <div class="col-lg-4">
                                        <input size="10" id="compra_fechaPago" data-date-format="dd-mm-yyyy" name="compra_fechaPago" type="date" placeholder="Fecha" value="" />
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label class="col-lg-5 control-label"><strong>Interes / Costo Adiciona</strong></label>
                                    <div class="col-lg-4">
                                        <strong><input type="text" id="compra_montoInteres"  class="form-control" style="text-align: right"   name="compra_montoInteres" size="8" onkeyup="descuentos();" /></strong>

                                    </div>
                                </div>
                                <br>
                                <br>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;                        
                                <strong><input size="10" type="text"  name="compra_saldofavor" id="compra_saldofavor"  hidden/></strong>

                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><strong>Credito</strong></label>
                                    <div class="col-lg-3">
                                        <strong><input size="10" type="text" class="form-control" name="compra_credito" style="text-align: right"  id="compra_credito"/></strong>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-6 control-label"><strong>Saldo</strong></label>
                                    <div class="col-lg-4">
                                        <strong><input size="10" type="text" class="form-control" name="compra_saldo" style="text-align: right"  id="compra_saldo" /></strong>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#guardarCompra').attr('disabled', true);
                                    $('#compra_saldo').attr('readonly', true);
                                    $('#compra_credito').attr('readonly', true);
                                    $('#compra_montoInteres').attr('readonly', true);
                                    $('#compra_fechaPago').attr('readonly', true);
                                    $('#compra_contado').attr('readonly', true);
                                });
                            </script>
                            <div class="form-inline" >
                                <center>

                                    <br/>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-primary"  id="guardarCompra" name="guardarCompra" onclick="registrarDetalle_pedido();" value="Guardar">
                                            </div>

                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-success" onclick="imprimirCompraP()"  id="imprimir" name="imprimir" value="Imprimir">
                                            </div>

                                            <div class="col-lg-3">
                                                <a type="button" href="index.php" class="btn btn-danger"  > Cancelar</a>
                                            </div>
                                        </div>
                                    </div>
                                </center> 
                            </div>
                        </form>

                        <br>  
                        <br>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>


</div>
<?php
require_once ('../header_footer/footer.php');
?> 