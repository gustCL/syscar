<?php
require_once '../datos/Database.php';
session_start();
$id_usu = $_SESSION['userMaster'];
$idSucursal = $id_usu['idSucursal'];


$dato = $_POST['id'];
$almacen =$_POST['idalm'];
$consulta = "SELECT DISTINCT p.codigoProducto, pro.nombreComercial, s.nombreSector, pro.codigo, p.cantidad, prov.nombreProve, prov.idProveedor, pre.precioCosto, pre.precioVenta, ROUND( ((pre.precioVenta - pre.precioCosto) *100 ) / pre.precioCosto) AS margen 
                FROM Compra c, Pedido p, Producto pro, Almacen a, Proveedor prov, Sectores s, Ubicacion u, PrecioSucursal pre
                WHERE prov.idProveedor = c.idProveedor
                AND a.idSucursal = c.idSucursal
                AND c.idSucursal ='$idSucursal'
                AND c.idCompra = p.idCompra
                AND p.idCompra ='$dato'
                AND c.estado =1
                AND pro.codigoProducto = p.codigoProducto
                AND a.idAlmacen ='$almacen'
                AND pro.codigoProducto = u.codigoProducto
                AND s.idSector = u.idSector
                AND s.idSector = u.idSector
                AND a.idAlmacen = s.idAlmacen
                AND pre.codigoProducto = p.codigoProducto
                GROUP BY p.codigoProducto";
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$subTotal=0;
$contador = 1;
$tabla = '';
while ($valores = $comando->fetch()) {
    $sect = $valores['nombreSector'];
    $prod = $valores['codigoProducto'];
    $codigo = $valores['codigo'];
    $nombreProd = $valores['nombreComercial'];
    $precioCosto = $valores['precioCosto'];
    $cantidad = $valores['cantidad'];
    $mtotal = $precioCosto * $cantidad;
    $prov = $valores ['nombreProve'];
    $idProve = $valores['idProveedor'];
    $subTotal = $subTotal + $mtotal;
 
    $Pvent = $valores['precioVenta'];
    $marg = $valores['margen'];
    
    ?>

    <tr>
    <!--CONTADOR DE FILAS DE LA TABLA -->
        <td> <?= $contador ?> </td>
        <td><input size="25" type="text" class="form-control" id="codigoT" name="codigoT<?= $contador ?>"  style="text-align: right" value="<?= $codigo ?>" readonly/><input  type="text"  id="codig" name="codig<?= $contador ?>" value="<?= $prod ?>" hidden/><input  type="text"  id="subto<?= $contador ?>" name="subto" value="<?= $subTotal ?>" hidden/> </td>
        <td><input size="80" type="text" class="form-control" id="descripcionT" name="descripcionT<?= $contador ?>"  value="<?= $nombreProd ?>" readonly/><input  type="text"  id="anSector" name="anSector<?= $contador ?>" value="" hidden/><input  type="text"  id="idprov" name="idprov" value="<?= $idProve ?>" hidden/><input type="text" id="contas" name="contas" value="<?= $contador ?>" hidden/> </td>
        <td><input size="20" type="text" class="form-control" id="precioCT<?= $contador ?>" name="precioCT<?= $contador ?>"  style="text-align: right" value="<?= $precioCosto ?>" onkeyup="calculoPrecioVP(<?= $contador ?>)" /> </td>
        <td><input size="20" type="text" class="form-control" id="precioVT<?= $contador ?>" name="precioVT<?= $contador ?>"  style="text-align: right" value="<?= $Pvent ?>" onkeyup="calculoMargenCompraPed(<?= $contador ?>)" />  </td>
        <td><input size="10" type="text" class="form-control" id="margenT<?= $contador ?>" name="margenT<?= $contador ?>"  style="text-align: right" value="<?= $marg ?>" onkeyup="calculoPrecioVP(<?= $contador ?>)"  /> </td>
        <td><input size="20" type="text" class="form-control" id="cantidadT<?= $contador ?>" name="cantidadT<?= $contador ?>"  style="text-align: right" value="<?= $cantidad ?>" onkeyup="calculoCantidad(<?= $contador ?>)"  /> </td>
        <td><input size="20" type="text" class="form-control" id="nroloteT" name="nroloteT<?= $contador ?>" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();"  required/></td>
        <td> <?php
            require_once("../datos/Database.php");
            $datos = Database::getInstance()->getDb()->prepare("SELECT distinct s.nombreSector ,s.idSector  FROM Ubicacion ubi , Sectores s,  Almacen a WHERE  s.idAlmacen = '$almacen' and a.idAlmacen = s.idAlmacen and a.idSucursal = '$idSucursal'  GROUP BY s.nombreSector");
            $datos->execute();
            ?>
            <select  name="sectorT<?= $contador ?>" id="sectorT<?= $contador ?>"   data-bv-field="country" >
                <?php while ($row = $datos->fetch())  { 
                          $selected = "";
                if ($sect == $row['nombreSector']) {
                 $selected = 'selected';
                }           
               ?>      
                     <option value="<?= $row['nombreSector'] ?>" <?= $selected ?>  ><?= $row['nombreSector'] ?></option>
                <?php } ?>
               
            </select><i class="form-control-feedback" data-bv-field="country" style="display: none;" ></i>
            
        </td>  
        <td><input  class="form-control date-picker" id="fechaT" name="fechaT<?= $contador ?>" data-date-format="dd-mm-yyyy" type="date" required/> </td>
        <td><input type="text" size="80" class="form-control" id="montoT<?= $contador ?>" name="montoT<?= $contador ?>"  style="text-align: right" value="<?= $mtotal ?>" /><input id="al" name="al" value="<?= $almacen?>" hidden/><input id="prove" name="prove" value="<?= $prov ?>" hidden /> </td>
        <td><a class="elimina"><i class="fa fa-trash-o" /></a></td>                     
        </tr> 
    <?php
 
    $contador = $contador +1;
}

?>
