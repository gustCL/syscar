<?php
require_once 'head.php';

$suc = $_SESSION['userMaster'];
$idSucursal = $suc['idSucursal'];

$codigo = $_POST['codigoP'];
$codigoAlmacen = $_POST['codigoAl'];

$consulta = Database::getInstance()->getDb()->prepare("select * from Almacen where idSucursal = '$idSucursal' and idAlmacen='$codigoAlmacen' ");
$consulta->execute();
$row_d = $consulta->fetch();
$nombAlmacen = $row_d['nombreAlmacen'];
$idAlmacen = $row_d['idAlmacen'];
$con = "SELECT * FROM Proveedor WHERE idProveedor = '$codigo' ";
$dato = Database::getInstance()->getDb()->prepare($con);
$dato->execute();
$fila = $dato->fetch();
$nit = $fila['nit'];
$nomb = $fila['nombreProve'];
$nombreContact = $fila['nombreContacto'];
$idProv = $fila['idProveedor'];


$nrofila = 0;
$cant = 0;
$subto = 0;
$total = 0;
$saldo = 0;
$saldoFavor = 0;
$montocredito = 0;
?>
<script src="../js/jquery.js"></script>
<script src="jfactura_compra.js"></script>
<script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
<script src="../js/autocompletado/jquery-ui-1.10.3.custom.min.js"></script> 
<!--<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/additional-methods.min.js"></script>-->
<link rel="stylesheet" href="../js/autocompletado/jquery-ui-1.10.3.custom.min.css"> 
<div class="page-content">

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><STRONG>
                    COMPRA
                </STRONG>

            </h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div> 
    </div>
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">


                    <div class="widget-body ">

                        <form method="post" class="form-horizontal" action="" id="form1" >                          
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>  Almacen :</strong></label>
                                <div class="col-lg-2">
                                    <label class="col-lg-2 control-label"><strong><?= $nombAlmacen ?></strong></label>                              
                                </div>
                                <label class="col-lg-2 control-label"><strong>  Nro. Factura :</strong></label>
                                <div class="col-lg-2">
                                    <strong><input size="5" type="text" class="form-control" style="text-align: right"  name="nrofactura" id="nrofactura" required /></strong>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label"><strong>  Nro. Autorizacion:</strong></label>
                                <div class="col-lg-2">
                                    <strong><input size="5" type="text" class="form-control" style="text-align: right"  name="nroAuto" id="nroAuto" required/></strong>
                                </div>
                                <label class="col-lg-2 control-label"><strong>  Nro. Control:</strong></label>
                                <div class="col-lg-2">
                                    <strong><input size="5" type="text" class="form-control" style="text-align: right"  name="nroControl" id="nroControl" required/></strong>
                                </div>
                            </div>
                        </form>
                    </div>


                    <br>


                    <div class="widget-body">
                        <form method="post"  action="javascript: addfila();" id="frm_usu" >
                            <div class="form-group">
                                <center><h2><strong>DETALLE</strong></h2></center>                     
                            </div>

                            <!-- AGREGAR PRODUCTOS-->

                            <div class="row">
                                <div class="col-sm-1">
                                </div>
                                <input type="text" name="codigoProve" id="codigoProve" value="<?= $idProv ?>" hidden/>
                                <input type="text" name="idAlm" id="idAlm" value="<?= $codigoAlmacen ?>" hidden/>
                                <div class="col-sm-5"> 
                                    <label class="control-label"><strong>Descripcion</strong></label>
                                    <input  id="descripcion" name="descripcion" type="text" Class="form-control"  placeholder="Todos" required readonly/>
                                    <script>
                                        var cont = 0;
                                        $('#descripcion').autocomplete({
                                            source: function (request, response) {
                                                $.ajax({
                                                    url: 'autocompletado.php',
                                                    dataType: "json",
                                                    data: {
                                                        name_startsWith: request.term,
                                                        type: 'Detalle'
                                                    },
                                                    success: function (data) {
                                                        response($.map(data, function (item, id) {
                                                            return {
                                                                label: id,
                                                                value: id,
                                                                id: item

                                                            };
                                                        }));
                                                    }
                                                });
                                            },
                                            select: function (event, ui) {
                                                var idpr = ui.item.id;
                                                $('#frm_usu')[0].reset();
                                                var url = 'compra_detalle.php';
                                                $.ajax({
                                                    type: 'POST', url: url,
                                                    data: 'id=' + idpr,
                                                    success: function (valores) {
                                                        var datos = eval(valores);
                                                        cont = cont + 1;
                                                        $('#contas').val(cont);
                                                        $('#precioC').val(datos[0]);
                                                        $('#precioV').val(datos[1]);
                                                        $('#descripcion').val(datos[2]);
                                                        $('#margen').val(datos[3]);
                                                        $('#idProd').val(datos[4]);
                                                        $('#codig').val(datos[5]);
                                                        $('#cantidad').focus();
                                                        var factura = $('#nrofactura').val();
                                                        $('#factura').val(factura);

                                                    }
                                                });
                                                var idprod = ui.item.id;
                                                var idAlamacen = $('#idAlm').val();
                                                var url = 'detalle_ubicaciones.php';
                                                $.ajax({
                                                    type: 'POST', url: url,
                                                    data: 'id=' + idprod + '&idAlma=' + idAlamacen,
                                                    success: function (valores) {
                                                        var datos = eval(valores);
                                                        $('#antSector').val(datos[0]);
                                                        var encontr = $('#antSector').val();
                                                        $('#nombreSector').val(encontr);
                                                        var nAuto = $('#nroAuto').val();
                                                        $('#nAuto').val(nAuto);
                                                        var nControl = $('#nroControl').val();
                                                        $('#nControl').val(nControl);
                                                        var subt = $('#suTotal').val();
                                                        $('#subT').val(subt);
                                                    }
                                                });

                                            }
                                        });
                                    </script>
                                    <input type="text" id="contas" name="contas" hidden /><!--CONTADOR DE FILAS DE LA TABLA -->
                                    <input type="text" id="idProd" name="idProd" hidden /><!--CODIGO DADO AL PRODUCTO-->
                                    <input type="text" id="codig" name="codig" hidden/><!--CODIGO PRODUCTO-->



                                </div>
                                <div class="col-sm-2">
                                    <label class="control-label"><strong>P Costo</strong></label>
                                    <input name="precioC" type="text" id="precioC" class="form-control"  onkeyup="cal_PrecioV()" readonly/>
                                </div>
                                <div class="col-sm-2">
                                    <label class="control-label"><strong>P Venta</strong></label>
                                    <input name="precioV" type="text" id="precioV" class="form-control"   onkeyup="cal_Margen()" readonly/>
                                </div>
                                <div class="col-sm-1">
                                    <label class="control-label"><strong>Margen%</strong></label>
                                    <input name="margen" type="text" id="margen" class="form-control" onkeypress="validarNumeroVentas2(event);"  onkeyup="cal_PrecioV()" readonly/>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-sm-1">
                                </div>                                        
                                <div class="col-sm-1">
                                    <br>
                                    <label class="control-label"><strong>Cant</strong></label>
                                    <input name="cantidad" type="text" id="cantidad" class="form-control" style="text-align: right"  onkeypress="validarNumeroVentas(event);"  onkeyup=" cal_cantidad()" required readonly/>
                                </div>
                                <div class="col-sm-1">
                                    <br>
                                    <label class="control-label"><strong>Nro_Lote</strong></label>
                                    <input type="text" id="lote" name="lote" class="form-control" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();"  required readonly/>
                                </div>
                                <div class="col-sm-1">
                                    <br>
                                    <label class="control-label"><strong>Fecha_V</strong></label>
                                    <input size="20"  id="fechaven" name="fechaven" data-date-format="DD-MM-AAAA" type="date" placeholder="Fecha"   required readonly/>
                                </div>
                                <div class="col-lg-1"></div>
                                <input name="totalProducto" type="text" id="totalProducto" hidden/>

                                <div class="col-lg-1">
                                    <br>
                                    <label class="control-label"><strong>Sector</strong></label>
                                    <?php
                                    /* Aqui se cargan los cargos */
                                    require_once("../datos/Database.php");
                                    $datos = Database::getInstance()->getDb()->prepare("SELECT distinct s.nombreSector ,s.idSector  FROM Sectores s ");
                                    $datos->execute();
                                    ?>
                                    <select  name="nombreSector" id="nombreSector"   data-bv-field="country" >
                                        <?php while ($row = $datos->fetch()) { ?>               
                                            <option value="<?= $row['nombreSector'] ?>"  ><?= $row['nombreSector'] ?></option>
                                        <?php } ?>
                                    </select><i class="form-control-feedback" data-bv-field="country" style="display: none;"></i>
                                </div>
                                <div class="col-sm-1">
                                    <br>
                                    <label class="control-label"><strong>Esta en</strong></label>
                                    <input type="text" id="antSector" name="antSector" class="form-control" readonly/>
                                </div>
                                <div class="col-sm-1">
                                    <br>
                                    <label class="control-label"><strong>&nbsp;</strong></label>
                                    <input  name="agregar" type="submit" id="agregar" value="Agregar Producto" class="btn btn-primary"/>
                                </div>
                            </div>

                            <br>
                            <br>

                        </form>
                        <br/>   

                    </div>
                    <br>
                    <div class="widget-body "> 
                        <form id="frm_usu123" method="post" action="">
                            <!-- TABLA DE NOTA DE VENTA-->
                            <input type="text" id="factura" name="factura" hidden/><!--NRO FACTURA-->
                            <input type="text" id="nAuto" name="nAuto" hidden/><!--NRO AUTO-->
                            <input type="text" id="subT" name="subT" hidden/><!--NRO SUBTOTAL-->
                            <input type="text" id="nControl" name="nControl" hidden/><!--NRO CONTROL-->
                            <input type="text" name="codigoProve" id="codigoProve" value="<?= $idProv ?>" hidden/>
                            <input type="text" name="idAlm" id="idAlm" value="<?= $codigoAlmacen ?>" hidden/>
                            <input type="text" id="cont2" name="cont2" hidden>
                            <div class="table-responsive">
                                <table id="grilla" class="table table-striped">
                                    <thead>
                                        <tr>

                                            <th><strong>Codigo</strong></th>
                                            <th><strong>Descripcion</strong></th>                                                    
                                            <th><strong>P_Costo</strong></th>
                                            <th><strong>P_Venta</strong></th>
                                            <th><strong>Margen</strong></th>
                                            <th><strong>Cant</strong></th>
                                            <th><strong>Nro_Lote</strong></th>
                                            <th><strong>Sector</strong></th>
                                            <th><strong>Fecha V</strong></th>
                                            <th><strong>Monto</strong></th>
                                            <th><strong></strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>

                                        <tr>

                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td ><strong>Sub Total</strong></td>
                                            <td class="text-center"> <input type="text" class="form-control" style="text-align: right"   id="subTotal"  name="subTotal"  size="8" /> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td >Descuento BS</td>
                                            <td class="text-center"><input type="text" id="compra_descuento1"  class="form-control"  style="text-align: right" name="compra_descuento1" size="8" onkeyup="descuentos();" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td >Descuento BS</td>
                                            <td class="text-center"><input type="text" id="compra_descuento2"  class="form-control" style="text-align: right"  name="compra_descuento2" size="8" onkeyup="descuentos();" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td >Descuento BS</td>
                                            <td class="text-center"><input type="text" id="compra_descuento3"  class="form-control" style="text-align: right"   name="compra_descuento3" size="8" onkeyup="descuentos();" /></td>
                                        </tr>                                    
                                        <tr>

                                            <td colspan="8" class="invisible bg-snow"></td>
                                            <td class="text-center"><strong>Total</strong></td>
                                            <td class="text-center "><strong><input type="text"  class="form-control" style="text-align: right"  id="total_compra" name="total_compra"  size="8" onkeyup="descuentos();"  /></strong></td>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>

                            <br>

                            <div class="form-inline">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;
                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><strong>Contado</strong></label>
                                    <div class="col-lg-3">
                                        <strong><input size="10" style="text-align: right"  type="text" class="form-control" name="compra_contado" id="compra_contado"  onkeyup="descuentos();" /></strong>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label"><strong>Fecha de Pago</strong></label>
                                    <div class="col-lg-4">
                                        <input size="10" data-date-format="DD-MM-AAAA"   id="compra_fechaPago" name="compra_fechaPago" type="date" placeholder="Fecha" value="<?= $row['fechaPago'] ?>" />
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label class="col-lg-5 control-label"><strong>Interes / Costo Adiciona</strong></label>
                                    <div class="col-lg-4">
                                        <input type="text" id="compra_montoInteres"  class="form-control" style="text-align: right"   name="compra_montoInteres" size="8"  onkeyup="descuentos();" />
                                    </div>
                                </div>
                                <br>
                                <br>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;                        
                                <strong><input size="10" type="text"  name="compra_saldofavor" id="compra_saldofavor" value="0"  hidden/></strong>

                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><strong>Credito</strong></label>
                                    <div class="col-lg-3">
                                        <strong><input size="10" type="text" class="form-control" name="compra_credito" style="text-align: right"  id="compra_credito"/></strong>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-6 control-label"><strong>Saldo</strong></label>
                                    <div class="col-lg-4">
                                        <strong><input size="10" type="text" class="form-control" name="compra_saldo" style="text-align: right"  id="compra_saldo" /></strong>
                                    </div>
                                </div>

                            </div>
                            <br>

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#guardar').attr('disabled', true);

                                });
                            </script>

                            <div class="form-inline" >
                                <center>

                                    <br/>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-primary"  id="guardar" name="guardar" onclick="registrarDetalle_compra();" value="Guardar" />
                                            </div>

                                            <div class="col-lg-3">
                                                <input type="button" class="btn btn-success"  id="facturar" name="facturar" onclick="imprimirCompra()" value="Imprimir"/>
                                            </div>

                                            <div class="col-lg-3">
                                                <a type="button" href="index.php" class="btn btn-danger"  > Cancelar</a>
                                            </div>
                                        </div>
                                    </div>
                                </center> 
                            </div>
                            <br>  
                            <br>
                            <br>
                            <br>
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>


</div>
<?php
require_once ('../header_footer/footer.php');
?> 