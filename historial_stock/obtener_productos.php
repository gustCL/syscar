<?php
require_once '../datos/conexion.php';
require_once '../datos/Database.php';
include "../extras/PHPPaging.lib/PHPPaging.lib.php";

if (isset($_GET['val'])) {
    $indicio = $_GET['val'];
    $query = "SELECT ex.idExistencia,pr.codigoProducto,pr.nombreComercial,lo.cantidad, lo.fechaVencimiento,lo.merma
              FROM Producto pr, Lote lo, Existencias ex
              WHERE pr.codigoProducto = lo.codigoProducto 
              AND pr.codigoProducto = lo.codigoProducto AND pr.estado = 1 AND ex.terminado = 1
              AND lo.idLote = ex.idLote AND (pr.nombreComercial LIKE '$indicio%') ORDER BY pr.nombreComercial,lo.fechaVencimiento";
} else {
    $query = "SELECT ex.idExistencia,pr.codigoProducto,pr.nombreComercial,lo.cantidad, lo.fechaVencimiento, lo.merma
              FROM Producto pr, Lote lo, Existencias ex
              WHERE pr.codigoProducto = lo.codigoProducto 
              AND pr.codigoProducto = lo.codigoProducto AND pr.estado = 1 AND ex.terminado = 1
              AND lo.idLote = ex.idLote ORDER BY pr.nombreComercial,lo.fechaVencimiento";
}

$pagina = new PHPPaging;
$pagina->agregarConsulta($query);
$pagina->modo('desarrollo');
$pagina->verPost(true);
$pagina->porPagina(12);
$pagina->paginasAntes(4);
$pagina->paginasDespues(4);
$pagina->linkSeparador(" - "); //Significa que no habrá separacion
$pagina->div('div-producto');
$pagina->linkSeparadorEspecial('...');   // Separador especial
$pagina->ejecutar();
?>
<script src="../extras/sortable/js/sortable.js"></script>
<table class="table table-striped ">
    <thead>
    <tr>

        <th hidden>ID</th>
        <!-- <th>NRO.</th>-->
        <th width="30%">NOMBRE COMERCIAL</th>
        <th width="10%">FECHA</th>
        <th width="12.5%">STOCK INGRESO</th>
        <th width="12.5%">STOCK EGRESO</th>
        <th width="10%">STOCK</th>
        <th width="10%">MERMA</th>
        <th width="15%">STOCK DISPONIBLE</th>


        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nro = 0;
    while ($result = $pagina->fetchResultado()) :  ?>
        <?php
        $idExistencia = $result['idExistencia'];
        $codigoP = $result['codigoProducto'];
        $fechaV = date('d-m-Y',strtotime($result['fechaVencimiento']));
        //consulta para la STOCK EGRESO
        $cmd_stock_egreso = Database::getInstance()->getDb()->prepare("SELECT SUM(dt.cantidad) AS stockEgreso FROM DetalleTransaccion dt WHERE dt.idExistencia = '$idExistencia'");
        $cmd_stock_egreso->execute();
        $stock_egreso = $cmd_stock_egreso->fetch();
        ?>
        <tr>
            <td hidden>
                <input type="number" name="codproducto[]" value="<?= $result['codigoProducto']; ?>">
            </td>
            <!-- <td><? /*= $nro; */ ?></td>-->
            <td><?= $result['nombreComercial']; ?></td>
            <td><?= $fechaV; ?></td>
            <td><?= $result['cantidad']; ?></td>
            <td><?=$stock_egreso['stockEgreso']; ?></td>
            <td> <?= $result['cantidad']-$stock_egreso['stockEgreso']?></td>


            <!--<td><input type="text" id="merma<?/*= $idExistencia */?>" name="merma<?/*= $idExistencia */?>" class="form-control"
                       size="100%" min="0" value="0" onkeypress="return onlyNumbers(event);"> </td>-->
            <td><?= $result['merma']?></td>
            <td><?=$result['cantidad']-$stock_egreso['stockEgreso']-$result['merma']?></td>
            <td><button class="btn btn-sm btn-primary" onclick="addMerma(<?= $idExistencia?>)">AGREGAR MERMA</button></td>
        </tr>
    <?php endwhile; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="7">Numero de pagina: <?php echo $pagina->fetchNavegacion(); ?></td>
        </tr>
    </tfoot>
</table>