<?php
require_once '../datos/Database.php';
$producto_id = $_POST['id'];
$stock = $_POST['stock'];
//RECUPERAMOS LOS VALORES DEL PRODUCTO
$cmd_producto = Database::getInstance()->getDb()->prepare("SELECT * FROM Producto WHERE codigoProducto = '$producto_id'");
$cmd_producto->execute();
$datos_producto = $cmd_producto->fetch();
//insertamos un nuevo Lote
$nada = 'Nada';
$nroLote = 'XXX';
$fechaV ='02-02-2019';
//$cmd_lote_insert = " INSERT INTO Lote ( idLote, nroLote, codigoProducto, idNotaIngreso, precioCosto, precioVenta, margen, fechaVencimiento, cantidad, observaciones, estado) VALUES ('0','XXX','$id_prod','$idNotaIngreso','$PCosto','$PVenta','10','02-02-2019','$stock','nada','1')";
$cmd_lote_insert = Database::getInstance()->getDb()->prepare("INSERT INTO Lote VALUES(0,
                                                                                      :nroLote,
                                                                                      :codigoProducto,
                                                                                      1,
                                                                                      :precioCosto,
                                                                                      :precioVenta,
                                                                                      :margen,
                                                                                      :fechaVencimiento,
                                                                                      :cantidad,
                                                                                      :observaciones,
                                                                                      1)");
$cmd_lote_insert->bindParam(':nroLote', $nroLote);
$cmd_lote_insert->bindParam(':codigoProducto', $datos_producto['codigoProducto']);
$cmd_lote_insert->bindParam(':precioCosto', $datos_producto['precioCosto']);
$cmd_lote_insert->bindParam(':precioVenta', $datos_producto['precioVenta']);
$cmd_lote_insert->bindParam(':margen', $datos_producto['margen']);
$cmd_lote_insert->bindParam(':fechaVencimiento', $fechaV);
$cmd_lote_insert->bindParam(':cantidad', $stock);
$cmd_lote_insert->bindParam(':observaciones',$nada);
$cmd_lote_insert->execute();

//reuperamos el id del ULTIMO LOTE INSERTADO
$DATO6 = Database::getInstance()->getDb()->prepare("SELECT Max(idLote) as idLote FROM Lote  ");
$DATO6->execute();
$count = $DATO6->fetch(PDO::FETCH_ASSOC);
$idlote1 = $count['idLote'];
///INSERTAMOS EN UBICACION
$cmd_lote_insert = Database::getInstance()->getDb()->prepare("INSERT INTO Ubicacion VALUES(0,
                                                                                           1,
                                                                                           :codigoProducto,
                                                                                           1)");
$cmd_lote_insert->bindParam(':codigoProducto', $producto_id);
$cmd_lote_insert->execute();
//recuperamos Ultima Ubicacion
$DATO10 = Database::getInstance()->getDb()->prepare("SELECT Max(idUbicacion) as idUbicacion FROM Ubicacion");
$DATO10->execute();
$count = $DATO10->fetch(PDO::FETCH_ASSOC);
$idUbica = $count['idUbicacion'];
///insertamos en EXISTENCIAS
$cmd_existencias_insert = Database::getInstance()->getDb()->prepare("INSERT INTO Existencias VALUES(0,
                                                                                                    :idLote,
                                                                                                    :idUbicacion,
                                                                                                    :stock,
                                                                                                    1,
                                                                                                    1)");
$cmd_existencias_insert->bindParam(':idLote', $idlote1);
$cmd_existencias_insert->bindParam(':idUbicacion', $idUbica);
$cmd_existencias_insert->bindParam(':stock',$stock);
$cmd_existencias_insert->execute();

exit();

?>