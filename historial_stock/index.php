<?php
require_once 'head.php';

//$productos = $db->Producto('estado = ?','1');/
// Obtener el stock de los productos activos
?>
<div class="page-content">
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>STOCK</strong></h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="widget-body">
                <div class="row">
                    <div class="col-md-offset-5 col-md-4">
                        <h3><strong>HISTORIAL DE INVENTARIO</strong></h3>
                    </div>
                    <div class="col-md-offset-4 col-md-4">
                         <span class="input-icon">
                             <input type="text" size="40" class="form-control"
                                    placeholder="Buscar Producto por : Codigo , Descripcion " id="bus-producto"/>
                             <i class="glyphicon glyphicon-search circular blue"></i>
                         </span>
                    </div>
                    <br>
                </div>
                <br>
                <form action="javascript: registrarStock();" class="form-horizontal" id="frm-registrar-stock">
                    <div class="table-responsive" id="div-producto">
                        <!-- atributo de table data-sortable-->
                        <!--atributo de class table-bordered table-hover sortable-theme-bootstrap-->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_add_merma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel"> </h4>
            </div>
            <form id="form-add-merma" name="form-add-merma"  action="javascript:registerMerma();" class="formulario ">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label> NOMBRE COMERCIAL:</label>
                                            <input type="text" class="form-control" id="merma-name" name="merma-name"
                                                   readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label> STOCK INGRESO:</label>
                                            <input type="text" class="form-control" id="merma-ingreso" name="merma-ingreso"
                                                   readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label> FECHA:</label>
                                            <input type="text" class="form-control" id="merma-fecha" name="merma-fecha"
                                                   readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label> STOCK EGRESO:</label>
                                            <input type="text" class="form-control" id="merma-egreso" name="merma-egreso"
                                                   readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label> STOCK DSPONIBLE:</label>
                                            <input type="text" class="form-control" id="merma-disponible" name="merma-disponible"
                                                   readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label> MERMA:</label>
                                            <input type="text" class="form-control" id="merma-realizada" name="merma-realizada"
                                                   readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-1">
                                        <div class="form-group">
                                            <label> AGREGAR MERMA:</label>
                                            <input type="text" class="form-control" id="merma-add" name="merma-add" required onkeypress="return onlyNumbers(event);">
                                            <input type="text" id="merma-idLote" name="merma-idLote" hidden>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <!--BOTONES-->
                            <br/>
                            <center>
                                <div class="form-group">
                                    <button  type="submit" class="btn btn-primary" id="btn-gradar-merma">Guardar
                                    </button>
                                    &nbsp;&nbsp;&nbsp;
                                    <button data-bb-handler="cancel" id="btn-cancel-merma" type="button" class="btn btn-danger"
                                            data-dismiss="modal" aria-hidden="true">Cancelar
                                    </button>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<?php
require_once '../header_footer/footer.php';
?>

