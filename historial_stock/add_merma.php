<?php
    require_once '../datos/Database.php';
    $idExistencia = $_POST['id'];
    //recuperamos los datos Existencias
    $cmd_datos_Existencia = Database::getInstance()->getDb()->prepare("SELECT ex.idExistencia,pr.codigoProducto,pr.nombreComercial,lo.cantidad, lo.fechaVencimiento,lo.merma,lo.idLote,lo.merma
                                                                      FROM Producto pr, Lote lo, Existencias ex
                                                                      WHERE pr.codigoProducto = lo.codigoProducto 
                                                                      AND pr.codigoProducto = lo.codigoProducto AND pr.estado = 1 AND ex.terminado = 1 AND ex.idExistencia = '$idExistencia'
                                                                      AND lo.idLote = ex.idLote ORDER BY pr.nombreComercial,lo.fechaVencimiento");
    $cmd_datos_Existencia->execute();
    $datos_existencias = $cmd_datos_Existencia->fetch();
    //query para calcular stockegreso y disponibe
    $cmd_stock_egreso = Database::getInstance()->getDb()->prepare("SELECT SUM(dt.cantidad) AS stockEgreso FROM DetalleTransaccion dt WHERE dt.idExistencia = '$idExistencia'");
    $cmd_stock_egreso->execute();
    $stock_egreso = $cmd_stock_egreso->fetch();
    $fechaV = date('d-m-Y',strtotime($datos_existencias['fechaVencimiento']));
    $data = array(
        0 => $datos_existencias['nombreComercial'],
        1 => $datos_existencias['cantidad'],
        2 => $fechaV,
        3 => $stock_egreso['stockEgreso'],
        4 => $datos_existencias['cantidad']-$stock_egreso['stockEgreso']-$datos_existencias['merma'],
        5 => $datos_existencias['merma'],
        6 => $datos_existencias['idLote']
    );
    echo json_encode($data);
    exit();
?>