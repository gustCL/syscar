
    <div id="ventas-realizadas" name="ventas-realizadas">
            <?php
            $nro = 0;
            $nro_venta = 0;
            $fecha_hora = "--";
            $monto_total = 0;
            $fila = array();
            $matriz;
            $i = 0; ?>
<?php       while ($row = $cmd_ventas->fetch()) {
                $nro = $nro + 1;
                $nro_venta = $row['nroVenta'];
                $nombre = $row['nombre'];
                $fecha_hora = $row['fecha'] . " - " . $row['hora'];
                $monto_de_venta = $row['montoTotal'];
                $monto_total = $monto_de_venta + $monto_total;

                $fila[0] = $nro;
                $fila[1] = $nro_venta;
                $fila[2] = get_nro_factura_from_venta($row['idVenta']);
                $fila[3] = $fecha_hora;
                $fila[4] = $monto_de_venta;

                $matriz[$i] = $fila;
                $i++;
            }
?>
<?php           if (count($matriz) <= 20) :                       ?>
                <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                    <thead>
                    <tr>
                        <th> Nro </th>
                        <th> Nro. Venta </th>
                        <th> Nro. Factura </th>
                        <th> Fecha - Hora </th>
                        <th> Monto Total </th>
                    </tr>
                    </thead>
                    <tbody>
<?php               for ($j = 0; $j < count($matriz); $j++) : ?>
                    <tr style="border: 1px solid black;">
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][0]; ?></td>
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][1]; ?></td>
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][2]; ?></td>
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][3]; ?></td>
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][4]; ?></td>
                    </tr>
    <?php           endfor; ?>
        <?php   endif;  ?>


    <?php       if (count($matriz) >= 20 && count($matriz) <=40 ) :                       ?>
                <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                    <thead>
                    <tr>
                        <th> Nro </th>
                        <th> Nro. Venta </th>
                        <th> Nro. Factura </th>
                        <th> Fecha - Hora </th>
                        <th> Monto Total </th>
                    </tr>
                    </thead>
                    <tbody>
<?php               for ($j = 0; $j < count($matriz); $j++) : ?>
                    <tr style="border: 1px solid black;">
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][0]; ?></td>
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][1]; ?></td>
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][2]; ?></td>
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][3]; ?></td>
                        <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][4]; ?></td>
                    </tr>
                    <div class="saltopagina"> </div>
        <?php       endfor; ?>
    <?php       endif;  ?>

<?php           if (count($matriz) > 40 ) :       $nro_registro = 0;                ?>
                    <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                        <thead>
                        <tr>
                            <th> Nro </th>
                            <th> Nro. Venta </th>
                            <th> Nro. Factura </th>
                            <th> Fecha - Hora </th>
                            <th> Monto Total </th>
                        </tr>
                        </thead>
                        <tbody>
<?php               for ($j = 0; $j < 40; $j++) : ?>
                        <tr style="border: 1px solid black;">
                            <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][0]; ?></td>
                            <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][1]; ?></td>
                            <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][2]; ?></td>
                            <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][3]; ?></td>
                            <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][4]; ?></td>
                        </tr>
    <?php           endfor; ?>
                        </tbody>
                    </table>
                    <div class="saltopagina"></div>
                    <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                        <thead>
                        <tr>
                            <th> Nro </th>
                            <th> Nro. Venta </th>
                            <th> Nro. Factura </th>
                            <th> Fecha - Hora </th>
                            <th> Monto Total </th>
                        </tr>
                        </thead>
                        <tbody>
<?php               for ($j = 40; $j < count($matriz); $j++) :                  ?>
    <?php               $nro_registro++;                            ?>
    <?php               if ($nro_registro == 60) :                            ?>
        <?php               $nro_registro = 0;                            ?>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][0]; ?></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][1]; ?></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][2]; ?></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][3]; ?></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][4]; ?></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="saltopagina"></div>
                            <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                                <thead>
                                <tr>
                                    <th> Nro </th>
                                    <th> Nro. Venta </th>
                                    <th> Nro. Factura </th>
                                    <th> Fecha - Hora </th>
                                    <th> Monto Total </th>
                                </tr>
                                </thead>
                                <tbody>
<?php                   else:                            ?>
                            <tr style="border: 1px solid black;">
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][0]; ?></td>
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][1]; ?></td>
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][2]; ?></td>
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][3]; ?></td>
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $matriz[$j][4]; ?></td>
                            </tr>
<?php                   endif;                            ?>
<?php               endfor; ?>
                        <tr style="border: 1px solid black;">
                            <td style="border-right: 1px solid black; padding-right: 1%" class="text-right"></td>
                            <td style="border-right: 1px solid black; padding-right: 1%" class="text-right"></td>
                            <td style="border-right: 1px solid black; padding-right: 1%" class="text-right"></td>
                            <td style="border-right: 1px solid black; padding-right: 1%" class="text-right"><strong>Monto total:</strong></td>
                            <td style="border-right: 1px solid black; padding-right: 1%" class="text-right"><strong><?= $monto_total ?></strong></td>
                        </tr>
                    </tbody>
                </table>
<?php           endif;  ?>
    </div>
