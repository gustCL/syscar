<?php

    require_once '../datos/Database.php';
    session_start();

    #region FUNCIONES

    /*
     * Obtener si el usuario con la sesión actual ha generado transacciones (ventas).
     * Si lo hizo, debe llenar un formulario de entrega de dinero.
     */
    function tieneTransaccion($id_usuario, $id_sucursal, $id_sesion) {
        $query = "SELECT  v.idVenta, u.nombre, t.fecha, t.hora, v.montoTotal, vs.nroVenta
                  FROM Transaccion t, Ventas v,VentaSucursal vs, InicioSesion i, Sucursal s, Usuario u
                  WHERE t.idInicioSesion=i.idInicio AND
                        t.idTransaccion=v.idTransaccion AND
                        v.idVenta=vs.idVenta AND
                        vs.idSucursal=s.idSucursal AND
                        i.sucursal=s.idSucursal AND
                        i.usuario=u.idUsuario AND
                        i.usuario=:id_usuario AND
                        i.sucursal=:id_sucursal AND
                        i.idInicio=:id_sesion AND
                        t.idTransaccion NOT IN (SELECT idTransaccion FROM transaccion WHERE tipoTrans=3)";
        $comando = Database::getInstance()->getDb()->prepare($query);
        $comando->bindParam(':id_usuario', $id_usuario);
        $comando->bindParam(':id_sucursal', $id_sucursal);
        $comando->bindParam(':id_sesion', $id_sesion);
        $comando->execute();

        $rows = $comando->rowCount();  // cantidad de filas retornadas por la consulta
        if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Cambiar el estado del usuario a 0
     */
    function cambiarEstado($id_usuario) {
        $query = "UPDATE Usuario SET activo = 0 WHERE idUsuario = :id_usuario";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_usuario', $id_usuario);
        $cmd->execute();
    }

    #endregion

    if (!isset($_SESSION['logueado']) || ($_SESSION['logueado']) != 'SI') {
        header('Location: ../login/');
        exit();
    } else {

        // Obtenemos las variables de sesion
        $id_usuario = $_SESSION['userMaster']['idUsuario'];
        $id_sucursal = $_SESSION['userMaster']['idSucursal'];
        $id_sesion = $_SESSION['idSesion'];
        date_default_timezone_set('America/La_Paz');
        $fecha = date("d-m-Y");
        $hora = date("H:i:s");

        try {
            if (tieneTransaccion($id_usuario, $id_sucursal, $id_sesion)) {
                header('Location: cierrecaja.php');
                exit();
            } else {

                //Insertando en Tabla "cierresesion"
                $total = 0;
                $total_sus = 0;
                $query_cierresesion = "INSERT INTO CierreSesion() VALUES (0, :fecha, :hora, :id_usuario, :total, :total_sus)";
                $cmd_cierresesion = Database::getInstance()->getDb()->prepare($query_cierresesion);
                $cmd_cierresesion->bindParam(':fecha', $fecha);
                $cmd_cierresesion->bindParam(':hora', $hora);
                $cmd_cierresesion->bindParam(':id_usuario', $id_usuario);
                $cmd_cierresesion->bindParam(':total', $total);
                $cmd_cierresesion->bindParam(':total_sus', $total_sus);
                $cmd_cierresesion->execute();

                // Cambiar el estado del usuario a 0 (inactivo)
                cambiarEstado($id_usuario);

                // Destruir las variables de sesión.
                session_unset();
                session_destroy();

                header('Location: ../login/');
                exit();
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
    }

?>