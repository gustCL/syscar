<script src="jcierresesion.js"></script>
<?php
        require_once ("../datos/Database.php");

        session_start();
        $idUsuarioM = $_SESSION['userMaster'];
        $usuario = $idUsuarioM['idUsuario'];

        // Billetes en Bolivianos
        $idocientos = $_POST['idocientos'];
        $ldocientos = $_POST['ldocientos'];
        $icien = $_POST['icien'];
        $lcien = $_POST['lcien'];
        $icincuenta = $_POST['icincuenta'];
        $lcincuenta = $_POST['lcincuenta'];
        $iveinte = $_POST['iveinte'];
        $lveinte = $_POST['lveinte'];
        $idiez = $_POST['idiez'];
        $ldiez = $_POST['ldiez'];
        $icinco = $_POST['icinco'];
        $lcinco = $_POST['lcinco'];
        $idos = $_POST['idos'];
        $ldos = $_POST['ldos'];
        $iuno = $_POST['iuno'];
        $luno = $_POST['luno'];

        // Moneda en Bolivianos
        $icincuenta_ctvs = $_POST['icincuenta-ctvs'];
        $lcincuenta_ctvs = $_POST['lcincuenta-ctvs'];
        $iveinte_ctvs = $_POST['iveinte-ctvs'];
        $lveinte_ctvs = $_POST['lveinte-ctvs'];
        $idiez_ctvs = $_POST['idiez-ctvs'];
        $ldiez_ctvs = $_POST['ldiez-ctvs'];

        // Billetes en Dólares
        $icien_sus = $_POST['icien-sus'];
        $lcien_sus = $_POST['lcien-sus'];
        $icincuenta_sus = $_POST['icincuenta-sus'];
        $lcincuenta_sus = $_POST['lcincuenta-sus'];
        $iveinte_sus = $_POST['iveinte-sus'];
        $lveinte_sus = $_POST['lveinte-sus'];
        $idiez_sus = $_POST['idiez-sus'];
        $ldiez_sus = $_POST['ldiez-sus'];
        $icinco_sus = $_POST['icinco-sus'];
        $lcinco_sus = $_POST['lcinco-sus'];
        $idos_sus = $_POST['idos-sus'];
        $ldos_sus = $_POST['ldos-sus'];
        $iuno_sus = $_POST['iuno-sus'];
        $luno_sus = $_POST['luno-sus'];

        // Monto total en Bolivianos
        $total = $_POST['ltotal'];
        // Monto total en Dólares
        $total_sus = $_POST['ltotal-sus'];

        //Insertando en Tabla "cierresesion"
        $fecha = date("Y-m-d");
        date_default_timezone_set('America/La_Paz');
        $time = time();
        $hora = date("H:i:s", $time);

        $consulta = "INSERT INTO CierreSesion(fecha,hora,idUsuario,montoTotalCajaBs,montoTotalCajaSus) "
                   ."VALUES ('$fecha','$hora','$usuario','$total','$total_sus')";
        $comandoP = Database::getInstance()->getDb()->prepare($consulta);

        //realizo la consulta para Obtener el ultimo id insertado
        $consulta_ult_id = "SELECT *
                            FROM CierreSesion
                            WHERE fecha='$fecha' and hora='$hora' and idUsuario=$usuario";
        $comandoID = Database::getInstance()->getDb()->prepare($consulta_ult_id);
        try {
            $comandoP->execute();

            $comandoID->execute(); //ultima tupla insertada con su id
            $resul = $comandoID->fetch();
            //echo $resul['idCierre'];
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }

        $idCierre = $resul['idCierre'];

        //----------------------------Insertando a la tabla EntregaCaja------------------------------------------------
        //200 Bolivianos
        $consultaBillete = "SELECT *
                            FROM billetes b, tipomoneda tm
                            WHERE b.tipoMoneda=tm.idMoneda and
                                  b.montoCambio='200.00' and tm.nombreMoneda='Bolivianos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "INSERT INTO EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)
                          VALUES      ($billete[idCambio],"."$idCierre, $idocientos, $ldocientos)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //100 Bs
        $consultaBillete = "SELECT *
                            FROM billetes b, tipomoneda tm
                            WHERE b.tipoMoneda=tm.idMoneda and
                                  b.montoCambio='100.00' and tm.nombreMoneda='Bolivianos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "INSERT INTO EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                            ."$idCierre,$icien,$lcien)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //50 Bs
        $consultaBillete = "SELECT * FROM billetes b, tipomoneda tm
                            WHERE b.tipoMoneda=tm.idMoneda and
                                  b.montoCambio='50.00' and tm.nombreMoneda='Bolivianos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$icincuenta,$lcincuenta)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //20 Bs
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='20.00' and tm.nombreMoneda='Bolivianos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$iveinte,$lveinte)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //10 Bs
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='10.00' and tm.nombreMoneda='Bolivianos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$idiez,$ldiez)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //5 Bs
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='5.00' and tm.nombreMoneda='Bolivianos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$icinco,$lcinco)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //2 Bs
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='2.00' and tm.nombreMoneda='Bolivianos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$idos,$ldos)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //1 Bs
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='1.00' and tm.nombreMoneda='Bolivianos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$iuno,$luno)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //0.50 Ctvs
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='0.50' and tm.nombreMoneda='Centavos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$icincuenta_ctvs,$lcincuenta_ctvs)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //0.20 Ctvs
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='0.20' and tm.nombreMoneda='Centavos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$iveinte_ctvs,$lveinte_ctvs)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //0.10 Ctvs
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='0.10' and tm.nombreMoneda='Centavos'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$idiez_ctvs,$ldiez_ctvs)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //100 Sus
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='100' and tm.nombreMoneda='Dolares'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$icien_sus,$lcien_sus)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //50 Sus
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='50' and tm.nombreMoneda='Dolares'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$icincuenta_sus,$lcincuenta_sus)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //20 Sus
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='20' and tm.nombreMoneda='Dolares'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$iveinte_sus,$lveinte_sus)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //10 Sus
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='10' and tm.nombreMoneda='Dolares'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$idiez_sus,$ldiez_sus)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //5 Sus
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='5' and tm.nombreMoneda='Dolares'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$icinco_sus,$lcinco_sus)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //2 Sus
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='2' and tm.nombreMoneda='Dolares'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$idos_sus,$ldos_sus)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        //1 Sus
        $consultaBillete = "select * from billetes b,tipomoneda tm where b.tipoMoneda=tm.idMoneda and
          b.montoCambio='1' and tm.nombreMoneda='Dolares'";
        $comando = Database::getInstance()->getDb()->prepare($consultaBillete);
        $comando->execute();
        $billete = $comando->fetch();
        $consultInsert = "insert into EntregaCaja(idBillete,idCierre,Cantidad,totalCambio)values($billete[idCambio],"
                . "$idCierre,$iuno_sus,$luno_sus)";
        $comando = Database::getInstance()->getDb()->prepare($consultInsert);
        $comando->execute();

        // Cambiar el estado del usuario a inactivo
        $consulta = "UPDATE Usuario set activo = 0 WHERE idUsuario=".$usuario;
        $comando = Database::getInstance()->getDb()->prepare($consulta);
        $comando->execute();

        // Destruir las variables de sesión.
        session_unset();
        session_destroy();

        header('Location: ../inicio/index.php');


?>

