<?php
    include_once 'head.php';
    require_once '../datos/Database.php';

    $id_usuario = $_SESSION['userMaster']['idUsuario'];
    $id_sesion = $_SESSION['idSesion'];
    $id_sucursal = $_SESSION['userMaster']['idSucursal'];
    $username = get_username($id_usuario);
    
    // Seleccionar el nombre de usuario
    function get_username($id_usuario) {
        $query = "SELECT nombre
                  FROM usuario
                  WHERE idUsuario = :id_usuario";
        $comand_name = Database::getInstance()->getDb()->prepare($query);
        $comand_name->bindParam(':id_usuario', $id_usuario);
        $comand_name->execute();
        $row_name = $comand_name->fetch();
        $nombre = $row_name['nombre'];
    }

    // Obtiene el número de factura de emitido por una venta
    // Si a la venta NO se le asignó una factura, retorna "(sin factura)"
    function get_nro_factura_from_venta($id_venta) {
        $query = "SELECT f.nroFactura
                  FROM   ventas v, factura f
                  WHERE  v.idVenta = f.idVenta AND v.idVenta = :id_venta";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_venta', $id_venta);
        $cmd->execute();
        $row_count = $cmd->rowCount();
        $result = $cmd->fetch();
        if ($row_count > 0) {
            return $result['nroFactura'];
        } else {
            return '(sin factura)';
        }
    }
?>

<div class="page-content">
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>CIERRE DE CAJA</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <!-- /Page Header -->

    <div class="page-body">
        <div class="widget-body">
            <center><h2><strong>VENTAS REALIZADAS</strong></h2></center>
            <!-- TABLA DE Resultados-->
            <div class="table-responsive" id="agrega-registros">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nro.</th>
                            <th>Nro. Venta</th>
                            <th>Nro. de Factura</th>
                            <th>Usuario</th>
                            <th>Fecha - Hora</th>
                            <th>Monto de Venta</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php

                    // Obtener las ventas realizadas durante la sesión activa del usuario
                    $consulta = "SELECT  v.idVenta, u.nombre, t.fecha, t.hora, v.montoTotal, vs.nroVenta
                                FROM Transaccion t, Ventas v,VentaSucursal vs, InicioSesion i, Sucursal s, Usuario u
                                WHERE
                                t.idInicioSesion=i.idInicio and
                                t.idTransaccion=v.idTransaccion and
                                v.idVenta=vs.idVenta and
                                vs.idSucursal=s.idSucursal and
                                i.sucursal=s.idSucursal and
                                i.usuario=u.idUsuario and
                                i.usuario=:id_usuario and
                                i.sucursal=:id_sucursal and
                                i.idInicio=:id_sesion and
                                t.idTransaccion not in (select idTransaccion from transaccion where tipoTrans=3)";

                    $comando = Database::getInstance()->getDb()->prepare($consulta);
                    $comando->bindParam(':id_usuario', $id_usuario);
                    $comando->bindParam(':id_sesion', $id_sesion);
                    $comando->bindParam(':id_sucursal', $id_sucursal);
                    $comando->execute();
                    $nro = 0;
                    $nro_venta = 0;
                    $nro_factura = "--";
                    $fecha_hora = "";
                    $monto_total = 0;

                    // cantidad de filas retornadas por la consulta
                    $rows_returned = $comando->rowCount();                  ?>
                    <?php   if ($rows_returned > 0) :                               ?>
                        <?php       $nro = 0;
                        $nro_venta = 0;
                        $fecha_hora = "--";                                 ?>
                        <?php       while ($row = $comando->fetch()) :                  ?>
                            <?php           $nro = $nro + 1;
                            $nro_venta = $row['nroVenta'];
                            $nombre = $row['nombre'];
                            $fecha_hora = $row['fecha']." - ".$row['hora'];
                            $monto_de_venta = $row['montoTotal'];
                            $monto_total = $monto_de_venta + $monto_total;   ?>
                            <tr>
                                <td><?= $nro; ?></td>
                                <td><?= $nro_venta; ?></td>
                                <td><?= get_nro_factura_from_venta($row['idVenta']); ?></td>
                                <td><?= $nombre; ?></td>
                                <td><?= $fecha_hora; ?></td>
                                <td><?= $monto_de_venta; ?></td>
                            </tr>

                        <?php    endwhile; ?>
                    <?php    endif; ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>MONTO TOTAL:</strong></td>
                            <td><strong><input id="monto-total" type="number" value="<?= $monto_total; ?>" readonly></strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <br>

        <div class="widget-body">
            <div class="table-responsive" id="agrega-registros-caja">
                <center><h2><strong>ENTREGA CAJA</strong></h2></center>
                <div id="div-tipo-cambio">
                    <?php
                        // Obtener el tipo de cambio del dolar
                        $tipo_cambio = $db->TipoCambio('estado = ?', '1')->fetch();
                    ?>
                    <div class="form-inline">
                        <input style="font-size: 25px; border: none;" value="Tipo de cambio:" readonly</input>
                        <input id="tipo-cambio" style="font-size: 30px; border: none;" type="number"
                               value="<?= $tipo_cambio['tipoCambioCompra']; ?>" readonly>
                    </div>
                </div>
                <br>
                <form id="form-entrega-caja" role="form" method="post" action="javascript: actualizar_e_imprimir();">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Montos Bs</th>
                                <th>Cantidad</th>
                                <th>Total</th>
                                <th>Montos Sus</th>
                                <th>Cantidad</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>200 Bs.</td>
                            <td><input class="form-control" id="idocientos" name="idocientos" type="number" value="0" min="0"></td>
                            <td><input type="text" class="form-control" id="ldocientos" name="ldocientos" value="0" readonly></td>

                            <td>100 Sus.</td>
                            <td><input class="form-control" id="icien-sus" name="icien-sus" type="number" value="0" min="0"></td>
                            <td><input type="text" class="form-control" id="lcien-sus" name="lcien-sus" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>100 Bs.</td>
                            <td><input class="form-control" id="icien" name="icien" type="number" value="0" min="0"></td>
                            <td><input type="text" class="form-control" id="lcien" name="lcien" value="0" readonly></td>

                            <td>50 Sus.</td>
                            <td><input class="form-control" id="icincuenta-sus" name="icincuenta-sus" type="number" value="0" min="0"></td>
                            <td><input type="text" class="form-control" id="lcincuenta-sus" name="lcincuenta-sus" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>50 Bs.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="icincuenta" name="icincuenta"></td>
                            <td><input type="text" class="form-control" id="lcincuenta" name="lcincuenta" value="0" readonly></td>

                            <td>20 Sus.</td>
                            <td><input class="form-control" id="iveinte-sus" name="iveinte-sus" type="number" value="0" min="0"></td>
                            <td><input type="text" class="form-control" id="lveinte-sus" name="lveinte-sus" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>20 Bs.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="iveinte" name="iveinte"></td>
                            <td><input type="text" class="form-control" id="lveinte" name="lveinte" value="0" readonly></td>

                            <td>10 Sus.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="idiez-sus" name="idiez-sus"></td>
                            <td><input type="text" class="form-control" id="ldiez-sus" name="ldiez-sus" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>10 Bs.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="idiez" name="idiez"></td>
                            <td><input type="text" class="form-control" id="ldiez" name="ldiez" value="0" readonly></td>

                            <td>5 Sus.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="icinco-sus" name="icinco-sus"></td>
                            <td><input type="text" class="form-control" id="lcinco-sus" name="lcinco-sus" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>5 Bs.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="icinco" name="icinco"></td>
                            <td><input type="text" class="form-control" id="lcinco" name="lcinco" value="0" readonly></td>

                            <td>2 Sus.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="idos-sus" name="idos-sus"></td>
                            <td><input type="text" class="form-control" id="ldos-sus" name="ldos-sus" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>2 Bs.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="idos" name="idos"></td>
                            <td><input type="text" class="form-control" id="ldos" name="ldos" value="0" readonly></td>

                            <td>1 Sus.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="iuno-sus" name="iuno-sus"></td>
                            <td><input type="text" class="form-control" id="luno-sus" name="luno-sus" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>1 Bs.</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="iuno" name="iuno"></td>
                            <td><input type="text" class="form-control" id="luno" name="luno" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>50 ctvs</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="icincuenta-ctvs" name="icincuenta-ctvs"></td>
                            <td><input type="text" class="form-control" id="lcincuenta-ctvs" name="lcincuenta-ctvs" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>20 ctvs</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="iveinte-ctvs" name="iveinte-ctvs"></td>
                            <td><input type="text" class="form-control" id="lveinte-ctvs" name="lveinte-ctvs" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td>10 ctvs</td>
                            <td><input type="number" value="0" min="0" class="form-control" id="idiez-ctvs" name="idiez-ctvs"></td>
                            <td><input type="text" class="form-control" id="ldiez-ctvs" name="ldiez-ctvs" value="0" readonly></td>
                        </tr>
                        <tr>
                            <td class="hidden-xs"></td>
                            <td class="hidden-xs"><label><strong>Total Bs. :</strong></label></td>
                            <td class="hidden-xs">
                                <input type="text" class="form-control" id="ltotal" name="ltotal"
                                       placeholder="Presione para ver Total" readonly
                                       style="background-color: #dca7a7">
                            </td>
                            <td class="hidden-xs"></td>
                            <td class="hidden-xs">
                                <label><strong>Total $us. :</strong></label>
                            </td>
                            <td class="hidden-xs" dir="rtl">
                                <input type="text" class="form-control" id="ltotal-sus" name="ltotal-sus"
                                       placeholder="Presione para ver Total" readonly
                                       style="background-color: #dca7a7">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>

                    <center>
                        <div class="form-group">
                            <input id="btnImprimir" class="btn btn-primary"  type="submit" value="Imprimir">
                        </div>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
    include_once '../header_footer/footer.php';
?>