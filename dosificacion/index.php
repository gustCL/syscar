<?php
    require_once 'head.php';

    $id_sucursal = $_SESSION['userMaster']['idSucursal'];

    function get_nombre_sucursal($id) {
        $query = "SELECT nombreSucur FROM Sucursal WHERE idSucursal = :id_sucursal";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_sucursal', $id);
        $cmd->execute();
        $result = $cmd->fetch();
        return $result['nombreSucur'];
    }

    function get_nombre_terminal($id) {
        $query = "SELECT nombre FROM Terminal WHERE idTerminal = :id_terminal";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_terminal', $id);
        $cmd->execute();
        $result = $cmd->fetch();
        return $result['nombre'];
    }

    function get_descripcion_estado($estado) {
        if ($estado == 1) {
            return 'Activo';
        } else {
            return 'Caducado';
        }
    }
?>
<div class="page-content">
    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>DOSIFICACIÓN</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">

            <div class="widget-body ">
                <h2><strong> Registrar Dosificación </strong></h2>
                <form method="post" action="javascript: registrarDosificacion();" id="formularioDosi"
                      class="form-horizontal">
                    <br>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> NIT de la Empresa </label>
                        <div class="col-lg-3">
                            <span class="input-icon">
                                <?php
                                    $empresa = $db->Empresa('idEmpresa = ?','1')->fetch();
                                ?>
                                <input type="text" id="nit" name="nit" value="<?= $empresa['nit']; ?>"
                                                           class="form-control" readonly>
                                <?php //endforeach; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group" hidden>
                        <label class="col-lg-4 control-label"> Sucursal </label>
                        <div class="col-lg-3">
                            <span class="input-icon">
                                <select class="form-control" id="sucursal" name="sucursal">
                                    <?php foreach ($db->Sucursal() as $sucursal)   : ?>
                                        <option value="<?= $sucursal['idSucursal']; ?>"><?= $sucursal['nombreSucur']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </span>
                            <script>
                                $("#sucursal").change(function () {
                                    var id = $(this).val();
                                    var dataString = 'id=' + id;
                                    $('#nroFac').val("");
                                    $.ajax({
                                        type: "POST",
                                        url: "obtener_terminal.php",
                                        data: dataString,
                                        success: function (html) {
                                            $("#terminal").html(html);
                                        }
                                    });
                                });
                            </script>
                        </div>
                    </div>
                    <div class="form-group" hidden>
                        <label class="col-lg-4 control-label"> Terminal </label>
                        <div class="col-lg-3">
                            <span class="input-icon">
                                <select class="form-control" id="terminal" name="terminal">
                                    <option value="0" selected="selected">--Terminal--</option>
                                </select>
                            </span>
                            <script>
                                $("#terminal").change(function () {
                                    var id = $(this).val();
                                    var dataString = 'id=' + id;
                                    $.ajax({
                                        type: "POST",
                                        url: "factura_por_primera_vez.php",
                                        data: dataString,
                                        success: function (result) {
                                            if (result == 'true') {
                                                $("#nroFac").val(1);
                                            } else {
                                                $("#nroFac").val("PENDIENTE");
                                            }
                                        }
                                    });
                                });
                            </script>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Nro. Autorización </label>
                        <div class="col-lg-3">
                            <span class="input-icon">
                                <input class="form-control" type="number" min="0" id="nroAut"
                                       name="nroAut" placeholder="Nro. Autorización" required>
                            </span>
                        </div>
                    </div>
                    <div id="div-factura-inicial" class="form-group">
                        <label class="col-lg-4 control-label"> Nro. Factura Inicial </label>
                        <div class="col-lg-3">
                            <span class="input-icon">
                                <input class="form-control" id="nroFac" name="nroFac" type="text"
                                       placeholder="Nro. Factura inicial" required readonly>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Fecha Límite </label>
                        <div class="col-lg-3">
                            <span class="input-icon">
                                <?php
                                    ini_set("date.timezone", "America/La_Paz");
                                    $fecha = date("Y-m-d");
                                ?>
                                <input type="date" name="fechaLimite" id="fechaLimite" value="<?= $fecha; ?>" class="input-sm form-control input-s-sm inline" required/>
                                    <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Llave de dosificación </label>
                        <div class="col-lg-3">
                            <span class="input-icon">
                                <input type="text" class="form-control" id="llaveDosi"
                                       name="llaveDosi" placeholder="Llave de dosificación" required>
                            </span>
                        </div>
                    </div>
                    <div class="form-inline">
                        <center>
                            <input type="submit" class="btn btn-primary" value="Registrar"/>
                            <input type="button" value="Limpiar" class="btn btn-azure" onclick="$('#formularioDosi').trigger('reset');"/>
                            <input type="button" class="btn btn-info" value="Limpiar" onclick="$('[name='stock']').val(0);">
                        </center>
                    </div>
                </form>
            </div>
            <br>
            <div class="widget-body">
                <div class="table-responsive">
                    <h2><strong>Dosificaciones Activas</strong></h2>
                    <?php
                    // Obtener las dosificaciones vigentes
                    $query_dosificacion_vigente = "SELECT *
                                                   FROM Dosificacion
                                                   WHERE estado = 1
                                                   ORDER BY fechaLimite";
                    $cmd_dosificacion_vigente = Database::getInstance()->getDb()->prepare($query_dosificacion_vigente);
                    $cmd_dosificacion_vigente->execute();
                    $row_count_dv = $cmd_dosificacion_vigente->rowCount();
                    ?>
                    <table class="table table-striped">
                        <?php if ($row_count_dv > 0): ?>
                            <tr>
                                <th><strong>Nro.</strong></th>
                                <th><strong>Nro. Autorización</strong></th>
                                <th><strong>Estado</strong></th>
                                <th><strong>Fecha Límite</strong></th>
                                <th><strong>Dias Restantes</strong></th>
                            </tr>
                            <?php while ($result_dosificacion_vigente = $cmd_dosificacion_vigente->fetch()): ?>
                                <?php $fecha_limite = $result_dosificacion_vigente['fechaLimite'];
                                date_default_timezone_set('America/La_Paz');
                                $fecha_actual = date('d-m-Y');
                                $diferencia_fecha = strtotime($fecha_limite) - strtotime($fecha_actual);
                                $diferencia_dias = intval($diferencia_fecha / 60 / 60 / 24);
                                ?>
                                <tr>
                                    <td><?= $result_dosificacion_vigente['idDosifi']; ?></td>
                                    <td><?= $result_dosificacion_vigente['nroAutorizacion']; ?></td>
                                    <td><?= get_descripcion_estado($result_dosificacion_vigente['estado']); ?></td>
                                    <td><?= $result_dosificacion_vigente['fechaLimite']; ?></td>
                                    <td><?= $diferencia_dias; ?></td>
                                </tr>
                            <?php endwhile; ?>
                        <?php else: ?>

                            <div class="alert alert-info" role="alert">
                                <center>
                                    <h3>No se encontraron resultados.</h3>
                                </center>
                            </div>

                        <?php endif; ?>
                    </table>
                </div>
            </div>
            <br>

            <div class="widget-body">
                <div class="table-responsive">
                    <h2><strong> Dosificaciones Caducadas </strong></h2>
                    <?php
                    // Obtener las dosificaciones vigentes
                    $query_dosificacion_caducada = "SELECT *
                                                                       FROM Dosificacion
                                                                       WHERE estado = 2
                                                                       ORDER BY fechaLimite";
                    $cmd_dosificacion_caducada = Database::getInstance()->getDb()->prepare($query_dosificacion_caducada);
                    $cmd_dosificacion_caducada->execute();
                    $row_count = $cmd_dosificacion_caducada->rowCount();
                    ?>
                    <table class="table table-striped">
                        <?php if ($row_count > 0): ?>
                            <tr>
                                <th><strong>Nro.</strong></th>
                                <th><strong>Nro. Autorización</strong></th>
                                <th><strong>Estado</strong></th>
                                <th><strong>Fecha Límite</strong></th>
                                <th><strong>Punto Asignado</strong></th>
                                <th><strong>Dias Restantes</strong></th>
                            </tr>
                            <?php while ($result_dosificacion_caducada = $cmd_dosificacion_caducada->fetch()): ?>
                                <?php $fecha_limite = $result_dosificacion_caducada['fechaLimite'];
                                date_default_timezone_set('America/La_Paz');
                                $fecha_actual = date('d-m-Y');
                                $diferencia_fecha = strtotime($fecha_limite) - strtotime($fecha_actual);
                                $diferencia_dias = intval($diferencia_fecha / 60 / 60 / 24);
                                ?>
                                <tr>
                                    <td><?= $result_dosificacion_caducada['idDosifi']; ?></td>
                                    <td><?= $result_dosificacion_caducada['nroAutorizacion']; ?></td>
                                    <td><?= get_descripcion_estado($result_dosificacion_caducada['estado']); ?></td>
                                    <td><?= $result_dosificacion_caducada['fechaLimite']; ?></td>
                                    <td><?= $result_dosificacion_caducada['idTerminal']; ?></td>
                                    <td><?= $diferencia_dias; ?></td>
                                </tr>
                            <?php endwhile; ?>
                        <?php else: ?>

                            <div class="alert alert-info" role="alert">
                                <center>
                                    <h3>No se encontraron resultados.</h3>
                                </center>
                            </div>

                        <?php endif; ?>
                    </table>
                </div>
            </div>
            <br>

        </div>
    </div>
</div>

<?php require_once '../header_footer/footer.php'; ?>