<?php

    require_once '../datos/Database.php';

        $id_sucursal = $_POST['id'];
        $query = "SELECT idTerminal, nombre FROM Terminal WHERE idSucursal = :id_sucursal";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_sucursal', $id_sucursal);
        $cmd->execute();
        $count = $cmd->rowCount();
        if ($count >= 1) {
            $val = 0;
            echo '<option selected="selected" value="'.$val.'">--Terminal--</option>';
            while($row = $cmd->fetch())
            {
                $id_terminal = $row['idTerminal'];
                $nombre = $row['nombre'];
                echo '<option value="'.$id_terminal.'">'.$nombre.'</option>';
            }
        } else {
            $id = 0;
            $name = "--Terminal--";
            echo '<option value="'.$id.'">'.$name.'</option>';
        }

?>