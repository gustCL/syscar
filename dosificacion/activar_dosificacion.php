<?php
    require_once 'head.php';
    require_once '../datos/Database.php';

    $id_sucursal = $_SESSION['userMaster']['idSucursal'];

    /*
    // Obtener las terminales registradas que terminaron su fecha límite para facturar
    $query_terminal = "select idTerminal, nombre
                       from terminal
                       where idTerminal NOT IN(
                        	              select idTerminal
                        	              from dosificacion
                        	              where estado = 1
						                )";
    $cmd_terminal = Database::getInstance()->getDb()->prepare($query_terminal);
    $cmd_terminal->execute();

    $array_terminal = array();
    while ($result_terminal = $cmd_terminal->fetch()) {
        $array_terminal[$result_terminal['idTerminal']] = $result_terminal['nombre'];
    }
    */

    // Funciones
    function get_nombre_terminal($id) {
        $query = "SELECT nombre FROM Terminal WHERE idTerminal = :id_terminal";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_terminal', $id);
        $cmd->execute();
        $result = $cmd->fetch();
        return $result['nombre'];
    }

    function estado($valor) {
        if ($valor == 1) {
            return 'ACTIVO';
        } else {
            return 'INACTIVO';
        }
    }

?>

<div class="page-content">
    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong> ACTIVAR DOSIFICACIÓN</strong></h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="widget-body ">
                <h2><strong> Dosificaciones Inactivas </strong></h2>
                <div id="dosificaciones-inactivas">
                    <form id="formTablaD">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th><strong>Nro.</strong></th>
                                <th><strong>Nro. Autorización</strong></th>
                                <th><strong>Nro. Factura Inicial</strong></th>
                                <th><strong>Fecha Registro</strong></th>
                                <th><strong>Fecha Límite</strong></th>
                                <th><strong>Llave de Dosificación</strong></th>
                                <th><strong>Estado</strong></th>
                                <th><strong>Punto Asignado</strong></th>
                                <th><strong>Opción</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $query_select_dosificacion = "SELECT * FROM Dosificacion where estado = 0";
                            $cmd_select_dosificacion = Database::getInstance()->getDb()->prepare($query_select_dosificacion);
                            $cmd_select_dosificacion->execute();
                            $nro_dosificacion = 0;
                            ?>
                            <?php while ($row = $cmd_select_dosificacion->fetch()): ?>
                                <?php $nro_dosificacion++; ?>
                                <tr>
                                    <td><?= $nro_dosificacion ?></td>
                                    <td><?= $row['nroAutorizacion'] ?></td>
                                    <td><?= $row['nroFacturaInicial'] ?></td>
                                    <td><?= $row['fechaRegistro'] ?></td>
                                    <td><?= $row['fechaLimite'] ?></td>
                                    <td><?= $row['llaveDosificacion'] ?></td>
                                    <td><?= estado($row['estado']); ?></td>
                                    <td><?= get_nombre_terminal($row['idTerminal']); ?></td>
                                    <td>
                                        <a href="javascript: activarDosificacion('<?= $row['idDosifi'] ?>','<?= $row['idTerminal']; ?>');"
                                           class="btn btn-success">Activar Dosificación</a>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->
</div>

<?php require_once '../header_footer/footer.php'; ?>