<?php

    // Como CASI todos registran su dosificación días antes de que esta caduque
    // Yo no puedo saber el número de factura con el que iniciará la nueva dosificación.

    require_once '../datos/Database.php';
    $id_terminal = $_POST['id'];

    // Verificar si la terminal actualmente factura o facturó
    $query = "SELECT idTerminal
              FROM dosificacion
              WHERE (estado = 1 or estado = 2) AND idTerminal = :id_terminal";
    $cmd = Database::getInstance()->getDb()->prepare($query);
    $cmd->bindParam(':id_terminal', $id_terminal);
    $cmd->execute();
    $count = $cmd->rowCount();
    if ($count == 0)
    {
        echo 'true';
        exit();
    }
        else
    {
        echo 'false';
        exit();
    }