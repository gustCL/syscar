<?php
    require_once '../datos/Database.php';

/*function get_next_factura($id_terminal) {
        $query = "SELECT nroFactura
                      FROM Factura f, Dosificacion d, Terminal t
                      WHERE t.idTerminal = :id_terminal and d.idTerminal = t.idTerminal and f.idDosifi = d.idDosifi
                      ORDER BY id DESC
                      LIMIT 1";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_terminal', $id_terminal);
        $cmd->execute();
        $result = $cmd->fetch(['nroFactura']);
        return $result + 1;
    }*/

    session_start();
    $session_id_usuario = $_SESSION['userMaster']['idUsuario'];
    //$session_id_terminal = $_SESSION['terminal']['idTerminal'];
    $session_id_sesion = $_SESSION['idSesion'];

    // Variables de formulario
    //$id_terminal = $_POST['terminal'];
    $nro_autorizacion = $_POST['nroAut'];
    $nro_factura = $_POST['nroFac'];
    $fecha_limite = $_POST['fechaLimite'];
    $fecha_limite = date("d-m-Y", strtotime($fecha_limite));
    $llave_dosificacion = $_POST['llaveDosi'];
    $id_sucursal = $_POST['sucursal'];

    // Definir la zona horario
    ini_set("date.timezone", "America/La_Paz");
    $fecha_registro = date('d-m-Y');
    $hora_registro = date("H:i:s");

    if ($id_sucursal == 0) {
        echo 'error_1';
        exit();
    } else {
        if ($nro_factura == 'PENDIENTE') {
            // Aún no sabemos el nro de factura inicial
            $consulta = "INSERT INTO Dosificacion VALUES ('',
                                             :idSucursal, 
                                             :nroAutorizacion, 
                                             :nroFacturaInicial, 
                                             :fechaLimite,
                                             :llaveDosificacion,
                                             :idSesion,
                                             :fechaRegistro,
                                             :horaRegistro,
                                             1,
                                             '')";
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            $comando->bindParam(':idSucursal', $id_sucursal);
            $comando->bindParam(':nroAutorizacion', $nro_autorizacion);
            $comando->bindParam(':nroFacturaInicial', $nro_factura);
            $comando->bindParam(':fechaLimite', $fecha_limite);
            $comando->bindParam(':llaveDosificacion', $llave_dosificacion);
            $comando->bindParam(':idSesion', $session_id_sesion);
            $comando->bindParam(':fechaRegistro', $fecha_registro);
            $comando->bindParam(':horaRegistro', $hora_registro);
            //$comando->bindParam(':id_terminal', $id_terminal);
            $comando->execute();
            echo 'exito';
            exit();
        } else {
            // Obtener el siguiente número de factura, de la tabla factura
            $cmd_next_number_factura = Database::getInstance()->getDb()->prepare("SELECT * ");

            $consulta = "INSERT INTO Dosificacion VALUES ('',
                                             :id_sucursal,
                                             :nro_autorizacion,
                                             :nro_factura_inicial,
                                             :fecha_limite, 
                                             :llave,
                                             :id_sesion,
                                             :fecha_registro,
                                             :hora_registro,
                                             1,
                                             '')";
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            $comando->bindParam(':id_sucursal', $id_sucursal);
            $comando->bindParam(':nro_autorizacion', $nro_autorizacion);
            $comando->bindParam(':nro_factura_inicial', $nro_factura);
            $comando->bindParam(':fecha_limite', $fecha_limite);
            $comando->bindParam(':llave', $llave_dosificacion);
            $comando->bindParam(':id_sesion', $session_id_sesion);
            $comando->bindParam(':fecha_registro', $fecha_registro);
            $comando->bindParam(':hora_registro', $hora_registro);
            $comando->execute();
            echo 'exito';
            exit();
        }
    }

?>