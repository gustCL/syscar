$(document).ready(function() {
	obtenerClientes();
});

function activarDosificacion(id_dosificacion, id_terminal) {
	//var select_id = "select-" + id_dosificacion;
	//var terminal = document.getElementById(select_id);
	//var id_terminal = terminal.options[terminal.selectedIndex].value;

	$.ajax({
		type: 'POST',
		url: 'activar.php',
		data: { id_dosificacion: id_dosificacion,
				id_terminal: id_terminal
				},
		success: function(result) {
			if (result == 'error') {
				swal("Acción Denegada", "No puede activar otra dosificación en esta terminal por que actualmente ya cuenta con una dosificación activa.", "error");
			} else {
				swal("Acción Completada", "Dosificación activada satisfactoriamente.", "success");
				$("#dosificaciones-inactivas").html(result);
			}
		}
	});
}

function obtener_terminales() {
		//var id = $(this).val();
		//var idSucursal = 'id='+ id;
		var sucursal = document.getElementById('sucursal');
		var idSucursal = sucursal.options[sucursal.selectedIndex].value;

		$.ajax({
			type: "POST",
			url: "obtener_terminal.php",
			data: idSucursal,
			success: function(html)
			{
				$("#terminal").html(html);
			}
		});
}

function registrarDosificacion() {
		var frm = $("#formularioDosi").serialize();
		$.ajax({
			type: "POST",
			url: "registrar_dosificacion.php",
			data: frm,
			success: function (result) {
				alert(result);
				if (result == 'exito') {
					swal("Registrar dosificación", "Dosificación registrada satisfactoriamente.", "success");
				} else {
					swal("ERROR!", "Ocurrió un error, intente nuevamente", "error");
				}
			}
		})

}
