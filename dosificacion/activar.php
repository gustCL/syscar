<?php
    require_once '../datos/Database.php';

    $id_dosificacion = $_POST['id_dosificacion'];
    $id_terminal = $_POST['id_terminal'];

    // Obtener el numero de factura inicial
    function get_factura_inicial($id) {
        $query = "SELECT nroFacturaInicial FROM Dosificacion WHERE idDosifi = :id_dosificacion";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_dosificacion', $id);
        $cmd->execute();
        $result = $cmd->fetch();
        return $result['nroFacturaInicial'];
    }

    function get_last_factura_from_terminal($id_terminal) {
        $query = "SELECT nroFactura
                    FROM Factura f, Dosificacion d, Terminal t
                    WHERE f.idDosifi = d.idDosifi and d.idTerminal = t.idTerminal and t.idTerminal = :id_terminal
                    ORDER BY nroFactura DESC LIMIT 1";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_terminal', $id_terminal);
        $cmd->execute();
        $result = $cmd->fetch();
        return $result['nroFactura'];
    }

    function get_id_terminal_from_dosificacion($id_dosi) {
        $query = "select idTerminal from Dosificacion where idDosifi = :id_dosificacion";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_dosificacion', $id_dosi);
        $cmd->execute();
        $result = $cmd->fetch();
        return $result['idTerminal'];
    }

    function get_last_dosificacion_from_terminal($id_terminal) {
        $query = "SELECT d.idDosifi
                    FROM Dosificacion d, Terminal t
                    WHERE t.idTerminal = d.idTerminal and t.idTerminal = :id_terminal
                    ORDER BY d.idDosifi DESC
                    LIMIT 1";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_terminal', $id_terminal);
        $cmd->execute();
        $result = $cmd->fetch();
        return $result['idDosifi'];
    }

    function get_nombre_terminal($id) {
        $query = "SELECT nombre FROM Terminal WHERE idTerminal = :id_terminal";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_terminal', $id);
        $cmd->execute();
        $result = $cmd->fetch();
        return $result['nombre'];
    }

    function tiene_dosificacion_activa($id_terminal) {
        $query = "SELECT * FROM Dosificacion WHERE idTerminal = :id_terminal AND estado = 1";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_terminal', $id_terminal);
        $cmd->execute();
        $result = $cmd->rowCount();
        if ($result > 0) {
            return true;
        } else {
            return false;
        }
    }

    function estado($valor) {
        if ($valor == 1) {
            return 'ACTIVO';
        } else {
            return 'INACTIVO';
        }
    }

    // Comprobar si la terminal NO tiene actualmente una dosificación activa
    if (tiene_dosificacion_activa($id_terminal)) {
        echo 'error';
        exit();
    } else {
        $factura_inicial = get_factura_inicial($id_dosificacion);
        if ($factura_inicial == 'PENDIENTE') {

            // Obtener el último número facturado
            //$id_terminal = get_id_terminal_from_dosificacion($id_dosificacion);
            //$last_dosificacion = get_last_dosificacion_from_terminal($id_terminal);
            $ultima_factura = get_last_factura_from_terminal($id_terminal);
            $nueva_factura_inicial = $ultima_factura + 1;

            $query_update_dosificacion = "UPDATE Dosificacion
                                          SET estado = 1, nroFacturaInicial = :factura_inicial
                                          WHERE idDosifi = :id_dosificacion";
            $cmd_update_dosificacion = Database::getInstance()->getDb()->prepare($query_update_dosificacion);
            $cmd_update_dosificacion->bindParam(':id_dosificacion', $id_dosificacion);
            $cmd_update_dosificacion->bindParam(':factura_inicial', $nueva_factura_inicial);
            $cmd_update_dosificacion->execute();
        } else {
            // Simplemente, activar la dosificacion
            $query_update_dosificacion = "UPDATE Dosificacion SET estado = 1 WHERE idDosifi = :id_dosificacion";
            $cmd_update_dosificacion = Database::getInstance()->getDb()->prepare($query_update_dosificacion);
            $cmd_update_dosificacion->bindParam(':id_dosificacion', $id_dosificacion);
            $cmd_update_dosificacion->execute();
        }
    }

?>
<form id="formTablaD">
    <table class="table table-striped">
        <thead>
        <tr>
            <th><strong>Nro.</strong></th>
            <th><strong>Nro. Autorización</strong></th>
            <th><strong>Nro. Factura Inicial</strong></th>
            <th><strong>Fecha Registro</strong></th>
            <th><strong>Fecha Límite</strong></th>
            <th><strong>Llave de Dosificación</strong></th>
            <th><strong>Estado</strong></th>
            <th><strong>Punto Asignado</strong></th>
            <th><strong>Opción</strong></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $query_select_dosificacion = "SELECT * FROM Dosificacion where estado = 0";
        $cmd_select_dosificacion = Database::getInstance()->getDb()->prepare($query_select_dosificacion);
        $cmd_select_dosificacion->execute();
        $nro_dosificacion = 0;
        ?>
        <?php while ($row = $cmd_select_dosificacion->fetch()): ?>
            <?php $nro_dosificacion++; ?>
            <tr>
                <td><?= $nro_dosificacion ?></td>
                <td><?= $row['nroAutorizacion'] ?></td>
                <td><?= $row['nroFacturaInicial'] ?></td>
                <td><?= $row['fechaRegistro'] ?></td>
                <td><?= $row['fechaLimite'] ?></td>
                <td><?= $row['llaveDosificacion'] ?></td>
                <td><?= estado($row['estado']); ?></td>
                <td><?= get_nombre_terminal($row['idTerminal']); ?></td>
                <td>
                    <a href="javascript: activarDosificacion('<?= $row['idDosifi'] ?>','<?= $row['idTerminal']; ?>');"
                       class="btn btn-success">Activar Dosificación</a>
                </td>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
</form>
