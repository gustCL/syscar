<?php
session_start();
if ($_SESSION['logueado'] != 'SI') {
    header('Location: ../inicio');
}
   // Código para denegar el acceso por URL
    if (!array_search('../backup_Restore', $_SESSION['privilegio'])) {
        header('Location: ../inicio');
    }

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Farmacia SRL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../assets/img/icono.png" type="image/x-icon" media="all">

    <!--Basic Styles-->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet"   media="all"/>
    <link id="bootstrap-rtl-link" href="#" rel="stylesheet" />
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet"   media="all"/>
    <link href="../assets/css/weather-icons.min.css" rel="stylesheet"   media="all"/>

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link href="../assets/css/beyond.min.css" rel="stylesheet"   media="all" />
    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="../assets/js/skins.min.js"></script>
    

    
    
    
</head>
<?php                
    require_once '../header_footer/header.php';                  
?>

