<?php

require_once './conexion.php';
$usuario = 'root';
$password = '';
$db = 'db_farmacia';
$local = 'localhost';
$tables = '*';
$dir = 'd:backup';


/**
 * Instantiate Backup_Database and perform backup
 */
$backupDatabase = new Backup_Database($local, $usuario, $password, $db);
$status = $backupDatabase->backupTables($tables, $dir) ? 'OK' : 'KO';
echo "


Backup result: " . $status;

/**
 * The Backup_Database class
 */
class Backup_Database {

    /**
     * Host where database is located
     */
    var $host = '';

    /**
     * Username used to connect to database
     */
    var $username = '';

    /**
     * Password used to connect to database
     */
    var $passwd = '';

    /**
     * Database to backup
     */
    var $dbName = '';

    /**
     * Database charset
     */
    var $charset = '';

    /**
     * Constructor initializes database
     */
    function Backup_Database($host, $username, $passwd, $dbName, $charset = 'utf8') {
        $this->host = $host;
        $this->username = $username;
        $this->passwd = $passwd;
        $this->dbName = $dbName;
        $this->charset = $charset;

        $this->initializeDatabase();
    }

    protected function initializeDatabase() {
        $conn = mysql_connect($this->host, $this->username, $this->passwd);
        mysql_select_db($this->dbName, $conn);
        if (!mysql_set_charset($this->charset, $conn)) {
            mysql_query('SET NAMES' . $this->charset);
        }
    }

    public function backupTables($tables = '*', $outputDir = '.') {
        try {
            /**
             * Tables to export
             */
            if ($tables == '*') {
                $tables = array();
                $result = mysql_query('SHOW TABLES');
                while ($row = mysql_fetch_row($result)) {
                    $tables[] = $row[0];
                }
            } else {
                $tables = is_array($tables) ? $tables : explode(',', $tables);
            }

            $sql = 'CREATE DATABASE IF NOT EXISTS ' . $this->dbName . ";\n\n";
            $sql .= 'USE ' . $this->dbName . ";\n\n";

            /**
             * Iterate tables
             */
            foreach ($tables as $table) {
                echo "Backing up " . $table . " table";

                $result = mysql_query('SELECT * FROM ' . $table);
                $numFields = mysql_num_fields($result);

                $sql .= 'DROP TABLE IF EXISTS ' . $table . ';';
                $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
                $sql.= "\n\n" . $row2[1] . ";\n\n";

                for ($i = 0; $i < $numFields; $i++) {
                    while ($row = mysql_fetch_row($result)) {
                        $sql .= 'INSERT INTO ' . $table . ' VALUES(';
                        for ($j = 0; $j < $numFields; $j++) {
                            $row[$j] = addslashes($row[$j]);
                            $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                            if (isset($row[$j])) {
                                $sql .= '"' . $row[$j] . '"';
                            } else {
                                $sql.= '""';
                            }

                            if ($j < ($numFields - 1)) {
                                $sql .= ',';
                            }
                        }

                        $sql.= ");\n";
                    }
                }

                $sql.="\n\n\n";

                echo " OK" . "
";
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
            return false;
        }

        return $this->saveFile($sql, $outputDir);
    }

    /**
     * Save SQL to file
     * @param string $sql
     */
    protected function saveFile(&$sql, $outputDir = '.') {
        if (!$sql)
            return false;

        try {
            $handle = fopen($outputDir . '/'. date("Y-m-d-H-i-s").'.sql','w+');
            fwrite($handle, $sql);
            fclose($handle);

            $fecha = date("d-m-Y-H_i_s");
            $fecha2 = date("Y-m-d-H-i-s");

            $cmd_ingresar = "cd C:\Program Files\MySQL\MySQL Server 5.6.21\bin && mysqldump --user=root --password=  > d:/backup/" . $fecha2 . ".sql";
            $msj = exec($cmd_ingresar);
            mysql_query("insert into Restore(nombre,direccion,fecha) values('" . $fecha2 . "','d:/backup/','" . $fecha2 . "');");
        } catch (Exception $e) {
            var_dump($e->getMessage());
            return false;
        }

        return true;
    }

}

header("location: backup_Restore.php");
?>


