<?php
require_once '../datos/Database.php';
$producto_id = $_POST['id-stock-mayor'];
$stock = $_POST['inp-stock-mayor'];
//RECUPERAMOS LOS VALORES DEL PRODUCTO
INI_SET("date.timezone", "America/La_Paz");
//insertamos un nuevo Lote
$fecha_actual = date('Y-m-d');
$hora_actual = date('H:i:s');

$cmd_lote_insert = Database::getInstance()->getDb()->prepare("INSERT INTO stock_mayor VALUES(0,
                                                                                      :stock,
                                                                                      :fecha_registro,
                                                                                      :hora_registro,
                                                                                      1,
                                                                                      :cantidad_ingresada,
                                                                                      1,
                                                                                      :producto_mayor_id)");
$cmd_lote_insert->bindParam(':stock', $stock);
$cmd_lote_insert->bindParam(':fecha_registro', $fecha_actual);
$cmd_lote_insert->bindParam(':hora_registro', $hora_actual);
$cmd_lote_insert->bindParam(':cantidad_ingresada',$stock);
$cmd_lote_insert->bindParam(':producto_mayor_id',$producto_id);
$cmd_lote_insert->execute();

exit();

?>