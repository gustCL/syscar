$(document).ready(function() {
    loadCorteMayor();
    loadCorteMenor();
    $("#bus-producto").on('keyup',function() {
        var incidencia= $("#bus-producto").val();
        $('#div-producto').load('obtener_productos.php?val='+incidencia);
    })
    
    
    $('#new-corte-mayor').on('click', function () {
        $('#frm-new-corte-mayor')[0].reset();
        $('#modal-new-corte-mayor').modal({
            show: true,
            backdrop: 'static'
        });
    });

    $("#btn-prueba").on('click',function () {
        $('#frm-prueba')[0].reset();
        $('#modal-prueba').modal({
            show: true,
            backdrop: 'static'
        });
    })
    $("#btn-corte-menor").on('click',function () {
        $('#frm-corte-menor')[0].reset();
        $('#modal-corte-menor').modal({
            show: true,
            backdrop: 'static'
        });
    })

    $("#bus-producto").on('keyup',function () {
        var incidencia= $("#bus-producto").val();
        $('#div-producto').load('list_corte_mayor.php?val='+incidencia);
    })
});

$(function() {
    $("#codigo").keyup(function(){
        if ($(this).val().length > 0) {
            $('#disponible').show();
            existeCodigoProducto($(this).val());
        } else {
            $('#disponible').hide();
        }
    });
});

function loadCorteMayor() {
    $('#div-producto').load('list_corte_mayor.php');
}
function deleteCorteMayor(id,nombre){
    swal({
            title: "ELIMINAR CORTE MAYOR?",
            text: "Está a punto de eliminar "+nombre+". ¿Desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: 'delete_corte_mayor.php',
                    data: {id: id},
                    success: function (response) {
                        swal("CORTE MAYOR ELIMINADO!", "CORTE MAYOR ELIMINADO CORRECTAMENTE", "success");
                        loadCorteMayor();
                    }
                });
            }
        }
    );
}
//eliminamos el corte menor
function deleteCorteMenor(id,nombre){
    swal({
            title: "ELIMINAR CORTE MENOR?",
            text: "ESTA A PUNTO DE ELIMIAR "+nombre+". ¿DESEA CONTINUAR..?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: 'delete_corte_menor.php',
                    data: {id: id},
                    success: function (response) {
                        swal("CORTE MENOR ELIMINADO!", "CORTE MENOR ELIMINADO CORRECTAMENTE", "success");
                        loadCorteMenor();
                    }
                });
            }
        }
    );
}
//
function saveCorteMayor() {
    var frm = $("#frm-new-corte-mayor").serialize();
    $.ajax({
        url: 'register_producto_mayor.php',
        data: frm,
        type: 'POST',
        success: function (valor) {
            loadCorteMayor();
            $("#btn-calcel-cortemayor").click();
            swal("DATOS CORRECTO", "Corte Mayor Registrado Correcctamente", "success");
        }
    });
}
// Busca productos en base a parámetros
function buscarProducto(val) {
    val = val.replace(/ /g, "%20");
    $('#div-producto').load('obtener_productos.php?val='+val);

}
//
///
function loadCorteMenor(){
    var mn= $("#ocultoId").val();
    $("#div-corte-menor").load('list_producto_menor.php?id='+mn);
}
//para argar el stock disponible en corte menor
function loadStockDisponible(){
    var id = $("#ocultoId").val();
    $.ajax({
        url:'stock_corte_mayor_disponoble.php',
        type: 'POST',
        data: {id: id},
        success: function (response){
             var dato = eval(response);
            dato= "&nbsp;&nbsp;"+dato;
            $("#lbl-stockDisponible").html(dato);
        }
    }) 
}
//REGISTRAMOS LA TRANSFERENCIA
function transferencia(codMayor, codMenor) {
    var can = $("#inp"+codMenor).val();
    if($("#inp"+codMenor).val()=='' || $("#inp"+codMenor).val()=='0' ){
            $("#inp"+codMenor).focus();
            sweetAlert("DATOS INCORRECTOS", "Señor Usuario Ingrese Una Cantidad Mayor a Cero", "error");
            $("#inp"+codMenor).focus();
            return;
    }else{
        $.ajax({
            url: 'transferencia_stock.php',
            data: 'codMayor='+codMayor+'&codMenor='+codMenor+'&cant='+can,
            type: 'POST',
            success: function (response) {
                var datos = eval(response);
                loadCorteMenor();
                if(datos[0]=='false'){
                    sweetAlert("MENSAJE", "SEÑOR USUARIO, NO EXISTE STOCK SUFICIENTE PARA REALIZAR LA TRANSACCION", "error");
                }else{
                    loadStockDisponible();
                    sweetAlert("DATOS CORRECTOS", "SEÑOR USUARIO, LA TRANSACCION SE REALIZO CORRECTAMENTE", "success");
                }
            }
        });
    }

}


//REGISTRAR EL CORTE MENOR
///
function registrarCorteMenor(){
    var frm = $("#frm-corte-menor").serialize();
    $.ajax({
        url:'register_producto_mayor_menor.php',
        data: frm ,
        type: 'POST',
        success: function (response) {
            loadCorteMenor();
            $("#cancel-menor").click();
            sweetAlert("DATOS CORRECTOS", "SEÑOR USUARIO, CORTE MENOR REGISTRADO CORRECTAMENTE... !", "success");
        }
    })
    
}
///
function addStockCorteMayor(id,name) {
    var titulo = "AGREGAR STOCK A: ";
    titulo = titulo.bold();
    $('#frm-add-stock-mayor')[0].reset();
    $("#id-stock-mayor").val(id);
    $("#lbl-title").html(titulo+name.toUpperCase().bold());
    $('#modal-add-stock-mayor').modal({
        show: true,
        backdrop: 'static'
    }); 
}
//
function registrarStockMayor() {
    var frm = $("#frm-add-stock-mayor").serialize();
    $.ajax({
        url: 'addStock.php',
        data: frm,
        type: 'POST',
        success: function (response) {
            loadCorteMayor();
            $('#btn-cancelar-stock-mayor').click();
            sweetAlert("DATOS CORRECTOS", "REGISTRO DE STOCK REALIZADO  CORRECTAMENTE", "success");
        }
    });
}
function eliminarProducto(id, nombre) {
    swal({
            title: "Eliminar Producto",
            text: "Está a punto de eliminar "+nombre+". ¿Desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    url: 'eliminar_producto.php',
                    data: {id: id},
                    success: function () {
                        swal("Producto eliminado!", "El producto ha sido removido de la base de datos", "success");
                        obtenerProductos();
                    }
                });
            }
        }
    );
}
function verCorteMenor(id){
    document.location.href = 'view_corte_mayor.php?id='+id;
    loadCorteMenor();
}
function agregarStock(id){
    if($("#stock"+id).val()=='' || $("#stock"+id).val()==0){
        $("#stock"+id).focus();
        sweetAlert("DATOS INCORRECTOS", "Señor Usuario Verifique Sus Datos", "error");
        $("#stock"+id).focus();
        return;
    }else{
        var stock= $("#stock"+id).val();
        $.ajax({
            url: 'addStok.php',
            data: 'id='+id+'&stock='+stock,
            type: 'POST',
            success: function (valores) {
                obtenerProductos();
                //swal("DATOS CORRECTOS!", "Stock registrado correctamente!", "success");
            }
        });
    }
}


function fn_paginar(var_div, url){
    var div = $("#" + var_div);
    $(div).load(url);
}

function onlyNumbers(e) {//Funcion que devuelve solo numeros y punto
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "";
    especiales = [46,48, 49, 50, 51, 52, 53, 54, 55, 56, 57]; //,37,39,46,8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57
    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}
