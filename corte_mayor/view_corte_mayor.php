<?php
require_once 'head.php';

$corte_mayor_id = $_GET['id'];
//obtenemos datos del producto mayor
$cmd_obtener_producto_mayor = Database::getInstance()->getDb()->prepare("SELECT * FROM producto_mayor WHERE id=:id");
$cmd_obtener_producto_mayor->bindParam(':id', $corte_mayor_id);
$cmd_obtener_producto_mayor->execute();
$datos_producto_mayor = $cmd_obtener_producto_mayor->fetch();
//para obtener los corte menores
$cmd_stock_disponible = Database::getInstance()->getDb()->prepare("SELECT SUM(stock) AS disponible FROM stock_mayor WHERE terminado = 1 
                                                                           AND estado = 1 AND producto_mayor_id = '$corte_mayor_id'");
$cmd_stock_disponible->execute();
$datos_stock_disponible = $cmd_stock_disponible->fetch();
?>
<div class="page-content">
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="widget-body">
                <div class="col-lg-offset-2">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-4">
                            <center>
                                <h4><strong>CORTE MAYOR </strong></h4>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="col-lg-offset-2">
                    <div class="row">
                        <label style="font-size: 18px"><strong>DESCRIPCION : </strong></label><label
                            style="font-size: 18px">
                            &nbsp;&nbsp;<?php echo strtoupper($datos_producto_mayor['descripcion']); ?></label>
                    </div>
                    <div class="row">
                        <label style="font-size: 18px"><strong>STOCK DISPONIBLE: </strong></label><label
                            style="font-size: 18px" id="lbl-stockDisponible">
                            &nbsp;&nbsp;<?php echo strtoupper($datos_stock_disponible['disponible']); ?></label>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-4">
                            <center>
                                <h4><strong>CORTES MENORES: </strong></h4>
                                <a class="btn btn-darkorange shiny" id="btn-corte-menor">
                                    <i class="fa fa-plus">
                                    </i> NUEVO CORTE MENOR
                                </a>
                            </center>
                        </div>
                    </div>

                    <br>
                </div>
                <br>
                <form action="javascript: registrarStock();" class="form-horizontal" id="frm-registrar-stock">
                    <input type="text" value="<?= $corte_mayor_id ?>" hidden name="ocultoId" id="ocultoId">
                    <div class="table-responsive " id="div-corte-menor">

                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal para registrar nuevo cliente -->
<div class="modal fade" id="modal-new-corte-mayor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>NUEVO CORTE MENOR</b></h4>
            </div>
            <form id="frm-new-corte-mayor" name="frm-new-corte-mayor" class="form-horizontal">
                <div class="modal-body">
                    <center>
                        <div class="row">
                            <div class="col-lg-10 col-sm-10 col-xs-10 col-lg-offset-1">
                                <div class="form-horizontal">
                                    <div class="form-group ">
                                        <label class="control-label"><h5><strong>D E S C R I P C I O N</h5></label>
                                        <input type="text" class="form-control" id="nombreMedida" name="nombreMedida"
                                               placeholder="Nombre Del Nuevo Corte Nayor"/>
                                    </div>
                                </div>
                            </div>
                    </center>
                </div>
                <div class="modal-footer">
                    <div class="form-inline">
                        <div class="form-group">
                            <button id="guardarUnidadMedida" class="btn btn-primary"
                                    onclick="return GuardarRegistro();" data-dismiss="modal" aria-hidden="true">
                                Guardar
                            </button>
                            <button data-bb-handler="cancel" type="button" class="btn btn-danger"
                                    data-dismiss="modal" aria-hidden="true">Cancelar
                            </button>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- pruebaa -->
<div class="modal fade" id="modal-corte-menor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary shiny">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>AGREGAR CORTE MENOR</b></h4>
            </div>
            <form id="frm-corte-menor" name="frm-corte-menor" class="form-horizontal" action="javascript:registrarCorteMenor();  ">
                <div class="modal-body">
                    <center>
                        <div class="row">
                            <div class="col-lg-10 col-sm-10 col-xs-10 col-lg-offset-1">
                                <div class="form-horizontal">
                                    <input type="text" value="<?= $corte_mayor_id ?>" hidden name="cod-mayor">
                                    <div class="form-group ">
                                        <label class="control-label"><h5><strong>SELECCIONE CORTE MENOR</h5></label>
                                        <?php
                                        $cmd_list_producto = Database::getInstance()->getDb()->prepare("SELECT * FROM Producto WHERE estado = 1");
                                        $cmd_list_producto->execute();
                                        ?>
                                        <select id="cod-menor" name="cod-menor" class="form-control" required>
                                            <option value="">SELECCIONE UN PRODUCTO</option>
                                            <?php while ($row_producto = $cmd_list_producto->fetch()): ?>
                                                <option
                                                    value="<?= $row_producto['codigoProducto']; ?>"><?php echo strtoupper($row_producto['nombreComercial']); ?></option>
                                            <?php endwhile; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    </center>
                </div>
                <div class="modal-footer">
                    <div class="form-inline">
                        <div class="form-group">
                            <button id="guardarUnidadMedida" class="btn btn-primary" type="submit">
                                Guardar
                            </button>
                            <button data-bb-handler="cancel" id="cancel-menor" type="button" class="btn btn-danger"
                                    data-dismiss="modal" aria-hidden="true">Cancelar
                            </button>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<?php
require_once '../header_footer/footer.php';
?>

