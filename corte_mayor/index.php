<?php
require_once 'head.php';
require_once '../datos/Database.php';
$cmd_list_unidad_de_medidad = Database::getInstance()->getDb()->prepare("SELECT * FROM unidadmedida");
$cmd_list_unidad_de_medidad->execute();
// query para consulta de unidad de medidad

//$productos = $db->Producto('estado = ?','1');/
// Obtener el stock de los productos activos
?>
<div class="page-content">
    <div class="page-header position-relative">
        <div class="header-title">
            <h1><strong>CORTES MAYORES</strong></h1>
        </div>
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="widget-body">
                <div class="row">
                    <div class="col-md-offset-5 col-md-4">
                        <h3><strong>CORTE MAYOR DE RESES</strong></h3>
                    </div>
                    <div class="col-md-offset-4 col-md-4">
                         <span class="input-icon">
                             <input type="text" size="40" class="form-control"
                                    placeholder="Buscar Corte Mayor " id="bus-producto"/>
                             <i class="glyphicon glyphicon-search circular blue"></i>
                         </span>

                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-darkorange shiny" id="new-corte-mayor"><i class="fa fa-plus"></i> NUEVO CORTE MAYOR
                        </a>
                    </div>
                    <br>
                </div>
                <br>
                <form action="javascript: registrarStock();" class="form-horizontal" id="frm-registrar-stock">
                    <div class="table-responsive" id="div-producto">

                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal para registrar nuevo cliente -->
<div class="modal fade" id="modal-new-corte-mayor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>NUEVO CORTE MAYOR</b></h4>
            </div>
            <form action="javascript:saveCorteMayor();" id="frm-new-corte-mayor" name="frm-new-corte-mayor" class="form-horizontal">
                <div class="modal-body">
                    <center>
                        <div class="row">
                            <div class="col-lg-10 col-sm-10 col-xs-10 col-lg-offset-1">
                                <div class="form-horizontal">
                                    <div class="form-group ">
                                        <label class="control-label"><h5><strong>DESCRIPCION</h5></label>
                                        <input type="text" class="form-control" id="name-corte-mayor" name="name-corte-mayor"
                                               placeholder="Nombre Del Nuevo Corte Nayor" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-sm-10 col-xs-10 col-lg-offset-1">
                                <div class="form-horizontal">
                                    <div class="form-group ">
                                        <label class="control-label"><h5><strong>UNIDAD DE MEDIDA</h5></label>
                                        <select name="unidad-medida" id="unidad-medida" class="btn-default form-control" required>
                                            <option value="">SELECCIONE UNIDAD DE MEDIDA</option>
                                            <?php while ($row_unidad_medida = $cmd_list_unidad_de_medidad->fetch()): ?>
                                                <option value="<?= $row_unidad_medida['idUnidad'] ?>"><?= $row_unidad_medida['nombreUM']; ?></option>
                                            <?php endwhile; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>
                </div>
                <div class="modal-footer">
                    <div class="form-inline">
                        <div class="form-group">
                            <button id="guardarUnidadMedida" class="btn btn-success" type="submit">GUARDAR</button>
                            <button data-bb-handler="cancel" id="btn-calcel-cortemayor" type="button" class="btn btn-danger"
                                    data-dismiss="modal" aria-hidden="true">CANCELAR
                            </button>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- pasar a otro lado-->
<div class="modal fade" id="modal-add-stock-mayor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b><label id="lbl-title"  style="font-size: 18px;"></label></b></h4>
            </div>
            <form id="frm-add-stock-mayor" name="frm-add-stock-mayor" action="javascript:registrarStockMayor();" class="form-horizontal">
                <div class="modal-body">
                    <center>
                        <div class="row">
                            <div class="col-lg-10 col-sm-10 col-xs-10 col-lg-offset-1">
                                <div class="form-horizontal">
                                    <div class="form-group ">
                                        <input type="text" hidden value="" id="id-stock-mayor" name="id-stock-mayor"></input>
                                        <label class="control-label"><h5><strong>CANTIDAD</h5></label>
                                        <input type="text" class="form-control" id="inp-stock-mayor" name="inp-stock-mayor" required
                                               placeholder="Cantidad del Stock que decea agregar" style="font-size:18px; text-align:center"/>
                                    </div>
                                </div>
                            </div>
                    </center>
                </div>
                <div class="modal-footer">
                    <div class="form-inline">
                        <div class="form-group">
                            <button id="guardarUnidadMedida" class="btn btn-primary" type="submit">
                                GUARDAR
                            </button>
                            <button data-bb-handler="cancel" type="button" id="btn-cancelar-stock-mayor" class="btn btn-danger"
                                    data-dismiss="modal" aria-hidden="true">CANCELAR
                            </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<?php
require_once '../header_footer/footer.php';
?>

