<?php
    require_once '../datos/Database.php';
    $cod_mayor = $_POST['cod-mayor'];
    $cod_menor = $_POST['cod-menor'];
    //verificamos si ya esta o no esta registrado
    $cmd_verify_producto_mayor_menor = Database::getInstance()->getDb()->prepare("SELECT * 
                                                                                  FROM producto_mayor_menor 
                                                                                  WHERE producto_mayor_id=:producto_mayor_id AND producto_id =:producto_id ");
    $cmd_verify_producto_mayor_menor->bindParam(':producto_mayor_id',$cod_mayor);
    $cmd_verify_producto_mayor_menor->bindParam(':producto_id',$cod_menor);
    $cmd_verify_producto_mayor_menor->execute();
    if($cmd_verify_producto_mayor_menor->rowCount()>0){
        $datos = $cmd_verify_producto_mayor_menor->fetch();
        if($datos['estado']==0){
            $cmd_cambiar_estado = Database::getInstance()->getDb()->prepare("UPDATE producto_mayor_menor SET estado = 1 
                                                                             WHERE producto_mayor_id =:cod_mayor AND producto_id=:cod_menor");
            $cmd_cambiar_estado->bindParam(':cod_mayor',$cod_mayor);
            $cmd_cambiar_estado->bindParam(':cod_menor',$cod_menor);
            $cmd_cambiar_estado->execute();
            echo "ACTUALIZE";
            exit();
        }
        echo "YA EXISTE";
        exit();
    }
    //
    $cmd_register_corte_mayor = Database::getInstance()->getDb()->prepare("INSERT INTO producto_mayor_menor VALUES (:cod_mayor,:cod_menor,1)");
    $cmd_register_corte_mayor->bindParam(':cod_mayor',$cod_mayor);
    $cmd_register_corte_mayor->bindParam(':cod_menor',$cod_menor);
    $cmd_register_corte_mayor->execute();
    echo "RESTRE NUEVO";
    exit();
?>