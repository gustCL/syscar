<?php
require_once '../datos/conexion.php';
require_once '../datos/Database.php';
include "../extras/PHPPaging.lib/PHPPaging.lib.php";

if (isset($_GET['val'])) {
    $indicio = $_REQUEST['val'];
    $query_corte_mayor = "SELECT * FROM producto_mayor pm
                          WHERE pm.estado = 1 AND (pm.descripcion LIKE '$indicio%')";
} else {
    $query_corte_mayor = "SELECT * FROM producto_mayor 
                          WHERE estado = 1";
}
$pagina = new PHPPaging;
$pagina->agregarConsulta($query_corte_mayor);
$pagina->modo('desarrollo');
$pagina->verPost(true);
$pagina->porPagina(8);
$pagina->paginasAntes(4);
$pagina->paginasDespues(4);
$pagina->linkSeparador(" - "); //Significa que no habrá separacion
$pagina->div('div-producto');
$pagina->linkSeparadorEspecial('...');   // Separador especial
$pagina->ejecutar();
?>
<table class="table table-striped ">
    <thead>
    <tr>
        <!-- <th>NRO.</th>-->
        <th width="15%">DESCRIPCION</th>
        <th width="15%">CANTIDAD DE INGRESO(PIEZAS)</th>
        <th width="20%">TOTAL Kg INGRESADO</th>
        <th width="20%">STOCK DISPONIBLE</th>
        <th width="30%"></th>
    </tr>
    </thead>
    <tbody>
    <?php while ($row_corte_mayor = $pagina->fetchResultado()): ?>
        <?php
        $codigo_corte_mayor = $row_corte_mayor['id'];
        //para obtener su cantidad
        $cmd_stock_disponible = Database::getInstance()->getDb()->prepare("SELECT SUM(stock) AS disponible FROM stock_mayor WHERE terminado = 1 
                                                                               AND estado = 1 AND producto_mayor_id = '$codigo_corte_mayor'");
        $cmd_stock_disponible->execute();
        $datos_stock_disponible = $cmd_stock_disponible->fetch();
        //obtenemos cantidad de ingresos para producto mayor
        $cmd_stock_ingresado = Database::getInstance()->getDb()->prepare("SELECT COUNT(*) AS ingreso FROM stock_mayor 
                                                                          WHERE producto_mayor_id = '$codigo_corte_mayor'");
        $cmd_stock_ingresado->execute();
        $datos_stock_ingresado = $cmd_stock_ingresado->fetch();
        //query for get sum stock
        $cmd_total_ingreso = Database::getInstance()->getDb()->prepare("SELECT SUM(sm.cantidad_ingresada) AS total
                                                                        FROM stock_mayor sm
                                                                        WHERE sm.producto_mayor_id = '$codigo_corte_mayor'");
        $cmd_total_ingreso->execute();
        $datos_total = $cmd_total_ingreso->fetch();
        ?>
        <tr>
            <td><?php echo strtoupper($row_corte_mayor['descripcion']); ?></td>
            <td><?= $datos_stock_ingresado['ingreso']?></td>
            <td><?= $datos_total['total']?></td>
            <td><?= $datos_stock_disponible['disponible']?></td>
            <td>
                <button type="button" class="btn btn-active btn-sm shiny " onclick="addStockCorteMayor('<?= $codigo_corte_mayor;?>','<?= $row_corte_mayor['descripcion'];?>');">AGREGAR STOCK</button>
                <button type="button" class="btn btn-success btn-sm shiny " onclick="verCorteMenor('<?= $codigo_corte_mayor;?>');">CORTES MENORES</button>
                <button type="button" class="btn btn-primary btn-sm shiny " onclick="deleteCorteMayor('<?= $codigo_corte_mayor;?>','<?php echo strtoupper($row_corte_mayor['descripcion']); ?>');">ELIMINAR</button>
            </td>
        </tr>
    <?php endwhile; ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="7">Numero de pagina</td>
    </tr>
    </tfoot>
</table>