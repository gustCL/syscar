<?php
    require_once '../datos/Database.php';
    INI_SET("date.timezone", "America/La_Paz");
    $fecha_actual = date('Y-m-d');
    $hora_actual = date('H:i:s'); //hora actual del sistema
    $codMayor = $_POST['codMayor'];
    $codMenor = $_POST['codMenor'];
    $cantidad = $_POST['cant'];


//verificamos stock total disponible  disponible en el estock_mayor
    $cmd_verificar_stock = Database::getInstance()->getDb()->prepare("SELECT SUM(sm.stock)AS stock_disponible 
                                                                      FROM stock_mayor sm,producto_mayor_menor pmm 
                                                                      WHERE sm.producto_mayor_id =:codMayor 
                                                                      AND pmm.producto_id =:codMenor");
    $cmd_verificar_stock->bindParam(':codMayor',$codMayor);
    $cmd_verificar_stock->bindParam(':codMenor',$codMenor);
    $cmd_verificar_stock->execute();
    $datos_stok_dosponible = $cmd_verificar_stock->fetch();
    if($datos_stok_dosponible['stock_disponible']<$cantidad){
        $datos = array(
            0 => 'false'
        );
        echo json_encode($datos);
        exit();
    }
    //registramos en transfericia
    $cmd_save_transferencia = Database::getInstance()->getDb()->prepare("INSERT INTO transferencia VALUES (0,
                                                                                                           :producto_mayor_id,
                                                                                                           :producto_id,
                                                                                                           :cantidad,
                                                                                                           :fecha_registro,
                                                                                                           :hora_registro)");
    $cmd_save_transferencia->bindParam(':producto_mayor_id',$codMayor);
    $cmd_save_transferencia->bindParam(':producto_id',$codMenor);
    $cmd_save_transferencia->bindParam(':cantidad',$cantidad);
    $cmd_save_transferencia->bindParam(':fecha_registro',$fecha_actual);
    $cmd_save_transferencia->bindParam(':hora_registro',$hora_actual);
    $cmd_save_transferencia->execute();
    //STOCK MAYOR

    //OBTENEMOS TODOS LOS STOCK DE ESE PRODUCTO MAYOR PARA LUEGO REDUCIR EN ELLOS HASTA Q SUME LA CANTIDAD TOTAL
    $cmd_list_stock = Database::getInstance()->getDb()->prepare("SELECT * FROM stock_mayor sm
                                                                 WHERE sm.producto_mayor_id =:codMayor 
                                                                 AND sm.terminado = 1 ORDER BY sm.id ASC");
    $cmd_list_stock->bindParam(':codMayor',$codMayor);
    $cmd_list_stock->execute();
    $flag_completo = 'false';
    $contenedor_total = $cantidad;
    while ($flag_completo=='false'){
        $row_stock = $cmd_list_stock->fetch();
        if($contenedor_total>=$row_stock['stock']){
            $contenedor_total = $contenedor_total - $row_stock['stock'];
            $cmd_update_stock = Database::getInstance()->getDb()->prepare("UPDATE stock_mayor SET stock = 0 
                                                                           WHERE producto_mayor_id =:codMayor AND id=:id_stock");
            $cmd_update_stock->bindParam(':codMayor',$codMayor);
            $cmd_update_stock->bindParam(':id_stock',$row_stock['id']);
            $cmd_update_stock->execute();
        }else if($row_stock['stock']>$contenedor_total){
            $stockMayor = $row_stock['stock']-$contenedor_total;
            $cmd_update_stock = Database::getInstance()->getDb()->prepare("UPDATE stock_mayor SET stock =:stockMayor  
                                                                           WHERE producto_mayor_id =:codMayor AND id=:id_stock ");
            $cmd_update_stock->bindParam(':stockMayor',$stockMayor);
            $cmd_update_stock->bindParam(':codMayor',$codMayor);
            $cmd_update_stock->bindParam(':id_stock',$row_stock['id']);
            $cmd_update_stock->execute();
            $contenedor_total = 0;
        }
        if($contenedor_total==0){
            $flag_completo = 'true';
        }
    }

    //procedemos a registrar el stock en PRODUCTOS, para eso recuperamos todos los datos de producto
    $nroLote = 'XXX';
    $nada = 'Nada';
    $cmd_producto = Database::getInstance()->getDb()->prepare("SELECT * FROM Producto WHERE codigoProducto='$codMenor'");
    $cmd_producto->execute();
    $datos_producto = $cmd_producto->fetch();
    //como ya recuperamos insertamos
    $cmd_lote_insert = Database::getInstance()->getDb()->prepare("INSERT INTO Lote VALUES(0,
                                                                                          :nroLote,
                                                                                          :codigoProducto,
                                                                                          1,
                                                                                          :precioCosto,
                                                                                          :precioVenta,
                                                                                          :margen,
                                                                                          :fechaVencimiento,
                                                                                          :cantidad,
                                                                                          :observaciones,
                                                                                          0,
                                                                                          1)");
    $cmd_lote_insert->bindParam(':nroLote', $nroLote);
    $cmd_lote_insert->bindParam(':codigoProducto', $datos_producto['codigoProducto']);
    $cmd_lote_insert->bindParam(':precioCosto', $datos_producto['precioCosto']);
    $cmd_lote_insert->bindParam(':precioVenta', $datos_producto['precioVenta']);
    $cmd_lote_insert->bindParam(':margen', $datos_producto['margen']);
    $cmd_lote_insert->bindParam(':fechaVencimiento', $fecha_actual);
    $cmd_lote_insert->bindParam(':cantidad', $cantidad);
    $cmd_lote_insert->bindParam(':observaciones',$nada);
    $cmd_lote_insert->execute();

    //reuperamos el id del ULTIMO LOTE INSERTADO
    $DATO6 = Database::getInstance()->getDb()->prepare("SELECT Max(idLote) as idLote FROM Lote  ");
    $DATO6->execute();
    $count = $DATO6->fetch(PDO::FETCH_ASSOC);
    $idlote1 = $count['idLote'];
    ///INSERTAMOS EN UBICACION
    $cmd_lote_insert = Database::getInstance()->getDb()->prepare("INSERT INTO Ubicacion VALUES(0,
                                                                                               1,
                                                                                               :codigoProducto,
                                                                                               1)");
    $cmd_lote_insert->bindParam(':codigoProducto', $codMenor);
    $cmd_lote_insert->execute();
    //recuperamos Ultima Ubicacion
    $DATO10 = Database::getInstance()->getDb()->prepare("SELECT Max(idUbicacion) as idUbicacion FROM Ubicacion");
    $DATO10->execute();
    $count = $DATO10->fetch(PDO::FETCH_ASSOC);
    $idUbica = $count['idUbicacion'];
    ///insertamos en EXISTENCIAS
    $cmd_existencias_insert = Database::getInstance()->getDb()->prepare("INSERT INTO Existencias VALUES(0,
                                                                                                        :idLote,
                                                                                                        :idUbicacion,
                                                                                                        :stock,
                                                                                                        1,
                                                                                                        1)");
    $cmd_existencias_insert->bindParam(':idLote', $idlote1);
    $cmd_existencias_insert->bindParam(':idUbicacion', $idUbica);
    $cmd_existencias_insert->bindParam(':stock',$cantidad);
    $cmd_existencias_insert->execute();
    $datos = array(
        0 => 'true'
    );
    echo json_encode($datos);
    exit();

?>