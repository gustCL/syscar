<?php
    require_once '../datos/Database.php';
    $corte_mayor_id = $_GET['id'];
    $cmd_lista_corte_menor = Database::getInstance()->getDb()->prepare("SELECT p.nombreComercial, p.codigoProducto 
                                                                        FROM producto_mayor pm, producto_mayor_menor pmm, producto p
                                                                         WHERE pm.id = pmm.producto_mayor_id AND pmm.producto_id = p.codigoProducto AND pm.id =:id AND pmm.estado = 1");
    $cmd_lista_corte_menor->bindParam(':id',$corte_mayor_id);
    $cmd_lista_corte_menor->execute();
    
    
?>
<table class="table table-striped ">
    <thead>
    <tr>
        <!-- <th>NRO.</th>-->
        <th width="15%">PRODUCTO</th>
        <th width="15%"></th>
        <th width="15%"></th>
        <th width="15%"></th>
        <th width="20%"></th>
        <th width="20%"></th>
    </tr>
    </thead>
    <tbody>
    <?php while ($row_corte_menor = $cmd_lista_corte_menor->fetch()): ?>
        <?php
        $codigo_corte_menor = $row_corte_menor['codigoProducto'];
        $nombre_corte_menor = $row_corte_menor['nombreComercial']
        ?>
        <tr>
            <td><?php echo strtoupper($row_corte_menor['nombreComercial']); ?></td>
            <td></td>
            <td></td>
            <td>
                <input  class="form-control btn-sm" onkeypress="return onlyNumbers(event);" type="text" id="inp<?= $codigo_corte_menor?>" name="inp<?= $codigo_corte_menor?>">
            </td>
            <td>
                <button  type="button" class="btn btn-success btn-sm shiny block btn-block "
                         onclick="transferencia('<?= $corte_mayor_id;?>','<?= $codigo_corte_menor;?>');">
                    AGREGAR AL STOCK
                </button>
            </td>
            <td>
                <button type="button" class="btn btn-block btn-primary btn-sm shiny " onclick="deleteCorteMenor('<?= $codigo_corte_menor;?>','<?= $nombre_corte_menor?>')">ELIMINAR</button>
            </td>
        </tr>
    <?php endwhile; ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="7">Numero de pagina</td>
    </tr>
    </tfoot>
</table>

