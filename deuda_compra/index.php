<?php
    require_once 'head.php';
?>
<div class="page-content">
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <Strong>DEUDAS POR PAGAR</strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="widget-body">
                <br>
                <center>
                    <div class="form-inline">
                        <div class="input-group">
                            <div class="col-lg-3">
                                <span class="input-icon">
                                    <input id="bs-deuda" name="bs-deuda" type="text" size="70"
                                           class="form-control"
                                           placeholder="Codigo, NIT del Proveedor, Nro. de Factura, Nombre del Proveedor">
                                    <i class="glyphicon glyphicon-search circular blue"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </center>
                <br>
            </div>
            <br>
            <div class="widget-body">
                <!-- TABLA DE SECTORES-->
                <center><h3><strong>LISTA DE DEUDAS POR PAGAR</strong></h3></center>
                <br/>
                <div class="table-responsive" id="div-tabla-deudas">
                </div>
                <br>
            </div>
        </div>
    </div>
</div>

<!--     D I A L O G   M O D A L ##V E R##   -->
<div class="modal fade" id="formulario_ver" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">DEUDAS</h4>
            </div>
            <form id="formularioVer" class="form-horizontal" role="form" >
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="email">Codigo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control "  id="deudacod" name="deudacod" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd">Nro Factura</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="deudanombre" name="deudanombre" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd">Proveedor</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="deudaproveedor" name="deudaproveedor" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd">Fecha pago</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="deudafecha" name="deudafecha" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="pwd">Saldo a pagar</label>
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control"  id="deudasaldo" name="deudasaldo" readonly>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <center>

                                <div class="form-group">
                                    <button data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                </div>  
                            </center>
                        </div>  
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


<?php
    require_once '../header_footer/footer.php';
?>
