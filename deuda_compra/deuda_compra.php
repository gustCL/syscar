<?php 
    session_start();
    if ($_SESSION['logueado'] != 'SI') {
        header('Location: ../inicio');
    }
    require_once '../datos/Database.php';
?>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <!--Fila de titulos de columnas-->
        <tr>
            <th class="hidden-xs">
                Cod.
            </th>
            <th class="hidden-xs">
                Nombre
            </th>
            <th class="hidden-xs">
                Ubicacion
            </th>
            <th class="hidden-xs">
                Tipo Almacen
            </th>
            <th>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        //se realiza la consulta de todos los sectores q estan en la sucursal donde esta logueado el usuario
        //y q tienen como parametro ya sea codigo o nombre o almancen, q ademas es case sensite en cuanto al nombre y almacen
        $dato = $_POST['dato'];
        $auxSuc =$_SESSION['userMaster'];
        $auxidsucursal = $auxSuc['idSucursal'];
        $registro = "SELECT sec.idSector,sec.nombreSector,al.nombreAlmacen 
                    FROM 
                    Sectores sec, Almacen al , Sucursal su 
                    WHERE sec.idAlmacen=al.idAlmacen 
                    AND su.idSucursal=al.idSucursal 
                    AND su.idSucursal='$auxidsucursal'
                    AND sec.estado=1 
                    AND (sec.idSector LIKE '%$dato%'
                         OR sec.nombreSector LIKE '%$dato%'    
                         OR al.nombreAlmacen LIKE '%$dato%')";        
        try {
            $comando = Database::getInstance()->getDb()->prepare($registro);
            $comando->execute();
            while ($row = $comando->fetch()) {
                $idS = $row['idSector'];
                ?><tr>
                    <td id="idSector">
                        <?= $idS ?>
                    </td>
                    <td class="hidden-xs">
                        <?= $row['nombreSector'] ?>
                    </td>
                    <td>
                        <?= $row['nombreAlmacen'] ?>
                    </td>
                    <td>
                        <a href="javascript:verSector('<?= $idS ?>');"  class="btn btn-default btn-xs blue" ><i class="fa fa-eye"></i> Ver</a>
                        <a  href="javascript:editarSector('<?= $idS ?>');"  class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                        <a href="javascript:eliminarSector('<?= $idS ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                    </td>

                </tr>

                <?php
            }//terminacion del while
        } catch (PDOException $e) {
            echo 'Error: ' . $e;
        }
        ?>
    </tbody>
</table>
