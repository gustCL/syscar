<?php
    require_once 'head.php';

    $id = $_GET['id'];

    // Recupermos los datos de la factura(id)
    $query_deuda = "SELECT  *
                            FROM RegistroPagoCompra rpc, FacturaIngreso fi, Proveedor pr
                            WHERE rpc.idNotaIngreso = fi.idNotaIngreso
                            AND fi.idProveedor = pr.idProveedor
                            AND rpc.idPagoCompra = :id";
    $cmd_deuda = Database::getInstance()->getDb()->prepare($query_deuda);
    $cmd_deuda->bindParam(':id', $id);
    $cmd_deuda->execute();
    $result = $cmd_deuda->fetch();
?>
<div class="page-content">        
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>PAGO DE CREDITO</strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <div class="page-body">
        <div class="widget-body">
            <form method="post" onsubmit="return validarmontos();" class="form-horizontal" id="formEnviar">
                <div class="form-group">
                    <div class="col-lg-offset-4">
                        <h3><strong>REGISTRAR PAGO</strong></h3>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" style=" font-weight: bold"
                           for="idNotaIngreso">Nro. Factura:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="idNotaIngreso" name="idNotaIngreso"
                               readonly value="<?= $result['idNotaIngreso'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" style=" font-weight: bold"
                           for="montoContado">Saldo Pagado:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="montoContado" name="montoContado"
                               readonly value="<?= $result['montoContado'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="saldos"
                           style=" font-weight: bold">Saldo por Pagar:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" readonly id="saldos" name="saldos"
                               value="<?= $result['saldo'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="montopagar"
                           style=" font-weight: bold">Monto a pagar:</label>
                    <div class="col-sm-6">
                        <input type="number" min="1" class="form-control" id="montopagar" name="montopagar">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="resumen"
                           style=" font-weight: bold">Resumen:</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="resumen" name="resumen"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <center>
                            <input class="btn btn-primary" type="submit" value="Registrar pago">
                            <a href="../deuda_compra/" type="button" class="btn btn-danger">Cancelar</a>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    require_once ('../header_footer/footer.php');
?>  

