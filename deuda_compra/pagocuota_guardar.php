<?php
    session_start();
    if ($_SESSION['logueado'] != 'SI') {
        header('Location: ../inicio');
        exit();
    }
    require_once '../datos/Database.php';

    // Obtenemos las variables de SESSION
    $id_sucursal = $_SESSION['userMaster']['idSucursal'];
    $id_sesion = $_SESSION['idSesion'];
    $id_usuario = $_SESSION['userMaster']['idUsuario'];

    // Insertar en PagoCuotaPagar
    $id_nota_ingreso = $_POST['idNotaIngreso'];
    $saldo = $_POST['saldos'];
    $resumen = $_POST['resumen'];
    $monto_pagar = $_POST['montopagar'];
    ini_set("date.timezone", "America/La_Paz");
    $fecha_registro = date('d-m-Y');
    $hora_registro = date('H:i:s');


    if ($monto_pagar > 0 and $monto_pagar <= $saldo) {
        // Insertar en PagoCuotaPagar
        $query_pago_cuota = "INSERT INTO PagosCuotaPagar() VALUES ('', :id_pago, :fecha_registro, :hora_registro, :monto_pagado, :id_inicio, :id_usuario, :resumen_pago);";
        $cmd_pago_cuota = Database::getInstance()->getDb()->prepare($query_pago_cuota);
        $cmd_pago_cuota->bindParam(':id_pago', $id_nota_ingreso);
        $cmd_pago_cuota->bindParam(':fecha_registro', $fecha_registro);
        $cmd_pago_cuota->bindParam(':hora_registro', $hora_registro);
        $cmd_pago_cuota->bindParam(':monto_pagado', $monto_pagar);
        $cmd_pago_cuota->bindParam(':id_inicio', $id_sesion);
        $cmd_pago_cuota->bindParam(':id_usuario', $id_usuario);
        $cmd_pago_cuota->bindParam(':resumen_pago', $resumen);
        $cmd_pago_cuota->execute();

        // Actualizar FacturaIngreso
        $query_registro_pago_compra = "UPDATE RegistroPagoCompra
                                       SET saldo = saldo - :monto_pagar, montoContado = montoContado + :monto_pagar
                                       WHERE idNotaIngreso = :id_nota_ingreso";
        $cmd_registro_pago_compra = Database::getInstance()->getDb()->prepare($query_registro_pago_compra);
        $cmd_registro_pago_compra->bindParam(':monto_pagar', $monto_pagar);
        $cmd_registro_pago_compra->bindParam(':id_nota_ingreso', $id_nota_ingreso);
        $cmd_registro_pago_compra->execute();

        // Registramos en FacturaIngreso.
        $query_factura_ingreso = "SELECT * FROM  FacturaIngreso WHERE idNotaIngreso = :id_nota_ingreso";
        $cmd_factura_ingreso = Database::getInstance()->getDb()->prepare($query_factura_ingreso);
        $cmd_factura_ingreso->bindParam(':id_nota_ingreso', $id_nota_ingreso);
        $cmd_factura_ingreso->execute();
        $result_factura_ingreso = $cmd_factura_ingreso->fetch();
        $id_proveedor = $result_factura_ingreso['idProveedor'];

        // Recuramos los datos del proveedor
        $query_proveedor = "SELECT * FROM  Proveedor WHERE idProveedor = :id_proveedor";
        $cmd_proveedor = Database::getInstance()->getDb()->prepare($query_proveedor);
        $cmd_proveedor->bindParam(':id_proveedor', $id_proveedor);
        $cmd_proveedor->execute();
        $result_proveedor = $cmd_proveedor->fetch();

        // Creamos la glosa q es Nombre proveedor con su nit(concatenamos);
        $cadenaGlosa = "Proveedor: " . $result_proveedor['nombreProve'] . "  NIT:" . $result_proveedor['nit'];

        $idTipoIngresoDos = 2;
        $cero = 0;
        $cmd_insert_egresoCaja = Database::getInstance()->getDb()->prepare("INSERT INTO EgresoCaja VALUES(
                                                                                        0,
                                                                                        :idInicio,
                                                                                        2,
                                                                                        :nroFactura,
                                                                                        :nroNotaIngreso,
                                                                                        :glosa,
                                                                                        :montoTotal,
                                                                                        :idTipoCambio,
                                                                                        :tipoCambio,
                                                                                        :efectivoBs,
                                                                                        0,
                                                                                        :fechaRegistro,
                                                                                        :horaRegistro,
                                                                                        1)");
        $cmd_insert_egresoCaja->bindParam(':idInicio', $id_sesion);
        $cmd_insert_egresoCaja->bindParam(':nroFactura', $result_factura_ingreso['nroFactura']);
        $cmd_insert_egresoCaja->bindParam(':nroNotaIngreso', $result_factura_ingreso['idNotaIngreso']);
        $cmd_insert_egresoCaja->bindParam(':glosa', $cadenaGlosa);
        $cmd_insert_egresoCaja->bindParam(':montoTotal', $monto_pagar);
        $cmd_insert_egresoCaja->bindParam(':idTipoCambio', $result_factura_ingreso['idTipoCambio']);
        $cmd_insert_egresoCaja->bindParam(':tipoCambio', $result_factura_ingreso['tipoCambioCompra']);
        $cmd_insert_egresoCaja->bindParam(':efectivoBs', $monto_pagar);
        $cmd_insert_egresoCaja->bindParam(':fechaRegistro', $fecha_registro);
        $cmd_insert_egresoCaja->bindParam(':horaRegistro', $hora_registro);
        $cmd_insert_egresoCaja->execute();

        //recuperamos ID del ultimo EgresoCaja para insertarlo luego en EgresoCompra  [$dataFacturaCompra['nroFactura'],$dataFacturaCompra['nroNotaIngreso'],$cadenaGlosa,$montopagar,$dataFacturaCompra['idTipoCambio'],$dataFacturaCompra['tipoCambioCompra'],$montopagar,$dataFacturaCompra['tipoCambioCompra'],$fecharegistro,$horaregistro]
        $query_lastIdEgresoCaja = "SELECT * FROM EgresoCaja ORDER BY idEgresoCaja DESC Limit 1";
        $cmd_select_lastIdEgresoCaja = Database::getInstance()->getDb()->prepare($query_lastIdEgresoCaja);
        $cmd_select_lastIdEgresoCaja->execute();
        $idEgresoCaja = $cmd_select_lastIdEgresoCaja->fetch();

        // Insertamos en EgresoCompra
        $query_insertEgresoCompra = "INSERT INTO EgresoCompra VALUES(:idEgresoCaja, :idNotaIngreso)";
        $cmd_egresocompra = Database::getInstance()->getDb()->prepare($query_insertEgresoCompra);
        $cmd_egresocompra->bindParam(':idEgresoCaja', $idEgresoCaja['idEgresoCaja']);
        $cmd_egresocompra->bindParam(':idNotaIngreso', $idEgresoCaja['nroNotaIngreso']);
        $cmd_egresocompra->execute();

        //si se termino de pagar el saldo pones q ya se pago todo en pagado = 0
        if (($saldo - $monto_pagar) == 0) {
            $query_registro_pago_compra = "UPDATE RegistroPagoCompra
                                SET pagado=0
                                WHERE idNotaIngreso='$id_nota_ingreso'";
            $cmd_pago_cuota = Database::getInstance()->getDb()->prepare($query_registro_pago_compra);
            $cmd_pago_cuota->execute();
        }
    } else {
        echo "<script> alert('Ingrese un monto menor') </script>";
    }

    $datos = array(
        0 => $id_nota_ingreso,//id del credito
        1 => $monto_pagar,

    );
    echo json_encode($datos);
    exit();
?>
