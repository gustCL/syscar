$(function() {

    //  Al cargar la página, obtener todas las deudas por pagar
    obtenerDeudas();

    $('#bs-deuda').keyup( function() {
        var dato = $('#bs-deuda').val();
        dato = dato.replace(/ /g, "%20");
        $("#div-tabla-deudas").load('obtener_deudas.php?dato='+dato);
    });
});

// Obtiene todas las deudas por pagar en una tabla y la inserta en el elemento respectivo
function obtenerDeudas() {
    $("#div-tabla-deudas").load('obtener_deudas.php');
}

function validarmontos() {
    var montoPago = $('#montopagar').val();
    var saldoRestante = $('#saldos').val();
    montoPago = parseFloat(montoPago);
    saldoRestante = parseFloat(saldoRestante);
    if (montoPago <= saldoRestante && montoPago > 0) {
        var form = $('#formEnviar').serialize();
        $.ajax({
            url: 'pagocuota_guardar.php',
            data: form,
            type: 'POST',
            success: function(valores){
                var datos = eval(valores);
                document.location.href = 'imprimirpagocompra.php?id=' + datos[0]+ '&monto=' + datos[1];
            }
        });
    } else {
        $('#montopagar').focus();
        alertify.error('El monto a pagar debe ser menor al saldo restante.');
    }
    return false;
}

// Obtener información de la deuda correspondiente
function verDeuda(id) {
    $('#formularioVer')[0].reset();
    $.ajax({
        type: 'POST',
        url: 'deuda_ver.php',
        data: {id: id},
        success: function(valores) {
            var datos = eval(valores);
            $('#deudacod').val(datos[0]);
            $('#deudanombre').val(datos[1]);
            $('#deudaproveedor').val(datos[2]);
            $('#deudafecha').val(datos[3]);
            $('#deudasaldo').val(datos[4]);
            $('#formulario_ver').modal({
                show: true,
                backdrop: 'static'
            });
            return false;
        }
    });
    return false;
}
