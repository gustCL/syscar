<?php
    require_once '../datos/Database.php';

    session_start();
    if ($_SESSION['logueado'] != 'SI') {
        header('Location: ../inicio');
        exit();
    }

    if (!array_search('../deuda_compra', $_SESSION['privilegio'])) {
        header('Location: ../inicio/');
        exit();
    }

    if (isset($_GET['dato'])) {
        $dato = $_GET['dato'];
        $query = "SELECT rpc.idPagoCompra,rpc.idNotaIngreso,fi.nroFactura, pr.nombreProve,rpc.fechaPago,rpc.contado,rpc.pagado, rpc.saldo
                  FROM RegistroPagoCompra rpc, FacturaIngreso fi, Proveedor pr
                  WHERE rpc.pagado=1 AND rpc.contado=0
                  AND rpc.idNotaIngreso=fi.idNotaIngreso
                  AND fi.idProveedor=pr.idProveedor
                  AND (fi.nroFactura LIKE '%$dato%' OR rpc.idNotaIngreso LIKE '%$dato%' OR pr.nit LIKE '%$dato%' OR pr.nombreProve LIKE '%$dato%')";
    } else {
        $query = "SELECT rpc.idPagoCompra,rpc.idNotaIngreso,fi.nroFactura, pr.nombreProve,rpc.fechaPago,rpc.contado,rpc.pagado, rpc.saldo
                  FROM RegistroPagoCompra rpc, FacturaIngreso fi, Proveedor pr
                  WHERE rpc.pagado = 1 AND
                    rpc.contado = 0 AND
                    rpc.saldo > 0 AND
                    rpc.idNotaIngreso = fi.idNotaIngreso AND
                    fi.idProveedor = pr.idProveedor";
    }
    $cmd = Database::getInstance()->getDb()->prepare($query);
    $cmd->execute();
?>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Nro. Nota Ingreso</th>
        <th>Nro. Factura Compra</th>
        <th>Proveedor</th>
        <th>Fecha pago</th>
        <th>Saldo a pagar</th>
        <th>Opciones</th>
    </tr>
    </thead>
    <tbody>
    <?php
    while ($row = $cmd->fetch()) :
        $nro_nota = $row['idNotaIngreso']; ?>
        <tr>
            <td id="idPagoCompra"><?= $nro_nota ?></td>
            <td><?= $row['nroFactura'] ?></td>
            <td><?= $row['nombreProve'] ?></td>
            <td><?= $row['fechaPago'] ?></td>
            <td><?= $row['saldo'] ?></td>
            <td>
                <a href="javascript:verDeuda('<?= $nro_nota ?>');" class="btn btn-default btn-xs blue">
                    <i class="fa fa-eye"></i> Ver</a>
                <a href="registro_pago.php?id=<?= $nro_nota ?>" class="btn btn-default btn-xs purple">
                    <i class="fa fa-edit"></i> Realizar pago</a>
            </td>
        </tr>
    <?php endwhile; ?>
    </tbody>
</table>