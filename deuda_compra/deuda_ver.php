<?php
    require_once '../datos/Database.php';

    $id = $_POST['id'];

    // Obtener la información de la deuda
    $consulta = "SELECT rpc.idPagoCompra, rpc.idNotaIngreso, pr.nombreProve, rpc.fechaPago, rpc.contado, rpc.pagado, rpc.saldo
                 FROM RegistroPagoCompra rpc, FacturaIngreso fi, Proveedor pr
                 WHERE rpc.idNotaIngreso = fi.idNotaIngreso AND
                       fi.idProveedor = pr.idProveedor AND
                       rpc.idPagoCompra = :id";
    $comando = Database::getInstance()->getDb()->prepare($consulta);
    $comando->bindParam(':id', $id);
    $comando->execute();
    $valores = $comando->fetch();

    $datos = array(
        0 => $valores['idPagoCompra'],
        1 => $valores['idNotaIngreso'],
        2 => $valores['nombreProve'],
        3 => $valores['fechaPago'],
        4 => $valores['saldo'],
    );
    echo json_encode($datos);
    exit();
?>
