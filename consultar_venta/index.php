<?php
    require_once 'head.php';
    ini_set("date.timezone", "America/La_Paz");
    $inicioDate=date("Y-m");
    $inicioDate=$inicioDate."-01";
    
?>  
 <script src="../js/jquery.js"></script>
<script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
<script src="../js/autocompletado/jquery-ui-1.10.3.custom.min.js"></script> 
<link rel="stylesheet" href="../js/autocompletado/jquery-ui-1.10.3.custom.min.css">  
<!-- Page Content -->
<div class="page-content">

    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>CONSULTAR VENTAS</strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                   <div class="widget-body " >
                       <br/>
                       <form class="responsive" id="frm_buscar" name="frm_buscar" method="post">  
               
                <div class="row">
                <div class="col-sm-3 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Fecha Inicial</strong></label>
                        <br />
                        <input type="date" name="fechaInicial" id="fechaInicial" value="<?=$inicioDate?>" class="input-sm form-control input-s-sm inline"/>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-3 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Fecha Final</strong></label>
                        <br />
                        <input type="date" name="fechaFinal" id="fechaFinal" value="<?=date("Y-m-d")?>" class="input-sm form-control input-s-sm inline"/>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-4 m-b">                                
                    <div class="input-group">
                       
                        <label for="user_name"></label>
                        <br />
                        <input type="button" id="btnBuscar" class="btn btn-yellow" value="Buscar" onclick="fn_buscar()" />&nbsp;&nbsp;
                       
                        
                    </div> 
                </div>
                </div>
            </form>
                            </div>
                            <br>
                          <div class="widget-body " >
                                
                              <div class="table-responsive" id="div_listar">
                              
                              </div>
                              <br/>
                            </div>
                            <br/>
                        </div>
                
                    </div>
                </div>
            </div>

    <!-- /Page Content -->
</div>
<!-- /Page Container -->


<?php require_once '../header_footer/footer.php'; ?>


