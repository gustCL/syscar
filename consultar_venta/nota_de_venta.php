<?php
require_once ('head.php');

$sesion=$_SESSION['userMaster'];
$idCl = $_GET['id'];
require_once '../datos/Database.php';
//Obtengo detalle de la venta
$consulta = "SELECT* FROM DetalleVenta WHERE idVenta = $idCl ";
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
//OBtengo Datos del clienta
$comando2 = Database::getInstance()->getDb()->prepare($consulta);
$comando2->execute();
$cliente = $comando2->fetch();
$idVenta=$cliente['idVenta'];

$consulta3 = "SELECT* FROM Factura WHERE idVenta = $idCl ";
$comando3 = Database::getInstance()->getDb()->prepare($consulta3);
$comando3->execute();
$nro=$comando3->fetch();
?>  

<!-- Page Content -->
<div class="page-content">

    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>CONSULTAR VENTA</strong>

            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget-body ">
                                <form   class="formulario">
                                    <center><h3><strong>NOTA DE VENTA</strong></h3></center>
                                    
                                    <br/> 
                                    <div class="form-inline" > 
                                        <?php if($nro['idVenta']==$cliente['idVenta']){?>
                                       <div class="form-group" >
                                           <label class="col-lg-12"><h4><strong>Nro. Factura: <?= $nro['nroFactura']?></strong></h4></label>
                                        </div>
                                        <?php }else{?>
                                        <div class="form-group">
                                            <label class="col-lg-12"><h4><strong>Nro. Nota de Venta: <?= $cliente['nroVenta']?></strong></h4></label>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="form-inline" > 
                                        <center>
                                        <div class="form-group">
                                            <label class="col-lg-12"><h4><strong> NIT: </strong><?= $cliente['nit']?></h4></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-12"><h4><strong>Cliente: </strong><?= $cliente['nombreCliente']?></h4></label>
                                        </div>
                                        </center>
                                    </div>                
                                    <br />
                                </form>
                            </div>
                            <br>
                            <div class="widget-body " id="agrega-registros">
                                <form id="formDetalle" name="formDetalle" method="POST">
                                    <input type="text" value="<?= $idCl?>" hidden/><!--ID de la venta-->
                                <center><h3><strong>DETALLE</strong></h3></center>
                                <br/>
                                <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>Nro</strong><input type="text" id="trans" name="trans" value="<?= $cliente['idTransaccion']?>" hidden/></th>
                                                <th><strong>Descripcion</strong></th>
                                                <th><strong>Unidad</strong></th>
                                                <th><strong>Precio</strong></th>
                                                <th><strong>Cantidad</strong></th>
                                                <th><strong>Ubicacion</strong></th>
                                                <th><strong>Monto</strong></th>
                                                <th></th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $descuento = $cliente['descuento'];
                                            $subTotal=$cliente['montoSubTotal'];
                                            
                                            $descuentoP= ($descuento * 100)/$subTotal;
                                            while($row=$comando->fetch()){
                                                
                                            ?>
                                            <tr>
                                                <td> </td>
                                                <td><?= $row['nombreComercial']?> </td>
                                                <td><?= $row['abreviatura']?> </td>
                                                <td><?= $row['precioVenta']?> </td>
                                                <td><?= $row['cantidad']?> </td>
                                                <td><?= $row['nombreSector']?> </td>
                                                <td><?= $row['precioVenta']?> </td>
                                                <td> </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5" class="invisible bg-snow"></td>
                                                <td><strong>Subtotal</strong></td>
                                                <td><?= $cliente['montoSubTotal']?><input type="text" id="Stotal" name="Stotal" size="5" value="<?= $subTotal?>" hidden/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" class="invisible bg-snow"></td>
                                                <td><strong>Descuento %</strong></td>
                                                <td><?= $descuentoP?><input type="text" id="descuentoP" name="descuentoP"size="5" value="<?= $descuentoP ?>" hidden/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" class="invisible bg-snow"></td>
                                                <td><strong>Descuento Bs</strong></td>
                                                <td><?= $descuento?><input type="text" id="descuentoB" name="descuentoB" size="5" value="<?= $descuento?>" hidden/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" class="invisible bg-snow"></td>
                                                <td><strong>Total</strong></td>
                                                <td><?= $cliente['montototal']?><input type="text" id="total" name="total" size="5" value="<?= $cliente['montototal']?>" hidden/></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </form>
                                <br/>
                            </div>
                            <br/>
                            <div class="widget-body ">
                                <form>
                                    <br/>
                                    <div class="form-inline" > 
                                        <center>
                                        <div class="form-group">
                                            <input type="button" class="btn btn-primary" onclick="Anular()" value="Anular"/>
                                        </div>
                                            &nbsp;
                                        <div class="form-group">
                                            <a type="button" class="btn btn-primary" href="consultar_notaVenta.php?vt=<?=$idCl?>" >Imprimir</a>
                                        </div>
                                        </center>
                                    </div>                
                                    <br />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- /Page Content -->
</div>
<!-- /Page Container -->


<!-- End Formulario Modal-->
<?php require_once '../header_footer/footer.php'; ?>



