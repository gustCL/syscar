$(document).ready(function(){
	fn_buscar();
});
$(function() {

});
function fn_buscar(){
	var str = $("#frm_buscar").serialize();
        var inicial=$("#fechaInicial").val();
        var final=$("#fechaFinal").val();
        
	$.ajax({
		url: 'lista_gestionarIngreso.php',
		type: 'get',
		data: str+"&fechaInicial="+inicial+"&fechaFinal="+final,
		success: function(data){
                    $("#div_listar").html(data);
                    
		}
	});
}

function AnularFactura(factura,idventa){
    var idV = idventa;
    var fac = factura;
    var str = "fac="+fac+"&v="+idV;

    swal({
            title: "Esta seguro que desea anular la factura?",
            text: "El estado de la factura sera cambiado totalmente",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Anular Factura!",
            cancelButtonText: "No deseo anular factura!",
            closeOnConfirm: false,
            closeOnCancel: false },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: 'anular.php',
                    data: str,
                    type: 'get',
                    success: function(registro){
                        if (registro == 'error'){
                            swal("Error", "Problemas al anular", "error");
                        }else{
                            swal("Anulada!", "La factura se anulo correctamente", "success");
                            $("#div_listar").html(registro);
                        }
                    }
                });
            } else {
                swal("Cancelado", "Accion cancelada.", "error");
            }
        });
}

function fn_paginar(var_div, url){
	var div = $("#" + var_div);
	$(div).load(url);
}
function fn_anular(idV,nFac){
	var str = $("#frm_buscar").serialize();
	$.ajax({
		url: 'anular.php',
		type: 'post',
		data: str+"v="+idV+"f="+nFac,
		success: function(data){
                  alert("Factura anulada");
                    
		}
	});
}





function fn_imprimirLista(){
       $("#frm_buscar").serialize();
redirect_by_post("imprimir.php",{sucursal:$("#sucursal").val(),fechaInicial:$("#fechaInicial").val(),fechaFinal:$("#fechaFinal").val(),tipoIngreso:$("#tipoIngreso").val(),glosa:$("#glosa").val()},true);
 
};

function redirect_by_post(purl, pparameters, in_new_tab) {
    pparameters = (typeof pparameters == 'undefined') ? {} : pparameters;
    in_new_tab = (typeof in_new_tab == 'undefined') ? true : in_new_tab;
    var form = document.createElement("form");
    $(form).attr("id", "reg-form").attr("name", "reg-form").attr("action", purl).attr("method", "post").attr("enctype", "multipart/form-data");
    if (in_new_tab) {
        $(form).attr("target", "_blank");
    }
    $.each(pparameters, function(key) {
        $(form).append('<input type="text" name="' + key + '" value="' + this + '" />');
    });
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
    return false;
}
