<?php
$nFactura = $_POST['dato'];
?>
<table class="table table-striped " id="formVentas">
                                    <thead>
                                        <!--Fila de titulos de columnas-->
                                        
                                        <tr>
                                    
                                            <th>
                                                Nro.
                                            </th>
                                            
                                            <th>
                                                Nro. Factura
                                            </th>
                                            <th>
                                                Nro. Nota Venta
                                            </th>
                                            <th>
                                               Nombre Cliente
                                            </th>
                                            <th>
                                                NIT
                                            </th>
                                            <th>
                                               Fecha
                                            </th>

                                            <th>
                                               Monto Total
                                            </th>
                                            <th>

                                            </th>
                                    
                                        </tr>
                                        
                                    </thead>
                                    <tbody>
                                        <?php
                                        session_start();
                                        $sesion=$_SESSION['userMaster'];
                                        require_once '../datos/Database.php';
                                        $consulta = "SELECT* FROM Cliente cl, Ventas vt, VentaSucursal vs, Factura vf, Transaccion tr WHERE vt.idCliente = cl.idCliente AND tr.estado = '1' AND tr.tipoTrans='1' AND vt.idTransaccion = tr.idTransaccion AND vt.idVenta = vs.idVenta AND vt.idVenta = vf.idVenta AND vs.idSucursal = '".$sesion['idSucursal']."' AND vf.nroFactura = '$nFactura' ORDER BY vt.idVenta DESC";

                                        try {
                                            $item=0;
                                            $comando = Database::getInstance()->getDb()->prepare($consulta);
                                            $comando->execute();
                                            while (($row = $comando->fetch())  ){
                                                  $item=$item+1;
                                                   $idVenta=$row['idVenta'];
                                                ?>

                                                <tr>
                                                    <td>
                                                    <?= $item?>
                                                    </td>
                                                    
                                                    <td>
                                                        <?php echo $row['nroFactura'];?>
                                                    </td>
                                                   
                                                    <td>
                                                        <?= $row['nroVenta'] ?>
                                                    </td>

                                                    <td>
                                                        <?= $row['nombreCliente'] ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['nit'] ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['fecha'] ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['montoTotal'] ?>
                                                    </td>
                                                    <td>
                                                        <a type="submit" class="btn btn-default btn-xs blue" href="nota_de_venta.php?id=<?= $idVenta ?>"><i class="fa fa-eye"></i> Ver </a>
                                                   
                                                        

                                                    </td>

                                                </tr>
                                                <?php
                                            }//terminacion del while
                                        } catch (PDOException $e) {

                                            echo 'Error: ' . $e;
                                        }
                                        ?>
                                    </tbody>
                                </table>