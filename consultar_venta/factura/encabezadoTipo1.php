<script language="JavaScript">
    $(document).ready(function () {
        doPrint();
    });
    function doPrint() {
        //document.all.item("mostrarUser").style.visibility='visible'; 
        window.print()();
        //document.all.item("mostrarUser").style.visibility='hidden';
    }
    function volver(dato) {
        location.href = "../index.php";
    }
</script>
<?php


?>
<div id="noprint">
    <table>
        <tr>
            <td>
                <button type="button" name="volver" id="volver" onclick="volver()">Volver</button>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>
                <button type="button" onclick="doPrint()">Imprimir</button>
            </td>
        </tr>
    </table>
</div>

<div style="width: 100%;height: 16%; /*border: 1px solid red">
    <div style="float: left; width: 35%; /*border: 1px solid green">
    <!-- style="width: auto; height: 30%;padding-bottom: 11%;padding-left: 8%; /*border: 1px solid darkmagenta"-->

            <div >
            <img width="180" height="60" src="../../assets/img/sc.png"/>
        </div>
        <table cellpadding="1" style="font-size: 8pt">
            <tr>
                <td>
                    <strong><?php echo mb_strtoupper($nombSucur, 'utf-8') ?></strong>
                </td>
            <tr>
                <td><?= $sucursal['direccion'] ?></td>
            </tr>
            <tr>
                <td><strong>Telf.:</strong><?= $SucurTelef ?></td>
            </tr>
            <tr>
                <td><strong><?= $sucurLugar ?> SFC-1</strong></td>
            </tr>
        </table>
    </div>
    <div style="text-align: center; padding-top: 2%; /*border: 1px solid black;*/height: 80%;  float: left; width: 29%">
        <br><br><br><br><br><br><br>
            <label style="font-family: Arial; font-size: 12pt; color: red;"><strong>FACTURA</strong></label><BR>
    </div>
    <div style="float: right; padding-left: 1%; width: 34%;/*border: 1px solid blue">
        <table cellpadding="3" cellspacing="4"
               style="font-size: 9pt; float: right; width: 75%; text-align: right; border:1.5px solid black; border-radius: 6px; margin-bottom: 1%">
            <tbody>
            <tr>
                <td align="left">
                    <label for="user_name"><strong>NIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong></label>
                </td>
                <td align="center">
                    <label for="user_name"
                           style="font-size: 12pt"><strong><?= $nitEmpr ?></strong></label>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <label for="user_name"><strong>Nº FACTURA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong></label>
                </td>
                <td align="center">
                    <label for="user_name"><?= $nroFac ?></label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <strong><label for="user_name">AUTORIZACION: </label></strong>
                </td>
                <td align="right">
                    <label for="user_name"><?= $autorizacion ?></label>
                </td>
            </tr>
            </tbody>
        </table>
        <table>
            <tr>
                <td style="color: red;text-align: center">
                    COPIA
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt">
                    <?= $actividad ?>
                </td>
            </tr>
        </table>
    </div>
</div>