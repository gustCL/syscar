<?php
require_once '../../datos/Database.php';

//EMPRESA
$consulta = 'SELECT * FROM Empresa';
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$empresa = $comando->fetch();
//SUCURSAL
session_start();
$inicio = $_SESSION['idSesion'];

$userMaster = $_SESSION['userMaster'];
$consulta1 = "SELECT * FROM Sucursal WHERE idSucursal =?";
$comando1 = Database::getInstance()->getDb()->prepare($consulta1);
$comando1->execute([$userMaster['idSucursal']]);
$sucursal = $comando1->fetch();

//USUARIO
$userMaster = $_SESSION['userMaster'];
$consulta5 = "select u.idUsuario, u.nombre, u.apellido from Usuario u , InicioSesion i, Sucursal s where u.idUsuario = i.usuario and i.sucursal = s.idSucursal and s.idSucursal =? and i.idInicio = ?";
$comando5 = Database::getInstance()->getDb()->prepare($consulta5);
$comando5->execute([$userMaster['idSucursal'], $_SESSION['idSesion']]);
$usuario = $comando5->fetch();

//OBTENEMOS EL ID DE LA TERMINAL DE INICIOTERMINAL
$con = "Select idTerminal from InicioTerminal where idInicio = ? ";
$com = Database::getInstance()->getDb()->prepare($con);
$com->execute([$_SESSION['idSesion']]);
$terminal = $com->fetch();

//OBTENEMOS DATOS DE LA DOSIFICACION
$consulta4 = "SELECT * FROM Dosificacion WHERE idSucursal =? and idTerminal =? and estado = 1";
$comando4 = Database::getInstance()->getDb()->prepare($consulta4);
$comando4->execute([$userMaster['idSucursal'], $terminal['idTerminal']]);
$dosificacion = $comando4->fetch();

//OBTENEMOS DATOS DE LA TRANSACCION
$nventa = $_REQUEST['vt'];
$consulta2 = "SELECT * FROM Ventas WHERE idVenta =?";
$comando2 = Database::getInstance()->getDb()->prepare($consulta2);
$comando2->execute([$nventa]);
$venta = $comando2->fetch();

$consu = "SELECT * FROM Transaccion WHERE idTransaccion =?";
$com = Database::getInstance()->getDb()->prepare($consu);
$com->execute([$venta['idTransaccion']]);
$trans = $com->fetch();

//OBTENEMOS DATOS DEL CLIENTE
$consulta3 = "SELECT * FROM Cliente WHERE idCliente =?";
$comando3 = Database::getInstance()->getDb()->prepare($consulta3);
$comando3->execute([$venta['idCliente']]);
$cliente = $comando3->fetch();

//////// EXTRAER EL NUMERO DE FACTURA SIGUIENTE /////
$sql = "select count(*)cant
from InicioSesion i, InicioTerminal it, Terminal t, Factura f, Dosificacion d
where i.idInicio = it.idInicio and it.idTerminal = t.idTerminal and t.idTerminal = f.idTerminal and f.idDosifi = d.idDosifi and d.estado = 1 and d.idTerminal = ? and d.idSucursal = ?";
$com = Database::getInstance()->getDb()->prepare($sql);
$com->execute([$terminal['idTerminal'], $userMaster['idSucursal']]);
$dato = $com->fetch();
$cant = $dato['cant'];

$idSuc = $sucursal['idSucursal'];
$idDos = $dosificacion['idDosifi'];
if ($cant == 0) {
    $nroFac = $dosificacion['nroFacturaInicial'];  ///numero inicial qeu se registro en la dosificacion
} else {
    $sql = "select max(f.nroFactura)nro
    from InicioSesion i, InicioTerminal it, Terminal t, Factura f, Dosificacion d
    where i.idInicio = it.idInicio and it.idTerminal = t.idTerminal and t.idTerminal = f.idTerminal and f.idDosifi = d.idDosifi and d.estado = 1 and f.idTerminal = ? and f.idSucursal = ?";
    $com = Database::getInstance()->getDb()->prepare($sql);
    $com->execute([$terminal['idTerminal'], $userMaster['idSucursal']]);
    $nro = $com->fetch();
    $nroFac = $nro['nro'];
    $nroFac++;
}
///////////// EXTRAER LOS PARAMETROS DE LA FACTURA //////////////

$nombEmpr = $empresa['nombreEmpresa'];
$nitEmpr = $empresa['nit'];
$autorizacion = $dosificacion['nroAutorizacion'];
$llave = $dosificacion['llaveDosificacion'];
$nombSucur = $sucursal['nombreSucur'];
$sucurLugar = $sucursal['ciudad'];
$SucurTelef = $sucursal['telefono'];
$ventaMonto = $venta['montoTotal'];
$FechaT = $trans['fecha'];
$HoraTransac = $trans['hora'];
$nombCliente = $cliente['nombreCliente'];
$nitCliente = $cliente['nit'];
$idCliente= $cliente['idCliente'];
$fechaLimite = $dosificacion['fechaLimite'];
$UsuarioNombre = $usuario['nombre'];
$usuarioApellido = $usuario['apellido'];

// QUITANDO LOS SEPARADORES DE LA FECHA DE TRANSACCION ///////////
$D = substr($FechaT, 0, 2);
$M = substr($FechaT, 3, 2);
$Y = substr($FechaT, 6);
$FechaTransac = $D . $M . $Y;

/////////////////////////////////////////////////////////////////
require_once 'CodigoControl.class.php';

$PNG_TEMP_DIR = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;

//html PNG location prefix
$PNG_WEB_DIR = 'temp/';

include "qrlib.php";
//ofcourse we need rights to create temp dir
if (!file_exists($PNG_TEMP_DIR))
    mkdir($PNG_TEMP_DIR);
$errorCorrectionLevel = 'L';
if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L', 'M', 'Q', 'H')))
    $errorCorrectionLevel = $_REQUEST['level'];

$matrixPointSize = 3;
if (isset($_REQUEST['size']))
    $matrixPointSize = min(max((int) $_REQUEST['size'], 1), 10);
//$query=$cons->cons_cond("ca_diariofac","numfac='$id_diario'",'cod_fac');
//$llave='F#I-gUHCtC*B5=7MM8=*#HD@6*(bEQ*FB6BEg3jQ)jpKB@%C)RE=XZnFpTDk-$x+';
$fec_lim = $dosificacion['fechaLimite']; //$cons->cons_simple('par_dos',"estado='1'",'fecha_limite');
$tipo_impre = 1;
?>
<style type="text/css">
    table {
        color: #000000;
        font-size: 8pt; }
    </style>
    <html> 
        <head> 

            <link rel="stylesheet" type="text/css" href="../../css/print/stilo.css" media="screen">   
            <link href="../printListaTransac.css" rel="stylesheet" type="text/css" media="print" />      
            <link href="../../css/font-awesome.css" rel="stylesheet"  media="all">
    <!--        <script language="javascript" type="text/javascript" src="jreport_inventario.js"></script>-->
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        </head> 
        <body> 
            <script language="JavaScript">
                window.print();
                //parent.print();
                $(document).ready(function() {
                    doPrint();
                });
                function doPrint() {
                    //document.all.item("mostrarUser").style.visibility='visible'; 
                    window.print();
                    //document.all.item("mostrarUser").style.visibility='hidden';
                }
                function volver() {
                    location.href = "../../nueva_venta";
                }
            </script>
            <div id="leteral"></div>
        <div id="hoja" style="height: auto; width: auto">
            <table align="center">
                <tr>
                    <td align="center"><strong> <?= $nombEmpr ?></strong> </td>
                </tr>
                <tr>
                    <td align="center"><strong> Sucursal:</strong> <?= $nombSucur ?> </td>
                </tr>
                <tr>
                    <td align="center"><strong>Telefono:</strong> <?= $SucurTelef ?> </td>
                </tr>
                <tr>
                    <td align="center"> Santa Cruz-Bolivia </td>
                </tr>
                <tr>
                    <td><div align="center">F A C T U R A<br>O R I G I N A L</div></td>
                </tr>
                <tr>
                    <td><div align="center">-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -</div></td>
                </tr>
                <tr>
                    <td align="center">NIT: <?php echo $nitEmpr; ?></td>
                </tr>
                <tr>
                    <td align="center">FACTURA No. 0000000000<?php echo $nroFac; ?></td>
                </tr>
                <tr>
                    <td align="center">AUTORIZACION No. <?php echo $autorizacion; ?></td>
                </tr>
                <tr>
                    <td>-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -</td>
                </tr>
                <tr>
                    <td align="center">Fecha: <?= $FechaT ?></td>
                </tr>
                <tr>
                    <td align="center">Nombre: <?php echo $nombCliente; ?></td>
                </tr>
                <tr>
                    <td align="center">NIT/CI: <?php echo $nitCliente; ?></td>
                </tr>
                <tr>
                    <td>-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -</td>
                </tr>
            </table>
            <table align="center" width="180">
                <tr>
                    <td align="center"><strong>Nro</strong></td>
                    <td><strong>   Detalle</strong></td>
                    <td align="center"><strong>Cant.</strong></td>
                    <td align="center"><strong>P.Uni.</strong></td>
                    <td align="center"><strong>Monto</strong></td>
                </tr>
                <?php
                /* ///CANTIDAD DE DATOS DE ESTA FACTURA ///////
                  $sql = "SELECT COUNT(d.idTransaccion)cant FROM DetalleTransaccion d, Transaccion t, ventas v WHERE d.idTransaccion= t.idTransaccion  and t.idTransaccion = v.idTransaccion and t.estado = 1 and v.idVenta =?";
                  $com1 = Database::getInstance()->getDb()->prepare($sql);
                  $com1->execute([$venta['idVenta']]);
                  $CantDetalle = $com1->fetch(); */

// EXTRAEMOS EL DETALLE DE LA VENTA ///
                $sql2 = "SELECT v.idVenta, p.codigoProducto, p.nombreComercial, sum(d.cantidad)cantidad, d.precioVenta, v.montoSubTotal, v.montoTotal, v.descuento, v.efectivoSus, v.efectivoBs, v.cambio "
                        . " FROM DetalleTransaccion d, Transaccion t, Ventas v, Producto p "
                        . "WHERE d.idTransaccion= t.idTransaccion  and t.idTransaccion = v.idTransaccion and d.codigoProducto = p.codigoProducto "
                        . "and t.estado = 1 and v.idVenta =? group by p.codigoProducto";
                $com2 = Database::getInstance()->getDb()->prepare($sql2);
                $com2->execute([$venta['idVenta']]);

                $num = 1;
                while ($detalle = $com2->fetch()) {
                    $cantidad = $detalle['cantidad'];
                    $descripcion = $detalle['nombreComercial'];
                    $precio = $detalle['precioVenta'];
                    $subTotal = $cantidad * $precio;
                    $pagobs = $detalle['efectivoBs'];
                    $pagosus = $detalle['efectivoSus'];
                    $cambio = $detalle['cambio'];
                    $montot = $detalle['montoTotal'];
                    $submonto = $detalle['montoSubTotal'];
                    $descuento = $detalle['descuento'];
                    $trecepor = $montot * 0.13;
                    ?>
                    <tr>
                        <td align="center"><?= $num ?></td>
                        <td><?= $descripcion ?></td>
                        <td align="center"><?= $cantidad ?></td>
                        <td align="center"><?= $precio ?></td>
                        <td align="center"><div align="right"><?= number_format($subTotal, 2); ?></div></td>
                    </tr>
                    <?php
                    
                    $num++;
                }
                ?>
                <br>
                <tr>
                    <td><br></td>
                </tr>
                <tr>
                    <td colspan="1" class="invisible"></td>
                    <td colspan="2" align="left"><strong>Sub Total:</strong></td>
                    <td colspan="2" align="right"><strong><?= $submonto ?></strong></td>
                </tr>
                <tr>
                    <td colspan="1" class="invisible"></td>
                    <td colspan="2" align="left"><strong>Descuento:</strong></td>
                    <td colspan="2" align="right"><?= $descuento ?></td>
                </tr>
                <tr>
                    <td colspan="1" class="invisible"></td>
                    <td colspan="2" align="left"><strong>Total a pagar:</strong></td>
                    <td colspan="2" align="right"><strong><?php $montoOrigi = number_format($montot, 2);
                echo $montoOrigi;
                ?></strong><input type="hidden" name="numfac" id="numfactu" value="<?php echo $nroFac; ?>"></div></td>
                </tr>
            </table>
            <br>
            <table align="center">
                <tr>
                    <td>
                        Son: <?php
                        require_once 'convertidor.php';
                        $v = new EnLetras();
                        $valor = $v->ValorEnLetras($montot, "Bolivianos");
                        echo $valor;
                        ?>
                        <br>                       
                    </td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
                <tr>
                    <td><center><strong>Pago Bs:</strong></center></td>
                    <td><center><strong><?= $pagobs ?></strong></center></td>
                </tr>
                <tr>
                    <td align="center"><strong>Pago $us:</strong></td>
                    <td align="right"><strong><?= $pagosus ?></strong></td>
                </tr>
                <tr>
                    <td align="center"><strong>Cambio:</strong></td>
                    <td align="right"><strong><?= $cambio ?></strong></td>
                </tr>
                <tr>
                    <td>
                        Cajero(a): <?= $UsuarioNombre . " " . $usuarioApellido ?>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        Hora: <?php echo $HoraTransac ?>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        T. de Pago: Efectivo
                        <br>
                    </td>
                </tr>
                <tr>
                    <?php
                    require_once('CodigoControl.class.php');
                    $montoEntero = explode(".", $montot); // DIVIDO EL MONTO A PAGAR PARA EXTRAER LA PARTE ENTERA Y LA PARTE DECIMAL
                    $entero = $montoEntero[0];
                    $decimal = @$montoEntero[1]; // CONCATENO LA PARTE ENTERA Y DECIMAL PARA QUE SE UN SOLO NUMERO Y PUEDA MANDARLO SIN PROBLEMAS AL GENERADOR DE CODIGO DE CONTROL
                    if ($decimal >= 50){
                        $entero = $entero +1;
                    }
                    $CodigoControl = new CodigoControl(
                            $autorizacion, $nroFac, $nitCliente, $FechaTransac, $entero, $llave
                    );
                    $cod_con = $CodigoControl->generar();

                    $filename = $PNG_TEMP_DIR . 'test' . $FechaTransac . $nroFac . '.png';
                    $datos = $nitCliente . "|" . $autorizacion . "|" . $nroFac . "|" . $FechaTransac . "|" . $montot . "|" . $cod_con . "|" . $nombCliente;
//QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
                    QRcode::png($datos, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
                    ?>
                    <td align="left">
                        Codigo de Control: <?= $cod_con ?>
                        <input type="hidden" name="ver" id="ver" value="<?php //echo $valor;         ?>">
                    </td>
                </tr>
                <tr>
                    <td>
                        Fecha limite de Emision: <?= $fechaLimite ?>
                    </td>
                </tr>
            </table>
            <table width="180" align="center">
                <tr>
                    <td><?php echo '<img src="' . $PNG_WEB_DIR . basename($filename) . '" /><br>'; ?></td>
                </tr>
                <tr>
                    <td>ESTA FACTURA CONTRIBUYE AL DESARROLLO DEL PAÍS. EL USO ILÍCITO DE ÉSTA SERÁ SANCIONADO<br> DE ACUERDO A LEY</td>
                </tr>
            </table>
            <?php
            $sql = "insert into Factura (idVenta,nroFactura,idDosifi,montoTotal,impICE,impExce,impNeto,impuestos,codigoControl,idSucursal,idTerminal,fecha, idCliente, estado) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $com = Database::getInstance()->getDb()->prepare($sql);
            $com->execute([$venta['idVenta'], $nroFac, $dosificacion['idDosifi'], $montot, 0, 0, $montot, $trecepor, $cod_con, $idSuc, $terminal['idTerminal'],$FechaT,$idCliente, 'V']);
            // ACTUALIZAMOS EL TIPO DE TRANSACCION EN TRANSACCION
            $sql1 = "update Transaccion set tipoTrans = 2 where idTransaccion = ?";
            $com = Database::getInstance()->getDb()->prepare($sql1);
            $com->execute([$venta['idTransaccion']]);
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //INSERTAR DATOS IngresoCaja
            $con1 = "select * from VentaSucursal where idVenta = ?";
            $com1 = Database::getInstance()->getDb()->prepare($con1);
            $com1->execute([$venta['idVenta']]);
            $nroVenta = $com1->fetch();
            /////////
            $consultaIC = "INSERT INTO IngresoCaja(idIngresoCaja, idInicio, idTipoIngresoCaja, nroFactura, nroVenta, glosa, montoTotal, idTipoCambio, tipoCambio, efectivoBs, efectivoSus, fechaRegistro, horaRegistro, estado) VALUES ('',?,1,?,?,?,?,?,?,?,?,?,?,?)";
            $comandoIC = Database::getInstance()->getDb()->prepare($consultaIC);
            $comandoIC->execute([$inicio,$nroFac, $nroVenta['nroVenta'], 'Venta de productos', $montot, $venta['idTipoCambio'], $venta['tipoCambio'], $venta['efectivoBs'], $venta['efectivoSus'],$FechaT , $HoraTransac, 1]);
            //selecionar el id de IngresoCaja
            $consultaICs = "SELECT * FROM IngresoCaja WHERE nroVenta='".$nroVenta['nroVenta']."' AND estado=1 AND fechaRegistro='" . $FechaT . "' AND horaRegistro='" . $HoraTransac . "'";
            $comandoICs = Database::getInstance()->getDb()->prepare($consultaICs);
            $comandoICs->execute();
            $idIngresoCaja = $comandoICs->fetch(PDO::FETCH_ASSOC);
            //INSERTAR DATOS IngresoVenta
            $consultaIV = "INSERT INTO IngresoVenta (idIngresoCaja, idVenta) VALUES (?,?)";
            $comandoIV = Database::getInstance()->getDb()->prepare($consultaIV);
            $comandoIV->execute([$idIngresoCaja['idIngresoCaja'], $venta['idVenta']]);
            ?>
        </div>
        <div id="noprint" style="height: auto; width: auto" >
            <div align="center">
                <table>
                    <tr>
                        <td>
                            <button type="button" name="volver" id="volver" onClick="volver()">Volver</button>
                        </td>
                        <td>&nbsp;&nbsp;</td>
                        <td>
                            <button name="imprimir" id="imprimir" onClick="doPrint()">Imprimir</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </body>
</html>
