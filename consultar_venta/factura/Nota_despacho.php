﻿<?php

$fec_lim = $dosificacion['fechaLimite']; //$cons->cons_simple('par_dos',"estado='1'",'fecha_limite');
?>
<?php
require 'encabezadoNotaDespacho.php';
?>
<table id="tablaD" cellpadding="1" style=" width: 100%; border: 1px solid black; border-radius: 6px">
    <tr>
        <td>
            <strong>Cliente: </strong> <?= $nombCliente ?>
        </td>
        </tr>
    <tr>
        <td>
            <strong>Destino: </strong> <?= $destino ?>
        </td>
    </tr>
</table>
<br>
<div heigth="50">
    <table width="100%" cellspacing="0" cellpadding="1">
        <thead>
        <th style="border-left:1px solid black; border-bottom:1px solid black;border-top:1px solid black;">
            CANTIDAD
        </th>
        <th style="border-left:1px solid black;border-bottom:1px solid black;border-top:1px solid black;">
            CONCEPTO
        </th>
        <th style="width:15%; border-top: 1px solid black;border-bottom: 1px solid black;border-left :1px solid black;">
            PRECIO UNITARIO
        </th>
        <th style="width:15%; border-top: 1px solid black;border-bottom: 1px solid black;border-left :1px solid black;border-right:1px solid black;">
            SUBTOTAL
        </th>
        </thead>
        <tbody>
        <?php
        // EXTRAEMOS EL DETALLE DE LA VENTA ///
        $sql2 = "SELECT v.idVenta, p.codigoProducto, p.nombreComercial, sum(d.cantidad)cantidad, d.precioVenta, v.montoSubTotal, v.montoTotal, v.descuento, v.efectivoSus, v.efectivoBs, v.cambio "
            . " FROM DetalleTransaccion d, Transaccion t, Ventas v, Producto p "
            . "WHERE d.idTransaccion= t.idTransaccion  and t.idTransaccion = v.idTransaccion and d.codigoProducto = p.codigoProducto "
            . "and t.estado = 1 and v.idVenta =? group by p.codigoProducto";
        $com2 = Database::getInstance()->getDb()->prepare($sql2);
        $com2->execute([$venta['idVenta']]);

        $num = 1;
        while ($detalle = $com2->fetch()) {
            $cantidad = $detalle['cantidad'];
            $descripcion = $detalle['nombreComercial'];
            $precio = $detalle['precioVenta'];
            $subTotal = $cantidad * $precio;
            $pagobs = $detalle['efectivoBs'];
            $pagosus = $detalle['efectivoSus'];
            $cambio = $detalle['cambio'];
            $montot = $detalle['montoTotal'];
            $submonto = $detalle['montoSubTotal'];
            $descuento = $detalle['descuento'];
            if ($cantidad == 0) {
                ?>
                <tr>
                    <td style="font-size: 12px; border-left:1px solid black"></td>
                    <td style="font-size: 12px; border-left:1px solid black; padding-left: 1%">
                        <?= $descripcion ?>
                    </td>
                    <td style="border-left:1px solid black; text-align: center"></td>
                    <td style="border-right :1px solid black;border-left:1px solid black;text-align: center"></td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td style="font-size: 12px; border-left:1px solid black;text-align: center"><?= $cantidad ?></td>
                    <td style="font-size: 12px; border-left:1px solid black;padding-left: 1%">
                        <?= $descripcion ?>
                    </td>
                    <td style="border-left:1px solid black; text-align: center">
                        <?php echo number_format($precio, 2);
                        ?>
                    </td>
                    <td style="border-left:1px solid black;border-right:1px solid black;text-align: center">
                        <?php echo number_format($precio * $cantidad, 2);
                        ?>
                    </td>
                </tr>
                <?php
            }
            $monto = $montot;
            $num++;
        }
        $iva = $monto * 0.13;
        $dato = 6;
        if ($num == 1) {
            ImprimirEspacios($dato);
        } else {
            if ($num > 1) {
                $dato = $dato - $num;
                ImprimirEspacios($dato);
            }
        }

        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"
                style="border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;">
                <strong>Son:</strong> <?php
                echo $valor . " Bolivianos";
                ?>
            </td>
            <td style="text-align: right; border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black">
                <strong>TOTAL Bs.:</strong></td>
            <td style="text-align: center;border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;border-right:1px solid black;"><?= number_format($montot, 2) ?></td>
        </tr>
        </tfoot>
    </table>
</div>
<br>
<br>
<br>
<div style="width:65%; float:left">
    <table>
        <tr>
            <td>
                <center>
                <br>
                <labe>_______________________________</labe><br>
                 <i>ENTREGADO POR</i></center>
            </td>
            <td style="padding-left: 20%">
                <center>
                    <br>
                    <labe>_______________________________</labe><br>
                    <i>RECIBIDO POR EL CLIENTE Y/O ENCARGADO</i></center>
            </td>
        </tr>
        <tr>
            <td>
                <label>Nombre: .................................................................</label>
            </td>
            <td style="padding-left: 20%">
                <label>Nombre: .................................................................</label>
            </td>
        </tr>
        <tr>
            <td>
                <label>CI: ........................</label>
            </td>
            <td style="padding-left: 20%">
                <label>CI: ........................</label>
            </td>
        </tr>
    </table>
</div>
<br><br>
<div style="float: left; padding-top: 2%;padding-left: 12%">
    <i><strong>OBSERVACION: !UNA VEZ FIRMADO ESTE DOCUMENTO NO EXISTE NINGUN RECLAMO POSTERIOR.!</strong></i>
</div>

