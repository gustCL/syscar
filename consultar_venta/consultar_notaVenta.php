<?php
require_once '../datos/Database.php';

$actual = date("Y-m-d");
$inicioMes = date("Y-m");
$inicioMes = $inicioMes . "-01";
//EMPRESA
$consulta='SELECT * FROM Empresa';
$comando=Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$empresa=$comando->fetch();
//SUCURSAL
 session_start();
$userMaster =$_SESSION['userMaster'];
$consulta1="SELECT * FROM Sucursal WHERE idSucursal =?";
$comando1=Database::getInstance()->getDb()->prepare($consulta1);
$comando1->execute([$userMaster['idSucursal']]);
$sucursal=$comando1->fetch();
//OBTENEMOS DATOS DE LA TRANSACCION

$nventa = $_REQUEST['vt'];
$consulta2="SELECT * FROM Ventas WHERE idVenta =?";
$comando2=Database::getInstance()->getDb()->prepare($consulta2);
$comando2->execute([$nventa]);
$venta=$comando2->fetch();     
//OBTENEMOS DATOS DE LA NOTA DE VENTA
$consulta4="SELECT * FROM VentaSucursal WHERE idVenta =?";
$comando4=Database::getInstance()->getDb()->prepare($consulta4);
$comando4->execute([$nventa]);
$notaVenta=$comando4->fetch(); 
//OBTENEMOS DATOS DEL CLIENTE
$consulta3="SELECT * FROM Cliente WHERE idCliente =?";
$comando3=Database::getInstance()->getDb()->prepare($consulta3);
$comando3->execute([$venta['idCliente']]);
$cliente=$comando3->fetch();

$idTransaccion=$venta['idTransaccion'];

$consulta5="SELECT Distinct( dt.codigoProducto) AS codigoProducto, pr.nombreComercial FROM DetalleTransaccion dt, Producto pr WHERE dt.idTransaccion =? AND dt.codigoProducto = pr.codigoProducto";
$comando5=Database::getInstance()->getDb()->prepare($consulta5);
$comando5->execute([$idTransaccion]);

$consulta6="SELECT * FROM Transaccion WHERE idTransaccion =? ";
$comando6=Database::getInstance()->getDb()->prepare($consulta6);
$comando6->execute([$idTransaccion]);
$trans=$comando6->fetch();
?> 
<style type="text/css">
  label {
    color: #000000;
    font-size: 7pt; }
  </style>
<html> 
    <head> 
      
        <link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen">   
        <link href="printListaTransac.css" rel="stylesheet" type="text/css" media="print" />      
        <link href="../css/font-awesome.css" rel="stylesheet"  media="all">
        <script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
<!--        <script language="javascript" type="text/javascript" src="jreport_inventario.js"></script>-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head> 
    <body> 
        <script language="JavaScript">
            window.print();
            //parent.print();
            $(document).ready(function () {
                doPrint();
            });
            function doPrint() {
                //document.all.item("mostrarUser").style.visibility='visible'; 
              window.print();
                //document.all.item("mostrarUser").style.visibility='hidden';
            }
            function volver() {
                location.href = "../consultar_venta";
            }
        </script>
        <div id="leteral"></div>
        <div id="hoja" style="height: auto; width: auto"> 
            
            <div align="center"><label id="titulo"><?= $empresa['nombreEmpresa']?></label></div>
            <div align="center"><label ><?= $sucursal['nombreSucur']?></label></div>
            <div align="center"><label >Telf. <?= $sucursal['telefono']?></label></div>
            <div align="center"><label ><?= $sucursal['direccion']?></label></div>
            <div align="center"><label ><?= $sucursal['ciudad']?></label></div>
            <div align="center"><label>------------------------------------------------------------------------------</label></div>
            <div align="center"><label >NOTA DE VENTA</label></div>
            <div align="center"><label>------------------------------------------------------------------------------</label></div>
            <div align="center"><label >NIT/CI: <?= $empresa['nit']?></label><br/><label >Nro Venta: <?= $notaVenta['nroVenta']?></label></div>
            <div align="center"><label>------------------------------------------------------------------------------</label></div>
            <div align="center">
            <table>
                <tr>
                    <td> <label>Fecha: <?= $trans['fecha']?></label><label > Hora: <?= $trans['hora']?></label></td>
                    
                    
                </tr>
                <tr>
                    <td><label>Cliente: <?= $cliente['nombreCliente']?></label></td>
                </tr>
                <tr>
                   <td><label>NIT/CI: <?= $cliente['nit']?></label></td> 
                </tr>
                
            </table>
                </div>
            <div align="center"><label>------------------------------------------------------------------------------</label></div>
            <!--<div align="center"><label >CANT.</label><label>&nbsp;&nbsp;&nbsp;&nbsp;</label><label>P.UNT.</label><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><label>DETALLE</label><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><label>MONTO</label></div>--> 
            <!--<div align="center"></div>-->
            <div align="center">
            <table>
                <thead>
                <tr>
                    <th>
                       <label>CANT.</label> 
                    </th>
                    <th>
                       <label>P.UNT.</label> 
                    </th>
                    <th>
                        <label>DETALLE</label>
                    </th>
                    <th>
                        <label>MONTO</label>
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
            <?php
                           
            while ($detalle=$comando5->fetch()){
                ////PARA NO REPETIR PRODUCTOS O MOSTRAR POR CANTIDAD DE LOTES
                $consulta8="SELECT dt.idTransaccion AS idTransaccion,dt.idExistencia AS idExistencia,dt.codigoProducto AS codigoProducto, SUM( dt.cantidad ) AS cant, (
                 dt.precioVenta * SUM( dt.cantidad )) AS monto, precioVenta
                 FROM DetalleTransaccion dt
                 WHERE dt.idTransaccion =? AND dt.codigoProducto=?";
                $comando8=Database::getInstance()->getDb()->prepare($consulta8);
                $comando8->execute([$idTransaccion, $detalle['codigoProducto']]);
                $detalle2=$comando8->fetch();
                $cant=$detalle2['cant'];
                $precioVenta=$detalle2['precioVenta'];
                $st=$detalle2['monto'];
                $descrip= substr($detalle['nombreComercial'], 0, 18);
            ?>
                    <tr>
                        <td>
                <center><label><?= $cant ?></label></center>  
                        </td>
                        <td>
                        <div align="right"><label><?= $precioVenta?></label></div>
                        </td>
                        <td>
                            <label>&nbsp;<?= $descrip?>&nbsp;</label> 
                        </td>
                        <td>
                            <div align="right"><label><?= $st?></label></div> 
                        </td>
                    </tr>
             <?php }?>
                </tbody>
            </table>
                </div>
            
            <!--            <div align="center"></label></div><br/> -->
            
            <br/>
            <div align="center">
            <table width="230">
                <tr>
                    <td width="98">
                    </td>
                    <td width="73">
                        <label >SubTotal:</label>
                    </td>
                    <td width="43">
                        <div align="right"> <label>&nbsp;&nbsp;<?= $venta['montoSubTotal']?></label></div>
                    </td>
                </tr>
                
                <tr>
                    <td>
                    </td>
                    <td>
                      <label>Descuento: </label>  
                    </td>
                    <td>
                        <div align="right"><label><?= $venta['descuento']?></label></div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                      <label>Total a pagar: Bs</label>
                    </td>
                    <td>
                      <div align="right"><label><?= $venta['montoTotal']?></label></div>
                    </td>
                </tr>
            </table>
            </div> 
            <?php 

class EnLetras 
{ 
  var $Void = ""; 
  var $SP = " "; 
  var $Dot = "."; 
  var $Zero = "0"; 
  var $Neg = "Menos"; 
   
function ValorEnLetras($x, $Moneda )  
{ 
    $s=""; 
    $Ent=""; 
    $Frc=""; 
    $Signo=""; 
         
    if(floatVal($x) < 0) 
     $Signo = $this->Neg . " "; 
    else 
     $Signo = ""; 
     
    if(intval(number_format($x,2,'.','') )!=$x) //<- averiguar si tiene decimales 
      $s = number_format($x,2,'.',''); 
    else 
      $s = number_format($x,2,'.',''); 
        
    $Pto = strpos($s, $this->Dot); 
         
    if ($Pto === false) 
    { 
      $Ent = $s; 
      $Frc = $this->Void; 
    } 
    else 
    { 
      $Ent = substr($s, 0, $Pto ); 
      $Frc =  substr($s, $Pto+1); 
    } 

    if($Ent == $this->Zero || $Ent == $this->Void) 
       $s = "Cero "; 
    elseif( strlen($Ent) > 7) 
    { 
       $s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
             "Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
    } 
    else 
    { 
      $s = $this->SubValLetra(intval($Ent)); 
    } 

    if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
       $s = $s . "de "; 

    $s = $s . $Moneda; 

    if($Frc != $this->Void) 
    { 
       $s = $s . " " . $Frc. "/100"; 
       //$s = $s . " " . $Frc . "/100"; 
    } 
    $letrass=$Signo . $s ; 
    return ($Signo . $s); 
    
} 


function SubValLetra($numero)  
{ 
    $Ptr=""; 
    $n=0; 
    $i=0; 
    $x =""; 
    $Rtn =""; 
    $Tem =""; 

    $x = trim("$numero"); 
    $n = strlen($x); 

    $Tem = $this->Void; 
    $i = $n; 
     
    while( $i > 0) 
    { 
       $Tem = $this->Parte(intval(substr($x, $n - $i, 1).  
                           str_repeat($this->Zero, $i - 1 ))); 
       If( $Tem != "Cero" ) 
          $Rtn .= $Tem . $this->SP; 
       $i = $i - 1; 
    } 

     
    //--------------------- GoSub FiltroMil ------------------------------ 
    $Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 
    while(1) 
    { 
       $Ptr = strpos($Rtn, "Mil ");        
       If(!($Ptr===false)) 
       { 
          If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 
            $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 
          Else 
           break; 
       } 
       else break; 
    } 

    //--------------------- GoSub FiltroCiento ------------------------------ 
    $Ptr = -1; 
    do{ 
       $Ptr = strpos($Rtn, "Cien ", $Ptr+1); 
       if(!($Ptr===false)) 
       { 
          $Tem = substr($Rtn, $Ptr + 5 ,1); 
          if( $Tem == "M" || $Tem == $this->Void) 
             ; 
          else           
             $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 
       } 
    }while(!($Ptr === false)); 

    //--------------------- FiltroEspeciales ------------------------------ 
    $Rtn=str_replace("Diez Un", "Once", $Rtn ); 
    $Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 
    $Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 
    $Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 
    $Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 
    $Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 
    $Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 
    $Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 
    $Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 
    $Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 
    $Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 
    $Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 
    $Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 
    $Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 
    $Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 
    $Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 
    $Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 
    $Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 

    //--------------------- FiltroUn ------------------------------ 
    If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 
    //--------------------- Adicionar Y ------------------------------ 
    for($i=65; $i<=88; $i++) 
    { 
      If($i != 77) 
         $Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 
    } 
    $Rtn=str_replace("*", "a" , $Rtn); 
    return($Rtn); 
} 


function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) 
{
  $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 
} 


function Parte($x) 
{ 
    $Rtn=''; 
    $t=''; 
    $i=''; 
    Do 
    { 
      switch($x) 
      { 
         Case 0:  $t = "Cero";break; 
         Case 1:  $t = "Un";break; 
         Case 2:  $t = "Dos";break; 
         Case 3:  $t = "Tres";break; 
         Case 4:  $t = "Cuatro";break; 
         Case 5:  $t = "Cinco";break; 
         Case 6:  $t = "Seis";break; 
         Case 7:  $t = "Siete";break; 
         Case 8:  $t = "Ocho";break; 
         Case 9:  $t = "Nueve";break; 
         Case 10: $t = "Diez";break; 
         Case 20: $t = "Veinte";break; 
         Case 30: $t = "Treinta";break; 
         Case 40: $t = "Cuarenta";break; 
         Case 50: $t = "Cincuenta";break; 
         Case 60: $t = "Sesenta";break; 
         Case 70: $t = "Setenta";break; 
         Case 80: $t = "Ochenta";break; 
         Case 90: $t = "Noventa";break; 
         Case 100: $t = "Cien";break; 
         Case 200: $t = "Doscientos";break; 
         Case 300: $t = "Trescientos";break; 
         Case 400: $t = "Cuatrocientos";break; 
         Case 500: $t = "Quinientos";break; 
         Case 600: $t = "Seiscientos";break; 
         Case 700: $t = "Setecientos";break; 
         Case 800: $t = "Ochocientos";break; 
         Case 900: $t = "Novecientos";break; 
         Case 1000: $t = "Mil";break; 
         Case 1000000: $t = "Millón";break; 
      } 

      If($t == $this->Void) 
      { 
        $i = $i + 1; 
        $x = $x / 1000; 
        If($x== 0) $i = 0; 
      } 
      else 
         break; 
            
    }while($i != 0); 
    
    $Rtn = $t; 
    Switch($i) 
    { 
       Case 0: $t = $this->Void;break; 
       Case 1: $t = " Mil";break; 
       Case 2: $t = " Millones";break; 
       Case 3: $t = " Billones";break; 
    } 
    return($Rtn . $t); 
} 

} 
 
 $V=new EnLetras(); 
 $con=strtoupper($V->ValorEnLetras($venta['montoTotal'],'Bolivianos')); 
    
?> 
            <div align="center"><label>Son:&nbsp;<?= $con?></label></div>
            <?php 
            $consulta7="SELECT * FROM Usuario WHERE idUsuario =? ";
            $comando7=Database::getInstance()->getDb()->prepare($consulta7);
            $comando7->execute([$userMaster['idUsuario']]);
            $cajero=$comando7->fetch();
            ?>
            <div align="center"><label>---------------------------------------------------------------------------</label></div>
            <div align="center"><label>Cajero(a): </label><label>&nbsp;&nbsp;<?= $cajero['nombre']?></label></div>
            <div align="center"><label>Pago Efectivo: Bs. </label><label>&nbsp;&nbsp;<?= $venta['efectivoBs']?></label></div>
            <div align="center"><label>Pago Efectivo: $us. </label><label>&nbsp;&nbsp;<?= $venta['efectivoSus']?></label></div> 
            <div align="center"><label>Cambio: Bs. </label><label>&nbsp;&nbsp;<?= $venta['cambio']?></label></div> 
            </div>
        <div id="noprint" style="height: auto; width: auto" >
            <div align="center">
            <table>
                <tr>
                    <td>
                        <button type="button" name="volver" id="volver" onClick="volver()">Volver</button>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <button name="imprimir" id="imprimir" onClick="doPrint()">Imprimir</button>
                    </td>
                </tr>
            </table>
            </div>
        </div>
        
    </body>
</html>
