<?php 
include "../datos/conexion.php";
include "../datos/Database.php";

include "../extras/PHPPaging.lib/PHPPaging.lib.php";
ini_set("date.timezone", "America/La_Paz");
$fechaI=$_REQUEST['fechaInicial'];
$fechaF=$_REQUEST['fechaFinal'];
$fechaInicial=$fechaI;
$fechaFinal=$fechaF;
$nro=0;
session_start();
$inicio = $_SESSION['idSesion'];
$userMaster = $_SESSION['userMaster'];
$consulta1 = "SELECT * FROM Sucursal WHERE idSucursal =?";
$comando1 = Database::getInstance()->getDb()->prepare($consulta1);
$comando1->execute([$userMaster['idSucursal']]);
$sucursal = $comando1->fetch();
$idSucursal = $sucursal['idSucursal'];

mysql_query("SET NAMES 'utf8'");

    $consulta ="SELECT * from LibroVentasIva WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinal' and idSucursal='$idSucursal'";

    //$consultaTotal="SELECT sum(ic.montoTotal) as total FROM IngresoCaja ic, TipoIngresoCaja tic, InicioSesion vs,Sucursal s WHERE ic.estado = 1 AND tic.estado= 1 AND ic.fechaRegistro>='".date_format($fechaInicial, 'd-m-Y')."' AND ic.fechaRegistro<='".date_format($fechaFinal, 'd-m-Y')."' AND tic.idTipoIngresoCaja=ic.idTipoIngresoCaja AND  ic.idInicio = vs.idInicio and vs.sucursal=s.idSucursal ";

    $consultaTotal="SELECT sum(montoTotal) as total FROM LibroVentasIva WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND idSucursal='$idSucursal'";


$comandoF=Database::getInstance()->getDb()->prepare($consulta);
$comandoF->execute();




$comando = mysql_query($consultaTotal);

$resTotal= mysql_fetch_array($comando);    

$montoTotal=$resTotal['total'];

$pagina = new PHPPaging;

?>

<br/> 

                                <table class="table table-striped">

                                    

                                    <thead>

                                        <!--Fila de titulos de columnas-->

                                        <tr>

                                            <th>

                                    <center><strong> Nro.</strong></center>

                                            </th>

                                            <th>

                                    <center>  <strong>Fecha</strong></center>

                                            </th>

                                            <th>

                                    <center>  <strong>Nro. Factura</strong></center>

                                            </th>

								    <th>

                                    <center>  <strong>Nombre del Cliente</strong></center>

                                            </th>			

                                            <th>

                                    <center> <strong>Monto (Bs.)</strong></center>

                                            </th>

                                            <th>

											<center> <strong>Re-imprimir</strong></center>

                                            </th>

											<th>

											<center> <strong>Anular</strong></center>

                                            </th>


                                        </tr>

                                    </thead>

<?php

$pagina->agregarConsulta($consulta);



  $pagina->modo('desarrollo'); 

  $pagina->verPost(true);

$pagina->porPagina(50);

  $pagina->paginasAntes(4);

  $pagina->paginasDespues(4);

  $pagina->linkSeparador(" - "); //Significa que no habrá separacion

  $pagina->div('div_listar');

  $pagina->linkSeparadorEspecial('...');   // Separador especial

  

$pagina->ejecutar();


if($comandoF->rowCount()>0) {
    while ($res = $pagina->fetchResultado()) {

        $nro++;

        $fecha = $res["fecha"];

        $nombre = $res["nombreCliente"];

        $nFac = $res["nroFactura"];

        $monto = $res['montoTotal'];

        $idV = $res['idVenta'];

        ?>

        <tbody>

        <tr>

            <td>
                <center><?php echo $nro ?></center>
            </td>

            <td>
                <center><?php echo $fecha; ?></center>
            </td>

            <td>
                <center><?php echo $nFac; ?></center>
            </td>

            <td>
                <center><?php echo $nombre ?></center>
            </td>

            <td>
                <center><?php echo $monto ?></center>
            </td>
            <?php
            if ($res['estado'] == 'V') {
                ?>
                <td>
                    <center>
                        <a href="factura/imprime_facturaCartaTipo1.php?vt=<?= $idV ?>" id="editar"><img width="30"
                                                                                                        height="30"
                                                                                                        src="../assets/img/imprimir.png"></a>
                    </center>
                </td>
                <td>
                    <center>
                        <a href="javascript: AnularFactura(<?= $nFac ?>,<?= $idV ?>)"><img width="30" height="30"
                                                                                           src="../assets/img/anular.png"></a>
                    </center>
                </td>
                <?php
            } else {
                ?>
                <td>
                </td>
                <td>
                </td>
                <?php
            }
            ?>
        </tr>

        </tbody>

        <?php

    }
}else{
    ?>
    La consulta no genero resultados.
    <?php
}

?>

<tfoot>

        <tr>

                                            <td colspan="3" invisible bg-snow> </td>

                                            <td align="right"><strong>Monto total (Bs.):</strong> </td>

                                            <td><center><?=  number_format($montoTotal,2)?></center></td>

											<td></td>

											<td></td>

                                        </tr>

            <td colspan="7">Numero de pagina: <?php echo $pagina->fetchNavegacion(); ?></td>                   

        </tr>

</tfoot>

</table>

