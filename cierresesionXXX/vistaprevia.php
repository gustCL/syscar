<script src="../js/jcierresesion.js"></script>

<?php
    require_once '../datos/Database.php';

    // Se inicializan las variables de sesión
    session_start();

    //
    $id_usuario = $_SESSION['userMaster']['idUsuario'];

    $consulta = "SELECT e.nombreEmpresa, s.direccion, s.telefono
                 FROM InicioSesion i, Sucursal s, Empresa e
                 WHERE i.sucursal=s.idSucursal and e.idEmpresa=s.idEmpresa and i.usuario=2";

    //where i.sucursal=s.idSucursal and e.idEmpresa=s.idEmpresa and i.usuario='$user'";
    $comando = Database::getInstance()->getDb()->prepare($consulta);
    try{
        $comando->execute();
        $row = $comando->fetch();
    } catch (Exception $e){
        echo 'Error: '.$e;
    }
?>

    <div class="page-content">

        <!-- /Page Header -->
        <div class="main-container container-fluid">
            <div class="page-body">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="widget" >

                                    <div class="widget-body">

                                        <div class="row">
                                            <div>
                                                <label>Nombre de La Empresa:</label>&nbsp;&nbsp;&nbsp;
                                                <label><?= $row['nombreEmpresa'] ?></label>
                                            </div>

                                            <div>
                                                <label>Direccion:</label>&nbsp;&nbsp;&nbsp;
                                                <label><?= $row['direccion'] ?></label>
                                            </div>

                                            <div>
                                                <label>Telefono:</label>&nbsp;&nbsp;&nbsp;
                                                <label><?= $row['telefono'] ?></label>
                                            </div>
                                            <div>
                                                Santa Cruz - Bolivia
                                            </div>
                                            <center>
                                                <div>
                                                    CIERRE DE TURNO
                                                </div>
                                                <div>
                                                    --------------------------------------------------------------------------------
                                                </div>
                                                <div>
                                                    Resumen de Ventas
                                                </div>
                                            </center>
                                        </div>
                                        <?php
                                        // Seleccionar el nombre de usuario
                                        $query = "SELECT nombre
                                                  FROM usuario
                                                  WHERE idUsuario=".$id_usuario;
                                        $comand_name = Database::getInstance()->getDb()->prepare($query);
                                        $comand_name->execute();
                                        $row_name = $comand_name->fetch();
                                        $nombre = $row_name['nombre'];
                                        // --------------------------------
                                        ?>
                                        <div>
                                            Usuario: <?= $nombre; ?>
                                        </div>
                                        <br>
                                        <div class="registros" id="agrega-registros">                    
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <!--Fila de titulos de columnas-->
                                                    <tr>
                                                        <th class="hidden-xs">
                                                            Nro. |
                                                        </th>
                                                        <th class="hidden-xs">
                                                            Nro. Venta |
                                                        </th>
                                                        <th class="hidden-xs">
                                                            Nro. de Factura
                                                        </th>
                                                        <th class="hidden-xs">
                                                            | ------ Fecha - Hora ------ |
                                                        </th>
                                                        <th class="hidden-xs" >
                                                            Monto Total
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                    // Obtener el último inicio de sesión del usuario actual
                                                    $query_inicio_sesion = "SELECT idInicio
                                                                            FROM iniciosesion
                                                                            WHERE usuario=".$id_usuario.
                                                        " ORDER BY idInicio DESC limit 1";
                                                    $comand_inicio_sesion = Database::getInstance()->getDb()->prepare($query_inicio_sesion);
                                                    $comand_inicio_sesion->execute();
                                                    $row_inicio_sesion = $comand_inicio_sesion->fetch();
                                                    $id_ultima_sesion = $row_inicio_sesion['idInicio'];
                                                    //------------------------------------------------------

                                                $consulta = "SELECT v.idVenta, u.nombre, t.fecha, t.hora, v.montoTotal
                                                             FROM ventas v, usuario u, transaccion t
                                                             WHERE t.idTransaccion=v.idTransaccion and t.tipoTrans=1 and
                                                                   u.idUsuario=t.idUsuario and u.idUsuario=".$id_usuario."
                                                                   and t.fecha=CURDATE() and t.idInicioSesion=".$id_ultima_sesion.
                                                                   " ORDER BY t.hora";

                                                    try {
                                                        $comando = Database::getInstance()->getDb()->prepare($consulta);
                                                        $comando->execute();
                                                        $montoTotal = 0;
                                                        $id_venta = 0;
                                                        $nro = 0;
                                                        while ($row = $comando->fetch()) {

                                                            $id_venta = $row['idVenta'];
                                                            $query = "SELECT f.nroFactura
                                                                      FROM   ventas v, factura f
                                                                      WHERE  v.idVenta=f.idVenta and v.idVenta=".$id_venta;

                                                            $cmd = Database::getInstance()->getDb()->prepare($query);
                                                            $cmd->execute();
                                                            $nro_factura = "(sin factura)";
                                                            while($result = $cmd->fetch()) {
                                                                $nro_factura = $result['nroFactura'];
                                                            }
                                                            $nro = $nro + 1;
                                                ?>
                                                            <tr>
                                                                <td>
                                                                    <?= $nro ?>
                                                                </td>
                                                                <td class="hidden-xs">
                                                                    <?= $row['idVenta'] ?>
                                                                </td>
                                                                <td class="hidden-xs">
                                                                    <?= $nro_factura ?>
                                                                </td>

                                                                <td class="hidden-xs">
                                                                    &nbsp;&nbsp;&nbsp; <?= $row['fecha'] . " - " . $row['hora'] ?>
                                                                </td>
                                                                <td>
                                                                    <?= $row['montoTotal'] ?>
                                                                    <?php
                                                                    $montoTotal = $montoTotal + $row['montoTotal'];
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                <?php

                                                        }//terminacion del while
                                                    } catch (PDOException $e) {
                                                        echo 'Error: ' . $e;
                                                    }
                                                ?>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td dir="rtl">
                                                    Monto total:
                                                </td>
                                                <td dir="rtl">&nbsp;&nbsp;&nbsp;
                                                    <?= $montoTotal ?>
                                                </td>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="registros-caja" id="agrega-registros-caja">   
                                            <center>
                                                <t1>
                                                    Entrega Caja
                                                </t1>
                                            </center>
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <!--Fila de titulos de columnas-->
                                                    <tr>
                                                        <th class="col-lg-2">
                                                            Monto |
                                                        </th>
                                                        <th>
                                                            Moneda |
                                                        </th>
                                                        <th class="col-lg-2">
                                                            Cantidad |
                                                        </th>
                                                        <th class="col-lg-2">
                                                            Total
                                                        </th>
                                                    </tr>
                                                </thead>
                                                    <!--Fila de cuerpo de tabla-->
                                                <tbody>
                                                <?php

                                                    // Obtener el ID del último cierre de sesión

                                                    // Para llenar la tabla de montos entregados, llenar la tabla con la siguiente consulta:
                                                    $consulta = "SELECT b.montoCambio, tm.abreviatura, ec.cantidad, (b.montoCambio*ec.cantidad) as 'total'
                                                                 FROM   billetes b, tipomoneda tm, entregacaja ec, cierresesion cs, usuario u
                                                                 WHERE  b.idCambio=ec.idBillete and b.tipoMoneda=tm.idMoneda and
                                                                        ec.idCierre=cs.idCierre and cs.idUsuario=u.idUsuario and
                                                                        /*cs.fecha=CURDATE() and */u.idUsuario=".$id_usuario." and ec.idCierre=".$id_ultima_sesion;
                                                    try {
                                                        $comando = Database::getInstance()->getDb()->prepare($consulta);
                                                        $comando->execute();
                                                        while ($row = $comando->fetch()) {

                                                ?>
                                                    <tr>
                                                        <td>
                                                            <?= $row['montoCambio']; ?>
                                                        </td>
                                                        <td><?= $row['abreviatura']; ?></td>
                                                        <td>
                                                            <?= /*$row['cantidad']*/ $id_ultima_sesion; ?>
                                                        </td>
                                                        <td>
                                                            <?= $row['total']; ?>
                                                        </td>
                                                    </tr>
                                                <?php
                                                        }
                                                    } catch(PDOException $e) {
                                                        echo 'Error: ' . $e;
                                                    }
                                                ?>

                                                </tbody>
                                            </table>


                                        </div>
                                        <center>
                                            <div class="buttons-preview"> 
                                                <a  href="javascript:imprimir();">
                                                    imprimir
                                                </a>
                                            </div>
                                        </center>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
//require_once '../header_footer/footer.php';
?>