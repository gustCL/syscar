var total = 0;

function validar_numero() {
    if (event.keyCode == 46) {
        return true;
    }
    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
    }
}

$(function() {
    $('#idocientos').on('keyup', function() {
        var dato = $('#idocientos').val();
        var result = parseInt(dato) * 200;
        $('#ldocientos').val(result);

        if ($('#idocientos').val().length < 1) {
            $('#ldocientos').val('0');
        }
        return false;
    });
});
$(function() {
//    var aux=total;
    $('#icien').on('keyup', function() {
        var dato = $('#icien').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 100;
            $('#lcien').val(result);
//            total=total+result;
//            $('#ltotal').val(total);
        }
        if ($('#icien').val().length < 1) {
            $('#lcien').val('0');
//             total=aux;
//            $('#ltotal').val(total);
        }
        return false;
    });
});
$(function() {
    $('#icincuenta').on('keyup', function() {
        var dato = $('#icincuenta').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 50;
            $('#lcincuenta').val(result);
        }
        if ($('#icincuenta').val().length < 1) {
            $('#lcincuenta').val('0');
        }
        return false;
    });
});
$(function() {
    $('#iveinte').on('keyup', function() {
        var dato = $('#iveinte').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 20;
            $('#lveinte').val(result);
        }
        if ($('#iveinte').val().length < 1) {
            $('#lveinte').val('0');
        }
        return false;
    });
});
$(function() {
    $('#idiez').on('keyup', function() {
        var dato = $('#idiez').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 10;
            $('#ldiez').val(result);
        }
        if ($('#idiez').val().length < 1) {
            $('#ldiez').val('0');
        }
        return false;
    });
});
$(function() {
    $('#icinco').on('keyup', function() {
        var dato = $('#icinco').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 5;
            $('#lcinco').val(result);
        }
        if ($('#icinco').val().length < 1) {
            $('#lcinco').val('0');
        }
        return false;
    });
});
$(function() {
    $('#idos').on('keyup', function() {
        var dato = $('#idos').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 2;
            $('#ldos').val(result);
        }
        if ($('#idos').val().length < 1) {
            $('#ldos').val('0');
        }
        return false;
    });
});
$(function() {
    $('#iuno').on('keyup', function() {
        var dato = $('#iuno').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 1;
            $('#luno').val(result);
        }
        if ($('#iuno').val().length < 1) {
            $('#luno').val('0');
        }
        return false;
    });
});
$(function() {
    $('#icincuenta-ctvs').on('keyup', function() {
        var dato = $('#icincuenta-ctvs').val();
        if ($.isNumeric(dato)) {
            var result = parseFloat(dato) * 0.5;
            $('#lcincuenta-ctvs').val(result);
        }
        if ($('#icincuenta-ctvs').val().length < 1) {
            $('#lcincuenta-ctvs').val('0');
        }
        return false;
    });
});
$(function() {
    $('#iveinte-ctvs').on('keyup', function() {
        var dato = $('#iveinte-ctvs').val();
        if ($.isNumeric(dato)) {
            var result = parseFloat(dato) * 0.2;
            result = result.toFixed(2);
            $('#lveinte-ctvs').val(result);
        }
        if ($('#iveinte-ctvs').val().length < 1) {
            $('#lveinte-ctvs').val('0');
        }
        return false;
    });
});
$(function() {
    $('#idiez-ctvs').on('keyup', function() {
        var dato = $('#idiez-ctvs').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 0.1;
            result = result.toFixed(2);
            $('#ldiez-ctvs').val(result);
        }
        if ($('#idiez-ctvs').val().length < 1) {
            $('#ldiez-ctvs').val('0');
        }
        return false;
    });
});

$(function() {
    $('#idiez-ctvs').on('keyup', function() {
        var dato = $('#idiez-ctvs').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 0.1;
            result = result.toFixed(2);
            $('#ldiez-ctvs').val(result);
        }
        if ($('#idiez-ctvs').val().length < 1) {
            $('#ldiez-ctvs').val('0');
        }
        return false;
    });
});

$(function() {
    $('#icien-sus').on('keyup', function() {
        var dato = $('#icien-sus').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 100;
            result = result.toFixed(2);
            $('#lcien-sus').val(result);
        }
        if ($('#icien-sus').val().length < 1) {
            $('#lcien-sus').val('0');
        }
        return false;
    });
});

$(function() {
    $('#icincuenta-sus').on('keyup', function() {
        var dato = $('#icincuenta-sus').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 50;
            result = result.toFixed(2);
            $('#lcincuenta-sus').val(result);
        }
        if ($('#icincuenta-sus').val().length < 1) {
            $('#lcincuenta-sus').val('0');
        }
        return false;
    });
});

$(function() {
    $('#iveinte-sus').on('keyup', function() {
        var dato = $('#iveinte-sus').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 20;
            result = result.toFixed(2);
            $('#lveinte-sus').val(result);
        }
        if ($('#iveinte-sus').val().length < 1) {
            $('#lveinte-sus').val('0');
        }
        return false;
    });
});

$(function() {
    $('#idiez-sus').on('keyup', function() {
        var dato = $('#idiez-sus').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 10;
            result = result.toFixed(2);
            $('#ldiez-sus').val(result);
        }
        if ($('#idiez-sus').val().length < 1) {
            $('#ldiez-sus').val('0');
        }
        return false;
    });
});

$(function() {
    $('#icinco-sus').on('keyup', function() {
        var dato = $('#icinco-sus').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 5;
            result = result.toFixed(2);
            $('#lcinco-sus').val(result);
        }
        if ($('#icinco-sus').val().length < 1) {
            $('#lcinco-sus').val('0');
        }
        return false;
    });
});

$(function() {
    $('#idos-sus').on('keyup', function() {
        var dato = $('#idos-sus').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 2;
            result = result.toFixed(2);
            $('#ldos-sus').val(result);
        }
        if ($('#idos-sus').val().length < 1) {
            $('#ldos-sus').val('0');
        }
        return false;
    });
});

$(function() {
    $('#iuno-sus').on('keyup', function() {
        var dato = $('#iuno-sus').val();
        if ($.isNumeric(dato)) {
            var result = parseInt(dato) * 1;
            result = result.toFixed(2);
            $('#luno-sus').val(result);
        }
        if ($('#iuno-sus').val().length < 1) {
            $('#luno-sus').val('0');
        }
        return false;
    });
});

$(function() {
    $('#ltotal').on('click', function() {
        actualiza();

        return false;
    });
});

$(function() {
    $('#ltotal-sus').on('click', function() {
        actualiza();

        return false;
    });
});

function actualiza() {
    var total = 0;
    var total_sus=0;
    if ($('#ldocientos').val().length > 0) {
        var docientosBs = $('#ldocientos').val();
        total = total + parseFloat(docientosBs);
    }
    if ($('#lcien').val().length > 0) {
        var cienBs = $('#lcien').val();
        total = total + parseFloat(cienBs);
    }
    if ($('#lcincuenta').val().length > 0) {
        var cincuentaBs = $('#lcincuenta').val();
        total = total + parseFloat(cincuentaBs);
    }
    if ($('#lveinte').val().length > 0) {
        var veinteBs = $('#lveinte').val();
        total = total + parseFloat(veinteBs);
    }
    if ($('#ldiez').val().length > 0) {
        var diezBs = $('#ldiez').val();
        total = total + parseFloat(diezBs);
    }
    if ($('#lcinco').val().length > 0) {
        var cincoBs = $('#lcinco').val();
        total = total + parseFloat(cincoBs);
    }
    if ($('#ldos').val().length > 0) {
        var dosBs = $('#ldos').val();
        total = total + parseFloat(dosBs);
    }
    if ($('#luno').val().length > 0) {
        var unoBs = $('#luno').val();
        total = total + parseFloat(unoBs);
    }
    if ($('#lcincuenta-ctvs').val().length > 0) {
        var cincuentaCtvs = $('#lcincuenta-ctvs').val();
        total = total + parseFloat(cincuentaCtvs);
    }
    if ($('#lveinte-ctvs').val().length > 0) {
        var veinteCtvs = $('#lveinte-ctvs').val();
        total = total + parseFloat(veinteCtvs);
    }
    if ($('#ldiez-ctvs').val().length > 0) {
        var diezCtvs = $('#ldiez-ctvs').val();
        total = total + parseFloat(diezCtvs);
    }
    $('#ltotal').val(total);

    //para Dolares
    if ($('#lcien-sus').val().length > 0) {
        var cien_sus = $('#lcien-sus').val();
        total_sus = total_sus + parseFloat(cien_sus);
    }
    if ($('#lcincuenta-sus').val().length > 0) {
        var cincuenta_sus = $('#lcincuenta-sus').val();
        total_sus = total_sus + parseFloat(cincuenta_sus);
    }
    if ($('#lveinte-sus').val().length > 0) {
        var veinte_sus = $('#lveinte-sus').val();
        total_sus = total_sus + parseFloat(veinte_sus);
    }
    if ($('#ldiez-sus').val().length > 0) {
        var diez_sus = $('#ldiez-sus').val();
        total_sus = total_sus + parseFloat(diez_sus);
    }
    if ($('#lcinco-sus').val().length > 0) {
        var cinco_sus = $('#lcinco-sus').val();
        total_sus = total_sus + parseFloat(cinco_sus);
    }
    if ($('#ldos-sus').val().length > 0) {
        var dos_sus = $('#ldos-sus').val();
        total_sus = total_sus + parseFloat(dos_sus);
    }
    if ($('#luno-sus').val().length > 0) {
        var uno_sus = $('#luno-sus').val();
        total_sus= total_sus + parseFloat(uno_sus);
    }
    $('#ltotal-sus').val(total_sus);
    
    llenarVacios();
}

function llenarVacios() {

    if ($('#idocientos').val().length < 1) {
        $('#idocientos').val(0);
    }
    if ($('#icien').val().length < 1) {
        $('#icien').val(0);
    }
    if ($('#icincuenta').val().length < 1) {
        $('#icincuenta').val(0);
    }
    if ($('#iveinte').val().length <1) {
        $('#iveinte').val(0);
    }
    if ($('#idiez').val().length <1) {
        $('#idiez').val(0);
    }
    if ($('#icinco').val().length <1) {
        $('#icinco').val(0);
    }
    if ($('#idos').val().length <1) {
        $('#idos').val(0);
    }
    if ($('#iuno').val().length <1) {
        $('#iuno').val(0);
    }
    if ($('#icincuenta-ctvs').val().length <1) {
        $('#icincuenta-ctvs').val(0);
    }
    if ($('#iveinte-ctvs').val().length <1) {
        $('#iveinte-ctvs').val(0);
    }
    if ($('#idiez-ctvs').val().length <1) {
        $('#idiez-ctvs').val(0);
    }
   
    //para Dolares
    if ($('#icien-sus').val().length < 1) {
        $('#icien-sus').val(0);
    }
    if ($('#icincuenta-sus').val().length < 1) {
        $('#icincuenta-sus').val(0);
    }
    if ($('#iveinte-sus').val().length < 1) {
        $('#iveinte-sus').val(0);
    }
    if ($('#idiez-sus').val().length < 1) {
        $('#idiez-sus').val(0);
    }
    if ($('#icinco-sus').val().length <1) {
        $('#icinco-sus').val(0);
    }
    if ($('#idos-sus').val().length <1) {
        $('#idos-sus').val(0);
    }
    if ($('#iuno-sus').val().length <1) {
        $('#iuno-sus').val(0);
    }    
}

function actualizar_total() {
    alert('guardando..');
    actualiza();
        var dato = $('#ltotal').val();
        var url = 'cierresesion_guardar.php';
        $.ajax({        
		type:'POST',
		url:url,
		data:'id='+dato,
		success: function(){
			return false;
		}
	});      
    return false;
}

function imprimir() {
    window.print('');
}

function actualizar_e_imprimir() {
    actualiza();
    comprobar_totales();
}

function comprobar_totales() {
    var total_bs = parseFloat($('#ltotal').val());
    var total_sus = parseFloat($('#ltotal-sus').val());
    var tipo_cambio = parseFloat($("#tipo-cambio").val());
    var total_sus_en_bs =  parseFloat(total_sus) * parseFloat(tipo_cambio);
    var total_vendido = parseFloat($("#monto-total").val());
    var total_entregado = parseFloat(total_bs) + parseFloat(total_sus_en_bs);
    if (total_entregado < total_vendido) {
        //alert('es menor ' + total_entregado + ' < ' + total_vendido);
        alertify.error('Monto entregado en caja debe ser mayor o igual al monto de ventas realizadas');
    } else {
        cerrarSesion();
        //alert('es mayor');
    }
}

// Envia el formulario de entrega de dinero en caja al de cierre de sesión
function cerrarSesion() {
    // Enviar las variables al formulario de impresión
    var frm = $("#form-entrega-caja").serialize();
    document.location.href = 'vista_previa_impresion.php?'+frm;
}
