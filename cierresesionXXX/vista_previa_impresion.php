<?php
    require_once '../extras/config.php';
    require_once '../datos/Database.php';
    require_once '../extras/notorm/notorm.lib.php';

    // Se inicializan las variables de sesión
    session_start();

    if (($_SESSION['logueado']) != 'SI') {
        header('Location: ../inicio');
    }

    // id del usuario activo en la sesión
    $id_usuario = $_SESSION['userMaster']['idUsuario'];
    $id_sucursal = $_SESSION['userMaster']['idSucursal'];
    $id_sesion = $_SESSION['idSesion'];


    //----------------------FUNCIONES----------------------------------------

    // Obtiene la información de la sucursal donde se emite el cierre de caja
    function get_datos_sucursal() {
        $query = "SELECT s.nombreSucur, s.ciudad, s.direccion, s.telefono
                  FROM Empresa e, Sucursal s
                  WHERE e.idEmpresa=s.idEmpresa and s.idSucursal = :id_sucursal";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_sucursal', $id_sucursal);
        $cmd->execute();
    }

    // Obtiene el número de factura de emitido por una venta
    // Si a la venta NO se le asignó una factura, retorna "(sin factura)"
    function get_nro_factura_from_venta($id_venta) {
        $query = "SELECT f.nroFactura
                  FROM   ventas v, factura f
                  WHERE  v.idVenta = f.idVenta AND v.idVenta = :id_venta";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_venta', $id_venta);
        $cmd->execute();
        $row_count = $cmd->rowCount();
        $result = $cmd->fetch();
        if ($row_count > 0) {
            return $result['nroFactura'];
        } else {
            return '(sin factura)';
        }
    }

    //
    try{
        // Obtener datos de empresa
        $query_datos_empresa = "SELECT s.nombreSucur, s.ciudad, s.direccion, s.telefono
                                FROM Empresa e, Sucursal s
                                WHERE e.idEmpresa=s.idEmpresa and s.idSucursal = :id_sucursal";
        $cmd_datos_empresa = Database::getInstance()->getDb()->prepare($query_datos_empresa);
        $cmd_datos_empresa->bindParam(':id_sucursal', $id_sucursal);
        $cmd_datos_empresa->execute();
        $row_datos_empresa = $cmd_datos_empresa->fetch();
        $nombre_sucursal = $row_datos_empresa['nombreSucur'];
        $ciudad_sucursal = $row_datos_empresa['ciudad'];
        $direccion_sucursal = $row_datos_empresa['direccion'];
        $telefono_sucursal = $row_datos_empresa['telefono'];

        // Obtener datos de usuario
        $consulta_datos_usuario = "SELECT nombre FROM Usuario WHERE idUsuario = :id_usuario";
        $comand_datos_usuario = Database::getInstance()->getDb()->prepare($consulta_datos_usuario);
        $comand_datos_usuario->bindParam(':id_usuario', $id_usuario);
        $comand_datos_usuario->execute();
        $row_usuario = $comand_datos_usuario->fetch();
        $nombre_usuario = $row_usuario['nombre'];

    } catch (Exception $e){
        echo 'Error: '.$e;
    }

    // Obtener los billetes ingresados por el usuario
    // Billetes en Bolivianos
    $idocientos = $_GET['idocientos'];
    $ldocientos = $_GET['ldocientos'];
    $icien = $_GET['icien'];
    $lcien = $_GET['lcien'];
    $icincuenta = $_GET['icincuenta'];
    $lcincuenta = $_GET['lcincuenta'];
    $iveinte = $_GET['iveinte'];
    $lveinte = $_GET['lveinte'];
    $idiez = $_GET['idiez'];
    $ldiez = $_GET['ldiez'];
    $icinco = $_GET['icinco'];
    $lcinco = $_GET['lcinco'];
    $idos = $_GET['idos'];
    $ldos = $_GET['ldos'];
    $iuno = $_GET['iuno'];
    $luno = $_GET['luno'];

    // Moneda en Bolivianos
    $icincuenta_ctvs = $_GET['icincuenta-ctvs'];
    $lcincuenta_ctvs = $_GET['lcincuenta-ctvs'];
    $iveinte_ctvs = $_GET['iveinte-ctvs'];
    $lveinte_ctvs = $_GET['lveinte-ctvs'];
    $idiez_ctvs = $_GET['idiez-ctvs'];
    $ldiez_ctvs = $_GET['ldiez-ctvs'];

    // Billetes en Dólares
    $icien_sus = $_GET['icien-sus'];
    $lcien_sus = $_GET['lcien-sus'];
    $icincuenta_sus = $_GET['icincuenta-sus'];
    $lcincuenta_sus = $_GET['lcincuenta-sus'];
    $iveinte_sus = $_GET['iveinte-sus'];
    $lveinte_sus = $_GET['lveinte-sus'];
    $idiez_sus = $_GET['idiez-sus'];
    $ldiez_sus = $_GET['ldiez-sus'];
    $icinco_sus = $_GET['icinco-sus'];
    $lcinco_sus = $_GET['lcinco-sus'];
    $idos_sus = $_GET['idos-sus'];
    $ldos_sus = $_GET['ldos-sus'];
    $iuno_sus = $_GET['iuno-sus'];
    $luno_sus = $_GET['luno-sus'];

    // Monto total en Bolivianos
    $total = $_GET['ltotal'];
    // Monto total en Dólares
    $total_sus = $_GET['ltotal-sus'];
?>

<html>
<head>
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen">
    <link href="../reporteCompras/printListaTransac.css" rel="stylesheet" type="text/css" media="print" />
    <script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
    <script language="javascript" type="text/javascript" src="jPedidos.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="vista_previa_impresion.css">
    <script type="text/javascript" src="jcierresesion.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            doPrint();
        });

        function doPrint() {
            window.print()();
        }

        function volver(dato) {
            location.href = "index.php";
        }
    </script>
    <style type="text/css" media="print,screen">

        @media print {
            th {
            color:black;
            background-color:lightgrey;
        }
        thead {
            display:table-header-group;
        }
        tbody {
            display:table-row-group;
        }

        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }

        }

        /*
        @media print{
            div.registros-caja {
                display: block;
                page-break-inside: avoid;
            }

            div.saltopagina{
                display:block;
                page-break-before:always;
            }
        }
        */
        /*
        table { page-break-after:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        td    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        */

    </style>
</head>
    <body>
        <div id="leteral"></div>
        <div id="noprint">
            <table>
                <tr>
                    <td><button type="button" name="volver" id="volver" onclick="volver()">Volver</button></td>
                    <td>&nbsp;&nbsp;</td>
                    <td><button type="button" onclick="doPrint();" >Imprimir</button></td>
                    <td>&nbsp;&nbsp;</td>
                </tr>
            </table>
        </div>

        <div id="hoja" style="height: auto">
            <div class="widget">
                <div id="encabezado">
                    <div id="logo" ><img width="100" height="100" src=""></div>
                    <div id="titulo">
                        <center>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="right">
                                            <?php
                                                $sucursal = $db->Sucursal('idSucursal = ?', $id_sucursal)->fetch();
                                            ?>
                                            <font size='6'><center><strong><label for="user_name">CIERRE DE CAJA</label></strong></center></font><br>
                                            <font size='5'><center><strong><label for="user_name">Sucursal: <?= $sucursal['nombreSucur']; ?></label></strong></center></font><br>
                                            <font size='4'><center><label for="user_name">Dirección: <?= $sucursal['direccion']; ?></label></center></font>
                                            <font size='4'><center><label for="user_name">Teléfono: <?= $sucursal['telefono']; ?></label></center></font>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                    <div id="inf">
                        <center>
                            <table >
                                <tbody>
                                <tr>
                                    <td>
                                        <strong>
                                            <label for="user_name">Fecha: <?= $fecha = date("d-m-Y"); ?></label>
                                        </strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                </div>

                <div align="center" id="informacion">
                    <div id="content">
                        <table class="tabla" border="1" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th><strong><label>Usuario:</label> <?= $nombre_usuario; ?></strong></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div>
                        <h2>Ventas Realizadas</h2>
                    <?php
                    // Ventas realizadas durante la sesion activa
                    $query_ventas = "SELECT  v.idVenta, u.nombre, t.fecha, t.hora, v.montoTotal, vs.nroVenta
                                 FROM    Transaccion t, Ventas v,VentaSucursal vs, InicioSesion i, Sucursal s, Usuario u
                                 WHERE   t.idInicioSesion=i.idInicio and
                                         t.idTransaccion=v.idTransaccion and
                                         v.idVenta=vs.idVenta and
                                         vs.idSucursal=s.idSucursal and
                                         i.sucursal=s.idSucursal and
                                         i.usuario=u.idUsuario and
                                         i.usuario=:id_usuario and
                                         i.sucursal=:id_sucursal and
                                         i.idInicio=:id_sesion and
                                         t.idTransaccion not in (select idTransaccion from transaccion where tipoTrans=3)";
                    $cmd_ventas = Database::getInstance()->getDb()->prepare($query_ventas);
                    $cmd_ventas->bindParam(':id_usuario', $id_usuario);
                    $cmd_ventas->bindParam(':id_sesion', $id_sesion);
                    $cmd_ventas->bindParam(':id_sucursal', $id_sucursal);
                    $cmd_ventas->execute();

                    /*// Variables para el salto de página
                    $nro_pagina = 0;
                    $nro_registro = 0;
@
                    $rows_returned = $cmd_ventas->rowCount();  // cantidad de filas retornadas por la consulta
                    if ($rows_returned > 0) {
                        require_once 'ventas_realizadas.php';
                    }*/
                    $nro = 0;
                    ?>
                        <div id="ventas-realizadas">
                        <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                            <thead>
                                <tr>
                                    <th class="text-center"> Nro </th>
                                    <th class="text-center"> Nro. Venta </th>
                                    <th class="text-center"> Nro. Factura </th>
                                    <th class="text-center"> Hora </th>
                                    <th class="text-center"> Fecha </th>
                                    <th class="text-center"> Monto Total </th>
                                </tr>
                            </thead>
                            <tbody>
                    <?php   while ($row = $cmd_ventas->fetch()) :   $nro++; ?>
                            <tr style="border: 1px solid black; font-size: 12px;">
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $nro; ?></td>
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $row['nroVenta']; ?></td>
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= get_nro_factura_from_venta($row['nroVenta']); ?></td>
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $row['hora']; ?></td>
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $row['fecha']; ?></td>
                                <td style="border-right: 1px solid black; padding-right: 1%;" class="text-right"><?= $row['montoTotal']; ?></td>
                            </tr>
                    <?php   endwhile; ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="registros-caja" id="agrega-registros-caja">
                        <center><h3>Entrega Caja</h3></center>
                        <form id="entrega" class="tabla" border="0" cellpadding="0" cellspacing="0" role="form" method="post" action="cierresesion_guardar.php" >
                            <table >
                                <thead>
                                <!--Fila de titulos de columnas-->
                                <tr>
                                    <th> Montos Bs </th>
                                    <th> Cantidad </th>
                                    <th> Total </th>
                                    <th> Montos $us </th>
                                    <th> Cantidad </th>
                                    <th> Total </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">200 Bs.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="idocientos" type="text" readonly value=<?= $idocientos;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="ldocientos" type="text" readonly value=<?= $ldocientos;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%">100 $us.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="icien-sus" type="text" readonly value=<?= $icien_sus;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lcien-sus" type="text" readonly value=<?= $lcien_sus;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">100 Bs.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="icien" type="text" readonly value=<?= $icien;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lcien" type="text" readonly value=<?= $lcien;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%">50 $us.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="icincuenta-sus" type="text" readonly value=<?= $icincuenta_sus;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lcincuenta-sus" type="text" readonly value=<?= $lcincuenta_sus;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">50 Bs.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="icincuenta" type="text" readonly value=<?= $icincuenta;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lcincuenta" type="text" readonly value=<?= $lcincuenta;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%">20 $us.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="iveinte-sus" type="text" readonly value=<?= $iveinte_sus;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lveinte-sus" type="text" readonly value=<?= $lveinte_sus;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">20 Bs.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="iveinte" type="text" readonly value=<?= $iveinte;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lveinte" type="text" readonly value=<?= $lveinte;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%">10 $us.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="idiez-sus" type="text" readonly value=<?= $idiez_sus;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="ldiez-sus" type="text" readonly value=<?= $ldiez_sus;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">10 Bs.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="idiez" type="text" readonly value=<?= $idiez;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="ldiez" type="text" readonly value=<?= $ldiez;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%">5 $us.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="icinco-sus" type="text" readonly value=<?= $icinco_sus;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lcinco-sus" type="text" readonly value=<?= $lcinco_sus;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">5 Bs.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="icinco" type="text" readonly value=<?= $icinco;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lcinco" type="text" readonly value=<?= $lcinco;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%">2 $us.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="idos-sus" type="text" readonly value=<?= $idos_sus;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="ldos-sus" type="text" readonly value=<?= $ldos_sus;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">2 Bs.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="idos" type="text" readonly value=<?= $idos;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="ldos" type="text" readonly value=<?= $ldos;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%">1 $us.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="iuno-sus" type="text" readonly value=<?= $iuno_sus;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="luno-sus" type="text" readonly value=<?= $luno_sus;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">1 Bs.</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="iuno" type="text" readonly value=<?= $iuno;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="luno" type="text" readonly value=<?= $luno;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">50 ctvs</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="icincuenta-ctvs" type="text" readonly value=<?= $icincuenta_ctvs;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lcincuenta-ctvs" type="text" readonly value=<?= $lcincuenta_ctvs;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">20 ctvs</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="iveinte-ctvs" type="text" readonly value=<?= $iveinte_ctvs;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="lveinte-ctvs" type="text" readonly value=<?= $lveinte_ctvs;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black; padding-right: 1%">10 ctvs</td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="idiez-ctvs" type="text" readonly value=<?= $idiez_ctvs;?>></td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="ldiez-ctvs" type="text" readonly value=<?= $ldiez_ctvs;?>></td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%">
                                        <label>Total:</label>
                                    </td>

                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="ltotal" type="text" readonly value=<?= $total; ?>></td>
                                    <td></td>
                                    <td style="border-right: 1px solid black; padding-right: 1%">
                                        <label>Total:</label>
                                    </td>
                                    <td style="border-right: 1px solid black; padding-right: 0%"><input style="width: 100%" name="itotal-sus" type="text" readonly value=<?= $total_sus;?>></td>
                                </tr>
                                </tbody>
                            </table>
                            <a type="button" onclick="entrega.submit()" style="font-size: 25px"; >Cerrar Sesión</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
