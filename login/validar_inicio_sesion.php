<?php

    require_once '../datos/Database.php';

    require_once 'validar_dosificacion.php';

    $username = trim($_POST['usuario']);
    $clave = trim($_POST['pass']);
    $sucursal = trim($_POST['sucursal']);
    
    $query_select_user = "SELECT * FROM Usuario WHERE usuario= :username AND estado = 1";
    $cmd_select_user = Database::getInstance()->getDb()->prepare($query_select_user);
    $cmd_select_user->bindParam(':username', $username);
    $cmd_select_user->execute();

    $row_count_user = $cmd_select_user->rowCount();// si devuelve algun numero significativo

    if( $row_count_user > 0) {

        $row_user = $cmd_select_user->fetch();

        // Obtenemos la información primordial del usuario
        $id_usuario = $row_user['idUsuario'];
        $passwordenBD = $row_user['clave'];
        $estado_usuario = $row_user['estado'];   //ESTADO DEL USUARIO
        $id_sucursal = $sucursal; //get_id_sucursal_from_name($sucursal);

        if( md5($clave) == $passwordenBD) {
            if ($estado_usuario == 0) {
                echo "error_1";
                exit;
            } else {


                if (usuario_tiene_sesion_activa($id_usuario)) { // Reanudar la sesión anterior
                    session_start();
                    $_SESSION['logueado'] = 'SI';
                    //$_SESSION['userMaster'] = array("idUsuario" => $id_usuario, "idSucursal" => $id_sucursal);
                    reanudar_ultima_sesion($id_usuario);
                    get_privilegios_usuario($id_usuario);

                    echo 'exito_1';
                    exit();
                } else {
                    session_start();
                    // Iniciar una nueva sesión
                    update_usuario_to_activo($id_usuario);
                    insertar_inicio_sesion($id_usuario, $sucursal);
                    set_id_inicio_sesion($id_usuario);
                    get_privilegios_usuario($id_usuario);

                    $_SESSION['logueado'] = 'SI';
                    $_SESSION['userMaster'] = array("idUsuario" => $id_usuario, "idSucursal" => $id_sucursal);
                    echo "exito_1";
                    exit();
                }
            }
        } else {
            echo "error_1";
            exit;
        }
    } else {
        echo "error_1";
        exit();
    }

    function usuario_tiene_sesion_activa($id_usuario) {
        $query = "SELECT activo FROM Usuario WHERE idUsuario = :id_usuario";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_usuario', $id_usuario);
        $cmd->execute();
        $result = $cmd->fetch();
        $activo = $result['activo'];
        if ($activo == 1) {
            return true;
        } else {
            return false;
        }
    }

    function reanudar_ultima_sesion($id_usuario) {
        $query = "SELECT idInicio, sucursal
                  FROM InicioSesion
                  WHERE usuario = :id_usuario
                  ORDER BY idInicio DESC LIMIT 1";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_usuario', $id_usuario);
        $cmd->execute();
        $result = $cmd->fetch();
        $id_sesion_inicio = $result['idInicio'];
        $id_sesion_sucursal = $result['sucursal'];

        $_SESSION['userMaster'] = array("idUsuario" => $id_usuario, "idSucursal" => $id_sesion_sucursal);

        //$_SESSION['userMaster']['idUsuario'] = $id_usuario;
        //$_SESSION['userMaster']['idSucursal'] = $id_sesion_sucursal;
        $_SESSION['idSesion'] = $id_sesion_inicio;

    }

    function get_privilegios_usuario($id_usuario) {
        // Obtener los privilegios por URL del usuario
        $query_privilegio = "SELECT pcu.idCasoUso, pcu.url
                             FROM   Usuario u, PrivilegiosCasoUso pcu, AsignaPrivilegioCasoUso apcu
                             WHERE  u.idUsuario = :id_usuario AND
                                    u.idUsuario = apcu.idUsuario AND
                                    apcu.idCasoUso = pcu.idCasoUso";
        $cmd_privilegio = Database::getInstance()->getDb()->prepare($query_privilegio);
        $cmd_privilegio->bindParam(':id_usuario',$id_usuario);
        $cmd_privilegio->execute();

        $array_privilegio = array();
        while ($result_privilegio = $cmd_privilegio->fetch()) {
            $array_privilegio[$result_privilegio['idCasoUso']] = $result_privilegio['url'];
        }
        $_SESSION['privilegio'] = $array_privilegio;
    }

    function update_usuario_to_activo($id_usuario) {
        // Actualizar el usuario a "activo"
        $activo = 1;
        $query_update_usuario = "UPDATE Usuario set activo = :activo where idUsuario = :id_usuario";
        $cmd_update_usuario = Database::getInstance()->getDb()->prepare($query_update_usuario);
        $cmd_update_usuario->bindParam(':activo', $activo);
        $cmd_update_usuario->bindParam(':id_usuario', $id_usuario);
        $cmd_update_usuario->execute();
    }

    function get_id_sucursal_from_name() {
        // Obtener los datos de la sucursal donde se inicia sesión
        $query_select_sucursal = "SELECT idSucursal FROM Sucursal WHERE nombreSucur = :sucursal ";
        $cmd_select_sucursal = Database::getInstance()->getDb()->prepare($query_select_sucursal);
        $cmd_select_sucursal->bindParam(':sucursal', $sucursal);
        $cmd_select_sucursal->execute();
        $row_sucursal = $cmd_select_sucursal->fetch();
        return $row_sucursal['idSucursal'];
    }

    function set_id_inicio_sesion($id_usuario) {
        $query = "SELECT idInicio
                  FROM InicioSesion
                  WHERE usuario = :id_usuario
                  ORDER BY idInicio DESC LIMIT 1";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id_usuario', $id_usuario);
        $cmd->execute();
        $result = $cmd->fetch();
        $_SESSION['idSesion'] = $result['idInicio'];
    }

    function get_last_id_inicio_sesion($id_usuario) {
        $query = "SELECT idInicio FROM InicioSesion WHERE usuario = :id_usuario ORDER BY idInicio DESC LIMIT 1";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':usuario', $id_usuario);
        $cmd->execute();
        $result = $cmd->fetch();
        return $result['idInicio'];
    }

    function insertar_inicio_sesion($id_usuario, $id_sucursal) {
        date_default_timezone_set("America/La_Paz");
        $fecha = date('Y-m-d');
        $hora = date("H:i:s");

        $query = "INSERT INTO InicioSesion() VALUES('', :fecha, :hora, :usuario, :sucursal)";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':fecha', $fecha);
        $cmd->bindParam(':hora', $hora);
        $cmd->bindParam(':usuario', $id_usuario);
        $cmd->bindParam(':sucursal', $id_sucursal);
        $cmd->execute();
    }

?>