<?php

    require_once '../extras/config.php';
    require_once '../datos/Database.php';

    session_start();

    if (isset($_SESSION['logueado']) && $_SESSION['logueado'] == 'SI') {
        header('Location: ../inicio/');
    }

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <title><?= TITULO; ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" href="../assets/img/sc.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link id="bootstrap-rtl-link" href="#" rel="stylesheet"/>
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet"/>

    <!--Fonts-->
    <link
        href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300"
        rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link id="beyond-link" href="../assets/css/beyond.min.css" rel="stylesheet"/>
    <link href="../assets/css/demo.min.css" rel="stylesheet"/>
    <link href="../assets/css/animate.min.css" rel="stylesheet"/>
    <link id="skin-link" href="#" rel="stylesheet" type="text/css"/>

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="../extras/js/jquery/jquery-2.2.0.min.js"></script>
    <script src="login.js"></script>
</head>
<!--Head Ends-->
<!--Body-->
    <body>
        <div class="login-container animated fadeInDown">
            <div class="loginbox bg-white">
                <div class="loginbox-title"><?= TITULO; ?></div>
                <div class="loginbox-social">
                    <div class="social-title ">
                    </div>
                </div>
                <center>
                    <div>
                        <img alt="" src="../assets/img/sc.png" width="195" height="130"/>
                    </div>
                </center>

                <form id="login" class="m-t" action="javascript: validarInicioSesion();" name="login">
                    <div class="loginbox-textbox">
                        <div id="msj" class="alert alert-danger fade in" hidden>
                            <i class="fa-fw fa fa-warning"></i>
                            <strong>Error</strong><br/>
                            Nombre de USUARIO y/o CONTRASEÑA no son válidos..
                            <input type="text" id="input-error" value="">
                        </div>
                        <select name="sucursal" id="sucursal" style="width: 100%;" hidden>
<!--
//                            foreach ($db->Sucursal('estado = ?', '1') as $sucursal )  : -->
                            <option value="1">Casa Matriz</option>
                        </select>
                    </div>
                    <div class="loginbox-textbox">
                        <input class="form-control" type="text" required="" placeholder="Nombre de usuario " name="usuario"/>
                    </div>

                    <div class="loginbox-textbox">
                        <input class="form-control" type="password" required="" placeholder="Password" name="pass"/>
                    </div>
                    <div class="loginbox-submit">
                        <button class="btn btn-primary btn-block" name="validar" type="submit">Iniciar Sesion</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
