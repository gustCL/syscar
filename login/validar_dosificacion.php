<?php
    /*
     * Este script es usado para validar la fecha de las dosificaciones.
     *
     * Se verificará todas las dosificaciones, comprobando su fecha de caducidad y comparando con la fecha actual.
     * Toda dosificación que haya rebasado con uno o mas dias su fecha de caducidad, pasará a tener estado "2".
     *
     */

    require_once '../extras/config.php';
    require_once '../datos/Database.php';


    // Cambia el estado a 2 (anulado) de una dosificación
    function anularDosificacion($id) {
        $query = "UPDATE Dosificacion SET estado = 2 WHERE idDosifi = :id";
        $cmd = Database::getInstance()->getDb()->prepare($query);
        $cmd->bindParam(':id', $id);
        $cmd->execute();
    }

    function diferenciaDias($fecha_limite) {
        date_default_timezone_set('America/La_Paz');
        $fecha_actual = date('d-m-Y');
        $diferencia_fecha = strtotime($fecha_limite) - strtotime($fecha_actual);
        $diferencia_dias = intval($diferencia_fecha / 60 / 60 / 24);
        return $diferencia_dias;
    }

    $dosificaciones = $db->Dosificacion();

    foreach ($dosificaciones as $dosificacion) {
        if (diferenciaDias($dosificacion['fechaLimite']) < 0) {
            anularDosificacion($dosificacion['idDosifi']);
        }
    }