
<?php
require_once ('head.php');
?>
<div class="page-content">
    <!-- Arbol de navegacion -->
    <div class="page-breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a>Inicio</a>
            </li>
            <li>
                <a>Reporte de Ventas</a>
            </li>
        </ul>
    </div>
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                Reportes

            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget">
                                <div class="widget-body bordered-left bordered-warning">
                                    <span class="widget-caption"> Reportes de Ventas </span>
                                </div>

                                <div class="widget-body">
                                    <form   class="form-horizontal">							
                                        <center>
                                            <div class="form-group">                                   
                                                <a style="width: 20%" class="btn btn-primary" href="facVentas.php">Reporte de Facturas Venta</a>
                                            </div>

                                            <div class="form-group"> 
                                                <a style="width: 20%" class="btn btn-primary" href="VentasEspecificacion.php">Ventas por Especificacion</a>                                               
                                            </div>
                                            <div class="form-group"> 
                                                <a style="width: 20%" class="btn btn-primary" href="VentasProducto.php">Ventas por Producto</a>                                               
                                            </div>
                                            <div class="form-group"> 
                                                <a style="width: 20%" class="btn btn-primary" href="VentasUsuario.php">Ventas de Usuario</a>                                               
                                            </div>
<!--                                            <div class="form-group"> 
                                                <a style="width: 15%" class="btn btn-primary" href="VentasTurno.php">Ventas por Turno</a>                                               
                                            </div>-->
                                            <div class="form-group"> 
                                                <a style="width: 20%" class="btn btn-primary" href="GananciaProducto.php">Ganancias por Producto</a>                                               
                                            </div>
                                        </center>      				
                                    </form>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>                                      

                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->

    </div>
</div>
<?php
require_once ('../header_footer/footer.php');
?>  



<!--Basic Scripts-->


