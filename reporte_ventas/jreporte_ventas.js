
function Buscar_FacVentas()
{
 var str = $('#formFacVentas').serialize();

    $.ajax({
            url: 'reporte_facVentas.php',
            type: 'get',
            data: str,
            success: function(data){
                if (data == 'error1'){
                    swal("Error","Seleccione un mes y año para generar LCV.","error");
                }else{
                    $("#agrega-registros").html(data);
                }
            }
    });
    
}

function Buscar_Ventas()
{
    var str = $('#formFacVentas').serialize();

    $.ajax({
        url: 'reporte_Ventas.php',
        type: 'post',
        data: str,
        success: function(data){
            if (data == 'error1'){
                swal("Error","Seleccione un mes y año para generar LCV.","error");
            }else{
                $("#agrega-registros").html(data);
            }
        }
    });

}

function exportarTxtVentas(mes, anio){
    $.ajax({
        url: 'exportarTxtVentas.php',
        type: 'post',
        data: 'm='+mes+"&a="+anio,
        success: function(data){
            if(data == 'exito'){
                swal("Exportacion correxta","la exportacion a txt de las ventas se realizo correctamente.","success");
            }
    }
    });
}

function exportarVentasExcel(fechai, fechaf){
    $.ajax({
        url: 'exportarVentasExcel.php',
        type: 'post',
        data: 'fi='+fechai+"&ff="+fechaf,
        success: function(data){
            alert(data);
            if(data == 'exito'){
                swal("Exportacion correxta","la exportacion a txt de las ventas se realizo correctamente.","success");
            }
        }
    });
}

function Buscar_VentasEspecifica()
{
 var str = $("#formVentasEsp").serialize();
    
    $.ajax({
            url: 'reporte_VentasEsp.php',
            type: 'GET',
            data: str,
            success: function(data){
                $("#agrega-registros").html(data);
                 
            }
    });
    
}

function fn_paginar(var_div, url) {
    var div = $("#" + var_div);
    $(div).load(url);
}

function Buscar_VentasProv()
{
 var str = $("#formVentasProv").serialize();
  
    $.ajax({
            url: 'reporte_VentasProv.php',
            type: 'get',
            data: str,
            success: function(data){
                $("#agrega-registros").html(data);
                 
            }
    });
    
}

function Buscar_VentasProd()
{
 var str = $("#formVentasProd").serialize();
  
    $.ajax({
            url: 'reporte_VentasProd.php',
            type: 'POST',
            data: str,
            success: function(data){
                $("#agrega-registros").html(data);
                 
            }
    });
    
}

function Buscar_VentasPorUsuario()
{
 var str = $("#formVentasPorUsuarios").serialize();
  
    $.ajax({
            url: 'reporte_VentasPorUsuario.php',
            type: 'POST',
            data: str,
            success: function(data){
                $("#agrega-registros").html(data);
                 
            }
    });
    
}

function Buscar_VentasPorUsuarioDetallado()
{
 var str = $("#formVentasPorUsuariosDetallado").serialize();
  
    $.ajax({
            url: 'reporte_VentasPorUsuarioDetallado.php',
            type: 'POST',
            data: str,
            success: function(data){
                $("#agrega-registros").html(data);
                 
            }
    });
    
}

function Buscar_VentasLab()
{
 var str = $("#formVentasLab").serialize();
  
    $.ajax({
            url: 'reporte_VentasLab.php',
            type: 'get',
            data: str,
            success: function(data){
                $("#agrega-registros").html(data);
                 
            }
    });
    
}

function ProductMasComprado()
{
 var str = $("#formProdMasComprado").serialize();
    
    $.ajax({
            url: 'RepDatProdComprados.php',
            
            type: 'get',
            data: str,
            success: function(data){
                $("#agrega-registros").html(data);
                 
            }
    });
    
}

function imprimir(dato){
        window.print(dato);
       // alert("yandiiiiiii");
       //dato.printAre();
       
        
    }

$(document).ready(function(){
	fn_buscar();
});

function fn_cerrar(){
	$.unblockUI({ 
		onUnblock: function(){
			//$("#div_oculto").html("");
                        //$("#metodo_name").val('');
		}
	}); 
};
function fn_paginar(var_div, url){
	var div = $("#" + var_div);
	$(div).load(url);
	/*
	div.fadeOut("fast", function(){
		$(div).load(url, function(){
			$(div).fadeIn("fast");
		});
	});
	*/
}
function fn_buscar(){
	var str = $("#formLaboratorio").serialize();
        var inicial=$("#fechaI").val();
        var final=$("#fechaF").val();
        $("#btnBuscar").hide();
       
	$.ajax({
		url: 'reporte_datos.php',
		type: 'get',
		data: str+"&fechaInicial="+inicial+"&fechaFinal="+final,
		success: function(data){
                    $("#div_listar").html(data);
                    $("#btnBuscar").show();
                 
		}
	});
}

function redirect_by_post(purl, pparameters, in_new_tab) {
    pparameters = (typeof pparameters == 'undefined') ? {} : pparameters;
    in_new_tab = (typeof in_new_tab == 'undefined') ? true : in_new_tab;
    var form = document.createElement("form");
    $(form).attr("id", "reg-form").attr("name", "reg-form").attr("action", purl).attr("method", "post").attr("enctype", "multipart/form-data");
    if (in_new_tab) {
        $(form).attr("target", "_blank");
    }
    $.each(pparameters, function(key) {
        $(form).append('<input type="text" name="' + key + '" value="' + this + '" />');
    });
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
    return false;
}

function redireccionar(idTxn){
    redirect_by_post("reporteResultado.php",{idTxn:idTxn},true);
}

function redirecIngRes(idTxn){
    redirect_by_post("../ingresoResultados/index.php",{idTxn:idTxn},true);
}

function redirecValidar(idTxn){
    redirect_by_post("../validacionResultados/index.php",{idTxn:idTxn},true);
}


function cabezera(idTxn){
    redirect_by_post("encabezado.php",{idTxn:idTxn},true);
}


