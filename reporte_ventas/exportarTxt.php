
<?php
require_once ('head.php');
?>
<div class="page-content">
    <!-- Arbol de navegacion -->
    <div class="page-breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a>Inicio</a>
            </li>
            <li>
                <a>Reporte de Ventas</a>
            </li>
        </ul>
    </div>
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                Reportes

            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>


        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget">
                                <div class="widget-body bordered-left bordered-warning">
                                    <span class="widget-caption"> Reportes de Ventas </span>
                                </div>

                                <div class="widget-body">
                                    <form   class="form-horizontal">							
                                        <?php
                                        require_once '../datos/Database.php';
                                        if ($_GET['tipo'] == "MesAnio") {
                                            $mes = $_GET['Mes'];
                                            $anio = $_GET['Anio'];
                                            $sql = "select * from ReporteFacturaVentas where date_format(fecha, '%m') ='$mes' and date_format(fecha, '%Y') ='$anio'";
                                            $con = Database::getInstance()->getDb()->prepare($sql);
                                            $con->execute();
                                          function formatear_fecha($fecha){
                                            $cadena=explode("-", $fecha);
                                            $anio=$cadena[0];
                                            $mes=$cadena[1];
                                            $dia=$cadena[2];
                                            $nueva=$dia."/".$mes."/".$anio;
                                            return $nueva;
                                          }
                                          $tot_comp=0;
                                          $tot_comp_por=0;
                                          $consul = "select nit from Empresa";
                                          $comando = Database::getInstance()->getDb()->prepare($consul);
                                          $comando->execute();
                                          $empresa= $comando->fetch();
                                          $nit = $empresa['nit'];
                                          $nb_archivo="ventas_".$mes.$anio."_".$nit.".txt";
                                          $ar=fopen($nb_archivo,"w") or die("Problemas en la creacion");
                                          while ($filac= mysql_fetch_array($con)) {
                                          $code = $filac['id'];
                                          $nit=$filac['nit'];
                                          $nom=$filac['nombreCliente'];
                                          $numfac=$filac['nroFactura'];
                                          $numauto=$filac['Autorizacion'];
                                          $fecomp=$filac['fecha'];
                                          $monto=$filac['montoTotal'];
                                          $porci=$filac['impuestos'];
                                          //$va=$filac['va'];
                                          //$cc=$filac['cero_tres'];
                                          $tot_comp=$tot_comp+$monto;
                                          //echo " - ";
                                          $tot_comp_por=$tot_comp_por+$porci;
                                          $new_fec=formatear_fecha($fecomp);
                                          //echo "<br>";
                                          fputs($ar,$nit);
                                          fputs($ar,"|");
                                          fputs($ar,$nom);
                                          fputs($ar,"|");
                                          fputs($ar,$numfac);
                                          fputs($ar,"|");
                                          fputs($ar,$numauto);
                                          fputs($ar,"|");
                                          fputs($ar,$new_fec);
                                          fputs($ar,"|");
                                          fputs($ar,$monto);
                                          fputs($ar,"|");
                                          fputs($ar,"0.00");
                                          fputs($ar,"|");
                                          fputs($ar,"0.00");
                                          fputs($ar,"|");
                                          fputs($ar,$monto);
                                          fputs($ar,"|");
                                          fputs($ar,$porci);
                                          fputs($ar,"|");
                                         // fputs($ar,$va);
                                         // fputs($ar,"|");
                                         // fputs($ar,$cc);
                                          fputs($ar,"\n");
                                          }
                                          echo "<br>";
                                          fclose($ar);
                                          echo "Los datos se cargaron correctamente.";
                                        } else {
                                            $fechai = $_GET['fechaI'];
                                            $fechaF = $_GET['fechaF'];
                                            $tipo = $_GET['tipo'];
                                            echo $fechai;
                                            ?><br><?php echo $fechaF; ?><br><?php
                                            echo $tipo;
                                              $explo=explode("-", $fecha);
                                          $anio=$explo[0];
                                          $mes=$explo[1];
                                          function formatear_fecha($fecha){
                                          $cadena=explode("-", $fecha);
                                          $anio=$cadena[0];
                                          $mes=$cadena[1];
                                          $dia=$cadena[2];
                                          $nueva=$dia."/".$mes."/".$anio;
                                          return $nueva;
                                          }
                                          $tot_comp=0;
                                          $tot_comp_por=0;
                                          $nit=$cons->cons_simple('par_datos',"cod>0",'nit');
                                          $nb_archivo="ventas_".$mes.$anio."_".$nit.".txt";
                                          $ar=fopen($nb_archivo,"w") or
                                          die("Problemas en la creacion");
                                          while ($filac=mysql_fetch_array($query)) {
                                          $code=$filac['cod'];
                                          $nit=$filac['nitci'];
                                          $nom=$filac['nombre'];
                                          $numfac=$filac['numfac'];
                                          $numauto=$filac['numauto'];
                                          $fecomp=$filac['fecha'];
                                          $monto=$filac['monto'];
                                          $porci=$filac['trecepor'];
                                          $va=$filac['va'];
                                          $cc=$filac['cero_tres'];
                                          $tot_comp=$tot_comp+$monto;
                                          //echo " - ";
                                          $tot_comp_por=$tot_comp_por+$porci;
                                          $new_fec=formatear_fecha($fecomp);
                                          //echo "<br>";
                                          fputs($ar,$nit);
                                          fputs($ar,"|");
                                          fputs($ar,$nom);
                                          fputs($ar,"|");
                                          fputs($ar,$numfac);
                                          fputs($ar,"|");
                                          fputs($ar,$numauto);
                                          fputs($ar,"|");
                                          fputs($ar,$new_fec);
                                          fputs($ar,"|");
                                          fputs($ar,$monto);
                                          fputs($ar,"|");
                                          fputs($ar,"0.00");
                                          fputs($ar,"|");
                                          fputs($ar,"0.00");
                                          fputs($ar,"|");
                                          fputs($ar,$monto);
                                          fputs($ar,"|");
                                          fputs($ar,$porci);
                                          fputs($ar,"|");
                                          fputs($ar,$va);
                                          fputs($ar,"|");
                                          fputs($ar,$cc);
                                          fputs($ar,"\n");
                                          }
                                          echo "<br>";
                                          fclose($ar);
                                          echo "Los datos se cargaron correctamente.";
                                        }
                                          
                                        ?>

                                    </form>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>                                      

                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->

    </div>
</div>
<?php
require_once ('../header_footer/footer.php');
?>  