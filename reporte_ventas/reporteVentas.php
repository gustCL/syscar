<?php
/**
 * Created by PhpStorm.
 * User: Ariel
 * Date: 08/04/2016
 * Time: 02:15 PM
 */
require_once'head.php';
?>
<div class="page-content">
    <!-- Arbol de navegacion -->

    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>Reportes</strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <!--        <div class="header-buttons">-->
        <!--            <a class="sidebar-toggler" href="#">-->
        <!--                <i class="fa fa-arrows-h"></i>-->
        <!--            </a>-->
        <!--            <a class="refresh" id="refresh-toggler" href="#">-->
        <!--                <i class="glyphicon glyphicon-refresh"></i>-->
        <!--            </a>-->
        <!--            <a class="fullscreen" id="fullscreen-toggler" href="#">-->
        <!--                <i class="glyphicon glyphicon-fullscreen"></i>-->
        <!--            </a>-->
        <!--        </div>-->


        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget">
                                <div class="widget-body">
                                    <br>
                                    <form id="formFacVentas" action="" name="formFacVentas">
                                        <center>
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label><strong>Fecha Inicial</strong></label>
                                                    <input type="text" id="fechaI" name="fechaI" data-date-format="yyyy-mm-dd" class="form-control date-picker" >
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label><strong>Fecha Final</strong></label>
                                                    <input type="text" id="fechaF" name="fechaF" data-date-format="yyyy-mm-dd" class="form-control date-picker" >
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <div class="input-group">
                                                    <a type="button" href="javascript:Buscar_Ventas()"
                                                       class="btn btn-primary shiny">Aceptar</a>
                                                </div>

                                            </div>
                                            <br>
                                        </center>
                                    </form>
                                </div>
                                <br>
                                <div class="widget-body">
                                    <div id="agrega-registros">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <!--Fila de titulos de columnas-->
                                            <tr>
                                                <th class="hidden-xs">
                                                    NRO
                                                </th>
                                                <th class="hidden-xs">
                                                    FECHA VENTA
                                                </th>
                                                <th class="hidden-xs">
                                                    NIT DEL COMPRADOR
                                                </th>
                                                <th>
                                                    NOMBRE O RAZON SOCIAL
                                                </th>
                                                <th>
                                                    NRO.DE FACTURA
                                                </th>
                                                <th class="hidden-xs">
                                                    NRO.DE AUTORIZACION
                                                </th>
                                                <th class="hidden-xs">
                                                    CODIGO DE CONTROL
                                                </th>
                                                <th class="hidden-xs">
                                                    TOTAL FACTURA
                                                </th>
                                                <th class="hidden-xs">
                                                    TOTAL I.C.E.
                                                </th>
                                                <th class="hidden-xs">
                                                    TOTAL EXENTO
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr>
                                                <td id="" name="">

                                                </td>
                                                <td class="hidden-xs">

                                                </td>
                                                <td class="hidden-xs">

                                                </td>
                                                <td>

                                                </td>
                                                <td>

                                                </td>

                                                <td>

                                                </td>
                                                <td class="hidden-xs">

                                                </td>
                                                <td>

                                                </td>

                                                <td>

                                                </td>
                                                <td class="hidden-xs">

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->
    </div>
</div>
<?php
require_once('../header_footer/footer.php');
?>

<link href="printListaTransac.css" rel="stylesheet" type="text/css" media="print"/>
