<?php
/**
 * Created by PhpStorm.
 * User: Ariel
 * Date: 08/04/2016
 * Time: 02:58 PM
 */
require_once '../datos/Database.php';
require_once '../extras/PHPPaging.lib/PHPPaging.lib.php';

session_start();
$fechaI = $_POST['fechaI'];
$fechaF = $_POST['fechaF'];
$opcion = 1;

//echo $fechaI."<br>";
//echo $fechaF;
$vacio = '<table class="table table-striped table-bordered table-hover" >
                                            <thead>
                                                <!--Fila de titulos de columnas-->
                                                <tr>
                                                    <th class="hidden-xs">
                                                        NRO
                                                    </th>
                                                    <th class="hidden-xs">
                                                        CODIGO VENTA
                                                    </th>
                                                    <th class="hidden-xs">
                                                        FECHA VENTA
                                                    </th>
                                                    <th class="hidden-xs">
                                                        NIT DEL COMPRADOR
                                                    </th>
                                                    <th>
                                                        NOMBRE O RAZON SOCIAL
                                                    </th>
                                                    <th>
                                                        NRO.DE FACTURA
                                                    </th>
                                                    <th class="hidden-xs">
                                                        NRO.DE AUTORIZACION
                                                    </th>
                                                    <th class="hidden-xs">
                                                        CODIGO DE CONTROL
                                                    </th>
                                                    <th class="hidden-xs">
                                                        TOTAL FACTURA
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        La consulta no genero resultados.';

if ($fechaI == "" and $fechaF == "" or $fechaI == "" or $fechaF == "") {
    echo $vacio;
} else {
    $userMaster = $_SESSION['userMaster'];
    $consulta1 = "SELECT * FROM Sucursal WHERE idSucursal =?";
    $comando1 = Database::getInstance()->getDb()->prepare($consulta1);
    $comando1->execute([$userMaster['idSucursal']]);
    $sdatosSuc = $comando1->fetch();
    $sucursal = $sdatosSuc['nombreSucur'];


    $ventas_sql = "select DISTINCT idVenta, nit, nombreCliente, sum(cantidad)as kilos from z_reporte_ventas where (fecha >= ? and fecha <= ?) group by idVenta";
    $com = Database::getInstance()->getDb()->prepare($ventas_sql);
    $com->execute([$fechaI, $fechaF]);

//echo $consulta;
    function Invertir_Fecha($fecha)
    {
        $Y = substr($fecha, 0, 4);
        $M = substr($fecha, 5, 2);
        $D = substr($fecha, 8);
        $FechaInvertida = $D . "/" . $M . "/" . $Y;
        return $FechaInvertida;
    }

    function obtener_mes($valor)
    {
        $result = '';
        switch ($valor) {
            case '01':
                $result = 'Enero';
                break;
            case '02':
                $result = 'Febrero';
                break;
            case '03':
                $result = 'Marzo';
                break;
            case '04':
                $result = 'Abril';
                break;
            case '05':
                $result = 'Mayo';
                break;
            case '06':
                $result = 'Junio';
                break;
            case '07':
                $result = 'Julio';
                break;
            case '08':
                $result = 'Agosto';
                break;
            case '09':
                $result = 'Septiembre';
                break;
            case '10':
                $result = 'Octubre';
                break;
            case '11':
                $result = 'Noviembre';
                break;
            case '12':
                $result = 'Diciembre';
                break;
        }
        return $result;
    }


    if ($com->rowCount() > 0) {
        ?>
        <div id="tabla" class="table-responsive">
            <form id="facturasVentas" action="VistaFacturaVentas.php" method="POST">
                <div class="form-inline">
                    <a type="button" target="_blank" id="btnImprimirtxt" class="btn btn-success shiny"
                       href="javascript: exportarVentasExcel(<?= $fechaI ?>, <?= $fechaF ?>)" style="font-size: 12pt">Exportar a Excel</a> <!--exportarVentasExcel.php?fi=--><?//= $fechaI ?><!--&ff=--><?//= $fechaF ?>
                </div>
                <!--</center>-->
                <br>
                <?php
                while ($row = $com->fetch()) {
                    $idVenta = $row['idVenta'];
                    ?>
                    <table class="table table-responsive table-bordered" style="width: 60%;">
                        <thead>
                        <!--Fila de titulos de columnas-->
                        <tr>
                            <th class="text-center">
                                VENTA
                            </th>
                            <th class="text-center" >
                                NIT
                            </th>
                            <th class="text-center">
                                CLIENTE
                            </th>
                            <th class="text-center">
                                CANT. KILOGRAMOS VENDIDOS
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center">
                                <?= $idVenta ?>
                            </td>
                            <td class="text-center">
                                <?= $row['nit'] ?>
                            </td>
                            <td class="text-center">
                                <?= $row['nombreCliente'] ?>
                            </td>
                            <td class="text-center" style="font-size: 12pt; ">
                                <?= $row['kilos'] ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">
                                Codigo Producto
                            </th>
                            <th class="text-center">
                                Producto
                            </th>
                            <th class="text-center">
                                Cantidad
                            </th>
                            <th class="text-center">
                                Precio
                            </th>
                            <th class="text-center">
                                SubTotal
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 1;
                        $total = 0;
                        $ventas = "select * from z_reporte_ventas where (fecha >= ? and fecha <= ?) and idVenta = ?";
                        $com1 = Database::getInstance()->getDb()->prepare($ventas);
                        $com1->execute([$fechaI, $fechaF, $idVenta]);
                        while ($row1 = $com1->fetch()) {
                            ?>
                            <tr>
                                <td class="text-center">
                                    <?= $row1['codigoProducto'] ?>
                                </td>
                                <td class="text-center">
                                    <?= $row1['nombreComercial'] ?>
                                </td>
                                <td class="text-center">
                                    <?= $row1['cantidad'] ?>
                                </td>
                                <td class="text-center">
                                    <?= $row1['precioVenta'] ?>
                                </td>
                                <td class="text-center">
                                    <?= round($row1['cantidad'] * $row1['precioVenta'],2) ?>
                                </td>
                            </tr>
                            <?php
                            $subtotal = $row1['montoSubTotal'];
                            $total = $row1['montoTotal'];
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3" class="invisible bg-snow"></td>
                            <td class="text-right"><font size="3"><strong>SubTotal:</strong></font></td>
                            <td class="text-center"><font size="3"> <?= number_format($subtotal, 2) ?> </font></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="invisible bg-snow"></td>
                            <td class="text-right"><font size="3"><strong>Total:</strong></font></td>
                            <td class="text-center"><font size="3"> <?= number_format($total, 2) ?> </font></td>
                        </tr>
                        </tfoot>
                    </table>
                    <br>
                    <?php
                }
                ?>
            </form>
        </div>
        <?php

    }
}
?>
