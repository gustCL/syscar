<?php
require_once '../datos/Database.php';
$mes = $_GET['m'];
$anio = $_GET['a'];

$consulta = 'SELECT * FROM Empresa';
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$empresa = $comando->fetch();
$nombreEmp = $empresa['nombreEmpresa'];
$nit = $empresa['nit'];

$consulta1 = "select * from LibroVentasIva where date_format(fecha, '%m') = $mes and date_format(fecha, '%Y') = $anio order by nroFactura asc";
$con = Database::getInstance()->getDb()->prepare($consulta1);
$con->execute();

function Invertir_Fecha($fecha) {
    $Y = substr($fecha, 0, 4);
    $M = substr($fecha, 5, 2);
    $D = substr($fecha, 8);
    $FechaInvertida = $D . "/" . $M . "/" . $Y;
    return $FechaInvertida;
}

function SacarMesAnio($fecha) {
    $Y = substr($fecha, 0, 4);
    $M = substr($fecha, 5, 2);
    $mesAnio = $M . $Y;
    return $mesAnio;
}

if ($con->rowCount() > 0) {
    require_once 'Classes/PHPExcel.php';
    $objPHPExcel = new PHPExcel();

    //Informacion del excel
    $objPHPExcel->
            getProperties()
            ->setCreator("wwww.softcem.com")
        ->setLastModifiedBy("Ing. Ariel Fernando Aparicio Delgadillo")
        ->setTitle("LCV compras y ventas")
        ->setSubject("Libro Ventas")
        ->setDescription("Documento generado por Factura Facil")
        ->setKeywords("Ing. Ariel Fernando Aparicio Delgadillo")
        ->setCategory("ventas");
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B1', $nombreEmp)
            ->setCellValue('B3', "NRO")
            ->setCellValue('C3', "FECHA")
            ->setCellValue('D3', "NRO FACTURA")
            ->setCellValue('E3', "NRO AUTORIZACION")
            ->setCellValue('F3', "ESTADO")
            ->setCellValue('G3', "NIT")
            ->setCellValue('H3', "NOMBRE CLIENTE")
            ->setCellValue('I3', "MONTO TOTAL")
            ->setCellValue('J3', "IMPORTE ICE")
            ->setCellValue('K3', "IMPORTE EXECTO")
            ->setCellValue('L3', "VENTAS GRABADAS A TASA CERO")
            ->setCellValue('M3', "SUBTOTAL")
            ->setCellValue('N3', "DESCUENTOS Y BONIDICACIONES")
            ->setCellValue('O3', "IMPORTE BASE PARA DEBITO FISCAL")
            ->setCellValue('P3', "DEBITO FISCAL")
            ->setCellValue('Q3', "CODIGO DE CONTROL");
    $i = 4;
$j = 1;
    $ventasGrabadas = "0.00";
    $descuentos = "0.00";
    while ($registro = $con->fetch()) {
        $fechaN = Invertir_Fecha($registro['fecha']);
        $subtotal = $registro['montoTotal'] - $registro['impICE'] - $registro['impExce'] - $ventasGrabadas;
        $base = $subtotal - $descuentos;
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B' . $i, $j)
                ->setCellValue('C' . $i, $fechaN)
                ->setCellValue('D' . $i, $registro['nroFactura'])
                ->setCellValue('E' . $i, $registro['nroAutorizacion'])
                ->setCellValue('F' . $i, $registro['estado'])
                ->setCellValue('G' . $i, $registro['nit'])
                ->setCellValue('H' . $i, $registro['nombreCliente'])
                ->setCellValue('I' . $i, $registro['montoTotal'])
                ->setCellValue('J' . $i, $registro['impICE'])
                ->setCellValue('K' . $i, $registro['impExce'])
                ->setCellValue('L' . $i, $ventasGrabadas)
                ->setCellValue('M' . $i, $subtotal)
                ->setCellValue('N' . $i, $descuentos)
                ->setCellValue('O' . $i, $base)
                ->setCellValue('P' . $i, $registro['Iva'])
                ->setCellValue('Q' . $i, $registro['codigoControl']);
        $i++;
	$j++;
    }
}
$nb_archivo="ventas_".$mes.$anio."_".$nit.".xls";
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$nb_archivo.'"');
header('Cache-Control: max-age=0');
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
?>