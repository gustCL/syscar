<?php
require_once '../datos/Database.php';
require_once '../extras/PHPPaging.lib/PHPPaging.lib.php';

session_start();
$mes = $_GET['periodoMes'];
$anio = $_GET['periodoAnio'];
$opcion = 1;

//echo $mes."<br>";
//echo $anio;

if ($mes == 0 and $anio == "" or $mes == 0 and $anio != "" or $mes != 0 and $anio == "") {
    echo 'error1';
    exit();
} else {
    $consulta = 'SELECT * FROM Empresa';
    $comando = Database::getInstance()->getDb()->prepare($consulta);
    $comando->execute();
    $empresa = $comando->fetch();
    $nombreEmp = $empresa['nombreEmpresa'];
    $nit = $empresa['nit'];
    $userMaster = $_SESSION['userMaster'];
    $consulta1 = "SELECT * FROM Sucursal WHERE idSucursal =?";
    $comando1 = Database::getInstance()->getDb()->prepare($consulta1);
    $comando1->execute([$userMaster['idSucursal']]);
    $sdatosSuc = $comando1->fetch();
    $sucursal = $sdatosSuc['nombreSucur'];
    if ($opcion == 1) {
        //VENTAS
        $ventas_sql = "select * from LibroVentasIva where date_format(fecha, '%m') = $mes and date_format(fecha, '%Y') = $anio and idSucursal = ".$userMaster['idSucursal']." order by nroFactura asc";
        $com = Database::getInstance()->getDb()->prepare($ventas_sql);
        $com->execute();
    } else {
        // COMPRAS
        $compras_sql = "select * from LibroventasIva where date_format(fecha, '%m') = ? and date_format(fecha, '%Y') = ?";
        $com = Database::getInstance()->getDb()->prepare($compras_sql);
        $com->execute([$mes, $anio]);
    }

//echo $consulta;
    function Invertir_Fecha($fecha)
    {
        $Y = substr($fecha, 0, 4);
        $M = substr($fecha, 5, 2);
        $D = substr($fecha, 8);
        $FechaInvertida = $D . "/" . $M . "/" . $Y;
        return $FechaInvertida;
    }

    function obtener_mes($valor)
    {
        $result = '';
        switch ($valor) {
            case '01':
                $result = 'Enero';
                break;
            case '02':
                $result = 'Febrero';
                break;
            case '03':
                $result = 'Marzo';
                break;
            case '04':
                $result = 'Abril';
                break;
            case '05':
                $result = 'Mayo';
                break;
            case '06':
                $result = 'Junio';
                break;
            case '07':
                $result = 'Julio';
                break;
            case '08':
                $result = 'Agosto';
                break;
            case '09':
                $result = 'Septiembre';
                break;
            case '10':
                $result = 'Octubre';
                break;
            case '11':
                $result = 'Noviembre';
                break;
            case '12':
                $result = 'Diciembre';
                break;
        }
        return $result;
    }

    $pagina = new PHPPaging;
    if ($com->rowCount() > 0) {
        $pagina->agregarConsulta($ventas_sql);
        $pagina->modo('desarrollo');
        $pagina->verPost(true);
        $pagina->porPagina(10);
        $pagina->paginasAntes(4);
        $pagina->paginasDespues(3);
        $pagina->linkSeparador(' - ');
        $pagina->div('agrega-registros');
        $pagina->linkSeparadorEspecial('...');
        $pagina->ejecutar();
        switch ($opcion) {
            case 1:
                ?>
                <div id="tabla" class="table-responsive">
                    <form id="facturasVentas" action="VistaFacturaVentas.php" method="POST">
                        <div class="form-inline">
                            <a type="button" target="_blank" id="btnImprimirtxt" class="btn btn-danger shiny"
                               href="exportarTxtVentas.php?m=<?= $mes ?>&a=<?= $anio ?>">Exportar a TXT</a>
                            <a type="button" id="btnImprimirexce" class="btn btn-success shiny" target="_blank"
                               href="exportarExcelVentas.php?m=<?= $mes ?>&a=<?= $anio ?>">Exportar a Excel</a>
                        </div>
                        <!--</center>-->
                        <br>
                        <table cellpadding="3" style="width: 60%;"
                               class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <th style="text-align: center; background: #D0D0D0">
                                INFORMACION DEL REPORTE
                            </th>
                            <th style="text-align: center; background: #D0D0D0">
                                PERIODO FISCAL
                            </th>
                            <th style="background: #D0D0D0;text-align: center;">
                                NIT
                            </th>
                            <th style="background: #D0D0D0;text-align: center;">
                                INFORMACION EMPRESA
                            </th>
                            <th style="background: #D0D0D0;text-align: center;">
                                SUCURSAL
                            </th>
                            </thead>
                            <tbody style="text-align: center;">
                            <tr>
                                <td>
                                    Libro de Ventas I.V.A.
                                </td>
                                <td>
                                    <?php echo obtener_mes($mes) . " del ";
                                    echo $anio; ?>
                                </td>
                                <td>
                                    <?= $nit ?>
                                </td>
                                <td align="center">
                                    <?= $nombreEmp ?>
                                </td>
                                <td align="center">
                                    <?= $sucursal ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-responsive table-striped table">
                            <thead>
                            <!--Fila de titulos de columnas-->
                            <tr>
                                <th style="background: #D0D0D0">
                                    Nit Del Comprador
                                </th>
                                <th style="background: #D0D0D0">
                                    Nombre O Razon Social
                                </th>
                                <th style="background: #D0D0D0">
                                    Nro.De Factura
                                </th>
                                <th style="background: #D0D0D0">
                                    Nro.De Autorizacion
                                </th>
                                <th style="background: #D0D0D0">
                                    Fecha
                                </th>
                                <th style="background: #D0D0D0">
                                    Total Factura
                                </th>
                                <th style="background: #D0D0D0">
                                    Importe I.C.E.
                                </th>
                                <th style="background: #D0D0D0">
                                    Importe Excento
                                </th>
                                <th style="background: #D0D0D0">
                                    Importe Neto
                                </th>
                                <th style="background: #D0D0D0">
                                    Debito Fiscal
                                </th>
                                <th style="background: #D0D0D0">
                                    Estado
                                </th>
                                <th style="background: #D0D0D0">
                                    Codigo De Control
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            $total = 0;
                            while ($row = $pagina->fetchResultado()) {
                                ?>
                                <tr>
                                    <td>
                                        <?= $row['nit'] ?>
                                    </td>
                                    <th>
                                        <?= $row['nombreCliente'] ?>
                                    </th>
                                    <td>
                                        <?= $row['nroFactura'] ?>
                                    </td>
                                    <td>
                                        <?= $row['nroAutorizacion'] ?>
                                    </td>
                                    <td>
                                        <?= Invertir_Fecha($row['fecha']) ?>
                                    </td>
                                    <td>
                                        <?= $row['montoTotal'] ?>
                                    </td>
                                    <td>
                                        <?= $row['impICE'] ?>
                                    </td>
                                    <td>
                                        <?= $row['impExce'] ?>
                                    </td>
                                    <td>
                                        <?= $row['montoTotal'] ?>
                                    </td>
                                    <td>
                                        <?= $row['Iva'] ?>
                                    </td>
                                    <th>

                                        <?= $row['estado'] ?>
                                    </th>
                                    <td>
                                        <?= $row['codigoControl'] ?>
                                    </td>
                                </tr>
                                <?php
                                $total = $total + $row['montoTotal'];
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="7" class="invisible bg-snow"></td>
                                <td><font size="3"><strong>Total:</strong></font></td>
                                <td class="text-center"><font size="3"> <?= number_format($total, 2) ?> </font></td>
                            </tr>
                            <tr>
                                <td colspan="7">Número de página: <?= $pagina->fetchNavegacion(); ?></td>
                            </tr>
                            </tfoot>
                        </table>
                        <input type="text" id="dato1" name="anio" value="<?= $mes ?>" hidden="hidden"/>
                        <input type="text" id="dato2" name="mes" value="<?= $anio ?>" hidden="hidden"/>
                    </form>
                </div>
                <?php
                break;
            case 2:
                ?>
                <div id="tabla" class="table-responsive">
                    <form id="facturasVentas" action="VistaFacturaCompras.php" method="POST">
                        <div class="form-inline">
                            <a type="button" id="btnImprimirtxt" class="btn btn-danger shiny"
                               href="exportarTxtCompras.php?m=<?= $mes ?>&a=<?= $anio ?>">Exportar a TXT</a>
                            <a type="button" id="btnImprimirexce" class="btn btn-success shiny"
                               href="exportarExcelCompras.php?m=<?= $mes ?>&a=<?= $anio ?>">Exportar a Excel</a>
                        </div>
                        <!--</center>-->
                        <br>
                        <table cellpadding="3" style="width: 60%;"
                               class="table table-responsive tab-content table-hover table-bordered">
                            <thead>
                            <th style="text-align: center; background: #D0D0D0">
                                INFORMACION DEL REPORTE
                            </th>
                            <th style="text-align: center; background: #D0D0D0">
                                PERIODO FISCAL
                            </th>
                            <th style="background: #D0D0D0;text-align: center;">
                                NIT
                            </th>
                            <th style="background: #D0D0D0;text-align: center;">
                                INFORMACION EMPRESA
                            </th>
                            </thead>
                            <tbody style="text-align: center;">
                            <tr>
                                <td>
                                    Libro de Compras I.V.A.
                                </td>
                                <td>
                                    <?php echo obtener_mes($mes) . " del ";
                                    echo $anio; ?>
                                </td>
                                <td>
                                    <?= $nitEmpresa ?>
                                </td>
                                <td align="center">
                                    <?= $NombreEmpresa ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-responsive table-striped table">
                            <thead>
                            <!--Fila de titulos de columnas-->
                            <tr>
                                <th style="background: #D0D0D0">
                                    Nit Del Comprador
                                </th>
                                <th style="background: #D0D0D0">
                                    Nombre O Razon Social
                                </th>
                                <th style="background: #D0D0D0">
                                    Nro.De Factura
                                </th>
                                <th style="background: #D0D0D0">
                                    Nro.De Autorizacion
                                </th>
                                <th style="background: #D0D0D0">
                                    Fecha
                                </th>
                                <th style="background: #D0D0D0">
                                    Total Factura
                                </th>
                                <th style="background: #D0D0D0">
                                    Importe I.C.E.
                                </th>
                                <th style="background: #D0D0D0">
                                    Importe Excento
                                </th>
                                <th style="background: #D0D0D0">
                                    Importe Neto
                                </th>
                                <th style="background: #D0D0D0">
                                    Debito Fiscal
                                </th>
                                <th style="background: #D0D0D0">
                                    Codigo De Control
                                </th>
                                <th style="background: #D0D0D0">
                                    Estado
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            $total = 0;
                            while ($row = $com->fetch()) {
                                ?>
                                <tr>
                                    <td>
                                        <?= $row['nit'] ?>
                                    </td>
                                    <th>
                                        <?= $row['nombre'] ?>
                                    </th>
                                    <td>
                                        <?= $row['nroFactura'] ?>
                                    </td>
                                    <td>
                                        <?= $row['nroAutorizacion'] ?>
                                    </td>
                                    <td>
                                        <?= Invertir_Fecha($row['fechaCompra']) ?>
                                    </td>
                                    <td>
                                        <?= $row['importeTotal'] ?>
                                    </td>
                                    <td>
                                        <?= $row['importeICE'] ?>
                                    </td>
                                    <td>
                                        <?= $row['importeExcento'] ?>
                                    </td>
                                    <td>
                                        <?= $row['importeNeto'] ?>
                                    </td>
                                    <td>
                                        <?= $row['creditoFiscal'] ?>
                                    </td>
                                    <th>
                                        <?= $row['codigoControl'] ?>
                                    </th>
                                    <td>
                                        <?= $row['estado'] ?>
                                    </td>
                                </tr>
                                <?php
                                $total = $total + $row['importeTotal'];
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="7" class="invisible bg-snow"></td>
                                <td><font size="3"><strong>Total:</strong></font></td>
                                <td class="text-center"><font size="3"> <?= number_format($total, 2) ?> </font></td>
                            </tr>
                            </tfoot>
                        </table>

                        <input type="text" id="tipod" name="tipod" value="<?= $opcion ?>" hidden="hidden"/>
                        <input type="text" id="dato1" name="anio" value="<?= $mes ?>" hidden="hidden"/>
                        <input type="text" id="dato2" name="mes" value="<?= $anio ?>" hidden="hidden"/>
                    </form>
                </div>
                <?php
                break;
        }
    } else {
        echo '<table class="table table-striped table-bordered table-hover" >
                                            <thead>
                                                <!--Fila de titulos de columnas-->
                                                <tr>
                                                    <th class="hidden-xs">
                                                        NRO
                                                    </th>
                                                    <th class="hidden-xs">
                                                        CODIGO VENTA
                                                    </th>
                                                    <th class="hidden-xs">
                                                        FECHA VENTA
                                                    </th>
                                                    <th class="hidden-xs">
                                                        NIT DEL COMPRADOR
                                                    </th>
                                                    <th>
                                                        NOMBRE O RAZON SOCIAL
                                                    </th>
                                                    <th>
                                                        NRO.DE FACTURA
                                                    </th>
                                                    <th class="hidden-xs">
                                                        NRO.DE AUTORIZACION 
                                                    </th>
                                                    <th class="hidden-xs">
                                                        CODIGO DE CONTROL
                                                    </th>
                                                    <th class="hidden-xs">
                                                        TOTAL FACTURA
                                                    </th>
                                                </tr>
                                            </thead>    
                                        </table>
                                        La consulta no genero resultados.';
    }
}
?>