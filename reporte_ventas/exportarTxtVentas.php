<?php
require_once '../datos/Database.php';
$mes = $_REQUEST['m'];
$anio = $_REQUEST['a'];


$consulta = "select * from LibroVentasIva where date_format(fecha, '%m') = $mes and date_format(fecha, '%Y') = $anio order by nroFactura asc";
$con = Database::getInstance()->getDb()->prepare($consulta);
$con->execute();

function Invertir_Fecha($fecha)
{
    $Y = substr($fecha, 0, 4);
    $M = substr($fecha, 5, 2);
    $D = substr($fecha, 8);
    $FechaInvertida = $D . "/" . $M . "/" . $Y;
    return $FechaInvertida;
}

//
//if(!file_exists("C:/reportes")){
//$dir = "C:/reportes";
//mkdir($dir,0777);
//}
$i = 1;

$consulta = 'SELECT * FROM Empresa';
$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();
$empresa = $comando->fetch();
$nombreEmp = $empresa['nombreEmpresa'];
$nit = $empresa['nit'];

$nb_archivo = "ventas_" . $mes . $anio . "_" . $nit . ".txt";
header('Content-Disposition: attachment;filename="' . $nb_archivo . '"');
ob_start();
include($enlace);

$linea = "";
$i = 1;
try {
    while ($filac = $con->fetch()) {

        $nit = $filac['nit'];
        $nom = $filac['nombreCliente'];
        $numfac = $filac['nroFactura'];
        $numauto = $filac['nroAutorizacion'];
        $fecomp = $filac['fecha'];
        $monto = $filac['montoTotal'];
        $neto = $filac['impNeto'];
        $iva = $filac['Iva'];
        $impICE = $filac['impICE'];
        $impExc = $filac['impExce'];
        $codControl = $filac['codigoControl'];
        $estado = $filac['estado'];
        $ventasGrabadas = "0.00";
        $descuentos = "0.00";
        $subtotal = $monto - $impICE - $impExc - $ventasGrabadas;
        $baseIva = $subtotal - $descuentos;
        //echo " - ";
        //$tot_venta_por = $tot_venta_por + $porci;
        $new_fec = Invertir_Fecha($fecomp);

        $linea .= $i . "|" . $new_fec . "|" . $numfac . "|" . $numauto . "|" . $estado . "|" . $nit . "|" . $nom . "|" . $monto . "|" . $impICE . "|" . $impExc . "|" . $ventasGrabadas . "|" . $subtotal . "|" . $descuentos . "|" . $baseIva . "|" . $iva . "|" . $codControl . "\r\n";
        $i++;
    }
    $content = ob_get_contents();
    ob_end_clean();
    header('Content-Type: application/txt');
    header('Content-Disposition: attachment;filename='.$nb_archivo.'');
    header('Pragma:no-cache');
    //header ("Content-Length: ".strlen($linea));
    echo $linea;

    exit();

} catch (PDOException $ex) {
    echo "Error: " . $ex;
}
?>