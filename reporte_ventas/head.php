<?php
session_start();
if ($_SESSION['logueado'] != 'SI') {
    header('Location:../inicio');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Farmacia Jafeth SRL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="../assets/img/icono.gif" type="image/x-icon" media="all">

        <!--Basic Styles-->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet"   media="all"/>
        <link id="bootstrap-rtl-link" href="#" rel="stylesheet" />
        <link href="../assets/css/font-awesome.min.css" rel="stylesheet"   media="all"/>
        <link href="../assets/css/weather-icons.min.css" rel="stylesheet"   media="all"/>

        <!--Fonts-->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

        <!--Beyond styles-->
        <link href="../assets/css/beyond.min.css" rel="stylesheet"   media="all" />
        <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
        <script src="../assets/js/skins.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="../assets/img/icono.gif" type="image/x-icon" media="all">

        <!-- LIBRERIA PARA EXPORTAR A EXCEL -->
        <script type="text/javascript" src="jquery-1.3.2.min.js"></script>
        <script language="javascript">
            $(document).ready(function () {
                $(".botonExcel").click(function (event) {
                    $("#datos_a_enviar").val($("<div>").append($("#Exportar_a_Excel").eq(0).clone()).html());
                    $("#FormularioExportacion").submit();
                });
            });
        </script>
        <!---------------------------------------------------------------->
        <!--lo que aumente-->
        <link href="../css/estilo.css" rel="stylesheet">
        <script src="../js/jquery.js"></script>
        <!-- finnnnn--->  

        <!--Fonts-->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

        <!--Beyond styles-->
        <link href="../assets/css/beyond.min.css" rel="stylesheet"   media="all" />
        <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
        <script src="../assets/js/skins.min.js"></script>
        
        <!-- LIBRERIA DEL AUTOCOMPLETADO -->
        <script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
        <script src="../js/autocompletado/jquery-ui-1.10.3.custom.min.js"></script> 
        <link rel="stylesheet" href="../js/autocompletado/jquery-ui-1.10.3.custom.min.css">   

        <!-- PROBANDO LINKS PARA LA IMPRESION DE PANTALLA -->
        <link rel="stylesheet" href="style.css" type="text/css" media="screen" />

        <link rel="stylesheet" href="print.css" type="text/css" media="print" />
        <!--Para reportes de compras de laboratorio-->
        <script src="../reporte_ventas/jreporte_ventas.js"></script>

    </head>
    <?php
    require_once '../header_footer/header.php';
    ?>

