<?php
require_once '../datos/Database.php';
$tipo = $_POST['tipod'];
if ($tipo == 'fechas'){
    $d1= $_POST['dato1'];
    $d2 = $_POST['dato2'];
    $consulta = "select * from ReporteFacturaVentas where (fecha >= '" . $d1 . "%') and (fecha <= '" .$d2 . "%')";
}else{
$d1 = $_POST['dato1'];
$d2 = $_POST['dato2'];
    $consulta = "select * from ReporteFacturaVentas where date_format(fecha, '%m') ='" . $d1 . "' and date_format(fecha, '%Y') ='" . $d2 . "'";
}

$comando = Database::getInstance()->getDb()->prepare($consulta);
$comando->execute();

function Invertir_Fecha($fecha) {
    $Y = substr($fecha, 0, 4);
    $M = substr($fecha, 5, 2);
    $D = substr($fecha, 8);
    $FechaInvertida = $D . "/" . $M . "/" . $Y;
    return $FechaInvertida;
}

$actual = Invertir_Fecha(date("Y-m-d"));
//EMPRESA
$consulta = "select nombreEmpresa, nit from Empresa ";
$resultado = Database::getInstance()->getDb()->prepare($consulta);
$resultado->execute();
$empresa = $resultado->fetch();

//require '../fpdf/fpdf.php';
//
//$fpdf = new FPDF("P", "pt", "mm","Letter");

?>
<html> 
    <head> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="all">
        <link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen">
        <!--<link rel="stylesheet" type="text/css" href="../css/print/print.css" media="print">-->
        <link href="../reporteCompras/printListaTransac.css" rel="stylesheet" type="text/css" media="print" />  
        <!--<link href="../css/style.css" rel="stylesheet" media="all">-->
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet"  media="all">
        <script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="jPedidos.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- USO DE LA LIBRERIA PARA EXPORTAR A EXCEL-->
        <script type="text/javascript" src="jquery-1.3.2.min.js"></script>
        <link href="estiloReportes.css" rel="stylesheet" type="text/css">
        <script language="javascript">
            $(document).ready(function() {
                $(".botonExcel").click(function(event) {
                    $("#datos_a_enviar").val($("<div>").append($("#Exportar_a_Excel").eq(0).clone()).html());
                    $("#FormularioExportacion").submit();
                });
            });
        </script>
    </head> 
    <body> 
        <script language="JavaScript">
            $(document).ready(function() {
                doPrint();
            });
            function doPrint() {
                //document.all.item("mostrarUser").style.visibility='visible'; 
                window.print()();
                //document.all.item("mostrarUser").style.visibility='hidden';
            }
            function volver(dato) {
                location.href = "index.php";
            }
        </script>
        <div id="leteral"></div>
        <div id="noprint">
            <table>
                <tr>
                    <td>
                        <button type="button" name="volver" id="volver" onclick="volver()">Volver</button>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <button type="button" onclick="doPrint()" >Imprimir</button>
                    </td>
<!--                    <td>
                        <a type="button" class="botonExcel">Exportar a excel</a>
                        <link rel="alternate" media="print" href="nota_pedido.xlsx"/>
                    </td>-->
                </tr>
            </table> 
        </div>
        <div id="hoja" style="height: auto"> 
            <div id="mostrarUser">

<!--                <label><script type="text/javascript">
                    //<![CDATA[
                    function makeArray() {
                        for (i = 0; i < makeArray.arguments.length; i++)
                            this[i + 1] = makeArray.arguments[i];
                    }
                    var months = new makeArray('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                            'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                    var days = new makeArray('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes',
                            'Sabado');
                    var date = new Date();
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var yy = date.getYear();
                    var year = (yy < 1000) ? yy + 1900 : yy;
                    document.write(days[date.getDay() + 1] + " " + day + " de " + months[month] + " del " + year + " " + date.getHours() + ":" + date.getMinutes());
                    //]]>
                    </script></label>-->
            </div>
            <div class="widget">
                <div id="encabezado" >
                    <div id="logo" >
                        <img width="100" height="100" src="..\..\farmacia\assets\img\Logo1.png"/>
                    </div>
                    <div id="titulo">
                        <center>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="right">
                                <font size='6'><center><strong><label for="user_name">FACTURAS VENTAS</label></strong></center></font><br>
                                            <font size='5'><strong><center><label for="user_name"><?= $empresa['nombreEmpresa'] ?></label></center></strong></font><br>
                                <font size='4'><center><label for="user_name">NIT: <?= $empresa['nit'] ?></label></center></font>

                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>
                    <div id="inf">
                        <center>
                            <table >
                                <tbody>
                                    <tr class="">
                                        <td>

                                            <strong><label for="user_name">Fecha: </label></strong>

                                        </td>
                                        <td >
                                            <fieldset>
                                                <label for="user_name"><?= $actual ?></label>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>

                </div>

                <div align="center" id="informacion">
                    <div id="content">
                        <table class="tabla" border="1" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <?php if ($tipo == "fechas"){ ?>
                                    <th>
                                        <strong><label for="user_name">Corresponde a las Fechas De:</label> <?= $d1 ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <strong><label for="user_name">Hasta:</label></strong> <?= $d2 ?><br></strong>
                                    </th>
                                    <?php }  else {?>
                                    <th>
                                        <strong><label for="user_name">Corresponde Al Mes:</label> <?= $d1 ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <strong><label for="user_name">Del Año:</label></strong> <?= $d2 ?><br></strong>
                                    <?php } ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <br>
                    <div>
                        <table class="tabla" border="0" cellpadding="0" cellspacing="0" >
                            <thead>
                                <tr>
                            <th >
                                Nro
                            </th>
                            <th>
                                Fecha Venta    
                            </th>
                            <th >
                                Autorizacion 
                            </th>
                            <th>
                                Factura
                            </th>
                            <th >
                                Codigo De Control
                            </th>
                            <th >
                                Nit Cliente
                            </th>
                            <th>
                                Razon Social
                            </th>
                            <th >
                                Total Factura
                            </th>
                        </tr>
                            </thead>
                            <tbody>
                                <?php
                        $i = 1;
                        while ($row = $comando->fetch()) {
                            ?>
                                <tr style="border: 1px solid black;">
                                    <td style="border-right: 1px solid black" id = "" name="" class="text-center" >
                                    <?= $i ?>
                                </td>
                                <td style="border-right: 1px solid black; padding-right: 1%" class="text-right">
                                    <?php $fechainv = Invertir_Fecha($row['fecha']);
                                    echo $fechainv;?>
                                </td>
                                <td style="border-right: 1px solid black; padding-right: 1%" class="text-right">
                                    <?= $row['Autorizacion'] ?>
                                </td>
                                <td style="border-right: 1px solid black; padding-right: 1%" class="text-right">
                                    <?= $row['nroFactura'] ?>
                                </td>
                                <td style="border-right: 1px solid black; padding-right: 1%" class="text-right">
                                    <?= $row['codigoControl'] ?>
                                </td>
                                <td style="border-right: 1px solid black; padding-right: 1%" class="text-right">
                                    <?= $row['nit'] ?>
                                </td>
                                <td style="border-right: 1px solid black; padding-right: 1%" class="text-right">
                                    <?= $row['nombreCliente'] ?>
                                </td>
                                <td style="border-right: 1px solid black; padding-right: 1%" class="text-right">    
                                    <?= $row['montoTotal'] ?>
                                </td>    
                            </tr>  
                            <?php
                            $i++;
                        }
                        ?>
                            </tbody>
                        </table>
                    </div>
<!--                    <div style="border: 1px solid black; width: 40%; height: 12%; margin-top: 2%; " >
                        <br>
                        <strong><br>
                        _______________________<br>
                        ENTREGADO POR:</strong>
                    </div>-->
                </div>
                <form action="ficheroExcel.php" method="post" id="FormularioExportacion">
                    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                </form>
                </body>
                </html>
             
