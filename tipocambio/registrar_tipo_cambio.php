<?php
    session_start();
    if ($_SESSION['logueado'] != 'SI') {
        header('Location: ../inicio');
    }

    require_once '../datos/Database.php';

    try {
        $tipoCambioCompra = $_POST['valor-compra'];
        $tipoCambioVenta = $_POST['valor-venta'];
        $tipoCambioUFV = $_POST['ufv'];

        $id_usuario = $_SESSION['userMaster']['idUsuario'];

        ini_set("date.timezone", "America/La_Paz");
        $fecha = date('Y-m-d');
        $hora = date('H:i:s');

        // Al insertar un nuevo tipo de cambio, activarlo
        $consultaP = "UPDATE TipoCambio SET estado=0 WHERE estado=1";
        $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
        $comandoP->execute();
        //realizamos la inserccion
        $consultaP = "INSERT INTO TipoCambio(tipoCambioCompra,tipoCambioVenta,tipoCambioUFV,fecha,hora,idUsuario,estado) VALUES ('$tipoCambioCompra','$tipoCambioVenta','$tipoCambioUFV','$fecha','$hora','$id_usuario',1)";
        $comandoP = Database::getInstance()->getDb()->prepare($consultaP);
        $comandoP->execute();

        echo 'exito';
        exit();
    } catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }
?>
