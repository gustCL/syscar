$(document).ready(function() {
    //$('#div-tabla').load('obtener_tipo_cambio.php');
});


function buscarTipoCambio() {
    var fechaInicio = $('#fecha-inicio').val();
    $.ajax({
        type: 'POST',
        url: 'buscar_tipo_cambio.php',
        data: 'fecha_inicio=' + fechaInicio,
        success: function (result) {
            $('#div-tabla').html(result);
        }
    });
}

// Registrar un nuevo tipo de cambio
function registrarTipoCambio() {
    var frm = $("#frm-registrar-tipo-cambio").serialize();
    $.ajax({
        url: 'registrar_tipo_cambio.php',
        data: frm,
        type: 'POST',
        success: function (result) {
            if (result == 'exito') {
                alertify.success('Tipo de cambio registrado');
                $('#div-tabla').load('obtener_tipo_cambio.php');
            }
        }
    });
}

// Activa el tipo de cambio
function cambiarEstado(id) {
    $.ajax({
        type: 'POST',
        url: 'cambiar_estado.php',
        data: {id: id},
        success: function() {
            //$('#div-tabla').load('obtener_tipo_cambio.php');
            obtenerTodo();
        }
    });
}

// Ver todos los tipos de cambio registrados
function obtenerTodo() {
    $('#div-tabla').load('obtener_tipo_cambio.php');
}