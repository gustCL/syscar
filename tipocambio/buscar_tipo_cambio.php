<?php

    require_once '../extras/config.php';
    require_once '../extras/notorm/notorm.lib.php';

    $fecha = $_POST['fecha_inicio'];
    //$fecha = date("d-m-Y", strtotime($fecha));
    //$date = date_create($fecha);

    function estado($valor) {
        if ($valor == 1) {
            return 'ACTIVO';
        } else {
            return 'INACTIVO';
        }
    }

    function activo($valor) {
        if ($valor == 1) {
            return true;
        } else {
            return false;
        }
    }
?>

<?php
    $tipo_cambio = $db->TipoCambio('fecha>=?', $fecha);
    if (count($tipo_cambio) > 0) : ?>
        <script src="../extras/sortable/js/sortable.js"></script>
        <table id="tbl-tipo-cambio" class="table sortable-theme-bootstrap" data-sortable>
            <thead>
            <tr>
                <th>NÚMERO</th>
                <th>T.C.COMPRA</th>
                <th>T.C.VENTA</th>
                <th>UFV</th>
                <th>FECHA REGISTRO</th>
                <th>ESTADO</th>
                <th>OPCIÓN</th>
            </tr>
            </thead>
            <tbody>
        <?php $nro = 0;
        foreach ($tipo_cambio as $tp) : $nro++; ?>
                <tr>
                    <td><?= $nro; ?></td>
                    <td><?= $tp['tipoCambioCompra']; ?></td>
                    <td><?= $tp['tipoCambioVenta']; ?></td>
                    <td><?= $tp['tipoCambioUFV']; ?></td>
                    <td><?= date("d-m-Y", strtotime($tp['fecha'])); ?></td>
                    <td><?= estado($tp['estado']); ?></td>
                    <td>
                        <?php
                        if (activo($tp['estado'])) :    ?>
                            <input type="button" class="btn btn-danger" value="Desactivar"
                                   onclick="cambiarEstado('<?= $tp['idTipoCambio']; ?>');">
                        <?php else: ?>
                            <input type="button" class="btn btn-success" value="Activar"
                                   onclick="cambiarEstado('<?= $tp['idTipoCambio']; ?>');">
                        <?php endif; ?>
                    </td>
                </tr>
    <?php endforeach; ?>
            </tbody>
        </table>
<?php else: ?>
        <div class="alert alert-info" role="alert">
            <center>
                <h3>NO SE ENCONTRARON RESULTADOS</h3>
            </center>
        </div>
<?php endif; ?>
