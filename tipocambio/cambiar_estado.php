<?php
    require_once '../datos/Database.php';

    $id = $_POST['id'];

    $query = "UPDATE TipoCambio SET estado = 1 WHERE idTipoCambio = :id";
    $cmd = Database::getInstance()->getDb()->prepare($query);
    $cmd->bindParam(':id', $id);
    $cmd->execute();

    $query2 = "UPDATE TipoCambio SET estado = 0 WHERE idTipoCambio != :id";
    $cmd2 = Database::getInstance()->getDb()->prepare($query2);
    $cmd2->bindParam(':id', $id);
    $cmd2->execute();

    exit();
?>
