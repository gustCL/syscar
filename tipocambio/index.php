<?php
    require_once 'head.php';

    function estado($valor) {
        if ($valor == 1) {
            return 'ACTIVO';
        } else {
            return 'INACTIVO';
        }
    }

    function activo($valor) {
        if ($valor == 1) {
            return true;
        } else {
            return false;
        }
    }
?>
<div class="page-content">
    <!-- subtTitulo -->
    <div class="page-header position-relative" > 
        <div class="header-title" >
            <h1>
                <Strong>TIPO DE CAMBIO</strong>
            </h1> 
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
    </div>
    <div class="main-container container-fluid">
        <div class="page-body">
            <div class="widget-body">
                <div class="row">
                    <div class="col-md-offset-4">
                        <h3><strong>REGISTRAR TIPO DE CAMBIO</strong></h3>
                    </div>
                </div>

                <br>
                <form id="frm-registrar-tipo-cambio" name="frm-registrar-tipo-cambio" class="form-horizontal" action="javascript: registrarTipoCambio();">
                    <div class="form-group">
                        <label class="col-md-offset-2 col-md-2 control-label lead">Valor de Compra:</label>
                        <div class="col-md-2">
                            <input type="number" min="0" class="form-control" name="valor-compra" id="valor-compra" required>
                        </div>
                        <label class="col-md-2 control-label lead">Valor de Venta:</label>
                        <div class="col-md-2">
                            <input type="number" min="0" class="form-control" name="valor-venta" id="valor-venta" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-offset-2 col-md-2 control-label lead">UFV:</label>
                        <div class="col-md-2">
                            <input type="number" min="0" class="form-control" name="ufv" id="ufv" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-5">
                            <input type="submit" class="btn btn-primary" id="btn-guardar" value="Registrar">
                            <input type="reset" value="Limpiar" class="btn btn-azure">
                        </div>
                    </div>
                </form>
            </div>
            <br>

            <div class="widget-body">
                <div id="div-contenido" class="table-responsive">
                    <center><h3><strong>TIPOS DE CAMBIO REGISTRADOS</strong></h3></center>
                    <div class="form-inline">
                        <div class="form-group">
                            <div class="col-lg-3">
                                <span class="input-icon">
                                    <?php
                                    ini_set("date.timezone", "America/La_Paz");
                                    $fecha_actual = date("Y-m-d");
                                    ?>
                                    <input type="date" name="fecha-inicio" id="fecha-inicio" value="<?= $fecha_actual; ?>" class="input-sm form-control input-s-sm inline">
                                        <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <input type="button" id="btn-buscar" value="Buscar" class="btn btn-primary"
                               onclick="buscarTipoCambio();">
                        <input type="button" id="btn-ver-todo" value="Ver Todo" class="btn btn-info"
                               onclick="obtenerTodo();">
                    </div>
                    <br>
                    <div id="div-tabla">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            obtenerTodo();
        });
    </script>
</div>
<?php
    include_once '../header_footer/footer.php';
?>
