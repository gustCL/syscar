<?php
require_once ('head.php');
require_once '../datos/Database.php';

$id = $_GET['id'];
$cadenaConsultaDatos = "SELECT vc.idCredito,vc.montoContado,vsuc.nroVenta,suc.nombreSucur,cl.nombreCliente,vc.fechaVencimiento,vc.nroCuotas,vc.saldoPagar  FROM VentasCredito vc, Ventas v, Cliente cl, Sucursal suc, VentaSucursal vsuc  WHERE vc.idVenta=v.idVenta AND v.idCliente = cl.idCliente  AND vsuc.idVenta=v.idVenta AND suc.idSucursal=vsuc.idSucursal AND vc.pagado=1 AND idCredito='$id'";
$dato = Database::getInstance()->getDb()->prepare($cadenaConsultaDatos);
$dato->execute();
$dat = $dato->fetch();

///pra ver numero de cuotasa faltantes
$numeroCuotas = "SELECT COUNT(*) AS total FROM PagoCuotasCobrar pcc WHERE pcc.idCredito='$id'";
$datoCuotas = Database::getInstance()->getDb()->prepare($numeroCuotas);
$datoCuotas->execute();
$cuotas = $datoCuotas->fetch();
?>  
<div class="page-content">        
    <!-- subtTitulo -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <Strong>PAGO DE SALDO PENDIENTE</strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="widget-body control-label " >
                            <form  method="post"  class="form-horizontal" onsubmit="return validarmonto();" id="formEnviar">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                            <div class="form-group" >
                                                <label class="col-sm-2 control-label no-padding-left"  style=" font-weight: bold"  for="idcredito">Nro. de Venta:</label>
                                                <div class="col-sm-6"> 
                                                    <input type="text" class="form-control"  id="idcredito" name="idcredito" readonly="true" value="<?= $dat['idCredito'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-left strong" style="font-weight: bold" for="sucural">Sucursal:</label>
                                                <div class="col-sm-6"> 
                                                    <input type="text" class="form-control"  id="sucural" name="sucural" readonly="true" value="<?= $dat['nombreSucur'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-left"  style="font-weight: bold" for="cliente">Cliente:</label>
                                                <div class="col-sm-6"> 
                                                    <input type="text" class="form-control"  id="cliente" name="cliente" readonly="true" value="<?= $dat['nombreCliente'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-left" for="cuotafaltante" style="font-weight: bold">Cuotas Faltantes:</label>
                                                <div class="col-sm-6"> 
                                                    <input type="text" class="form-control"  id="cuotafaltante" name="cuotafaltante" readonly="true"value="<?= $dat['nroCuotas'] - $cuotas['total'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-left" style="font-weight: bold"  for="montoContado">Monto Total de Venta:</label>
                                                <div class="col-sm-6"> 
                                                    <input type="text" class="form-control"  id="montototal" name="montototal" readonly="true"value="<?= $dat['montoContado'] + $dat['saldoPagar'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-left" style="font-weight: bold" for="montocontado">Monto Pagado:</label>
                                                <div class="col-sm-6"> 
                                                    <input type="text" class="form-control"  id="montocontado" name="montocontado" readonly="true"value="<?= $dat['montoContado'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-left"  style="font-weight: bold" for="saldoporpagar">Saldo por Pagar:</label>
                                                <div class="col-sm-6"> 
                                                    <input type="text" class="form-control"  id="saldoporpagar" name="saldoporpagar" readonly="true" value="<?= $dat['saldoPagar'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-left"  style="font-weight: bold" for="montopagar">Monto a pagar:</label>
                                                <div class="col-sm-6"> 
                                                    <input  type="text" class="form-control"  id="montopagar" name="montopagar" ondragleave="false" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-left " style="font-weight: bold"  for="resumen">Resumen:</label>
                                                <div class="col-sm-6"> 
                                                    <textarea type="text" class="form-control"  id="resumen" name="resumen" ></textarea>
                                                </div>
                                            </div>
                                            <br/>
                                            <br/>
                                            <center>
                                                <div class="form-group" >
                                                    <div class="input-group">                                          
                                                        <input id="enviardatos" class="btn btn-primary" type="submit" value="Registrar pago"/>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <a href="../ventacredito/" data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancelar</a>
                                                    </div>
                                                </div>  
                                            </center>
                                        </div>  
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div> 
                </div>
            </div>
        </div>  
    </div>
</div>
<?php
require_once ('../header_footer/footer.php');
?>  

