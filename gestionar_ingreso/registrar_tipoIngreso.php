<?php
require_once '../datos/Database.php';
$nombre=$_REQUEST['modalNombre'];
$consulta ="INSERT INTO TipoIngresoCaja(idTipoIngresoCaja, nombreTipoIngresoCaja, estado) VALUES ('',?,1)";
$comando =Database::getInstance()->getDb()->prepare($consulta);
$comando->execute([$nombre]);
   
?>
<!--<div class="col-sm-3 m-b">
                    <div class="form-group">
                        <fieldset>
                            <label for="user_name"><strong>Tipo de Ingreso</strong></label>
                        <br />  
                        <select name="tipoEgreso" id="tipoEgreso" class="input-sm form-control input-s-sm inline">
                                <option value="Todos">Todos</option>
                            <?php
                            require_once '../datos/Database.php';
                            $consulta = "SELECT * FROM TipoIngresoCaja ";
                            $comando = Database::getInstance()->getDb()->prepare($consulta);
                            $comando->execute();
                            while ($fila = $comando->fetch(PDO::FETCH_ASSOC)) {?>
                                <option value="<?php echo $fila['idTipoIngresoCaja'];?>"><?php echo $fila['nombreTipoIngresoCaja'];?></option>
                               
                              <?php  }
                            ?>
                            </select>
                        </fieldset> 
                    </div>
                    </div>-->
                     <table class="table table-striped" id="TipIngreso" name="TipIngreso" >
                                    <?php  

                                    $query_tipo_comp = "SELECT * FROM TipoIngresoCaja WHERE estado = 1";
                                                             
                                    $cmd_tip_c = Database::getInstance()->getDb()->prepare($query_tipo_comp);
                                    $cmd_tip_c->execute();
                                    $row_count_dv = $cmd_tip_c->rowCount();
                                     if($row_count_dv > 0): ?>
                                                <tr>
                                                    <th><strong>Codigo</strong></th>
                                                    <th><strong>Nombre</strong></th>
                                                    <th><strong></strong></th>
                                                </tr>    
                                    <?php       while($tipo_com = $cmd_tip_c->fetch()): ?>
                                    <?php         $id = $tipo_com['idTipoIngresoCaja'];
                                        ?>
                                                    <tr>
                                                        <td><?php echo  $id ?></td>

                                                        <td><?php echo $tipo_com['nombreTipoIngresoCaja'] ;?></td>
                                                        
                                                        <td>
                                                         <a href="javascript: actuali_tipIng('<?= $id ?>');" class="btn btn-default btn-xs purple" onclick="javascript: ocultar();" ><i class="fa fa-edit"></i> Editar</a>
                                                         <a href="javascript: eliminarTipoEgreso('<?= $id ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                                                        </td>
                                                    </tr>
                                    <?php       endwhile; ?>
                                <?php       else: ?>
                                                
                                                    <div class="alert alert-info" role="alert">
                                                        <center>
                                                            <h3>No se encontraron resultados.</h3>
                                                        </center>
                                                    </div>
                                                
                                <?php       endif; ?>
                                </table>

 

