<?php
    require_once ('head.php');  
    require_once '../datos/Database.php';
    ini_set("date.timezone", "America/La_Paz");
    $inicioDate=date("Y-m");
    $inicioDate=$inicioDate."-01";
    
?>  
 <script src="../js/jquery.js"></script>
 <script src="gestionar_ingreso.js"></script>
<script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
<script src="../js/autocompletado/jquery-ui-1.10.3.custom.min.js"></script> 
<link rel="stylesheet" href="../js/autocompletado/jquery-ui-1.10.3.custom.min.css">  
<!-- Page Content -->
<div class="page-content">

    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>GESTIONAR INGRESO</strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                   <div class="widget-body " >
                       <br/>
                       <form class="responsive" id="frm_buscar" name="frm_buscar" method="post">  
                <div class="row">
                    <div class="col-sm-3 m-b">
                    <div class="form-group">
                        <fieldset>
                            <label for="user_name"><strong>Sucursal</strong></label>
                        <br />  
               
                        <select name='sucursal' id='sucursal' class="input-sm form-control input-s-sm inline">    
                          <!--  <option value="Todos">Todos</option>    -->
                            <?php
                            $consulta1 = "SELECT * FROM Sucursal WHERE idEmpresa=1 AND estado = 1";
                            $comando1 = Database::getInstance()->getDb()->prepare($consulta1);
                            $comando1->execute();
                            while ($fila = $comando1->fetch()) {
                                    echo "<option value='$fila[idSucursal]'/>$fila[nombreSucur]";
                                }
                               
                            ?>
                             
                            </select>
                        </fieldset>
                    </div>
                    </div> 
                    <div id="lista-tipoIngreso">
                    <div class="col-sm-3 m-b">
                    <div class="form-group">
                        <fieldset>
                            <label for="user_name"><strong>Tipo de Ingreso</strong></label>
                        <br />  
                        <select name="tipoIngreso" id="tipoIngreso" class="input-sm form-control input-s-sm inline">
                                <option value="Todos">Todos</option>
                            <?php
                            $consulta2 = "SELECT * FROM TipoIngresoCaja ";
                            $comando2 = Database::getInstance()->getDb()->prepare($consulta2);
                            $comando2->execute();
                            while ($fila = $comando2->fetch()) {
                                    echo "<option value='$fila[idTipoIngresoCaja]'/>$fila[nombreTipoIngresoCaja]";
                                }
                            ?>
                            </select>
                        </fieldset> 
                    </div>
                    </div>
                    </div>
                    <div class="col-sm-3 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Glosa</strong></label>
                        <br />
                        <input id="glosa" name="glosa" type="text" class="input-sm form-control input-s-sm inline">
                            <script>
                                $('#glosa').autocomplete({
                                    source: function( request, response ) {
                                        $.ajax({
                                            url : 'autocompletado.php',
                                            dataType: "json",
                                            data: {
                                               name_startsWith: request.term,
                                               type: "G"
                                            },
                                             success: function( data ) {
                                                 response( $.map( data, function( item ) {
                                                    return {
                                                        label: item,
                                                        value: item
                                                    };
                                                }));
                                            }
                                        });
                                    }	      	
                                });
                            </script>
                        </fieldset>
                    </div>
                    </div>
                     <div class="col-sm-3 m-b">                                
                    <div class="input-group">
                       
                        <label for="user_name"></label>
                        <br />
                        <a type="button" id="btnNuevoTipo" name="btnNuevoTipo" class="btn btn-warning shiny" onclick="javascript: ocul();" ><i class="fa fa-plus"></i>Nuevo Tipo Ingreso</a>
                        
                        
                    </div> 
                </div>
                </div>
                <div class="row">
                <div class="col-sm-3 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Fecha Inicial</strong></label>
                        <br />
                        <input type="date" name="fechaInicial" id="fechaInicial" value="<?=$inicioDate?>" class="input-sm form-control input-s-sm inline"/>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-3 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Fecha Final</strong></label>
                        <br />
                        <input type="date" name="fechaFinal" id="fechaFinal" value="<?=date("Y-m-d")?>" class="input-sm form-control input-s-sm inline"/>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-4 m-b">                                
                    <div class="input-group">
                       
                        <label for="user_name"></label>
                        <br />
                        <input type="button" id="btnBuscar" class="btn btn-yellow" value="Buscar" onclick="fn_buscar()" />&nbsp;&nbsp;
                        <input type="button" id="btnImprimir" class="btn btn-primary" value="Imprimir" onclick="fn_imprimirLista()"/>&nbsp;&nbsp;
                        <a type="button" id="btnNuevoIng" name="btnNuevoIng" class="btn btn-warning shiny" ><i class="fa fa-plus"></i>Nuevo Ingreso</a>
                        
                    </div> 
                </div>
                </div>
            </form>
                            </div>
                            <br>
                          <div class="widget-body " >
                                
                              <div class="table-responsive" id="div_listar">
                              
                              </div>
                              <br/>
                            </div>
                            <br/>
                        </div>
                
                    </div>
                </div>
            </div>

    <!-- /Page Content -->
</div>
<!-- /Page Container -->
<!-- FORMULARIO MODAL PARA REGISTRAR NUEVO INGRESO-->
<div class="modal fade" id="registra-ingreso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Ingreso</b></h4>
            </div>

            <!--              aqui agregamos los registros-->

            <form id="formularioReg" name="formularioReg" class="formulario" >
                <div class="modal-body">
                  <div class="row">
                      <div id="lista-modalTipoIngreso">
                    <div class="col-sm-4 m-b">
                    <div class="form-group">
                        <fieldset>
                            <label for="user_name"><strong>Tipo de Ingreso</strong></label>
                        <br />  
                        <select name="modalTipoIngreso" id="modalTipoIngreso" class="input-sm form-control input-s-sm inline">
                            <?php
                            $consulta2 = "SELECT * FROM TipoIngresoCaja ";
                            $comando2 = Database::getInstance()->getDb()->prepare($consulta2);
                            $comando2->execute();
                            while ($fila = $comando2->fetch()) {
                                $idIngreso=$fila['idTipoIngresoCaja'];
                                 if($idIngreso!=1){
                                    echo "<option value='".$idIngreso."'/>".$fila['nombreTipoIngresoCaja'];
                                    }
                                }
                            ?>
                            </select>
                        </fieldset> 
                    </div>
                    </div>
                      </div>
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Nro. Factura</strong></label>
                        <br />
                        <input type="text" name="nFac" id="nFac" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Nro. Nota Venta</strong></label>
                        <br />
                        <input type="text" name="nNota" id="nNota" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>    
                </div>
                    
                    <div class="row">
                    <div class="col-sm-8 m-b">                                
                    <div class="form-group">
                        <input name="idIngresoCaja" id="idIngresoCaja" type="text" hidden/>   
                        <fieldset>
                             
                        <label for="user_name"><strong>Glosa</strong></label>
                        <br />
                        <textarea name="modalGlosa" id="modalGlosa" class="form-control m-b" maxlength='200'></textarea>
                        
                        </fieldset>
                    </div>
                    </div>
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <?php 
                         $consulataCambio = "SELECT * FROM TipoCambio WHERE estado = 1";
                                        $comandoCambio = Database::getInstance()->getDb()->prepare($consulataCambio);
                                        $comandoCambio->execute();
                                        $tipoCambio = $comandoCambio->fetch();   
                        ?>
                        <fieldset>
                        <label for="user_name"><strong>Tipo de Cambio</strong></label>
                        <br />
                        <input type="text" name="modalCambio" id="modalCambio" value="<?=$tipoCambio['tipoCambioCompra']?>" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>    
                </div>
                <div class="row">
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Monto Total</strong></label>
                        <br />
                        <input type="text" name="modalMonto" id="modalMonto" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>
                    
                    
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Efectivo Bs.</strong></label>
                        <br />
                        <input type="text" name="modalEfectivoBs" id="modalEfectivoBs" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>
                    <div class="col-sm-4 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Efectivo $us.</strong></label>
                        <br />
                        <input type="text" name="modalEfectivoSus" id="modalEfectivoSus" value="" class="input-sm form-control input-s-sm inline"/>
                       
                        </fieldset>
                    </div>
                    </div>    
                </div>  
                   
                <div class="modal-footer">
                    <div class="form-inline" >   
                        <center>
                                 <div class="form-group">
                                     <button  id="reg"class="btn btn-primary" onclick="return GuardarRegistro();" data-dismiss="modal" aria-hidden="true">Guardar</button>
                                 </div>&nbsp;&nbsp;&nbsp;&nbsp;
                                 <div class="form-group">
                                     <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                                 </div>
                        </center>
                    </div>
                </div>

            
              
        </div>
            </form>        
    </div>
</div>  
</div>
    <!--modal para registrar nuevo tipo ingreso-->
<div class="modal fade" id="registra-Tipoingreso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Tipo Ingreso</b></h4>
            </div>
            <!--              aqui agregamos los registros-->
            <form id="formularioRegTipo" name="formularioRegTipo" class="formulario" method="post"  >
                <div class="modal-body">                 
                <div class="row" >                    
                    <div class="col-sm-12 m-b">                                
                    <div class="form-group">
                        <fieldset>
                        <label for="user_name"><strong>Nombre de Tipo Ingreso</strong></label>
                        <br />
                        <input type="text" name="modalNombre" id="modalNombre" class="input-sm form-control input-s-sm inline"/>                       
                        </fieldset>
                    </div>
                    </div>                   
                </div>                      
                <div class="modal-footer">
                    <div class="form-inline" id="ocultar">   
                        <center>
                              <div class="form-group">
                                   <button  id="regtipo" class="btn btn-primary" onclick="return GuardarRegistroTipo();"  aria-hidden="true">Guardar</button>
                              </div>&nbsp;&nbsp;&nbsp;&nbsp;
                              <div class="form-group">
                                   <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                              </div>
                        </center>
                    </div>
                    </br>
                   <!-- el div que oculto para editar-->
                    <div class="form-inline" id="ver" hidden >   
                        <center>
                          <div class="form-group">
                              <input type="text" name="id_tIng" id="id_tIng" class="input-sm form-control input-s-sm inline" aria-hidden="true"  readonly/>                                    
                          </div>&nbsp;&nbsp;&nbsp;&nbsp;
                                 </br>
                          <div class="form-group"> <!--onclick="return actualizar_TipoI();" -->
                              <button  id="actTipoI" name="actTipoI" class="btn btn-primary"  aria-hidden="true">actualizar</button>
                              <button  data-bb-handler="cancel" type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" >Cancelar</button>
                          </div>
                        </center>
                    </div>
                </div>
             <!------------------------------- tabla de tipos de ingresos-->
             <div class="widget-body" >
                 <h2><strong> LISTA DE TIPOS DE INGRESOS </strong></h2>
                <div id="tip_ing"name="tip_ing">
                                <table class="table table-striped" id="TipIngreso" name="TipIngreso" >
                                    <?php  

                                    $query_tipo_comp = "SELECT * FROM TipoIngresoCaja WHERE estado = 1";
                                                             
                                    $cmd_tip_c = Database::getInstance()->getDb()->prepare($query_tipo_comp);
                                    $cmd_tip_c->execute();
                                    $row_count_dv = $cmd_tip_c->rowCount();
                                     if($row_count_dv > 0): ?>
                                                <tr>
                                                    <th><strong>Codigo</strong></th>
                                                    <th><strong>Nombre</strong></th>
                                                    <th><strong></strong></th>
                                                </tr>    
                                    <?php       while($tipo_com = $cmd_tip_c->fetch()): ?>
                                    <?php         $id = $tipo_com['idTipoIngresoCaja'];
                                        ?>
                                                    <tr>
                                                        <td><?php echo  $id ?></td>

                                                        <td><?php echo $tipo_com['nombreTipoIngresoCaja'] ;?></td>
                                                        
                                                        <td>
                                                         <a href="javascript: actuali_tipIng('<?= $id ?>');" class="btn btn-default btn-xs purple" onclick="javascript: ocultar();" ><i class="fa fa-edit"></i> Editar</a>
                                                         <a href="javascript: eliminar_Tip('<?= $id ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                                                        </td>
                                                    </tr>
                                    <?php       endwhile; ?>
                                <?php       else: ?>
                                                
                                                    <div class="alert alert-info" role="alert">
                                                        <center>
                                                            <h3>No se encontraron resultados.</h3>
                                                        </center>
                                                    </div>
                                                
                                <?php       endif; ?>
                                </table>
                </div>
                               
            </div><br>
              
        </div>
            </form>        
    </div>
</div>  
</div>

<?php require_once '../header_footer/footer.php'; ?>


