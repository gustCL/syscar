$(document).ready(function(){
	fn_buscar(); 

});
$(function() {
    $('#btnNuevoIng').on('click', function() {
        
        $('#formularioReg')[0].reset();
        $('#idIngresoCaja').val('si');
        $('#registra-ingreso').modal({
            show: true,
            backdrop: 'static'
        });
    });
   $('#btnNuevoTipo').on('click', function() {
        
        $('#formularioRegTipo')[0].reset();
        $('#registra-Tipoingreso').modal({
            show: true,
            backdrop: 'static'
        });
    });
 
  $('#actTipoI').on('click',function(){
    //$("#ver").hide();
     //$("#ocultar").show();
       var nom = $('#modalNombre').val();
       var id = $('#id_tIng').val();
      // alert(id);
       //alert(nom);    
       ac_TipoI(id , nom);  

  });


});
function fn_buscar(){
	var str = $("#frm_buscar").serialize();
        var inicial=$("#fechaInicial").val();
        var final=$("#fechaFinal").val();
        
	$.ajax({
		url: 'lista_gestionarIngreso.php',
		type: 'get',
		data: str+"&fechaInicial="+inicial+"&fechaFinal="+final,
		success: function(data){
                    $("#div_listar").html(data);
                    
		}
	});
}
function fn_paginar(var_div, url){
	var div = $("#" + var_div);
	$(div).load(url);
}
function GuardarRegistro(){//registra un nuevo egreso
    var frm = $("#formularioReg").serialize();
    $.ajax({
        url: 'registrar_ingreso.php',
        data: frm,
        type: 'post',
        success: function(registro) {
 
             swal("Guardado Correctamente", "Ingreso Guardado Correctamente", "success");
           fn_buscar();
        }

    });
    return false;    
}
function fn_eliminarIngreso(id){

    $.ajax({
        url: 'eliminar_ingreso.php',
        data: 'id='+id,
        type: 'post',
        success: function(registro) {
           swal("Eliminado Exitosamente!", "El Ingreso Ha Sido Eliminado Correctamente", "success"); 
           fn_buscar();
        }

    });
    return false;    
}
function fn_mostrarIngreso(id){ //MUESTRA LOS DATOS DEL PROVEEDOR EN UN MODAL
   // alert(id);
	$('#formularioReg')[0].reset();
	var url = 'mostrar_ingreso.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'id='+id,
		success: function(valores){

				var datos = eval(valores);
                
				$('#modalTipoIngreso').val(datos[0]);
				$('#nFac').val(datos[1]);
				$('#nNota').val(datos[2]);
				$('#modalGlosa').val(datos[3]);
                                $('#modalCambio').val(datos[4]);
                                $('#modalMonto').val(datos[5]); 
                                $('#modalEfectivoBs').val(datos[6]);
                                $('#modalEfectivoSus').val(datos[7]);
                                $('#idIngresoCaja').val(id);

				$('#registra-ingreso').modal({

					show:true,
					backdrop:'static'
				});
     

			return false;
		}
	});
	return false;
}
function GuardarRegistroTipo(){
 var frm = $("#formularioRegTipo").serialize();
    $.ajax({
        url: 'registrar_tipoIngreso.php',
        data: frm,
        type: 'post',
        success: function(registro) {

      //$("#lista-tipoIngreso").html(registro);
      //$("#tip_ing").html(registro);
      swal("Guardado Correctamente", "Tipo Ingreso Guardado Correctamente", "success");
      ActualizarRegistroTipo();   

           return false;
        }

    });
    return false;     
}
function ActualizarRegistroTipo(){
// var frm = $("#formularioRegTipo").serialize();
    $.ajax({
        url: 'modal_tipoIngreso.php',
//        data: frm,
        type: 'post',
        success: function(registro) {
            //alert(registro);
      //$("#lista-modalTipoIngreso").html(registro);
        //   $("#tip_ing").html(registro);
           
        }

    });
    return false;     
}
function fn_imprimirLista(){
       $("#frm_buscar").serialize();
redirect_by_post("imprimir.php",{sucursal:$("#sucursal").val(),fechaInicial:$("#fechaInicial").val(),fechaFinal:$("#fechaFinal").val(),tipoIngreso:$("#tipoIngreso").val(),glosa:$("#glosa").val()},true);
 
};

function redirect_by_post(purl, pparameters, in_new_tab) {
    pparameters = (typeof pparameters == 'undefined') ? {} : pparameters;
    in_new_tab = (typeof in_new_tab == 'undefined') ? true : in_new_tab;
    var form = document.createElement("form");
    $(form).attr("id", "reg-form").attr("name", "reg-form").attr("action", purl).attr("method", "post").attr("enctype", "multipart/form-data");
    if (in_new_tab) {
        $(form).attr("target", "_blank");
    }
    $.each(pparameters, function(key) {
        $(form).append('<input type="text" name="' + key + '" value="' + this + '" />');
    });
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
    return false;
}

function mensaje() {
    swal("Modificacion Exitosa", "Este Tipo De Ingreso Fue Modificada Correctamente", "success");
}
function ocultar() {
    $("#ocultar").hide();
     $("#ver").show();
 return false;


}

function ocul() {
  $("#ocultar").show();
  $("#ver").hide();
  return false;
}


function actuali_tipIng(id){
  $.ajax({
        url: 'mod_TipoIng.php',
        data: 'id='+id,
        type: 'post',
        success: function(dato) {
              $('#id_tIng').val(id);
            $('#modalNombre').val(dato);
        return false;
        }
    });
    return false;     
}


function ac_TipoI(id , nom){  
  var frm = $('#formularioRegTipo').serialize();
 $.ajax({
    type: 'POST',
    url: 'modificar_I.php',
     data: frm,     
        success: function(registro) {      
        $('#modal-eliminar').modal('show');      
         // alert("hello word");
          $("#tip_ing").html(registro);
          swal("Modificado Correctamente", "Tipo Ingreso Modificado Correctamente", "success");  
          return false;            
        }
    });
   return false;    
}

function eliminar_Tip(id){
    $.ajax({
        url: 'eliminar_TipoI.php',
        data: 'id='+id,
        type: 'post',
        success: function(registro) {
            $("#tip_ing").html(registro);
            $("#lista-modalTipoIngreso").html(registro);
           swal("Eliminado Exitosamente!", "El Ingreso Ha Sido Eliminado Correctamente", "success");  
        }
    });
    return false;    
}

/*function ac_TipoI(id , nom){  
     var parametros = {
                "id" : id,
                "nom" : nom,
        };
 $.ajax({
    type: 'POST',
    url: 'modificar_I.php',
     data: parametros,     
        success: function(registro) {            
          alert("hello word");
          $("#tip_ing").html(registro);
          swal("Modificado Correctamente", "Tipo Ingreso Modificado Correctamente", "success");  
          return false;            
        }
    });
   return false;    
}*/
