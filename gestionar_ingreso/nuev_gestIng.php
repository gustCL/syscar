<?php
    require_once 'head.php';
    require_once '../datos/Database.php';

?>
<div class="page-content">
    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                <strong>TIPO DE INGRESO</strong>
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a>
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="main-container container-fluid">
        <!-- Page Body -->
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">

                            <div class="widget-body ">
                                <form method="post" action="javascript: GuardarRegistroTipo();" id="formularioRegTipo" class="form-horizontal" name="formularioRegTipo">
                                    <br>
                                    
                                    <div class="form-group" >
                                      <label class="col-lg-4 control-label"> Nombre </label>
                                        <div class="col-lg-3">
                                            <span class="input-icon">
                                                <input type="text" class="form-control" id="nom_comp" name="nom_comp" placeholder="" required>
                                            </span>
                                        </div>
                                    </div>
                                                                    
                               </br>
                                    <div class="form-inline">
                                        <center>   
                                        <!-- onclick="return GuardarRegistroTipo();"-->
                                            <input type="submit" class="btn btn-primary"  value="REGISTRAR" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input  type="button" class="btn btn-danger" href="../index.php" value="CANCELAR" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          <!--  <input type="button" value="Cancelar" class="btn btn-danger"/>-->
                                        </center>
                                    </div>
                                </form>
                            </div>
                            <br>
                                
                            <div class="widget-body" id="tip_ing" name="tip_ing">
                                <h2><strong> LISTA DE TIPOS DE INGRESOS </strong></h2>

                                <table class="table table-striped" >
                                    <?php  

                                    $query_tipo_comp = "SELECT * FROM TipoIngresoCaja WHERE estado = 1";
                                                             
                                    $cmd_tip_c = Database::getInstance()->getDb()->prepare($query_tipo_comp);
                                    $cmd_tip_c->execute();
                                    $row_count_dv = $cmd_tip_c->rowCount();
                                     if($row_count_dv > 0): ?>
                                                <tr>
                                                    <th><strong>Codigo</strong></th>
                                                    <th><strong>Nombre</strong></th>
                                                    <th><strong></strong></th>
                                                </tr>    
                                    <?php       while($tipo_com = $cmd_tip_c->fetch()): ?>
                                    <?php         $id = $tipo_com['idTipoIngresoCaja'];
                                        ?>
                                                    <tr>
                                                        <td><?=  $id ?></td>

                                                        <td><?= $tipo_com['nombreTipoIngresoCaja'] ;?></td>
                                                        
                                                        <td>
                                                         <a href="=<?= $id ?>" class="btn btn-default btn-xs purple"  ><i class="fa fa-edit"></i> Editar</a>
                                                         <a href="('<?= $id ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                                                        </td>
                                                    </tr>
                                    <?php       endwhile; ?>
                                <?php       else: ?>
                                                
                                                    <div class="alert alert-info" role="alert">
                                                        <center>
                                                            <h3>No se encontraron resultados.</h3>
                                                        </center>
                                                    </div>
                                                
                                <?php       endif; ?>
                                </table>
                            </div><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- /Page Content -->
</div>

<?php require_once '../header_footer/footer.php'; ?>
