<?php 

include "../datos/conexion.php";
include "../extras/PHPPaging.lib/PHPPaging.lib.php";
ini_set("date.timezone", "America/La_Paz");
$fechaI=$_REQUEST['fechaInicial'];
$fechaF=$_REQUEST['fechaFinal'];
$fechaInicial=date_create($fechaI);
$fechaFinal=date_create($fechaF);    
$nro=1;
mysql_query("SET NAMES 'utf8'");
    $consulta ="SELECT ic.*, tic.nombreTipoIngresoCaja FROM IngresoCaja ic, TipoIngresoCaja tic, InicioSesion vs,Sucursal s WHERE ic.estado = 1 AND tic.estado= 1 AND ic.fechaRegistro>='".date_format($fechaInicial, 'd-m-Y')."' AND ic.fechaRegistro<='".date_format($fechaFinal, 'd-m-Y')."' AND tic.idTipoIngresoCaja=ic.idTipoIngresoCaja  AND ic.idInicio = vs.idInicio and vs.sucursal=s.idSucursal";
    $consultaTotal="SELECT sum(ic.montoTotal) as total FROM IngresoCaja ic, TipoIngresoCaja tic, InicioSesion vs,Sucursal s WHERE ic.estado = 1 AND tic.estado= 1 AND ic.fechaRegistro>='".date_format($fechaInicial, 'd-m-Y')."' AND ic.fechaRegistro<='".date_format($fechaFinal, 'd-m-Y')."' AND tic.idTipoIngresoCaja=ic.idTipoIngresoCaja AND  ic.idInicio = vs.idInicio and vs.sucursal=s.idSucursal ";
    if($_REQUEST['sucursal'] != 'Todos'){
        $consulta.="  and vs.sucursal=".$_REQUEST['sucursal'];
        $consultaTotal.=" and vs.sucursal=".$_REQUEST['sucursal'];       
    }
        if ($_REQUEST['glosa'] != ''){
        $consulta.=" and ic.glosa LIKE '".$_REQUEST['glosa']."%'";
        $consultaTotal.=" and ic.glosa LIKE '".$_REQUEST['glosa']."%'";
    }

    if($_REQUEST['tipoIngreso'] != 'Todos'){
        $consulta.=" and tic.idTipoIngresoCaja = ".$_REQUEST['tipoIngreso'];
        $consultaTotal.=" and tic.idTipoIngresoCaja = ".$_REQUEST['tipoIngreso'];
    }

$comando = mysql_query($consultaTotal);
$resTotal= mysql_fetch_array($comando);    
$montoTotal=$resTotal['total'];
$pagina = new PHPPaging;
?>
<br/> 
                                <table class="table table-striped">
                                    
                                    <thead>
                                        <!--Fila de titulos de columnas-->
                                        <tr>
                                            <th>
                                    <center><strong> Id.</strong></center>
                                            </th>
                                            <th>
                                    <center>  <strong>Fecha</strong></center>
                                            </th>
                                            <th>
                                    <center>   <strong>Tipo Ingreso</strong></center>
                                            </th>
                                            
                                            <th>
                                    <center> <strong>Glosa</strong></center>
                                            </th>
                                            <th>
                                    <center>  <strong>Nro. Factura</strong></center>
                                            </th>
                                            <th>
                                    <center><strong>Nro. Nota Venta</strong></center>
                                            </th>
                                            <th>
                                    <center> <strong>Monto (Bs.)</strong></center>
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                    </thead>
<?php
$pagina->agregarConsulta($consulta);

  $pagina->modo('desarrollo'); 
  $pagina->verPost(true);
$pagina->porPagina(10);
  $pagina->paginasAntes(4);
  $pagina->paginasDespues(4);
  $pagina->linkSeparador(" - "); //Significa que no habrá separacion
  $pagina->div('div_listar');
  $pagina->linkSeparadorEspecial('...');   // Separador especial
  
$pagina->ejecutar();

while($res=$pagina->fetchResultado()){
        
	$fecha=$res["fechaRegistro"];
	$nombre=$res["nombreTipoIngresoCaja"];
	$glosa=$res["glosa"];
        $nFac=$res["nroFactura"];
        $nNot=$res["nroVenta"];
        $monto=$res['montoTotal'];
        $id=$res["idIngresoCaja"];
        $idTipoIngreso=$res['idTipoIngresoCaja'];
?>  
<tbody>                                    
<tr>
<td><center><?php echo $id ?></center></td>    
<td><center><?php echo $fecha; ?></center></td>
<td><center><?php echo $nombre; ?></center></td>
<td><center><?php echo $glosa; ?></center></td>
<td><center><?php echo $nFac; ?></center></td>
<td><center><?php echo $nNot; ?></center></td>
<td><center><?php echo $monto?></center></td>
<td>
    <?php if ($idTipoIngreso!=1){?>
    <center> 
    <a href="javascript:fn_mostrarIngreso('<?=$id?>');" id="editar" class="btn btn-default btn-xs purple" ><i class="fa fa-edit"></i> Editar</a>
    <a href="javascript:fn_eliminarIngreso('<?=$id?>');" id="eliminar" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
    </center>
    <?php }?>
</td>                                  
</tr>
</tbody>
<?php
}
?>
<tfoot>
        <tr>
                                            <td colspan="5" invisible bg-snow> </td>
                                            <td align="right"><strong>Monto total (Bs.):</strong> </td>
                                            <td><center><?=  number_format($montoTotal,2)?></center></td>
                                        </tr>
            <td colspan="7">Numero de pagina: <?php echo $pagina->fetchNavegacion(); ?></td>                   
        </tr>
</tfoot>
</table>
