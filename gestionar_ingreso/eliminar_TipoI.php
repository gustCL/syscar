<?php
require_once("../datos/Database.php");
$id = $_POST['id'];

$elimina = Database::getInstance()->getDb()->prepare("UPDATE TipoIngresoCaja SET estado=0 WHERE idTipoIngresoCaja='$id'");
$elimina->execute();


?>


                                <table class="table table-striped" >
                                    <?php  

                                    $query_tipo_comp = "SELECT * FROM TipoIngresoCaja WHERE estado = 1";
                                                             
                                    $cmd_tip_c = Database::getInstance()->getDb()->prepare($query_tipo_comp);
                                    $cmd_tip_c->execute();
                                    $row_count_dv = $cmd_tip_c->rowCount();
                                     if($row_count_dv > 0): ?>
                                                <tr>
                                                    <th><strong>Codigo</strong></th>
                                                    <th><strong>Nombre</strong></th>
                                                    <th><strong></strong></th>
                                                </tr>    
                                    <?php       while($tipo_com = $cmd_tip_c->fetch()): ?>
                                    <?php         $id = $tipo_com['idTipoIngresoCaja'];
                                        ?>
                                                    <tr>
                                                        <td><?=  $id ?></td>

                                                        <td><?= $tipo_com['nombreTipoIngresoCaja'] ;?></td>
                                                        
                                                        <td>
                                                         <a href="javascript: actuali_tipIng('<?= $id ?>');" class="btn btn-default btn-xs purple" onclick="javascript: ocultar();" ><i class="fa fa-edit"></i> Editar</a>
                                                         <a href="javascript:eliminarTipoEgreso('<?= $id ?>');" class="btn btn-default btn-xs black" ><i class="fa fa-trash-o"></i> Eliminar</a>
                                                        </td>
                                                    </tr>
                                    <?php       endwhile; ?>
                                <?php       else: ?>
                                                
                                                    <div class="alert alert-info" role="alert">
                                                        <center>
                                                            <h3>No se encontraron resultados.</h3>
                                                        </center>
                                                    </div>
                                                
                                <?php       endif; ?>
                                </table>