<?php
require_once '../datos/Database.php';
ini_set("date.timezone", "America/La_Paz");
$fecha = date("d-m-Y");
$hora = date("H:i:s");
session_start();
$idIngresoCaja=$_POST['idIngresoCaja'];

$idInicio=$_SESSION['idSesion'];
$idTipoIngreso= $_POST['modalTipoIngreso'];
$nroF=$_POST['nFac'];
$nrN=$_POST['nNota'];
$glosa=$_POST['modalGlosa'];
$montoTotal=$_POST['modalMonto'];
$EfectivoBs=$_POST['modalEfectivoBs'];
$EfectivoSus=$_POST['modalEfectivoSus'];
$cambio=$_POST['modalCambio'];
if ($idIngresoCaja=='si'){
$consulta ="INSERT INTO IngresoCaja(idIngresoCaja, idInicio, idTipoIngresoCaja, nroFactura, nroVenta, glosa, montoTotal, idTipoCambio, tipoCambio, efectivoBs, efectivoSus, fechaRegistro, horaRegistro, estado) VALUES ('',?,?,?,?,?,?,' ',?,?,?,?,?,?)";
$comando =Database::getInstance()->getDb()->prepare($consulta);
$comando->execute([$idInicio,$idTipoIngreso,$nroF,$nrN,$glosa,$montoTotal,$cambio,$EfectivoBs,$EfectivoSus,$fecha,$hora,1]);
 
}else{
$consulta ="UPDATE IngresoCaja SET idTipoIngresoCaja=?,nroFactura=?,nroVenta=?,glosa=?,montoTotal=?,tipoCambio=?,efectivoBs=?,efectivoSus=? WHERE idIngresoCaja= '".$idIngresoCaja."'";
$comando =Database::getInstance()->getDb()->prepare($consulta);
$comando->execute([$idTipoIngreso,$nroF,$nrN,$glosa,$montoTotal,$cambio,$EfectivoBs,$EfectivoSus]);
       
}
?>