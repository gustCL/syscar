<?php
session_start();
 require_once '../datos/Database.php';
 ini_set("date.timezone", "America/La_Paz");
$fechaI=$_REQUEST['fechaInicial'];
$fechaF=$_REQUEST['fechaFinal'];
$fechaInicial=date_create($fechaI);
$fechaFinal=date_create($fechaF);
$consulta="SELECT ic.*, tic.nombreTipoIngresoCaja FROM IngresoCaja ic, TipoIngresoCaja tic,  InicioSesion vs,Sucursal s WHERE ic.estado = 1 AND tic.estado=1 AND ic.fechaRegistro>='".date_format($fechaInicial, 'd-m-Y')."' AND ic.fechaRegistro<='".date_format($fechaFinal, 'd-m-Y')."' AND tic.idTipoIngresoCaja=ic.idTipoIngresoCaja  AND ic.idInicio = vs.idInicio and vs.sucursal=s.idSucursal ";
$consultaTotal="SELECT sum(ic.montoTotal) as total FROM IngresoCaja ic, TipoIngresoCaja tic,  InicioSesion vs,Sucursal s WHERE ic.estado = 1 AND tic.estado=1 AND ic.fechaRegistro>='".date_format($fechaInicial, 'd-m-Y')."' AND ic.fechaRegistro<='".date_format($fechaFinal, 'd-m-Y')."' AND tic.idTipoIngresoCaja=ic.idTipoIngresoCaja  AND ic.idInicio = vs.idInicio and vs.sucursal=s.idSucursal ";
    if($_REQUEST['sucursal'] != 'Todos'){
        $consulta.="  and vs.sucursal=".$_REQUEST['sucursal'];
        $consultaTotal.=" and vs.sucursal=".$_REQUEST['sucursal'];       
    }
        if ($_REQUEST['glosa'] != ''){
        $consulta.=" and ic.glosa LIKE '".$_REQUEST['glosa']."%'";
        $consultaTotal.=" and ic.glosa LIKE '".$_REQUEST['glosa']."%'";
    }
    $tipoNombre='Todos';
    if($_REQUEST['tipoIngreso'] != 'Todos'){
        $consultaTipo="SELECT * FROM TipoIngresoCaja WHERE estado = 1 AND idTipoIngresoCaja=".$_REQUEST['tipoIngreso'];
    $comandoTipo=Database::getInstance()->getDb()->prepare($consultaTipo);
    $comandoTipo->execute();     
    $tipo=$comandoTipo->fetch();
    $tipoNombre=$tipo['nombreTipoIngresoCaja'];
        $consulta.=" and tec.idTipoIngresoCaja = ".$_REQUEST['tipoIngreso'];
        $consultaTotal.=" and tec.idTipoIngresoCaja = ".$_REQUEST['tipoIngreso'];       
    }
    $comando=Database::getInstance()->getDb()->prepare($consulta);
    $comando->execute();
    
    $comandoTotal=Database::getInstance()->getDb()->prepare($consultaTotal);
    $comandoTotal->execute();
    $montoTotal = $comandoTotal->fetch();
    
?> 
<html> 
<head> 
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" type="text/css" href="../css/print/stilo.css" media="screen">
    <link rel="stylesheet" type="text/css" href="../css/print/print.css" media="print">
    <!--<link href="printListaTransac.css" rel="stylesheet" type="text/css" media="print" />-->  
<!--    <link href="../css/style.css" rel="stylesheet" media="all">-->
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet"  media="all">
    <script src="../js/autocompletado/jquery-1.10.2.min.js"></script>
    <!--<script language="javascript" type="text/javascript" src="JSListaTransacciones.js"></script>-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head> 
<body> 
    <script language="JavaScript">
    $(document).ready(function (){
        doPrint();
    });
    function doPrint(){
    //document.all.item("mostrarUser").style.visibility='visible'; 
        window.print();
    //document.all.item("mostrarUser").style.visibility='hidden';
    }
    function volver(){
        location.href="../gestionar_ingreso";
    }
</script>
<div id="leteral"></div>
<div id="noprint">
    <table>
        <tr>
            <td>
                <button type="button" name="volver" id="volver" onclick="volver()">Volver</button>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>
                <button name="imprimir" id="imprimir" onclick="doPrint()">Imprimir</button>
            </td>
        </tr>
    </table>
</div>

<div id="hoja" style="height: auto">    
    <?php 
    require_once 'cabezera.php';
    ?>
    <div  class="mitabla">
    <div class="table-responsive">
        <hr class="hr" style="margin-bottom: 0px; padding-bottom: 0px;">
        <table class="table table-striped m-t-none border-left-right" style="margin-top: 0px; padding-top: 0px">
      <thead>
            <tr>
                <th>Nro.</th>
                <th>Fecha</th>
                <th>Tipo de Egreso</th>
                <th>Glosa</th>
                <th>Nro. Factura</th>
                <th>Nro. Nota de Venta</th>
                <th>Monto (Bs.)</th>
            </tr>
        </thead>
        <tbody>
            <?php
               $filas=0;
               $n=0;
                      while ($res = $comando->fetch()){
                          $filas++;
                          $n++;
                          
                            if($filas>16 ){
                            $filas=0 ;
                            ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
                            
 <div id="hoja" style="height: auto">  <?php
                            echo "<div class='saltoBefore'></div>";
                            require 'cabezera.php';
                            ?>  
       <div  class="mitabla">
    <div class="table-responsive">
        <hr class="hr" style="margin-bottom: 0px; padding-bottom: 0px;">
        <table class="table table-striped m-t-none border-left-right" style="margin-top: 0px; padding-top: 0px">
      <thead>
            <tr>
                <th>Nro.</th>
                <th>Fecha</th>
                <th>Tipo de Egreso</th>
                <th>Glosa</th>
                <th>Nro. Factura</th>
                <th>Nro. Nota de Venta</th>
                <th>Monto (Bs.)</th>
            </tr>
        </thead>
        <tbody>
            
            <?php
                            }
                  ?>
            <tr>
                <td><?=$n?></td>
                <td><?=$res["fechaRegistro"]?></td>
                <td><?=$res["nombreTipoIngresoCaja"]?></td>
                <td><?=$res["glosa"]?></td>
                <td><?=$res["nroFactura"]?></td>
                <td><?=$res["nroVenta"]?></td>
                <td><?=  number_format($res['montoTotal'],2)?></td>
<!--                <td style=" text-align: right"></td>
                <td style=" text-align: right"></td>
                <td style=" text-align: center;" ></td>
                <td style=" text-align: center"></td>-->
                
            </tr>
      <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="11"><br></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: right"> Total: </td>
                <td style=" text-align: right"><?=  number_format($montoTotal['total'],2) ?></td>
<!--                <td style=" text-align: right"></td>
                <td style=" text-align: right"></td>
                <td style=" text-align: right"></td>-->
                <td colspan="2"></td>
            </tr>
        </tfoot>
    </table>
    </div>
    
</div>
</body>
</html>

