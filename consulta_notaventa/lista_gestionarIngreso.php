<?php


include "../datos/conexion.php";

include "../extras/PHPPaging.lib/PHPPaging.lib.php";

ini_set("date.timezone", "America/La_Paz");

$fechaI = $_REQUEST['fechaInicial'];

$fechaF = $_REQUEST['fechaFinal'];

$fechaInicial = $fechaI;

$fechaFinal = $fechaF;

$nro = 0;

mysql_query("SET NAMES 'utf8'");

$consulta = "SELECT v.*, cl.*,tr.fecha, vs.nroVenta
                  FROM Ventas v, Transaccion tr, Cliente cl,VentaSucursal vs 
                  WHERE v.idTransaccion=tr.idTransaccion AND tr.tipoTrans = 1 AND tr.estado = 1
                  AND tr.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND v.idCliente=cl.idCliente AND vs.idVenta = v.idVenta ORDER BY vs.idVenta DESC";

//$consultaTotal="SELECT sum(ic.montoTotal) as total FROM IngresoCaja ic, TipoIngresoCaja tic, InicioSesion vs,Sucursal s WHERE ic.estado = 1 AND tic.estado= 1 AND ic.fechaRegistro>='".date_format($fechaInicial, 'd-m-Y')."' AND ic.fechaRegistro<='".date_format($fechaFinal, 'd-m-Y')."' AND tic.idTipoIngresoCaja=ic.idTipoIngresoCaja AND  ic.idInicio = vs.idInicio and vs.sucursal=s.idSucursal ";

$consultaTotal = "SELECT SUM(v.montoTotal)as total
                  FROM Ventas v, Transaccion tr, Cliente cl,VentaSucursal vs
                  WHERE v.idTransaccion=tr.idTransaccion AND tr.tipoTrans = 1 AND tr.estado = 1
                  AND tr.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND v.idCliente=cl.idCliente AND vs.idVenta = v.idVenta ORDER BY vs.idVenta DESC";

$comando = mysql_query($consulta);
$resTotal = mysql_fetch_array($comando);


$comandoV = mysql_query($consultaTotal);
$resTotalV = mysql_fetch_array($comandoV);

$montoTotal = $resTotalV['total'];
$pagina = new PHPPaging;

?>

<br/>

<table class="table table-striped">
    <thead>
    <!--Fila de titulos de columnas-->
        <tr>
            <th style="width: 10%"><center><strong> Nota Venta.</strong></center></th>
            <th style="width: 15%"><center><strong>Fecha</strong></center></th>
            <!--<th><center><strong>Nro. Factura</strong></center></th>-->
            <th style="width: 40%"><center><strong>Nombre del Cliente</strong></center></th>
            <th style="width: 20%"><center><strong>Monto (Bs.)</strong></center></th>
            <!--<th><center><strong>Re-imprimir</strong></center></th>-->
            <th style="width: 15%"><center><strong>Anular</strong></center></th>
        </tr>
    </thead>

    <?php

    $pagina->agregarConsulta($consulta);
    $pagina->modo('desarrollo');
    $pagina->verPost(true);
    $pagina->porPagina(50);
    $pagina->paginasAntes(4);
    $pagina->paginasDespues(4);
    $pagina->linkSeparador(" - "); //Significa que no habrá separacion
    $pagina->div('div_listar');
    $pagina->linkSeparadorEspecial('...');   // Separador especial
    $pagina->ejecutar();


    while ($res = $pagina->fetchResultado()) {
        $nro++;
        $nroSucursal = $res['nroVenta'];

        $fecha = date("d-m-Y", strtotime($res["fecha"]));

        $nombre = $res["nombreCliente"];

        $idTran = $res['idTransaccion'];

        $monto = $res['montoTotal'];

        $idV = $res['idVenta'];

        ?>

        <tbody>

        <tr>

            <td>
                <center><?php echo $nroSucursal ?></center>
            </td>

            <td>
                <center><?php  echo $fecha; ?></center>
            </td>

            <td>
                <center><?php echo $nombre ?></center>
            </td>

            <td>
                <center><?php echo $monto ?></center>
            </td>

            <!--<td>

                <center>

                    <a href="factura/imprime_factura.php?vt=<?/*= $idV */?>" id="editar"><img width="30" height="30"
                                                                                          src="../assets/img/imprimir.png"></a>

                </center>


            </td>-->

            <td>

                <center>

                    <a href="anular.php?v=<?= $idTran ?>" id="eliminar"><img width="30" height="30"
                                                                                         src="../assets/img/anular.png"></a>

                </center>


            </td>
        </tr>

        </tbody>

        <?php

    }

    ?>

    <tfoot>

    <tr>
        <td colspan="3" invisible bg-snow></td>

        <td align="right"><strong>Monto total (Bs.):</strong></td>

        <td>
            <center><?= number_format($montoTotal, 2) ?></center>
        </td>

        <td></td>

        <td></td>

    </tr>

    <td colspan="7">Numero de pagina: <?php echo $pagina->fetchNavegacion(); ?></td>

    </tr>

    </tfoot>

</table>

