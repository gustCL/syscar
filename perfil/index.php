<?php
    require_once 'head.php';
?>
    <div class="page-content">
        <!-- subtTitulo -->
        <div class="page-header position-relative">
            <div class="header-title">
                <h1><strong>MI PERFIL DE USUARIO</strong></h1>
            </div>
            <!--Header Buttons-->
            <div class="header-buttons">
                <a class="sidebar-toggler" href="#">
                    <i class="fa fa-arrows-h"></i>
                </a>
                <a class="refresh" id="refresh-toggler" href="#">
                    <i class="glyphicon glyphicon-refresh"></i>
                </a>
                <a class="fullscreen" id="fullscreen-toggler" href="#">
                    <i class="glyphicon glyphicon-fullscreen"></i>
                </a>
            </div>
        </div>
        <div class="page-body">
            <div class="widget-body">
                <h2><strong><center>CAMBIAR CONTRASEÑA</center></strong></h2>
                <br>
                <div class="col-md-offset-2">
                    <p class="lead">Ingrese su nombre de usuario y su clave actual para poder cambiar a una nueva clave.</p>
                </div>
                <form id="change-password" action="javascript: cambiarClave();" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-4 control-label lead">Usuario:</label>
                        <div class="col-md-4">
                            <input type="text" title="INGRESE NOMBRE DE USUARIO"
                                   class="form-control" name="nombreusuario" id="nombreusuario"
                                   placeholder="Nombre de usuario" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label lead">Clave:</label>
                        <div class="col-md-4">
                            <input type="password" title="INGRESE CLAVE DE SEGURIDAD"
                                   class="form-control" name="password" id="password"
                                   placeholder="Clave" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label lead">Nueva Clave:</label>
                        <div class="col-md-4">
                            <input type="password" title="NUEVA CLAVE DE SEGURIDAD"
                                   class="form-control" name="new-password-1" id="new-password-1"
                                   placeholder="Nueva Clave" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label lead">Repita Nueva Clave:</label>
                        <div class="col-md-4">
                            <input type="password" title="INGRESE CLAVE DE SEGURIDAD"
                                   class="form-control" name="new-password-2" id="new-password-2"
                                   placeholder="Vuelva a escribir su nueva clave" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-5 col-md-6">
                            <input id="btn-guardar" class="btn btn-primary" type="submit" value="Guardar Datos">
                            <a id="btn-cancelar" href="../inicio" class="btn btn-danger">Cancelar</a>
                        </div>
                    </div>
                    <div>
                        <div id="exito" class="alert alert-info" role="alert" hidden>
                            <center>
                                <h2>Contraseña modificada</h2>
                            </center>
                        </div>
                        <div id="error" class="alert alert-danger" role="alert" hidden>
                            <center>
                                <h2>Usuario o Contraseña incorrecta</h2>
                            </center>
                        </div>
                        <div id="pass-diferentes" class="alert alert-danger" role="alert" hidden>
                            <center>
                                <h2>Contraseñas deben ser iguales</h2>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
    require_once('../header_footer/footer.php');
?>