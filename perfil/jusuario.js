﻿// Cambia la clave del usuario
function cambiarClave() {
    var pass_1 = document.getElementById('new-password-1');
    var pass_2 = document.getElementById('new-password-2');
    if (pass_1.value == pass_2.value) {

        pass_1.style.border = null;
        pass_2.style.border = null;

        var frm = $("#change-password").serialize();
        $.ajax({
            url: 'cambiar_password.php',
            data: frm,
            type: 'post',
            success: function (result) {
                if (result == 'exito_1') {
                    $("#exito").show();
                    $("#error").hide();
                    $('#pass-diferentes').hide();

                    document.getElementById('nombreusuario').readOnly = true;
                    document.getElementById('password').readOnly = true;
                    pass_1.readOnly = true;
                    pass_2.readOnly = true;

                    $('#btn-guardar').prop('disabled', true);
                    $('#btn-cancelar').text('Salir');
                    alertify.success("Contraseña modificada");
                } else {
                    $("#error").show();
                    $("#exito").hide();
                    $('#pass-diferentes').hide();
                }
            }
        });
    } else {
        pass_1.style.border = '2px solid #da4517';
        pass_2.style.border = '2px solid #da4517';
        $("#exito").hide();
        $("#error").hide();
        $('#pass-diferentes').show();
    }
}