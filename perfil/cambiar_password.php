<?php

    require_once("../datos/Database.php");

    session_start();

    // Id del usuario activo en la sesión
    $id_usuario = $_SESSION['userMaster']['idUsuario'];

    // Parámetros obtenidos del formulario de entrada de datos
    $username = $_REQUEST['nombreusuario'];
    $password = $_REQUEST['password'];
    $new_password_1 = $_REQUEST['new-password-1'];
    $new_password_2 = $_REQUEST['new-password-2'];

    // Comprobar que el nombre de usuario y contraseña son correctos
    $query = "SELECT usuario, clave
              FROM Usuario
              WHERE idUsuario = :id_usuario";
    $command = Database::getInstance()->getDb()->prepare($query);
    $command->bindParam(':id_usuario', $id_usuario);
    $command->execute();
    $result = $command->fetch();

    if ($result['usuario'] != $username || $result['clave'] != md5($password)) {
        echo 'error_1';
        exit();
    } else {

        // Se actualiza la clave del usuario, previa encriptación
        $query_update_password = "UPDATE Usuario set clave = :new_password WHERE idUsuario = :id_usuario";
        $cmd_update_password = Database::getInstance()->getDb()->prepare($query_update_password);
        $cmd_update_password->bindParam(':new_password', md5($new_password_1));
        $cmd_update_password->bindParam(':id_usuario', $id_usuario);
        $cmd_update_password->execute();

        echo 'exito_1';
        exit();
    }

    /*  Tipos de notificaciones:
     *             error_1     :   Nombre de usuario o contraseña no coinciden con el usuario activo en la sesión
     *             exito_1     :   Parámetros válidos, coinciden con el usuario activo en la sesión
     */
?>